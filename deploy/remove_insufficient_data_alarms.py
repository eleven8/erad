from boto.ec2.cloudwatch import connect_to_region
cw = connect_to_region('us-west-2')
next_token=""

while next_token is not None:
    insufficient_data_alarms = cw.describe_alarms(state_value="INSUFFICIENT_DATA", max_records=100, next_token=next_token)
    next_token = insufficient_data_alarms.next_token
    if len(insufficient_data_alarms) > 0:
        try:
            # this is left to allow clean up of old alarms, can be removed in a few weeks
            cw.delete_alarms([alarm.name for alarm in insufficient_data_alarms if 'cpu-credit-balance-' in alarm.name])
        except:
            pass
        try:
            cw.delete_alarms([alarm.name for alarm in insufficient_data_alarms if 'cpu-surplus-credits-charged-' in alarm.name])
        except:
            pass
        try:
            cw.delete_alarms([alarm.name for alarm in insufficient_data_alarms if 'disk-utilization-' in alarm.name])
        except:
            pass
        try:
            cw.delete_alarms([alarm.name for alarm in insufficient_data_alarms if 'swap-utilization-' in alarm.name])
        except:
            pass
