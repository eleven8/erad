#!/bin/bash

export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
# AWS_SECRET_ACCESS_KEY injected by Jenkins
export AWS_DEFAULT_REGION=us-west-2
export DEPLOY_S3_BUCKET="teamfortytwo.deploy"
export API_URL="https://api.enterpriseauth.com"

# $CLUSTER_ID variable should be a string with cluster ids separated only by commas, or one cluster id value
# Examples: 'a,b,c', 'a'

IFS=$',' read -r -a clusters_list <<< "$CLUSTER_ID"

# verify we are in transition
if aws s3 ls s3://$DEPLOY_S3_BUCKET/ | grep "FRMOD-TRANSITION-IN-PROGRESS"
then

  # promote archives
  aws s3 mv s3://$DEPLOY_S3_BUCKET/erad-load-balancer.stage.tar.gz s3://$DEPLOY_S3_BUCKET/erad-load-balancer.tar.gz
  aws s3 mv s3://$DEPLOY_S3_BUCKET/erad_freeradius.stage.tar.gz s3://$DEPLOY_S3_BUCKET/erad_freeradius.tar.gz
  aws s3 mv s3://$DEPLOY_S3_BUCKET/staged_radius_version s3://$DEPLOY_S3_BUCKET/deployed_radius_version


  # tear-down previous production stacks
  aws s3 cp "s3://$DEPLOY_S3_BUCKET/frmod_production_stacks" ./
  aws s3 cp "s3://$DEPLOY_S3_BUCKET/frmod_production_stacks_radius" ./

  cat "frmod_production_stacks" > work.tmp
  cat "frmod_production_stacks_radius" >> work.tmp
  while read stack_name; do
    aws --region us-west-2 cloudformation delete-stack --stack-name $stack_name
    aws --region us-east-1 cloudformation delete-stack --stack-name $stack_name
    aws --region us-east-2 cloudformation delete-stack --stack-name $stack_name
  done < ./work.tmp

  # clean up stage info files
  aws s3 rm "s3://$DEPLOY_S3_BUCKET/frmod_stage_stacks"
  aws s3 rm "s3://$DEPLOY_S3_BUCKET/frmod_stage_stacks_radius"
  aws s3 rm "s3://$DEPLOY_S3_BUCKET/frmod_production_stacks"
  aws s3 rm "s3://$DEPLOY_S3_BUCKET/frmod_production_stacks_radius"

  aws s3 rm s3://$DEPLOY_S3_BUCKET/FRMOD-TRANSITION-IN-PROGRESS

  for current_cluser_id in ${clusters_list[@]}; do
    # Clean Erad_Endpoint from ::dirty ports (skip useast-01-0001, we're decommissioning)
    [ "$current_cluser_id" != "useast-01-0001" ] && curl -X POST --header 'Content-Type: application/json' -d "{\"SystemKey\": \"$SYSTEM_KEY\", \"Cluster_ID\": \"$current_cluser_id\" }" "${API_URL}/erad/system/radius/finish"
  done

  exit 0
else
  echo "We don't appear to be in transition."
  exit 1
fi
