#!/bin/bash

export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
# AWS_SECRET_ACCESS_KEY injected by Jenkins
export AWS_DEFAULT_REGION=us-west-2

# do not tear down if rollout is under way
if aws s3 ls s3://teamfortytwo.deploy/ | grep "TRANSITION-IN-PROGRESS"
then
  echo "Found TRANSITION-IN-PROGRESS"
  exit 0
fi

# delete any stacks listed on S3
set +e
touch stage_stacks stage_stacks_radius
aws s3 cp s3://teamfortytwo.deploy/stage_stacks .
aws s3 cp s3://teamfortytwo.deploy/stage_stacks_radius .
cat stage_stacks_radius >> stage_stacks
while read stack_name; do
  aws --region us-west-2 cloudformation delete-stack --stack-name $stack_name
  aws --region us-east-1 cloudformation delete-stack --stack-name $stack_name
  aws --region us-east-2 cloudformation delete-stack --stack-name $stack_name
done < ./stage_stacks

# delete stage files
aws s3 rm s3://teamfortytwo.deploy/stage_stacks
aws s3 rm s3://teamfortytwo.deploy/stage_stacks_radius
aws s3 rm s3://teamfortytwo.deploy/production_stacks
aws s3 rm s3://teamfortytwo.deploy/production_stacks_radius
aws s3 rm s3://teamfortytwo.deploy/stage_ips
aws s3 rm s3://teamfortytwo.deploy/stage_ips_radius
aws s3 rm s3://teamfortytwo.deploy/staged_api_version
aws s3 rm s3://teamfortytwo.deploy/staged_radius_version
aws s3 rm s3://teamfortytwo.deploy/erad-load-balancer.stage.tar.gz
aws s3 rm s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz
aws s3 rm s3://teamfortytwo.deploy/erad_freeradius.stage.tar.gz

exit 0
