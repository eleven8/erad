#!/bin/bash

export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
# AWS_SECRET_ACCESS_KEY injected by Jenkins
export AWS_DEFAULT_REGION=us-west-2

Regions="us-west-2 us-east-2"

# verify we are in transition
if aws s3 ls s3://teamfortytwo.deploy/ | grep "WEB-TRANSITION-IN-PROGRESS"
then

  # promote archives
  aws s3 mv s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz s3://teamfortytwo.deploy/erad_webservices.tar.gz
  aws s3 mv s3://teamfortytwo.deploy/staged_api_version s3://teamfortytwo.deploy/deployed_api_version

  # tear-down previous production stacks
  aws s3 cp s3://teamfortytwo.deploy/production_web_stacks ./
  cat production_web_stacks >> work.tmp
  while read stack_name; do
    for Region in $Regions; do
      aws --region $Region cloudformation delete-stack --stack-name $stack_name
    done
  done < ./work.tmp

  # clean up stage info files
  aws s3 rm s3://teamfortytwo.deploy/web_stage_stacks
  aws s3 rm s3://teamfortytwo.deploy/web_stage_ips
  aws s3 rm s3://teamfortytwo.deploy/production_web_stacks

  aws s3 rm s3://teamfortytwo.deploy/WEB-TRANSITION-IN-PROGRESS

  exit 0
else
  echo "We don't appear to be in transition."
  exit 1
fi
