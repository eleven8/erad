#!/bin/bash

# this staging job should no longer be used for deployments
# we are keeping it because it works well for integration testing
# use the individual jobs for deploying either API or RADIUS

export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
# AWS_SECRET_ACCESS_KEY injected by Jenkins
export AWS_DEFAULT_REGION=us-west-2

DEFAULT_SSH_KEY_NAME='shared_primary'
DEFAULT_SECURITY_GROUP_NAME='primary'
BASE_AMI_NAME="amzn2-ami-hvm-2.0.20200917.0-x86_64-gp2"

chmod +x src/erad_loadbalancer/pack.sh

# cleanup any previous run
rm -f production_stacks stage_stacks stage_ips

# do not continue if rollout is under way
if aws s3 ls s3://teamfortytwo.deploy/ | grep "TRANSITION-IN-PROGRESS"
then
  echo "Found TRANSITION-IN-PROGRESS"
  exit 1
fi

# check if a previous stage hasn't been torn down
if aws s3 ls s3://teamfortytwo.deploy/ | grep "stage"
then
  echo "**********************  PREVIOUS STAGE NOT TORN DOWN. CANNOT CONTINUE.  **********************"
  exit 1
fi

# determine what API build archive is requested
rm -f deployed_api_version staged_api_version version erad_webservices.stage.tar.gz
if [ "$ApiVersionNumber" == "Deployed" ]; then
  aws s3 cp s3://teamfortytwo.deploy/deployed_api_version .
  v=$(cat deployed_api_version)
  aws s3 cp s3://teamfortytwo.deploy/erad_webservices/erad_webservices_$v.tar.gz s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz
  echo "$v" > staged_api_version
elif [ "$ApiVersionNumber" == "Latest" ]; then
  aws s3 cp s3://teamfortytwo.deploy/erad_webservices.dev.tar.gz s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz
  aws s3 cp s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz .
  tar -zxvf erad_webservices.stage.tar.gz version
  mv version staged_api_version
else
  aws s3 cp s3://teamfortytwo.deploy/erad_webservices/erad_webservices_$ApiVersionNumber.tar.gz s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz
  echo "$ApiVersionNumber" > staged_api_version
fi

aws s3 cp staged_api_version s3://teamfortytwo.deploy/

# determine what RADIUS build archive is requested
rm -f deployed_radius_version staged_radius_version version erad_freeradius.stage.tar.gz
if [ "$RadiusVersionNumber" == "Deployed" ]; then
  aws s3 cp s3://teamfortytwo.deploy/deployed_radius_version .
  v=$(cat deployed_radius_version)
  aws s3 cp s3://teamfortytwo.deploy/erad_freeradius/erad_freeradius_$v.tar.gz s3://teamfortytwo.deploy/erad_freeradius.stage.tar.gz
  echo "$v" > staged_radius_version
elif [ "$RadiusVersionNumber" == "Latest" ]; then
  aws s3 cp s3://teamfortytwo.deploy/erad_freeradius.dev.tar.gz s3://teamfortytwo.deploy/erad_freeradius.stage.tar.gz
  aws s3 cp s3://teamfortytwo.deploy/erad_freeradius.stage.tar.gz .
  tar -zxvf erad_freeradius.stage.tar.gz version
  mv version staged_radius_version
else
  aws s3 cp s3://teamfortytwo.deploy/erad_freeradius/erad_freeradius_$RadiusVersionNumber.tar.gz s3://teamfortytwo.deploy/erad_freeradius.stage.tar.gz
  echo "$RadiusVersionNumber" > staged_radius_version
fi

aws s3 cp staged_radius_version s3://teamfortytwo.deploy/

### NEW VERSION - read number of required RADIUS servers - both auth and acct
payload="{\"SystemKey\": \"$SYSTEM_KEY\", \"Cluster_ID\": \"$CLUSTER_ID\"}"


read AuthNum AcctNum <<< $(curl -s -X POST --header 'Content-Type: application/json' --header 'Accept: application/json'  -d "$payload" 'https://api.enterpriseauth.com/erad/system/radius/load' | python3 -c "import sys, json; output=json.load(sys.stdin); print(output['Cluster']['AuthNum'], output['Cluster']['AcctNum'])")
read Region <<< $(curl -s -X POST --header 'Content-Type: application/json' --header 'Accept: application/json'  -d "$payload" 'https://api.enterpriseauth.com/erad/system/radius/region' | python3 -c "import sys, json; output=json.load(sys.stdin); print(output['Region'])")


# determine existing production servers
aws --region us-west-2 cloudformation list-stacks --max-items 10000 --stack-status-filter "CREATE_COMPLETE" | grep '"StackName": "erad-web-services-' | awk '{ print $2 }' | sed 's/,//;s/"//;s/"//' >> production_stacks
aws --region $Region cloudformation list-stacks --max-items 10000 --stack-status-filter "CREATE_COMPLETE" | grep "\"StackName\": \"erad-radius-$CLUSTER_ID-auth-" | awk '{ print $2 }' | sed 's/,//;s/"//;s/"//' >> production_stacks_radius_`echo $CLUSTER_ID`_auth
aws --region $Region cloudformation list-stacks --max-items 10000 --stack-status-filter "CREATE_COMPLETE" | grep "\"StackName\": \"erad-radius-$CLUSTER_ID-acct-" | awk '{ print $2 }' | sed 's/,//;s/"//;s/"//' >> production_stacks_radius_`echo $CLUSTER_ID`_acct

cat production_stacks_radius_`echo $CLUSTER_ID`_auth > production_stacks_radius
cat production_stacks_radius_`echo $CLUSTER_ID`_acct >> production_stacks_radius


# get available subnets from us-west-2
# to deploy only erad-web-server (ERAD API)
subnets_amount=$(aws ec2 describe-subnets --region us-west-2 --filters "Name=tag:Name,Values=vpc-erad-prod*" --query "Subnets[] | length(@)")

# exit if there are no productions subnets (availability zones)
if [ $subnets_amount -eq "0" ]; then
  echo $'\n\n\n!!! There are no productions subnets - exiting !!!\n\n\n'
  exit 1;
fi

TARGET_REGION='us-west-2'

subnets=$(aws ec2 describe-subnets \
  --region $TARGET_REGION \
  --filters "Name=tag:Name,Values=vpc-erad-prod*" \
  --query "Subnets[].SubnetId" \
  --output text\
)

AMIID=$(aws ec2 describe-images \
  --region $TARGET_REGION \
  --output text \
  --owners amazon \
  --filters "Name=name,Values=${BASE_AMI_NAME}" \
  --query 'Images[*].ImageId | [0]'\
)

VPCID=$(aws ec2 describe-vpcs \
  --region $TARGET_REGION \
  --filters "Name=isDefault,Values=true" \
  --query 'Vpcs[0].VpcId' \
  --output text \
)

# determine security group based on region
TARGET_REGION_SECURITY_GROUP=$(aws ec2 describe-security-groups \
  --region ${TARGET_REGION} \
  --query 'SecurityGroups[?GroupName==`'"$DEFAULT_SECURITY_GROUP_NAME"'`].GroupId|[0]' \
  --output text \
  )

OIFS=$IFS
IFS=$'\t'
subnets_list=( $subnets )
IFS=$OIFS

i=0

# launch as many new web servers as are currently in production
while read prod; do
  selected_subnet=${subnets_list[$(( i % subnets_amount ))]}

  stack_name=erad-web-services-$(date +%Y%m%d%H%M%S)
  aws --region $TARGET_REGION cloudformation create-stack \
    --capabilities CAPABILITY_NAMED_IAM \
    --stack-name $stack_name \
    --template-body file://./aws/erad-web-server.yaml \
    --parameters \
      ParameterKey=AMIID,ParameterValue=$AMIID \
      ParameterKey=VPCID,ParameterValue=$VPCID \
      ParameterKey=DeployFile,ParameterValue=erad_webservices.stage.tar.gz \
      ParameterKey=KeyName,ParameterValue=$DEFAULT_SSH_KEY_NAME \
      ParameterKey=SecurityGroupId,ParameterValue=${TARGET_REGION_SECURITY_GROUP} \
      ParameterKey=Passphrase,ParameterValue=$T42_PASSPHRASE \
      ParameterKey=Subnet,ParameterValue=$selected_subnet
  let i=i+1
  echo $stack_name >> stage_stacks
  sleep 1  # so stack names don't conflict
done < ./production_stacks

# push the list of web server stacks to S3
aws s3 cp stage_stacks s3://teamfortytwo.deploy/
aws s3 cp production_stacks s3://teamfortytwo.deploy/


# get available subnets from $Region
# to deploy only radius servers
subnets_amount=$(aws ec2 describe-subnets \
  --region $Region \
  --filters "Name=tag:Name,Values=vpc-erad-prod*" \
  --query "Subnets[] | length(@)" \
)

# exit if there are no productions subnets (availability zones)
if [ $subnets_amount -eq "0" ]; then
  echo $'\n\n\n!!! There are no productions subnets - exiting !!!\n\n\n'
  exit 1;
fi

subnets=$(aws ec2 describe-subnets \
  --region $Region \
  --filters "Name=tag:Name,Values=vpc-erad-prod*" \
  --query "Subnets[].SubnetId" \
  --output text
)

AMIID=$(aws ec2 describe-images \
  --region $Region \
  --output text \
  --owners amazon \
  --filters "Name=name,Values=${BASE_AMI_NAME}" \
  --query 'Images[*].ImageId | [0]'\
)

VPCID=$(aws ec2 describe-vpcs \
  --region $Region \
  --filters "Name=isDefault,Values=true" \
  --query 'Vpcs[0].VpcId' \
  --output text \
)

# determine security group based on region
TARGET_REGION_SECURITY_GROUP=$(aws ec2 describe-security-groups \
  --region ${TARGET_REGION} \
  --query 'SecurityGroups[?GroupName==`'"$DEFAULT_SECURITY_GROUP_NAME"'`].GroupId|[0]' \
  --output text \
  )


OIFS=$IFS
IFS=$'\t'
subnets_list=( $subnets )
IFS=$OIFS

# launch as many new radius servers as are currently in production
echo $AuthNum
echo $AcctNum
stackcounter=0
i=0
while [ $AuthNum -gt $stackcounter ]; do
  selected_subnet=${subnets_list[$(( i % subnets_amount ))]}

  stack_name=erad-radius-`echo $CLUSTER_ID`-auth-$(date +%Y%m%d%H%M%S)
  aws --region $Region cloudformation create-stack \
    --stack-name $stack_name \
    --template-body file://./aws/erad-radius-server.yaml \
    --parameters \
      ParameterKey=AMIID,ParameterValue=$AMIID \
      ParameterKey=VPCID,ParameterValue=$VPCID \
      ParameterKey=DeployFile,ParameterValue=erad_freeradius.stage.tar.gz \
      ParameterKey=KeyName,ParameterValue=$DEFAULT_SSH_KEY_NAME \
      ParameterKey=Passphrase,ParameterValue=$T42_PASSPHRASE \
      ParameterKey=ServerID,ParameterValue=$CLUSTER_ID \
      ParameterKey=SecurityGroupId,ParameterValue=$TARGET_REGION_SECURITY_GROUP \
      ParameterKey=Subnet,ParameterValue=$selected_subnet
  let i=i+1
  echo $stack_name >> stage_stacks_radius_auth
  sleep 1  # so stack names don't conflict
  let stackcounter=stackcounter+1
done

stackcounter=0
i=0
while [ $AcctNum -gt $stackcounter ]; do
  selected_subnet=${subnets_list[$(( i % subnets_amount ))]}

  stack_name=erad-radius-`echo $CLUSTER_ID`-acct-$(date +%Y%m%d%H%M%S)
  aws --region $Region cloudformation create-stack \
    --stack-name $stack_name \
    --template-body file://./aws/erad-radius-server.yaml \
    --parameters \
      ParameterKey=AMIID,ParameterValue=$AMIID \
      ParameterKey=VPCID,ParameterValue=$VPCID \
      ParameterKey=DeployFile,ParameterValue=erad_freeradius.stage.tar.gz \
      ParameterKey=KeyName,ParameterValue=$DEFAULT_SSH_KEY_NAME \
      ParameterKey=Passphrase,ParameterValue=$T42_PASSPHRASE \
      ParameterKey=ServerID,ParameterValue=$CLUSTER_ID \
      ParameterKey=SecurityGroupId,ParameterValue=$TARGET_REGION_SECURITY_GROUP \
      ParameterKey=Subnet,ParameterValue=$selected_subnet
  let i=i+1
  echo $stack_name >> stage_stacks_radius_acct
  sleep 1  # so stack names don't conflict
  let stackcounter=stackcounter+1
done

cat stage_stacks_radius_auth >> stage_stacks_radius
cat stage_stacks_radius_acct >> stage_stacks_radius

# push the list of radius server stacks to S3
aws s3 cp stage_stacks_radius s3://teamfortytwo.deploy/
aws s3 cp production_stacks_radius s3://teamfortytwo.deploy/

# add function to watch for server creation
watchStack() {
  if [ $# -eq 0 ]
  then
    echo "Usage: $1 Stack Name"
    echo "Output: [PRIVATE_IP] [PUBLIC_IP] [INSTANCE_ID]"
    exit 1
  fi
  stage_stack=$1
  Region=$2

  aws cloudformation wait stack-create-complete \
    --region $Region \
    --stack-name $stage_stack

  echo $(aws cloudformation describe-stacks \
    --region $Region \
    --stack-name $stage_stack \
    --output text \
    --query  \
    'Stacks[0].[
      Outputs[?OutputKey==`PrivateIp`].OutputValue,
      Outputs[?OutputKey==`PublicIp`].OutputValue,
      Outputs[?OutputKey==`InstanceId`].OutputValue
    ] []')
}

# for each API server, wait for creation and then retrieve IP addresses
newApiIds=""
while read stage_stack; do
  read private_ip public_ip instance_id <<<$(watchStack $stage_stack us-west-2)
  echo $public_ip >> stage_ips
  newApiIds="$newApiIds $instance_id"
done < ./stage_stacks

# push the list of API server IPs to S3
aws s3 cp stage_ips s3://teamfortytwo.deploy/

# for each RADIUS server, wait for creation and then retrieve IP addresses
while read stage_stack; do
  read private_ip public_ip instance_id <<<$(watchStack $stage_stack $Region)
  echo $public_ip >> stage_ips_radius
  echo \"$instance_id\": \"$public_ip\" >> stage_ips_radius_auth
done < ./stage_stacks_radius_auth
while read stage_stack; do
  read private_ip public_ip instance_id <<<$(watchStack $stage_stack $Region)
  echo $public_ip >> stage_ips_radius
  echo \"$instance_id\": \"$public_ip\" >> stage_ips_radius_acct
done < ./stage_stacks_radius_acct


# push the list of RADIUS server IPs to S3
aws s3 cp stage_ips_radius s3://teamfortytwo.deploy/

# pack the load balancer
. ./src/erad_loadbalancer/pack.sh

# copy down stack lists
aws s3 cp s3://teamfortytwo.deploy/stage_stacks ./
aws s3 cp s3://teamfortytwo.deploy/production_stacks ./


# add function to wait for response on HTTP port
watchForResponse() {
  if [ $# -ne 2 ]
  then
    echo "Usage: $1 IP Address"
    echo "       $2 Traffic Type (either 'HTTP' or 'RADIUS')"
    exit 1
  fi
  ip=$1
  type=$2

  set +e
  for i in {1..120};
  do
    if [ "$type" == "HTTP" ]; then
      nc -z $ip 80
    else
      echo "radius"
      echo "User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -D /usr/local/share/freeradius/ -t 5 -x -r 1  "$ip:1814" auth V72QSZ2CE3
    fi
    if [ $? -eq 0 ]
    then
      break
    fi
    sleep 5
  done
  set -e
  if [ "$i" -gt "1" ]; then # if the server was already up, it probably doesn't need this sleep
    sleep 5 # let the server finish spinning up
  fi
}

# integration test the individual API servers
while read public_ip; do
  watchForResponse $public_ip "HTTP"
  python3 ./deploy/test_erad_system.py --SystemKey $SYSTEM_KEY --IntegrationKey $INTEGRATION_KEY --BaseUrl http://$public_ip system_test
done < ./stage_ips

# test each RADIUS server
while read public_ip; do
  watchForResponse $public_ip "RADIUS"

  RESULT1=$(echo "User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 5 -D /usr/local/share/freeradius/ $public_ip:1814 auth V72QSZ2CE3 | grep "Received Access-Accept")
  if [ -z "$RESULT1" ]
  then
    echo "Failed Access-Accept test on $public_ip"
    exit 1
  fi

  RESULT2=$(echo "User-Name=babble,User-Password=9001,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 5 -D /usr/local/share/freeradius/ $public_ip:1814 auth V72QSZ2CE3 | grep "Received Access-Reject")
  if [ -z "$RESULT2" ]
  then
    echo "Failed Access-Reject test on $public_ip"
    exit 1
  fi

  RESULT3=$(echo "User-Name=SR-980-83/erad-monitor,User-Password=nt5ewa9s,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 35 -x -r 1 -D /usr/local/share/freeradius/ $public_ip auth 4905DVCA3B | grep "Received Access-Accept")
  if [ -z "$RESULT3" ]
  then
   echo "Failed Proxy test on $public_ip"
   exit 1
  fi

  #RESULT4=$(echo "User-Name=babble20006,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 1 -D /usr/local/share/freeradius/ $public_ip:20006 auth GAPWROCQSZ | grep "Received Access-Accept")
  #if [ -z "$RESULT4" ]
  #then
  #  echo "Failed Access-Accept test on $public_ip for port 20006"
  #  exit 1
  #fi

  #RESULT5=$(echo "User-Name=babble20006,User-Password=9001,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 1 -D /usr/local/share/freeradius/ $public_ip:20006 auth GAPWROCQSZ | grep "Received Access-Reject")
  #if [ -z "$RESULT5" ]
  #then
  #  echo "Failed Access-Reject test on $public_ip for port 20006"
  #  exit 1
  #fi
done < ./stage_ips_radius


## NEW - save RADIUS servers to Erad_Cluster.Instances
auth='"auth": {'
while read server; do
    auth="$auth $server,"
done < ./stage_ips_radius_auth
auth=${auth%$'\n'}
auth=${auth::${#auth}-1}
auth="$auth}"

acct='"acct": {'
while read server; do
    acct="$acct $server,"
done < ./stage_ips_radius_acct
acct=${acct%$'\n'}
acct=${acct::${#acct}-1}
acct="$acct}"

authacct="{$auth, $acct}"
payload="{\"SystemKey\": \"$SYSTEM_KEY\", \"Servers\": {\"$CLUSTER_ID\": $authacct}}"

# save Instances
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json'  -d "$payload" 'https://api.enterpriseauth.com/erad/system/radius/save' --fail

# find existing load balancer and add to production stack list
aws --region $Region cloudformation list-stacks --max-items 10000 --stack-status-filter "CREATE_COMPLETE" | grep '"StackName": "erad-load-balancer-' | awk '{ print $2 }' | sed 's/,//;s/"//;s/"//' >> production_stacks
aws s3 cp ./production_stacks s3://teamfortytwo.deploy/

i=0
# launch the primary load balancer
# select first_available subnet
selected_subnet=${subnets_list[$(( i % subnets_amount ))]}

stack_name_a=erad-load-balancer-a-$(date +%Y%m%d%H%M%S)
aws --region $Region cloudformation create-stack \
  --stack-name $stack_name_a \
  --capabilities CAPABILITY_NAMED_IAM \
  --template-body file://./aws/erad-load-balancer.yaml \
  --parameters \
    ParameterKey=AMIID,ParameterValue=$AMIID \
    ParameterKey=VPCID,ParameterValue=$VPCID \
    ParameterKey=DeployFile,ParameterValue=erad-load-balancer.stage.tar.gz \
    ParameterKey=KeyName,ParameterValue=$DEFAULT_SSH_KEY_NAME \
    ParameterKey=Passphrase,ParameterValue=$T42_PASSPHRASE \
    ParameterKey=ClusterId,ParameterValue=$CLUSTER_ID \
    ParameterKey=SecurityGroupId,ParameterValue=$TARGET_REGION_SECURITY_GROUP \
    ParameterKey=Subnet,ParameterValue=$selected_subnet
echo $stack_name_a >> stage_stacks
aws s3 cp ./stage_stacks s3://teamfortytwo.deploy/

# wait for the primary load balancer to be created
aws --region $Region cloudformation wait stack-create-complete --stack-name "$stack_name_a"

read private_ip_a public_ip_a instance_id_a <<<$(watchStack $stack_name_a $Region)
[ -n "$private_ip_a" ] || {
	echo "Failed to launch primary load balancer"
	exit 1
}

# test RADIUS on the primary load balancer
watchForResponse $public_ip_a "RADIUS"
RESULT1=$(echo "User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 5 -D /usr/local/share/freeradius/ $public_ip_a:1814 auth V72QSZ2CE3 | grep "Received Access-Accept")
if [ -z "$RESULT1" ]
then
	echo "Failed Access-Accept test on $public_ip"
	exit 1
fi


# select next available subnet
let i=i+1
selected_subnet=${subnets_list[$(( i % subnets_amount ))]}
# launch the standby load balancer
stack_name_b=erad-load-balancer-b-$(date +%Y%m%d%H%M%S)
aws --region $Region cloudformation create-stack \
  --stack-name $stack_name_b \
  --capabilities CAPABILITY_NAMED_IAM \
  --template-body file://./aws/erad-load-balancer.yaml \
  --parameters \
    ParameterKey=AMIID,ParameterValue=$AMIID \
    ParameterKey=VPCID,ParameterValue=$VPCID \
    ParameterKey=DeployFile,ParameterValue=erad-load-balancer.stage.tar.gz \
    ParameterKey=KeyName,ParameterValue=$DEFAULT_SSH_KEY_NAME \
    ParameterKey=Passphrase,ParameterValue=$T42_PASSPHRASE \
    ParameterKey=PrimaryIp,ParameterValue=$private_ip_a \
    ParameterKey=ClusterId,ParameterValue=$CLUSTER_ID \
    ParameterKey=SecurityGroupId,ParameterValue=$TARGET_REGION_SECURITY_GROUP \
    ParameterKey=Subnet,ParameterValue=$selected_subnet
echo $stack_name_b >> stage_stacks
aws s3 cp ./stage_stacks s3://teamfortytwo.deploy/

# wait for the standby load balancer to be created
aws --region $Region cloudformation wait stack-create-complete --stack-name "$stack_name_b"
read private_ip_b public_ip_b instance_id_b <<<$(watchStack $stack_name_b $Region)
[ -n "$private_ip_b" ] || {
	echo "Failed to launch standby load balancer"
	exit 1
}

# test RADIUS on the standby load balancer
watchForResponse $public_ip_b "RADIUS"
RESULT1=$(echo "User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 5 -D /usr/local/share/freeradius/ $public_ip_b:1814 auth V72QSZ2CE3 | grep "Received Access-Accept")
if [ -z "$RESULT1" ]
then
	echo "Failed Access-Accept test on $public_ip"
	exit 1
fi
