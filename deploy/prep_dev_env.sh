#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

# install system prerequisites
pip3 install --upgrade pip python-dateutil

pip3 install \
	'awscli<1.16.264' \
	'botocore<1.13.0' \
	'pynamodb<4.0' \
	'pyyaml>=3.10,<=5.1' \
	boto \
	'celery<5.0' \
	coverage \
	Flask \
	flask-caching \
	flask-compress \
	mako \
	mock \
	nose \
	pycrypto \
	pytz \
	redis \
	requests \
	validate_email \
	'boto3==1.9.253'



# Installing fork of signxml with changes that fix this issue https://github.com/XML-Security/signxml/issues/43
pip3 install https://bitbucket.org/teamfortytwo/signxml/get/master.zip

# configure system prerequisites
export BOTO_CONFIG=./boto_local.cfg
echo "[Credentials]" >> ./boto_local.cfg
echo "aws_access_key_id = foo" >> ./boto_local.cfg
echo "aws_secret_access_key = bar" >> ./boto_local.cfg
echo "[DynamoDB]" >> ./boto_local.cfg
echo "region = us-west-2" >> ./boto_local.cfg
echo "host = https://dynamodb.us-west-2.amazonaws.com" >> ./boto_local.cfg
echo "validate_checksums = True" >> ./boto_local.cfg

# copy files and dependencies
export AWS_ACCESS_KEY_ID=AKIAJTK5BJ3KH2DRVTUQ
export AWS_SECRET_ACCESS_KEY=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
export AWS_DEFAULT_REGION=us-west-2

# download and install dynamodb
mkdir dynamodb
wget https://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.zip
unzip dynamodb_local_latest.zip -d dynamodb
