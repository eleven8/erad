#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

# assumes we have a global install of node, bower, grunt-cli

export PATH=./node_modules/.bin:$PATH:/usr/local/bin

cd src/erad_static/
# replace {%ERAD_SERVER_NAME%} with the production server api address
sed -i 's/{%ERAD_SERVER_NAME%}/https:\/\/api.enterpriseauth.com/g' app/scripts/services.js
npm install
bower install

node -v
npm -v
bower -v
npm run deploy -- production
