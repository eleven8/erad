#!/bin/bash
set -e
set -x


parse_global_config() {
    for file in "/etc/boto.cfg" "/etc/erad.cfg"; do
      echo ${file}
      if [[ -f ${file} ]]; then
        sed -i \
            -e "s:\##AWS_ACCESS_KEY_ID##:$AWS_ACCESS_KEY_ID:g" \
            -e "s:\##AWS_SECRET_ACCESS_KEY##:$AWS_SECRET_ACCESS_KEY:g" \
            -e "s:\##AWS_DEFAULT_REGION##:$AWS_DEFAULT_REGION:g" \
            ${file}
      fi
    done
}


export ERAD_AIO_INSTALL=true

export AWS_ACCESS_KEY_ID=AKIAJTK5BJ3KH2DRVTUQ   # bootstrap
export AWS_SECRET_ACCESS_KEY=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
export AWS_DEFAULT_REGION=us-west-2

[ -z "$T42_API_DEPLOY_FILE" ] && export T42_API_DEPLOY_FILE=erad_webservices.dev.tar.gz
[ -z "$T42_RADIUS_DEPLOY_FILE" ] && export T42_RADIUS_DEPLOY_FILE=erad_freeradius.dev.tar.gz
[ -z "$T42_SERVER_ID" ] && export T42_SERVER_ID=uswest-dev-01
[ -z "$DEPLOY_BUCKET" ] && export DEPLOY_BUCKET=teamfortytwo.deploy

if [ "$1" == "local" ]; then
    # Local pack before deploy
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    sh -xe $DIR/../src/erad/api/pack.sh || echo with warnings
    cp $DIR/../src/erad/api/erad_webservices_DEV.tar.gz /usr/local/src/$T42_API_DEPLOY_FILE
    sh -xe $DIR/../src/erad/frmod/pack.sh || echo with warnings
    cp $DIR/../src/erad/frmod/erad_freeradius_DEV.tar.gz /usr/local/src/$T42_RADIUS_DEPLOY_FILE
fi

# install the API
pushd /usr/local/src
if [ -f "$T42_API_DEPLOY_FILE" ]; then
    # If you need to test deploy without packed file on DEPLOY_BUCKET
    # put TAR into /usr/local/src/erad_webservices.dev.tar.gz
    # it will be moved, so next check will not pass
    # and file will fetched from s3
    mv $T42_API_DEPLOY_FILE api.tar.gz
else
    aws s3 cp s3://$DEPLOY_BUCKET/$T42_API_DEPLOY_FILE api.tar.gz
fi
tar -xzvf api.tar.gz
sh ./erad/api/deploy.sh

# install FreeRADIUS
aws s3 cp s3://$DEPLOY_BUCKET/freeradius_server.tar.gz .
tar -xzvf /usr/local/src/freeradius_server.tar.gz -C /

# install our custom RADIUS module
if [ -f "$T42_RADIUS_DEPLOY_FILE" ]; then
    mv $T42_RADIUS_DEPLOY_FILE radius.tar.gz
else
    aws s3 cp s3://$DEPLOY_BUCKET/$T42_RADIUS_DEPLOY_FILE ./radius.tar.gz
fi
tar -xzvf radius.tar.gz
sh ./erad/frmod/deploy.sh
popd


amazon-linux-extras install epel -y

# Merge frmod/erad.cfg.dev and api/erad.cfg.dev
yum install --enablerepo=epel jq -y -q
PROXY_CONF=$(cat /usr/local/src/erad/frmod/erad.cfg.dev | jq '.proxy_conf')
jq ".proxy_conf |= ${PROXY_CONF}" /usr/local/src/erad/api/erad.cfg.dev > /etc/erad.cfg
parse_global_config

# install static
#!/bin/bash
yum install --enablerepo=epel npm nodejs git -y
which grunt || npm install -g grunt-cli
which bower || npm install -g bower


# copy archive with files necessary to deploy UI (erad_static)
pushd /usr/local/src


# Inside cfn-init script we have instructions to copy archive with erad_static files and extract them.
# See cloudfomration template all-in-one-stack.yaml
pub_ip=$(dig +short myip.opendns.com @resolver1.opendns.com)
sed -i "s/52.26.34.201/$pub_ip/g" deploy/erad_static_deploy_development.sh
sed -i "s/52.26.34.201/$pub_ip/g" src/tools/dashboard/UI/config.json
sed -i "s/dev.enterpriseauth.com/$pub_ip/g" src/tools/dashboard/UI/config.json

# disable error checking
# because `erad_static_deploy_development.sh` trying to upload files to dev.enterpriseauth.com bucket
# but missing credentials
set +e
echo n|sudo -u ec2-user bash -c 'source deploy/erad_static_deploy_development.sh'
set -e

rm -rf /var/www/html/*
cp -r src/erad_static/dist/* /var/www/html/
cp -r src/tools/dashboard/UI/ /var/www/html/start
pushd /etc/httpd/conf.d/
mv erad.conf erad.conf_backup


chown root:root /usr/local/webservices/erad/api/statuscheck.conf
mv /usr/local/webservices/erad/api/statuscheck.conf /etc/httpd/conf.d/

popd

systemctl reload httpd
