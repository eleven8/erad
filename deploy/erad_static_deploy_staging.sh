#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

# assumes we have a global install of node, aws-cli

pwd
ls
cd ./src/new_erad_static/
node -v
npm -v
npm install

set -e

cat << EOF > .env.production
ERAD_API_PATH="https://api.enterpriseauth.com"
EOF

# Please modify values below accordingly
target_bucket_id="erad.gold11.us"
target_distribution_id="ELJ6DTN3NX6T4"

# build files
rm -rf build && npm run build

# copy to target bucket
aws s3 sync build s3://${target_bucket_id} --acl public-read --delete

# invalidate CloudFront cache
aws cloudfront create-invalidation --distribution-id "$target_distribution_id" --paths '/*'
