#!/bin/bash
set -x
set -e

export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
# AWS_SECRET_ACCESS_KEY injected by Jenkins
export AWS_DEFAULT_REGION=us-west-2
export DEPLOY_S3_BUCKET="teamfortytwo.deploy"
export API_URL="https://api.enterpriseauth.com"
export KEYNAME="t42-production"
export JENKINS_HOST="ci.gd1.io"


export DEFAULT_SSH_KEY_NAME='shared_primary'
export DEFAULT_SECURITY_GROUP_NAME='primary'
export BASE_AMI_NAME="amzn2-ami-hvm-2.0.20200917.0-x86_64-gp2"


declare -A ELASTIC_IPS
ELASTIC_IPS['us-west-2']='eipalloc-755efb10'
ELASTIC_IPS['us-east-1']='eipalloc-f5d277cb'
ELASTIC_IPS['us-east-2']='eipalloc-039272108cf6a1a09'

# this array variables will store new load balancers, and will be used in the final steps
# Usually only `LOAD_BALANCERS_A` variable will be used, `LOAD_BALANCERS_B` just to store value and is not used by this script
declare -A LOAD_BALANCERS_A
declare -A LOAD_BALANCERS_B

# Array will store last deployed cluster id in region
declare -A CHANGED_REGIONS


# $CLUSTER_ID variable should be a string with cluster ids separated only by commas, or one cluster id value
# Examples: 'a,b,c', 'a'

IFS=$',' read -r -a clusters_list <<< "$CLUSTER_ID"


chmod +x src/erad_loadbalancer/pack.sh

# cleanup any previous run
rm -f frmod_production_stacks frmod_stage_stacks frmod_stage_ips

# do not continue if rollout is under way
if aws s3 ls s3://$DEPLOY_S3_BUCKET/ | grep "FRMOD-TRANSITION-IN-PROGRESS"
then
  echo "Found FRMOD-TRANSITION-IN-PROGRESS"
  exit 1
fi

# check if a previous stage hasn't been torn down
if aws s3 ls s3://$DEPLOY_S3_BUCKET/ | grep "stage"
then
  echo "**********************  PREVIOUS STAGE NOT TORN DOWN. CANNOT CONTINUE.  **********************"
  exit 1
fi

# add function to watch for server creation
watchStack() {
  if [ $# -eq 0 ]
  then
    echo "Usage: $1 Stack Name"
    echo "Output: [PRIVATE_IP] [PUBLIC_IP] [INSTANCE_ID]"
    exit 1
  fi
  stage_stack=$1
  Region=$2

  aws cloudformation wait stack-create-complete \
    --region $Region \
    --stack-name $stage_stack

  echo $(aws cloudformation describe-stacks \
    --region $Region \
    --stack-name $stage_stack \
    --output text \
    --query  \
    'Stacks[0].[
      Outputs[?OutputKey==`PrivateIp`].OutputValue,
      Outputs[?OutputKey==`PublicIp`].OutputValue,
      Outputs[?OutputKey==`InstanceId`].OutputValue
    ] []')
}

# add function to wait for response on HTTP port
watchForResponse() {
  if [ $# -ne 2 ]
  then
    echo "Usage: $1 IP Address"
    echo "       $2 Traffic Type (either 'HTTP' or 'RADIUS')"
    exit 1
  fi
  ip=$1
  type=$2

  set +e
  for i in {1..120};
  do
    if [ "$type" == "HTTP" ]; then
      nc -z $ip 80
    else
      echo "radius"
      echo "User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -D /usr/local/share/freeradius/ -t 5 -x -r 1  "$ip:1814" auth V72QSZ2CE3
    fi
    if [ $? -eq 0 ]
    then
      break
    fi
    sleep 5
  done
  set -e
  if [ "$i" -gt "1" ]; then # if the server was already up, it probably doesn't need this sleep
    sleep 5 # let the server finish spinning up
  fi
}


getSubnetsAmount() {
  Region=$1
  # get available subnets from $Region
  # to deploy only radius servers
  subnets_amount=$(aws ec2 describe-subnets \
    --region $Region \
    --filters "Name=tag:Name,Values=vpc-erad-prod*" \
    --query "Subnets[] | length(@)")

  # exit if there are no productions subnets (availability zones)
  if [ $subnets_amount -eq "0" ]; then
    echo $'\n\n\n!!! There are no productions subnets - exiting !!!\n\n\n'
    exit 1;
  fi

  echo $subnets_amount
}

getSubnets() {
  Region=$1
  subnets=$(aws ec2 describe-subnets \
    --region $Region \
    --filters "Name=tag:Name,Values=vpc-erad-prod*" \
    --query "Subnets[].SubnetId" \
    --output text)

  echo $subnets
}

# This function expect that all subnets with tag Name ~ `vpc-erad-prod*` belongs to one VPC
getVPCID() {
  Region=$1
  vpcid=$(aws ec2 describe-subnets \
    --region $Region \
    --filters "Name=tag:Name,Values=vpc-erad-prod*" \
    --query "Subnets[0].VpcId" \
    --output text)

  echo $vpcid
}

# determine what RADIUS build archive is requested
rm -f deployed_radius_version staged_radius_version version erad_freeradius.stage.tar.gz
if [ "$RadiusVersionNumber" == "Deployed" ]; then
  aws s3 cp s3://$DEPLOY_S3_BUCKET/deployed_radius_version .
  v=$(cat deployed_radius_version)
  aws s3 cp s3://$DEPLOY_S3_BUCKET/erad_freeradius/erad_freeradius_$v.tar.gz s3://$DEPLOY_S3_BUCKET/erad_freeradius.stage.tar.gz
  echo "$v" > staged_radius_version
elif [ "$RadiusVersionNumber" == "Latest" ]; then
  aws s3 cp s3://$DEPLOY_S3_BUCKET/erad_freeradius.dev.tar.gz s3://$DEPLOY_S3_BUCKET/erad_freeradius.stage.tar.gz
  aws s3 cp s3://$DEPLOY_S3_BUCKET/erad_freeradius.stage.tar.gz .
  tar -zxvf erad_freeradius.stage.tar.gz version
  mv version staged_radius_version
else
  aws s3 cp s3://$DEPLOY_S3_BUCKET/erad_freeradius/erad_freeradius_$RadiusVersionNumber.tar.gz s3://$DEPLOY_S3_BUCKET/erad_freeradius.stage.tar.gz
  echo "$RadiusVersionNumber" > staged_radius_version
fi

aws s3 cp staged_radius_version s3://$DEPLOY_S3_BUCKET/

instances='' # variable to store instances information

> frmod_production_stacks_radius
> frmod_stage_stacks_radius

for current_cluster_id in ${clusters_list[@]}; do
  > frmod_stage_stacks_radius_${current_cluster_id}_auth
  > frmod_stage_stacks_radius_${current_cluster_id}_acct
  > frmod_stage_ips_radius_${current_cluster_id}_acct
  > frmod_stage_ips_radius_${current_cluster_id}_auth

  ### NEW VERSION - read number of required RADIUS servers - both auth and acct
  payload="{\"SystemKey\": \"$SYSTEM_KEY\", \"Cluster_ID\": \"$current_cluster_id\"}"

  declare -A instance_amount
  read instance_amount['auth'] instance_amount['acct'] <<< $(curl -s -X POST --header 'Content-Type: application/json' --header 'Accept: application/json'  -d "$payload" "${API_URL}/erad/system/radius/load" | python3 -c "import sys, json; output=json.load(sys.stdin); print(output['Cluster']['AuthNum'], output['Cluster']['AcctNum'])")
  read Region <<< $(curl -s -X POST --header 'Content-Type: application/json' --header 'Accept: application/json'  -d "$payload" "${API_URL}/erad/system/radius/region" | python3 -c "import sys, json; output=json.load(sys.stdin); print(output['Region'])")


  AMIID=$(aws ec2 describe-images \
    --region $Region \
    --output text \
    --owners amazon \
    --filters "Name=name,Values=${BASE_AMI_NAME}" \
    --query 'Images[*].ImageId | [0]'\
  )

  VPCID=$(getVPCID $Region)

  # determine security group based on region
  TARGET_REGION_SECURITY_GROUP=$(aws ec2 describe-security-groups \
    --region ${Region} \
    --query 'SecurityGroups[?GroupName==`'"$DEFAULT_SECURITY_GROUP_NAME"'`].GroupId|[0]' \
    --output text \
  )

# This arrays is storing last cluster id that was deployed in region.
# Cluster id will be used by load_ports.py script on load balancer server, and value/id of only cluster will be enough because of these:
# API `/erad/system/server/config/load` when asked about load balancer do next. 1. Query one (any) endpoint that belongs to cluster id.
# Endpoint storing load balancer ip + port. All instances that served by one load balancer will have same ip address,
# but usually will have diferent ports. 2. Using ip query all endpoints that using given ip address. From it we can get list of all cluster ids
# that server by loadbalancer, and should be reassigned to new load balancer. 3. Using queried list of cluster ids extract all instances and
# theirs ip addresses. List of ip address will be used load_ports.py to construct arguments for pen loadbalancer.
#
# This is enought for pen but not enough for freeradius ad loadbalancer, because we need to pass radius shared secrets values. And current API
# will return shared secrets for ports that belongs to only one specific given cluster id.
  CHANGED_REGIONS[$Region]=${current_cluster_id}

  # determine existing production servers
  for instance_type in 'auth' 'acct'; do
    stack_name="erad-radius-${current_cluster_id}-${instance_type}-"

    aws cloudformation list-stacks \
      --region $Region \
      --stack-status-filter "CREATE_COMPLETE" \
      --max-items 10000 \
      --output text \
      --query 'StackSummaries[?contains(StackName,`'$stack_name'`)].StackName' \
      | sed s'/\t/\n/g' \
      >>  "frmod_production_stacks_radius"

  done

  # get available subnets from $Region
  # to deploy only radius servers
  subnets_amount=$(getSubnetsAmount $Region)
  subnets=$(getSubnets $Region)
  read -r -a subnets_list <<< "$subnets"


  # launch the new radius servers
  echo ${instance_amount[auth]}
  echo ${instance_amount[acct]}

  for instance_type in 'auth' 'acct'; do

    stackcounter=0
    i=0
    while [[ ${instance_amount[$instance_type]} -gt $stackcounter ]]; do
      selected_subnet=${subnets_list[$(( i % subnets_amount ))]}
      stack_name="erad-radius-${current_cluster_id}-${instance_type}-$(date +%Y%m%d%H%M%S)"

      aws --region $Region cloudformation create-stack \
        --stack-name $stack_name \
        --template-body file://./aws/erad-radius-server.yaml \
        --parameters \
          ParameterKey=AMIID,ParameterValue=$AMIID \
          ParameterKey=VPCID,ParameterValue=$VPCID \
          ParameterKey=DeployFile,ParameterValue=erad_freeradius.stage.tar.gz \
          ParameterKey=KeyName,ParameterValue=$DEFAULT_SSH_KEY_NAME \
          ParameterKey=Passphrase,ParameterValue=$T42_PASSPHRASE \
          ParameterKey=ServerID,ParameterValue=$current_cluster_id \
          ParameterKey=SecurityGroupId,ParameterValue=$TARGET_REGION_SECURITY_GROUP \
          ParameterKey=Subnet,ParameterValue=$selected_subnet

      let i=i+1
      echo $stack_name >> "frmod_stage_stacks_radius_${current_cluster_id}_${instance_type}" # used to retrieve instances ip address
      echo $stack_name >> "frmod_stage_stacks_radius"  # used in finish_frmod_transition.sh
      sleep 1  # so stack names don't conflict
      let stackcounter=stackcounter+1
    done

  done

  # push the list of radius server stacks to S3
  aws s3 cp "frmod_stage_stacks_radius" s3://$DEPLOY_S3_BUCKET/
  aws s3 cp "frmod_production_stacks_radius" s3://$DEPLOY_S3_BUCKET/

done # next cluster_id

for current_cluster_id in ${clusters_list[@]}; do

  # get the region for the cluster
  payload="{\"SystemKey\": \"$SYSTEM_KEY\", \"Cluster_ID\": \"$current_cluster_id\"}"
  read Region <<< $(curl -s -X POST --header 'Content-Type: application/json' --header 'Accept: application/json'  -d "$payload" "${API_URL}/erad/system/radius/region" | python3 -c "import sys, json; output=json.load(sys.stdin); print(output['Region'])")

  # for each RADIUS server, wait for creation and then retrieve IP addresses
  for instance_type in 'auth' 'acct'; do

    while read stage_stack; do
      read private_ip public_ip instance_id <<< $(watchStack $stage_stack $Region)
      echo \"$instance_id\": \"$public_ip\" >> "frmod_stage_ips_radius_${current_cluster_id}_${instance_type}"

      # test the RADIUS server
      watchForResponse $public_ip "RADIUS"

      RESULT1=$(echo "User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 5 -D /usr/local/share/freeradius/ $public_ip:1814 auth V72QSZ2CE3 | grep "Received Access-Accept")
      if [ -z "$RESULT1" ]
      then
        echo "Failed Access-Accept test on $public_ip"
        exit 1
      fi

      RESULT2=$(echo "User-Name=babble,User-Password=9001,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 5 -D /usr/local/share/freeradius/ $public_ip:1814 auth V72QSZ2CE3 | grep "Received Access-Reject")
      if [ -z "$RESULT2" ]
      then
        echo "Failed Access-Reject test on $public_ip"
        exit 1
      fi

      #RESULT3=$(echo "User-Name=SR-980-83/erad-monitor,User-Password=nt5ewa9s,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 35 -x -r 1 -D /usr/local/share/freeradius/ $public_ip auth V72QSZ2CE3 | grep "Received Access-Accept")
      #if [ -z "$RESULT3" ]
      #then
      #  echo "Failed Proxy test on $public_ip"
      #  exit 1
      #fi

      # RESULT4=$(echo "User-Name=babble20006,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 1 -D /usr/local/share/freeradius/ $public_ip:20006 auth GAPWROCQSZ | grep "Received Access-Accept")
      # if [ -z "$RESULT4" ]
      # then
      #   echo "Failed Access-Accept test on $public_ip for port 20006"
      #   exit 1
      # fi

      # RESULT5=$(echo "User-Name=babble20006,User-Password=9001,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 1 -D /usr/local/share/freeradius/ $public_ip:20006 auth GAPWROCQSZ | grep "Received Access-Reject")
      # if [ -z "$RESULT5" ]
      # then
      #   echo "Failed Access-Reject test on $public_ip for port 20006"
      #   exit 1
      # fi

    done < "./frmod_stage_stacks_radius_${current_cluster_id}_${instance_type}"
  done

  str_buffer=''
  for instance_type in 'auth' 'acct'; do
    str_buffer="${str_buffer} \"$instance_type\": {"

    while read server; do
        str_buffer="${str_buffer} ${server},"
    done < "./frmod_stage_ips_radius_${current_cluster_id}_${instance_type}"

    str_buffer=${str_buffer%$'\n'}
    str_buffer=${str_buffer::${#str_buffer}-1} # removing comma after last instance record to get valid json
    str_buffer="$str_buffer},"
  done
  str_buffer=${str_buffer%$'\n'}
  str_buffer=${str_buffer::${#str_buffer}-1} # removing comma after acct section to get valid json
  authacct="{${str_buffer}}"

  instances="${instances} \"${current_cluster_id}\" : ${authacct},"

done


instances=${instances::${#instances}-1} # removing the last comma, to get valid json
payload="{\"SystemKey\": \"$SYSTEM_KEY\", \"Servers\": { ${instances} }}"

# save Instances
curl -X POST \
  "${API_URL}/erad/system/radius/save" \
  --header 'Content-Type: application/json' \
  --header 'Accept: application/json' \
  -d "$payload" \
  --fail

# find existing load balancer and add to production stack list
for current_region in ${!CHANGED_REGIONS[@]}; do
   aws cloudformation list-stacks \
    --region $current_region \
    --stack-status-filter "CREATE_COMPLETE" \
    --max-items 10000 \
    --output text \
    --query 'StackSummaries[?contains(StackName,`erad-load-balancer-`)].StackName' \
    | sed s'/\t/\n/g' \
    >>  "frmod_production_stacks"
done
aws s3 cp "./frmod_production_stacks" s3://$DEPLOY_S3_BUCKET/


# Deploy new load balancers.

# pack the load balancer
source ./src/erad_loadbalancer/pack.sh

# Because clusters sharing load balancer, it will be enough to do redeploy one in every $CHANGED_REGIONS
for current_region in ${!CHANGED_REGIONS[@]}; do

  AMIID=$(aws ec2 describe-images \
    --region $current_region \
    --output text \
    --owners amazon \
    --filters "Name=name,Values=${BASE_AMI_NAME}" \
    --query 'Images[*].ImageId | [0]'\
  )

  VPCID=$(aws ec2 describe-vpcs \
    --region $current_region \
    --filters "Name=isDefault,Values=true" \
    --query 'Vpcs[0].VpcId' \
    --output text \
  )
  if [ "$Region" == "us-east-1" ]; then
    VPCID='vpc-0982816f'
  fi

  # determine security group based on region
  TARGET_REGION_SECURITY_GROUP=$(aws ec2 describe-security-groups \
    --region ${current_region} \
    --query 'SecurityGroups[?GroupName==`'"$DEFAULT_SECURITY_GROUP_NAME"'`].GroupId|[0]' \
    --output text \
  )



  # get available subnets from $Region
  # to deploy only radius servers
  subnets_amount=$(getSubnetsAmount $current_region)
  subnets=$(getSubnets $current_region)
  read -r -a subnets_list <<< "$subnets"

  current_cluster_id=${CHANGED_REGIONS[$current_region]}

  i=0
  # launch the primary load balancer
  # select first_available subnet
  selected_subnet=${subnets_list[$(( i % subnets_amount ))]}

  stack_name_a=erad-load-balancer-a-$(date +%Y%m%d%H%M%S)
  aws --region $current_region cloudformation create-stack \
    --stack-name $stack_name_a \
    --capabilities CAPABILITY_NAMED_IAM \
    --template-body file://./aws/erad-load-balancer.yaml \
    --parameters \
      ParameterKey=AMIID,ParameterValue=$AMIID \
      ParameterKey=VPCID,ParameterValue=$VPCID \
      ParameterKey=DeployFile,ParameterValue=erad-load-balancer.stage.tar.gz \
      ParameterKey=KeyName,ParameterValue=$DEFAULT_SSH_KEY_NAME \
      ParameterKey=Passphrase,ParameterValue=$T42_PASSPHRASE \
      ParameterKey=ClusterId,ParameterValue=$current_cluster_id \
      ParameterKey=SecurityGroupId,ParameterValue=$TARGET_REGION_SECURITY_GROUP \
      ParameterKey=Subnet,ParameterValue=$selected_subnet
  echo $stack_name_a >> "frmod_stage_stacks"
  aws s3 cp "./frmod_stage_stacks" s3://$DEPLOY_S3_BUCKET/

  # wait for the primary load balancer to be created
  aws --region $current_region cloudformation wait stack-create-complete --stack-name "$stack_name_a"
  read private_ip_a public_ip_a instance_id_a <<<$(watchStack $stack_name_a $current_region)
  [ -n "$private_ip_a" ] || {
    echo "Failed to launch primary load balancer"
    exit 1
  }

  # test RADIUS on the primary load balancer
  watchForResponse $public_ip_a "RADIUS"
  RESULT1=$(echo "User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 5 -D /usr/local/share/freeradius/ $public_ip_a:1814 auth V72QSZ2CE3 | grep "Received Access-Accept")
  if [ -z "$RESULT1" ]
  then
    echo "Failed Access-Accept test on $public_ip"
    exit 1
  fi

  # select next available subnet
  let i=i+1
  selected_subnet=${subnets_list[$(( i % subnets_amount ))]}
  # launch the standby load balancer
  stack_name_b=erad-load-balancer-b-$(date +%Y%m%d%H%M%S)
  aws --region $current_region cloudformation create-stack \
    --stack-name $stack_name_b \
    --capabilities CAPABILITY_NAMED_IAM \
    --template-body file://./aws/erad-load-balancer.yaml \
    --parameters \
      ParameterKey=AMIID,ParameterValue=$AMIID \
      ParameterKey=VPCID,ParameterValue=$VPCID \
      ParameterKey=DeployFile,ParameterValue=erad-load-balancer.stage.tar.gz \
      ParameterKey=KeyName,ParameterValue=$DEFAULT_SSH_KEY_NAME \
      ParameterKey=Passphrase,ParameterValue=$T42_PASSPHRASE \
      ParameterKey=PrimaryIp,ParameterValue=$private_ip_a \
      ParameterKey=ClusterId,ParameterValue=$current_cluster_id \
      ParameterKey=SecurityGroupId,ParameterValue=$TARGET_REGION_SECURITY_GROUP \
      ParameterKey=Subnet,ParameterValue=$selected_subnet
  echo $stack_name_b >> "frmod_stage_stacks"
  aws s3 cp "./frmod_stage_stacks" s3://$DEPLOY_S3_BUCKET/

  # wait for the standby load balancer to be created
  aws --region $current_region cloudformation wait stack-create-complete --stack-name "$stack_name_b"
  read private_ip_b public_ip_b instance_id_b <<<$(watchStack $stack_name_b $current_region)
  [ -n "$private_ip_b" ] || {
    echo "Failed to launch standby load balancer"
    exit 1
  }

  # test RADIUS on the standby load balancer
  watchForResponse $public_ip_b "RADIUS"
  RESULT1=$(echo "User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 5 -D /usr/local/share/freeradius/ $public_ip_b:1814 auth V72QSZ2CE3 | grep "Received Access-Accept")
  if [ -z "$RESULT1" ]
  then
    echo "Failed Access-Accept test on $public_ip"
    exit 1
  fi

  # LOAD_BALANCERS_A[] is associative array
  # so after for loop will be finished
  # LOAD_BALANCERS_A[] will be store deployed and working load balancers stack name for each region
  LOAD_BALANCERS_A[$current_region]=$stack_name_a
  LOAD_BALANCERS_B[$current_region]=$stack_name_b
done

# deploy if the go-live switch is set
if aws --region us-west-2 s3 ls s3://$DEPLOY_S3_BUCKET/ | grep "GO-LIVE-SWITCH"
then

  # make a note that we are in a production transition; this will block tear down from running which would tear down the live servers
  echo "We're in the middle of a rollout!" >> FRMOD-TRANSITION-IN-PROGRESS
  aws --region us-west-2 s3 cp FRMOD-TRANSITION-IN-PROGRESS s3://$DEPLOY_S3_BUCKET/

  # flip the go-live switch off
  # curl -X POST "https://automation:228ad5effbad04f6ee9b305aa44afe21@${JENKINS_HOST}/job/__erad-production-GO-LIVE-switch__/build"
  curl -k "https://$JENKINS_HOST/buildByToken/build?job=__erad-production-GO-LIVE-switch__&token=AxUnLDpPjRX51OPsoYxuLA3fF"

  # associate static IP with new load balancer

  for current_region in ${!LOAD_BALANCERS_A[@]}; do

    instance_id=$(aws cloudformation describe-stacks \
      --region $current_region \
      --stack-name ${LOAD_BALANCERS_A[$current_region]} \
      --query 'Stacks[0].Outputs[?OutputKey==`InstanceId`].OutputValue | [0]' \
      --output text )

    aws ec2 associate-address \
      --region $current_region \
      --instance-id ${instance_id} \
      --allocation-id ${ELASTIC_IPS[$current_region]} \
      --allow-reassociation

  done

  # finish_frmon_transition job understand $CLUSTER_ID with many values separated by commas
  # schedule the job to finish the transition after a 10 minute delay
  # wget -q --output-document - "https://$JENKINS_HOST/buildByToken/buildWithParameters?job=__erad-production-finish-frmod-transition__&delay=600sec&token=Fxy0Hlc2Nxn8JQzg6HPrp1Mxl0Qis6Ac&CLUSTER_ID=${CLUSTER_ID}"
  curl -k "https://$JENKINS_HOST/buildByToken/buildWithParameters?job=__erad-production-finish-frmod-transition__&delay=600sec&token=Fxy0Hlc2Nxn8JQzg6HPrp1Mxl0Qis6Ac&CLUSTER_ID=${CLUSTER_ID}"
fi

