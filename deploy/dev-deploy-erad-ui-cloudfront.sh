#!/bin/sh
set -x

if [[ $# -lt 2 ]]
  then
    echo "No arguments supplied"
    echo '$1 - CloudFormation stack name'
    echo '$2 - s3 bucket website url'
    echo '$3 - Custom domain name for cloudfront'
    echo '$4 - Cert ARN, use certs that was issued in us-east-1 only'
    exit
fi

stack_name=$1 # name for CloudFront stack
bucket_website_url=$2 # s3 bucket with docs stack name
custom_domain_name=$3 # Custom domain name for CloudFront Distribution
custom_domain_certificate_arn=$4 # Custom domain name for CloudFront Distribution


# bucket_website_url value contains protocol/scheme part, CloudFront require only domain
# removing scheme part
bucket_website_domain=${bucket_website_url#"http://"}

aws cloudformation deploy \
   --template-file $(pwd)/aws/dev-erad-ui-cloudfront.template.yaml \
   --stack-name ${stack_name} \
   --parameter-overrides \
        S3BucketSiteDomain="${bucket_website_domain}" \
        CustomDomainName="${custom_domain_name}" \
        CertArn="${custom_domain_certificate_arn}"

cf_url=$(aws cloudformation describe-stacks --stack-name ${stack_name} \
--query 'Stacks[0].Outputs[?OutputKey==`CloudFrontUrl`].OutputValue | [0]' \
--output text )

set +x
echo -e "\n"
echo "https://${cf_url}/       - CloudFront Distribution url"

