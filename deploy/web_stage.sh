#!/bin/bash

# The following values will be injected by Jenkins
# AWS_SECRET_ACCESS_KEY
# T42_PASSPHRASE
# SYSTEM_KEY
# INTEGRATION_KEY
# TARGET_REGION

export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
export AWS_DEFAULT_REGION=us-west-2

DEFAULT_SSH_KEY_NAME='shared_primary'
DEFAULT_SECURITY_GROUP_NAME='primary'
BASE_AMI_NAME="amzn2-ami-hvm-2.0.20200917.0-x86_64-gp2"


# This function expect that all subnets with tag Name ~ `vpc-erad-prod*` belongs to one VPC
getVPCID() {
  Region=$1
  vpcid=$(aws ec2 describe-subnets \
    --region $Region \
    --filters "Name=tag:Name,Values=vpc-erad-prod*" \
    --query "Subnets[0].VpcId" \
    --output text)

  echo $vpcid
}

getSubnets() {
  Region=$1
  subnets=$(aws ec2 describe-subnets \
    --region $Region \
    --filters "Name=tag:Name,Values=vpc-erad-prod*" \
    --query "Subnets[].SubnetId" \
    --output text)

  echo $subnets
}

# cleanup any previous run
rm -f production_web_stacks web_stage_stacks web_stage_ips

# do not continue if rollout is under way
if aws s3 ls s3://teamfortytwo.deploy/ | grep "WEB-TRANSITION-IN-PROGRESS"
then
  echo "Found WEB-TRANSITION-IN-PROGRESS"
  exit 1
fi

# check if a previous stage hasn't been torn down
if aws s3 ls s3://teamfortytwo.deploy/ | grep "stage"
then
  echo "**********************  PREVIOUS STAGE NOT TORN DOWN. CANNOT CONTINUE.  **********************"
  exit 1
fi

# determine what API build archive is requested
rm -f deployed_api_version staged_api_version version erad_webservices.stage.tar.gz
if [ "$ApiVersionNumber" == "Deployed" ]; then
  aws s3 cp s3://teamfortytwo.deploy/deployed_api_version .
  v=$(cat deployed_api_version)
  aws s3 cp s3://teamfortytwo.deploy/erad_webservices/erad_webservices_$v.tar.gz s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz
  echo "$v" > staged_api_version
elif [ "$ApiVersionNumber" == "Latest" ]; then
  aws s3 cp s3://teamfortytwo.deploy/erad_webservices.dev.tar.gz s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz
  aws s3 cp s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz .
  tar -zxvf erad_webservices.stage.tar.gz version
  mv version staged_api_version
else
  aws s3 cp s3://teamfortytwo.deploy/erad_webservices/erad_webservices_$ApiVersionNumber.tar.gz s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz
  echo "$ApiVersionNumber" > staged_api_version
fi

aws s3 cp staged_api_version s3://teamfortytwo.deploy/

# determine existing production servers in target region
aws --region $TARGET_REGION cloudformation list-stacks --max-items 10000 --stack-status-filter "CREATE_COMPLETE" | grep '"StackName": "erad-web-services-' | awk '{ print $2 }' | sed 's/,//;s/"//;s/"//' >> production_web_stacks

# get available subnets from target region
subnets_amount=$(aws --region $TARGET_REGION ec2 describe-subnets --filters "Name=tag:Name,Values=vpc-erad-prod*" --query "Subnets[] | length(@)")

# exit if there are no productions subnets (availability zones)
if [ "$subnets_amount" -eq "0" ]; then
  echo $'\n\n\n!!! There are no productions subnets - exiting !!!\n\n\n'
  exit 1;
fi

AMIID=$(aws ec2 describe-images \
  --region $TARGET_REGION \
  --output text \
  --owners amazon \
  --filters "Name=name,Values=${BASE_AMI_NAME}" \
  --query 'Images[*].ImageId | [0]'\
)

subnets=$(getSubnets $TARGET_REGION)
VPCID=$(getVPCID $TARGET_REGION)

# determine security group based on region
TARGET_REGION_SECURITY_GROUP=$(aws ec2 describe-security-groups \
  --region ${TARGET_REGION} \
  --query 'SecurityGroups[?GroupName==`'"$DEFAULT_SECURITY_GROUP_NAME"'`].GroupId|[0]' \
  --output text \
  )


OIFS=$IFS
IFS=$' '
subnets_list=( $subnets )
IFS=$OIFS


i=0

# launch as many new web servers as are currently in production
while read prod; do
  selected_subnet=${subnets_list[$(( i % subnets_amount ))]}

  stack_name=erad-web-services-$(date +%Y%m%d%H%M%S)
  aws --region $TARGET_REGION cloudformation create-stack \
    --capabilities CAPABILITY_NAMED_IAM \
    --stack-name $stack_name \
    --template-body file://./aws/erad-web-server.yaml \
    --parameters \
      ParameterKey=AMIID,ParameterValue=$AMIID \
      ParameterKey=VPCID,ParameterValue=$VPCID \
      ParameterKey=DeployFile,ParameterValue=erad_webservices.stage.tar.gz \
      ParameterKey=KeyName,ParameterValue=$DEFAULT_SSH_KEY_NAME \
      ParameterKey=SecurityGroupId,ParameterValue=$TARGET_REGION_SECURITY_GROUP \
      ParameterKey=Passphrase,ParameterValue=$T42_PASSPHRASE \
      ParameterKey=Subnet,ParameterValue=$selected_subnet
  let i=i+1
  echo $stack_name >> web_stage_stacks
  sleep 1  # so stack names don't conflict
done < ./production_web_stacks

# push the list of web server stacks to S3
aws s3 cp web_stage_stacks s3://teamfortytwo.deploy/
aws s3 cp production_web_stacks s3://teamfortytwo.deploy/

# add function to watch for server creation
watchStack() {

  if [ $# -eq 0 ]
  then
    echo "Usage: $1 Stack Name"
    echo "Output: [PRIVATE_IP] [PUBLIC_IP] [INSTANCE_ID]"
    exit 1
  fi
  stage_stack=$1

  status=""
  while [ "$status" != "CREATE_COMPLETE" ]
  do
    status=$(aws --region $TARGET_REGION cloudformation describe-stacks --stack-name $stage_stack | grep '"StackStatus"' | awk '{ print $2 }' | sed 's/,//;s/"//;s/"//')
    if [ "$status" == "CREATE_COMPLETE" ]; then
      break
    fi
    if [ "$status" != "CREATE_IN_PROGRESS" ]; then
      exit 1
    fi

    sleep 15
  done

  aws --region $TARGET_REGION cloudformation describe-stacks --stack-name $stage_stack | grep '"OutputValue"' | awk '{ print $2 }' | sed 's/,//;s/"//;s/"//' > info
  private_ip=$(cat info | sed '1q;d')
  public_ip=$(cat info | sed '2q;d')
  instance_id=$(cat info | sed '3q;d')
  rm -f info

  echo "$private_ip $public_ip $instance_id"
  exit 0
}

# for each API server, wait for creation and then retrieve IP addresses
newApiIds=""
while read stage_stack; do
  read private_ip public_ip instance_id <<<$(watchStack $stage_stack)
  echo $public_ip >> web_stage_ips
  newApiIds="$newApiIds $instance_id"
done < ./web_stage_stacks

# push the list of API server IPs to S3
aws s3 cp web_stage_ips s3://teamfortytwo.deploy/

# copy down stack lists
aws s3 cp s3://teamfortytwo.deploy/web_stage_stacks ./
aws s3 cp s3://teamfortytwo.deploy/production_web_stacks ./


# add function to wait for response on HTTP port
watchForResponse() {
  if [ $# -ne 2 ]
  then
    echo "Usage: $1 IP Address"
    echo "       $2 Traffic Type (either 'HTTP' or 'RADIUS')"
    exit 1
  fi
  ip=$1
  type=$2

  set +e
  for i in {1..120};
  do
    if [ "$type" == "HTTP" ]; then
      nc -z $ip 80
    else
      echo "radius"
      nc -zu $ip 1812
    fi
    if [ $? -eq 0 ]
    then
      break
    fi
    sleep 5
  done
  set -e
  sleep 5 # let the server finish spinning up
}

# integration test the individual API servers
while read public_ip; do
  # REVIEWTAG - consider the security implications for calling the public IP
  watchForResponse $public_ip "HTTP"
  python3 ./deploy/test_erad_system.py --SystemKey $SYSTEM_KEY --IntegrationKey $INTEGRATION_KEY --BaseUrl http://$public_ip system_test
done < ./web_stage_ips


# deploy if the go-live switch is set
if aws s3 ls s3://teamfortytwo.deploy/ | grep "GO-LIVE-SWITCH"
then

  # can't go live if instances are currently out of service
  if aws --region $TARGET_REGION elb describe-instance-health --load-balancer-name erad-api | grep -c '"State": "OutOfService"'
  then
    echo "*** CANNOT GO LIVE WITH INSTANCES OUT OF SERVICE IN THE LOAD BALANCER POOL ***"
    echo "Manually remove out of service instances and try again."
    exit 1
  fi

  # make a note that we are in a production transition; this will block tear down from running which would tear down the live servers
  echo "We're in the middle of a rollout!" >> WEB-TRANSITION-IN-PROGRESS
  aws s3 cp WEB-TRANSITION-IN-PROGRESS s3://teamfortytwo.deploy/

  # flip the go-live switch off
  curl -k "https://ci.gd1.io/buildByToken/build?job=__erad-production-GO-LIVE-switch__&token=AxUnLDpPjRX51OPsoYxuLA3fF"

  # get a list of current instances in the load balancer
  current=$(aws --region $TARGET_REGION elb describe-instance-health --load-balancer-name erad-api | grep "\"InstanceId\": \"" | awk '{ print $2 }' | sed 's/,//;s/"//;s/"//' | xargs)

  # add new instances to the load balancer
  aws --region $TARGET_REGION elb register-instances-with-load-balancer --load-balancer-name erad-api --instances $newApiIds

  # wait for all instances to become healthy
  found="false"
  set +e
  for i in {1..24}; # up to 2 minutes
  do
    sleep 5
    if ! aws --region $TARGET_REGION elb describe-instance-health --load-balancer-name erad-api | grep '"State": "OutOfService"'
    then
      found="true"
      break
    fi
  done
  set -e

  if [ "$found" = "false" ]; then
    echo "*** DEPLOYMENT FAILED. INSTANCES NEVER BECAME HEALTHY. ***"
    exit 1
  fi

  # remove previous instances from the load balancer
  aws --region $TARGET_REGION elb deregister-instances-from-load-balancer --load-balancer-name erad-api --instances $current

  # schedule the job to finish the transition after a 10 minute delay
  #wget -q --output-document - "https://ci.gd1.io/buildByToken/build?job=__erad-production-finish-web-transition__&delay=600sec&token=Fxy0Hlc2Nxn8JQzg6HPrp1Mxl0Qis6Ac"
  curl -k "https://ci.gd1.io/buildByToken/build?job=__erad-production-finish-web-transition__&delay=600sec&token=Fxy0Hlc2Nxn8JQzg6HPrp1Mxl0Qis6Ac"

fi
