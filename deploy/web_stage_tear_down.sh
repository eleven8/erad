#!/bin/bash

export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
# AWS_SECRET_ACCESS_KEY injected by Jenkins
export AWS_DEFAULT_REGION=us-west-2

Regions="us-west-2 us-east-2"

# do not tear down if rollout is under way
if aws s3 ls s3://teamfortytwo.deploy/ | grep "WEB-TRANSITION-IN-PROGRESS"
then
  echo "Found WEB-TRANSITION-IN-PROGRESS"
  exit 0
fi

# delete any stacks listed on S3
set +e
touch web_stage_stacks
aws s3 cp s3://teamfortytwo.deploy/web_stage_stacks .
while read stack_name; do
  for Region in $Regions; do
    aws --region $Region cloudformation delete-stack --stack-name $stack_name
  done
done < ./web_stage_stacks

# delete stage files
aws s3 rm s3://teamfortytwo.deploy/web_stage_stacks
aws s3 rm s3://teamfortytwo.deploy/production_web_stacks
aws s3 rm s3://teamfortytwo.deploy/web_stage_ips
aws s3 rm s3://teamfortytwo.deploy/staged_api_version
aws s3 rm s3://teamfortytwo.deploy/erad_webservices.stage.tar.gz

exit 0
