#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

# assumes we have a global install of node, bower, grunt-cli
set -x

export PATH=./node_modules/.bin:$PATH:/usr/local/bin
cd ./src/erad_static/

# replace {%ERAD_SERVER_NAME%} with the development server api address
sed -i 's/{%ERAD_SERVER_NAME%}/http:\/\/52.26.34.201:5000/g' app/scripts/services.js

npm install
bower install
node -v
npm -v
bower -v
npm run deploy -- development
