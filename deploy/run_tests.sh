#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

# configure local DynamoDB
python3 ./src/remove_database.py
python3 ./src/create_database.py


# base helpers for rlm_python from Freeradius

export PYTHONPATH=$PYTHONPATH:$(pwd)/src/erad/frmod/raddb/mods-config/python3


# run the tests
if [ ! -z "$T42_WEB_SERVICES_TEST" ]; then
	nosetests --with-xunit -s $@ ./src/erad/api/tests_teardown.py > erad-docs.yaml
    aws s3 cp erad-docs.yaml s3://teamfortytwo.deploy/docs/dev/erad-docs.yaml
else
    nosetests --with-xunit $@
fi
