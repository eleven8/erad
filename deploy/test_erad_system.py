import unittest
import sys
import requests
import json
from optparse import OptionParser

# These tests run against staging webservers which access PRODUCTION Database. 
# Keep them safe (no writes)

class TestEradSystem(unittest.TestCase):

    # These will be overwritten by arguments passed in. They will reference production.
    BaseUrl = None
    SystemKey = None
    IntegrationKey = None

    def assert_is_superset( self, innerset, superset ):
        # for arrays, they must contain the same elements
        if isinstance( innerset, list ):
            self.assertEqual( len(innerset), len(superset) )
            for i in range(0, len(innerset)):
                self.assert_is_superset( innerset[i], superset[i] )
        # for dicts, the superset must contain the items in the innerset (but may have more)
        elif isinstance(innerset, dict):
            for k in innerset:
                self.assertTrue( k in superset )
                self.assert_is_superset( innerset[k], superset[k] )
        # otherwise, values must be equivilent
        else:
            self.assertEqual( superset, innerset )

    def json_get_expect( self, path, code ):
        r = requests.get(
                self.BaseUrl + path,  
                headers={ 'Content-type': 'application/json' })
        self.assertEqual(r.status_code, code)

    def json_post_expect( self, path, input, expected_response, code ):
        self.maxDiff = None # show full diff if response doesnt match
        r = requests.post(
                self.BaseUrl + path,  
                headers={ 'Content-type': 'application/json' },
                data=json.dumps(input))
        self.assertEqual(r.status_code, code, '{0}, {1}, {2}'.format(r.status_code, code, "JSON: " + str(r.json())))
        actual = r.json()
        if not expected_response is None:
            self.assert_is_superset( expected_response, actual )
        return actual

    def json_post_expect_200( self, path, input, expected_response=None ):
        return self.json_post_expect( path, input, expected_response, 200 )

    def test_erad_database_notfound(self):
        self.json_get_expect( '/erad/database', 404 )

    def test_erad_schema_notfound(self):
        self.json_get_expect( '/erad/schema', 404 )
    
    def test_system_supplicant_groupid(self):
        self.json_post_expect_200( 
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.SystemKey,
                "Authenticator": { "ID": "bogus" },
                "Supplicant": { "Username" : "babble" },
                "Group": { "ID" : "SR-980-83" },
            },
            {
                "Error": None,
                "Group": 
                {
                    "ID": "SR-980-83",
                    "Name": "ElevenRADIUS Monitoring",
                    "RemoteServerUrl": "52.41.63.155:1812",
                    "SharedSecret": "ElevenRules"
                },
                "Supplicant": 
                {
                    "Group_ID": "elevenos::SR-980-83",
                    "Username": "babble",
                    "Password": "3000",
                    "UseRemote": False,
                    "UseWildcard": False,
                    "Vlan": 42
                }
            })
    
    def test_system_supplicant_user(self):
        self.json_post_expect_200( 
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.SystemKey,
                "Authenticator": { "ID": "BA:BB:1E:BA:BB:1E" },
                "Supplicant": { "Username" : "babble" }
            },
            {
                "Error": None,
                "Group": 
                {
                    "ID": "SR-980-83",
                    "Name": "ElevenRADIUS Monitoring",
                    "RemoteServerUrl": "52.41.63.155:1812",
                    "SharedSecret": "ElevenRules"
                },
                "Supplicant": 
                {
                    "Group_ID": "elevenos::SR-980-83",
                    "Username": "babble",
                    "Password": "3000",
                    "UseRemote": False,
                    "UseWildcard": False,
                    "Vlan": 42
                }
            })

    def test_system_supplicant_wildcard(self):
        self.json_post_expect_200( 
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.SystemKey,
                "Authenticator": { "ID": "BA:BB:1E:BA:BB:1E" },
                "Supplicant": { "Username" : "James.Morgan.McGill" }
            },
            {
                "Error": None,
                "Group": 
                {
                    "ID": "SR-980-83",
                    "Name": "ElevenRADIUS Monitoring",
                    "RemoteServerUrl": "52.41.63.155:1812",
                    "SharedSecret": "ElevenRules"
                },
                "Supplicant": 
                {
                    "Group_ID": "elevenos::SR-980-83",
                    "Username": "^",
                    "Password": None,
                    "UseRemote": True,
                    "UseWildcard": True,
                    "Vlan": 1
                }
            })

    def test_integration_group_load(self):
        self.json_post_expect_200( 
            '/erad/integration/group/load',
            {
                "IntegrationKey": self.IntegrationKey,
                "Group": { "ID": "SR-980-83" }
            },
            {
                "Error": None,
                "Group": 
                {
                    "ID": "SR-980-83",
                    "Name": "ElevenRADIUS Monitoring",
                    "RemoteServerUrl": "52.41.63.155:1812",
                    "SharedSecret": "ElevenRules"
                }
            })

    def test_integration_authenticator_load(self):
        self.json_post_expect_200( 
            '/erad/integration/authenticator/load',
            {
                "IntegrationKey": self.IntegrationKey,
                "Authenticator": { "ID": "BA:BB:1E:BA:BB:1E" }
            },
            {
                "Error": None,
                "Authenticator": 
                {
                    "ID": "BA:BB:1E:BA:BB:1E",
                    "Group_ID": "SR-980-83",
                    "RadiusAttribute": "called-station-id"
                }
            })

    def test_admin_session_load(self):
        self.json_post_expect( 
            '/erad/admin/session/load',
            {
                "Session": { "ID": "this-id-will-never-exist-60439a12-6b20-402b-94a1-570a0364900e" }
            },
            {
                "Error": "Access Denied."
            },
            403)
    

if __name__ == "__main__":
    parser = OptionParser(usage="Usage: test_erad_system.py [options] UrlBase")
    parser.add_option("-R", "--SystemKey", dest="SystemKey",
                      help="SystemKey to be used for testing", metavar="SystemKey")
    parser.add_option("-I", "--IntegrationKey", dest="IntegrationKey",
                      help="IntegrationKey to be used for testing", metavar="IntegrationKey")
    parser.add_option("-B", "--BaseUrl", dest="BaseUrl",
                      help="BaseUrl that will be tested. Defaults to http://localhost:5000", metavar="BaseUrl")
    (options, args) = parser.parse_args()
    TestEradSystem.IntegrationKey = "i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF" if not options.IntegrationKey else options.IntegrationKey
    TestEradSystem.SystemKey = "r_7X385PDgqPSB2cMvpmPZ6GHqXh5Cd8ZF" if not options.SystemKey else options.SystemKey
    TestEradSystem.BaseUrl = "http://localhost:5000" if not options.BaseUrl else options.BaseUrl

    sys.argv = args
    unittest.main()








