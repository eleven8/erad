GPG is used to encrypt our privileged information. We
encrypt using a symmetric cipher so only a passphrase
is needed.

-- PASSPHRASE --
Create a passphrase to use with encryption. 
Use a long random string of numbers and uppercase and
lowercase letters (e.g., 64 characters). At least two,
but only a few, trusted individuals within the
organization should hold the key in a secure password
vault. At this time, those individuals are:

John Steele
Piotr Wiatrowski

Repeat this process when a holder of the key leaves the
organization. Change privileged information within the
file and re-encrypt. Search for all files with a
".secure" extension.


-- ENCRYPTING A VALUE --
To encrypt a value, run the following on a Linux system
that has GPG installed (yum -y install gpg):

   echo '<<INPUT>>' | gpg --batch --yes --cipher-algo AES256 --symmetric --passphrase <<PASSPHRASE>> | base64 | tr -d '\n' && echo
   
The output from above should be placed inside a file
with a ".secure" extension. Example:

   AWS_SECRET_ACCESS_KEY=$(decrypt $1 <<SECURE_VALUE>>)

As the first line in the ".secure" file, we include
this standard function:

   decrypt() { echo "$2" | base64 --decode | gpg --decrypt --batch --passphrase "$1" ; }
   

LEGACY PROCEDURE (DEPRECATED 2018-09-01):
To encrypt a file, run the following on a fresh Amazon
Linux server:

   yum -y install gpg
   gpg --batch --yes --armor --cipher-algo AES256 --symmetric --passphrase <<PASSPHRASE HERE>> --output <<SECURE OUTPUT FILENAME>> <<INPUT FILENAME>>
   
Use the ".secure" extension for all encrypted files
and keep them in bitbucket so we can easily find all
files that need to be re-encrypted when individuals
leave the organization. Use the --armor option (ascii)
to make file manipulations easier.


-- DECRYPTING A FILE --
Generally, this is going to happen in an automated
fashion on a production server. Remember to keep the
server secure (limited to only the privileged users).
Anyone with access to the server will have access to
the decrypted data and likely the passphrase from log
files. 

To decrypt a file, use the following as a guideline
for creating your automated process:

   yum -y install gpg
   gpg --batch --yes --passphrase <<PASSPHRASE HERE>> --output <<OUTPUT FILENAME>> --decrypt <<SECURE INPUT FILENAME>>

