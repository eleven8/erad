
==================== (Reading Material) =====================

http://flask.pocoo.org/docs/0.10/config/
http://pynamodb.readthedocs.org/en/latest/
http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Tools.DynamoDBLocal.html
http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/BestPractices.html


==================== Configure Python =====================
We are using Python 2.7.2.
Install modules:
-sudo pip install Flask
-sudo pip install flask-compress
-sudo pip install pytz
-sudo pip install boto
-sudo pip install pynamodb
-sudo pip install requests

On CentOS 7 I had a different version of dateutil. The fix:
    sudo pip uninstall python-dateutil
    sudo pip install python-dateutil==2.2


==================== Configure BOTO =====================

sudo touch /etc/boto.cfg
sudo chmod 644 /etc/boto.cfg

(Edit /etc/boto.cfg with the following settings)
.......
[Credentials]
aws_access_key_id = <Access ID without quotes>
aws_secret_access_key = <Secret Key without quotes>

[DynamoDB]
region = us-west-2
host = https://dynamodb.us-west-2.amazonaws.com
validate_checksums = True
.......


================ Configure DynamoDB Local =================
Run DynamoDB Local:
-Note: DynamoDB Local runs as a webserver on http://localhost:8000.
-Download and unzip https://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.zip
-Rename the directory to "dynamodb"
-Make sure you have at least Java 6: https://www.java.com/en/download
-Start your DynamoDB Local by running 'DynamoDBLocal.jar'
    On OSX:
        $ cd dynamodb
        $ "/Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java" -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar
        When DynamoDB Local is used, you will see logs in the Terminal
        When you want to quit dynamodb local, hit CTRL+C
    On Linux:
        $ cd dynamodb
        $ java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar
        When DynamoDB Local is used, you will see logs in the Terminal
        When you want to quit dynamodb local, hit CTRL+C


==================== Configure Apache ====================

This guide is for ubuntu server. If you are using another variant,
you may need to replace apt-get with something else.

Steps:
1. SSH into the linux server

2. Update essentials
    $ sudo apt-get update
    $ sudo apt-get install build-essential git-core curl

3. Install Apache
    $ sudo apt-get install apache2

4. Install pip (the python package manager)
    $ sudo easy_install pip

5. Install flask
    $ sudo pip install Flask

6. Install flask mod for python: mod_wsgi
    $ sudo apt-get install libapache2-mod-wsgi

7. Make a WSGI file: /path/to/your/<appname>/<appname>.wsgi
    Give it the following contents:
        import sys
        sys.path.insert(0, '/path/to/your/<appname>')
        from <appname> import app as application

8. Make sure <appname>.py and <appname>.wsgi are marked as executable files.

9. Configure Apache:
    Edit whatever ".conf" file you are using:
            <VirtualHost *:80>
                ServerName yoursitedomainname.com
                WSGIDaemonProcess <appname> user=<app-user> group=<app-group> threads=5
                WSGIScriptAlias / /path/to/your/<appname>/<appname>.wsgi
                <Directory /path/to/your/<appname>>
                    WSGIProcessGroup <appname>
                    WSGIApplicationGroup %{GLOBAL}
                    Order deny,allow
                    Allow from all
                    Require all granted
                </Directory>
            </VirtualHost>

10. Restart Apache




































