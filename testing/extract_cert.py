import json
import base64
from OpenSSL import crypto
import os
import argparse

parser = argparse.ArgumentParser(description='Extract certificate from json response and saved it to use with eapol_test utility.')

parser.add_argument('certname', type=str,
                    help='filename to save extracted certificate')

args = parser.parse_args()

dir_path = os.path.dirname(os.path.realpath(__file__))

json_data_filename = 'json_with_cert'
path_to_eapol_test ='test-eapol/eapol_test_configs'
tmp_cert_name = args.certname

with open(os.path.join(dir_path, json_data_filename), 'rb') as json_data, \
    open(os.path.join(dir_path, path_to_eapol_test, tmp_cert_name + '.key'), 'wb') as key, \
        open(os.path.join(dir_path, path_to_eapol_test, tmp_cert_name + '.crt'), 'wb') as cert:

    json_data = json.loads(json_data.read().decode('utf-8'))
    decoded_cert = base64.decodebytes(json_data['Certificate'].encode('utf-8'))
    p12 = crypto.load_pkcs12(decoded_cert, b"")

    key.write(
        crypto.dump_privatekey(crypto.FILETYPE_PEM, p12.get_privatekey())
    )
    cert.write(
        crypto.dump_certificate(crypto.FILETYPE_PEM, p12.get_certificate())
    )
