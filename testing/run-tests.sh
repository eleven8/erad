#!/bin/bash

set -x
# uncomment these lines if you run tests on new ec2 instance
# -------------------------
# PATH=/usr/local/bin:$PATH
# yum -y update
# yum -y install docker
# service docker start
# curl -O https://bootstrap.pypa.io/get-pip.py
# python get-pip.py
# pip install docker-compose pyopenssl
# ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
# -------------------------

# frmod_tests uses nose module
# so if you run these tests on local machine
# create virtualenv and install nose or you can install it in system

# for jenkins
# if command not available by standard $PATH
# make alias
docker-compose -v
if [[ $? -ne 0 ]]; then
    alias docker-compose="/usr/sbin/docker-compose"
fi

teardown(){
    docker-compose down
    docker image prune -f
    exit 1
}

pip freeze

pip install -U pip
pip install pyopenssl nose

docker-compose down
if ! docker-compose build \
    dynamodb-localhost \
    erad-webservice ; then
    teardown
fi


if ! docker-compose build radius-home; then
    teardown
fi

# In Jenkins environment there is a possibility that two or more tests will be running simultaneously.
# If some service inside docker will expose same ports, then containers will interfere with each other.
# To prevent this, for tests inside jenkins is used another docker-compose file. It repeat main docker-compose
# file, except it not expose ports

if [ ! -z $WORKSPACE ]; then
    COMPOSE_FILE='-f jenkins-env-docker-compose.yaml'
else
    COMPOSE_FILE=''
fi

docker-compose $COMPOSE_FILE up -d dynamodb-localhost
sleep 10
docker-compose $COMPOSE_FILE up -d erad-webservice


echo waiting 60 seconds to up erad-webservice
sleep 60

LOGS=$(docker-compose logs erad-webservice)

MAGIC_STATUS=$(echo "$LOGS" | grep -oP 'ERAD-WEBSERVICE READY FOR SERVE$' || echo '')

echo -n 'Checking erad-webservice status'
if [ -n "$MAGIC_STATUS" ]; then
echo ' ERAD-WEBSERCIE - OK!'; else echo ' Fail! Try to increase sleep pause'; docker-compose down && exit 1; fi;

# run default tests
if ! docker-compose exec -T erad-webservice  /bin/bash /usr/local/src/api_unit_testing.sh ; then
    teardown
fi

# when api_unit_testing.sh runs in docker container it runs under root user and create ca.srl file
# changing permission of directory and all files in it to allow erad api wsgi process
# read ca.srl file and create new certs
docker-compose exec erad-webservice chown -R apache:apache /usr/local/webservices/erad/api/certs/


if [ ! -z $WORKSPACE ]; then
    if ! docker-compose exec -T erad-webservice cat /usr/local/nosetests.xml > $WORKSPACE/nosetests.xml ; then
        teardown
    fi
fi;

ERAD_WEBSERVICE=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker-compose ps -q erad-webservice))


curl -H "Content-Type: application/json" -X POST -d '{
    "Session": { "ID": "erad-tests-session" },
    "NoPassword" : "true",
    "Supplicant":
    {
        "Group_ID": "foo",
        "Username": "testing",
        "Password": "testing",
        "UseRemote": "false",
        "Vlan": 1232
    }
}' http://$ERAD_WEBSERVICE/erad/admin/supplicant/certificate > json_with_cert
python extract_cert.py testing

curl -H "Content-Type: application/json" -X POST -d '{
    "Session": { "ID": "erad-tests-session" },
    "NoPassword" : "true",
    "Supplicant":
    {
        "Group_ID": "foo",
        "Username": "expired_testing",
        "Password": "expired",
        "UseRemote": "false",
        "Vlan": 1232
    }
}' http://$ERAD_WEBSERVICE/erad/admin/supplicant/certificate > json_with_cert
python extract_cert.py expired_testing

# erad_auth.py now using redis cache, starting it first
docker-compose up -d redis
sleep 3

docker-compose up -d eapol-test radius-home radius-eaponly-home
sleep 3
docker-compose up -d radius-proxy radius-proxy-for-eap-only

echo "sleep 10 seconds to bootstrap containers"
sleep 10

# this comment just to not look how construct strig to run test
# also do not forget add calling-station-id to erad-config on /start page
# docker-compose exec eapol-test eapol_test -c /test_configs/eap-tls.conf -s 4905DVCA3B -a $RADIUS_IP -p 1812 -N 30:s:elevenos -N 32:s:foo


if [ ! -z $WORKSPACE ]; then
    if ! docker-compose exec -T radius-proxy python3 -m nose --with-xunit --xunit-file=/erad_module.xml /erad-src/erad/frmod/test ; then
        teardown
    fi

    if ! docker-compose exec -T radius-proxy cat /erad_module.xml > $WORKSPACE/nosetests.xml ; then
        teardown
    fi

    if ! python3 -m nose -x --with-xunit --xunit-file=$WORKSPACE/radius_connections_nosetests.xml frmod_tests ; then
        teardown
    fi


    if ! docker-compose logs dynamodb-localhost &> $WORKSPACE/dynamo.log ; then
        teardown
    fi

    touch $WORKSPACE/dynamo_error.log

    if ! docker-compose logs erad-webservice &> $WORKSPACE/webservice_error.log ; then
        teardown
    fi
    touch $WORKSPACE/webservice.log
else
    # local testing no needs in generating xunit/junit result file
    # to run tests /erad-src/erad/frmod/test we need to tell python where to find radiusd.py module
    # because it is docker PYTHONPATH configured with docker-compose file
    docker-compose exec -T radius-proxy python3 -m nose /erad-src/erad/frmod/test
    python3 -m unittest discover -s frmod_tests
fi;


docker-compose down
docker image prune -f
