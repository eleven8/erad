# coding: utf-8
import copy
import unittest
from time import sleep
from nose.tools import nottest

from subprocess import Popen,  PIPE, STDOUT

from io import StringIO

# not tested:
# - MS-CHAPv2-Password because it should be hex string
# - EAP-MSCHAPv2 because in eap module config default_eap_type = md5


# to test MS-CHAPv2-Password use config like this, but password should be in hex
# docker exec eapol-test sh -c "echo ""User-Name = testing, MS-CHAPv2-Password = testing, Nas-Identifier = foo, Called-Station-Id = elevenos"" | radclient -s $RADIUS_IP:1812 auth 4905DVCA3B"

def get_container_id(container_name):
	args = ['docker-compose',
		'ps',
		'-q',
		container_name,
		]
	docker_compose_process = Popen(args, stdout = PIPE, stderr = STDOUT, universal_newlines=True)
	container_id, stderrdata = docker_compose_process.communicate()
	container_id = container_id.strip()
	return container_id

class BaseTestCase(unittest.TestCase):
	# This pause is not matter anything
	# It just to order log records in web interface.
	# Without it log records can be mixed,
	# because time resolution for logs is 1 second,
	# but request can be process in less 1 second.
	SLEEP_TIME = 0

	testing_attrs = {
		'vlan' : u'999'
	}

	testing_remote_attrs = {
		'vlan' : u'888'
	}


	testing_home_server_attrs = {
		'vlan' : u'143',
		'tunnel_type' : u'VLAN',
		'tunnel_medium' : u'IEEE-802',
		'idle_timeout' : u'1800',
	}


	testing_remote_fulleap_attrs = {
		'vlan' : u'333'
	}


	def _args(container_name):
		return ['docker',
			'inspect',
			'-f',
			'{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}',
			get_container_id(container_name)
			]


	def eapol_args(self, config_file, radius_ip, nas_id=None, additional_attrs_list=[]):
		args = ['docker-compose',
			'exec',
			'-T',
			'eapol-test',
			'eapol_test', # eapol_test - this is programm name
			'-c',
			config_file,
			'-s',
			'4905DVCA3B',
			'-a',
			radius_ip,
			'-p',
			'1812',
			'-N',
			'30:s:elevenos',
			'-N',
			'32:s:{nas_id}'.format(nas_id = nas_id or 'foo')
			]
		args.extend(additional_attrs_list)
		return args


	inspect_process = Popen(_args('radius-proxy'), stdout = PIPE, stderr = STDOUT, universal_newlines=True)

	RADIUS_IP, stderrdata = inspect_process.communicate()
	RADIUS_IP = RADIUS_IP.strip()

	inspect_process = Popen(_args('radius-proxy-for-eap-only'), stdout = PIPE, stderr = STDOUT, universal_newlines=True)

	RADIUS_IP_EAP_ONLY, stderrdata = inspect_process.communicate()
	RADIUS_IP_EAP_ONLY = RADIUS_IP_EAP_ONLY.strip()


	def check_attrs(self):
		raise NotImplementedError

class TestRadclient(BaseTestCase):
	@nottest
	def base_successful_test(self, config, radius_ip, expected_attrs = None):
		def f():
			sleep(self.SLEEP_TIME)
			args = ['docker-compose',
					'exec',
					'-T',
					'eapol-test',
					'sh',
					'-c',
					config + ' | radclient -x -s {RADIUS_IP}:1812 auth 4905DVCA3B'.format(RADIUS_IP = radius_ip),
					]

			p = Popen(args, stdout = PIPE, stderr = STDOUT, universal_newlines=True)
			(stdoutdata, stderrdata) = p.communicate()
			try:
				self.assertEqual(p.returncode, 0)
				if expected_attrs is not None:
					self.check_attrs(stdoutdata, **expected_attrs)

			except AssertionError as err:
				print(stdoutdata)
				raise err

			# cache erad-auth
			p = Popen(args, stdout = PIPE, stderr = STDOUT, universal_newlines=True)
			(stdoutdata, stderrdata) = p.communicate()
			try:
				self.assertEqual(p.returncode, 0)
			except AssertionError as err:
				print(stdoutdata)
				raise err
		return f


	@nottest
	def base_fail_test(self, config, radius_ip):
		def f():
			sleep(self.SLEEP_TIME)
			args = ['docker-compose',
					'exec',
					'-T',
					'eapol-test',
					'sh',
					'-c',
					config + ' | radclient -x -s {RADIUS_IP}:1812 auth 4905DVCA3B'.format(RADIUS_IP = radius_ip),
					]

			p = Popen(args, stdout = PIPE, stderr = STDOUT, universal_newlines=True)
			(stdoutdata, stderrdata) = p.communicate()
			try:
				self.assertEqual(p.returncode, 1)
			except AssertionError as err:
				print(stdoutdata)
				raise err

			# cache erad-auth
			p = Popen(args, stdout = PIPE, stderr = STDOUT, universal_newlines=True)
			(stdoutdata, stderrdata) = p.communicate()
			try:
				self.assertEqual(p.returncode, 1)
			except AssertionError as err:
				print(stdoutdata)
				raise err
		return f

	@nottest
	def check_attrs(self, stdout, vlan=None, tunnel_type=None, tunnel_medium=None, idle_timeout=None):
		stdout_iter = StringIO(stdout)
		found = {}

		if vlan is not None:
			found['vlan'] = None
		if tunnel_type is not None:
			found['tunnel_type'] = None
		if tunnel_medium is not None:
			found['tunnel_medium'] = None
		if idle_timeout is not None:
			found['idle_timeout'] = None

		for line in stdout_iter:
			line = line.strip()
			# this check works only for vlan with 3 digit number
			# tune it if you need to test vlan number with a greater or lesser number of digits
			if line.startswith('Tunnel-Private-Group-Id:0 = ') and vlan is not None:
				attr_value = line.replace('Tunnel-Private-Group-Id:0 = ', '').strip('"')
				self.assertEqual(attr_value, str(vlan))
				found['vlan'] = attr_value

			if line.startswith('Tunnel-Type:0 = ') and tunnel_type is not None:
				attr_value = line.replace('Tunnel-Type:0 = ', '').strip('"')
				self.assertEqual(attr_value, str(tunnel_type))
				found['tunnel_type'] = attr_value

			if line.startswith('Tunnel-Medium-Type:0 = ') and tunnel_medium is not None:
				attr_value = line.replace('Tunnel-Medium-Type:0 = ', '').strip('"')
				self.assertEqual(attr_value, str(tunnel_medium))
				found['tunnel_medium'] = attr_value

			if line.startswith('Idle-Timeout = ') and idle_timeout is not None:
				attr_value = line.replace('Idle-Timeout = ', '').strip('"')
				self.assertEqual(attr_value, str(idle_timeout))
				found['idle_timeout'] = attr_value

		for key, value in found.items():
			self.assertNotEqual(value, None)

	# local auth

	def test_erad_auth(self):
		config = 'echo ""User-Name = aa-bb-cc-dd-ee-ff ,User-Password = testing, Called-Station-Id = 11-22-33-44-55-66, Calling-Station-Id = aa-bb-cc-dd-ee-ff""'
		self.base_successful_test(config, self.RADIUS_IP, self.testing_attrs)()

	def test_simple_auth_ms_chap(self):
		config = 'echo ""User-Name = testing, MS-CHAP-Password = testing, Nas-Identifier = foo, Called-Station-Id = elevenos""'
		self.base_successful_test(config, self.RADIUS_IP, self.testing_attrs)()

	def test_simple_proxy_local_auth_ms_chap(self):
		config = 'echo ""User-Name = testing, MS-CHAP-Password = testing, Nas-Identifier = foo, Called-Station-Id = elevenos""'
		self.base_successful_test(config, self.RADIUS_IP, self.testing_attrs)()

	def test_custom_json_data(self):
		config = 'echo ""User-Name = username_custom_data, MS-CHAP-Password = aihjwd9awd098jawd9j8ad, Nas-Identifier = XX-YYYY, Called-Station-Id = elevenos""'
		custom_vlan = copy.deepcopy(self.testing_attrs)
		custom_vlan['vlan'] = '1232'
		self.base_successful_test(config, self.RADIUS_IP, custom_vlan)()


	# local auth wrong credentials

	def test_erad_auth_wrong_username(self):
		config = 'echo ""User-Name = aa-bb-cc-dd-ee-ab ,User-Password = testing, Called-Station-Id = 11-22-33-44-55-66, Calling-Station-Id = aa-bb-cc-dd-ee-ff""'
		self.base_fail_test(config, self.RADIUS_IP)()

	def test_simple_auth_ms_chap_wrong_cred(self):
		config = 'echo ""User-Name = testing, MS-CHAP-Password = wrong_password, Nas-Identifier = foo, Called-Station-Id = elevenos""'
		self.base_fail_test(config, self.RADIUS_IP)()

	def test_simple_proxy_local_auth_ms_chap_wrong_cred(self):
		config = 'echo ""User-Name = testing, MS-CHAP-Password = wrong_password, Nas-Identifier = foo, Called-Station-Id = elevenos""'
		self.base_fail_test(config, self.RADIUS_IP)()

	# remote auth

	def test_erad_auth_proxy_remote_auth(self):
		config = 'echo ""User-Name = aa-bb-cc-dd-ee-99, User-Password = testing, Called-Station-Id = 11-22-33-44-55-66, Calling-Station-Id = aa-bb-cc-dd-ee-99""'
		self.base_successful_test(config, self.RADIUS_IP, self.testing_remote_attrs)()

	def test_simple_proxy_local_auth_ms_chap(self):
		config = 'echo ""User-Name = testing_remote, MS-CHAP-Password = testing, Nas-Identifier = foo, Called-Station-Id = elevenos""'
		self.base_successful_test(config, self.RADIUS_IP, self.testing_remote_attrs)()

	# remote auth wrong credentials

	def test_erad_auth_proxy_remote_auth_wrong_username(self):
		config = 'echo ""User-Name = aa-bb-cc-dd-ee-ab ,User-Password = testing, Called-Station-Id = 11-22-33-44-55-66, Calling-Station-Id = aa-bb-cc-dd-ee-99""'
		self.base_fail_test(config, self.RADIUS_IP)()


	def test_simple_proxy_local_auth_ms_chap_wrong_cred(self):
		config = 'echo ""User-Name = testing_remote, MS-CHAP-Password = wrong_password, Nas-Identifier = foo, Called-Station-Id = elevenos""'
		self.base_fail_test(config, self.RADIUS_IP)()

	# Testing for realm value (suffix)
	def test_erad_auth_realm_pass(self):
		config = 'echo ""User-Name = testing@enterpriseauth.com, User-Password = testing, Nas-Identifier = foo, Called-Station-Id = elevenos""'
		self.base_successful_test(config, self.RADIUS_IP, self.testing_attrs)()

	def test_erad_auth_realm_fail(self):
		config = 'echo ""User-Name = wrong@enterpriseauth.com, User-Password = testing, Nas-Identifier = foo, Called-Station-Id = elevenos""'
		self.base_fail_test(config, self.RADIUS_IP)()

	def test_erad_auth_realm_not_exists_pass(self):
		config = 'echo ""User-Name = testing@notexists.com, User-Password = testing, Nas-Identifier = foo, Called-Station-Id = elevenos""'
		self.base_successful_test(config, self.RADIUS_IP, self.testing_attrs)()


	# By default home radius server instance configured to always return hardcoded vlan id value and other attrs,
	# see `erad/srcerad/frmod/remove_proxy.py`.
	# This test is check, that these attributes from home server will be in final reply.
	# erad/src/erad/api/schema/__init__.py create additional group `fooremoteattr`, for this tests.
	def test_attr_from_home_server(self):
		config = 'echo ""User-Name = testing_remote_attr, MS-CHAP-Password = testing, Nas-Identifier = fooremoteattr, Called-Station-Id = elevenos""'
		self.base_successful_test(config, self.RADIUS_IP, self.testing_home_server_attrs)()


class TestEapol(BaseTestCase):
	@nottest
	def base_successful_test(self, config_file, radius_srv_ip, expected_attrs = None, nas_id = None, additional_attrs_list=[]):

		def f():
			sleep(self.SLEEP_TIME)
			args = self.eapol_args(config_file, radius_srv_ip, nas_id = nas_id, additional_attrs_list=additional_attrs_list)


			p = Popen(args, stdout = PIPE, stderr = STDOUT, universal_newlines=True)
			(stdoutdata, stderrdata) = p.communicate()
			try:
				self.assertEqual(p.returncode, 0)
				if expected_attrs is not None:
					self.check_attrs(stdoutdata, **expected_attrs)
			except AssertionError as err:
				print(stdoutdata)
				raise err

			# cache erad-auth
			p = Popen(args, stdout = PIPE, stderr = STDOUT, universal_newlines=True)
			(stdoutdata, stderrdata) = p.communicate()
			try:
				self.assertEqual(p.returncode, 0)
			except AssertionError as err:
				print(stdoutdata)
				raise err

		return f

	@nottest
	def base_fail_test(self, config_file, radius_srv_ip):

		def f():
			sleep(self.SLEEP_TIME)
			args = self.eapol_args(config_file, self.RADIUS_IP)

			p = Popen(args, stdout = PIPE, stderr = STDOUT, universal_newlines=True)
			(stdoutdata, stderrdata) = p.communicate()
			try:
				self.assertNotEqual(p.returncode, 0)
			except AssertionError as err:
				print(stdoutdata)
				raise err

			# cache erad-auth
			p = Popen(args, stdout = PIPE, stderr = STDOUT, universal_newlines=True)
			(stdoutdata, stderrdata) = p.communicate()
			try:
				self.assertNotEqual(p.returncode, 0)
			except AssertionError as err:
				print(stdoutdata)
				raise err

		return f

	@nottest
	def check_attrs(self, stdout, vlan=None, tunnel_type=None, tunnel_medium=None, idle_timeout=None):
		stdout_iter = StringIO(stdout)

		found = {}

		if vlan is not None:
			found['vlan'] = None
		if tunnel_type is not None:
			found['tunnel_type'] = None
		if tunnel_medium is not None:
			found['tunnel_medium'] = None
		if idle_timeout is not None:
			found['idle_timeout'] = None

		# Mandatory attr
		found['eap-message'] = None


		maps = {
			'0000000d': u'VLAN',
			'00000006': u'IEEE-802'
		}


		for line in stdout_iter:
			line = line.strip()

			if 'Attribute 79 (EAP-Message)' in line:
				attr_value_str = stdout_iter.readline().strip()
				attr_value = attr_value_str.replace('Value: ', '')
				found['eap-message'] = attr_value

			if line == 'Attribute 81 (Tunnel-Private-Group-Id) length=5' and vlan is not None:
				attr_value_str = stdout_iter.readline().strip()
				attr_value = attr_value_str.replace('Value: ', '')
				self.assertEqual(bytearray.fromhex(attr_value).decode(), str(vlan))
				found['vlan'] = attr_value

			if line == 'Attribute 64 (Tunnel-Type) length=6' and tunnel_type is not None:
				attr_value_str = stdout_iter.readline().strip()
				attr_value = attr_value_str.replace('Value: ', '')
				self.assertEqual(maps[attr_value], str(tunnel_type))
				found['tunnel_type'] = attr_value

			if line == 'Attribute 65 (Tunnel-Medium-Type) length=6' and tunnel_medium is not None:
				attr_value_str = stdout_iter.readline().strip()
				attr_value = attr_value_str.replace('Value: ', '')
				self.assertEqual(maps[attr_value], str(tunnel_medium))
				found['tunnel_medium'] = attr_value

			if line == 'Attribute 28 (Idle-Timeout) length=6' and idle_timeout is not None:
				attr_value_str = stdout_iter.readline().strip()
				attr_value = attr_value_str.replace('Value: ', '')
				self.assertEqual(attr_value, str(idle_timeout))
				found['idle_timeout'] = attr_value

			if line.startswith('      Value: 0000372a08060'):
				attr_value = line.replace('      Value: 0000372a08060', '')
				max_down = int(attr_value, 16)
				print(max_down)

			if line.startswith('      Value: 0000372a07060'):
				attr_value = line.replace('      Value: 0000372a07060', '')
				max_up = int(attr_value, 16)
				print(max_up)

		for key, value in found.items():
			self.assertNotEqual(value, None)

	# Realm
	def test_eap_realm_tls(self):
		config_file = '/test_configs/realm/eap-tls-realm.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_expired_eap_realm_tls(self):
		config_file = '/test_configs/realm/eap-tls-realm-expired.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	def test_eap_realm_ttls_pap(self):
		config_file = '/test_configs/realm/eap-ttls-pap-realm.conf'
		self.base_successful_test(config_file, self.RADIUS_IP_EAP_ONLY)()

	def test_wrong_eap_realm_ttls_pap(self):
		config_file = '/test_configs/realm/wrong-eap-ttls-pap-realm.conf'
		self.base_fail_test(config_file, self.RADIUS_IP_EAP_ONLY)()

	# EAP + ERAD Auth
	def test_eap_with_eradauth(self):
		config_file = '/test_configs/ttls-chap-with-erad-auth.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	# expired cert
	def test_expired_eap_tls(self):
		config_file = '/test_configs/expired-eap-tls.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	# local auth

	# do not forget put certificate ID in anonymous identity in eapol_config
	# this is needed to test, what freeradius logs real user name and not cert id
	def test_eap_tls(self):
		config_file = '/test_configs/eap-tls.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_pap(self):
		config_file = '/test_configs/ttls-pap.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_peap_mschapv2(self):
		config_file = '/test_configs/peap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_chap(self):
		config_file = '/test_configs/ttls-chap.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_mschapv2(self):
		config_file = '/test_configs/ttls-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_eapmd5(self):
		config_file = '/test_configs/ttls-eapmd5.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_eap_mschapv2(self):
		config_file = '/test_configs/ttls-eap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_peap_plus_erad_auth(self):
		config_file = '/test_configs/peap-erad-mac-auth.conf'
		additional_attrs_list = [
		'-N', '31:s:aa-bb-cc-dd-ee-ff'
		]
		self.base_successful_test(
			config_file,
			self.RADIUS_IP,
			self.testing_attrs,
			additional_attrs_list=additional_attrs_list)()

	# local auth wrong credentials
	# bad certificate or not trusted by CA.crt configured in freeradius config
	def test_eap_tls_wrong_credentials(self):
		config_file = '/test_configs/wrong-credentials/wrong-creds-eap-tls.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	# certificate trusted by CA.crt configured in freeradius config
	# but user with this certificate should not be found in ERAD API DB
	def test_eap_tls_wrong_credentials2(self):
		config_file = '/test_configs/wrong-credentials/wrong-creds-eap-tls-2.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	def test_peap_mschapv2_wrong_credentials(self):
		config_file = '/test_configs/wrong-credentials/wrong-creds-peap-mschapv2.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	def test_ttls_pap_wrong_credentials(self):
		config_file = '/test_configs/wrong-credentials/wrong-creds-ttls-pap.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	def test_ttls_chap_wrong_credentials(self):
		config_file = '/test_configs/wrong-credentials/wrong-creds-ttls-chap.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	def test_ttls_mschapv2_wrong_credentials(self):
		config_file = '/test_configs/wrong-credentials/wrong-creds-ttls-mschapv2.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	def test_ttls_eapmd5_wrong_credentials(self):
		config_file = '/test_configs/wrong-credentials/wrong-creds-ttls-eapmd5.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	def test_ttls_eap_mschapv2_wrong_credentials(self):
		config_file = '/test_configs/wrong-credentials/wrong-creds-ttls-eap-mschapv2.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()


	# local auth wrong anonymous identity
	# should authorize user but in logs will be real identity
	def test_peap_mschapv2_wrong_anon_credentials(self):
		config_file = '/test_configs/wrong-anon-credentials/wrong-anon-peap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_pap_wrong_anon_credentials(self):
		config_file = '/test_configs/wrong-anon-credentials/wrong-anon-ttls-pap.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_chap_wrong_anon_credentials(self):
		config_file = '/test_configs/wrong-anon-credentials/wrong-anon-ttls-chap.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_mschapv2_wrong_anon_credentials(self):
		config_file = '/test_configs/wrong-anon-credentials/wrong-anon-ttls-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_eapmd5_wrong_anon_credentials(self):
		config_file = '/test_configs/wrong-anon-credentials/wrong-anon-ttls-eapmd5.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_eap_mschapv2_wrong_anon_credentials(self):
		config_file = '/test_configs/wrong-anon-credentials/wrong-anon-ttls-eap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	# remote auth
	def test_proxy_remote_auth_peap_mschapv2(self):
		config_file = '/test_configs/proxy/proxy-creds-peap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()


	def test_proxy_remote_auth_ttls_pap(self):
		config_file = '/test_configs/proxy/proxy-creds-ttls-pap.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()


	def test_proxy_remote_auth_ttls_chap(self):
		config_file = '/test_configs/proxy/proxy-creds-ttls-chap.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()


	def test_proxy_remote_auth_ttls_mschapv2(self):
		config_file = '/test_configs/proxy/proxy-creds-ttls-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()


	def test_proxy_remote_auth_ttls_eapmd5(self):
		config_file = '/test_configs/proxy/proxy-creds-ttls-eapmd5.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()

	def test_proxy_remote_auth_ttls_eap_mschapv(self):
		config_file = '/test_configs/proxy/proxy-creds-ttls-eap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()

	# Uses ERAD authentication (mac authenticate), real identity in anonymous identity field.
	# Real identity should not convert request from eap to "simple" - reply should contain "EAP-Message" attribute
	def test_peap_plus_erad_auth_notanon_identity(self):
		config_file = '/test_configs/peap-erad-mac-auth-notanon-identity.conf'
		additional_attrs_list = [
		'-N', '31:s:aa-aa-aa-aa-aa-aa'
		]
		self.base_successful_test(
			config_file,
			self.RADIUS_IP,
			self.testing_home_server_attrs,
			additional_attrs_list=additional_attrs_list,
			nas_id='11:22:33:44:55:67')()


	# remote auth wrong cred
	def test_proxy_remote_auth_peap_mschapv2_wrong_creds(self):
		config_file = '/test_configs/proxy/wrong-credentials/wrong-proxy-creds-peap-mschapv2.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()


	def test_proxy_remote_auth_ttls_pap_wrong_creds(self):
		config_file = '/test_configs/proxy/wrong-credentials/wrong-proxy-creds-ttls-pap.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()


	def test_proxy_remote_auth_ttls_chap_wrong_creds(self):
		config_file = '/test_configs/proxy/wrong-credentials/wrong-proxy-creds-ttls-chap.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()


	def test_proxy_remote_auth_ttls_mschapv2_wrong_creds(self):
		config_file = '/test_configs/proxy/wrong-credentials/wrong-proxy-creds-ttls-mschapv2.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()


	def test_proxy_remote_auth_ttls_eapmd5_wrong_creds(self):
		config_file = '/test_configs/proxy/wrong-credentials/wrong-proxy-creds-ttls-eapmd5.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()


	def test_proxy_remote_auth_ttls_eap_mschapv_wrong_creds(self):
		config_file = '/test_configs/proxy/wrong-credentials/wrong-proxy-creds-ttls-eap-mschapv2.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()

	# remote auth wrong anonymous identity
	# should authorize user but in logs will be real identity
	def test_proxy_remote_auth_peap_mschapv2_wrong_anon_creds(self):
		config_file = '/test_configs/proxy/wrong-anon-credentials/wrong-anon-proxy-creds-peap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()


	def test_proxy_remote_auth_ttls_pap_wrong_anon_creds(self):
		config_file = '/test_configs/proxy/wrong-anon-credentials/wrong-anon-proxy-creds-ttls-pap.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()


	def test_proxy_remote_auth_ttls_chap_wrong_anon_creds(self):
		config_file = '/test_configs/proxy/wrong-anon-credentials/wrong-anon-proxy-creds-ttls-chap.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()


	def test_proxy_remote_auth_ttls_mschapv2_wrong_anon_creds(self):
		config_file = '/test_configs/proxy/wrong-anon-credentials/wrong-anon-proxy-creds-ttls-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()


	def test_proxy_remote_auth_ttls_eapmd5_wrong_anon_creds(self):
		config_file = '/test_configs/proxy/wrong-anon-credentials/wrong-anon-proxy-creds-ttls-eapmd5.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()


	def test_proxy_remote_auth_ttls_eap_mschapv_wrong_anon_creds(self):
		config_file = '/test_configs/proxy/wrong-anon-credentials/wrong-anon-proxy-creds-ttls-eap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_remote_attrs)()


	# By default home radius server instance configured to always return hardcoded vlan id value and other attrs,
	# see `erad/srcerad/frmod/remove_proxy.py`.
	# This test is check, that these attributes from home server will be in final reply.
	# erad/src/erad/api/schema/__init__.py create additional group `fooremoteattr`, for this tests.
	def test_attr_from_home_server(self):
		config_file = '/test_configs/proxy/proxy-creds-home-srv-attr-peap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_home_server_attrs, nas_id='fooremoteattr')()


	# test requests with real identity in anonymous identity field
	# expected what this request will be redirected to home server without processing by inner tunnel virtual site

	def test_proxy_eap_only_remote_auth_peap_mschapv2(self):
		config_file = '/test_configs/proxy-full-eap/proxy-creds-full-eap-peap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP_EAP_ONLY, self.testing_remote_fulleap_attrs)()


	def test_proxy_eap_only_remote_auth_ttls_pap(self):
		config_file = '/test_configs/proxy-full-eap/proxy-creds-full-eap-ttls-pap.conf'
		self.base_successful_test(config_file, self.RADIUS_IP_EAP_ONLY, self.testing_remote_fulleap_attrs)()


	def test_proxy_eap_only_remote_auth_ttls_chap(self):
		config_file = '/test_configs/proxy-full-eap/proxy-creds-full-eap-ttls-chap.conf'
		self.base_successful_test(config_file, self.RADIUS_IP_EAP_ONLY, self.testing_remote_fulleap_attrs)()


	def test_proxy_eap_only_remote_auth_ttls_mschapv2(self):
		config_file = '/test_configs/proxy-full-eap/proxy-creds-full-eap-ttls-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP_EAP_ONLY, self.testing_remote_fulleap_attrs)()


	def test_proxy_eap_only_remote_auth_ttls_eapmd5(self):
		config_file = '/test_configs/proxy-full-eap/proxy-creds-full-eap-ttls-eapmd5.conf'
		self.base_successful_test(config_file, self.RADIUS_IP_EAP_ONLY, self.testing_remote_fulleap_attrs)()

	def test_proxy_eap_only_remote_auth_ttls_eap_mschapv(self):
		config_file = '/test_configs/proxy-full-eap/proxy-creds-full-eap-ttls-eap-mschapv2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP_EAP_ONLY, self.testing_remote_fulleap_attrs)()


	# test requests with real identity in anonymous identity field and wrong password
	# expected what this request will be redirected to home server without processing by inner tunnel virtual site
	# and then will be rejected

	def test_proxy_eap_only_wrong_creds_remote_auth_peap_mschapv2(self):
		config_file = '/test_configs/proxy-full-eap/wrong-credentials/wrong-proxy-creds-full-eap-peap-mschapv2.conf'
		self.base_fail_test(config_file, self.RADIUS_IP_EAP_ONLY)()

	def test_proxy_eap_only_wrong_creds_remote_auth_ttls_pap(self):
		config_file = '/test_configs/proxy-full-eap/wrong-credentials/wrong-proxy-creds-full-eap-ttls-pap.conf'
		self.base_fail_test(config_file, self.RADIUS_IP_EAP_ONLY)()

	def test_proxy_eap_only_wrong_creds_remote_auth_ttls_chap(self):
		config_file = '/test_configs/proxy-full-eap/wrong-credentials/wrong-proxy-creds-full-eap-ttls-chap.conf'
		self.base_fail_test(config_file, self.RADIUS_IP_EAP_ONLY)()

	def test_proxy_eap_only_wrong_creds_remote_auth_ttls_mschapv2(self):
		config_file = '/test_configs/proxy-full-eap/wrong-credentials/wrong-proxy-creds-full-eap-ttls-mschapv2.conf'
		self.base_fail_test(config_file, self.RADIUS_IP_EAP_ONLY)()

	def test_proxy_eap_only_wrong_creds_remote_auth_ttls_eapmd5(self):
		config_file = '/test_configs/proxy-full-eap/wrong-credentials/wrong-proxy-creds-full-eap-ttls-eapmd5.conf'
		self.base_fail_test(config_file, self.RADIUS_IP_EAP_ONLY)()

	def test_proxy_eap_only_wrong_creds_remote_auth_ttls_eap_mschapv(self):
		config_file = '/test_configs/proxy-full-eap/wrong-credentials/wrong-proxy-creds-full-eap-ttls-eap-mschapv2.conf'
		self.base_fail_test(config_file, self.RADIUS_IP_EAP_ONLY)()

	# test supported TLS versions
	def test_ttls_eap_mschapv2_tls10(self):
		config_file = '/test_configs/ttls-eap-mschapv2_tls1.0.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_eap_mschapv2_tls11(self):
		config_file = '/test_configs/ttls-eap-mschapv2_tls1.1.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_eap_mschapv2_tls12(self):
		config_file = '/test_configs/ttls-eap-mschapv2_tls1.2.conf'
		self.base_successful_test(config_file, self.RADIUS_IP, self.testing_attrs)()

	def test_ttls_eap_mschapv2_tls13(self):
		config_file = '/test_configs/ttls-eap-mschapv2_tls1.3.conf'
		self.base_fail_test(config_file, self.RADIUS_IP)()
