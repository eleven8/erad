#!/bin/bash
set -x

# for jenkins
# if command not available by standrad $PATH
# make alias
docker-compose -v
if [[ $? -ne 0 ]]; then
    alias docker-compose="/usr/sbin/docker-compose"
fi

docker-compose build erad-static
docker-compose run --rm --entrypoint=./erad-static-testing-entrypoint.sh erad-static
docker-compose down
