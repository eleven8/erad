#!/bin/bash

set -x

# # uncomment these lines if you run tests on new ec2 instance
# # -------------------------
# PATH=/usr/local/bin:$PATH
# yum -y update
# yum -y install docker
# service docker start
# curl -O https://bootstrap.pypa.io/get-pip.py
# python get-pip.py
# pip install docker-compose pyopenssl
# # -------------------------

# for jenkins
# if command not available by standard $PATH
# make alias
docker-compose -v
if [[ $? -ne 0 ]]; then
    alias docker-compose="/usr/sbin/docker-compose"
fi

teardown(){
	docker-compose down
	docker image prune -f
	exit 1
}

docker-compose down
if ! docker-compose build \
	dynamodb-localhost \
	erad-webservice ; then
	teardown
fi

# In Jenkins environment there is a possibility that two or more tests will be running simultaneously.
# If some service inside docker will expose same ports, then containers will interfere with each other.
# To prevent this, for tests inside jenkins is used another docker-compose file. It repeat main docker-compose
# file, except it not expose ports

if [ ! -z $WORKSPACE ]; then
    COMPOSE_FILE='-f jenkins-env-docker-compose.yaml'
else
    COMPOSE_FILE=''
fi

docker-compose $COMPOSE_FILE up -d dynamodb-localhost
sleep 10
docker-compose $COMPOSE_FILE up -d erad-webservice


echo waiting 60 seconds to up erad-webservice
sleep 60

LOGS=$(docker-compose logs erad-webservice)

MAGIC_STATUS=$(echo "$LOGS" | grep -oP 'ERAD-WEBSERVICE READY FOR SERVE$' || echo '')

echo -n 'Checking erad-webservice status'
if [ -n "$MAGIC_STATUS" ]; then
echo ' ERAD-WEBSERCIE - OK!'; else echo ' Fail! Try to increase sleep pasue'; docker-compose down && exit 1; fi;

# run default tests
if ! docker-compose exec -T erad-webservice python3 -m nose --with-xunit \
	--xunit-file=/usr/local/nosetests.xml \
	-s \
	/usr/local/webservices/erad/api/test/ \
	/usr/local/src/erad/api/tests_teardown.py \
	> erad-docs.yaml ; then
	teardown
fi

if [ ! -z $WORKSPACE ]; then
	if ! docker-compose exec -T erad-webservice cat /usr/local/nosetests.xml > $WORKSPACE/nosetests.xml ; then
		teardown
	fi

	if ! docker-compose logs dynamodb-localhost &> $WORKSPACE/dynamo.log ; then
		teardown
	fi
	touch $WORKSPACE/dynamo_error.log

	if ! docker-compose logs erad-webservice &> $WORKSPACE/webservice_error.log ; then
		teardown
	fi
	touch $WORKSPACE/webservice.log
fi;

docker-compose down
docker image prune -f
