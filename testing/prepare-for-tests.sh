#/bin/bash

set -e
set -x


# uncomment these lines if you run tests on new ec2 instance
# -------------------------
# PATH=/usr/local/bin:$PATH
# yum -y update
# yum -y install docker
# service docker start
# curl -O https://bootstrap.pypa.io/get-pip.py
# python get-pip.py
# pip install docker-compose pyopenssl
# ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
# -------------------------
FRMOD_DIR=/erad-src/erad/frmod
RADDB_DIR=/usr/local/etc/raddb

# Not secure!!!
# Use only for local testing.
# During tests radius-proxy and other contaiers mounting rardb directory from host.
# To be able edit configuration and use it inside container passing current user uid inside
# container to create user with same uid.
radius_user_uid=$(stat -c '%u' .)
docker-compose build --build-arg RADIUSD_USER_UID="$radius_user_uid" radius-proxy


mkdir -p tests_root
cp -r ../src/erad/frmod tests_root/proxy-srv/
cp -r ../src/erad/frmod tests_root/proxy-srv-for-eap-only/
cp -r ../src/erad/frmod tests_root/home-srv/
cp -r ../src/erad/frmod tests_root/home-eaponly-srv/

docker-compose run --entrypoint="/bin/bash" radius-proxy -c "\
mkdir -p $RADDB_DIR/sites-enabled;
python3 $FRMOD_DIR/create_test_virtual_site.py default > $RADDB_DIR/sites-enabled/testing-stie;\
python3 $FRMOD_DIR/create_test_eap_modules_sites.py $RADDB_DIR/mods-enabled/ $RADDB_DIR/sites-enabled/;\
python3 $FRMOD_DIR/set_local_proxy_settings.py default $RADDB_DIR/sites-enabled/testing-stie; \
python3 $FRMOD_DIR/set_local_proxy_settings.py default $RADDB_DIR/sites-enabled/peap_and_ttls; \
chown -R radiusd:radiusd $RADDB_DIR/sites-enabled; \
chown -R radiusd:radiusd $RADDB_DIR/mods-config/; \
chown -R radiusd:radiusd $RADDB_DIR/certs/; \
chown -R radiusd:radiusd $RADDB_DIR/mods-enabled;
"

docker-compose run --entrypoint="/bin/bash" radius-proxy-for-eap-only -c "\
mkdir -p $RADDB_DIR/sites-enabled;
python3 $FRMOD_DIR/create_test_virtual_site.py default > $RADDB_DIR/sites-enabled/testing-stie;\
python3 $FRMOD_DIR/create_test_eap_modules_sites.py $RADDB_DIR/mods-enabled/ $RADDB_DIR/sites-enabled/;\
python3 $FRMOD_DIR/set_local_proxy_settings.py onlyeap $RADDB_DIR/sites-enabled/testing-stie; \
python3 $FRMOD_DIR/set_local_proxy_settings.py onlyeap $RADDB_DIR/sites-enabled/peap_and_ttls; \
chown -R radiusd:radiusd $RADDB_DIR/sites-enabled; \
chown -R radiusd:radiusd $RADDB_DIR/mods-config/; \
chown -R radiusd:radiusd $RADDB_DIR/certs/; \
chown -R radiusd:radiusd $RADDB_DIR/mods-enabled;
"

docker-compose run --entrypoint="/bin/bash" radius-home -c "\
mkdir -p $RADDB_DIR/sites-enabled;
python3 $FRMOD_DIR/create_test_virtual_site.py default > $RADDB_DIR/sites-enabled/testing-stie; \
python3 $FRMOD_DIR/create_test_eap_modules_sites.py $RADDB_DIR/mods-enabled/ $RADDB_DIR/sites-enabled/
python3 $FRMOD_DIR/remove_proxy.py $RADDB_DIR/sites-enabled/testing-stie; \
python3 $FRMOD_DIR/remove_proxy.py $RADDB_DIR/sites-enabled/peap_and_ttls; \
chown -R radiusd:radiusd $RADDB_DIR/sites-enabled; \
chown -R radiusd:radiusd $RADDB_DIR/mods-config/; \
chown -R radiusd:radiusd $RADDB_DIR/certs/; \
chown -R radiusd:radiusd $RADDB_DIR/mods-enabled;
"

docker-compose run --entrypoint="/bin/bash" radius-eaponly-home -c "\
mkdir -p $RADDB_DIR/sites-enabled;
python3 $FRMOD_DIR/create_test_virtual_site.py onlyeap > $RADDB_DIR/sites-enabled/testing-stie; \
python3 $FRMOD_DIR/create_test_eap_modules_sites.py $RADDB_DIR/mods-enabled/ $RADDB_DIR/sites-enabled/
python3 $FRMOD_DIR/remove_proxy.py $RADDB_DIR/sites-enabled/testing-stie; \
python3 $FRMOD_DIR/remove_proxy.py $RADDB_DIR/sites-enabled/peap_and_ttls; \
chown -R radiusd:radiusd $RADDB_DIR/sites-enabled; \
chown -R radiusd:radiusd $RADDB_DIR/mods-config/; \
chown -R radiusd:radiusd $RADDB_DIR/certs/; \
chown -R radiusd:radiusd $RADDB_DIR/mods-enabled;
"

rm -f tests_root/proxy-srv/raddb/proxy.conf
rm -f tests_root/proxy-srv-for-eap-only/raddb/proxy.conf
cp tests_root/proxy-srv/raddb/dev.proxy.conf tests_root/proxy-srv/raddb/proxy.conf
cp tests_root/proxy-srv/raddb/dev.proxy.conf tests_root/proxy-srv-for-eap-only/raddb/proxy.conf


