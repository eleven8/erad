import argparse
from random import randint


def get_mac() -> str:
	return ':'.join([f'{randint(0, 255):02x}' for x in range(6)])


def gen_cmdline(mac_addr: str = None, radius_secret: str = None, ip_addr: str = None, port: str = None) -> str:
	return f'eapol_test -c /test_configs/peap-mschapv2.conf -M {mac_addr} -s {radius_secret} -a  {ip_addr} -p {port} -N 30:s:elevenos -N 32:s:foo >> /dev/null\n'


def create_file(filename: str, num: int, **kwargs) -> None:
	with open(filename, 'w') as f_write:
		for x in range(num):
			mac_addr = get_mac()
			f_write.writelines(gen_cmdline(mac_addr=mac_addr, **kwargs))


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("filename", help="Filename to write", type=str)
	parser.add_argument("number", help="Number of records to generate", type=int)
	parser.add_argument("ip_addr", help="Radius server ip address", type=str)
	parser.add_argument("-radius_secret", help="Radius shared secret", type=str, default='4905DVCA3B')
	parser.add_argument("-port", help="Radius server port", type=str, default='1812')
	args = parser.parse_args()

	parameters = {
		'radius_secret': args.radius_secret,
		'ip_addr': args.ip_addr,
		'port': args.port
	}
	create_file(filename=args.filename, num=args.number, **parameters)


if __name__ == '__main__':
	main()
