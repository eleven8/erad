#!/usr/bin/env bash

pkill -f "python ./spawn_api_server.py"
pkill -f "bash /usr/local/src/tools/freeradius_launcher/freeradius_launcher.sh"
pkill celery
pkill redis-server
pkill java
