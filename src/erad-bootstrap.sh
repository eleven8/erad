set -e
set -x

parse_global_config() {
    for file in boto.cfg erad.cfg; do
        sed -i "s:\##AWS_ACCESS_KEY_ID##:${AWS_ACCESS_KEY_ID}:g" ${file}
        sed -i "s:\##AWS_SECRET_ACCESS_KEY##:${AWS_SECRET_ACCESS_KEY}:g" ${file}
        sed -i "s:\##AWS_DEFAULT_REGION##:${AWS_DEFAULT_REGION}:g" ${file}
        sed -i "s:\##SYSTEM_KEY##:${SYSTEM_KEY}:g" ${file}
        sed -i "s:\##SITEWIDE_SALT##:${SITEWIDE_SALT}:g" ${file}
        sed -i "s:\##DEPLOY_REGION##:${DEPLOY_REGION}:g" ${file}
    done
}


SOURCE=/usr/local/src
TARGET=/usr/local/webservices
# deploy wsgi file
# TODO change to volume bind
mkdir -p ${TARGET}/erad
cp ${SOURCE}/erad/api/api.wsgi ${TARGET}/api.wsgi
cp ${SOURCE}/erad/api/statuscheck.wsgi ${TARGET}/statuscheck.wsgi
cp -rf ${SOURCE}/erad ${TARGET}

# deploy configuration for apache
cp ${SOURCE}/erad/api/erad.conf /etc/httpd/conf.d/

#docker containers don't have ec2-users, removing
sed -i 's/user=ec2-user group=ec2-user//g' /etc/httpd/conf.d/erad.conf

# copy swagger-ui to the apache root
# curl https://codeload.github.com/swagger-api/swagger-ui/tar.gz/v2.2.3 \
# | tar xzf - -C /var/www/html/
#     cp -r /var/www/html/swagger-ui-2.2.3/dist/* /var/www/html
#     rm -rf /var/www/html/swagger-ui-2.2.3/
#     sed -i 's/http:\/\/petstore.swagger.io\/v2\/swagger.json/erad-docs.yaml/g' /var/www/html/index.html


export PATH=$PATH:${TARGET}
export PYTHONPATH=$PATH:/usr/local/sbin

# copy files to the proper location
cp ${SOURCE}/create_database.py ${TARGET}
cp ${SOURCE}/remove_database.py ${TARGET}
cp ${SOURCE}/spawn_api_server.py ${TARGET}
cp ${SOURCE}/api_unit_testing.sh /usr/bin

# install development .cfg files
cp ${SOURCE}/enable_streams.py ${TARGET}
cp ${SOURCE}/erad/api/boto.cfg.dev /etc/boto.cfg
cp ${SOURCE}/erad/api/erad.cfg.dev /etc/erad.cfg
chmod 644 /etc/erad.cfg
pushd /etc
parse_global_config
popd

pushd ${TARGET}/
# create a blank development database
python3 ./create_database.py
# enable streams
python3 ./enable_streams.py

# run the web service in the background (on the dev port of 5000)
python3 ./spawn_api_server.py &

# In addition in this container main ERAD api process running as wsgi application
# it is started by httpd


popd

pushd ${TARGET}/erad/api
/usr/local/bin/celery worker \
	-A tasks.celery \
	--loglevel=info \
	--beat  \
	--logfile="${TARGET}/%n.log"  &

redis-server &

echo 'ERAD-WEBSERVICE READY FOR SERVE'
/usr/sbin/httpd -DFOREGROUND


