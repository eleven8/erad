#!/usr/bin/env bash

export SNS_REGION='us-east-1'
export TOPIC_ARN='arn:aws:sns:us-east-1:439838887486:code-yellow'
export TOPIC_ARN_NORMAL='arn:aws:sns:us-east-1:439838887486:trouble'
export PEN_RADIUS_WATCHER_PROBE_INTERVAL=20
export MAX_DOWNTIME=30 # max time in seconds a server has to respond to a ping test

