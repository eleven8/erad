import os
import subprocess

import boto3

SNS_AWS_ACCESS_KEY_ID = os.environ['SNS_AWS_ACCESS_KEY_ID']
SNS_AWS_SECRET_ACCESS_KEY = os.environ['SNS_AWS_SECRET_ACCESS_KEY']
SNS_REGION = os.environ['SNS_REGION']

TOPIC_ARN = os.environ['TOPIC_ARN']
TOPIC_ARN_NORMAL = os.environ['TOPIC_ARN_NORMAL']


class SNSWrapper():
    def __init__(self):
        self.sns_session = boto3.session.Session(
                aws_access_key_id=SNS_AWS_ACCESS_KEY_ID,
                aws_secret_access_key=SNS_AWS_SECRET_ACCESS_KEY,
                region_name=SNS_REGION)

        self.sns = self.sns_session.resource('sns')
        self.topic_arn = TOPIC_ARN
        self.topic_arn_normal = TOPIC_ARN_NORMAL

    def publish_failure(self, message, subjecttext):
        try:
            response = self.sns.Topic(self.topic_arn).publish(
                    Subject=subjecttext,
                    Message=message)
            self.log_publish_response(response)
            return response
        #except botocore.exceptions.ClientError as e: Invalid topic, invalid aws_secret_access_key
        #botocore.exceptions.EndpointConnectionError problem with Region or aws_access_key_id
        except Exception as e:
            e_message = str(e)
            subprocess.call(['logger', 'pen-radius-monitor: SNS Error'])
            subprocess.call(['logger', 'pen-radius-monitor: {0}'.format(e_message)])
        return

    def publish_message(self, message, subjecttext):
        try:
            response = self.sns.Topic(self.topic_arn_normal).publish(
                    Subject=subjecttext,
                    Message=message)
            self.log_publish_response(response)
            return response
        #except botocore.exceptions.ClientError as e: Invalid topic, invalid aws_secret_access_key
        #botocore.exceptions.EndpointConnectionError problem with Region or aws_access_key_id
        except Exception as e:
            e_message = str(e)
            subprocess.call(['logger', 'pen-radius-monitor: SNS Error'])
            subprocess.call(['logger', 'pen-radius-monitor: {0}'.format(e_message)])
        return

    def log_publish_response(self, response):
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            messageId = response['MessageId']
            subprocess.call(['logger', 'pen-radius-monitor: SNS message was successfully published - {0}'.format(messageId)])
        else:
            subprocess.call(['logger', 'pen-radius-monitor: error! got {0} while publishing SNS message'
                      .format(response['ResponseMetadata']['HTTPStatusCode'])])
