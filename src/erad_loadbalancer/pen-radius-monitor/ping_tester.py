import os
import time

class PingTester():
    PING_CMD_BASE = "/usr/local/bin/radclient -x -r 1 "
    PING_CMD_5 = "/usr/local/bin/radclient -x -r 1 -t 5 "
    INPUT_DATA = "User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e"

    def __init__(self, subscriber, ips):
        self.servers = list(ips)
        self.subscribers = [subscriber]

    def set_command_base(self, command_base):
        self.PING_CMD_BASE = command_base
        return self

    def register(self, subscriber):
        self.subscribers.append(subscriber)

    def test_servers(self):
        should_notify = False
        removed_servers = []
        for server in self.servers:
            print(f'server debug line!!! {server}', )
            response = self._ping(server)
            if response != 0:
                # retrial
                time.sleep(float(os.environ['MAX_DOWNTIME']))
                response = self._ping(server, self.PING_CMD_5)
                if response != 0:
                    self.servers.remove(server) # is this ok?
                    removed_servers.append(server)
                    should_notify = True

        if should_notify:
            for subscriber in self.subscribers:
                subscriber.update("The following servers should be removed {0}".format(removed_servers), removed_servers)


    """
    Method to actually perform the ping command.
    """
    def _ping(self, server, cmd=PING_CMD_BASE):
        radclient_cmd = 'echo "{input_data}" | {cmd}{ip}:1814 auth V72QSZ2CE3'.format(input_data=self.INPUT_DATA, cmd=cmd, ip=server)
        print(radclient_cmd)
        response = os.system(radclient_cmd)
        return response

    def __str__(self):
        return "Ping Tester servers:\n{servers}\nPing Tester subscribers:\n{subscribers}".format(servers=self.servers,subscribers=self.subscribers)
