#!/usr/bin/env bash

set -e
set -x

APP_DIR=/opt/pen-radius-watcher

SCRIPT_CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

gpg --batch --yes --passphrase $T42_PASSPHRASE --output $SCRIPT_CURRENT_DIR/credentials.sh --decrypt $SCRIPT_CURRENT_DIR/credentials.sh.secure

yum install -y gcc python3-devel python3 libtalloc

# Create Virtualenv for pen-radius-monitor script and install dependencies
rm -rf $APP_DIR && mkdir -p $APP_DIR
python3 -m venv $APP_DIR
cp -R $SCRIPT_CURRENT_DIR/* $APP_DIR

source $APP_DIR/bin/activate
pip3 install -r $APP_DIR/requirements.txt
source $APP_DIR/credentials.sh
source $APP_DIR/settings.sh
exec $APP_DIR/bin/python3 $APP_DIR/pen-radius-monitor.py&
