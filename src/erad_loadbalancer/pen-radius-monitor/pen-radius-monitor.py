import os
import time

from pen_handler import PenHandler
from ping_tester import PingTester
from sns_wrapper import SNSWrapper

"""
This class will correct pen calls removing unresponsive servers from it.
"""
class PenRadiusMonitor():

    def __init__(self):
        self.pen_handler = PenHandler()
        self.ping_tester = PingTester(self, self.pen_handler.ips)
        self.sns_wrapper = SNSWrapper()
        self.ip_with_port = {}  # removed ips with ports as a list will be stored here

    def update(self, message, removed_servers):
        self.remove_servers(removed_servers)

    def remove_server(self, server):
        self.pen_handler.remove_server(server)

    def remove_servers(self, servers):
        for server in servers:
            # check if server is acct or auth
            if(self.pen_handler._get_ports_and_ip(server)):
                message = " The {0} account server has been detected with some connectivity issue.".format(server)
                self.sns_wrapper.publish_message(message, 'Accounting server is struggling')
            # if auth server remove it from the pool
            else:
                self.ip_with_port.update(self.pen_handler._retrieve_ips_with_ports(server))
                self.remove_server(server)
                message = " The {0} radius server has been removed from the load balancer pool due to loss of connectivity.".format(server)
                self.sns_wrapper.publish_failure(message, 'Radius server failure')
        self.pen_handler.commit() # applies changes

    def start_test(self):
        if len(self.pen_handler.ips) == 0:
            self.pen_handler.refresh()  # anything new?
            if len(self.pen_handler.ips) > 0:
                self.ping_tester = PingTester(self, self.pen_handler.ips) # get that into a method.
        self.ping_tester.test_servers()
        # Check removed server if they started working again
        if len(list(self.ip_with_port.keys())) > 0:
            for ip, ports in list(self.ip_with_port.items()):
                if self.ping_tester._ping(ip) == 0:
                    self.pen_handler.add_pen(ip, ports)
                    message = " The {0} radius server has been added back to the load balancer pool.".format(ip)
                    self.sns_wrapper.publish_message(message, 'Radius server recovered')
                    del self.ip_with_port[ip]
                    self.pen_handler.ips = []


    """
    Starts the main loop of the watcher
    """
    def run(self, sleep_time):
        while True:
            self.start_test()
            time.sleep(sleep_time)


if __name__ == '__main__':
    PROBE_INTERVAL = int(os.environ['PEN_RADIUS_WATCHER_PROBE_INTERVAL'])
    prm = PenRadiusMonitor()
    prm.run(PROBE_INTERVAL or 5)
