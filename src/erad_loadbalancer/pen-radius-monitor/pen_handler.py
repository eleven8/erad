import psutil
import os


"""
This class will handle pen related operations.
It should keep all pen processes information.
"""
class PenHandler():
    DESIRED_ATTR = ['pid', 'name', 'exe', 'cmdline', 'username']

    def __init__(self):
        self.pen_processes = self._retrieve_pen_processes()
        self.pids = self._get_pids_by_ip()
        self.ips = self._retrieve_ips()
        self.removed_servers = []

    def _retrieve_pen_processes(self):
        pen_processes = []
        for p in psutil.process_iter():
            if p.name() == "pen":
                pen_processes.append(p.as_dict(attrs=PenHandler.DESIRED_ATTR))

        return pen_processes

    def _retrieve_ips(self):
        ips = set()
        for process in self.get_current_pen_processes():
            for ip_port in self._extract_ip_from_cmd(process['cmdline']):
                ips.add(ip_port.split(':')[0])
        return ips

    def _retrieve_ips_with_ports(self, server):
        ip_ports = {}
        pen_processes = self.pen_processes

        for process in pen_processes:
            for ip_port in self._extract_ip_from_cmd(process['cmdline']):
                ip = ip_port.split(':')[0] # IP:PORT so this split will leave only the IP
                port = ip_port.split(':')[1]
                if ip == server:
                    if ip in ip_ports:
                        if port not in ip_ports[ip]:
                            ip_ports[ip].append(port)
                    else:
                        ip_ports[ip] = [port]

        return ip_ports


    def get_current_pen_processes(self):
        return self._retrieve_pen_processes()

    """
    Kills processes if needed and starts them correctly.
    """
    def commit(self):
        if self.has_changed(): # The system is different from what the PenHandler holds.
            pids_to_commit = set() # Set of pids that need to change.
            for removed_server in self.removed_servers: # for every ip on the removed servers there must be a pid added to the set
                for pid in self.pids[removed_server]: # These pids will be updated, but I am using a set for more than one server could be removed from the same pid
                    pids_to_commit.add(pid) # This will add the pid that needs to change to the set

            # now I will iterate over the pids that need to change and kill them and restart pen with my new self.pen_processes matching those same pids.
            # its important to refresh everything after that
            for pid in pids_to_commit:
                self.kill_pen_process(pid)

            # for the second part of getting the processes up again
            for pid in pids_to_commit:
                self.start_pen(pid)

            self.refresh() # refreshes the object so it reflects how the system is
        pass

    def start_pen(self, pid):
        if len(self.ips) > 0:
            for p in self.pen_processes: # these are the uncommited and changed processes.
                if p['pid'] == pid: # if this is the updated process..
                    os.system(' '.join(str(x) for x in p['cmdline']))  #  execute new comdline

    """
    The system differs from what the PenHandler instance holds?
    """
    def has_changed(self):
        if len(self.removed_servers) > 0:
            return True
        return False

    """
    Refreshes values from the system.
    """
    def refresh(self):
        self.pen_processes = self._retrieve_pen_processes()
        self.ips = self._retrieve_ips()
        self.pids = self._get_pids_by_ip()
        self.removed_servers = []


    def kill_pen_process(self, pid):
        psutil.Process(pid).terminate()

    """
    This will iterate over all the IPs from the command and return them in a list.
    """
    def _extract_ip_from_cmd(self, cmd):
        ips = []
        for ip in cmd[6:]:
            ips.append(ip)
        return ips

    """
    This will iterate over all the IPs and Ports and determine if the pass server is Auth or Acct
    """
    def _get_ports_and_ip(self, ipaddress):
        ports_and_ip = {}
        pen_processes = self.pen_processes

        for process in pen_processes:
            for ip_port in self._extract_ip_from_cmd(process['cmdline']):
                ip = ip_port.split(':')[0] # IP:PORT so this split will leave only the IP
                ports_and_ip[ip] = int(ip_port.split(':')[1])
        if ipaddress in ports_and_ip:
            # check if the passed server is Accounting server and return True
            if(ports_and_ip[ipaddress]%2 == 1):
                return True
            else:
                return False

    """
    This will create a dictionary where the keys are the servers and the value is a list of associated processes with that particular server ip.
    """
    def _get_pids_by_ip(self):
        processes_by_ip = {}
        pen_processes = self.pen_processes

        for process in pen_processes:
            for ip_port in self._extract_ip_from_cmd(process['cmdline']):
                ip = ip_port.split(':')[0] # IP:PORT so this split will leave only the IP
                if ip in list(processes_by_ip.keys()):
                    processes_by_ip[ip].append(process['pid'])
                else:
                    processes_by_ip[ip] = [process['pid']]

        return processes_by_ip

    """
    This will remove the given ip_server from a list of pen processes.
    It will return a list of mockup processes as dictionaries that may be executed.
    """
    def remove_server(self, ip_server):
        num_of_servers = len(self.ips)
        if ip_server not in self.ips or num_of_servers <= 1:
            return
        current_pen_processes = self.pen_processes
        pids_by_ip = self._get_pids_by_ip()
        new_pen_processes = []

        for process in current_pen_processes: # list of processes
            if process['pid'] in pids_by_ip[ip_server]: # if that process is a processes that has the given ip in it
                for ip in process['cmdline'][6:]:
                    if ip_server in ip:
                        process['cmdline'].remove(ip)
            new_pen_processes.append(process)

        self.pen_processes = new_pen_processes
        self.ips.remove(ip_server)
        self.removed_servers.append(ip_server)
        return self.pen_processes


    """
    Add server to the pen if they are healthy again.
    """
    def add_pen(self, ip, ports):
        for port in ports:
            os.system("/usr/local/bin/pen -x 15 -hr -U {port} {ip}:{port}".format(port=port, ip=ip))


if __name__ == "__main__":
    handler = PenHandler()
#    for p in handler.pen_processes:
#        print p
#
#
#    print "\n\n"
#
#    for ip in handler.ips:
#        print ip
#
#    handler.remove_server("172.31.5.224")
#
#    print "\n remved server\n"
#    for ip in handler.ips:
#        print ip
#
#    print "\n removed server process\n"
#    for p in handler.pen_processes:
#        print p
#
#    print "\n but actually processes are:\n"
#    for p in handler.get_current_pen_processes():
#        print p
#
#    handler.refresh()
#    print "\n after reset\n"
#
#    for ip in handler.ips:
#        print ip
#
#    print "\n\n"
#    for p in handler.pen_processes:
#        print p
#
#    print handler.has_changed()
#    handler.commit()
