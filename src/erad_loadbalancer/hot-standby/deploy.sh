#!/usr/bin/env bash

set -e

if [ $# -eq 0 ]
  then
    echo "Usage: $0 [MASTER_NODE_IP]"
    exit 1
fi

APP_DIR=/opt/hot-standby
TARGET_IP=$1


# will give the full directory name of the script no matter where it is being called from
SCRIPT_CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# decrypt credentials if on production
gpg --batch --yes --passphrase $T42_PASSPHRASE --output $SCRIPT_CURRENT_DIR/credentials.sh --decrypt $SCRIPT_CURRENT_DIR/credentials.sh.secure

# copy correct region-specific settings file to settings.sh
read Region <<< $(curl -s 'http://169.254.169.254/latest/dynamic/instance-identity/document' | python -c "import sys, json; output=json.load(sys.stdin); print output['region']")
cp $SCRIPT_CURRENT_DIR/settings-$Region.sh $SCRIPT_CURRENT_DIR/settings.sh

# Create Virtualenv for hot-standby script and install dependencies
yum install -y python-virtualenv

rm -rf $APP_DIR && mkdir -p $APP_DIR
virtualenv $APP_DIR
cp -R $SCRIPT_CURRENT_DIR/* $APP_DIR

$APP_DIR/bin/pip install -r $APP_DIR/requirements.txt

# run the script
source $APP_DIR/credentials.sh
source $APP_DIR/settings.sh
$APP_DIR/bin/python $APP_DIR/hot-standby.py $TARGET_IP & disown
