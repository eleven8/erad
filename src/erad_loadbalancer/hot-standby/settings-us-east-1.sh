#!/usr/bin/env bash

export EC2_REGION='us-east-1'
export SNS_REGION='us-east-1'

export TOPIC_ARN='arn:aws:sns:us-east-1:439838887486:code-yellow'
export ELASTIC_IP_ALLOCATION_ID='eipalloc-f5d277cb'

# Grace period before triggering the hot-standby procedure
export GRACE_PERIOD=30
# Timeout period before declaring MASTER node dead...
export TIMEOUT=20
# Ping timeout period, in seconds
export PING_TIMEOUT=1
# Interval between server probes, in seconds
export PROBE_INTERVAL=5

export JENKINS_URL='https://ci.gd1.io/buildByToken/build'
export JENKINS_JOB='__erad-REDEPLOY-production-radius-useast-01-0001__'
export JENKINS_TOKEN='a2qxNzBNAqs5SrkcGmD4gnmqJEDvcerq'

export RADIUS_PORT=1814
export RADIUS_SECRET='V72QSZ2CE3'
export RADIUS_TIMEOUT=3
export RADIUS_REPEAT=2
export RADIUS_PATH='/usr/local/share/freeradius/'
export RADIUS_PAYLOAD='User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e'
