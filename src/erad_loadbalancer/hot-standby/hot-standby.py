#!/usr/bin/python

import sys
import subprocess
import time
import datetime
import os
import boto3
import requests


# Check for the presence of [TARGET_IP] argument. Exit if not present...
if len(sys.argv) < 2:
    sys.exit('Usage: %s [MASTER_NODE_IP]' % sys.argv[0])

# Amazon AWS credentials for reassociating Elastic IP
EC2_AWS_ACCESS_KEY_ID = os.environ['EC2_AWS_ACCESS_KEY_ID']
EC2_AWS_SECRET_ACCESS_KEY = os.environ['EC2_AWS_SECRET_ACCESS_KEY']
EC2_REGION = os.environ['EC2_REGION']

# Amazon AWS credentials for publishing SNS
SNS_AWS_ACCESS_KEY_ID = os.environ['SNS_AWS_ACCESS_KEY_ID']
SNS_AWS_SECRET_ACCESS_KEY = os.environ['SNS_AWS_SECRET_ACCESS_KEY']
SNS_REGION = os.environ['SNS_REGION']

# SNS Topic ARN for publishing alerts - default arn:aws:sns:us-east-1:439838887486:code-yellow
TOPIC_ARN = os.environ['TOPIC_ARN']

# Allocation ID for Public Elastic IP
ELASTIC_IP_ALLOCATION_ID = os.environ['ELASTIC_IP_ALLOCATION_ID']

# MASTER NODE IP
MASTER_NODE = sys.argv[1]
# Grace period before triggering the hot-standby procedure - default 30 secs
GRACE_PERIOD = int(os.environ['GRACE_PERIOD'])
# Timeout period before declaring MASTER node dead - default 20 secs
TIMEOUT = int(os.environ['TIMEOUT'])
# Ping timeout period, in seconds - default 1 sec
PING_TIMEOUT = int(os.environ['PING_TIMEOUT'])
# Interval between server probes, in seconds - default 5 secs
PROBE_INTERVAL = int(os.environ['PROBE_INTERVAL'])
# Get this host's ip
THIS_HOST_IP = os.popen('ifconfig eth0 | awk \'/inet addr/{print substr($2,6)}\'').read().strip('\n')

# JENKINS REDEPLOYMENT JOB PARAMETERS - with default values
JENKINS_URL = os.environ['JENKINS_URL']
JENKINS_JOB = os.environ['JENKINS_JOB']
JENKINS_TOKEN = os.environ['JENKINS_TOKEN']

# Creating Amazon AWS session and resource objects
sns_session = boto3.session.Session(
    aws_access_key_id=SNS_AWS_ACCESS_KEY_ID,
    aws_secret_access_key=SNS_AWS_SECRET_ACCESS_KEY,
    region_name=SNS_REGION)

ec2_session = boto3.session.Session(
    aws_access_key_id=EC2_AWS_ACCESS_KEY_ID,
    aws_secret_access_key=EC2_AWS_SECRET_ACCESS_KEY,
    region_name=EC2_REGION)

sns = sns_session.resource('sns')
ec2 = ec2_session.resource('ec2')


# Start our hot-standby procedure
subprocess.call(['logger', 'hot-standby monitor: waiting for {0} seconds before starting'.format(GRACE_PERIOD)])
time.sleep(GRACE_PERIOD)
subprocess.call(['logger', 'hot-standby monitor: monitoring master node {0}'.format(MASTER_NODE)])

RADIUS_PORT = int(os.environ.get('RADIUS_PORT', 1814))
RADIUS_SECRET  = os.environ['RADIUS_SECRET']
RADIUS_TIMEOUT = os.environ['RADIUS_TIMEOUT']
RADIUS_REPEAT  = os.environ['RADIUS_REPEAT']
RADIUS_PATH    = os.environ['RADIUS_PATH']
RADIUS_PAYLOAD = os.environ['RADIUS_PAYLOAD']

def standard_radius_test(server_ip):
    cmd_args = [
        'radclient',
        '-t', RADIUS_TIMEOUT,
        '-x',
        '-r', RADIUS_REPEAT,
        '-D', RADIUS_PATH,
        '-s',
        '%s:%d' % (server_ip, RADIUS_PORT),
        'auth',
        RADIUS_SECRET,
    ]
    cmd_args = [x for x in [str(x) for x in cmd_args]]

    radius_response_received = False
    try:
        stdout, stderr = subprocess.Popen(
            cmd_args,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        ).communicate(RADIUS_PAYLOAD)

        if 'Received Access' in stdout:
            radius_response_received = True
    except Exception:
        pass

    return radius_response_received

while True:
    subprocess.call(['logger', 'hot-standby monitor: pinging master node {0}'.format(MASTER_NODE)])
    t0 = time.time()
    while (time.time() - t0) < TIMEOUT:
        response = standard_radius_test(MASTER_NODE)
        if response:
            break
        else:
            subprocess.call(['logger', 'hot-standby monitor: ping failed for master node {0}'.format(MASTER_NODE)])
            continue

    if response:
        subprocess.call(['logger', 'hot-standby monitor: master node {0} ok'.format(MASTER_NODE)])
        time.sleep(PROBE_INTERVAL)
    else:
        subprocess.call(['logger', 'hot-standby monitor: error! could not reach {0}'.format(MASTER_NODE)])
        break

subprocess.call(['logger', 'hot-standby monitor: Executing fallback routine'])

####################        Fallback procedure      ####################

######        Step 1: Reassociate Elastic IP to this machine     #######

response = requests.get('http://169.254.169.254/latest/meta-data/instance-id')
instance_id = response.text
address = ec2.VpcAddress(ELASTIC_IP_ALLOCATION_ID)
try:
    address.associate(InstanceId=instance_id)

    # 1.1 - Confirm that the Elastic IP is indeed associated with our instance:
    client = ec2_session.client('ec2')
    addresses_dict = client.describe_addresses()
    allocation_id_list = [allocation_id['AllocationId'] for allocation_id in addresses_dict['Addresses']]

    if addresses_dict['ResponseMetadata']['HTTPStatusCode'] == 200:
        allocation = [address for address in addresses_dict['Addresses'] if address['AllocationId'] == ELASTIC_IP_ALLOCATION_ID]
        if not allocation:
            subprocess.call(['logger', 'hot-standby monitor: error! could not find Allocation ID {0}'.format(ELASTIC_IP_ALLOCATION_ID)])
        else:
            allocation = allocation[0]
            if allocation['InstanceId'] != instance_id:
                subprocess.call(['logger', 'hot-standby monitor: error! Elastic IP {0} is associated with the wrong instance:'
                                           ' {1} instead of {2}'.format(allocation['InstanceId'], instance_id)])
            else:
                EIP_association_status = 'Elastic IP {0} is now associated with {1}'\
                    .format(allocation['PublicIp'], allocation['PrivateIpAddress'])
                subprocess.call(['logger', 'hot-standby monitor: {0}'.format(EIP_association_status)])
    else:
        subprocess.call(['logger', 'hot-standby monitor: error! got {0} while getting Elastic IP allocation list'
                        .format(addresses_dict['ResponseMetadata']['HTTPStatusCode'])])
except Exception as e:
    allocation_error_message = e.message
    subprocess.call(['logger', 'hot-standby monitor: {0}'.format(e.message)])


#######        Step 2: Trigger the Jenkins "redeploy" job     ########

subprocess.call(['logger', 'hot-standby monitor: Triggering Jenkins Job!'])

#wget -q --output-document - "http://jenkins.42tm.com:8080/buildByToken/build?job=__erad-REDEPLOY-production-backend__&token=yD3aPV5gmLV7nDN8uiW4aqDE0fJA2qQL"
payload = {'job': JENKINS_JOB, 'token': JENKINS_TOKEN}
try:
    r = requests.get(JENKINS_URL, params=payload)
    jenkins_feedback = '\n\nFrom Jenkins got:\nStatus Code: ' + str(r.status_code) + '\n' + r.text
    subprocess.call(['logger', 'hot-standby monitor: From Jenkins got {0}'.format(str(r.status_code))])
except Exception as e:
    jenkins_feedback = str(e)
    subprocess.call(['logger', 'hot-standby monitor: From Jenkins got error {0}'.format(str(e))])


#########        Step 3: Send an SNS code yellow alert     ##########

now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S UTC")
message = '{0}\n\nLoadbalancer {1} is currently unreachable.\n\nNode {2} is taking over.\n\n'\
    .format(now, MASTER_NODE, THIS_HOST_IP)
if 'EIP_association_status' in locals():
    message += EIP_association_status
else:
    message += allocation_error_message

message += jenkins_feedback

response = sns.Topic(TOPIC_ARN).publish(
    Subject='Load balancer failover occurred',
    Message=message)

if response['ResponseMetadata']['HTTPStatusCode'] == 200:
    messageId = response['MessageId']
    subprocess.call(['logger', 'hot-standby monitor: SNS message was successfully published - {0}'.format(messageId)])
else:
    subprocess.call(['logger', 'hot-standby monitor: error! got {0} while publishing SNS message'
                    .format(response['ResponseMetadata']['HTTPStatusCode'])])

sys.exit(0)

