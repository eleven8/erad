### Synopsis
------
This service provides a monitoring service that continuously tests network connectivity between the host running it and 
a designated remote host (so called MASTER node). In case the connectivity is lost for more than a predetermined amount 
 of time, the **hot-standby service** executes a fallback procedure.

The fallback procedure consists of the following three steps:

1. A designated Elastic IP is associated to the machine running the **hot-standby** service;
2. A designated Jenkins redeployment job is triggered;
3. An alert is published in a designed Amazon SNS topic.
    
### Deployment requirements
1. The **MASTER NODE IP/URL** parameter must be passed to the **deploy.sh** script during service deployment.
2. All the services parameters can be configured by editing the **settings.sh** file before deployment. Parameter details as follows:

```bash
EC2_AWS_ACCESS_KEY_ID='<your aws access key for associating Elastic IP>'
EC2_AWS_SECRET_ACCESS_KEY='<your aws secret access key for associating Elastic IP>'
EC2_REGION='<region to be used for EC2 session>'

SNS_AWS_ACCESS_KEY_ID='<your aws access key for publishing SNS messages>'
SNS_AWS_SECRET_ACCESS_KEY='<your aws secret access key for publishing SNS messages>'
SNS_REGION='<region to be used for SNS session>'

TOPIC_ARN='<SNS topic in which hot-standby service will publish alerts>'
ELASTIC_IP_ALLOCATION_ID='<Elastic IP AllocationID that this node will claim if MASTER disappears>'

GRACE_PERIOD=<delay, in seconds, that the service will sleep after being started>
TIMEOUT=<if MASTER node is not reachable after TIMEOUT seconds, will be declared dead and fallback procedure is triggered>
PING_TIMEOUT=<timeout period, in seconds, for each ping to MASTER node>
PROBE_INTERVAL=<delay between sucessfull probes to MASTER node, in seconds>

JENKINS_URL='<url + endpoint for Jenkins Server>'
JENKINS_JOB='<Jenkins redeployment job descriptor>'
JENKINS_TOKEN='<Jenkins token for redeployment script>'
```

### Installation
------
```bash
chmod +x deploy.sh
sudo ./deploy.sh [MASTER IP/URL]      # [MASTER IP/URL] - IP/URL of MASTER node to be monitored
```
After installation, the configuration and script files will reside in the following directory:
```bash
/opt/hot-standby
```

### Controlling the hot-standby service
------
```bash
# Starting the service:
sudo start hot-standby

# Stopping the service:
sudo stop hot-standby

# Restarting the service:
sudo restart hot-standby

# Disabling the service:
# edit the file /etc/init/hot-standby.conf and comment out the following lines:
#start on runlevel [2345]
#stop on runlevel [!2345]

```

### Checking hot-standby service logs
**Log messages** for the hot-standby service can be monitored using the following command:
```bash
sudo tail -f /var/log/messages | grep "hot-standby monitor"
```


### Code Organization
------

```
hot-standby
   deploy.sh        # Deploy script, must pass the IP or URL of MASTER NODE as argument  
   hot-standby.py   # hot-standby script, must be provided with the environment variables contained in settings.sh 
   README.md        # this documentation file
   requirements.txt # required python packages
   settings.sh      # sets environment variables for hot-standby.py, must be customized before deployment
```