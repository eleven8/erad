#!/bin/bash

# cfn-init will copy deployment files to /usr/local/src
set -e
set -x

yum -y update

# installing docker and upgrading aws-cli to version 2, aws ecr in version 2 uses new syntax, so migrating now
yum -y remove aws-cli
yum -y install docker-19.03.13ce-1.amzn2 unzip libatomic

pushd /tmp
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install
ln -s /usr/local/bin/aws /usr/bin/aws
popd

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/sbin/docker-compose

chmod +x /usr/local/sbin/docker-compose
# This commented here becaues script will start docker
# after deploy script deploy iptables rules about blocked ip addresses
# or docker networks will not work
# systemctl start docker

cd /usr/local/src/erad-load-balancer

INSTANCE=$(curl http://169.254.169.254/latest/meta-data/instance-id)

# add alarm for cpu-surplus-credits-charged metric
export AWS_ACCESS_KEY_ID=AKIAIDR4RVSXLXAIAUYQ
export AWS_SECRET_ACCESS_KEY=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
aws cloudwatch put-metric-alarm \
    --alarm-name "cpu-surplus-credits-charged-${INSTANCE}-load-balancer" \
    --alarm-description "CPUSurplusCreditsCharged " \
    --metric-name CPUSurplusCreditsCharged --namespace AWS/EC2 \
    --statistic Average --period 300 --threshold 0 \
    --comparison-operator GreaterThanThreshold \
    --dimensions Name=InstanceId,Value=${INSTANCE} \
    --evaluation-periods 1 \
    --alarm-actions arn:aws:sns:us-west-2:439838887486:trouble \
    --unit Count --region us-west-2

## Add disk and swap monitoring if this is a production server
yum -y install \
          perl-Switch \
          perl-DateTime \
          perl-Sys-Syslog \
          perl-LWP-Protocol-https \
          perl-Digest-SHA.x86_64

curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
unzip CloudWatchMonitoringScripts-1.2.2.zip && \
rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
pushd aws-scripts-mon

echo "AWSAccessKeyId=$AWS_ACCESS_KEY_ID
AWSSecretKey=$AWS_SECRET_ACCESS_KEY
" > awscreds.conf
crontab -l > crontab.conf || true
echo "* * * * * /usr/local/src/erad-load-balancer/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail --swap-util --swap-used --disk-space-util --disk-path=/ --from-cron" >> crontab.conf
crontab crontab.conf

# add alarms for disk and swap
aws cloudwatch put-metric-alarm \
  --alarm-name "disk-utilization-${INSTANCE}-erad-lb" \
  --alarm-description "Disk usage is high" \
  --metric-name DiskSpaceUtilization \
  --namespace System/Linux \
  --statistic Average \
  --period 300 \
  --threshold 90 \
  --comparison-operator GreaterThanThreshold \
  --dimensions \
    Name=Filesystem,Value=/dev/xvda1 \
    Name=MountPath,Value=/ \
    Name=InstanceId,Value=${INSTANCE} \
  --evaluation-periods 1 \
  --alarm-actions arn:aws:sns:us-west-2:439838887486:trouble \
  --unit Percent \
  --region us-west-2
aws cloudwatch put-metric-alarm \
  --alarm-name "swap-utilization-${INSTANCE}-erad-lb" \
  --alarm-description "Swap usage is high" \
  --metric-name SwapUtilization \
  --namespace System/Linux \
  --statistic Average \
  --period 300 \
  --threshold 20 \
  --comparison-operator GreaterThanThreshold \
  --dimensions \
    Name=InstanceId,Value=${INSTANCE} \
  --evaluation-periods 1 \
  --alarm-actions arn:aws:sns:us-west-2:439838887486:trouble \
  --unit Percent \
  --region us-west-2



# decrypt and install cfg files
gpg --batch --yes --passphrase $T42_PASSPHRASE --output /etc/erad.cfg --decrypt /usr/local/src/erad-load-balancer/erad.cfg.prod.secure

# if decryption failed, we must be on a dev box, deploy dev cfg file
if [ ! -f /etc/erad.cfg ]; then
  mv /usr/local/src/erad-load-balancer/erad.cfg.dev /etc/erad.cfg
  export T42_SERVER_ID=52.26.34.201
fi

chmod 644 /etc/erad.cfg

# setup the ipblocker
cp /usr/local/src/erad-load-balancer/ipblocker/ip_pkt_list_tot.conf /etc/modprobe.d/
cp /usr/local/src/erad-load-balancer/ipblocker/blockerd /usr/local/sbin/
cp /usr/local/src/erad-load-balancer/ipblocker/blockip.sh /usr/local/sbin/
chmod 755 /usr/local/sbin/blockerd
chmod 755 /usr/local/sbin/blockip.sh
export AWS_ACCESS_KEY_ID=AKIAJ4YG4YCNYM2U2OYA
export AWS_SECRET_ACCESS_KEY=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
export AWS_DEFAULT_REGION=us-west-2
aws s3 cp s3://teamfortytwo.security/blocked_ips.txt /tmp/blocked_ips.txt
/sbin/iptables-restore /usr/local/src/erad-load-balancer/ipblocker/iptables.save
while read blockedip; do
  echo "Blocking $blockedip"
  /sbin/iptables -A INPUT -p udp -s $blockedip -j DROP
done < /tmp/blocked_ips.txt
/usr/local/sbin/blockerd &

systemctl start docker

# Configuring docker credentials to access AWS ECR
# exporting AWS_ACCOUNT_ID to be able to access it inside aws-freeradius-lb.docker-compose.yaml
export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query "Account" --output text)

# unseting AWS CREDENTIALS
# ec2 instance uses attached policy see cloudformation template
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin "${AWS_ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com"

cd /usr/local/src/erad-load-balancer

pushd /usr/local/src/erad-load-balancer
mkdir -p working-copy-freeradius-lb-raddb
docker-compose -f aws-freeradius-lb.docker-compose.yaml pull
docker-compose -f aws-freeradius-lb.docker-compose.yaml run radius-lb-conf-gen $CLUSTER_ID
docker-compose -f aws-freeradius-lb.docker-compose.yaml up -d radius-lb

sleep 60
popd

# start pen to load balance radius traffic (will be dynamically populated by load_ports.py)
python /usr/local/src/erad-load-balancer/load_ports.py -c $CLUSTER_ID

# Starting monitory after pen load balancer stated serving ports
chmod +x pen-radius-monitor/deploy.sh
pen-radius-monitor/deploy.sh

pushd /usr/local/src/erad-load-balancer
# launch the deploy for the automated failover system
if [ ! -z "$PRIMARY_IP" ]; then
  chmod +x hot-standby/deploy.sh
  hot-standby/deploy.sh $PRIMARY_IP
fi
popd



#iptables --new-chain RATE-LIMIT
#iptables --append INPUT -p udp --jump RATE-LIMIT
#iptables --append RATE-LIMIT \
#    -p udp \
#    --match hashlimit \
#    --hashlimit-mode dstport \
#    --hashlimit-upto 10/sec \
#    --hashlimit-burst 10 \
#    --hashlimit-name conn_rate_limit \
#    --jump ACCEPT
#
#iptables --append RATE-LIMIT --match limit --limit 1/sec --jump LOG --log-prefix "IPTables-Dropped: "
#iptables --append RATE-LIMIT --jump DROP

