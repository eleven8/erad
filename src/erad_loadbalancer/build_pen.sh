#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
##
## This build script produce a tar.gz file with the proper files for
## deployment.
##
################################################################################
set -x
VERSION=$1
if [ -z "$VERSION" ]
then
   VERSION="DEV"
fi

# on dev boxes, make sure required packages are installed
# script must run as root for this to work
if [ "$VERSION" == "DEV" ]
then
  yum -y install gcc
fi

# Old stable version: http://siag.nu/pub/pen/pen-0.27.5.tar.gz
wget http://siag.nu/pub/pen/pen-0.31.0.tar.gz
tar xvfz pen-0.31.0.tar.gz
pushd pen-0.31.0

mkdir -p ./dist/usr/local
./configure --prefix=$(pwd)/dist/usr/local
make
make install
pushd dist
tar -cvzf ../../pen_$VERSION.tar.gz --group=root --owner=root *
popd
popd


#./pen -l pen.log -p pen.pid 172.31.11.165:80 172.31.8.17:80

#./pen -l pen.log -p pen.pid 1812 172.31.10.34:1812 172.31.11.228:1812
#./pen -l pen.log -p pen.pid 1813 172.31.10.34:1813 172.31.11.228:1813
#./pen -l pen.log -p pen.pid -U 1812 172.31.10.34:1812 172.31.11.228:1812
#./pen -l pen.log -p pen.pid -U 1813 172.31.10.34:1813 172.31.11.228:1813

#aws s3 cp pen_DEV.tar.gz s3://teamfortytwo.deploy/public/pen_DEV.tar.gz
