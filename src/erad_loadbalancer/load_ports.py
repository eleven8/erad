#!/usr/bin/env python


#
# Load Balancer Pens creator
#
import os
import sys, getopt
import string
import json
import requests

def main(argv):
    def extract_ips(ips):
        return ips.split(':')

    def erad_cfg():
        erad_configuration = None

        # specify defaults
        cfg = {
            "system_key": ["r_7X385PDgqPSB2cMvpmPZ6GHqXh5Cd8ZF"],
            "api": {
                "host": "http://52.26.34.201:5000"
            }
        }
        # try override defaults with machine config
        try:
            cfg.update( json.load(open("/etc/erad.cfg")) or {} )
        except:
            pass
        # convert dict to object so it can be referenced
        # as cfg.region instead of cfg["region"]
        erad_configuration = deep_convert_dict_to_object(cfg)
        return erad_configuration

    def deep_convert_dict_to_object( d ):
        for k in d:
            if type(d[k]) is dict:
                d[k] = deep_convert_dict_to_object( d[k] )
        return type("", (), d)()


    def json_post( path, input ):
        return requests.post(
            # util.erad_cfg().api.host + path,  # official version
            erad_cfg().api.host + path,
            headers={ 'Content-type': 'application/json' },
            data=json.dumps(input))


    clusterId = ''
    try:
      opts, args = getopt.getopt(argv,"c:",["clusterId="])
    except getopt.GetoptError:
      print('loadPorts.py -c <clusterId>')
      sys.exit(2)
    for opt, arg in opts:
        if opt in ('-c', '--clusterId'):
            clusterId = arg
        else:
             print('loadPorts.py -c <clusterId>')
             sys.exit()

    if (clusterId == ''):
        print('loadPorts.py -c <clusterId>')
        sys.exit()

    data = json_post(
        '/erad/system/server/config/load',
        {
            "SystemKey": erad_cfg().system_key[0],
            "ClusterId": clusterId,
            "ServerType": "load_balancer"
        })

    vs_list = data.json()

    for auth_port, servers in vs_list["VirtualServers"]:
        if auth_port >= 40000:
            continue

        auth_port = str(auth_port)
        auth_cmd = "/usr/local/bin/pen -x 15 -hr -U " + auth_port + " "
        auth_servers = servers.get('auth')
        for auth_server in auth_servers:
            auth_cmd = auth_cmd + (auth_servers[auth_server] + ":" + auth_port + " ")
        print(auth_cmd)
        os.system(auth_cmd)

        acct_servers = servers.get('acct')
        acct_port = str(int(auth_port) + 1)
        acct_cmd = "/usr/local/bin/pen -x 15 -hr -U " + acct_port + " "
        for acct_server in acct_servers:
            acct_cmd = acct_cmd + (acct_servers[acct_server] + ":" + acct_port + " ")
        print(acct_cmd)
        os.system(acct_cmd)


if __name__ == "__main__":
    main(sys.argv[1:])
