#!/bin/bash

export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
# AWS_SECRET_ACCESS_KEY is injected by Jenkins
export AWS_DEFAULT_REGION=us-west-2

# zip the deployment files
tar -cvzf "erad-load-balancer.stage.tar.gz" -C ./src  \
erad_loadbalancer \
erad_freeradius_lb_cfg \
freeradius-lb \
df.radius-lb-conf-gen.Dockerfile \
df.radius-lb.Dockerfile \
freeradius-lb.docker-compose.yaml \
build-freeradius-lb.docker-compose.yaml \
aws-freeradius-lb.docker-compose.yaml

# upload the deployment zip to S3
aws s3 cp erad-load-balancer.stage.tar.gz s3://teamfortytwo.deploy/erad-load-balancer/erad-load-balancer.$(printf "%05d\n" $BUILD_NUMBER).tar.gz
aws s3 cp erad-load-balancer.stage.tar.gz s3://teamfortytwo.deploy/
