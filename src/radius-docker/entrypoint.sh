#!/bin/sh
set -e

cp /erad-src/erad/api/boto.cfg.dev /etc/boto.cfg
cp /erad-src/erad/api/erad.cfg.dev /etc/erad.cfg

source /erad-src/erad/api/config.dev

for file in boto.cfg erad.cfg; do
    cat /etc/$file \
        | sed "s:\##AWS_ACCESS_KEY_ID##:$AWS_ACCESS_KEY_ID:g" \
        | sed "s:\##AWS_SECRET_ACCESS_KEY##:$AWS_SECRET_ACCESS_KEY:g" \
        | sed "s:\##AWS_DEFAULT_REGION##:$AWS_DEFAULT_REGION:g" \
        | sed "s:\##SYSTEM_KEY##:$SYSTEM_KEY:g" \
        | sed "s:\##DEPLOY_REGION##:$DEPLOY_REGION:g" \
        > /etc/$file.deploy

    mv /etc/$file.deploy /etc/$file
done


python3 /opt/savelog/save_log_server.py &
radiusd $@
