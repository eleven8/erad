FROM amazonlinux:2

RUN \
	yum -y upgrade && \
	yum -y install \
		python3 && \
	#
	pip3 install requests

ADD freeradius-lb/raddb /usr/local/etc/raddb
ADD freeradius-lb/load_ports_ng.py /usr/local/bin/load_port_ng.py
ADD freeradius-lb/freeradius-lb-confgen-entrypoint.sh /usr/local/bin/freeradius-lb-confgen-entrypoint.sh
RUN chmod +x /usr/local/bin/freeradius-lb-confgen-entrypoint.sh

VOLUME /output-cfg

ENTRYPOINT ["/usr/local/bin/freeradius-lb-confgen-entrypoint.sh"]
CMD []




