FROM amazonlinux:2

RUN \
	yum -y upgrade && \
	amazon-linux-extras install epel -y && \
	yum -y install \
		tar \
		bzip2 \
		git \
		# for headless chromium/chrome
		cups-libs \
		dbus-glib \
		libXrandr \
		libXcursor \
		libXinerama \
		cairo \
		cairo-gobject \
		pango \
		atk \
		libXScrnSaver \
		libXcomposite \
		libXi \
		libXcursor \
		libXdamage \
		libXext \
		libXtst \
		alsa-lib \
		at-spi2-atk \
		gtk3 \
		# for phantomjs
		freetype.x86_64 \
		fontconfig \
		nodejs && \
	#
	npm install -g grunt-cli bower

ADD erad_static 				/usr/local/src/erad_static
ADD tools 						/usr/local/src/tools

RUN \
	groupadd -r eradstatic -g 5000 \
	&& useradd -u 5000 --no-log-init --create-home -r -g eradstatic eradstatic \
	&& chown -R 5000:5000 /usr/local/src/erad_static /usr/local/src/tools


USER eradstatic:eradstatic


RUN \
	cd /usr/local/src/erad_static && \
	mkdir -p ./node_modules && \
	npm install  && \
	chmod +x ./erad-static-entrypoint.sh ./erad-static-testing-entrypoint.sh


WORKDIR /usr/local/src/erad_static/

ENTRYPOINT ["/erad-static-entrypoint.sh"]

