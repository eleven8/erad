FROM alpine:3.8

RUN \
	apk add --no-cache python3

ADD tools/dashboard/UI /usr/local/erad-start-ui

WORKDIR /usr/local/erad-start-ui

ENTRYPOINT ["/usr/bin/python3", "-m", "http.server", "8888"]


