FROM amazonlinux:2 as builder

RUN \
	yum -y upgrade && \
	yum -y install \
		git-core \
		gcc \
		libtalloc-devel \
		openssl \
		openssl-devel \
		python2-devel \
		python3-devel \
		make


ARG source=https://github.com/FreeRADIUS/freeradius-server.git
ARG release=v3.0.x
ARG depth=1
#ARG commit="6aaeab6e9d1d8e64829a0f3c4d2dfbdc745ff108"

RUN cd /root &&\
	git clone --depth ${depth} --single-branch --branch ${release} ${source} && \
	cd freeradius-server && \
#	git checkout "${commit}" &&\
	./configure && \
	# ./configure --disable-openssl-version-check &&\
	make && \
	make install

FROM amazonlinux:2

ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG AWS_DEFAULT_REGION
ARG RADIUSD_USER_UID=1000
WORKDIR /usr
COPY --from=builder /usr/local /usr/local

ADD radius-docker/requirements.txt /root
ADD radius-docker/entrypoint.sh /entrypoint.sh
ADD radius-docker/freeradius.logrotate.conf /etc/logrotate.d/freeradius.logrotate.conf
ADD erad/frmod/savelog /opt/savelog


RUN \
	yum -y upgrade && \
	yum -y install \
		gzip \
		logrotate \
		python3 \
		python3-devel \
		shadow-utils \
		libtalloc \
		libatomic \
		# libxslt only needed for erad.frmod.test.test_module
		# todo: optimize it
		libxslt &&\
	cd /root && \
	pip3 install -r /root/requirements.txt && \
	useradd -M -u $RADIUSD_USER_UID radiusd && \
	usermod -L radiusd && \
	yum -y remove shadow-utils && \
	yum clean all && \
	mkdir /var/log/radius && \
	chmod -R 777 /var/log/radius && \
	mkdir -p /var/run/radiusd/ && \
	chown -R $RADIUSD_USER_UID:$RADIUSD_USER_UID /var/run/radiusd/ && \
	chmod +x /entrypoint.sh && \
	chmod 0644 /etc/logrotate.d/freeradius.logrotate.conf && \
	yum -y autoremove


ENTRYPOINT ["/entrypoint.sh"]
CMD ["-X"]





