#!/usr/bin/env python
import getopt
import json
import sys

import requests

template_proxy_conf = '''
proxy server {
    default_fallback = no
}

%s

%s

realm LOCAL {
    #  If we do not specify a server pool, the realm is LOCAL, and
    #  requests are not proxied to it.
}

%s
'''

template_vs_conf = '''
server %s {
    listen {
        type = auth
        ipaddr = *      #all addresses
        port = %s       #/etc/services (1812)

        limit {
              max_pps = 10000             #relief valve (packets per second)
              max_connections = 0       #no limit
              lifetime = 60             #seconds
              idle_timeout = 30         #seconds
        }
    }
    listen {
        ipaddr = *
        port = %s 
        type = acct
    }

    client ElevenWireless {
        ipaddr = 0.0.0.0/0
        secret = %s
    }

    authorize {
        update control {
            Load-Balance-Key := "%%{Called-Station-Id}-%%{Calling-Station-Id}"
            Proxy-To-Realm := %s
            Cleartext-Password !* ANY
        }
    }

    authenticate {
    }

    preacct {
    }

    accounting {
        update control {
            Load-Balance-Key := "%%{Called-Station-Id}-%%{Calling-Station-Id}"
            Proxy-To-Realm := %s
            Cleartext-Password !* ANY
        }
    }

    session {
    }


    # &control - per packet, and can be empty
    post-auth {
    }

    pre-proxy {
    }

    post-proxy {
    }
}
'''

realm_template = '''
realm realm_{type}_{port} {{
    {type}_pool = {type}_{port}
}}
'''

server_template = '''
home_server {type}_proxy_{port}_{srv_number:02} {{
    type = {type}
    ipaddr = {srv_ip}
    port = {port}
    secret = {shared_secret}
    response_window = 20
    zombie_period = 40
    revive_interval = 10
    status_check = request
    username = ping
    password = test
    check_interval = 30
    check_timeout = 4
    num_answers_to_alive = 3
    max_outstanding = 65536
}}
'''

pool_template = '''
home_server_pool {type}_{port} {{
    type = keyed-balance
    {server_list}
}}
'''

server_line_template = '''
    home_server = {type}_proxy_{port}_{srv_number:02}
'''


def convert(dir, **kwargs):
    name = kwargs.get('name', '')
    result = '%s %s {' % ( dir, name )
    for k, v in kwargs.items():
        if k == 'name':
            continue
        if type(v) in (list, dict):
            for vv in v:
                result += '\n  %s = %s' % (k, vv)
        else:
            result += '\n  %s = %s' % (k, v)

    result += '\n}\n\n'
    return result


def erad_cfg():
    erad_configuration = None

    # specify defaults
    cfg = {
        "system_key": ["r_7X385PDgqPSB2cMvpmPZ6GHqXh5Cd8ZF"],
        "api": {
            "host": "http://52.26.34.201:5000"
        }
    }
    # try override defaults with machine config
    try:
        cfg.update( json.load(open("/etc/erad.cfg")) or {} )
    except:
        pass
    # convert dict to object so it can be referenced
    # as cfg.region instead of cfg["region"]
    erad_configuration = deep_convert_dict_to_object(cfg)
    return erad_configuration


def deep_convert_dict_to_object( d ):
    for k in d:
        if type(d[k]) is dict:
            d[k] = deep_convert_dict_to_object( d[k] )
    return type("", (), d)()


def json_post( path, input ):
    return requests.post(
        # util.erad_cfg().api.host + path,  # official version
        erad_cfg().api.host + path,
        headers={ 'Content-type': 'application/json' },
        data=json.dumps(input))


def get_virtual_servers(clusterId):
    data = json_post(
        '/erad/system/server/config/load',
        {
            "SystemKey": erad_cfg().system_key[0],
            "ClusterId": clusterId,
            "ServerType": "load_balancer"
        })

    vs_list = data.json()
    # print(json.dumps(vs_list['VirtualServers'], indent=2))
    return vs_list


def get_radius_secrets_config(clusterId):
    data = json_post(
        '/erad/system/server/config/load',
        {
            "SystemKey": erad_cfg().system_key[0],
            "ClusterId": clusterId,
            "ServerType": "radius_shared_secrets"
        })

    secrets_list = data.json()
    secrets_list = secrets_list['SharedSecrets']
    # print(json.dumps(secrets_list, indent=2))
    return secrets_list


def get_secret(secrets, port):
    try:
        return secrets[str(port)]
    except KeyError as e:
        sys.stderr.write('WARNING: Secret for port %s is not found!\n' % port)
        # raise e
        return


def make_proxy_conf(vs_list, secrets):
    pools_list = []
    realms_list = []
    server_list = []
    for auth_port, servers in vs_list["VirtualServers"]:
        # TODO for testing freeradius as load balancer we will use port with number > 40000.
        if auth_port < 40000:
            continue

        acct_port = auth_port + 1
        auth_servers = servers.get('auth', {})
        acct_servers = servers.get('acct', {})
        secret = get_secret(secrets, auth_port)

        gen_proxy_config_block(auth_port, 'auth', auth_servers, pools_list, realms_list, secret, server_list)
        gen_proxy_config_block(acct_port, 'acct', acct_servers, pools_list, realms_list, secret, server_list)

    print(template_proxy_conf % (
        ''.join(server_list),
        ''.join(pools_list),
        ''.join(realms_list)
    ))


def gen_proxy_config_block(port, port_type, servers, pools_list, realms_list, secret, server_list):
    srv_lines = []
    for i, server_ip in enumerate(servers.values()):
        home_srv_str = server_template.format(**{
            'type': port_type,
            'port': port,
            'srv_number': i,
            'srv_ip': server_ip,
            'shared_secret': secret
        })
        server_list.append(home_srv_str)

        srv_lines.append(
            server_line_template.format(**{
                'type': port_type,
                'port': port,
                'srv_number': i
            })
        )

    # No home servers for port - no empty config for pools and realms
    if len(srv_lines) == 0:
        return

    server_list_lines = ''.join(srv_lines)
    pools_str = pool_template.format(**{
        'type': port_type,
        'port': port,
        'server_list': server_list_lines
    })
    pools_list.append(pools_str)
    realms_list.append(realm_template.format(**{
        'type': port_type,
        'port': port,

    }))


def make_vs_conf(vs_list, secrets):
    def make_vs(port, servers):
        if not len(servers):
            return

        auth_port = port
        acct_port = port + 1
        secret = get_secret(secrets, auth_port)
        if not secret:
            return

        print(template_vs_conf % (
            'server_%s' % ( auth_port),
            auth_port,
            acct_port,
            secret,
            'realm_auth_%s' % (auth_port),
            'realm_acct_%s' % (acct_port)
        ))

    for auth_port, servers in vs_list["VirtualServers"]:
        # TODO for testing freeradius as load balancer we will use port with number > 40000.
        if auth_port < 40000:
            continue

        make_vs(auth_port, servers.get('auth', {}))


def main(argv):
    usage_msg = 'loadPorts.py -c <clusterId> -o [vs|proxy]'

    clusterId = ''
    try:
      opts, args = getopt.getopt(argv,"c:o:",["clusterId=,output="])
    except getopt.GetoptError:
      print(usage_msg)
      sys.exit(2)

    output = 'proxy'
    for opt, arg in opts:
        if opt in ('-c', '--clusterId'):
            clusterId = arg
        elif opt in ('-o', '--output'):
            output = arg
        else:
             print(usage_msg)
             sys.exit()

    if (clusterId == ''):
        print(usage_msg)
        sys.exit()

    vs_list = get_virtual_servers(clusterId)

    secrets_info = get_radius_secrets_config(clusterId)

    if output == 'vs':
        make_vs_conf(vs_list, secrets_info)
    else:
        make_proxy_conf(vs_list, secrets_info)


if __name__ == "__main__":
    main(sys.argv[1:])
