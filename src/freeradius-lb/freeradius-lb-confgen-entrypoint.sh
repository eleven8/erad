#!/bin/bash

set -e
set -x

if [[ $# -ne 1 ]]; then
	echo "Please pass cluster id"
	exit 1
fi

cluster_id=$1

# sites-enabled by default empty dir,
# git ingore empty dir, so empty dir can not be checkout from repository with rest configuration
# manually creating necessary dir
mkdir -p /usr/local/etc/raddb/sites-enabled/
python3 /usr/local/bin/load_port_ng.py -c "$cluster_id" -o proxy  > /usr/local/etc/raddb/proxy.conf
python3 /usr/local/bin/load_port_ng.py -c "$cluster_id" -o vs  > /usr/local/etc/raddb/sites-enabled/load-balancer-virtual-site

cp -r /usr/local/etc/raddb /output-raddb/

# Note 1000:1000 is gid:uid of radiusd user in radius-lb Docker image
chown -R 1000:1000 /output-raddb
