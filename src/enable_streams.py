#!/usr/bin/python3
from erad import util
import botocore.session
session = botocore.session.get_session()
client = session.create_client('dynamodb', region_name=util.erad_cfg().database.region, endpoint_url=util.erad_cfg().database.streams_host)
try:
    response = client.update_table(
        TableName='Erad_RadiusLog_Global',
        StreamSpecification={
            'StreamEnabled': True,
            'StreamViewType': 'NEW_IMAGE'
        }
    )
except botocore.exceptions.ClientError as e:
    if not 'already has an enabled' in str(e):
        raise e

