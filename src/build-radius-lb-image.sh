#!/bin/bash
set -e

if [[ "$#" -lt 1 ]]; then
	echo "specify target: build-image.sh local|aws <aws-region>"
	exit 1
fi

target=$1
DOCKER_COMPOSE_FILE="build-freeradius-lb.docker-compose.yaml"

if [[ "$target" == "local" ]]; then
	BUILD_ID=$(git rev-parse HEAD) docker-compose -f $DOCKER_COMPOSE_FILE build
elif [[ "$target" == "aws" ]]; then
	AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query "Account" --output text)
	aws_region=$2
	if [[ "$aws_region" == "" ]]; then
		echo "specify aws region: build-image.sh local|aws <aws-region>"
		exit 1
	fi
	REPO="${AWS_ACCOUNT_ID}.dkr.ecr.${aws_region}.amazonaws.com/"  BUILD_ID=$(git rev-parse HEAD) docker-compose -f $DOCKER_COMPOSE_FILE  build
fi

