FROM amazonlinux:2

ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG AWS_DEFAULT_REGION

ADD erad-webservice-requirements.txt	/usr/local/src/erad-webservice-requirements.txt

RUN \
	yum -y upgrade && \
	# epel for redis
	amazon-linux-extras install -y epel redis4.0 && \
	yum -y install \
		httpd \
		httpd-devel \
		logrotate \
		gcc \
		python3-devel \
		python3 \
		unzip \
		openssl

RUN \
	# installing python libs
	pip3 install -r /usr/local/src/erad-webservice-requirements.txt && \
	pip3 install mod_wsgi

RUN \
	echo 'LoadModule wsgi_module /usr/local/lib64/python3.7/site-packages/mod_wsgi/server/mod_wsgi-py37.cpython-37m-x86_64-linux-gnu.so' >> /etc/httpd/conf.modules.d/00-base.conf && \
	aws s3 cp s3://teamfortytwo.deploy/docs/erad-docs.yaml /var/www/html/erad-docs.yaml && \
	# removing cache and temporary files
	yum -y remove \
		gcc && \
	yum -y autoremove && \
	yum clean all && \
	rm -rf /var/cache/yum && \
	rm -rf /root/.cache/pip/*


ADD erad 								/usr/local/src/erad
ADD erad-bootstrap.sh					/tmp/bootstrap.sh
ADD create_database.py					/usr/local/src/create_database.py
ADD remove_database.py					/usr/local/src/remove_database.py
ADD spawn_api_server.py					/usr/local/src/spawn_api_server.py
ADD enable_streams.py					/usr/local/src/enable_streams.py
ADD api_unit_testing.sh					/usr/local/src/api_unit_testing.sh


RUN \
	chmod +x /usr/local/src/api_unit_testing.sh && \
	chmod +x /tmp/bootstrap.sh


ENTRYPOINT ["/bin/sh"]
CMD ["/tmp/bootstrap.sh"]


