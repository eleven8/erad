#!/usr/bin/env python

from erad.api import app
from os import environ


LOG_LEVEL = environ.get('LOG_LEVEL', 'INFO')
DEBUG = LOG_LEVEL == 'DEBUG'


#---------------------- Debug Mode -----------------------
# This next statement only runs if this script is run manually from the CLI
# When enabled as a wsgi module within Apache, this will not execute.
if __name__ == '__main__':
    app.run(debug = DEBUG, host='0.0.0.0', port=5000)
