#!/bin/bash

pushd /usr/local/

mv /usr/local/sbin/erad/api/erad.cfg.dev /etc/erad.cfg

# spawn api server
python ./sbin/spawn_api_server.py 2> /usr/local/webservices/webservice_error.log > /usr/local/webservices/webservice.log &
# Remove and Create a New copy of database for Unit Testing
python ./sbin/remove_database.py
python ./sbin/create_database.py
# Unit Testing Command
nosetests --with-xunit ./sbin/erad/frmod/test/__init__.py ./sbin/erad/frmod/test/test_module.py ./sbin/erad/frmod/test/test_loadvs.py ./sbin/erad/frmod/test/test_loadeap.py
popd
# stop API Server
pkill -f spawn_api_server.py
