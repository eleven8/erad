#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main():
    # Get Arguments
    ProvidedGroup_ID = sys.argv[1]

    try:
        exists = erad_model.Erad_Group2.get("elevenos", ProvidedGroup_ID)

        print "Delete Authenticators"
        # loop through all Authenticators and delete items with elevenos group
        erad_authenticator = erad_model.Erad_Authenticator2
        for item in erad_authenticator.account_group_index.query("elevenos", Group_ID__eq=ProvidedGroup_ID):
            # if authenticator account_id = eleneos and group_id = ProvidedGroup_ID delete this
            # if(item.Account_ID == "elevenos" and item.Group_ID == ProvidedGroup_ID):
            # deletet the item
            item.delete()
            print "Deleted Account_ID: elevenos Group_ID: "+ item.Group_ID + " - " +item.ID
            sleep(0.05)

        print "\nDelete Supplicants"
        # loop through all Supplicants and delete items with elevenos group
        erad_supplicant = erad_model.Erad_Supplicant
        for item in erad_supplicant.query("elevenos::"+ ProvidedGroup_ID):
            # deletet the item
            item.delete()
            print "Deleted ID: " + item.Group_ID + " - " +item.Username
            sleep(0.05)

        print "\nDelete Group"
        # items exists delete it
        print "Deleted: " + exists.ID + "::" + exists.Account_ID
        exists.delete()

    except Exception, e:
        print ""
        print "*****"
        print str(e)
        print "*****"
        print ""

if __name__ == "__main__":
    main()
