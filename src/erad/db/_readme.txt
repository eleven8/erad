DEVELOPMENT NOTES:
Commit pending migration scripts to the "/erad/src/erad/db" directory with the name
"migrate-pending-{description}.py". The script will be manually deployed after review.


DEPLOYMENT NOTES:
Use a fresh production API server so it will be configured with the packages and key necessary.

Need to have the erad folder in the python path. On an API server, erad exists at
"/usr/local/webservices", so use:
   export PYTHONPATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/webservices

Then run the script using python:
   python migrate-pending.py
   
Finally, change the name of the migration script to "migrate-YYYYMMDD-{description}.py" to indicate the date
the migration was executed against production.
