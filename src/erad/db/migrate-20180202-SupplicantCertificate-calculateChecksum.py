#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from erad.api.interface_util import EradInterfaceUtil

def main(argv):

    supcert_table = erad_model.Erad_SupplicantCertificate

    u = EradInterfaceUtil()

    for item in supcert_table.scan():
        if item.Checksum == None:
            sup_model = erad_model.Erad_Supplicant.get(item.Group_ID, item.Username)
            saved_account_id = sup_model.Group_ID.split("::")[0]
            saved_group_id = sup_model.Group_ID.split("::")[1]
            item.Checksum = u.password_based_checksum(
                saved_account_id+saved_group_id+sup_model.Username+sup_model.Password,
                saved_account_id)
            item.save()
            print "Updated ID: " + item.Group_ID

if __name__ == "__main__":
    main(sys.argv[1:])
