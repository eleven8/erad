#!/usr/bin/env python

import sys, getopt, os, codecs
from erad import util
from erad.api import erad_model
from erad.api import schema
from time import sleep
from sets import Set


def main(argv):
    if(not os.path.exists("pos.txt")):
        open("pos.txt", "a").close()

    pos = open("pos.txt", "r+")
#    erad_model.Erad_PasswordBlacklist.create_table(wait=True)
#    schema.create_database()
    if(os.path.exists(argv[0])):
        source_list_path = argv[0] # path to the source list
    else:
        print "File %s not found." % argv[0]
    try:
        try:
            last_pos = int(pos.readline())
        except Exception as e: # files were just created
            last_pos = 0

        print "las_pos = ", last_pos

        batch_count = 0
        batch_set = Set()
        items = []

        f = codecs.open(source_list_path, mode="rt", encoding="utf-8")
        f.seek(last_pos)
        with erad_model.Erad_PasswordBlacklist.batch_write() as batch:
            for line in f:
                password = line.replace("\u","").replace("\n","").replace("\r","").lower()
                if(password == '' ):
                    continue
                if(sys.getsizeof(password) > 1024):
                    continue
                if password in batch_set:
                    print "%s is already on the batch" % password
                    continue
                item_pass = erad_model.Erad_PasswordBlacklist(Password=password)
                items.append(item_pass)
                batch_set.add(password)
                batch_count += 1
                pos.seek(0)
                pos.write("%d" % f.tell())
                if batch_count >= 1000:
                    for item in items:
                        try:
                            batch.save(item)
                        except Exception as e:
                            print item.Password
                            print e
                    batch_count = 0
                    items = []
                    batch_set.clear()
            if batch_count > 0:
                for item in items:
                    try:
                        batch.save(item)
                    except Exception as e:
                        print item.Password
                        print e
                batch_count = 0
                items = []
                batch_set.clear()

    except OSError as e:
        print e

    pos.close()
    os.remove(pos.name)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "You should provide a wordlist"
        sys.exit()
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        sys.exit(0)
