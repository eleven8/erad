#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):

    auth_table = erad_model.Erad_Authenticator

    for item in auth_table.scan():
        try:
                exists = erad_model.Erad_Authenticator2.get(item.ID, "elevenos")
        except Exception, e:
                if (str(e) == "Item does not exist"):
                     erad_model.Erad_Authenticator2(item.ID, Group_ID=item.Group_ID, RadiusAttribute=item.RadiusAttribute, Account_ID="elevenos").save()
                     print "created ID: " + item.ID
                else:
                     print ""
                     print "*****"
                     print e
                     print "*****"
                     print ""

if __name__ == "__main__":
    main(sys.argv[1:])
