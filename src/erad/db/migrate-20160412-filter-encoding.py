#!/usr/bin/env python
"""
This has a dependency of BeautifulSoup. It provides an elegant solution to figure out
what decoding there must be
"""
import sys
import os
import codecs
from BeautifulSoup import BeautifulSoup


def main(argv):
    if(not os.path.exists("last_pos.txt")):
        open("last_pos.txt", "a").close()
    if(not os.path.exists("no_errs.txt")): # number of errors from last execution
        open("last_pos.txt", "a").close()


    pos = open("last_pos.txt", "r+")

    if(os.path.exists(argv[0])):
        source_list_path = argv[0]
    else:
        print "File %s not found." % argv[0]

    try:
        last_pos = int(pos.readline())
    except Exception:
        print
        last_pos = 0


    print "last pos = %d" % last_pos
    codecs.register_error("count", count_errors)
    f = codecs.open(source_list_path, mode="rt", encoding="utf-8", errors="count")
    f.seek(last_pos)

    out = codecs.open("output-list.txt", mode="at", encoding="utf-8")

    for line in f:
        out.write(line)
        last_pos = f.tell()
        pos.seek(0)
        pos.write(str(last_pos))

    print "%d errors/skipped entries " % count_errors.counter
    os.remove(pos.name)

def count_errors(error):
    count_errors.counter += 1
    return codecs.ignore_errors(error)
count_errors.counter = 0

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "You should pass a text file as a parameter"
        sys.exit()
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        sys.exit(0)
