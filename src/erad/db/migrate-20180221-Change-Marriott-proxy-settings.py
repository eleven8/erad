#!/usr/bin/env python
# coding: utf-8

# README

# usage: migrate-pending-Change-Marriott-proxy-settings.py [-h]
#                                                          old_url new_url
#                                                          old_shared_secret
#                                                          new_shared_secret

# Migrate proxy settings

# positional arguments:
#   old_url            Old url pointed to home server, example: `127.0.0.1:1812`
#   new_url            Old url pointed to home server, example: `127.0.0.2:1812`
#   old_shared_secret  Old shared secret for home radius server, example:
#                      `oldsecret`
#   new_shared_secret  new shared secret for home radius server, example:
#                      `newsecret`

# optional arguments:
#   -h, --help         show this help message and exit


from erad.api import erad_model
from time import sleep

import argparse

def main(OLD_URL, NEW_URL, OLD_SHARED_SECRET, NEW_SHARED_SECRET):
    try:
        print('\n')
        print('-'*80)
        print("Group ID;Old home radius server;Old shraed secret;New home radius server; New shared secret")

        for record in erad_model.Erad_Group2.scan(
                RemoteServerUrl__eq=OLD_URL,
                SharedSecret__eq=OLD_SHARED_SECRET):

            #double check record
            if record.RemoteServerUrl != OLD_URL or record.SharedSecret != OLD_SHARED_SECRET:
                print('-' * 80)
                print('!!! Something went wrong: filterd record has wrong old url or old shared secret')
                print('record: {0}'.format(record._get_json()))
                print('Not all record was updated')
                return

            record.RemoteServerUrl = NEW_URL
            record.SharedSecret = NEW_SHARED_SECRET

            record.save()

            print('{GroupID};{OLD_URL};{OLD_SHARED_SECRET};{NEW_URL};{NEW_SHARED_SECRET}'.format(
                    GroupID = '::'.join([record.Account_ID, record.ID]),
                    OLD_URL = OLD_URL,
                    OLD_SHARED_SECRET = OLD_SHARED_SECRET,
                    NEW_URL = record.RemoteServerUrl,
                    NEW_SHARED_SECRET = record.SharedSecret
                ))

    except Exception, e:
        print ""
        print "*****"
        print str(e)
        print "*****"
        print ""

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Migrate proxy settings")
    parser.add_argument("old_url", help="Old url pointed to home server, example: `127.0.0.1:1812`")
    parser.add_argument("new_url", help="Old url pointed to home server, example: `127.0.0.2:1812`")
    parser.add_argument("old_shared_secret", help="Old shared secret for home radius server, example: `oldsecret`")
    parser.add_argument("new_shared_secret", help="new shared secret for home radius server, example: `newsecret`")
    args = parser.parse_args()


    main(
        OLD_URL = args.old_url,
        NEW_URL = args.new_url,
        OLD_SHARED_SECRET = args.old_shared_secret,
        NEW_SHARED_SECRET = args.new_shared_secret
    )
