#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):  
        
    sess_table = erad_model.Erad_Session

    for item in sess_table.scan():
        if(item.Account_ID == None):            
            print "Deleteting ID:", item.ID
            item.delete()

if __name__ == "__main__":
    main(sys.argv[1:])