#!/usr/bin/env python


#
# Loading data from /erad/system/server/config/load to Erad_Endpoint table in DB.
#
import sys, getopt
import string
import json
import requests
from erad import util
from erad.api import erad_model

def main(argv):  
    def json_post( path, input ):
        return requests.post(
            util.erad_cfg().api.host + path,
            headers={ 'Content-type': 'application/json' },
            data=json.dumps(input))

    data = json_post(
        '/erad/system/server/config/load',
        {
            "SystemKey": util.erad_cfg().system_key[0],
            "ServerId": "001",
            "ServerType": "radius"
        })

    vs_list = data.json()
    
    table = erad_model.Erad_Endpoint
    
    for port in vs_list["VirtualServers"]:
        secret = vs_list["VirtualServers"][port]        
        if(port == "1812"):
            item1 = table('52.11.20.214', Port=port, Group_ID="elevenos::Global", ClusterId="uswest-01-0001", Secret=secret, Region="us-west")
        else:
            item1 = table('52.11.20.214', Port=port, Group_ID="", ClusterId="uswest-01-0001", Secret=secret, Region="us-west")
        item1.save()
    


if __name__ == "__main__":
    main(sys.argv[1:])

