#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):  
            
    group_table = erad_model.Erad_Group

    # Scanning through the Group table and creating a record in Erad_Authenticator - if the ID already exists in Erad_Authenticator we're updating it
    # After the creation/update the new record will have only THREE parameters: ID, Group_ID and RadiusAttribute.
    for item in group_table.scan():
    	erad_model.Erad_Authenticator(item.ID, Group_ID=item.ID, RadiusAttribute="nas-identifier").save()
    	print "created ID: " + item.ID
    	sleep(0.05)            

if __name__ == "__main__":
    main(sys.argv[1:])