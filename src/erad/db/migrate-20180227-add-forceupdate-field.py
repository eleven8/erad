#!/usr/bin/env python
"""
This script has a dependency of boto3. To access dynamodb you have to add the
aws credentials in ~/.aws/credentials
To run the script:
python migrate-pending-add-forceupdate-field.py <table name>
"""
import sys
import boto3


def main(argv):
    # To test with local dynamodb uncomment the following line
    # client = boto3.client('dynamodb', endpoint_url='http://localhost:8000')

    client = boto3.client('dynamodb')
    table_description = client.describe_table(TableName=argv[0])['Table']['KeySchema']
    scanned = client.scan(TableName=argv[0])

    for item in scanned['Items']:
        # Key format
        # key = {'Account_ID': {'S': 'elevenos'}, 'ID': {'S': 'foo'}},
        keys = {}
        for i in table_description:
            keys.update({i['AttributeName']: item[i['AttributeName']]})

        # add the 'forceupdate' field
        response = client.update_item(
            TableName=argv[0],
            Key=keys,
            AttributeUpdates={
                'forceupdate': {
                    'Value': {'S': 'somevalue'},
                    'Action': 'PUT'
                }
            },
            ReturnValues='UPDATED_NEW'
        )
        print response['Attributes']

        # delete the 'forceupdate' field
        response = client.update_item(
            TableName=argv[0],
            Key=keys,
            AttributeUpdates={
                'forceupdate': {
                    'Action': 'DELETE'
                }
            },
            ReturnValues='UPDATED_NEW'
        )
        print response


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "You should pass a erad table name as a parameter"
        sys.exit()
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        sys.exit(0)