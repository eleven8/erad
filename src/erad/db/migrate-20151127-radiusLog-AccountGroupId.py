#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):

    rad_table = erad_model.Erad_Radius_Log
    count=0

    for item in rad_table.scan():
        count += 1
        if item.AccountGroup_ID == None:
                item.AccountGroup_ID = 'elevenos::'+item.Group_ID
                item.save()
                print "Updated ID: " + item.Group_ID + "--" + str(item.Instance) + "--" + str(count)

if __name__ == "__main__":
    main(sys.argv[1:])
