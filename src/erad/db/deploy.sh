#!/usr/bin/env bash
set -x

# configure a local database for development
# download and install dynamodb
# Using: install_local_dynamodb $TARGET
install_local_dynamodb() {
    if [ "$LOCAL_DYNDB_INSTALLED" == "$1" ]; then
        return 0
    fi

    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

    pushd $1
        mkdir -p dynamodb
        pushd dynamodb
            wget https://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.zip
            unzip -o dynamodb_local_latest.zip

            # run dynamodb in the background
            java -Djava.library.path=./DynamoDBLocal_lib \
                -jar ./DynamoDBLocal.jar \
                2> dynamo_error.log \
                > dynamo.log &

            # give dynamodb a moment to start up
            sleep 10
        popd
    popd

    pushd $DIR/../..
        # create a blank development database
        python3 ./create_database.py
        # enable streams
        python3 ./enable_streams.py

        cp remove_database.py $1

        # from FRMOD
        cp create_database.py /usr/local/sbin
        cp remove_database.py /usr/local/sbin
        cp spawn_api_server.py /usr/local/sbin
    popd

    export LOCAL_DYNDB_INSTALLED=$1

    return 0
}

if [ -n "$1" ]; then
    $1 $2 $3
else
    if [ -z "$ERAD_AIO_INSTALL" ]; then
        install_local_dynamodb /usr/local/src
    fi
fi

