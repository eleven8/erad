#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):

    sup_table = erad_model.Erad_Supplicant

    for item in sup_table.scan():
    	if "::" not in item.Group_ID:
	    try:
                erad_model.Erad_Supplicant('elevenos::'+item.Group_ID, item.Username, Password=item.Password, DeviceName=item.DeviceName, Description=item.Description, Location=item.Location, ExpirationDate=item.ExpirationDate, Vlan=item.Vlan, UseRemote=item.UseRemote, UseWildcard=item.UseWildcard).save()
	    except:
                pass
	    else:
                print "Updated ID: " + item.Group_ID
                item.delete()

if __name__ == "__main__":
    main(sys.argv[1:])
