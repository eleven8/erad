#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):

    group_table = erad_model.Erad_Group

    for item in group_table.scan():
        try:
            exists = erad_model.Erad_Group2.get("elevenos", item.ID)
        except Exception, e:
            if (str(e) == "Item does not exist"):
                erad_model.Erad_Group2(ID=item.ID, Name=item.Name, RemoteServerUrl=item.RemoteServerUrl, SharedSecret=item.SharedSecret, TimeZone=item.TimeZone, Account_ID="elevenos").save()
                print "created ID: " + item.ID
            else:
                print ""
                print "*****"
                print e
                print "*****"
                print ""

if __name__ == "__main__":
    main(sys.argv[1:])
