#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):  
            
    auth_table = erad_model.Erad_Authenticator

    for item in auth_table.scan():
    	item.CalledStationId = None
    	item.save()
    	print "updated ID: " + item.ID
    	sleep(0.05)            

if __name__ == "__main__":
    main(sys.argv[1:])