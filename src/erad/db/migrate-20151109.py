#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):

    audit_table = erad_model.Erad_Audit

    for item in audit_table.scan():
    	# Checks for Items without AccountGroup_ID
        if (item.AccountGroup_ID == None) :
        	# Replace it with default AccountGroup_ID
            item.AccountGroup_ID = "elevenos::"+item.Group_ID
            item.save()
            print "Updated Instance:"
            print item.Instance
            sleep(0.05)

if __name__ == "__main__":
    main(sys.argv[1:])
