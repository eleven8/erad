#!/usr/bin/env python

import sys, getopt
import string
import json
import requests
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):  
        
    auth_table = erad_model.Erad_Authenticator

# requires changing
#    RadiusAttribute = UnicodeAttribute(default="called-station-id")
# to
#    RadiusAttribute = UnicodeAttribute(null=True)
#
# also increase the write limit to 100

    for item in auth_table.scan():
        if(item.RadiusAttribute == None):
            item.RadiusAttribute = "called-station-id"
            item.save()
            print "Updated ID:", item.ID
            sleep(0.05)

if __name__ == "__main__":
    main(sys.argv[1:])