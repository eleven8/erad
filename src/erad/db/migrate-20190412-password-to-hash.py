# conding: utf-8
import pdb

from erad import util
from erad.api import erad_model
from erad.api.interface_util import EradInterfaceUtil
from time import sleep

from Crypto.Cipher import AES
from Crypto.Util import randpool
from base64 import urlsafe_b64decode
from base64 import urlsafe_b64encode

import sys, traceback
import argparse

block_size = 16
key_size = 32
mode = AES.MODE_ECB


def padding(key):
	padd_str = (key_size - len(key) % key_size) * "0"
	return key + padd_str


def decr(data, padded_key):
	cipher = AES.new(padded_key, mode)
	return cipher.decrypt(urlsafe_b64decode(data.encode('utf-8')))


def password_based_checksum(password, key):
	padding = lambda s: s + (key_size - len(s) % key_size) * "0"
	key_bytes = padding(key)

	pad = block_size - len(password) % block_size
	data = password + pad * chr(pad)

	encrypted_bytes = AES.new(key_bytes, mode).encrypt(data)
	encrypted_string = urlsafe_b64encode(str(encrypted_bytes))

	return encrypted_string


def unpadd(decrypted_str):
	str_len = -len(decrypted_str)
	i = -1
	while i >= str_len:
		if ord(decrypted_str[i]) > block_size :
			return (decrypted_str, decrypted_str[:i+1])
		i -= 1
	return (decrypted_str, '')




# SITEWIDE_SALT_SECRET_KEY - should be added/edited in config /erad.cfg or in config function
# without it salt will be corrupted
def main(SITEWIDE_SALT_SECRET_KEY = None, __SALT = None):
	""" SITEWIDE_SALT_SECRET_KEY - secret key to mix with salt
	__SALT - this argument only for tests, Usually salt generated with random value,
	but for test uses this argument to control salt value
	"""
	interface_util = EradInterfaceUtil()
	try:
		print('\n')
		print('-'*80)
		print("Account_ID;Stored_Pass;ReEncrypted_Pass;Plaintext_Pass;Real_Pass_Hash;CurrentSalt;NewSalt;")
		for record in erad_model.Erad_AccountOwner.scan():

			if record.Salt is None or record.Salt == 'SALT-PLACEHOLDER':
				current_salt = record.Salt
				stored_pass = record.Password
				padded_key = padding(record.Account_ID)
				decrypted_pass = decr(record.Password, padded_key)
				(padded_pass, unpadded_pass) = unpadd(decrypted_pass)
				reencrypted_pass = password_based_checksum(unpadded_pass, record.Account_ID)

				# sane check - to be sure what decrypted password correct
				assert(reencrypted_pass == record.Password)


				# IMPORTANT NOTE, PASSWORD HASH WILL BE SALTED
				# And salt will be unique each time when account save
				# so there is no way (in theory and assuming hash function
				# is strong against collisions) to get two equal password hash.
				# Therefore there will be unique values in printed table.
				NewSalt = __SALT or util.get_salt()

				real_pass_hash = interface_util.pass_hash( unpadded_pass,
														   NewSalt,
														   SITEWIDE_SALT_SECRET_KEY)


				record.Salt = NewSalt
				record.Password = real_pass_hash
				record.save()


			else :
				current_salt = record.Salt
				stored_pass = '---ALREADY PROCESSED---'
				reencrypted_pass = '---ALREADY PROCESSED---'
				unpadded_pass = '---ALREADY PROCESSED---'
				real_pass_hash = record.Password
				current_salt = record.Salt
				NewSalt = '---ALREADY PROCESSED---'


			print(  '{Account_ID};{Stored_Pass};{ReEncrypted_Pass};{Plaintext_Pass}'
					';{Real_Pass_Hash};{CurrentSalt};{NewSalt};'.format(
				Account_ID = record.Account_ID,
				Stored_Pass = stored_pass,
				ReEncrypted_Pass = reencrypted_pass,
				Plaintext_Pass = unpadded_pass,
				Real_Pass_Hash = real_pass_hash,
				CurrentSalt = current_salt,
				NewSalt = NewSalt
			))


	except Exception as e:
		print ""
		print "*****"
		print str(e)
		traceback.print_exc(file=sys.stdout)
		print "*****"
		print ""


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Migrate password from encrypt to hashed")
    parser.add_argument("sitewide_salt_secret_key", help="Sitewide secret key for salt, example: `sitewide-secret-key`")
    args = parser.parse_args()


    main(
        SITEWIDE_SALT_SECRET_KEY = args.sitewide_salt_secret_key,
    )
