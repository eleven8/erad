#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main():
    # Get Arguments
    ProvidedGroup_ID = sys.argv[1]
    NewAccount_ID = sys.argv[2]

    try:
        exists = erad_model.Erad_Group2.get("elevenos", ProvidedGroup_ID)

        print "Copy Group"
        # items exists save a clone of that with new account id
        erad_model.Erad_Group2(ID=exists.ID, Name=exists.Name, RemoteServerUrl=exists.RemoteServerUrl, SharedSecret=exists.SharedSecret, TimeZone=exists.TimeZone, Account_ID=NewAccount_ID).save()
        print "Copied: " + exists.Account_ID + "::" + exists.ID + " as " + NewAccount_ID+'::'+ProvidedGroup_ID

        print "\nSave Supplicants"
        # if item found in Erad_Group2 loop through all Supplicants
        erad_supplicant = erad_model.Erad_Supplicant

        # if supplicant is eleneos::ProvidedGroup_ID save a clone of this
        for item in erad_supplicant.query("elevenos::"+ ProvidedGroup_ID):
            # if supplicant is eleneos::ProvidedGroup_ID save a clone of this
            # save with new account id
            erad_model.Erad_Supplicant(NewAccount_ID+'::'+ProvidedGroup_ID, item.Username, Password=item.Password, DeviceName=item.DeviceName, Description=item.Description, Location=item.Location, ExpirationDate=item.ExpirationDate, Vlan=item.Vlan, UseRemote=item.UseRemote, UseWildcard=item.UseWildcard).save()
            print "Updated ID: " + NewAccount_ID+'::'+ProvidedGroup_ID + " - " +item.Username
            sleep(0.05)

    except Exception, e:
        print ""
        print "*****"
        print str(e)
        print "*****"
        print ""

if __name__ == "__main__":
    main()
