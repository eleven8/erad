#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):

    group_table = erad_model.Erad_Group2

    for item in group_table.scan():
        item.delete()

if __name__ == "__main__":
    main(sys.argv[1:])
