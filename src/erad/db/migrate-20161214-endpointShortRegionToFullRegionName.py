#!/usr/bin/env python

import sys, getopt
from erad import util
from erad.api import erad_model
from time import sleep

def main(argv):

    endpoint_table = erad_model.Erad_Endpoint

    for item in endpoint_table.scan():
    	if(item.Region == "us-west"):
            item.Region = "us-west-2"
            item.save()
            print "Cluster ID:", item.ClusterId, " IpAddress:", item.IpAddress, " Port:", item.Port
            sleep(0.05)

if __name__ == "__main__":
    main(sys.argv[1:])