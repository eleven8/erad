#!/usr/bin/env python

from erad import util
from erad.api import erad_model

def main():
    sup_table = erad_model.Erad_Endpoint
    batch_count = 0
    endpoints = []
    with sup_table.batch_write() as batch:
        for endpoint in sup_table.scan(Group_ID__null=True):
            endpoint.Group_ID = "::free"
            endpoints.append(endpoint)
            batch_count += 1

            if batch_count >= 1000:
                for endpoint in endpoints:
                    batch.save(endpoint)
                batch_count = 0
                endpoints = []

        if batch_count > 0:
            for endpoint in endpoints:
                batch.save(endpoint)

if __name__ == "__main__":
    main()
