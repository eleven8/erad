#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad import util, CoreTest
from erad.exceptions import *
import boto.ec2
import re
import logging
import random
import mock
from os import path
import json

# Accessing tags with dot notation used in util.get_instance_name()
class MockDict(dict):
    __getattr__= dict.__getitem__
    def __init__(self, instances):
        self.instances = instances

    def __getattr__(self, attr):
        return self.instances.get(attr)



class UtilTest(CoreTest):

    # need:  is_valid_proxy
    # need:  parse_ipaddr
    # need:  parse_port
    # need:  uncompress_mac

    # def setUp(self):
        # self.all_instances = util._get_ec2_instances()
        # self.logger = logging.getLogger(__name__)

    def test_sanitize_mac(self):
        mac_good = "AA:B2:CC:D4:EE:F6"

        test_mac = "aa:b2:cc:d4:ee:f6"
        self.assertEqual( util.sanitize_mac(test_mac), mac_good )

        test_mac = "AA-B2-CC-D4-EE-F6"
        self.assertEqual(util.sanitize_mac(test_mac), mac_good )

        # white space stripped
        test_mac = "   AA-B2-CC-D4-EE-F6    "
        self.assertEqual(util.sanitize_mac(test_mac), mac_good )

        test_mac = "i2-:Dd2!&#$$@F"
        self.assertRaisesRegexp(EradValidationException,"Invalid Mac",util.sanitize_mac,test_mac)

        test_mac = "qq:b2:cc:d4:ee:f6"
        self.assertRaisesRegexp(EradValidationException,"Invalid Mac",util.sanitize_mac,test_mac)

        expected = "00:00:00:00:00:00"
        # unchanged
        self.assertEqual( expected, util.sanitize_mac(expected) )

        expected = "10:DC:B1:A4:FF:E9"
        # unchanged
        self.assertEqual( expected, util.sanitize_mac("10:DC:B1:A4:FF:E9") )
        # lowercase
        self.assertEqual( expected, util.sanitize_mac("10:dc:b1:a4:ff:e9") )
        # mixed case
        self.assertEqual( expected, util.sanitize_mac("10:dc:b1:a4:FF:E9") )
        # dashes
        self.assertEqual( expected, util.sanitize_mac("10-dc-b1-a4-FF-E9") )
        # compressed
        self.assertEqual( expected, util.sanitize_mac("10dcb1a4FFE9") )

    def test_sanitize_mac_ssidname(self):
        test_mac = "aa:b2:cc:d4:ee:f6:ssidnameOkay"
        expected_mac = 'AA:B2:CC:D4:EE:F6'

        self.assertEqual( util.sanitize_mac(test_mac), expected_mac )

        test_mac = "AA-B2-cc-D4-EE-F6:ssidnameOkay"
        self.assertEqual( util.sanitize_mac(test_mac), expected_mac )

        # white space stripped
        test_mac = "  AA:B2:CC:D4:EE:F6  :  ssidnameOkay   "
        self.assertEqual( util.sanitize_mac(test_mac), expected_mac )

        # ssid is ignored if blank
        test_mac = "AA-B2-cc-D4-EE-F6: "
        self.assertEqual( util.sanitize_mac(test_mac), expected_mac )

    def test_parse_cluster_instances(self):
        self.assertEqual(util.parse_cluster_instances("not_a_cluster", []), {'auth': {}, 'acct': {}})

        # Minimal version of boto Instance objects
        instances = [
            {
            'id': u'i-01ba0a19517196d1c',
            'tags': {u'Name': u'test-instance-uswest-01-acct'},
            'ip_address': u'52.39.140.137'
            },
            {
            'id': u'i-01ba0a19517196d1b',
            'tags': {u'Name': u'test-instance-uswest-01-auth'},
            'ip_address': u'52.39.140.138'
            }
        ]

        instances_list = [
            MockDict(instances[0]),
            MockDict(instances[1])
        ]

        parsed = util.parse_cluster_instances('uswest-01', instances_list)
        return_value = {
            'acct': {u'i-01ba0a19517196d1c': u'52.39.140.137'},
            'auth': {u'i-01ba0a19517196d1b': u'52.39.140.138'}
        }

        self.assertEqual(parsed, return_value)


    def test_default_config_match_dev(self):
        cur_dir = path.dirname(__file__)
        api_dir = path.join(path.dirname(cur_dir), 'api')
        frmod_dir = path.join(path.dirname(cur_dir), 'frmod')

        files_to_compare = [
            path.join(api_dir, 'erad.cfg.dev'),
            path.join(api_dir, 'erad.cfg.prod'),
            path.join(frmod_dir, 'erad.cfg.dev'),
            path.join(frmod_dir, 'erad.cfg.prod.plaintext'),
        ]

        for filepath in files_to_compare:
            with open(filepath) as f:
                file_data = json.load(f)
                self.compare_only_keys(file_data, util.default_cfg)
                # self.assertDictEqual(file_data, util.default_cfg)


    def compare_only_keys(self, test_dict, target_dict):
        self.assertTrue(type(test_dict) == dict)
        self.assertTrue(type(target_dict) == dict)

        target_dict_keys = set(target_dict.keys())

        for key, value in test_dict.items():
            self.assertTrue(key in target_dict_keys)
            if type(value) == dict:
                self.compare_only_keys(value, target_dict[key])


    def test_add_wild_card(self):
        mac = 'AA:BB:CC:DD:EE:FF'

        expected = [
            'AA:BB:CC:*',
            'AA:BB:CC:^',
            'AA:BB:CC:DD:*',
            'AA:BB:CC:DD:^',
            'AA:BB:CC:DD:EE:*',
            'AA:BB:CC:DD:EE:^',
        ]
        not_expected = [
            'AA:BB:CC:1',
            'AA:BB:CC:^',
            'AA:BB:CC:DD:*',
            'AA:BB:CC:DD:^',
            'AA:BB:CC:DD:EE:*',
            'AA:BB:CC:DD:EE:^',
        ]
        result_list = util.add_wild_card(mac)
        self.assertEqual(expected, result_list)
        self.assertNotEqual(not_expected, result_list)

    def test_valid_expected_mac(self):
        valid_list = [
            'aabbccddeeff',
            'aa:bb:cc:dd:ee:ff',
            'AA:BB:CC:DD:EE:FF',
            'aa-bb-cc-dd-ee-ff',
            'AA-BB-CC-DD-EE-FF',
            'aa:bb-cc:dd:ee-ff',
            'aabbcc:ddeeff',
            'aAbBCc:dDEeFf',
            'aAbBCcdDEeFf',
        ]

        for item in valid_list:
            self.assertTrue(util.is_mac_address(item))

        invalid_list = [
            'zabbccddeeff',
        ]

        for item in invalid_list:
            self.assertFalse(util.is_mac_address(item))

    def test_mask_identifier(self):
        ids = [
            '',
            '1',
            '1234',
            '12345',
            '123456',
            '1234567890',
            'abcd_12345678901_efgh',
        ]
        expected_values = [
            '*',
            '*',
            '***4',
            '***5',
            '***56',
            '***90',
            'ab***fgh',
        ]

        for input_value, expected_value in zip(ids, expected_values):
            self.assertEqual(util.mask_identifier(input_value), expected_value)


if __name__ == "__main__":
    unittest.main()
