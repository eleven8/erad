#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
from base64 import b64encode
from datetime import datetime, timedelta
import dateutil.parser as parser
import pytz
import re
import boto
import boto.ec2
import json
from boto.s3.key import Key
from . import exceptions
import random
import string
from os import environ
from copy import deepcopy

from validate_email import validate_email

from hashlib import sha256
from os import urandom
import logging
logger = logging.getLogger(__name__)


erad_configuration = None

default_cfg = {
    "system_key": ["r_7X385PDgqPSB2cMvpmPZ6GHqXh5Cd8ZF"],
    "billing_key": ["b_kWLlOeY1HT86FPwX3mVbPuZup6uA8JTG"],
    "auth_sns_topic": None,
    "acct_sns_topic": None,
    "supplicant_sns_topic": None,
    "role_arn": None,
    "role_profile": "monitor_write",
    "api": {
        "redis_host": "localhost",
        "redis_port": "6379",
        "host": "http://0.0.0.0:5000",
        "allow_internal": True,
        "password_check": "https://api.pwnedpasswords.com/range/",
        "password_check_maximum_count_allowed": 1,
        "session": {
            "retries": 1,
            "timeout": 0
        },
    },
    "database" : {
        "region" : "us-west-2",
        "host" : "http://localhost:8000",
        "streams_host": "http://localhost:8000"
    },
    "proxy_conf": {
        "can_write" : False,
        "can_read" : False,
        "bucket": None,
        "filename": "proxy.conf",
        "radiusconf_key": "radiusconf",
        "s3_region": "us-west-2",
        "dev_aws_access_key_id": ["AKIAJTK5BJ3KH2DRVTUQ", "foo"]
    },
    "audit": {
        "admin" : {
            "group": True,
            "supplicant": True,
            "authenticator": True,
            "endpoint": True,
            "account": True,
            "apikey": True
        },
        "integration" : {
            "session": True
        }
    },
    "crypto_params" : {
        'salt_bytes_lengh' : 4,  # 4 bytes = 32 bits - NIST guidelines

        # REDEFINE IT FOR PRODUCTION,
        'sitewide_salt_secret_key': 'secret_key_dev',

        # RESULT salt will be hash(sitewide_salt_secret_key + salt)
        # by NIST https://pages.nist.gov/800-63-3/sp800-63b.html#memsecretver
        "iterations": 100000,
        "digest":  sha256().name
    },
    "s3_gc_max_age": 120, # maximum file age in s3
}

def erad_cfg():
    """Load configuration by combining `default_cfg` and file from /etc/erad.cfg. Which is json file.
    Error of reading/loading json file will be reported but will not abort program execution.
    If it is necessary to disable (set some value to None/null) None should be set in `default_cfg` and null in /etc/erad.cfg
    """
    global erad_configuration

    if not erad_configuration is None:
        return erad_configuration

    # specify defaults
    cfg = deepcopy(default_cfg)
    # print(cfg)
    # try override defaults with machine config
    try:
        cfg.update( json.load(open("/etc/erad.cfg")) or {} )
    except Exception as err:
        logger.error("Error during parsing of erad.cfg %s", err)
        # debug
        # print('"/etc/erad.cfg" not found, using default')
        pass
    # convert dict to object so it can be referenced
    # as cfg.region instead of cfg["region"]
    erad_configuration = deep_convert_dict_to_object(cfg)



    TEST_API_HOST = environ.get('TEST_API_HOST', None)
    TEST_REDIS_HOST = environ.get('TEST_REDIS_HOST', None)
    TEST_DYNAMODB_HOST = environ.get('TEST_DYNAMODB_HOST', None)
    TEST_DYNAMODB_REGION = environ.get('TEST_DYNAMODB_REGION', None)

    if TEST_API_HOST is not None:
        erad_configuration.api.host = TEST_API_HOST

    if TEST_REDIS_HOST is not None:
        erad_configuration.api.redis_host = TEST_REDIS_HOST

    if TEST_DYNAMODB_HOST is not None:
        erad_configuration.database.host = TEST_DYNAMODB_HOST
        erad_configuration.database.streams_host = TEST_DYNAMODB_HOST

    if TEST_DYNAMODB_REGION is not None:
        erad_configuration.database.region = TEST_DYNAMODB_REGION

    # debug
    # print(dir(erad_configuration.api))
    return erad_configuration

def deep_convert_dict_to_object( d ):
    for k in d:
        if type(d[k]) is dict:
            d[k] = deep_convert_dict_to_object( d[k] )
    return type("", (), d)()

def from_isoformat( str ):
    # 2015-04-09T06:23:57+00:00
    return parser.parse(str)

def utc_isoformat( utc ):
    # 2015-04-08T00:00:00Z
    return utc.strftime("%Y-%m-%dT%H:%M:%SZ")

def day_format( date ):
    return date.strftime("%Y-%m-%d")

def local_to_utc( dt, str_tz ):
    tz = pytz.timezone(str_tz)
    try:
        # this can raise exception if time is ambiguous
        aware = tz.normalize(tz.localize(dt))
    except:
        # choose standard time
        aware = tz.normalize(tz.localize(dt, is_dst=False))
    return aware.astimezone(pytz.utc)

def utc_to_local( utc, str_tz ):
    tz = pytz.timezone(str_tz)
    utc = utc.replace(tzinfo=pytz.utc)
    return utc.astimezone(tz)

def utcnow():
    return datetime.utcnow()

def now_isoformat():
    return utc_isoformat( datetime.utcnow() )

def now_ptzformat():
    return utc_to_local( datetime.utcnow(), 'US/Pacific' ).strftime("%Y-%m-%d %I:%M %p")

def today( str_tz ):
    return utc_to_local( datetime.utcnow(), str_tz )

def today_in_day_format( str_tz ):
    return day_format( today( str_tz ) )

# timedelta function to add support for python 2.6
def timedelta_total_seconds(timedelta):
    return (
        timedelta.microseconds + 0.0 +
        (timedelta.seconds + timedelta.days * 24 * 3600) * 10 ** 6) / 10 ** 6

def datetime_to_milliseconds_since_epoch( dt ):
    epoch = datetime.utcfromtimestamp(0).replace(tzinfo=pytz.utc)
    delta = dt - epoch
    return int(timedelta_total_seconds(delta) * 1000.0)


def session_time_is_expired(time):
    if time is None:
        return False
    threshold = (utcnow() - timedelta(hours=1))
    return time.replace(tzinfo=pytz.utc) < threshold.replace(tzinfo=pytz.utc)

def parse_human_date_str( s ):
    return datetime.strptime( s, "%m/%d/%Y" )

def to_human_date_str( dt ):
    return dt.strftime( "%m/%d/%Y" )

def parse_ipaddr( proxy ):
    return proxy.rsplit(":",1)[0]

def parse_port( proxy ):
    try:
        return int(proxy.rsplit(":",1)[1])
    except:
        # default port
        return 1812

def parse_shared_secret( shared_secret ):
    return shared_secret or "4905DVCA3B"

def construct_proxy_id( RemoteServerUrl ):
    return parse_ipaddr(RemoteServerUrl).replace(".","_") + "__" + str(parse_port(RemoteServerUrl))

def is_valid_proxy( proxy ):
    return ('52.11.20.214' not in (proxy or "")) and re.search( "\.", proxy or "" )

def is_valid_ipaddr( ipaddr ):
    return re.search( "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", ipaddr or "" )

def is_valid_email(email):
    return validate_email(email)

mac_addr_regex = re.compile(r'^([0-9a-fA-F][0-9a-fA-F][:-]{0,1}){5}([0-9a-fA-F][0-9a-fA-F])$')
def is_mac_address(input_str):
    return re.search(mac_addr_regex, input_str) is not None


# ensures 6 sets of hex numbers separated by : or -
def is_valid_mac( mac ):
    return re.match("[0-9a-f]{2}([-:])[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", mac.lower())

def is_valid_wildcarded_mac(mac):
    allowed = set('0123456789abcdef^*:-')
    if all(c in allowed for c in mac.lower()):
        if is_wild_carded(mac):
            tmp_mac = mac.upper().replace("-",":").strip()
            mac_segments = tmp_mac.split(":")
            for segment in mac_segments:
                if len(segment) != 2 and segment != "^":
                    return False
            return len(mac_segments) >= 4
    return False

# ensures 12 chars of hex and numbers formatting 112233445566
def is_valid_mac_hex12( mac ):
    return re.match("^[0-9a-f]{12}$", mac.lower())

def sanitize_proxy( proxy ):
    try:
        if is_valid_proxy( proxy ):
            ip = parse_ipaddr( proxy )
            port = parse_port( proxy )
            if is_valid_ipaddr( ip ):
                return ip + ":" + str(port)
    except:
        pass
    return None

def uncompress_mac( mac ):
    chars = list( mac )
    for i in range(2,15,3):
        chars.insert(i,':')
    return ''.join(chars)

# checks mac validity and converts to uppercase and colons
def sanitize_mac( calledId ):
    if calledId is None:
        return calledId
    calledId = calledId.strip()

    # uncompress
    if len(calledId) == 12:
        calledId = uncompress_mac(calledId)

    ssidName = ""

    # if we have an extra colon, its a wifi SSID name and we need to strip it out.
    if calledId.count(":") == 1 or calledId.count(":") == 6:
        calledId, ssidName = calledId.rsplit(':', 1)
        ssidName = ssidName.strip()

    calledId = calledId.upper().replace("-",":").strip()

    if not is_valid_mac(calledId):
        raise exceptions.EradValidationException('Invalid Mac')

    return calledId

def wild_sanitize_mac( calledId ):
    """Sanitizes MAC address considering wildcard character. """
    try:
        sanitized_mac = sanitize_mac(calledId)
        return sanitized_mac
    except exceptions.EradValidationException as e:
        if is_wild_carded(calledId) and not is_valid_wildcarded_mac(calledId):
            raise e
        if is_valid_wildcarded_mac(calledId):
            sanitized_mac = calledId.upper().replace("-",":").strip()
            return sanitized_mac
        raise e

def is_wild_carded(mac):
    """Checks if there is a ^ or * in the MAC and if the 3 first segments are provided."""

    # removing escaped symbols, to make sure that that `mac` variable contians wildcard symbols
    tmp_mac = mac.replace(r'\^', '')
    tmp_mac = tmp_mac.replace(r'\*', '')

    return '^' in tmp_mac or '*' in tmp_mac

def add_wild_card(mac):
    """Gets possible wildcarded macs for a given mac.
    If mac is a complete MAC address such as AA:BB:CC:DD:EE:FF, this function will return
    ['AA:BB:CC:^', 'AA:BB:CC:DD:^', 'AA:BB:CC:DD:EE:^']
    """
    possible_wildcards = []
    aux_mac = mac
    while len(aux_mac.split(":")) >= 4:
        len_aux_mac = len(aux_mac.split(":"))
        mac_part = ':'.join(aux_mac.split(":")[:len_aux_mac -1])
        possible_wildcards.append(mac_part + ':^')
        possible_wildcards.append(mac_part + ':*')
        aux_mac = aux_mac[:-3]
    possible_wildcards.reverse()
    return possible_wildcards


def read_string_from_s3( bucket_name, key ):
    connection = boto.connect_s3()
    try:
        bucket = connection.get_bucket(bucket_name)
        s3file = Key(bucket)
        s3file.key = key
        return s3file.get_contents_as_string()
    finally:
        connection.close()

def write_string_to_s3( bucket_name, key, contents ):
    connection = boto.connect_s3()
    try:
        bucket = connection.get_bucket(bucket_name)
        s3file = Key(bucket)
        s3file.key = key
        return s3file.set_contents_from_string( contents )
    finally:
        connection.close()

def read_proxy_conf_to_s3():
    pc = erad_cfg().proxy_conf
    return read_string_from_s3( pc.bucket, pc.filename )

def write_proxy_conf_to_s3( contents ):
    pc = erad_cfg().proxy_conf
    return write_string_to_s3( pc.bucket, pc.filename, contents )

def generate_random_string(size=20):
    """Generates a random string with 20 characters of length by default."""
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(size))


def load_client_arns(data):
    """ Loads region mapping from string object """

    try:
        if data is None:
            return None, None
        arn_map = json.loads(data)
        if type(arn_map) != dict:
            logger.warning("Expected key-value mapping from json data in ClientInfoArn")
            return None, None
    except Exception as e:
        logger.error(e)
        return None, None
    return arn_map.popitem()

def get_ec2_instances(region):
    """
    Fetches all production instances for a region.
    Default region is us-west-2.
    """
    client = boto.ec2.connect_to_region(region)#, aws_access_key_id=ACCESS_KEY_ID, aws_secret_access_key=ACCESS_KEY)
    reservations = client.get_all_instances(
        filters={'instance-state-name': 'running'}
    )

    instances = []
    for reservation in reservations:
        instances.extend([instance for instance in reservation.instances])
    return instances

def get_instance_name(instance):
    """
    Gets the name of the instance.
    """
    name = instance.tags.get('Name')
    return name

def get_radius_instance_type(tag_name):
    """
    Checks the type of a erad radius instance.
    """
    AUTH = 'auth'
    ACCT = 'acct'
    INSTANCE_TYPES = [
        AUTH,
        ACCT
    ]

    for instance_type in INSTANCE_TYPES:
        if instance_type in tag_name:
            return instance_type

def is_right_cluster(instance_name, cluster_id):
    """
    Checks if the typical prefix for an erad radius instance with the given
    cluster_id is present in the instance name.
    """
    BASE_PREFIX = 'erad-radius'
    prefix = '{base_prefix}-{cluster_id}'.format(
        base_prefix=BASE_PREFIX,
        cluster_id=cluster_id
    )

    matching_prefix = prefix in instance_name
    return matching_prefix

def parse_cluster_instances(cluster_id, instances_list):
    """
    This function looks for the AWS EC2 instances with the cluster_id in their names and build a set of
    acct and auth instances for a cluster with their IPs.
    """
    instances = {
        'auth': {},
        'acct': {}
    }

    for instance in instances_list:
        tag_name = get_instance_name(instance)
        if tag_name is not None and cluster_id in tag_name:
            instance_type = get_radius_instance_type(tag_name)
            if instance_type:
                instance_id, instance_ip = instance.id, instance.ip_address
                instances.get(instance_type).update({instance_id: instance_ip})

    return instances

_erad_cfg = erad_cfg()

def get_salt():
    return b64encode(urandom(_erad_cfg.crypto_params.salt_bytes_lengh)).decode('utf-8')


def none_to_false(js_obj, fields_set):
    for filed_name in fields_set:
        if js_obj[filed_name] is None:
            js_obj[filed_name] = False

    return js_obj


def mask_identifier(private_identifier):
    identifier_len = len(private_identifier)
    if identifier_len in {0,1}:
        return '*'
    if identifier_len <= 5:
        return '***' +  private_identifier[-1:]

    if identifier_len <= 10:
        return '***' + private_identifier[-2:]

    return ''.join([private_identifier[:2], '***', private_identifier[-3:]])


def get_log_instance_time(dt: datetime) -> int:
    """
    Instance = NumberAttribute(range_key=True) in Erad_Radius_Log field stored values,
    that roughly can be described as seconds 10^-4 decimilliseconds.
    This function convert datetime object to such dimension.

    It is intended to used in log queries. Separate function is necessary because `make_unique_instance()`
    add a random value, this may affect query.
    :param dt:
    :return:
    """

    return int(dt.timestamp()) * 10000


OFFSET_SIZE = int(10000)


def instance_pack(seconds_since_epoch, value):
    """
    :param seconds_since_epoch: UNIX timestamp (seconds
    :param value: random value
    :return:
    """
    # seconds becomes the most significant digits
    # value is mapped to the range 0 to (offset_size-1)
    # these values can then be added together without loss of information
    return seconds_since_epoch * OFFSET_SIZE + int(value * OFFSET_SIZE)


def instance_unpack(instance):
    seconds_since_epoch = int(instance) / OFFSET_SIZE
    value = float((int(instance) % OFFSET_SIZE)) / float(OFFSET_SIZE)
    return seconds_since_epoch, value


def make_unique_instance(time):
    """
    :param time: UNIX timestamp (seconds)
    :return:
    """
    return instance_pack(int(time), random.random())
