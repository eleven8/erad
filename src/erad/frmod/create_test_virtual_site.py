# coding: utf-8

import argparse
from loadVS import display as default_display
from loadvs_for_tests import display as onlyeap_display


parser = argparse.ArgumentParser(description='Create virtual sites for testing')

parser.add_argument('sitetype', type=str,
                    help='[default, onlyeap]')

args = parser.parse_args()

available_types = {'default', 'onlyeap'}

if args.sitetype == 'default':
	print((default_display('0.0.0.0', '1812', '4905DVCA3B', None)))
elif args.sitetype == 'onlyeap':
	print((onlyeap_display('0.0.0.0', '1812', '4905DVCA3B', None)))
else:
	print(('This type {0} not known, use [default, onlyeap]'. format(args.sitetype)))
