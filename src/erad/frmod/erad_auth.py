#! /usr/bin/env python3
import json
import requests

from ..exceptions import *
from .. import util

# for radiusd look into erad/frmod/raddb/mods-config/python3/radiusd.py
import radiusd
import socket
import redis

# During authentication radius server usually send many radius requests with same credentials, especially
# for requests with EAP. By default each request will be processed on ERAD API server. It is not strictly necessary.
# Because for example first N requests with EAP will have anonymous credentials and only after EAP session will
# be established, supplication will send real credentials.
# Another case, supplicants ('AAlarm') may "spam" with authentication requests, because of the number of devices
# and network architecture where they deployed.
# To improve performance for situations like these, using caching.
REDIS_CACHE_TTL = 10


# The list of custom attributes live in the dictionary in the root of raddb directory.
# `raddb/dictionary`, here it is repeated for continence
#
# ATTRIBUTE   Supplicant-User-Name            3000    string
# ATTRIBUTE   Supplicant-Group                3001    string
# ATTRIBUTE   Supplicant-Password             3002    string
# ATTRIBUTE   Supplicant-Secret               3003    string
# ATTRIBUTE   Supplicant-Vlan                 3004    string
# ATTRIBUTE   Supplicant-Proxy-Id             3005    string
# ATTRIBUTE   Supplicant-Use-Remote           3006    string
# ATTRIBUTE   Supplicant-Use-Mac-Auth         3007    string
# ATTRIBUTE   Auth-Control                    3008    string
# ATTRIBUTE   Supplicant-Inner-User-Name      3009    string
# ATTRIBUTE   Supplicant-Real-User-Name       3010    string
# ATTRIBUTE   ERAD-API-RESULT                 3011    string
# ATTRIBUTE   Supplicant-Auth-Type            3012    string
# ATTRIBUTE   Supplicant-Timestamp            3013    string
# ATTRIBUTE   Supplicant-Packet-Type          3014    string
# ATTRIBUTE   Supplicant-Auth-Result          3015    string
# ATTRIBUTE   Supplicant-Parent-Id	          3016    string


# -----------------------------------------------------------------------------------------------------------------

# Freradius server usually uses Cleartext-Password attribute to authenticate users,
# and sometimes "Auth-Type := Accept" expression to authenticate user without Cleartext-Password,
# for example EAP-TLS with only certificate.
# So in general if Freeradius servers does not know Cleartext-Password, authorization requests
# will be rejected. Therefore supplicant passwords in *general case* should be stored in databases
# in format, that will allow to get plaintext passwords (not an irreversible hashing).
# Read this http://deployingradius.com/documents/protocols/compatibility.html for more information
# about how to store passwords


# This module uses the following logic.

# If it find information about supplicant by supplicant username, it will return
# supplicant information including Cleartext-Password. Freeradius server will compare
# Cleartext-Password with credentials provided by supplicant, and authenticate request if its match.
# The rest of returned information will be used to form final user reply.

# If module will not find information in database about supplicant, by supplicant username.
# It will return nothing - freeradius server will not know the Cleartext-Password
# and will reject supplicant request.
# RLM_MODULE_FAIL

# If something went wrong, for example network problem or ERAD API return error or malformed response,
# module will return RLM_MODULE_FAIL. Freeradius stops processing request and will return Access-Reject reply.

# Freeradius in `authorize` section allows assign values to `Auth-Type` attributes many times.
# But in the `authenticate` section will use only the last one.
# https://github.com/FreeRADIUS/freeradius-server/blob/v3.0.x/src/main/auth.c#L197
# To set `Auth-Type = ERAD` (Customized MAC authentication), module uses Supplicant-Auth-Type.
# This attributes will store `Auth-Type` value until the end of processing the `authorize` section.
# And then value will be copied to `Auth-Type` attribute. This is to make sure,
# that `Auth-Type` will not be overwritten by other modules.

# tuples that will be passed from freeradius to this module
# vps = (request, reply, config, state, proxy_req, proxy_reply)


class EradAuthException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class EradAuthServiceError(EradAuthException):
    pass


class EradAuthNotFound(EradAuthException):
    pass


class EradAuthSubscriberSuspended(EradAuthException):
    pass


authorize_available_attrs = {
    'reply': {
        'Supplicant-Use-Remote',
        'Supplicant-Proxy-Id',
        'Supplicant-User-Name',
    },
    'control': {
        'Cleartext-Password',
        'Supplicant-Auth-Type'
    }
}

request_attr_dict = {
    'User-Name': 'UserName',
    'Called-Station-Id': 'CalledStationId',
    'Calling-Station-Id': 'CallingStationId',
    'Event-Timestamp': 'EventTimestamp',
    'Supplicant-Timestamp': 'Timestamp',
    'NAS-Identifier': 'NASIdentifier',
    'NAS-IP-Address': 'NASIPAddress',
    'Supplicant-Radius-Ip': 'EndpointIPAddress',
    'Supplicant-Radius-Port': 'EndpointPort',
    'Supplicant-Packet-Type': 'PacketType',
    'Supplicant-Auth-Result': 'Access'

}

reply_attr_dict = {
    'Supplicant-Real-User-Name': 'UserName',
    'Supplicant-Group': 'Group_ID',
    'Supplicant-Use-Remote': 'IsProxied',
    'Supplicant-Parent-Id': 'Parent_ID',
    'Supplicant-Profile-Used': 'ProfileUsed',
    'Supplicant-Device-Used': 'DeviceUsed',
    'Endpoint-Group-Name': 'EndpointGroupName',
    'Endpoint-Group-ID': 'EndpointGroupID',
    'Supplicant-Custom-Json-Data': 'CustomJsonData'
}

no_sanitize_fields = ['CustomJsonData']

request_attrs_keys = set(request_attr_dict.keys())
reply_attrs__keys = set(reply_attr_dict.keys())

redis_client = None

cfg = util.erad_cfg()

radiusd.radlog(radiusd.L_DBG, f'{cfg.api.redis_host}')
radiusd.radlog(radiusd.L_DBG, f'{cfg.api.redis_port}')

try:
    redis_client = redis.Redis(
        host=util.erad_cfg().api.redis_host,
        port=util.erad_cfg().api.redis_port,
        db=0
    )
except Exception as err:
    radiusd.radlog(radiusd.L_DBG, f'{err}')


def instantiate(vps):
    radiusd.radlog(radiusd.L_DBG, f'*** instantiate ***')
    radiusd.radlog(radiusd.L_DBG, f'{vps}')

    return 0
    # return 0 for success or -1 for failure


def authorize(vps):
    radiusd.radlog(radiusd.L_DBG, "*** authorize ***")
    request_attrs = vps[0]

    try:
        reply_attrs, control_attrs = get_supplicant_info(request_attrs)
        sanity_check(reply_attrs)
        sanity_check(control_attrs)
        reply_attrs, control_attrs = fillter_authorize_attrs(reply_attrs, control_attrs)
        return (radiusd.RLM_MODULE_UPDATED, reply_attrs, control_attrs)

    except EradAuthNotFound as err:
        radiusd.radlog(radiusd.L_DBG, f'{err}')
        return radiusd.RLM_MODULE_NOOP

    except EradAuthSubscriberSuspended as err:
        radiusd.radlog(radiusd.L_DBG, f'{err}')
        return radiusd.RLM_MODULE_REJECT

    except (EradAuthServiceError, Exception) as err:
        radiusd.radlog(radiusd.L_DBG, f'{err}')
        return radiusd.RLM_MODULE_FAIL
    finally:
        radiusd.radlog(radiusd.L_DBG, '***************')


def __get_recoreds_from_tuple(in_attr_tuple, attr_keys, attr_dict):
    log_record = {}

    for (attr_name, attr_value) in in_attr_tuple:
        if attr_name not in attr_keys:
            continue
        sanitized_attr_value = sanitize(attr_value)
        converted_attr_name = attr_dict.get(attr_name)
        log_record[converted_attr_name] = sanitized_attr_value

    return log_record


def get_logs_records_from_request(request_attrs):
    log_record = __get_recoreds_from_tuple(request_attrs, request_attrs_keys, request_attr_dict)

    if 'Called-Station-Id' in log_record:
        log_record['Called-Station-Id'] = log_record['Called-Station-Id'].split(':')[0]
    return log_record


def get_log_records_from_reply(reply_attrs):
    log_record = {
        'IsProxied': 'no'
    }

    for attr_name, op, attr_value in reply_attrs:
        if attr_name not in reply_attrs__keys:
            continue
        converted_attr_name = reply_attr_dict.get(attr_name)

        if converted_attr_name in no_sanitize_fields:
            log_record[converted_attr_name] = attr_value
        else:
            log_record[converted_attr_name] = sanitize(attr_value)

    return log_record


def post_auth(vps):
    """
        Work as authrozie(), but return full set of attrs

        Send logs to save_auth_log_server.py
    """
    radiusd.radlog(radiusd.L_DBG, f'*** post_auth ***')
    # vps[0] because Freeradius pass a tule off all vps, see documentation for rlm_python module
    request_attrs = vps[0]

    try:
        reply_attrs, control_attrs = get_supplicant_info(request_attrs)
        sanity_check(reply_attrs)
        sanity_check(control_attrs)
        save_logs(request_attrs, reply_attrs)
        return (radiusd.RLM_MODULE_UPDATED, reply_attrs, control_attrs)

    except EradAuthNotFound as err:
        save_logs(request_attrs, tuple())
        radiusd.radlog(radiusd.L_DBG, f'{err}')
        return radiusd.RLM_MODULE_NOOP

    except (EradAuthServiceError, Exception) as err:
        save_logs(request_attrs, tuple())
        radiusd.radlog(radiusd.L_DBG, f'{err}')
        return radiusd.RLM_MODULE_FAIL

    finally:
        radiusd.radlog(radiusd.L_DBG, '***************')


def save_logs(request_attrs, reply_attrs):
    log_record = {
        'PacketType': '',
        'Parent_ID': '',
        'UserName': '',
        'Group_ID': '',
        'CalledStationId': '',
        'CallingStationId': '',
        'EventTimestamp': '',
        'NASIdentifier': '',
        'NASIPAddress': '',
        'Timestamp': '',
        'Access': '',
        'IsProxied': '',
        'EndpointIPAddress': '',
        'EndpointPort': '',
        'ProfileUsed': '',
        'DeviceUsed': '',
        'EndpointGroupName': '',
        'EndpointGroupID': '',
        'CustomJsonData': ''
    }

    request_part = get_logs_records_from_request(request_attrs)
    reply_part = get_log_records_from_reply(reply_attrs)
    log_record.update(request_part)
    log_record.update(reply_part)
    raw_log = json.dumps(log_record)
    send_raw_auth_log(raw_log.encode('utf-8'))


def send_raw_auth_log(raw_log):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 11812))
    s.sendall(raw_log)
    radiusd.radlog(radiusd.L_DBG, f'{s.recv(1024)}')
    s.close()


def send_raw_acct_log(raw_log):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 11813))
    s.sendall(raw_log)
    radiusd.radlog(radiusd.L_DBG, f'{s.recv(1024)}')
    s.close()


# Assumption: when authenticate is called, it has already been settled that the user in question
# has been authorized. If mac auth is detected, we accept.  Note: in the authenticate step, we can
# outright reject, as only one authentication step is called per packet process.
def authenticate(vps):
    radiusd.radlog(radiusd.L_DBG, '*** authenticate ***')
    request_attrs = vps[0]
    try:
        user_name = ''
        calling_station_id = ''
        for att in request_attrs:
            if att[0] == 'User-Name':
                user_name = util.sanitize_mac(sanitize(att[1]))
            elif att[0] == 'Calling-Station-Id':
                calling_station_id = sanitize(att[1])
        if user_name == util.sanitize_mac(calling_station_id):
            return radiusd.RLM_MODULE_OK

        return radiusd.RLM_MODULE_REJECT
    except:
        return radiusd.RLM_MODULE_REJECT
    finally:
        radiusd.radlog(radiusd.L_DBG, '***************')


def sanity_check(the_tuple):
    for sub_tuple in the_tuple:
        if len(sub_tuple) != 3:
            radiusd.radlog(radiusd.L_DBG, '*** ERAD_AUTH RETURNED TUPLE HAS WRONG SIZE ***')
            radiusd.radlog(radiusd.L_DBG, (str(the_tuple)))
            break


def __find_supplicant(payload, api_path):
    """ Search for certificate with uid in db, if certificate not found return error
    """
    host = util.erad_cfg().api.host
    url = host + api_path
    headers = {'content-type': 'application/json'}

    radiusd.radlog(radiusd.L_DBG, f'{url}')

    response = requests.post(url, data=json.dumps(payload), headers=headers, timeout=5, verify=True)
    radiusd.radlog(radiusd.L_DBG, f'{response}')

    if response.status_code != 200:
        raise EradAuthServiceError(
            'Request to find supplicant failed. '
            'Error code: {0} and Error Message: {1}'.format(
                str(response.status_code),
                str(response.text)
            )
        )

    response_json = response.json()

    if 'Error' not in response_json:
        raise EradAuthServiceError(
            'Request to find supplicant failed. '
            'Wrong response format\n{0}'.format(
                str(response.text)
            )
        )

    if response_json['Error'] is not None:
        raise EradAuthNotFound(
            'Supplicant not found:\n{0}'.format(
                payload.get('Supplicant') or payload.get('SupplicantCertificate')
            )
        )
    return response_json


def store_supplicant_info(username, password, called_station_id, info):
    # "Time to live" for the stored value
    redis_client.setex(
        '%s:%s:%s' % (username, password, called_station_id),
        REDIS_CACHE_TTL,
        json.dumps(info)
    )
    return info


def restore_supplicant_info(username, password, called_station_id):
    reply_attrs, control_attrs = json.loads(redis_client.get('%s:%s:%s' % (username, password, called_station_id)))
    reply_attrs_tuple = make_tuple_from_list(reply_attrs)
    contrl_attrs_reply = make_tuple_from_list(control_attrs)

    return reply_attrs_tuple, contrl_attrs_reply


def make_tuple_from_list(attrs):
    # map(str, x) - freeradius want (str, str)
    # but erad_api (requests lib) return unicode
    return tuple(tuple(map(str, x)) for x in attrs)


# Calls the webservice to grab info about the supplicant/user.
def get_supplicant_info(request_attrs):
    called_station_id = ''
    calling_station_id = ''
    user_name = ''
    password = ''
    nas_id = ''
    server_ip = ''
    server_port = ''
    uid = None
    realm = ''

    for att in request_attrs:
        if att[0] == 'User-Name':
            user_name = sanitize(att[1])
        elif att[0] == 'Called-Station-Id':
            called_station_id = sanitize(att[1].split(':', 1)[0])
        elif att[0] == 'User-Password':
            password = sanitize(att[1])
        elif att[0] == 'Calling-Station-Id':
            calling_station_id = sanitize(att[1])
        elif att[0] == 'NAS-Identifier':
            nas_id = sanitize(att[1])
        elif att[0] == 'Supplicant-Radius-Ip':
            server_ip = sanitize(att[1])
        elif att[0] == 'TLS-Client-Cert-Common-Name':
            uid = sanitize(att[1])
        elif att[0] == 'Supplicant-Radius-Port':
            server_port = sanitize(att[1])
        elif att[0] == 'Realm':
            realm = sanitize(att[1])

    # check for MAC authentication pattern.  util.sanitize_mac can raise
    # exceptions, so just continue if this fails anywhere (i.e. assume
    # not MAC auth)
    mac_auth = False
    try:
        temp_user = util.sanitize_mac(user_name)
        if util.sanitize_mac(calling_station_id) == temp_user:
            user_name = temp_user
            mac_auth = True
    except:
        pass

    payload = {
        "SystemKey": util.erad_cfg().system_key[0],
        "Endpoint": {
            "IpAddress": server_ip,
            "Port": server_port
        },
        "Authenticator": {
            "ID": called_station_id
        },
        "Group": {
            "ID": nas_id
        },
        "Realm": realm,
    }

    # if using eap-tls, send uid as the user_name

    if uid is not None:
        user_name = uid
        api_path = '/erad/system/supplicant/find/certificate'
        payload.update({
            "SupplicantCertificate": {"ID": uid},

        })
    else:
        api_path = '/erad/system/supplicant/find'
        payload.update({
            "Supplicant": {
                "Username": user_name
            },
        })

    # Try to use cached supplicant info
    try:
        return restore_supplicant_info(user_name, password, called_station_id)
    except Exception as err:
        radiusd.radlog(radiusd.L_DBG, f'{err}')

    res = __find_supplicant(payload, api_path)

    radiusd.radlog(radiusd.L_DBG, repr(res))

    supp = res['Supplicant']
    group = res['Group']
    reply_attrs = []
    control_attrs = []

    # Some attributes may not be present (password, remote server url, etc.), but others must be.
    try:
        radiusd.radlog(radiusd.L_DBG, repr(supp))
        reply_attrs.extend(
            dict_to_radius_attr_tuple_list(get_vlan_speed_settings(group_settings=group, supplicant_settings=supp))
        )

        if supp['UseRemote'] is True:
            reply_attrs.append(('Supplicant-Use-Remote', ':=', 'yes'))

        supp_remote = group['RemoteServerUrl']
        if supp_remote is not None:
            reply_attrs.append(('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(supp_remote)))

        supp_pass = supp['Password']
        try:
            if util.sanitize_mac(supp_pass) == util.sanitize_mac(user_name):
                supp_pass = password
        except:
            pass

        if supp_pass is not None:
            reply_attrs.append(('Supplicant-Password', ':=', supp_pass))
            control_attrs.append(('Cleartext-Password', ':=', supp_pass))

        supp_secret = group['SharedSecret']
        if supp_secret is not None:
            reply_attrs.append(('Supplicant-Secret', ':=', supp_secret))

        # response on non EAP-TLS requests can contain wildcarded usernames
        # Supplicant-Real-User-Name should contain real (not wildcarded usernames) and it used in logs
        if uid is not None:
            real_user_identity = supp['Username']
        else:
            real_user_identity = user_name
        reply_attrs.append(('Supplicant-Real-User-Name', ':=', real_user_identity))

        reply_attrs.append(('Supplicant-User-Name', ':=', supp['Username']))
        reply_attrs.append(('Supplicant-Group', ':=', supp['Group_ID']))

        parent_id = supp.get('Parent_ID')
        reply_attrs.append(('Supplicant-Parent-Id', ':=', parent_id))

        # ClientInfo is a container to all information we receiving from Subscriber API
        # the true client info is in ClientInfo.sbuscriber.client_info
        client_info_container = supp.get('ClientInfo')

        radiusd.radlog(radiusd.L_DBG, f'{client_info_container}')

        if client_info_container is not None:
            if client_info_container.get('accept') is False:
                raise EradAuthSubscriberSuspended(f'Subscriber {parent_id or real_user_identity} is suspended')

            try:
                subscriber_client_info = client_info_container['subscriber']['client_info']
                if subscriber_client_info is not None:
                    reply_attrs.append(('Eleven-Client-Information', ':=', subscriber_client_info))
            except KeyError:
                # if client info is missing just log it as warning but do not reject request.
                radiusd.radlog(
                    radiusd.L_DBG_WARN,
                    f"Can't get client info for {supp['Username']} {supp['Group_ID']} {parent_id}"
                )


        reply_attrs.append(('Endpoint-Group-ID', ':=', res.get('EndpointGroupID')))
        reply_attrs.append(('Endpoint-Group-Name', ':=', res.get('EndpointGroupName')))

        # Not updated API servers during staging tests does not know about new Supplicant attributes and not return them
        # using .get() to be able read attribute
        reply_attrs.append(('Supplicant-Profile-Used', ':=', convert_none_empty_string(supp.get('profile_used'))))
        reply_attrs.append(('Supplicant-Device-Used', ':=', convert_none_empty_string(supp.get('device_used'))))

        # Add CustomJsonData field to log.
        reply_attrs.append(('Supplicant-Custom-Json-Data', ':=', supp.get('CustomJsonData')))

        # we do our own mac auth; let FR modules take care of normal auth (i.e. don't change auth type to ourselves)
        if mac_auth:
            control_attrs.append(('Supplicant-Auth-Type', ':=', 'ERAD'))
            reply_attrs.append(('Supplicant-Use-Mac-Auth', ':=', 'yes'))

    except EradAuthSubscriberSuspended as err:
        # special case suspended user
        raise err

    except Exception as error:
        message = "No message"
        if hasattr(error, message):
            message = error.message
        raise EradAuthServiceError("Malformed response.\n{0}, {1}".format(str(type(error)), message))

    reply_attrs_tuple = make_tuple_from_list(reply_attrs)
    contrl_attrs_reply = make_tuple_from_list(control_attrs)

    store_supplicant_info(user_name, password, called_station_id, (reply_attrs_tuple, contrl_attrs_reply))
    return reply_attrs_tuple, contrl_attrs_reply


def _filter(attr_set, attr_list):
    def filter_fnc(attr_tuple):
        (attr_key, attr_op, attr_value) = attr_tuple
        return attr_key in attr_set

    return tuple(filter(filter_fnc, attr_list))


def fillter_authorize_attrs(reply_attrs, control_attrs):
    return (
        _filter(authorize_available_attrs['reply'], reply_attrs),
        _filter(authorize_available_attrs['control'], control_attrs)
    )


# erad webservices like to return raw strings with double quotes
def sanitize(s):
    return s.replace('\"', '')


vlan_additional_setting = {
    'Tunnel-Type': 'VLAN',
    'Tunnel-Medium-Type': 'IEEE-802'
}

map_names = {
    'MaxDownloadSpeedBits': 'WISPr-Bandwidth-Max-Down',
    'MaxUploadSpeedBits': 'WISPr-Bandwidth-Max-Up',
    'Vlan': 'Tunnel-Private-Group-id',
}


def get_vlan_speed_settings(group_settings=[], supplicant_settings=[]):
    """ Take setting value from erad_api response.
    None or `0` values will be omited.
    The setting will be aplied only if `OverrideConnectionSpeed` or `OverrideVlan` is not equal True
    First check for setting in supplicant_settings list.
    If this list not contains required settings look in group_settings list.
    If group_settings list not contains it too - do nothing.
    """
    _settings = {}

    # Construction `is not True` is needed in case when ERAD API return `OverrideVlan` = None,
    # in this case `is False` will not trigger if clause.
    # Although, it is expected ERAD API return only `True` or `False`
    for setting in list(map_names.keys()):
        for setting_dict in [supplicant_settings, group_settings]:
            if setting in setting_dict:
                if 'SpeedBits' in setting:
                    if group_settings.get('OverrideConnectionSpeed') is not True:
                        value = get_seting_value(setting, setting_dict)
                        if value is not None:
                            _settings[map_names[setting]] = value
                            # setting was selected no need to check the other setting_dict
                            # or it override current selected setting
                            break
                if setting == 'Vlan':
                    if group_settings.get('OverrideVlan') is not True:
                        value = get_seting_value(setting, setting_dict)
                        if value is not None:
                            _settings[map_names[setting]] = value
                            _settings.update(vlan_additional_setting)
                            # setting was selected no need to check the other setting_dict
                            # or it override current selected setting
                            break
    return _settings


def get_seting_value(setting, setting_dict):
    value = setting_dict[setting]
    if value is not None and value != 0:
        return value


def convert_none_empty_string(value):
    if value is None:
        return ''
    return value


def dict_to_radius_attr_tuple_list(the_dict):
    return list([(str(key), ':=', str(value)) for key, value in list(the_dict.items())])


def preacct(vps):
    radiusd.radlog(radiusd.L_DBG, '*** preact ***')
    request_attrs = vps[0]
    log_record_parsed = dict(request_attrs)

    send_acct_log_to_erad(log_record_parsed)
    send_raw_acct_log(json.dumps(log_record_parsed).encode('utf-8'))


def send_acct_log_to_erad(log_record):
    try:
        api_path = '/erad/system/supplicant/acct/log/save'

        payload = {
            "SystemKey": util.erad_cfg().system_key[0],
            "Logs": json.dumps(log_record)
        }

        host = util.erad_cfg().api.host
        url = host + api_path
        headers = {'content-type': 'application/json'}

        response = requests.post(url, data=json.dumps(payload), headers=headers, timeout=5, verify=True)
        radiusd.radlog(radiusd.L_DBG, f'{response}')
        radiusd.radlog(radiusd.L_DBG, f'{response.status_code}')

        if response.status_code != 200:
            radiusd.radlog(radiusd.L_DBG, str(('Error from API: ', response.text)))
            return radiusd.RLM_MODULE_FAIL

        if response.json()['Error'] != None:
            radiusd.radlog(radiusd.L_DBG, str(('Error in parsing json response: ', response.text)))
            return radiusd.RLM_MODULE_FAIL

        return radiusd.RLM_MODULE_OK
    except Exception as error:
        radiusd.radlog(radiusd.L_DBG, f'{error}')
        return radiusd.RLM_MODULE_FAIL
