import sys

patterns = [
('''                if(&reply:Supplicant-Use-Remote == 'yes') {
                    update control {
                        Proxy-To-Realm := &reply:Supplicant-Proxy-Id
                        Cleartext-Password !* ANY
                    }
                }
''', ''),
('''Supplicant-Use-Remote := &reply:Supplicant-Use-Remote''', ''''''),
('''&reply:Supplicant-Proxy-Id''', '''testing__proxy'''),
('''if(&reply:Supplicant-Use-Remote == 'yes') {

        } else {''', ''),
('''}
# ------------------------------------

        }

        pap''','''}
# ------------------------------------

        pap'''),

# From home server Freeradius server expect only Access-Accept or Access-Reject
# Replacing linelog instruction for instruction to clear reply from unexpected attributes.
# linelog instrucructions removed to exclude double logging (one from home and another fromp proxy server)
('''        linelog''',
'''        linelog
        update reply {
            WISPr-Bandwidth-Max-Down   !* ANY
            WISPr-Bandwidth-Max-Up     !* ANY
            Tunnel-Type                := VLAN
            Tunnel-Medium-Type         := IEEE-802
            Tunnel-Private-Group-id    := 143
            Idle-Timeout               := 1800
        }'''),
]


filename = sys.argv[1]

with open(filename, 'r') as working_file:
    text_buff = working_file.read()
    for pattern, replacement in patterns:
        text_buff = text_buff.replace(pattern, replacement)

with open(filename, 'w') as working_file:
    working_file.write(text_buff)



