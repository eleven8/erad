#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
##
## This packing script produce a tar.gz file with the proper files for
## deployment and pushes them to s3 if it not a dev version.
##
################################################################################

export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
# AWS_SECRET_ACCESS_KEY injected by Jenkins
export AWS_DEFAULT_REGION=us-west-2

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

VERSION=$1
if [ -z "$VERSION" ]
then
    VERSION="DEV"
fi

# pack the deployment files
rm -f erad_freeradius_*.tar.gz

pushd $DIR/../../
echo "$VERSION" > version
tar --exclude="./erad/api" \
    --exclude="*.pyc" --exclude=".*" --exclude="*.tar.*" \
    -cvzf "$DIR/erad_freeradius_$VERSION.tar.gz" \
    erad spawn_api_server.py create_database.py \
    remove_database.py frmod_unit_testing.sh version \
    tools/freeradius_launcher enable_streams.py
popd

# copy deployment archive to S3
if [ "$VERSION" != "DEV" ]
then
   aws s3 cp "$DIR/erad_freeradius_$VERSION.tar.gz" s3://teamfortytwo.deploy/erad_freeradius/erad_freeradius_$VERSION.tar.gz
   aws s3 cp "$DIR/erad_freeradius_$VERSION.tar.gz" s3://teamfortytwo.deploy/erad_freeradius.dev.tar.gz
fi
