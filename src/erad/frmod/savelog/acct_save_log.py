#!/usr/bin/env python3
import socket
import sys
import os
import re
import time
import requests
import json
import threading
from random import random

sys.path.append('/usr/local/sbin') # add util from erad
from erad import util


def send_log( logs ):
    payload = {'SystemKey': util.erad_cfg().system_key[0], 'Logs': logs}
    host = util.erad_cfg().api.host
    url = host + '/erad/system/supplicant/acct/log/save'
    headers = {'content-type': 'application/json'}
    print('Sending logs to be saved')
    r = requests.post(url, data=json.dumps(payload), headers=headers, timeout=5, verify=True)
    print(r.text)


if len(sys.argv) == 1:
    print(('Usage:', sys.argv[0], '<comma separated log entries>'))
    exit(1)

raw_log = ''
for i in sys.argv[1:]:
    raw_log += i + ' '

send_log(raw_log)

