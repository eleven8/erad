#!/bin/sh

cat << EOF > /etc/systemd/system/savelog.service
[Unit]
Description=Save Log Service
After=network.target
StartLimitIntervalSec=1
StartLimitBurst=2

[Service]
Type=simple
Restart=always
RestartSec=1
User=ec2-user
ExecStart=/opt/savelog/save_log_server.py

[Install]
WantedBy=multi-user.target
EOF

systemctl enable savelog
systemctl start savelog
