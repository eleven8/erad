#!/usr/bin/env python3
import json
import socket
import sys
import threading
import time
from datetime import datetime

import requests

sys.path.append('/usr/local/sbin')  # add util from erad
from erad import util
import boto.ec2.cloudwatch
import boto.sns
from boto.sts import STSConnection

from dateutil.parser import parse


auth_logs = []
auth_logs_lock = threading.Lock()
acct_logs = []
acct_logs_lock = threading.Lock()
access_results = {}
access_results_lock = threading.Lock()
save_log_chunk = 20

assume_role_lock = threading.Lock()
assume_role_data = {}

MAX_SIZE = 256 * 1024 # KB
WAIT_TIME = 5 # Seconds


auth_sns_topic = util.erad_cfg().auth_sns_topic
acct_sns_topic = util.erad_cfg().acct_sns_topic

role_arn = util.erad_cfg().role_arn
role_profile = util.erad_cfg().role_profile
role_session_name = 'AssumeRole-LogServerSession'


def assume_role():
    global assume_role_data, assume_role_lock

    def check_expiry(date):
        date = datetime.strptime(date, '%Y-%m-%dT%H:%M:%SZ')
        return datetime.utcnow().timestamp() > date.timestamp()

    assume_role_lock.acquire()

    if not assume_role_data or check_expiry(assume_role_data['expiration']):
        connection = STSConnection(profile_name=role_profile)
        assumed_role_object = connection.assume_role(
            role_arn=role_arn,
            role_session_name=role_session_name
        )

        assume_role_data = {
            'access_key': getattr(assumed_role_object.credentials, 'access_key'),
            'secret_key': getattr(assumed_role_object.credentials, 'secret_key'),
            'session_token': getattr(assumed_role_object.credentials, 'session_token'),
            'expiration': getattr(assumed_role_object.credentials, 'expiration')
        }

    assume_role_lock.release()

    return assume_role_data


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i+n]


def add_to_aws(results):
    conn = boto.ec2.cloudwatch.CloudWatchConnection(profile_name='monitor_write')
    print('Adding results to aws cloudwatch')
    for result in results:
        if results[result] != 0:
            print((result, results[result]))
            conn.put_metric_data('RADIUS', result.lower(), results[result])


def send_log(logs):
    for c in chunks(logs, save_log_chunk):
        payload = {'SystemKey': util.erad_cfg().system_key[0], 'Logs': c}
        host = util.erad_cfg().api.host
        url = host + '/erad/system/supplicant/log/save'
        headers = {'content-type': 'application/json'}
        print('Sending logs to be saved')
        r = requests.post(url, data=json.dumps(payload), headers=headers, timeout=5, verify=True)
        print(r.text)


def resplit_logs(logs, output):
    dump = json.dumps(logs)
    chunk_size = int(sys.getsizeof(dump))
    if chunk_size > MAX_SIZE:
        middle = int(len(logs) / 2)
        first, second = logs[: middle], logs[middle:]
        resplit_logs(first, output)
        resplit_logs(second, output)
    if (not chunk_size > MAX_SIZE) and dump:
        output.append(dump)


def convert_iso(dt):
    return parse(dt).isoformat('T', 'microseconds')


def send_auth_logs_to_sns(logs, **kwargs):
    global auth_sns_topic
    if auth_sns_topic is None:
        return

    role_data = assume_role()
    conn = boto.sns.SNSConnection(
        aws_access_key_id=role_data["access_key"],
        aws_secret_access_key=role_data["secret_key"],
        security_token=role_data["session_token"]
    )

    print('Adding results to aws sns')

    updated_logs = []
    # copy logs to prevent sending new fields to DynamoDB
    for log in logs:
        copied_log = dict(log)
        copied_log['RequestType'] = 'Auth'
        copied_log['EventTimestamp_iso'] = convert_iso(copied_log['EventTimestamp'])
        updated_logs.append(copied_log)

    for c in chunks(updated_logs, 500):
        print('Sending auth chunk to aws sns')

        new_logs = []
        resplit_logs(c, new_logs)

        if new_logs:
            for message in new_logs:
                conn.publish(topic=auth_sns_topic, message=message)


def send_acct_logs_to_sns(logs, **kwargs):
    global acct_sns_topic
    if acct_sns_topic is None:
        return

    role_data = assume_role()
    conn = boto.sns.SNSConnection(
        aws_access_key_id=role_data["access_key"],
        aws_secret_access_key=role_data["secret_key"],
        security_token=role_data["session_token"]
    )
    updated_logs = []
    # copy logs to prevent sending new fields to DynamoDB
    for log in logs:
        copied_log = dict(log)
        copied_log['RequestType'] = 'Acct'
        copied_log['EventTimestamp_iso'] = datetime.utcnow().isoformat()
        updated_logs.append(copied_log)

    for c in chunks(updated_logs, 500):
        print('Sending acct chunk to aws sns')

        new_logs = []
        resplit_logs(c, new_logs)

        if new_logs:
            for message in new_logs:
                conn.publish(topic=acct_sns_topic, message=message)


def save_log(parsed):
    global auth_logs, access_results, auth_logs_lock, access_results_lock

    host = util.erad_cfg().api.host
    auth_logs_lock.acquire()  # entrying CS
    auth_logs.append(parsed)
    auth_logs_lock.release()  # exiting CS
    packet_type = parsed['PacketType']
    access_status = parsed['Access']
    access_results_lock.acquire()  # entrying CS
    if packet_type not in access_results:
        access_results[packet_type] = 0
    if access_status not in access_results:
        access_results[access_status] = 0
    access_results[packet_type] += 1
    access_results[access_status] += 1
    access_results_lock.release()  # exiting CS


def save_acct_log(parsed):
    global acct_logs, acct_logs_lock

    acct_logs_lock.acquire()  # entrying CS
    acct_logs.append(parsed)
    acct_logs_lock.release()  # exiting CS


def reset_data():
    global auth_logs, access_results, auth_logs_lock, access_results_lock, acct_logs, acct_logs_lock

    auth_logs_lock.acquire()  # entrying CS
    auth_logs = []
    auth_logs_lock.release()  # exiting CS
    acct_logs_lock.acquire()  # entrying CS
    acct_logs = []
    acct_logs_lock.release()  # exiting CS
    access_results_lock.acquire()  # entrying CS
    access_results = {}
    access_results_lock.release()  # exiting CS


def read_auth_log_thread(client_socket):
    raw_log = client_socket.recv(4096)
    parsed = json.loads(raw_log.strip())
    print(('received:', raw_log))
    print(('parsed:', str(parsed)))
    if parsed is None:
        client_socket.sendall(b'Error!')
    else:
        save_log(parsed)
        client_socket.sendall(b'Success!')
    client_socket.close()


def read_acct_log_thread(client_socket):
    raw_log = client_socket.recv(4096)
    parsed = json.loads(raw_log.strip())
    print(('received:', raw_log))
    print(('parsed:', str(parsed)))
    if parsed is None:
        client_socket.sendall(b'Error!')
    else:
        save_acct_log(parsed)
        client_socket.sendall(b'Success!')
    client_socket.close()


def send_log_server():
    global auth_logs, access_results, auth_logs_lock, access_results_lock, acct_logs, acct_logs_lock

    while True:
        time.sleep(WAIT_TIME)  # Sleep for 3 minutes
        access_results_lock.acquire()  # entrying CS
        new_access_results = access_results.copy()
        access_results_lock.release()  # exiting CS
        auth_logs_lock.acquire()  # entrying CS
        new_logs = list(auth_logs)
        auth_logs_lock.release()  # exiting CS
        acct_logs_lock.acquire()  # entrying CS
        new_acct_logs = list(acct_logs)
        acct_logs_lock.release()  # exiting CS
        reset_data()
        threading.Thread(target=add_to_aws, args=(new_access_results,)).start()
        threading.Thread(target=send_log, args=(new_logs,)).start()
        threading.Thread(target=send_auth_logs_to_sns, args=(new_logs,)).start()
        threading.Thread(target=send_acct_logs_to_sns, args=(new_acct_logs,)).start()


def save_auth_log_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('127.0.0.1', 11812))
    server_socket.listen(20)
    while True:
        client_socket, client_addr = server_socket.accept()
        threading.Thread(target=read_auth_log_thread, args=(client_socket,)).start()


def save_acct_log_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('127.0.0.1', 11813))
    server_socket.listen(20)
    while True:
        client_socket, client_addr = server_socket.accept()
        threading.Thread(target=read_acct_log_thread, args=(client_socket,)).start()


if __name__ == '__main__':
    threading.Thread(target=save_auth_log_server).start()
    threading.Thread(target=save_acct_log_server).start()
    threading.Thread(target=send_log_server).start()
