#!/usr/bin/env python3
import socket
import sys

if len(sys.argv) == 1:
    print(('Usage:', sys.argv[0], '<comma separated log entries>'))
    exit(1)
raw_log = ''
for i in sys.argv[1:]:
    raw_log += i + ' '
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1', 11813))
s.sendall(bytes(raw_log.encode('utf-8')))
print((s.recv(4096)))
s.close()
