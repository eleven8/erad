import unittest
import json
from sys import getsizeof
from save_log_server import resplit_logs

class TestSaveLogServer(unittest.TestCase):
	"""Checking that resplit_logs() leads to stable results"""
	def test_resplit_exact_limit(self):
		# checking if logs fit exactly in 256KB
		def generate_logs():
			row_sample = [{
				"Group_ID (S)": "UJ-977-53",
				"Instance (N)": 15452560741979,
				"Access (S)": "Access-Reject",
				"AccountGroup_ID (S)": "elevenos::UJ-977-53",
				"CalledStationId (S)": "2C-E6-CC-78-34-28:CS-Lab-1005",
				"CallingStationId (S)": "B4-8B-19-9C-0B-0F",
				"EndpointIPAddress (S)": "52.11.20.214",
				"EndpointPort (S)": 20042,
				"EventTimestamp (S)": "Dec 19 2018 21:47:54 UTC",
				"Group_Name (S)": "Hilton San Francisco Union Square (SFOFH)",
				"IsProxied (N)": 0,
				"NASIdentifier (S)": "2C-E6-CC-78-34-28",
				"NASIpAddress (S)": "192.168.69.5",
				"PacketType (S)": "Access-Request",
				"Username (S)": "anonymous@hilton.guestinternet.com",
				"aws:rep:deleting (BOOL)": False,
				"aws:rep:updateregion (S)": "us-west-2",
				"aws:rep:updatetime (N)": 1545256077.729001
			}]
			result = row_sample * 359
			# appending row to get exactly 256KB
			result.append({"a" : "aaaaaaaaaaaaaa"})
			print(getsizeof(json.dumps(result)))
			return result
		resplit_result = []
		resplit_logs(generate_logs(), resplit_result)

		# expection only one batch
		self.assertEqual(len(resplit_result), 1)

		MAX_SNS_SIZE = 256*1024
		for item in resplit_result:
			item_size = getsizeof(item)
			print(item_size)
			self.assertTrue(item_size <= MAX_SNS_SIZE)

	def test_resplit_limit_exceeded(self):
		def generate_logs():
			row_sample = [{
				"Group_ID (S)": "UJ-977-53",
				"Instance (N)": 15452560741979,
				"Access (S)": "Access-Reject",
				"AccountGroup_ID (S)": "elevenos::UJ-977-53",
				"CalledStationId (S)": "2C-E6-CC-78-34-28:CS-Lab-1005",
				"CallingStationId (S)": "B4-8B-19-9C-0B-0F",
				"EndpointIPAddress (S)": "52.11.20.214",
				"EndpointPort (S)": 20042,
				"EventTimestamp (S)": "Dec 19 2018 21:47:54 UTC",
				"Group_Name (S)": "Hilton San Francisco Union Square (SFOFH)",
				"IsProxied (N)": 0,
				"NASIdentifier (S)": "2C-E6-CC-78-34-28",
				"NASIpAddress (S)": "192.168.69.5",
				"PacketType (S)": "Access-Request",
				"Username (S)": "anonymous@hilton.guestinternet.com",
				"aws:rep:deleting (BOOL)": False,
				"aws:rep:updateregion (S)": "us-west-2",
				"aws:rep:updatetime (N)": 1545256077.729001
			}]
			result = row_sample * 359
			# appending row to get exactly 256KB + 1 byte
			result.append({"a" : "aaaaaaaaaaaaaaa"})
			print(getsizeof(json.dumps(result)))
			return result

		resplit_result = []
		resplit_logs(generate_logs(), resplit_result)

		# expection only one batch
		self.assertEqual(len(resplit_result), 2)

		MAX_SNS_SIZE = 256*1024
		for item in resplit_result:
			item_size = getsizeof(item)
			print(item_size)
			self.assertTrue(item_size <= MAX_SNS_SIZE)

	def test_resplit_limit_exceeded_2_times(self):
		def generate_logs():
			row_sample = [{
				"Group_ID (S)": "UJ-977-53",
				"Instance (N)": 15452560741979,
				"Access (S)": "Access-Reject",
				"AccountGroup_ID (S)": "elevenos::UJ-977-53",
				"CalledStationId (S)": "2C-E6-CC-78-34-28:CS-Lab-1005",
				"CallingStationId (S)": "B4-8B-19-9C-0B-0F",
				"EndpointIPAddress (S)": "52.11.20.214",
				"EndpointPort (S)": 20042,
				"EventTimestamp (S)": "Dec 19 2018 21:47:54 UTC",
				"Group_Name (S)": "Hilton San Francisco Union Square (SFOFH)",
				"IsProxied (N)": 0,
				"NASIdentifier (S)": "2C-E6-CC-78-34-28",
				"NASIpAddress (S)": "192.168.69.5",
				"PacketType (S)": "Access-Request",
				"Username (S)": "anonymous@hilton.guestinternet.com",
				"aws:rep:deleting (BOOL)": False,
				"aws:rep:updateregion (S)": "us-west-2",
				"aws:rep:updatetime (N)": 1545256077.729001
			}]
			result = row_sample * 359 * 2
			# appending row to get exactly 256KB * 2
			result.append({"a" : "aaaaaaaaaaaaaaaa"})

			print(getsizeof(json.dumps(result)))
			return result

		resplit_result = []
		resplit_logs(generate_logs(), resplit_result)

		# expection only one batch
		self.assertEqual(len(resplit_result), 3)

		MAX_SNS_SIZE = 256*1024
		for item in resplit_result:
			item_size = getsizeof(item)
			print(item_size)
			self.assertTrue(item_size <= MAX_SNS_SIZE)
