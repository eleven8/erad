# coding: utf-8
import argparse
import sys
parser = argparse.ArgumentParser(description='Set proxy setting for local testing')

parser.add_argument('sitetype', type=str,
                    help='[default, onlyeap]')

parser.add_argument('filepath', type=str,
                    help='Path to file, wich should be changed')

args = parser.parse_args()

available_types = {'default', 'onlyeap'}



if args.sitetype == 'default':
	patterns = [
		('''&reply:Supplicant-Proxy-Id''', '''testing__proxy'''),
	]
elif args.sitetype == 'onlyeap':
	patterns = [
		('''&reply:Supplicant-Proxy-Id''', '''testing__eap_only_proxy'''),
	]
else:
	print(('This type {0} not known, use [default, onlyeap]'. format(args.sitetype)))
	sys.exit(1)


with open(args.filepath, 'r') as working_file:
    text_buff = working_file.read()
    for pattern, replacement in patterns:
    	text_buff = text_buff.replace(pattern, replacement)

with open(args.filepath, 'w') as working_file:
	working_file.write(text_buff)



