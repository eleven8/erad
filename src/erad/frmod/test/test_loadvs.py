# coding: utf-8

import unittest

# to get expected data
# in tests this part will be commented out
from tempfile import mkstemp
from tempfile import mkdtemp
from shutil import rmtree
from os import close

from erad.frmod.loadVS import display
from erad.frmod.test import expecteddata


# do not forget double slash in linelog part in expected data


class TestloadVS(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base_path = mkdtemp()

    @classmethod
    def tearDownClass(cls):
        rmtree(cls.base_path)

    def test_display(self):
        result = display('127.0.1.8', 8888, 'the_test_secret', 'AAA')

        result_filename = mkstemp(dir=self.base_path)
        close(result_filename[0])
        with open(result_filename[1], 'w') as f1:
            f1.write(result)
        print(result_filename)

        self.assertEqual(result, expecteddata.expect_loadvs_with_cert_id)

        result2 = display('127.0.1.9', 9999, 'the_test_secret', None)

        result2_filename = mkstemp(dir=self.base_path)
        close(result2_filename[0])
        with open(result2_filename[1], 'w') as f2:
            f2.write(result2)
        print(result2_filename)

        self.assertEqual(result2, expecteddata.expect_loadvs_without_cert_id)


if __name__ == '__main__':
    unittest.main()
