import datetime
from time import time
import unittest
import mock
import json

import requests
from radiusd import *

from erad.exceptions import *
from erad import util
from erad.api.erad_model import Erad_Authenticator2
from erad.api.erad_model import Erad_Group2
from erad.api.erad_model import Erad_Session
from erad.api.erad_model import Erad_Supplicant
from erad.api.erad_model import Erad_SupplicantCertificate
from erad.frmod import erad_auth




class TestEradModule(unittest.TestCase):
    std_session = "1018129031-123123123-123123123123"
    std_group = "XX-ZZZZ"
    std_uid_group = "Global"
    std_account_id = "elevenos"
    std_authenticator = "FF-FF-FF-33-44-FA"
    std_invalid_authenticator = "AA-BB-13-37-73-32"
    std_username_wildcard = "HOST/^.^/marrcorp.marriott.com"
    std_invalid_username_wildcard = "HOST/^_^/marrcorp.marriott.com"
    usrname_for_wildcard = "HOST/abc.xyz/marrcorp.marriott.com"

    std_username = 'Fred_Durst69'
    std_cert_username = 'Fred_Durst69_cert'
    std_username_with_parent_id = 'test-username-with-parrent-id'
    std_username_custom = 'test-username-custom'
    parent_id = 'test-parrent-id'
    std_cert_uid = '0V1XNP9SF5ZLW3DEJ4M2'
    std_invalid_username = 'Michael_Jackson'
    std_mac_username = '00:0F:F0:AC:3D:8B'
    std_calling_station_id = '00-0F-F0-AC-3D-8B'

    std_password = "aihjwd9awd098jawd9j8ad"
    std_invalid_password = "halflife3"
    std_secret = "Eleven Bacon"
    std_vlan = 1232
    std_remote_url = "http://1091010.1.01010.101.com"
    std_cert = 'RAS01DZFFGCKYJ3Z0A579R0EEIUMLGNRBPEX9XWI4PJP32UV'
    std_checksum = "F927YM2-48nS_kd29yH9E8m-pq0ChKBpWvOIieD-Flwmc9zfcsq2zFOhggpxv1Uauw8OXtvL0NC218T3NOwH8Q=="

    def setUp(self):
        # standard group
        self.group1 = Erad_Group2()
        self.group1.Account_ID = "elevenos"
        self.group1.ID = self.std_group
        self.group1.Name = "The ABC Hotel"
        self.group1.RemoteServerUrl = self.std_remote_url
        self.group1.SharedSecret = self.std_secret
        self.group1.save()

        # other group
        self.group2 = Erad_Group2()
        self.group2.Account_ID = "elevenos"
        self.group2.ID = "FF-1922"
        self.group2.Name = "The XYZ Hotel"
        self.group2.RemoteServerUrl = "http://xyz.com"
        self.group2.SharedSecret = "XYZ Rules"
        self.group2.save()

        # standard authenticator
        self.auth = Erad_Authenticator2()
        self.auth.ID = util.sanitize_mac(self.std_authenticator)
        self.auth.Account_ID = 'elevenos'
        self.auth.Group_ID = self.std_group
        self.auth.save()

        self.auth = Erad_Authenticator2()
        self.auth.ID = self.std_group
        self.auth.Account_ID = 'elevenos'
        self.auth.Group_ID = self.std_group
        self.auth.RadiusAttribute = "nas-identifier"
        self.auth.save()

        # standard supplicant
        self.supp1 = Erad_Supplicant()
        self.supp1.Group_ID = self.std_account_id + "::" + self.std_group
        self.supp1.Username = self.std_username.lower()
        self.supp1.Password = self.std_password
        self.supp1.Vlan = self.std_vlan
        self.supp1.UseRemote = False
        self.supp1.UseWildcard = False
        self.supp1.save()

        # standard supplicant with parent id
        self.supp3 = Erad_Supplicant()
        self.supp3.Group_ID = self.std_account_id + "::" + self.std_group
        self.supp3.Username = self.std_username_with_parent_id
        self.supp3.Parent_ID = self.parent_id
        self.supp3.Password = self.std_password
        self.supp3.Vlan = self.std_vlan
        self.supp3.UseRemote = False
        self.supp3.UseWildcard = False
        self.supp3.save()

        # supplicant with CustomJsonData
        self.supp3 = Erad_Supplicant()
        self.supp3.Group_ID = self.std_account_id + "::" + self.std_group
        self.supp3.Username = self.std_username_custom
        self.supp3.Parent_ID = self.parent_id
        self.supp3.Password = self.std_password
        self.supp3.Vlan = self.std_vlan
        self.supp3.UseRemote = False
        self.supp3.UseWildcard = False
        self.supp3.CustomJsonData = "{\"DeviceType\": \"unknown\", \"CreatedAt\": \"2022-02-09T17:34:56.126803\"}"
        self.supp3.save()

        # supplicant with cert
        self.supp1 = Erad_Supplicant()
        self.supp1.Group_ID = self.std_account_id + "::" + self.std_uid_group
        self.supp1.Username = self.std_cert_username
        self.supp1.Password = self.std_password
        self.supp1.Vlan = self.std_vlan
        self.supp1.UseRemote = False
        self.supp1.UseWildcard = False
        self.supp1.save()

        # wildcard supplicant
        self.supp2 = Erad_Supplicant()
        self.supp2.Group_ID = self.std_account_id + "::" + self.std_group
        self.supp2.Username = self.std_username_wildcard
        self.supp2.Password = self.std_password
        self.supp2.Vlan = self.std_vlan
        self.supp2.UseRemote = False
        self.supp2.UseWildcard = True
        self.supp2.save()

        # MAC supplicant (password shouldn't matter)
        self.supp3 = Erad_Supplicant()
        self.supp3.Group_ID = self.std_account_id + "::" + self.std_group
        self.supp3.Username = self.std_mac_username
        self.supp3.Password = self.std_invalid_password
        self.supp3.Vlan = self.std_vlan
        self.supp3.UseRemote = False
        self.supp3.UseWildcard = False
        self.supp3.save()

        # session
        self.sess = Erad_Session()
        self.sess.ID = self.std_session
        self.sess.Account_ID = self.std_account_id
        self.sess.Group_ID_List = [self.std_group, "FF-1922"]
        self.sess.Identifier = "jimmy.mcgill"
        self.sess.LogoutUrl = "http://1.1.1.1"
        self.sess.LastUsed = datetime.datetime.utcnow()
        self.sess.save()

        # certificate
        self.cert = Erad_SupplicantCertificate()
        self.cert.ID = self.std_cert_uid
        self.cert.Username = self.std_cert_username
        self.cert.Group_ID = self.std_account_id + "::" + self.std_uid_group
        self.cert.Format = 'p12'
        self.cert.Certificate = self.std_cert
        self.cert.Checksum = self.std_checksum
        self.cert.save()

    # def tearDown(self):

    ############################################################
    #
    #  802.1x Tests
    #
    ############################################################

    # tests valid input...should return a tuple of tuples that correspond to correct custom FreeRADIUS values
    def test_valid(self):
        input_attr = (
        ('User-Name', self.std_username), ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
        ('NAS-Identifier', self.std_group))
        expected = []
        other = []
        expected.append(('Supplicant-User-Name', ':=', self.std_username))
        expected.append(('Supplicant-Real-User-Name', ':=', self.std_username))
        expected.append(('Supplicant-Group', ':=', self.std_account_id + "::" + self.std_group))
        expected.append(('Supplicant-Password', ':=', self.std_password))
        expected.append(('Supplicant-Secret', ':=', self.std_secret))
        expected.append(('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)))
        expected.append(('Tunnel-Type', ':=', 'VLAN'))
        expected.append(('Tunnel-Medium-Type', ':=', 'IEEE-802'))
        expected.append(('Tunnel-Private-Group-id', ':=', str(self.std_vlan)))
        expected.append(('Supplicant-Parent-Id', ':=', 'None'))
        expected.append(('Supplicant-Profile-Used', ':=', ''))
        expected.append(('Supplicant-Device-Used', ':=', ''))
        expected.append(('Endpoint-Group-ID', ':=', 'Global'))
        expected.append(('Endpoint-Group-Name', ':=', 'Global'))
        expected.append(('Supplicant-Custom-Json-Data', ':=', 'None'))
        other.append(('Cleartext-Password', ':=', self.std_password))
        result, other_result = erad_auth.get_supplicant_info(input_attr)
        expected_tuple = tuple(tuple(x) for x in expected)
        other_expected_tuple = tuple(tuple(x) for x in other)

        for item in result:
            self.assertTrue(item in expected_tuple)
        for item in other_result:
            self.assertTrue(item in other_expected_tuple)

    def test_valid_with_parent_id(self):
        input_attr = (
            ('User-Name', self.std_username_with_parent_id),
            ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
            ('NAS-Identifier', self.std_group)
        )
        expected = []
        other = []
        expected.append(('Supplicant-User-Name', ':=', self.std_username_with_parent_id))
        expected.append(('Supplicant-Real-User-Name', ':=', self.std_username_with_parent_id))
        expected.append(('Supplicant-Group', ':=', self.std_account_id + "::" + self.std_group))
        expected.append(('Supplicant-Password', ':=', self.std_password))
        expected.append(('Supplicant-Secret', ':=', self.std_secret))
        expected.append(('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)))
        expected.append(('Tunnel-Type', ':=', 'VLAN'))
        expected.append(('Tunnel-Medium-Type', ':=', 'IEEE-802'))
        expected.append(('Tunnel-Private-Group-id', ':=', str(self.std_vlan)))
        expected.append(('Supplicant-Parent-Id', ':=', self.parent_id))
        expected.append(('Supplicant-Profile-Used', ':=', ''))
        expected.append(('Supplicant-Device-Used', ':=', ''))
        expected.append(('Endpoint-Group-ID', ':=', 'Global'))
        expected.append(('Endpoint-Group-Name', ':=', 'Global'))
        expected.append(('Supplicant-Custom-Json-Data', ':=', 'None'))
        other.append(('Cleartext-Password', ':=', self.std_password))
        result, other_result = erad_auth.get_supplicant_info(input_attr)
        expected_tuple = tuple(tuple(x) for x in expected)
        other_expected_tuple = tuple(tuple(x) for x in other)
        for item in result:
            self.assertTrue(item in expected_tuple)
        for item in other_result:
            self.assertTrue(item in other_expected_tuple)

    # tests valid input...
    def test_valid_with_cert(self):
        """  Test if supplicant with uid in certificate atribute "commonname" valid - can be found in db
        """
        input_attr = (
            # ('User-Name', self.std_cert_uid),
            ('TLS-Client-Cert-Common-Name', self.std_cert_uid),
            ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
            ('NAS-Identifier', self.std_uid_group)
        )
        expected = []
        other = []
        expected.append(('Supplicant-User-Name', ':=', self.std_cert_username))
        expected.append(('Supplicant-Real-User-Name', ':=', self.std_cert_username))
        expected.append(('Supplicant-Group', ':=', self.std_account_id + "::" + self.std_uid_group))
        expected.append(('Supplicant-Password', ':=', self.std_password))
        expected.append(('Supplicant-Secret', ':=', self.std_secret))
        expected.append(('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)))
        expected.append(('Tunnel-Type', ':=', 'VLAN'))
        expected.append(('Tunnel-Medium-Type', ':=', 'IEEE-802'))
        expected.append(('Tunnel-Private-Group-id', ':=', str(self.std_vlan)))
        expected.append(('Supplicant-Parent-Id', ':=', 'None'))
        expected.append(('Supplicant-Profile-Used', ':=', ''))
        expected.append(('Supplicant-Device-Used', ':=', ''))
        expected.append(('Endpoint-Group-ID', ':=', 'Global'))
        expected.append(('Endpoint-Group-Name', ':=', 'Global'))
        expected.append(('Supplicant-Custom-Json-Data', ':=', 'None'))
        other.append(('Cleartext-Password', ':=', self.std_password))
        result, other_result = erad_auth.get_supplicant_info(input_attr)
        expected_tuple = tuple(tuple(x) for x in expected)
        other_expected_tuple = tuple(tuple(x) for x in other)
        for item in result:
            self.assertTrue(item in expected_tuple)
        for item in other_result:
            self.assertTrue(item in other_expected_tuple)

        # If supplicant in DB exists without realm, but search query with realm, API still should return supplicant
        input_attr = (
            ('TLS-Client-Cert-Common-Name', self.std_cert_uid + "@" + "enterpriseauth.com"),
            ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
            ('NAS-Identifier', self.std_uid_group)
        )
        result, other_result = erad_auth.get_supplicant_info(input_attr)
        expected_tuple = tuple(tuple(x) for x in expected)
        other_expected_tuple = tuple(tuple(x) for x in other)
        for item in result:
            self.assertTrue(item in expected_tuple)
        for item in other_result:
            self.assertTrue(item in other_expected_tuple)

    def test_valid_with_realm_cert(self):
        pass
    # tests valid input with garbage NAS ID...should return a tuple of tuples that correspond to correct custom FreeRADIUS values
    def test_valid_supplicant(self):
        input_attr = (
        ('User-Name', self.std_username), ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
        ('NAS-Identifier', 'garbage'))
        expected = []
        other = []
        expected.append(('Supplicant-User-Name', ':=', self.std_username))
        expected.append(('Supplicant-Real-User-Name', ':=', self.std_username))
        expected.append(('Supplicant-Group', ':=', self.std_account_id + "::" + self.std_group))
        expected.append(('Supplicant-Password', ':=', self.std_password))
        expected.append(('Supplicant-Secret', ':=', self.std_secret))
        expected.append(('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)))
        expected.append(('Tunnel-Type', ':=', 'VLAN'))
        expected.append(('Tunnel-Medium-Type', ':=', 'IEEE-802'))
        expected.append(('Tunnel-Private-Group-id', ':=', str(self.std_vlan)))
        expected.append(('Supplicant-Parent-Id', ':=', 'None'))
        expected.append(('Supplicant-Profile-Used', ':=', ''))
        expected.append(('Supplicant-Device-Used', ':=', ''))
        expected.append(('Endpoint-Group-ID', ':=', 'Global'))
        expected.append(('Endpoint-Group-Name', ':=', 'Global'))
        expected.append(('Supplicant-Custom-Json-Data', ':=', 'None'))
        other.append(('Cleartext-Password', ':=', self.std_password))
        result, other_result = erad_auth.get_supplicant_info(input_attr)
        expected_tuple = tuple(tuple(x) for x in expected)
        other_expected_tuple = tuple(tuple(x) for x in other)
        for item in result:
            self.assertTrue(item in expected_tuple)
        for item in other_result:
            self.assertTrue(item in other_expected_tuple)

    # tests valid group id input with bad authenticator info...should return a tuple of tuples that correspond to correct custom FreeRADIUS values
    def test_valid_group_id(self):
        input_attr = (
        ('User-Name', self.std_username), ('Called-Station-Id', 'garbage'), ('NAS-Identifier', self.std_group))
        expected = []
        other = []
        expected.append(('Supplicant-User-Name', ':=', self.std_username))
        expected.append(('Supplicant-Real-User-Name', ':=', self.std_username))
        expected.append(('Supplicant-Group', ':=', self.std_account_id + "::" + self.std_group))
        expected.append(('Supplicant-Password', ':=', self.std_password))
        expected.append(('Supplicant-Secret', ':=', self.std_secret))
        expected.append(('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)))
        expected.append(('Tunnel-Type', ':=', 'VLAN'))
        expected.append(('Tunnel-Medium-Type', ':=', 'IEEE-802'))
        expected.append(('Tunnel-Private-Group-id', ':=', str(self.std_vlan)))
        expected.append(('Supplicant-Parent-Id', ':=', 'None'))
        expected.append(('Supplicant-Profile-Used', ':=', ''))
        expected.append(('Supplicant-Device-Used', ':=', ''))
        expected.append(('Endpoint-Group-ID', ':=', 'Global'))
        expected.append(('Endpoint-Group-Name', ':=', 'Global'))
        expected.append(('Supplicant-Custom-Json-Data', ':=', 'None'))
        other.append(('Cleartext-Password', ':=', self.std_password))
        result, other_result = erad_auth.get_supplicant_info(input_attr)
        expected_tuple = tuple(tuple(x) for x in expected)
        other_expected_tuple = tuple(tuple(x) for x in other)
        for item in result:
            self.assertTrue(item in expected_tuple)
        for item in other_result:
            self.assertTrue(item in other_expected_tuple)

    # tests invalid supplicant with valid authenticator
    def test_invalid_supplicant(self):
        input_attr = (
        ('User-Name', self.std_invalid_username), ('Called-Station-Id', self.std_authenticator + ':freeradiustest'))
        with self.assertRaises(erad_auth.EradAuthNotFound):
            result, other_result = erad_auth.get_supplicant_info(input_attr)

    def test_invalid_supplicant(self):
        """  Test, supplicant with wrong uid in certificate atribute "commonname" valid - can not be found in db
        """
        input_attr = (
            ('User-Name', self.std_cert_username),
            # uid changed - Capital O is first symbol instead 0 not stored in test db
            ('TLS-Client-Cert-Common-Name', 'OV1XNP9SF5ZLW3DEJ4M2'),
            ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
            ('NAS-Identifier', self.std_uid_group)
        )

        with self.assertRaises(erad_auth.EradAuthNotFound):
            result, other_result = erad_auth.get_supplicant_info(input_attr)

    # tests invalid authenticator with valid supplicant
    def test_invalid_authenticator(self):
        input_attr = (
        ('User-Name', self.std_username), ('Called-Station-Id', self.std_invalid_authenticator + ':freeradiustest'))
        with self.assertRaises(erad_auth.EradAuthNotFound):
            result, other_result = erad_auth.get_supplicant_info(input_attr)
            pass

    # tests invalid username with valid supplicant
    def test_invalid_username_wildcard(self):
        input_attr = (('User-Name', self.std_invalid_username_wildcard),
                      ('Called-Station-Id', self.std_invalid_authenticator + ':freeradiustest'))
        with self.assertRaises(erad_auth.EradAuthNotFound):
            result, other_result = erad_auth.get_supplicant_info(input_attr)

    # tests valid input with wildcard...should return a tuple of tuples that correspond to correct custom FreeRADIUS values
    def test_valid_supplicant_wildcard(self):
        input_attr = (
        ('User-Name', self.std_username_wildcard), ('Called-Station-Id', self.std_authenticator + ':freeradiustest'))
        expected = []
        other = []
        expected.append(('Supplicant-User-Name', ':=', self.std_username_wildcard))
        expected.append(('Supplicant-Real-User-Name', ':=', self.std_username_wildcard))
        expected.append(('Supplicant-Group', ':=', self.std_account_id + "::" + self.std_group))
        expected.append(('Supplicant-Password', ':=', self.std_password))
        expected.append(('Supplicant-Secret', ':=', self.std_secret))
        expected.append(('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)))
        expected.append(('Tunnel-Type', ':=', 'VLAN'))
        expected.append(('Tunnel-Medium-Type', ':=', 'IEEE-802'))
        expected.append(('Tunnel-Private-Group-id', ':=', str(self.std_vlan)))
        expected.append(('Supplicant-Parent-Id', ':=', 'None'))
        expected.append(('Supplicant-Profile-Used', ':=', ''))
        expected.append(('Supplicant-Device-Used', ':=', ''))
        expected.append(('Endpoint-Group-ID', ':=', 'Global'))
        expected.append(('Endpoint-Group-Name', ':=', 'Global'))
        expected.append(('Supplicant-Custom-Json-Data', ':=', 'None'))
        other.append(('Cleartext-Password', ':=', self.std_password))
        result, other_result = erad_auth.get_supplicant_info(input_attr)
        expected_tuple = tuple(tuple(x) for x in expected)
        other_expected_tuple = tuple(tuple(x) for x in other)
        for item in result:
            self.assertTrue(item in expected_tuple)
        for item in other_result:
            self.assertTrue(item in other_expected_tuple)

    # tests valid input with not wildcard username
    # this test should match with wildcarded username in db and return Supplicant-Real-User-Name with not wildcarded value
    def test_not_wildcard_username(self):
        input_attr = (
        ('User-Name', self.usrname_for_wildcard), ('Called-Station-Id', self.std_authenticator + ':freeradiustest'))
        expected = []
        other = []
        expected.append(('Supplicant-User-Name', ':=', self.std_username_wildcard))
        expected.append(('Supplicant-Real-User-Name', ':=', self.usrname_for_wildcard))
        expected.append(('Supplicant-Group', ':=', self.std_account_id + "::" + self.std_group))
        expected.append(('Supplicant-Password', ':=', self.std_password))
        expected.append(('Supplicant-Secret', ':=', self.std_secret))
        expected.append(('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)))
        expected.append(('Tunnel-Type', ':=', 'VLAN'))
        expected.append(('Tunnel-Medium-Type', ':=', 'IEEE-802'))
        expected.append(('Tunnel-Private-Group-id', ':=', str(self.std_vlan)))
        expected.append(('Supplicant-Parent-Id', ':=', 'None'))
        expected.append(('Supplicant-Profile-Used', ':=', ''))
        expected.append(('Supplicant-Device-Used', ':=', ''))
        expected.append(('Endpoint-Group-ID', ':=', 'Global'))
        expected.append(('Endpoint-Group-Name', ':=', 'Global'))
        expected.append(('Supplicant-Custom-Json-Data', ':=', 'None'))
        other.append(('Cleartext-Password', ':=', self.std_password))
        result, other_result = erad_auth.get_supplicant_info(input_attr)
        expected_tuple = tuple(tuple(x) for x in expected)
        other_expected_tuple = tuple(tuple(x) for x in other)
        for item in result:
            self.assertTrue(item in expected_tuple)
        for item in other_result:
            self.assertTrue(item in other_expected_tuple)

    ############################################################
    #
    #  Standard RADIUS Auth Tests
    #
    ############################################################

    # tests standard RADIUS auth with normal user and normal password.
    def test_valid_radius_user(self):
        input_attr = (
        ('User-Name', self.std_username), ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
        ('User-Password', self.std_password))
        expected = []
        other = []
        expected.append(('Supplicant-User-Name', ':=', self.std_username))
        expected.append(('Supplicant-Real-User-Name', ':=', self.std_username))
        expected.append(('Supplicant-Group', ':=', self.std_account_id + "::" + self.std_group))
        expected.append(('Supplicant-Password', ':=', self.std_password))
        expected.append(('Supplicant-Secret', ':=', self.std_secret))
        expected.append(('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)))
        expected.append(('Tunnel-Type', ':=', 'VLAN'))
        expected.append(('Tunnel-Medium-Type', ':=', 'IEEE-802'))
        expected.append(('Tunnel-Private-Group-id', ':=', str(self.std_vlan)))
        expected.append(('Supplicant-Parent-Id', ':=', 'None'))
        expected.append(('Supplicant-Profile-Used', ':=', ''))
        expected.append(('Supplicant-Device-Used', ':=', ''))
        expected.append(('Endpoint-Group-ID', ':=', 'Global'))
        expected.append(('Endpoint-Group-Name', ':=', 'Global'))
        expected.append(('Supplicant-Custom-Json-Data', ':=', 'None'))
        other.append(('Cleartext-Password', ':=', self.std_password))
        result, other_result = erad_auth.get_supplicant_info(input_attr)
        expected_tuple = tuple(tuple(x) for x in expected)
        other_expected_tuple = tuple(tuple(x) for x in other)
        for item in result:
            self.assertTrue(item in expected_tuple)
        for item in other_result:
            self.assertTrue(item in other_expected_tuple)

    # tests standard RADIUS auth with normal wildcard user and normal password.
    def test_valid_radius_wildcard_user(self):
        input_attr = (
        ('User-Name', self.std_username_wildcard), ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
        ('User-Password', self.std_password))
        expected = []
        other = []
        expected.append(('Supplicant-User-Name', ':=', self.std_username_wildcard))
        expected.append(('Supplicant-Real-User-Name', ':=', self.std_username_wildcard))
        expected.append(('Supplicant-Group', ':=', self.std_account_id + "::" + self.std_group))
        expected.append(('Supplicant-Password', ':=', self.std_password))
        expected.append(('Supplicant-Secret', ':=', self.std_secret))
        expected.append(('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)))
        expected.append(('Tunnel-Type', ':=', 'VLAN'))
        expected.append(('Tunnel-Medium-Type', ':=', 'IEEE-802'))
        expected.append(('Tunnel-Private-Group-id', ':=', str(self.std_vlan)))
        expected.append(('Supplicant-Parent-Id', ':=', 'None'))
        expected.append(('Supplicant-Profile-Used', ':=', ''))
        expected.append(('Supplicant-Device-Used', ':=', ''))
        expected.append(('Endpoint-Group-ID', ':=', 'Global'))
        expected.append(('Endpoint-Group-Name', ':=', 'Global'))
        expected.append(('Supplicant-Custom-Json-Data', ':=', 'None'))
        other.append(('Cleartext-Password', ':=', self.std_password))
        result, other_result = erad_auth.get_supplicant_info(input_attr)
        expected_tuple = tuple(tuple(x) for x in expected)
        other_expected_tuple = tuple(tuple(x) for x in other)
        for item in result:
            self.assertTrue(item in expected_tuple)
        for item in other_result:
            self.assertTrue(item in other_expected_tuple)

    # tests standard RADIUS auth with invalid user and normal password.
    def test_invalid_radius_user(self):
        input_attr = (
        ('User-Name', self.std_invalid_username), ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
        ('User-Password', self.std_password))
        with self.assertRaises(erad_auth.EradAuthNotFound):
            result, other_result = erad_auth.get_supplicant_info(input_attr)

    # tests standard RADIUS auth with invalid wildcard user and normal password.
    def test_invalid_radius_user_wildcard(self):
        input_attr = (('User-Name', self.std_invalid_username_wildcard),
                      ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
                      ('User-Password', self.std_password))
        with self.assertRaises(erad_auth.EradAuthNotFound):
            result, other_result = erad_auth.get_supplicant_info(input_attr)

    # tests standard RADIUS auth with valid user and invalid password.
    def test_invalid_radius_password(self):
        input_attr = (
        ('User-Name', self.std_invalid_username), ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
        ('User-Password', self.std_invalid_password))
        with self.assertRaises(erad_auth.EradAuthNotFound):
            result, other_result = erad_auth.get_supplicant_info(input_attr)

    # tests standard RADIUS auth with valid user and password but invalid authenticator.
    def test_invalid_radius_authenticator(self):
        input_attr = (('User-Name', self.std_invalid_username),
                      ('Called-Station-Id', self.std_invalid_authenticator + ':freeradiustest'),
                      ('User-Password', self.std_invalid_password))
        with self.assertRaises(erad_auth.EradAuthNotFound):
            result, other_result = erad_auth.get_supplicant_info(input_attr)

    ############################################################
    #
    #  MAC Auth Tests
    #
    ############################################################

    # calling with normal password, while bad password is in db; shouldn't matter for MAC auth
    def test_valid_mac_auth(self):
        input_attr = (
        ('User-Name', self.std_mac_username), ('Called-Station-Id', self.std_authenticator + ':freeradiustest'),
        ('User-Password', self.std_password), ('Calling-Station-Id', self.std_calling_station_id))

        result, other_result = erad_auth.get_supplicant_info(input_attr)
        result = erad_auth.authenticate((input_attr,))
        expected_result = RLM_MODULE_OK
        self.assertTrue((result == expected_result))

        # call with bad authenticator; should fail

    def test_invalid_mac_auth(self):
        input_attr = (
        ('User-Name', self.std_mac_username), ('Called-Station-Id', self.std_invalid_authenticator + ':freeradiustest'),
        ('User-Password', self.std_password), ('Calling-Station-Id', self.std_calling_station_id))
        with self.assertRaises(erad_auth.EradAuthNotFound):
            result, other_result = erad_auth.get_supplicant_info(input_attr)

    def test_fillter_authorize_attrs(self):
        full_reply_attrs = [
            ('Supplicant-User-Name', ':=', self.std_username),
            ('Supplicant-Group', ':=', self.std_account_id + "::" + self.std_group),
            ('Supplicant-Password', ':=', self.std_password),
            ('Supplicant-Secret', ':=', self.std_secret),
            ('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)),
            ('Tunnel-Type', ':=', 'VLAN'),
            ('Tunnel-Medium-Type', ':=', 'IEEE-802'),
            ('Tunnel-Private-Group-id', ':=', str(self.std_vlan)),
            ('Supplicant-Use-Remote', ':=', 'yes'),
            ('Supplicant-Use-Mac-Auth', ':=', 'yes'),
        ]

        full_control_attrs = [
            ('Cleartext-Password', ':=', self.std_password),
            ('Auth-Type', ':=', 'ERAD'),

        ]

        auth_only_reply_attrs = [
            ('Supplicant-Use-Remote', ':=', 'yes'),
            ('Supplicant-User-Name', ':=', self.std_username),
            ('Supplicant-Proxy-Id', ':=', util.construct_proxy_id(self.std_remote_url)),
        ]

        auth_only_control_attrs = [
            ('Cleartext-Password', ':=', self.std_password),
            ('Auth-Type', ':=', 'ERAD'),
        ]

        result_reply_attrs, result_control_attrs = erad_auth.fillter_authorize_attrs(full_reply_attrs,
                                                                                     full_control_attrs)

        expected_reply_attrs = dict([(key, (op, value)) for (key, op, value) in auth_only_reply_attrs])
        expected_control_attrs = dict([(key, (op, value)) for (key, op, value) in auth_only_control_attrs])

        def __assert_func(expected_dict, result_list):
            for (key, op, value) in result_list:
                self.assertTrue(key in expected_dict)
                self.assertEqual(expected_dict[key], (op, value))

        __assert_func(expected_reply_attrs, result_reply_attrs)
        __assert_func(expected_control_attrs, result_control_attrs)

    def test_get_seting_value(self):
        setting_dict = {
            'test0': 0,
            'test1': None,
            'test2': 1,
            'test3': '1',
        }

        self.assertEqual(erad_auth.get_seting_value('test0', setting_dict), None)
        self.assertEqual(erad_auth.get_seting_value('test1', setting_dict), None)
        self.assertEqual(erad_auth.get_seting_value('test2', setting_dict), 1)
        self.assertEqual(erad_auth.get_seting_value('test3', setting_dict), '1')
        self.assertEqual(erad_auth.get_seting_value('test3', setting_dict), '1')

    def test_get_vlan_speed_settings(self):

        # `OverrideConnectionSpeed` and `OverrideVlan` is set to True,
        # speed or vlan settings should not be applied, use settings provided by home radius server
        supplicant_list = {
            'MaxDownloadSpeedBits': 1,
            'MaxUploadSpeedBits': 1,
        }
        group_list = {
            'OverrideConnectionSpeed': True,
            'OverrideVlan': True,
            'MaxDownloadSpeedBits': 2,
            'MaxUploadSpeedBits': 2,
        }

        tupple_list = erad_auth.get_vlan_speed_settings(
            group_settings=group_list, supplicant_settings=supplicant_list)
        self.assertEqual(len(tupple_list), 0)

        # `OverrideConnectionSpeed` and `OverrideVlan` is set to False,
        # supplicant settings absent, group settings set in `0` value
        # speed or vlan settings should not be applied, because `0` treated as None
        supplicant_list = {
        }
        group_list = {
            'OverrideConnectionSpeed': False,
            'OverrideVlan': False,
            'MaxDownloadSpeedBits': 0,
            'MaxUploadSpeedBits': 0,
            'Vlan': 0,
        }

        tupple_list = erad_auth.get_vlan_speed_settings(
            group_settings=group_list, supplicant_settings=supplicant_list)
        self.assertEqual(len(tupple_list), 0)

        # `OverrideConnectionSpeed` and `OverrideVlan` is set to False,
        # supplicant settings is equal `0`, group settings set in non `0` values
        # speed or vlan settings should be applied with group settings value,
        # because `0` values in supplicant settings treated as None
        supplicant_list = {
            'MaxDownloadSpeedBits': 0,
            'MaxUploadSpeedBits': 0,
        }
        group_list = {
            'OverrideConnectionSpeed': False,
            'OverrideVlan': False,
            'MaxDownloadSpeedBits': 1,
            'MaxUploadSpeedBits': 1,
            'Vlan': 2,
        }

        expected_result = dict(
            [
                ('WISPr-Bandwidth-Max-Down', 1),
                ('WISPr-Bandwidth-Max-Up', 1),
                ('Tunnel-Type', 'VLAN'),
                ('Tunnel-Medium-Type', 'IEEE-802'),
                ('Tunnel-Private-Group-id', 2),
            ]
        )

        tupple_list = erad_auth.get_vlan_speed_settings(group_settings=group_list, supplicant_settings=supplicant_list)

        for key, value in list(tupple_list.items()):
            self.assertEqual(expected_result[key], value)

        # `OverrideConnectionSpeed` and `OverrideVlan` is set to False,
        # supplicant settings is equal `None`, group settings set in non `0` values
        # speed or vlan settings should be applied with group settings value,
        supplicant_list = {
            'MaxDownloadSpeedBits': None,
            'MaxUploadSpeedBits': None,
        }
        group_list = {
            'OverrideConnectionSpeed': False,
            'OverrideVlan': False,
            'MaxDownloadSpeedBits': 1,
            'MaxUploadSpeedBits': 1,
            'Vlan': 2,
        }

        expected_result = dict(
            [
                ('WISPr-Bandwidth-Max-Down', 1),
                ('WISPr-Bandwidth-Max-Up', 1),
                ('Tunnel-Type', 'VLAN'),
                ('Tunnel-Medium-Type', 'IEEE-802'),
                ('Tunnel-Private-Group-id', 2),
            ]
        )

        tupple_list = erad_auth.get_vlan_speed_settings(group_settings=group_list, supplicant_settings=supplicant_list)

        for key, value in list(tupple_list.items()):
            self.assertEqual(expected_result[key], value)

        # `OverrideConnectionSpeed` and `OverrideVlan` is set to False,
        # supplicant settings set int non `0` values, group settings set in non `0` values
        # speed or vlan settings should be applied with supplicant settings value,
        supplicant_list = {
            'MaxDownloadSpeedBits': 2,
            'MaxUploadSpeedBits': 2,
        }
        group_list = {
            'OverrideConnectionSpeed': False,
            'OverrideVlan': False,
            'MaxDownloadSpeedBits': 1,
            'MaxUploadSpeedBits': 1,
            'Vlan': 2,
        }

        expected_result = dict(
            [
                ('WISPr-Bandwidth-Max-Down', 2),
                ('WISPr-Bandwidth-Max-Up', 2),
                ('Tunnel-Type', 'VLAN'),
                ('Tunnel-Medium-Type', 'IEEE-802'),
                ('Tunnel-Private-Group-id', 2),
            ]
        )

        tupple_list = erad_auth.get_vlan_speed_settings(group_settings=group_list, supplicant_settings=supplicant_list)

        for key, value in list(tupple_list.items()):
            self.assertEqual(expected_result[key], value)

        # `OverrideConnectionSpeed` and `OverrideVlan` is set to False,
        # supplicant settings set int non `0` values, group settings set in `0` values
        # speed settings should be applied with supplicant settings value,
        supplicant_list = {
            'MaxDownloadSpeedBits': 3,
            'MaxUploadSpeedBits': 3,
        }
        group_list = {
            'OverrideConnectionSpeed': False,
            'OverrideVlan': False,
            'MaxDownloadSpeedBits': 0,
            'MaxUploadSpeedBits': 0,
            'Vlan': 0,
        }

        expected_result = dict(
            [
                ('WISPr-Bandwidth-Max-Down', 3),
                ('WISPr-Bandwidth-Max-Up', 3),
            ]
        )

        tupple_list = erad_auth.get_vlan_speed_settings(group_settings=group_list, supplicant_settings=supplicant_list)

        for key, value in list(tupple_list.items()):
            self.assertEqual(expected_result[key], value)

        # `OverrideConnectionSpeed` and `OverrideVlan` is set to False,
        # Only part of supplicant settings set int non `0` values, group settings set in non `0` values
        # speed or vlan settings should be combined from supplicant and group settings
        supplicant_list = {
            'MaxDownloadSpeedBits': 2,
            'MaxUploadSpeedBits': 0,
        }
        group_list = {
            'OverrideConnectionSpeed': False,
            'OverrideVlan': False,
            'MaxDownloadSpeedBits': 1,
            'MaxUploadSpeedBits': 1,
            'Vlan': 2,
        }

        expected_result = dict(
            [
                ('WISPr-Bandwidth-Max-Down', 2),
                ('WISPr-Bandwidth-Max-Up', 1),
                ('Tunnel-Type', 'VLAN'),
                ('Tunnel-Medium-Type', 'IEEE-802'),
                ('Tunnel-Private-Group-id', 2),
            ]
        )

        tupple_list = erad_auth.get_vlan_speed_settings(group_settings=group_list, supplicant_settings=supplicant_list)

        for key, value in list(tupple_list.items()):
            self.assertEqual(expected_result[key], value)

        # `OverrideConnectionSpeed` and `OverrideVlan` is set to False,
        # supplicant and group settings contain not expected settings,
        # they should be skipped and result list should empty
        supplicant_list = {
            'Otherattr': 2,
            'Otherattr2': 1,
        }
        group_list = {
            'OverrideConnectionSpeed': False,
            'OverrideVlan': False,
            'Otherattr': 3,
            'Otherattr2': 4,
        }

        tupple_list = erad_auth.get_vlan_speed_settings(group_settings=group_list, supplicant_settings=supplicant_list)
        self.assertEqual(len(tupple_list), 0)

    def test_post_auth_logging_invocation(self):
        # Checking that logs will be saved, user found
        input_attr = (
            ('User-Name', self.std_username),
            ('Called-Station-Id', self.std_authenticator + '::freeradiustest'),
            ('NAS-Identifier', self.std_group)
        )


        with mock.patch('erad.frmod.erad_auth.save_logs') as wrap_function:
            result = erad_auth.post_auth((input_attr,))
            wrap_function.assert_called_once()
            wrap_function.assert_called_with(
                (
                    ('User-Name', 'Fred_Durst69'),
                    ('Called-Station-Id', 'FF-FF-FF-33-44-FA::freeradiustest'),
                    ('NAS-Identifier', 'XX-ZZZZ'),
                ),
                (
                    ('Tunnel-Private-Group-id', ':=', '1232'),
                    ('Tunnel-Type', ':=', 'VLAN'),
                    ('Tunnel-Medium-Type', ':=', 'IEEE-802'),
                    ('Supplicant-Proxy-Id', ':=', 'http__1812'),
                    ('Supplicant-Password', ':=', 'aihjwd9awd098jawd9j8ad'),
                    ('Supplicant-Secret', ':=', 'Eleven Bacon'),
                    ('Supplicant-Real-User-Name', ':=', 'Fred_Durst69'),
                    ('Supplicant-User-Name', ':=', 'Fred_Durst69'),
                    ('Supplicant-Group', ':=', 'elevenos::XX-ZZZZ'),
                    ('Supplicant-Parent-Id', ':=', 'None'),
                    ('Endpoint-Group-ID', ':=', 'Global'),
                    ('Endpoint-Group-Name', ':=', 'Global'),
                    ('Supplicant-Profile-Used', ':=', ''),
                    ('Supplicant-Device-Used', ':=', ''),
                    ('Supplicant-Custom-Json-Data', ':=', 'None')
                )
            )
            self.assertEqual(result[0], RLM_MODULE_UPDATED)

        # Checking that logs will be saved, if user is unknown or not found
        input_attr = (
            ('User-Name', 'unkown_user_name'),
            ('Called-Station-Id', self.std_authenticator + '::freeradiustest'),
            ('NAS-Identifier', self.std_group)
        )

        with mock.patch('erad.frmod.erad_auth.save_logs') as wrap_function:
            result = erad_auth.post_auth((input_attr,))
            wrap_function.assert_called_once()
            wrap_function.assert_called_with(
                (
                    ('User-Name', 'unkown_user_name'),
                    ('Called-Station-Id', 'FF-FF-FF-33-44-FA::freeradiustest'),
                    ('NAS-Identifier', 'XX-ZZZZ'),
                ),
                tuple()
            )
            self.assertEqual(result, RLM_MODULE_NOOP)

        # Checking that logs will be saved, if ERAD API return EradAuthServiceError (wrong integration key, or something like that)
        input_attr = (
            ('User-Name', self.std_username),
            ('Called-Station-Id', self.std_authenticator + '::freeradiustest'),
            ('NAS-Identifier', self.std_group)
        )

        with mock.patch('erad.frmod.erad_auth.get_supplicant_info',
                        side_effect=erad_auth.EradAuthServiceError('error for tests')):
            with mock.patch('erad.frmod.erad_auth.save_logs') as wrap_function:
                result = erad_auth.post_auth((input_attr,))
                wrap_function.assert_called_once()
                wrap_function.assert_called_with(
                    (
                        ('User-Name', 'Fred_Durst69'),
                        ('Called-Station-Id', 'FF-FF-FF-33-44-FA::freeradiustest'),
                        ('NAS-Identifier', 'XX-ZZZZ'),
                    ),
                    tuple()
                )
                self.assertEqual(result, RLM_MODULE_FAIL)

    def test_savelog_function_no_error(self):
        # Testing that profile_used and device_used updates work correctly.

        input_attr = (
            ('User-Name', self.std_username_with_parent_id),
            ('Called-Station-Id', self.std_authenticator + '::freeradiustest'),
            ('NAS-Identifier', self.std_group)
        )

        with mock.patch('erad.frmod.erad_auth.send_raw_auth_log') as wrap_function:
            result = erad_auth.post_auth((input_attr,))
            wrap_function.assert_called_once()
            data = json.loads(wrap_function.call_args[0][0])

            self.assertIn('ProfileUsed', data)
            self.assertIn('DeviceUsed', data)

            self.assertIn('EndpointGroupName', data)
            self.assertIn('EndpointGroupID', data)

            self.assertIn('CustomJsonData', data)

            self.assertEqual(data.get('UserName'), self.std_username_with_parent_id)

            self.assertEqual(data.get('ProfileUsed'), '')
            self.assertEqual(data.get('DeviceUsed'), '')

            self.assertEqual(data.get('EndpointGroupName'), "Global")
            self.assertEqual(data.get("EndpointGroupID"), 'Global')

            self.assertEqual(result[0], RLM_MODULE_UPDATED)

    def get_supplicant(self, username):
        # Fetching fresh copy from DB, setUp objects giving weird results.
        supp = Erad_Supplicant()
        supp.Group_ID = self.std_account_id + "::" + self.std_group
        supp.Username = username
        supp.refresh()

        return supp


    def test_suspended_account_access_reject_authorize(self):
        """Checking only reject because if account is not suspended only available for authorization attrs will be
        returned. To check client info in reply there is another test that work for post_auth step"""
        input_attr = (
            ('User-Name', "user_with_subscriber_suspended"),
            ('Called-Station-Id', self.std_authenticator + '::freeradiustest'),
            ('NAS-Identifier', self.std_group)
        )

        with mock.patch('erad.frmod.erad_auth.__find_supplicant') as find_supp_mock:
            find_supp_mock.return_value = {
                'Supplicant': {
                    'UseWildcard': False,
                    'Username': "does not matter",
                    'Password': "does not matter",
                    'Vlan': 0,
                    'UseRemote': False,
                    'Group_ID': 'elevenos::AABB-1',
                    "ClientInfo" : {
                        "accept": False,
                        "subscriber": {
                            "suspended": True,
                            "access_tags": None,
                            "client_info": "test client info",
                            "provider": "G2Ys+Xkz2Vm_2to4rl1cf",
                            "account": "Lee",
                            "id": "7fXwqmZcV-mSI-oDwRyc+",
                            "created_on": "2022-01-25T02:08:50.71+00:00",
                            "updated_on": "2022-01-25T02:08:50.71+00:00"
                        },
                    }
                },
                'Group': {
                    'RemoteServerUrl': 'remote.fake.com',
                    'SharedSecret': 'RandomKey',
                    'ID': 'AABB-1',
                    'Name': 'Mac support test',
                    'TimeZone': 'Asia/Qyzylorda'
                }
            }

            # Should be rejected
            result = erad_auth.authorize(input_attr)
            self.assertEqual(result, RLM_MODULE_REJECT)

    def test_not_suspended_account_ok(self):
        input_attr = (
            ('User-Name', "user_with_subscriber_not_suspended"),
            ('Called-Station-Id', self.std_authenticator + '::freeradiustest'),
            ('NAS-Identifier', self.std_group)
        )
        with mock.patch('erad.frmod.erad_auth.send_raw_auth_log') as wrap_function:
            with mock.patch('erad.frmod.erad_auth.__find_supplicant') as find_supp_mock:
                find_supp_mock.return_value = {
                    'Supplicant': {
                        'UseWildcard': False,
                        'Username': "does not matter",
                        'Password': "does not matter",
                        'Vlan': 0,
                        'UseRemote': False,
                        'Group_ID': 'elevenos::AABB-1',
                        "ClientInfo" : {
                            "accept": True,
                            "subscriber": {
                                "suspended": False,
                                "access_tags": None,
                                "client_info": "test client info",
                                "provider": "G2Ys+Xkz2Vm_2to4rl1cf",
                                "account": "Lee",
                                "id": "7fXwqmZcV-mSI-oDwRyc+",
                                "created_on": "2022-01-25T02:08:50.71+00:00",
                                "updated_on": "2022-01-25T02:08:50.71+00:00"
                            },
                        }
                    },
                    'Group': {
                        'RemoteServerUrl': 'remote.fake.com',
                        'SharedSecret': 'RandomKey',
                        'ID': 'AABB-1',
                        'Name': 'Mac support test',
                        'TimeZone': 'Asia/Qyzylorda'
                    }
                }

                result = erad_auth.post_auth((input_attr,))
                self.assertEqual(result[0], RLM_MODULE_UPDATED)

                # client info found
                found = False
                for x in result[1]:
                    if 'Eleven-Client-Information' == x[0]:
                        found = True
                        break

                self.assertTrue(found)

    def test_custom_data_not_sanitized(self):
        # Testing that profile_used and device_used updates work correctly.
        input_attr = (
            ('User-Name', self.std_username_custom),
            ('Called-Station-Id', self.std_authenticator + '::freeradiustest'),
            ('NAS-Identifier', self.std_group)
        )

        with mock.patch('erad.frmod.erad_auth.send_raw_auth_log') as wrap_function:
            result = erad_auth.post_auth((input_attr,))
            wrap_function.assert_called_once()
            data = json.loads(wrap_function.call_args[0][0])

            self.assertIn('CustomJsonData', data)
            self.assertEqual(data.get('CustomJsonData'), '{"DeviceType": "unknown", "CreatedAt": "2022-02-09T17:34:56.126803"}')
            self.assertEqual(data.get('UserName'), self.std_username_custom)

            self.assertEqual(result[0], RLM_MODULE_UPDATED)

    def test_realm_post_auth(self):
        """Check that Realm attribute is passed in payload to supplicant-find if present in request_attrs"""
        input_attr_realm = (
            ('User-Name', "testing@enterpriseauth.com"),
            ('Password', "testing"),
            ('Realm', "enterpriseauth.com"),
            ('Called-Station-Id', self.std_authenticator + '::freeradiustest'),
            ('NAS-Identifier', self.std_group)
        )

        input_attr = (
            ('User-Name', "realm@enterpriseauth.com"),
            ('Password', "testing"),
            ('Called-Station-Id', self.std_authenticator + '::freeradiustest'),
            ('NAS-Identifier', self.std_group)
        )

        return_value = {
                    'Supplicant': {
                        'UseWildcard': False,
                        'Username': "testing@enterpriseauth.com",
                        'Password': "does not matter",
                        'Vlan': 0,
                        'UseRemote': False,
                        'Group_ID': 'elevenos::AABB-1',
                        "ClientInfo" : {
                            "accept": True,
                            "subscriber": {
                                "suspended": False,
                                "access_tags": None,
                                "client_info": "test client info",
                                "provider": "G2Ys+Xkz2Vm_2to4rl1cf",
                                "account": "Lee",
                                "id": "7fXwqmZcV-mSI-oDwRyc+",
                                "created_on": "2022-01-25T02:08:50.71+00:00",
                                "updated_on": "2022-01-25T02:08:50.71+00:00"
                            },
                        }
                    },
                    'Group': {
                        'RemoteServerUrl': 'remote.fake.com',
                        'SharedSecret': 'RandomKey',
                        'ID': 'AABB-1',
                        'Name': 'Mac support test',
                        'TimeZone': 'Asia/Qyzylorda'
                    }
                }

        with mock.patch('erad.frmod.erad_auth.send_raw_auth_log') as wrap_function:
            with mock.patch('erad.frmod.erad_auth.__find_supplicant') as find_supp_mock:
                find_supp_mock.return_value = return_value

                # Test with realm passed
                result = erad_auth.post_auth((input_attr_realm,))
                self.assertEqual(result[0], RLM_MODULE_UPDATED)

                find_supp_mock.assert_called_once()
                payload = find_supp_mock.call_args[0][0]

                self.assertEqual(payload.get("Realm"), "enterpriseauth.com")

                find_supp_mock.reset_mock()

                # Test with no realm passed
                result = erad_auth.post_auth((input_attr,))
                self.assertEqual(result[0], RLM_MODULE_UPDATED)

                find_supp_mock.assert_called_once()
                payload = find_supp_mock.call_args[0][0]

                self.assertEqual(payload.get("Realm"), "")



if __name__ == '__main__':
    unittest.main()
