#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
##
## This deploy script will wrap up the install for freeradius and deploy our
## free radius module into it.
##
################################################################################

set -e
set -x

parse_global_config() {
    for file in "/etc/boto.cfg" "/etc/erad.cfg"; do
      echo ${file}
      if [[ -f ${file} ]]; then
        sed -i \
            -e "s:\##AWS_ACCESS_KEY_ID##:$AWS_ACCESS_KEY_ID:g" \
            -e "s:\##AWS_SECRET_ACCESS_KEY##:$AWS_SECRET_ACCESS_KEY:g" \
            -e "s:\##AWS_DEFAULT_REGION##:$AWS_DEFAULT_REGION:g" \
            ${file}
      fi
    done
}

export ERAD_FRMOD_INSTALL=true
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

yum -y update

# install epel and redis
# redis is used for caching in erad_auth.py

amazon-linux-extras install epel redis4.0 -y
systemctl start redis

# expected environment variables
#   T42_SERVER_ID
#   T42_PASSPHRASE (if missing, performs dev install)

instance_id=`curl http://169.254.169.254/latest/meta-data/instance-id`
RADDB_DIR=/usr/local/etc/raddb
export PYTHONPATH=$PATH:/usr/local/sbin


#start by getting dependencies
yum -y -q install \
            libtalloc \
            libatomic \
            python3 \
            python3-devel \
            rsync \
            awslogs \
            gzip \
            logrotate



# turning off pip upgrade: pip install --upgrade pip
pip3 install \
    'awscli<1.16.264' \
    'botocore<1.13.0' \
    'pyyaml>=3.10,<=5.1' \
    boto \
    mako \
    pytz \
    validate_email \
    requests \
    redis

#setup group, user, and permissions for them
if ! id -u radiusd; then
  useradd -M radiusd
fi
if ! id -g radiusd; then
  usermod -L radiusd
fi
usermod -a -G radiusd ec2-user

chown -R radiusd:radiusd $RADDB_DIR

mkdir -p /var/log/radius
chown -R root:radiusd /var/log/radius
chmod -R ug+rwx /var/log/radius

# this is for control socket to be able execute `radmin`
mkdir -p /var/run/radiusd
chown radiusd:radiusd /var/run/radiusd


# logrotate config

# logrotate for syslog is necessary becuase journald continue write information into syslog socket
# (bug Seems Amazon Linux 2 did not backport patch)
# and rsyslog continue create txt files for logs messages
# eventually it consumes all disk space
cp /usr/local/src/erad/frmod/freeradius.logrotate.conf /etc/logrotate.d/freeradius.logrotate.conf
cp -f /usr/local/src/erad/frmod/syslog.logrotate.conf /etc/logrotate.d/syslog

chmod 0644 /etc/logrotate.d/freeradius.logrotate.conf
chmod 0644 /etc/logrotate.d/syslog
# by default logrotate configured to runs daily, create hourly record
cp /etc/cron.daily/logrotate /etc/cron.hourly

# delete the default freeradius config
rm -rf $RADDB_DIR

# move raddb config over to the install
mkdir -p $RADDB_DIR
rsync -rl /usr/local/src/erad/frmod/raddb/ $RADDB_DIR

# Verify if decrypt passphrase was provided.
if [ ! -z "$T42_PASSPHRASE" ]; then
  # decrypt and install production cfg files
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output /etc/boto.cfg --decrypt /usr/local/src/erad/frmod/boto.cfg.prod.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output /etc/erad.cfg --decrypt /usr/local/src/erad/frmod/erad.cfg.prod.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output $RADDB_DIR/certs/server.pem --decrypt $RADDB_DIR/certs/server.pem.prod.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output $RADDB_DIR/certs/server_hilton.pem --decrypt $RADDB_DIR/certs/server_hilton.pem.prod.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output $RADDB_DIR/certs/server_rlhc.pem --decrypt $RADDB_DIR/certs/server_rlhc.pem.prod.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output $RADDB_DIR/certs/server_RRR.pem --decrypt $RADDB_DIR/certs/server_RRR.pem.prod.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output $RADDB_DIR/certs/server_stagingcalix.pem --decrypt $RADDB_DIR/certs/server_stagingcalix.pem.prod.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output $RADDB_DIR/certs/server_prodcalix.pem --decrypt $RADDB_DIR/certs/server_prodcalix.pem.prod.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output $RADDB_DIR/certs/server_spectrumstaging.pem --decrypt $RADDB_DIR/certs/server_spectrumstaging.pem.prod.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output $RADDB_DIR/certs/server_spectrumprod.pem --decrypt $RADDB_DIR/certs/server_spectrumprod.pem.prod.secure

  cat $RADDB_DIR/certs/ca.prod.pem > $RADDB_DIR/certs/ca.pem

  # credentials and config files for sns notification (used in freeradius_launcher.sh)
  source /usr/local/src/erad/frmod/sns_credentials/sns_creds_setup.sh
else
  # If not, we must be on a dev box. Deploy dev cfg file.
  cp /usr/local/src/erad/frmod/erad.cfg.dev /etc/erad.cfg

  # because customer certificate list is  hardcoded
  # make copies of default freeradius server certificate to corresponding certificate list
  cp $RADDB_DIR/certs/server.pem $RADDB_DIR/certs/server_hilton.pem
  cp $RADDB_DIR/certs/server.pem $RADDB_DIR/certs/server_rlhc.pem
  cp $RADDB_DIR/certs/server.pem $RADDB_DIR/certs/server_RRR.pem
  cp $RADDB_DIR/certs/server.pem $RADDB_DIR/certs/server_stagingcalix.pem
  cp $RADDB_DIR/certs/server.pem $RADDB_DIR/certs/server_prodcalix.pem
  cp $RADDB_DIR/certs/server.pem $RADDB_DIR/certs/server_spectrumstaging.pem
  cp $RADDB_DIR/certs/server.pem $RADDB_DIR/certs/server_spectrumprod.pem
fi

chmod 644 /etc/erad.cfg
parse_global_config

# deploy python package
mkdir -p /usr/local/sbin/erad/frmod/awslogs
cp /usr/local/src/erad/__init__.py /usr/local/sbin/erad/
cp /usr/local/src/erad/core_test.py /usr/local/sbin/erad/
cp /usr/local/src/erad/exceptions.py /usr/local/sbin/erad/
cp /usr/local/src/erad/util.py /usr/local/sbin/erad/
cp /usr/local/src/erad/frmod/__init__.py /usr/local/sbin/erad/frmod/
cp /usr/local/src/erad/frmod/erad_auth.py /usr/local/sbin/erad/frmod/
cp /usr/local/src/erad/frmod/proxy_loader.py /usr/local/sbin/erad/frmod/
cp /usr/local/src/erad/frmod/radiusd_conf_loader.py /usr/local/sbin/erad/frmod/
cp /usr/local/src/erad/frmod/loadVS.py /usr/local/sbin/erad/frmod/
cp /usr/local/src/erad/frmod/loadeap.py /usr/local/sbin/erad/frmod/


# TODO reseach do we need awlogs dir in /usr/local/sbin/erad/frmod
cp /usr/local/src/erad/frmod/awslogs/awslogs.conf /usr/local/sbin/erad/frmod/awslogs/

#make a profile.d to include the python path
echo "#!/bin/bash" > /etc/profile.d/python.sh
echo "export PYTHONPATH=$PATH:/usr/local/sbin" >> /etc/profile.d/python.sh
chmod +x /etc/profile.d/python.sh

# if this is a production server
if [ ! -z "$T42_PASSPHRASE" ]; then
  # install current proxy configuration
  export AWS_ACCESS_KEY_ID=$(grep -m 1 aws_access_key_id /etc/boto.cfg | awk '{print $3}')
  export AWS_SECRET_ACCESS_KEY=$(grep -m 1 aws_secret_access_key /etc/boto.cfg | awk '{print $3}')
  export AWS_DEFAULT_REGION=us-west-2
  aws s3 cp s3://freeradiusproxyconf/proxy.conf $RADDB_DIR/proxy.conf

  # setup cron with proxy_loader.py
  service crond stop

  # Deploy script uses hard error checking (set -e directive).
  # If crontab file is empty, command `crontab -l` will return error
  # and the rest of command in parentheses will NOT be executed.
  # So temporary disable hard error checking, but enable it again, when crontab file is not empty anymore.
  set +e
  (crontab -l 2>/dev/null; echo "") | crontab -
  set -e
  # set T42_SERVER_ID variable for loadVS.py script
  # set PYTHONPATH - to tell python from where it can import erad module
  (crontab -l 2>/dev/null; echo "T42_SERVER_ID=${T42_SERVER_ID}") | crontab -
  (crontab -l 2>/dev/null; echo "PYTHONPATH=${PYTHONPATH}") | crontab -

  # two scripts combined in one row to exclude race condition - run in queue
  # both scripts will be stop and start radiusd, so if they both will work simultaneously with each other
  # it is possible they will interfere with each other
  (crontab -l 2>/dev/null; echo "*/2 * * * * /usr/bin/python3 /usr/local/sbin/erad/frmod/radiusd_conf_loader.py /var/log/radiusd_conf_loader__log && /usr/bin/python3 /usr/local/sbin/erad/frmod/proxy_loader.py") | crontab -
  service crond start
fi


## RADIUS accept rate (radacct) deployment

mkdir -p $RADDB_DIR/sites-enabled/

#creating VirtualServers configuration
python3 /usr/local/sbin/erad/frmod/loadVS.py -s $T42_SERVER_ID > $RADDB_DIR/sites-available/eradVirtualServers
ln -s $RADDB_DIR/sites-available/eradVirtualServers $RADDB_DIR/sites-enabled/eradVirtualServers

# creating eap modules and virtual sites
python3 /usr/local/sbin/erad/frmod/loadeap.py $RADDB_DIR/mods-enabled/ $RADDB_DIR/sites-enabled/

# set timestamp for config
echo $(date +%s) > /var/radiusconf_timestamp


# Deploy save_log
sed -i -e "s/52.26.34.201/0.0.0.0/g" /etc/erad.cfg
cp -r /usr/local/src/erad/frmod/savelog /opt
chmod +x /opt/savelog/save_log_server.py
bash /opt/savelog/savelog-setup.sh


#starting radius
LAUNCHER_SCRIPT='/usr/local/src/tools/freeradius_launcher/freeradius_launcher.sh'
chmod +x $LAUNCHER_SCRIPT
$LAUNCHER_SCRIPT & disown


## Add proxy_loader_log to cloudwatch if this is a production server
if [ ! -z "$T42_PASSPHRASE" ]; then
  mkdir -p /root/.aws
  cp /usr/local/src/erad/frmod/awslogs/credentials /root/.aws
  chmod 600 /root/.aws/credentials
  cp -f /usr/local/src/erad/frmod/awslogs/awslogs.conf /etc/awslogs/awslogs.conf
  sudo systemctl start awslogsd
fi

# if this is a dev server, install unit testing package
if [ -z "$T42_PASSPHRASE" ]; then
  # install system prerequisites
  yum --enablerepo=epel install -y \
    gcc \
    httpd \
    java \
    python3 \
    python3-devel

  pip3 install \
    'awscli<1.16.264' \
    'botocore<1.13.0' \
    'flask-caching>=1.10' \
    'pynamodb<4.0' \
    'pyyaml>=3.10,<=5.1' \
    boto \
    'celery<5.0' \
    'Flask' \
    flask-compress \
    https://bitbucket.org/teamfortytwo/signxml/get/master.zip \
    lxml \
    mako \
    nose2 \
    ordereddict \
    pycrypto \
    pytz \
    redis \
    requests

  cp /usr/local/src/frmod_unit_testing.sh /usr/bin
  chmod u+x /usr/bin/frmod_unit_testing.sh

  cp -r /usr/local/src/erad/frmod/test /usr/local/sbin/erad/frmod

  if [ -z "$ERAD_AIO_INSTALL" ]; then
      DYNAMODB_PATH=/usr/local/dynamodb
      # copy API Files and Tests
      cp -r /usr/local/src/erad/api /usr/local/sbin/erad/

      # install development config files
      cp /usr/local/src/erad/api/boto.cfg.dev /etc/boto.cfg
      chmod 644 /etc/boto.cfg
      parse_global_config

      # install local dynamo db
      mkdir -p $DYNAMODB_PATH
      source $DIR/../db/deploy.sh install_local_dynamodb $DYNAMODB_PATH

      # deploy configuration for apache
      cp /usr/local/src/erad/api/erad.conf /etc/httpd/conf.d/


  fi

  # download and install eapol_test
  aws s3 cp s3://teamfortytwo.deploy/package/eapol_test.tar.gz /tmp/eapol_test.tar.gz
  pushd /tmp
  tar zxvf eapol_test.tar.gz
  cp /tmp/eapol_test /usr/bin/
  cp /tmp/libnl.so.1 /usr/lib64/
  popd

fi


export AWS_ACCESS_KEY_ID=AKIAIDR4RVSXLXAIAUYQ
export AWS_SECRET_ACCESS_KEY=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

# alarms removed for COVID-19 cost reduction
## add alarm for cpu-surplus-credits-charged metric
#aws cloudwatch \
#  put-metric-alarm \
#    --alarm-name cpu-surplus-credits-charged-$instance_id-radius \
#    --alarm-description "CPUSurplusCreditsCharged " \
#    --metric-name CPUSurplusCreditsCharged --namespace AWS/EC2 \
#    --statistic Average --period 300 --threshold 0 \
#    --comparison-operator GreaterThanThreshold \
#    --dimensions Name=InstanceId,Value=$instance_id \
#    --evaluation-periods 1 \
#    --alarm-actions arn:aws:sns:us-west-2:439838887486:trouble \
#    --unit Count \
#    --region us-west-2
#
### Add disk and swap monitoring if this is a production server
#if [ ! -z "$T42_PASSPHRASE" ]; then
#  yum -y install \
#          perl-Switch \
#          perl-DateTime \
#          perl-Sys-Syslog \
#          perl-LWP-Protocol-https \
#          perl-Digest-SHA.x86_64
#
#  curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
#  unzip CloudWatchMonitoringScripts-1.2.2.zip && \
#  rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
#  pushd aws-scripts-mon
#
#  echo "AWSAccessKeyId=$AWS_ACCESS_KEY_ID
#AWSSecretKey=$AWS_SECRET_ACCESS_KEY
#" > awscreds.conf
#  crontab -l > crontab.conf
#  echo "* * * * * /usr/local/src/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail --swap-util --swap-used --disk-space-util --disk-path=/ --from-cron" >> crontab.conf
#  crontab crontab.conf
#  popd
#
#  # add alarms for disk and swap
#  aws cloudwatch \
#    put-metric-alarm \
#      --alarm-name disk-utilization-$instance_id-radius \
#      --alarm-description "Disk usage is high" \
#      --metric-name DiskSpaceUtilization \
#      --namespace System/Linux \
#      --statistic Average \
#      --period 300 \
#      --threshold 90 \
#      --comparison-operator GreaterThanThreshold \
#      --dimensions Name=Filesystem,Value=/dev/xvda1 Name=MountPath,Value=/ Name=InstanceId,Value=$instance_id \
#      --evaluation-periods 1 \
#      --alarm-actions arn:aws:sns:us-west-2:439838887486:trouble \
#      --unit Percent \
#      --region us-west-2
#
#  aws cloudwatch \
#    put-metric-alarm \
#      --alarm-name swap-utilization-$instance_id-radius \
#      --alarm-description "Swap usage is high" \
#      --metric-name SwapUtilization \
#      --namespace System/Linux \
#      --statistic Average \
#      --period 300 \
#      --threshold 20 \
#      --comparison-operator GreaterThanThreshold \
#      --dimensions Name=InstanceId,Value=$instance_id \
#      --evaluation-periods 1 \
#      --alarm-actions arn:aws:sns:us-west-2:439838887486:trouble \
#      --unit Percent \
#      --region us-west-2
#fi


exit $?
