#!/bin/bash
set -x

ENCRYPTED_CREDENTIALS_PATH=/usr/local/src/erad/frmod/sns_credentials
mkdir -p /root/.aws

gpg --batch --yes \
    --passphrase ${T42_PASSPHRASE} \
    --output - \
    --decrypt  ${ENCRYPTED_CREDENTIALS_PATH}/credentials.secure >> /root/.aws/credentials

cp ${ENCRYPTED_CREDENTIALS_PATH}/config /root/.aws/config
