# coding: utf-8
import argparse

from loadeap import write_data

parser = argparse.ArgumentParser(description='Create !!! TEST !!! EAP modules and vritual sites for freeradius server. \n !!! USE ONLY FOR TESTING !!!')
parser.add_argument('eap_module_path',
                    metavar = 'eap-module-path',
                    type=str,
                    help='Specify the path, where eap modules will be saved'
                    )

parser.add_argument('virtual_site_path',
                    metavar = 'virtual-site-path',
                    type=str,
                    help='Specify the path, where virtual sites will be saved')

certs_data = {
	"1822":None,
}

args = parser.parse_args()

write_data(args.eap_module_path, args.virtual_site_path, certs_data)
