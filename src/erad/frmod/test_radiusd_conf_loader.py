# coding: utf-8

import unittest
from unittest import skip

import shutil
from os import path
from os import walk
from os import mkdir
from os import remove

from subprocess import Popen
from subprocess import PIPE

import tempfile

from boto.s3.connection import OrdinaryCallingFormat
from boto import s3

from . radiusd_conf_loader import backup_current_config
from . radiusd_conf_loader import restore_backup_config
from . radiusd_conf_loader import get_current_timestamp
from . radiusd_conf_loader import read_string_from_s3
from . radiusd_conf_loader import save_timestamp



class TestRadiusdConfLoader(unittest.TestCase):
    def test_backup_current_config(self):
        cur_dir = path.dirname(__file__)
        dst_dir_name = 'test_dst_dir'
        src_path = path.join(cur_dir, 'raddb')
        dst_path = path.join(cur_dir, dst_dir_name)
        backup_current_config(main_config_dir=src_path,
                              backup_config_dir=dst_path)
        src_path_list = [x for x in walk(src_path)]
        dst_path_list = [x for x in walk(dst_path)]
        shutil.rmtree(dst_path)
        self.assertEqual(len(src_path_list), len(dst_path_list))

    def test_restore_backup_config(self):
        cur_dir = path.dirname(__file__)
        dst_dir_name = 'test_dst_dir'
        restore_dst_name = 'test_restore_dst_dir'

        src_path = path.join(cur_dir, 'raddb')
        dst_path = path.join(cur_dir, dst_dir_name)
        resotre_dst_path = path.join(cur_dir, restore_dst_name)

        mkdir(resotre_dst_path)

        backup_current_config(main_config_dir=src_path,
                              backup_config_dir=dst_path)

        restore_backup_config(main_config_dir=resotre_dst_path,
                              backup_config_dir=dst_path)
        src_path_list = [x for x in walk(src_path)]
        dst_path_list = [x for x in walk(dst_path)]
        restore_path_list = [x for x in walk(resotre_dst_path)]
        shutil.rmtree(dst_path)
        shutil.rmtree(resotre_dst_path)
        self.assertEqual(len(src_path_list), len(dst_path_list))
        self.assertEqual(len(restore_path_list), len(dst_path_list))


    def test_read_write_timestamp(self):
        file_hndl, filepath = tempfile.mkstemp()

        timestamp = '123'
        save_timestamp(filepath, timestamp)
        readed_timestap = get_current_timestamp(filepath)
        remove(filepath)
        self.assertEqual(timestamp, readed_timestap)


    @unittest.skip('need aws credentials, tested in dev environmet')
    def test_read_string_from_s3(self):

        region = 'us-west-2'
        bucket_name = 'test-freeradiusconf'
        key_name = 'radiusconf'

        # The aws credentials will be read from boto config file (~/.boto, /etc/boto.cfg etc.)
        # if file is absent then boto s3 module will read aws credetilas from ~/.aws

        readed_string = read_string_from_s3(region, bucket_name, key_name)

        self.assertNotEqual(readed_string, None)
        self.assertNotEqual(readed_string, '')
