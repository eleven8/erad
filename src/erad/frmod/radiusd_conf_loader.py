#!/env/python3
# coding: utf-8

# To work correctly this script need to know
# 1. AWS Credentials to get access to S3 bucket with radiusconf timestamp
# 2. T42_SERVER_ID environment variable
import argparse

import time
import logging
import shutil
import signal

from os import kill
from os import path
from os import environ
from os import system
from sys import exit
from subprocess import Popen, PIPE, STDOUT

from boto import s3
from boto import config as boto_config
from boto.s3.connection import OrdinaryCallingFormat

LOGLEVEL = logging.INFO

parser = argparse.ArgumentParser(
    description='Check free radius configuration timestamp in s3 bucket and reload configuration if it changed')

parser.add_argument('logfile', type=str,
                    help='Path to logfile')
args = parser.parse_args()

LOG_FORMAT = "%(asctime)s:%(levelname)s:%(module)s:%(lineno)d:%(message)s"
logging.basicConfig(filename=args.logfile, format=LOG_FORMAT)
logger = logging.getLogger(__name__)
logger.setLevel(LOGLEVEL)

from erad import util

erad_cfg = util.erad_cfg()

REGION = erad_cfg.proxy_conf.s3_region
BUCKET_NAME = erad_cfg.proxy_conf.bucket
KEY_NAME = erad_cfg.proxy_conf.radiusconf_key

CURRENT_CONF_TIMESTAMP_PATH = '/var/radiusconf_timestamp'

MAIN_CONFIG_DIR = '/usr/local/etc/raddb/'
BACKUP_CONFIG_DIR = '/usr/local/etc/backup_raddb/'

RADIUSD = '/usr/local/sbin/radiusd'

LOADVS = '/usr/local/sbin/erad/frmod/loadVS.py'
LOADEAP = '/usr/local/sbin/erad/frmod/loadeap.py'

VIRTUALSITE_AVAILABLE = path.join(
    MAIN_CONFIG_DIR, 'sites-available/eradVirtualServers')
VIRTUALSITE_ENABLED = path.join(
    MAIN_CONFIG_DIR, 'sites-enabled/eradVirtualServers')

MODS_PATH = path.join(MAIN_CONFIG_DIR, 'mods-enabled')
SITES_PATH = path.join(MAIN_CONFIG_DIR, 'sites-enabled')

SERVER_ID_ENV = 'T42_SERVER_ID'

STARTSEC = 5
SLEEPSEC = 2  # the time given to freeradius_launcher, to detect stopped(killed) radiusd and restart it


# force credentials from boto config file
AWS_ACCESS_KEY_ID = boto_config.get_value('Credentials', 'aws_access_key_id')
AWS_SECRET_ACCESS_KEY = boto_config.get_value(
    'Credentials', 'aws_secret_access_key')


class ConfigOverwriteFailed(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


def read_string_from_s3(region, bucket_name, key_name):
    s3_connection = s3.connect_to_region(
        region,
        aws_access_key_id=AWS_ACCESS_KEY_ID,
        aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
        calling_format=s3.connection.OrdinaryCallingFormat())

    bucket = s3_connection.get_bucket(bucket_name, validate=False)
    key = bucket.get_key(key_name)
    timestamp = key.get_contents_as_string()
    if type(timestamp) is bytes:
        timestamp = timestamp.decode('utf-8')
    s3_connection.close()

    return timestamp


def get_current_timestamp(filepath):
    with open(filepath, 'rb') as f:
        return f.read().decode('utf-8')


def save_timestamp(filepath, timestamp):
    with open(filepath, 'wb') as f:
        f.write(timestamp.encode('utf-8'))


def kill_freeradius():
    logger.info('Stopping (Killing) freeradius...')
    proc = Popen(
        ['killall', '-9', 'radiusd'],
        stdout=PIPE,
        stderr=STDOUT,
        text=True
    )

    std_out, std_err = proc.communicate()

def proxy_test():
    """
        Function will check if radiusd response on radius packets.
        The command in function usually not work in dev environment,
        to skip test use env variable __IS_DEV_ENVIRONMENT__='True'

        Also this function write some data to stdout,
        and if radiusd_conf_loader will be started by cron, cron will send email
    """
    if environ.get('__IS_DEV_ENVIRONMENT__') == 'True':
        return True

    ret = system('echo "User-Name=SR-980-83/erad-monitor,User-Password=nt5ewa9s,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 1 -D /usr/local/share/freeradius/ 127.0.0.1:1814 auth V72QSZ2CE3 | grep "Received Access-Accept"')
    if ret != 0:
        raise ConfigOverwriteFailed(
            'Bad proxy/radclient test.  Status: {0}'.format(str(ret)))
    return True


def owerwrite_config():
    logger.info('Overwriting freeradiusd config files...')
    proc = Popen(
        [
            'python3',
            LOADVS,
            '-s',
            environ.get(SERVER_ID_ENV)
        ],
        stdout=PIPE,
        stderr=STDOUT,
    )

    # in python3 Popen used binary mode by default so stdout will have bytes, this match with f.write() signature
    std_out, std_err = proc.communicate()
    if proc.returncode == 0:
        with open(VIRTUALSITE_AVAILABLE, 'wb') as f:
            f.write(std_out)

        Popen(
            [
                'ln',
                '-s',
                '-f',
                VIRTUALSITE_AVAILABLE,
                VIRTUALSITE_ENABLED
            ],
            stdout=PIPE,
            stderr=STDOUT
        )

    else:
        logger.error(
            'Error on write freeradius virutal site; {0}'.format(std_out.decode('utf-8')))
        raise ConfigOverwriteFailed('Failed to overwrite config')

    proc = Popen(
        [
            'python3',
            LOADEAP,
            MODS_PATH,
            SITES_PATH
        ],
        stdout=PIPE,
        stderr=STDOUT,
        text=True
    )

    std_out, std_err = proc.communicate()
    if proc.returncode == 0:
        logger.info('Generating mods and virtual sites: \n{0}'.format(std_out))
    else:
        logger.error(
            'Error on generating freeradius mods and virutal sites; {0}'.format(std_out))
        raise ConfigOverwriteFailed('Failed to overwrite config')

    logger.info('Config overwritten')


def backup_current_config(main_config_dir, backup_config_dir):
    logger.info('Backup config dir...')
    shutil.copytree(main_config_dir, backup_config_dir, symlinks=True)
    logger.info('Backup finished')


def restore_backup_config(main_config_dir, backup_config_dir):
    logger.info('Restore backup config...')
    shutil.rmtree(main_config_dir)
    shutil.copytree(backup_config_dir, main_config_dir, symlinks=True)
    logger.info('Config restored')


def get_radiusd_pid(sleep=False):
    if sleep: time.sleep(5)
    p = Popen(['ps', '-A'], stdout=PIPE)
    out, err = p.communicate()
    for line in out.splitlines():
        if b'radiusd' in line:
            return int(line.split(None, 1)[0])
    return None




def main():
    if environ.get(SERVER_ID_ENV) is None:
        raise ConfigOverwriteFailed(
            'Environment vairalbe {0} not set'.format(SERVER_ID_ENV))

    try:
        new_conf_timestamp = read_string_from_s3(REGION, BUCKET_NAME, KEY_NAME)
        curr_conf_timestamp = get_current_timestamp(CURRENT_CONF_TIMESTAMP_PATH)
    except IOError as err:
        logger.exception(
            'Failed retrieving config timestamp from localy stored file: {0}'.format(err))
        exit(1)
    except Exception as err:
        logger.exception(
            'Failed retrieving config timestamp from s3: {0}'.format(err))
        exit(1)

    if new_conf_timestamp > curr_conf_timestamp:
        logger.info(
            'freeradius config timestamp is different [{0}, {1}].'.format(
                curr_conf_timestamp.strip(), new_conf_timestamp.strip())
        )

        try:
            backup_current_config(main_config_dir=MAIN_CONFIG_DIR,
                                  backup_config_dir=BACKUP_CONFIG_DIR)
        except Exception as err:
            logger.exception('Failed to backup current config; {0}\nDo nothing'.format(err))
            exit(1)

        try:
            owerwrite_config()
            kill_freeradius()
            time.sleep(SLEEPSEC)
            new_pid = get_radiusd_pid()
            time.sleep(STARTSEC)
            pid_5_sec_after = get_radiusd_pid()


            if new_pid != pid_5_sec_after or pid_5_sec_after is None:
                raise ConfigOverwriteFailed('new_pid and pid_5_sec_after do not match')

            proxy_test()
            save_timestamp(CURRENT_CONF_TIMESTAMP_PATH, new_conf_timestamp)
            shutil.rmtree(BACKUP_CONFIG_DIR)
            return 0
        except ConfigOverwriteFailed as err:
            logger.error(err)
            restore_backup_config(main_config_dir=MAIN_CONFIG_DIR,
                                  backup_config_dir=BACKUP_CONFIG_DIR)
            logger.info('Backup config {0}, was not deleted'.format(
                BACKUP_CONFIG_DIR))
            kill_freeradius()
            try:
                proxy_test()
            except ConfigOverwriteFailed as err:
                logger.exception(err)
            exit(1)
        except Exception as err:
            logger.exception(err)
            exit(1)


if __name__ == '__main__':
    main()
