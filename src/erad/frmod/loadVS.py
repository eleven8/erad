#!/usr/bin/env python3


#
# Radius VirtualServer creator - Loading data from /erad/system/server/config/load.
#
import sys, getopt, os
import string
import json
import requests
from erad import util


def display(server_ip, port, secret, cert_id):
    cert_id = '' if cert_id is None else cert_id
    selected_eap_moudle = 'eap{cert_id}'.format(cert_id = cert_id)
    return \
"""server 11os-all-clients""" +str(port)+ """ {
    listen {
        type = auth
        ipaddr = *      #all addresses
        port = """ +str(port)+ """     #/etc/services (1812)

        limit {
              max_pps = 10000             #relief valve (packets per second)
              max_connections = 0       #no limit
              lifetime = 60             #seconds
              idle_timeout = 30         #seconds
        }
    }

    listen {
        ipaddr = *
        port = """ +str(int(port)+1)+ """
        type = acct
    }

    client ElevenWireless {
        ipaddr = 0.0.0.0/0
        secret = """ +secret+ """
    }

    authorize {
        preprocess      # sanitizes attributes
        auth_log        # log authorization request

        update request {
            Supplicant-Radius-Ip := """+str(server_ip)+"""
            Supplicant-Radius-Port := """+str(port)+"""
            Selected-EAP-Module := """  + selected_eap_moudle + """
        }

        # Handle realm in User-Name attribute
        suffix

        chap
        mschap

        update { &control:Cache-Status-Only := 'yes' }
        cache_erad

        # Check if cache has records about negative response
        # if has not, calling erad_auth
        if(notfound) {
            erad_auth

            # if erad_auth not found supplicant, then remeber it
            # to not call erad_auth with same arguments
            if (noop) {
                update { &control:Cache-Status-Only := 'no' }
                cache_erad
            }
            elsif (updated){
                update control {
                    Cleartext-Password := &control:Cleartext-Password
                }

                if(&reply:Supplicant-Use-Remote == 'yes') {
                    update control {
                        Proxy-To-Realm := &reply:Supplicant-Proxy-Id
                        Cleartext-Password !* ANY
                    }
                }

                # If request not EAP, there will be only one identity
                # `save_reply_to_session_state` will remember this identity.
                #
                # Identities not known by ERAD API will not triger update action in erad_auth,
                # so `save_reply_to_session_state` will not be executed
                #
                # If request is EAP (two identities `outer` and `inner`)
                # and full EAP session should be proxied, `save_reply_to_session_state` will remember
                # only `outer` identity. But this identity will be real identity, because ERAD radius server
                # does not use realms for request proxying.
                # Home radius server may return `User-Name` and it should match with identity in proxy request.
                # Also home radius server may return `Chargeable-User-Identity` attribute (CUI)
                # see https://tools.ietf.org/html/rfc4372. Currently ERAD radius not support CUI.
                #
                # If EAP request should not be proxied, or only `inner` tunnel part should be proxied,
                # `save_reply_to_session_state` will save outer identity,
                # and `save_reply_to_outer_session_state` site will save inner identity in session state.
                #
                # Inner identity is valid, and will be used in logs etc.
                #
                # If EAP request contains malicious user identity in anonymous identity.
                # It means anonymous identity field contains real username, known by ERAD API,
                # but password from this identity not known.
                # If such request should be proxied - home server will reject request without proper password.
                # If request will be handled locally, `peap_and_ttls` virtual site will have access to real identity.

                save_reply_to_session_state  # (see policy identity_helpers)
            }
        }

        """ + selected_eap_moudle + """ {
            ok = return
        }

        pap

        # Setting the last customized Auth-Type, to make sure that `Auth-Type` will not be overwritten
        if(&control:Supplicant-Auth-Type == 'ERAD' && !&EAP-Message) {
            update control {
                Auth-Type := ERAD
                Cleartext-Password !* ANY
            }
        }
    }

    authenticate {
        #run only one of this
        Auth-Type ERAD{
            erad_auth
        }
        Auth-Type PAP {
            pap
        }

        Auth-Type CHAP {
            chap
        }

        Auth-Type MS-CHAP {
            mschap
        }

        """ + selected_eap_moudle + """

    }

    preacct {
        preprocess #sanitize
        update request {
            Supplicant-Radius-Ip := """+str(server_ip)+"""
            Supplicant-Radius-Port := """+str(port)+"""
        }
        erad_auth
        acct_unique
    }

    accounting {
        unix
        exec
        attr_filter.accounting_response
    }

    session {
    }


    # &control - per packet, and can be empty
    post-auth {

        # `copyback_user_identity` - copy user identity to request attribute list.
        # It can be or `Supplicant-Inner-User-name` or `Supplicant-User-name` (outer identity),
        # if first is not available.
        # `erad_auth` query database for additional attributes (see policy identity_helpers).
        # Also note, `erad_auth` will replace any attributes values, that previously was set
        # by proxy reply response or by other modules or instructions.
        update request {
            Supplicant-Timestamp := "%l"
            Supplicant-Packet-Type := "%{Packet-Type}"
            Supplicant-Auth-Result := "%{reply:Packet-Type}"
        }
        copyback_user_identity
        erad_auth
        exec
        remove_reply_message_if_eap
        linelog

        Post-Auth-Type REJECT {

        # `copyback_user_identity` - copy user identity to request attribute list.
        # It can be or `Supplicant-Inner-User-name` or `Supplicant-User-name` (outer identity),
        # if first is not available.
        # `erad_auth` query database for additional attributes (see policy identity_helpers).
        # Also note, `erad_auth` will replace any attributes values, that previously was set
        # by proxy reply response or by other modules or instructions.
        update request {
            Supplicant-Timestamp := "%l"
            Supplicant-Packet-Type := "%{Packet-Type}"
            Supplicant-Auth-Result := "%{reply:Packet-Type}"
        }
        copyback_user_identity
        erad_auth
        linelog
        attr_filter.access_reject
        """ + selected_eap_moudle + """
        remove_reply_message_if_eap
        }
    }

    pre-proxy {
    }

    post-proxy {
        # Reply from home server will replaced "local" reply list value.
        # post_auth section will query additional attributes from ERAD API
        # and will replace proxy reply attributes with it

        """ + selected_eap_moudle + """
    }
}

"""


def main(argv):
    def json_post( path, input ):
        return requests.post(
            util.erad_cfg().api.host + path,  # official version
            #erad_cfg().api.host + path,
            headers={ 'Content-type': 'application/json' },
            data=json.dumps(input))


    serverId = ''
    try:
      opts, args = getopt.getopt(argv,"s:",["serverId="])
    except getopt.GetoptError:
      print('loadVS.py -s <serverId>')
      sys.exit(2)
    for opt, arg in opts:
        if opt in ('-s', '--serverId'):
            serverId = arg
        else:
             print('loadVS.py -s <serverId>')
             sys.exit()

    if (serverId == ''):
        print('loadVS.py -s <serverId>')
        sys.exit()

    data = json_post(
        '/erad/system/server/config/load',
        {
            "SystemKey": util.erad_cfg().system_key[0],
            "ClusterId": serverId,
            "ServerType": "radius"
        })

    vs_list = data.json()

    server_ip = vs_list["ServerIP"]

    assert(len(vs_list["VirtualServers"]) == len(vs_list["Certs"]))
    certs = vs_list["Certs"]

    print("")
    print(("# ServerId: %s" % serverId))
    print(("# ServerIP: %s" % server_ip))
    print("")

    for port in vs_list["VirtualServers"]:
        secret = vs_list["VirtualServers"][port]
        cert_id = certs[port]

        print((display(server_ip, port, secret, cert_id)))


if __name__ == "__main__":
    main(sys.argv[1:])
