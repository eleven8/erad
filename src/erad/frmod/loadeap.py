# coding:utf-8

import argparse
from os import path
import json
import requests
from erad import util

TEMPLATE = '''eap eap{CERT_NAME}{{
    default_eap_type = md5
    timer_expire     = 60
    ignore_unknown_eap_types = no
    cisco_accounting_username_bug = no
    max_sessions = ${{max_sessions}}
    md5 {{
    }}
    gtc {{
        auth_type = PAP
    }}
    tls-config tls-common {{
        private_key_password = {CERT_PASSWORD}
        private_key_file = ${{certdir}}/server_{CERT_NAME}.pem
        certificate_file = ${{certdir}}/server_{CERT_NAME}.pem
        ca_file = ${{cadir}}/ca.pem
        dh_file = ${{certdir}}/dh
        ca_path = ${{cadir}}
        cipher_list = "DEFAULT"
        ecdh_curve = "prime256v1"
        cache {{
            enable = yes
            lifetime = 24 # hours
            max_entries = 255
        }}
        verify {{
        }}
        ocsp {{
            enable = no
            override_cert_url = yes
            url = "http://127.0.0.1/ocsp/"
        }}
    }}
    tls {{
        tls = tls-common
        virtual_server = tls_only
    }}
    ttls {{
        tls = tls-common
        default_eap_type = md5
        copy_request_to_tunnel = yes
        use_tunneled_reply = yes
        virtual_server = peap_and_ttls
    }}
    peap {{
        tls = tls-common
        default_eap_type = mschapv2
        copy_request_to_tunnel = yes
        use_tunneled_reply = yes
        proxy_tunneled_request_as_eap = yes
        virtual_server = peap_and_ttls
    }}
    mschapv2 {{
    }}
}}

'''

PEAP_AND_TTLS_TEMPLATE = '''server peap_and_ttls {{
    authorize {{

        # Handle realm in User-Name attribute
        suffix

        chap
        mschap

        update {{ &control:Cache-Status-Only := 'yes' }}
        cache_erad

        # Check if cache has records about negative response
        # if has not, calling erad_auth
        if(notfound) {{
            erad_auth

            # if erad_auth not found supplicant, then remeber it
            # to not call erad_auth with same arguments
            if (noop) {{
                update {{ &control:Cache-Status-Only := 'no' }}
                cache_erad
            }}

            elsif (updated){{

                update control {{
                    Cleartext-Password := &control:Cleartext-Password
                }}

                if(&reply:Supplicant-Use-Remote == 'yes') {{
                    update control {{
                        Proxy-To-Realm := &reply:Supplicant-Proxy-Id
                        Cleartext-Password !* ANY
                    }}
                }}
            }}
        }}

        # The next instruction will save inner identity into outer session state,
        # so it can be used in post-auth section (see policy identity_helpers).
        #
        # In case when erad_auth don't know about supplicant previous "if" clause will not be executed.
        # The next instruction save inner identity, so it can be used in linelog Post-Auth-Type REJECT section.
        save_reply_to_outer_session_state


        # Can't do both - call eap module and proxy request.
        # Should choose if Supplicant require remote - do nothing,
        # because earlier we set `Proxy-To-Realm` attribute.
        # Otherwise calling eap module and do authentication locally.

        if(&reply:Supplicant-Use-Remote == 'yes') {{

        }} else {{
{EAP_BLOCK}
        }}

        pap

        # Setting the last customized Auth-Type, to make sure that `Auth-Type` will not be overwritten
        if(&control:Supplicant-Auth-Type == 'ERAD') {{
            update control {{
                Auth-Type := ERAD
                Cleartext-Password !* ANY
            }}
        }}

    }}

    authenticate {{
        #run only one of this
        Auth-Type ERAD{{
                erad_auth
        }}
        Auth-Type PAP {{
                pap
        }}

        Auth-Type CHAP {{
                chap
        }}
        Auth-Type MS-CHAP {{
                mschap
        }}
{EAP_AUTH_BLOCK}
    }}

    post-auth {{
        Post-Auth-Type REJECT {{
            attr_filter.access_reject
        }}
    }}

    pre-proxy {{
    }}

    post-proxy {{
{EAP_BLOCK}
    }}
}}

'''

TLS_ONLY_TEMPLATE = '''server tls_only {{
    authorize {{

        # Handle realm in User-Name attribute
        suffix

        update {{ &control:Cache-Status-Only := 'yes' }}
        cache_erad_tls

        # Check if cache has records about negative response
        # if has not, calling erad_auth
        if (notfound) {{
            erad_auth

            if (updated){{

                # tls_only virtual site `erad_auth` use `TLS-Client-Cert-Common-Name` attribute
                # to query ERAD API for user identity and it will return real user name
                # The next instruction will save inner identity into outer session state,
                # so it can be used in post-auth section (see policy identity_helpers).
                save_reply_to_outer_session_state

                # and accept supplicant
                update control {{
                    Auth-Type := Accept
                }}

            }} elsif (noop || fail) {{

                # if erad_auth not found supplicant, then remeber it
                # to not call erad_auth with same arguments
                update {{ &control:Cache-Status-Only := 'no' }}
                cache_erad_tls

                # In the eap-tls request username will not be known.
                # Because ERAD API uses only certificates.
                # Successful erad_auth call will return Supplicant-User-Name matched with cert ID.
                # But in case of failure, freeradius will be know only certificates attributes.
                # So saving Common-Name as username. it will be used in linelog.
                # This is possible when certificate provided by supplicant, is valid (or recognized)
                # by freeradius server, but ERAD API not found any registered/active user,
                # with ID (Common Name) from provided certificate.

                # In case when eap module will fail to recognize certificate, nothing will be saved
                # in session-state list and linelog will use anonymous or outer identity, from request.

                update outer.session-state {{
                    Supplicant-Inner-User-Name := TLS-Client-Cert-Common-Name
                    # Supplicant-User-Name += TLS-Client-Cert-Issuer
                }}
            }}
        }}
        elsif (ok) {{
            # If found cache record with negative response
            # update Supplicant-Inner-User-Name becuase `elsif (noop || fail)` clause was not executed

            update outer.session-state {{
                Supplicant-Inner-User-Name := TLS-Client-Cert-Common-Name
                # Supplicant-User-Name += TLS-Client-Cert-Issuer
            }}
        }}
    }}

    # eap-tls not support proxy
    # {{control:Supplicant-Use-Remote}} should always be equal 'no'
    post-auth {{
        Post-Auth-Type REJECT {{
            attr_filter.access_reject
{EAP_BLOCK}
        }}
    }}
}}

'''

TEMPLATE_EAP_BLOCK = '''
# ----------- autogenerated -----------
switch &Selected-EAP-Module {{
{OTHER_CASE_BLOCK}

case eap {{
    eap
}}

}}
# ------------------------------------
'''

TEMPLATE_EAP_AUTH_BLOCK = '''
# ----------- autogenerated -----------

{OTHER_AUTH_BLOCK}

Auth-Type eap {{
    eap
}}


# ------------------------------------
'''


def create_case_block(cert_name):
    return '''
case eap{cert_name} {{
    eap{cert_name}
}}
'''.format(cert_name = cert_name)


def create_auth_block(cert_name):
    return '''
Auth-Type eap{cert_name} {{
    eap{cert_name}
}}
'''.format(cert_name = cert_name)


def create_switch_block(case_blocks):
    return TEMPLATE_EAP_BLOCK.format(OTHER_CASE_BLOCK = ''.join(case_blocks))


def create_eap_auth_block(case_blocks):
    return TEMPLATE_EAP_AUTH_BLOCK.format(OTHER_AUTH_BLOCK = ''.join(case_blocks))


def log_save(filename):
    print(('{filename} - saved'.format(filename = filename)))

def write_eap_module(eap_module_path, cert_name):
    eap_module_name = 'eap_{cert_name}'.format(cert_name = cert_name)

    vars = {
        'CERT_PASSWORD': '3l3v3nn3v3l3',
        'CERT_NAME' : cert_name,
    }
    filename = path.join(eap_module_path, eap_module_name)
    with open(filename, 'w') as f:
        data = f.write(TEMPLATE.format(**vars))
        log_save(filename)


def write_virutal_sites(virtual_site_path, case_blocks, auth_blocks):
    EAP_BLOCK = create_switch_block(case_blocks)
    EAP_AUTH_BLOCK = create_eap_auth_block(auth_blocks)

    peap_and_ttls__filename = path.join(virtual_site_path, 'peap_and_ttls')
    tls_only__filename = path.join(virtual_site_path, 'tls_only')

    with open(peap_and_ttls__filename, 'w') as f:
        data = f.write(PEAP_AND_TTLS_TEMPLATE.format(
            EAP_BLOCK = EAP_BLOCK,
            EAP_AUTH_BLOCK = EAP_AUTH_BLOCK
        ))
        f.close()
        log_save(peap_and_ttls__filename)

    with open(tls_only__filename, 'w') as f:
        data = f.write(TLS_ONLY_TEMPLATE.format(EAP_BLOCK = EAP_BLOCK))
        f.close()
        log_save(tls_only__filename)


def json_post( path, input ):
    return requests.post(
        util.erad_cfg().api.host + path,  # official version
        #erad_cfg().api.host + path,
        headers={ 'Content-type': 'application/json' },
        data=json.dumps(input))


def get_cert_info():
    data = json_post(
        '/erad/system/account/certs/list',
        {
            "SystemKey": util.erad_cfg().system_key[0]
        })

    return data.json()

def write_data(eap_module_path, virtual_site_path, certs_data):
    case_blocks = []
    auth_blocks = []

    for account_id, cert_name in list(certs_data.items()):
        # If Eraad_AccountOwner model has not dedicated certificate
        # not create eap module, it will be use default `eap` module
        if cert_name is None:
            continue

        # commented out because default eap module created manually
        # # create default eap module
        # if cert_name == 'default':
        #   write_eap_module(eap_module_path, '')
        case_blocks.append(create_case_block(cert_name))
        auth_blocks.append(create_auth_block(cert_name))
        write_eap_module(eap_module_path, cert_name)

    write_virutal_sites(virtual_site_path, case_blocks, auth_blocks)


def main(eap_module_path, virtual_site_path):
    certs_data = get_cert_info()['Certs']
    write_data(eap_module_path, virtual_site_path, certs_data)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Create EAP modules and vritual sites for freeradius server')
    parser.add_argument('eap_module_path',
                        metavar = 'eap-module-path',
                        type=str,
                        help='Specify the path, where eap modules will be saved'
                        )

    parser.add_argument('virtual_site_path',
                        metavar = 'virtual-site-path',
                        type=str,
                        help='Specify the path, where virtual sites will be saved')

    args = parser.parse_args()
    main(args.eap_module_path, args.virtual_site_path)
