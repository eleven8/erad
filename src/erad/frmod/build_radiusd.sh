#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
##
## This build script produce a tar.gz file with the proper files for
## deployment and pushes them to s3 if it not a dev version.
##
################################################################################

export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
# AWS_SECRET_ACCESS_KEY injected by Jenkins
export AWS_DEFAULT_REGION=us-west-2

VERSION=$1
if [ -z "$VERSION" ]
then
    VERSION="DEV"
fi

# on dev boxes, make sure required packages are installed
# script must run as root for this to work
if [ "$VERSION" == "DEV" ]
then
   yum -y install git-core gcc libtalloc-devel openssl-devel

   git clone https://github.com/FreeRADIUS/freeradius-server.git ..
   pushd freeradius-server
   git checkout v3.0.x
fi

#look up what revision this build was from so we can mark the package.
REVISION=$(git rev-list --reverse HEAD | wc -l)

./configure
make
if [ $? != 0 ]
then
   exit $?
fi

rm -rf ./dist ./freeradius_server_*.tar.gz

mkdir ./dist
export R=$(pwd)/dist

make install
pushd dist
tar -cvzf ../freeradius_server_$REVISION.tar.gz --group=root --owner=root *
popd

# copy deployment archive to S3
if [ "$VERSION" != "DEV" ]
then
   aws s3 cp "freeradius_server_$REVISION.tar.gz" s3://teamfortytwo.deploy/freeradius_server/freeradius_server_$REVISION.tar.gz
   aws s3 cp "freeradius_server_$REVISION.tar.gz" s3://teamfortytwo.deploy/freeradius_server.tar.gz
else
   popd
fi

