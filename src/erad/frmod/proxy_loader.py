# proxy_loader.py is called by crond and checks to see if a new FreeRADIUS proxy.conf on S3
# is available.  If so, we attempt to reload FreeRADIUS with the new proxy configuration,
# and test that FreeRADIUS still functions correctly.  If the test fails, we roll back to the
# old configuration.

# TODO: report bad configurations to Amazon.
# TODO: this file must be moved to sibling of erad and run as a script in order to use its config.

import os
import subprocess
import sys
import time

import boto
from boto.s3.key import Key

# setup
log_file = open('/var/log/proxy_loader_log', 'a')
# cron ignores previously-set environment variables, even in /etc/profile.
# TODO: make this cleaner (i.e. don't do it here)
os.environ["PYTHONPATH"] = '/usr/local/sbin'
sys.stdout = log_file
proxy_conf_path = '/usr/local/etc/raddb/proxy.conf'
STARTSEC = 5
SLEEPSEC = 2  # the time given to freeradius_launcher, to detect stopped(killed) radiusd and restart it


def read_string_from_s3(bucket_name, key):
    # it shouldn't be necessary to specify the credential values like this but it doesn't work without them
    connection = boto.connect_s3(
        boto.config.get_value('Credentials', 'aws_access_key_id'),
        boto.config.get_value('Credentials', 'aws_secret_access_key')
    )
    try:
        bucket = connection.get_bucket(bucket_name)
        s3file = Key(bucket)
        s3file.key = key
        return s3file.get_contents_as_string()
    finally:
        connection.close()


def read_proxy_conf_from_s3():
    return read_string_from_s3("freeradiusproxyconf", "proxy.conf")


def proxy_test():
    ret = os.system(
        'echo "User-Name=SR-980-83/erad-monitor,User-Password=nt5ewa9s,Called-Station-Id=ba-bb-1e-ba-bb-1e" | /usr/local/bin/radclient -t 15 -x -r 1 -D /usr/local/share/freeradius/ 127.0.0.1:1814 auth V72QSZ2CE3 | grep "Received Access-Accept"')
    if ret != 0:
        print(("Error: Bad proxy test.  Status: " + str(ret)))
        return False

    return True


def get_radiusd_pid(sleep=False):
    if sleep: time.sleep(5)
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE, text=True)
    out, err = p.communicate()
    for line in out.splitlines():
        if 'radiusd' in line:
            return int(line.split(None, 1)[0])
    return None


def kill_freeradius():
    subprocess.Popen(
        ['killall', '-9', 'radiusd'],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )


def get_current_proxy_as_string():
    with open(proxy_conf_path, 'rb') as proxy_file:
        return proxy_file.read()


def write_current_proxy_file(content):
    if type(content) is str:
        content = content.encode('utf-8')

    with open(proxy_conf_path, 'wb') as proxy_file:
        proxy_file.write(content)


try:
    new_proxy_conf = read_proxy_conf_from_s3()
except Exception as e:
    print('Error: failed retrieving proxy conf from s3')
    print(e)
    sys.exit(-1)

curr_proxy_conf = get_current_proxy_as_string()
if new_proxy_conf != curr_proxy_conf:
    print('Info: overwriting proxy.conf...')
    write_current_proxy_file(new_proxy_conf)
    print('Info: stopping(killing) freeradius')
    kill_freeradius()

    time.sleep(SLEEPSEC)
    new_pid = str(get_radiusd_pid())
    time.sleep(STARTSEC)
    pid_5_sec_after = str(get_radiusd_pid())

    if new_pid != pid_5_sec_after or pid_5_sec_after is None or not proxy_test():
        print('Error: failed to start with new proxy config; revert')
        write_current_proxy_file(curr_proxy_conf)
        kill_freeradius()
