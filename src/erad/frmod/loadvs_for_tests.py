# coding: utf-8

#
# This file contains display() function, which repeat function from loadVS.py,
# but create virtual sites wich allow only eap request.
# It is used for creation home radius server instance, that acts like Marriott home servers
#

import sys, getopt, os
import string
import json
import requests
from erad import util


def display(server_ip, port, secret, cert_id):
    cert_id = '' if cert_id is None else cert_id
    selected_eap_moudle = 'eap{cert_id}'.format(cert_id = cert_id)
    return \
"""server 11os-all-clients""" +str(port)+ """ {
    listen {
        type = auth
        ipaddr = *      #all addresses
        port = """ +str(port)+ """     #/etc/services (1812)

        limit {
              max_pps = 100             #relief valve (packets per second)
              max_connections = 0       #no limit
              lifetime = 60             #seconds
              idle_timeout = 30         #seconds
        }
    }

    listen {
        ipaddr = *
        port = """ +str(int(port)+1)+ """
        type = acct
    }

    client ElevenWireless {
        ipaddr = 0.0.0.0/0
        secret = """ +secret+ """
    }

    authorize {
        preprocess      # sanitizes attributes
        auth_log        # log authorization request

        # Handle realm in User-Name attribute
        suffix

        update request {
            Supplicant-Radius-Ip := """+str(server_ip)+"""
            Supplicant-Radius-Port := """+str(port)+"""
            Selected-EAP-Module := """  + selected_eap_moudle + """
        }

        """ + selected_eap_moudle + """ {
            ok = return
        }
    }

    authenticate {
        #run only one of this
        Auth-Type ERAD{
            erad_auth
        }
        Auth-Type PAP {
            pap
        }

        Auth-Type CHAP {
            chap
        }

        Auth-Type MS-CHAP {
            mschap
        }

        """ + selected_eap_moudle + """

    }

    preacct {
            preprocess #sanitize
            `/opt/savelog/acct_save_log.py '%{pairs:request:}, IpAddress = """+str(server_ip)+""", Port = """+str(port)+"""'`
            acct_unique
    }

    accounting {
            unix
            exec
            attr_filter.accounting_response
    }

    session {
    }



    # &control - per packet, and can be empty

    post-auth {

        # `copyback_user_identity` - copy user identity to request attribute list.
        # It can be or `Supplicant-Inner-User-name` or `Supplicant-User-name` (outer identity),
        # if first is not available.
        # `erad_auth` query database for additional attributes (see policy identity_helpers)
        # Also note, `erad_auth` will replace any attributes values, that previously was set
        # by proxy reply response or by other modules or instructions.
        copyback_user_identity
        erad_auth

        exec
        remove_reply_message_if_eap


        linelog
        `/opt/savelog/save_log.py \\
            PacketType=%{Packet-Type},\\
            UserName=%{%{reply:Supplicant-Real-User-Name}:-%{User-Name}},\\
            CalledStationId=%{Called-Station-Id},\\
            CallingStationId=%{Calling-Station-Id},\\
            EventTimestamp=%{Event-Timestamp},\\
            NASIdentifier=%{NAS-Identifier},\\
            NASIPAddress=%{NAS-IP-Address},\\
            Timestamp=%l,\\
            Access=%{reply:Packet-Type},\\
            IsProxied=%{%{reply:Supplicant-Use-Remote}:-no},\\
            EndpointIPAddress="""+str(server_ip)+""",\\
            EndpointPort="""+str(port)+"""`


        Post-Auth-Type REJECT {

        # `copyback_user_identity` - copy user identity to request attribute list.
        # It can be or `Supplicant-Inner-User-name` or `Supplicant-User-name` (outer identity),
        # if first is not available.
        # `erad_auth` query database for additional attributes (see policy identity_helpers)
        # Also note, `erad_auth` will replace any attributes values, that previously was set
        # by proxy reply response or by other modules or instructions.
        copyback_user_identity
        erad_auth

        linelog
        `/opt/savelog/save_log.py \\
            PacketType=%{Packet-Type},\\
            UserName=%{%{reply:Supplicant-Real-User-Name}:-%{User-Name}},\\
            CalledStationId=%{Called-Station-Id},\\
            CallingStationId=%{Calling-Station-Id},\\
            EventTimestamp=%{Event-Timestamp},\\
            NASIdentifier=%{NAS-Identifier},\\
            NASIPAddress=%{NAS-IP-Address},\\
            Timestamp=%l,\\
            Access=%{reply:Packet-Type},\\
            IsProxied=%{%{reply:Supplicant-Use-Remote}:-no},\\
            EndpointIPAddress="""+str(server_ip)+""",\\
            EndpointPort="""+str(port)+"""`

            attr_filter.access_reject
            """ + selected_eap_moudle + """
            remove_reply_message_if_eap
        }
    }

    pre-proxy {

    }

    post-proxy {
        # Reply from home server will replaced "local" reply list value.
        # post_auth section will query additional attributes from ERAD API
        # and will replace proxy reply attributes with it

        """ + selected_eap_moudle + """
    }
}

"""
