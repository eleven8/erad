#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import re
from erad import util
from erad.exceptions import *
from .erad_model import *
from .interface_util import *
from .auditor import Auditor

class EradInterfaceIntegration(EradInterfaceUtil):

    group_possible_none_fields = {
        'OverrideVlan',
        'OverrideConnectionSpeed',
    }


    # internal method
    def authorize_integration_key( self, fields ):
        key_input = self.expect_parameter( fields, 'IntegrationKey' )
        try:
            apikey = Erad_ApiKey.get(key_input)
        except DoesNotExist:
            raise EradAccessException( "Access Denied." )

        if not apikey.Active:
            raise EradAccessException( "Key disabled." )

    def session_save( self, fields ):
        # authorize the caller
        self.authorize_integration_key( fields )
        key_input = self.expect_parameter( fields, 'IntegrationKey' )

        # make the session
        session_model = Erad_Session()
        session_model.AccountOwner = 0
        session_fields = self.expect_dict( fields, session_model.Meta.name )
        self.set_unicode_value_nonempty( session_model, session_fields, "ID" )
        self.set_unicode_value( session_model, session_fields, "Identifier" )
        self.set_unicode_value( session_model, session_fields, "LogoutUrl" )
        self.set_unicode_array( session_model, session_fields, "Group_ID_List" )
        self.set_optional_integer_value( session_model, session_fields, "AccountOwner")

        # lookup for the Account_ID
        session_model.Account_ID = Erad_ApiKey.get(key_input).Account_ID

        session_model.LastUsed = util.utcnow()

        # lookup the Account_ID in Erad_Group2 when AccountOwner is true
        if session_model.AccountOwner == 1:
            groups = []
            for group_item in Erad_Group2.query(session_model.Account_ID):
                groups.append( group_item.ID )
            # Save the groups in Group_ID_List
            session_model.Group_ID_List = groups

        # save the session
        session_model.save()

        # delete older matching sessions
        self.delete_older_sessions(session_model)

        # audit save of session
        if util.erad_cfg().audit.integration.session:
            for group_id in session_model.Group_ID_List:
                Auditor().record(
                    "Create Session",
                    group_id,
                    session_model.Identifier
                )

        return self.standard_output_object( session_model )

    def group_load( self, fields ):
        # authorize the caller
        self.authorize_integration_key( fields )

        # make the group
        group_model = Erad_Group2()
        group_fields = self.expect_dict( fields, group_model.Meta.name )
        self.set_unicode_value_nonempty( group_model, group_fields, "ID" )

        key_input = self.expect_parameter( fields, 'IntegrationKey' )
        group2_account_id = Erad_ApiKey().get(key_input).Account_ID

        group_model = Erad_Group2.get(group2_account_id, group_model.ID)
        # save the group
        group_model.refresh(consistent_read=True)

        return self.standard_output_object_none2false(group_model, self.group_possible_none_fields)

    def group_save( self, fields ):
        # authorize the caller
        self.authorize_integration_key( fields )

        # TODO: This should only be a partial update? We dont want to save RemoteServerUrl/Secret
        # Currently the logic to not overwrite those fields exists on the 11OS side

        # Save Account_ID
        key_input = self.expect_parameter( fields, 'IntegrationKey' )
        account_id = Erad_ApiKey().get(key_input).Account_ID

        group_fields = self.expect_dict( fields, Erad_Group2.Meta.name )
        group_id = str(self.expect_parameter(group_fields, "ID"))


        try:
            group_model = Erad_Group2.get(account_id, group_id)
        except DoesNotExist:
            group_model = Erad_Group2()
            # Seting default values for optional fields of new group.
            # Because functions set_optional_*() will not set values for optional fields.
            # If not do this db will store values as None
            group_model.MaxDownloadSpeedBits = 0
            group_model.MaxUploadSpeedBits = 0
            group_model.OverrideVlan = False
            group_model.OverrideConnectionSpeed = False


        group_model.Account_ID = account_id

        self.set_unicode_value_nonempty( group_model, group_fields, "ID" )
        self.set_unicode_value( group_model, group_fields, "Name" )
        self.set_unicode_value( group_model, group_fields, "RemoteServerUrl" )
        self.set_unicode_value( group_model, group_fields, "SharedSecret" )
        self.set_optional_unicode_value( group_model, group_fields, "Domain" )
        self.set_optional_unicode_value( group_model, group_fields, "TimeZone" )
        self.set_optional_integer_value( group_model, group_fields, "MaxDownloadSpeedBits")
        self.set_optional_integer_value( group_model, group_fields, "MaxUploadSpeedBits")
        self.set_optional_boolean_value( group_model, group_fields, "OverrideVlan")
        self.set_optional_boolean_value( group_model, group_fields, "OverrideConnectionSpeed")
        self.set_optional_unicode_value( group_model, group_fields, "ClientInfoArn" )

        # save the group
        if (re.search('::', group_model.ID) is not None):
            raise EradValidationException( "Invalid ID." )
        group_model.save()


        if self.expect_optional_boolean_parameter(group_fields, "AddAuthenticator") is not False:
            auth_model = Erad_Authenticator2(
                    ID=group_model.ID,
                    Account_ID=account_id,
                    Group_ID=group_model.ID,
                    RadiusAttribute="nas-identifier")
            auth_model.save()


        return self.standard_output_object_none2false(group_model, self.group_possible_none_fields)


