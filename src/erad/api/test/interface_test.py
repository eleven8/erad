#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad import util, CoreTest
from ..erad_model import *

class InterfaceTest(CoreTest):

    #------ Helper Methods --------

    # Generates JSON input of the system key
    def get_input_system_key(self):
        return {"SystemKey" : util.erad_cfg().system_key[0]}

    def get_input_system_key_auth_sup(self, a_id, s_name):
        return {"SystemKey" : util.erad_cfg().system_key[0],
                "Authenticator" : {'ID':str(a_id)},
                "Supplicant" : {'Username' : str(s_name)}
                }

    def get_input_account_save(self, a_email, a_pass, a_aid):
        return {"SystemKey" : util.erad_cfg().system_key[0],
                "AccountOwner": {"Email": str(a_email),
                                 "Password": str(a_pass),
                                 "Account_ID": str(a_aid)}
                }

    def get_input_account_login(self, a_email, a_pass):
        return {"AccountOwner": {"Email": str(a_email),
                                 "Password": str(a_pass)}
                }

    # Generates JSON input of a key
    def get_input_integration_key(self):
        return {"IntegrationKey" :  self.std_integration_key}

    def get_input_integration_key_disabled(self):
        return {"IntegrationKey" :  self.std_integration_key_disabled}

    def get_input_key_group(self, g_id):
        return {"IntegrationKey" :  self.std_integration_key,
                "Group": {'ID':str(g_id)}
                }

    def get_input_key_auth(self, a_id):
        return {"IntegrationKey" :  self.std_integration_key,
                "Authenticator": {'ID':str(a_id)}
                }

    def set_input_key_auth(self, a_id, a_gid, a_radatt = ""):
        return {"IntegrationKey" :  self.std_integration_key,
                "Authenticator": {'ID':str(a_id),
                                  'Group_ID':str(a_gid),
                                  'RadiusAttribute':str(a_radatt)}
                }


    def set_input_key_group(self, g_id, g_n, g_url, g_ss, g_tz, g_aa = ""):
        return {"IntegrationKey" :  self.std_integration_key,
                "Group": {'ID':str(g_id),
                          'Name':str(g_n),
                          'RemoteServerUrl':str(g_url),
                          'SharedSecret':str(g_ss),
                          'TimeZone':str(g_tz),
                          'AddAuthenticator':str(g_aa)}
                }

    def set_input_session(self, ses_id, ses_i, ses_url, ses_glist, ses_ao):
        return {"Session": {"ID":str(ses_id),
                "Identifier":str(ses_i),
                "AccountOwner":ses_ao,
                "LogoutUrl":str(ses_url),
                "Group_ID_List":ses_glist}}

    def set_input_key_session(self, ses_id, ses_i, ses_url, ses_glist, ses_ao ):
        return {"IntegrationKey" :  self.std_integration_key,
                "Session": {"ID":str(ses_id),
                "Identifier":str(ses_i),
                "AccountOwner":ses_ao,
                "LogoutUrl":str(ses_url),
                "Group_ID_List":ses_glist } }

    # Generates JSON input of a session
    # for get/delete
    def get_input_session(self, ses_id):
        return {"Session" : {"ID" : str(ses_id)}}

    # Generates JSON input of a session and group
    # for get/delete
    def get_input_session_group(self, ses_id, g_id):
        return {"Group": {'ID':str(g_id)},
                "Session" : {"ID" : str(ses_id)}}

    # Generates JSON input of sessions, group,and supplicant
    # for get/delete
    def get_input_session_group_sup(self, ses_id, g_id, sup_u):
         return {"Group": {'ID':str(g_id)},
                "Session" : {"ID" : str(ses_id)},
                "Supplicant": {'Group_ID':str(g_id),
                            'Username':str(sup_u)}
         }

    # Generates JSON input of a Supplicant Log List
    def get_input_supplicant_log_list(self, ses_id, g_id, instance_interval):
         return {"Session" : {"ID" : str(ses_id)},
                "Radius_Log": {
                            'Group_ID':str(g_id),
                            'Include_Stray':True,
                            'Include_Bubble':False,
                            'Instance_Interval': instance_interval
                            }
         }
