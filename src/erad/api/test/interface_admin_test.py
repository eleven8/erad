#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from unittest import mock
from erad.exceptions import *
from erad.api.interface_admin import *
from erad.api.test.interface_test import *
from erad.util import make_unique_instance, instance_pack

from datetime import timedelta
import time
import datetime
import random
from nose.tools import nottest


class InterfaceAdminTest(InterfaceTest):

    def setUp(self):
        self.myAdminI = EradInterfaceAdmin()
        self.maxDiff = None


    def test_authorize_session_for_group(self):
        # No Exception
        session_model = Erad_Session("unit_test_session", Group_ID_List=["1"], LastUsed=util.utcnow())
        self.myAdminI.authorize_session_for_group(session_model, "1")

        # Invalid group, deny
        self.assertRaisesRegex(EradAccessException, "Access Denied.",self.myAdminI.authorize_session_for_group,session_model, 2)


    def test_load_session(self):
        expect = Erad_Session(ID=str("unit_test_session"), Group_ID_List=set(["1","2"]), Account_ID="elevenos", Identifier="jimmy.mcgill", LastUsed=util.utcnow())
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["1","2"], Account_ID="elevenos", Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )

        input = self.get_input_session("unit_test_session")
        result = self.myAdminI.load_and_use_session( input )

        # Did we get a valid session back?
        self.assertIsNotNone( result )
        self.assertEqual(result.ID, expect.ID)
        self.assertEqual(result.Group_ID_List, expect.Group_ID_List)

        # Test a session ID not in DB
        input = self.get_input_session("invalid_session")
        self.assertRaises(EradSessionExpiredException,self.myAdminI.load_and_use_session,input )


    def test_group_list(self):
        expect = { 'Count': 2, 'Group_List' : [
                                    {'Account_ID':'elevenos',
                                    'ID':'1',
                                    'Domain': None,
                                    'MaxDownloadSpeedBits':0,
                                    'MaxUploadSpeedBits':0,
                                    'Name':'Group1',
                                    'RemoteServerUrl':'1.2.3.4:50',
                                    'SharedSecret':'abc123',
                                    'TimeZone':'Asia/Qyzylorda',
                                    'OverrideVlan': False,
                                    'OverrideConnectionSpeed': False,
                                    'highCapacityProvisioned': False,
                                    'ClientInfoArn': None,
                                    },
                                    {'Account_ID':'elevenos',
                                    'ID':'2',
                                    'Domain':'another.com',
                                    'MaxDownloadSpeedBits':0,
                                    'MaxUploadSpeedBits':0,
                                    'Name':'Group2',
                                    'RemoteServerUrl':'1.2.3.4:50',
                                    'SharedSecret':'abc123',
                                    'TimeZone':'Asia/Qyzylorda',
                                    'OverrideVlan': False,
                                    'OverrideConnectionSpeed': False,
                                    'highCapacityProvisioned': False,
                                    'ClientInfoArn': None,
                                    }
                                    ] }

        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["1","2"], Account_ID="elevenos", Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )

        # Save Group2
        Erad_Group2.save( Erad_Group2(ID="1", Name="Group1", Account_ID="elevenos", RemoteServerUrl="1.2.3.4:50",
                                                SharedSecret="abc123", TimeZone="Asia/Qyzylorda", MaxDownloadSpeedBits=0, MaxUploadSpeedBits=0))
        Erad_Group2.save( Erad_Group2(ID="2", Name="Group2", Account_ID="elevenos", RemoteServerUrl="1.2.3.4:50",
                                                SharedSecret="abc123", TimeZone="Asia/Qyzylorda", MaxDownloadSpeedBits=0, MaxUploadSpeedBits=0, Domain="another.com"))

        # Valid session with matching group entries
        input = self.get_input_session("unit_test_session")
        result = self.myAdminI.group_list(input)
        self.assertEqual( result, expect )

        # Valid session, invalid groups. Blank list returned
        expect = { 'Count':0, 'Group_List': [] }
        Erad_Session.save( Erad_Session(ID=str("bad_unit_test_session"), Group_ID_List=["3","4"], Account_ID="elevenos", Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )
        input = self.get_input_session("bad_unit_test_session")
        result = self.myAdminI.group_list(input)
        self.assertEqual( result, expect )

        # Invalid session
        input = self.get_input_session("invalid_session")
        self.assertRaises(EradSessionExpiredException, self.myAdminI.group_list,input )


    def test_group_load(self):
        expect = {"Group": {'Account_ID': 'elevenos',
                                    'ID':'1',
                                    'Domain': None,
                                    'MaxDownloadSpeedBits':0,
                                    'MaxUploadSpeedBits':0,
                                    'Name':'Group1',
                                    'RemoteServerUrl':'1.2.3.4:50',
                                    'SharedSecret':'321cba',
                                    'TimeZone':'Asia/Qyzylorda',
                                    'OverrideVlan': False,
                                    'OverrideConnectionSpeed': False,
                                    'highCapacityProvisioned': False,
                                    'ClientInfoArn': None
                                    }}

        Erad_Group2.save( Erad_Group2(ID="1", Name="Group1", Account_ID="elevenos", RemoteServerUrl="1.2.3.4:50",
                                         SharedSecret="321cba", TimeZone="Asia/Qyzylorda", MaxDownloadSpeedBits=0, MaxUploadSpeedBits=0))

        # Valid session and group
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["1","2"], Account_ID="elevenos", Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )
        input = self.get_input_session_group("unit_test_session", "1")
        result = self.myAdminI.group_load(input)
        self.assertEqual( result, expect )


        expected_result_with_unique_devices = {
            "Group": {
                'Account_ID': 'elevenos',
                'ID': '1',
                'Domain': None,
                'MaxDownloadSpeedBits': 0,
                'MaxUploadSpeedBits': 0,
                'Name': 'Group1',
                'RemoteServerUrl': '1.2.3.4:50',
                'SharedSecret': '321cba',
                'TimeZone': 'Asia/Qyzylorda',
                'UniqueDevices': 0,
                'OverrideVlan': False,
                'OverrideConnectionSpeed': False,
                'highCapacityProvisioned': False,
                'ClientInfoArn': None
            }
        }

        result = self.myAdminI.get_group_unique_devices(input)
        self.assertEqual(result, expected_result_with_unique_devices)

        # Valid session, invalid group
        Erad_Session.save( Erad_Session(ID=str("bad_unit_test_session"), Group_ID_List=["3","4"], Account_ID="elevenos", Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )
        input = self.get_input_session_group("bad_unit_test_session","1")
        self.assertRaisesRegex(EradAccessException,"Access Denied", self.myAdminI.group_load,input)

        # Invalid session
        input = self.get_input_session_group("invalid_session","1")
        self.assertRaises(EradSessionExpiredException, self.myAdminI.group_load,input)



    def test_group_save(self):
        test_session = Erad_Session(ID=str("unit_test_session"), Group_ID_List=["222"], Account_ID="elevenos", AccountOwner=1, Identifier="jimmy.mcgill", LastUsed=util.utcnow())
        test_session.save()

        expect = {"Group": {'Account_ID':'elevenos',
                                    'ID':'222',
                                    'Domain':'client.com',
                                    'MaxDownloadSpeedBits':0,
                                    'MaxUploadSpeedBits':0,
                                    'Name':'Group 222',
                                    'RemoteServerUrl':None,
                                    'SharedSecret': None,
                                    'TimeZone':'America/Los_Angeles',
                                    'OverrideVlan': False,
                                    'OverrideConnectionSpeed': False,
                                    'highCapacityProvisioned': False,
                                    'ClientInfoArn': "{\"test\": 2}",
                                    }}
        # We will try to pass a name anyway
        input = {"Group": {'ID':'222',
                           'Domain':'client.com',
                           'RemoteServerUrl': None,
                           'SharedSecret': None,
                           'ClientInfoArn': "{\"test\": 2}"},
                 "Session" : {"ID" : str("unit_test_session")}
                 }


        result = self.myAdminI.group_save(input)
        self.assertEqual(result, expect)

        # Is it actually in our DB?
        result_db = self.myAdminI.group_load(input)
        self.assertEqual(result_db, expect)

        # Missing RemoteServerUrl Param
        input = {"Group": {'ID':'222',
                            'SharedSecret':'321cba'},
         "Session" : {"ID" : str("unit_test_session")}
        }
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter .* is missing.",
                                self.myAdminI.group_save,input)

    def test_group_save_optional_fields(self):
        test_session = Erad_Session(
                            ID=str("unit_test_session"),
                            Group_ID_List=["222"],
                            Account_ID="elevenos",
                            AccountOwner=1,
                            Identifier="jimmy.mcgill",
                            LastUsed=util.utcnow()
                        )
        test_session.save()

        expect = {"Group": {'Account_ID':'elevenos',
                                    'ID':'222',
                                    'Domain':'client.com',
                                    'MaxDownloadSpeedBits':0,
                                    'MaxUploadSpeedBits':0,
                                    'Name':'Group 222',
                                    'RemoteServerUrl':None,
                                    'SharedSecret': None,
                                    'TimeZone':'America/Los_Angeles',
                                    'OverrideVlan': True,
                                    'OverrideConnectionSpeed': True,
                                    'highCapacityProvisioned': False,
                                    'ClientInfoArn': "{\"test\": 3}",
                                    }}
        # We will try to pass a name anyway
        input = {"Group": {'ID':'222',
                           'Domain':'client.com',
                           'RemoteServerUrl': None,
                           'SharedSecret': None,
                           'OverrideVlan': True,
                            'OverrideConnectionSpeed': True,
                            'ClientInfoArn': "{\"test\": 3}"
                            },
                 "Session" : {"ID" : str("unit_test_session")}
                 }

        result = self.myAdminI.group_save(input)
        self.assertEqual(result, expect)


        result_db = self.myAdminI.group_load(input)
        self.assertEqual(result_db, expect)

    def test_group_save_filtered(self):
        Erad_Session.save(
                Erad_Session(
                    ID=str("unit_test_session"),
                    Group_ID_List=["222"],
                    Account_ID="elevenos",
                    AccountOwner=1,
                    Identifier="jimmy.mcgill",
                    LastUsed=util.utcnow()
                    )
                )
        expect = {"Group": {
                    'Account_ID':'elevenos',
                    'ID':'222',
                    'Domain':'client.com',
                    'MaxDownloadSpeedBits':0,
                    'MaxUploadSpeedBits':0,
                    'Name':'Group 222',
                    'RemoteServerUrl':None,
                    'SharedSecret':'321cba',
                    'TimeZone':'America/Los_Angeles',
                    'OverrideVlan': False,
                    'OverrideConnectionSpeed': False,
                    'ClientInfoArn': None,
                    }
                }

        # We will try to pass a name anyway

        input = {"Group": {'ID':'222',
                           'RemoteServer':'ElevenOS',
                           'Domain':'client.com',
                           'RemoteServerUrl':'new_1.2.3.4:50',
                           'SharedSecret':'321cba'},
                 "Session" : {"ID" : str("unit_test_session")}
                 }
        result = self.myAdminI.group_save(input)

        self.assertNotEqual(result['Group']['RemoteServerUrl'], None)
        self.assertNotEqual(result['Group']['SharedSecret'], expect['Group']['SharedSecret'])


        input = {"Group": {'ID':'222',
                           'RemoteServer':'Marriott',
                           'Domain':'client.com',
                           'RemoteServerUrl':'new_1.2.3.4:50',
                           'SharedSecret':'321cba'},
                 "Session" : {"ID" : str("unit_test_session")}
                 }
        result = self.myAdminI.group_save(input)

        self.assertNotEqual(result['Group']['RemoteServerUrl'], None)
        self.assertNotEqual(result['Group']['SharedSecret'], expect['Group']['SharedSecret'])


    def test_group_delete(self):
        Erad_Group2.save( Erad_Group2(ID="65654", Name="Group 65654", Account_ID="elevenos", RemoteServerUrl="1.2.3.4:50",
                                         SharedSecret="321cba", TimeZone="Asia/Qyzylorda", MaxDownloadSpeedBits=0, MaxUploadSpeedBits=0))
        Erad_Session.save( Erad_Session(ID=str("unit_test_session_delete"), Group_ID_List=["Global", "65654"], Account_ID="elevenos", Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )
        expect = None
        # We will try to pass a name anyway
        input = {"Group": {'ID':'65654'},
                 "Session" : {"ID" : str("unit_test_session_delete")}
                 }
        result = self.myAdminI.group_delete(input)
        self.assertEqual(result, expect)

        # Is it actually NOT in our DB?
        self.assertRaisesRegex(EradAccessException, "Access Denied", self.myAdminI.group_delete,input)

        # Wrong Group_ID Param
        input = {"Group": {'ID':'asdasdasdasdad'},
         "Session" : {"ID" : str("unit_test_session_delete")}
         }
        self.assertRaisesRegex(EradAccessException, "Access Denied", self.myAdminI.group_delete,input)

        # Wrong Session Param
        input = {"Group": {'ID':'65654'},
         "Session" : {"ID" : str("WRONG_SESSION")}
         }
        self.assertRaises(EradSessionExpiredException, self.myAdminI.group_delete,input)

    def test_delete_group_with_ports_session(self):
        # setup test data
        Erad_Group2(
            Account_ID='session-account-for-test',
            ID='test-group-with-ports',
            Name='test-group-with-ports',
            RemoteServerUrl='127.0.0.1',
            SharedSecret='whatever',
            TimeZone='Asia/Qyzylorda',
            MaxDownloadSpeedBits=0,
            MaxUploadSpeedBits=0).save()

        Erad_Endpoint(
            IpAddress='99.99.99.99',
            Port='9999',
            Group_ID='session-account-for-test::test-group-with-ports',
            ClusterId='test-cluster-01',
            Secret='whatever',
            Region='us-west-2').save()

        Erad_Authenticator2(
            ID='test-auth-with-ports',
            Account_ID='session-account-for-test',
            Group_ID='test-group-with-ports',
            RadiusAttribute='called-station-id').save()

        Erad_Session(
            ID=str("unit_test_session_delete"),
            Group_ID_List=["test-group-with-ports"],
            Account_ID="session-account-for-test",
            Identifier="test-identifier",
            LastUsed=util.utcnow()).save()

        query_value = 'session-account-for-test::test-group-with-ports'

        provisioned_ports = len([x for x in Erad_Endpoint.group_id_index.query(query_value)])
        self.assertEqual(provisioned_ports, 1)

        authenticators = [authenticator for authenticator in Erad_Authenticator2.account_group_index.query(
            'session-account-for-test',
            AccountGroupIDIndex2.Group_ID=='test-group-with-ports')
        ]
        self.assertEqual(len(authenticators), 1)


        input = {
            "Session": {"ID": "unit_test_session_delete"},
            "Group": {
                "ID": "test-group-with-ports",
                "RemoteServerUrl": "127.0.0.1",
                "SharedSecret": "whatever"
            }
        }
        result = self.myAdminI.group_delete(input)
        self.assertEqual(result, None)

        provisioned_ports = len([x for x in Erad_Endpoint.group_id_index.query(query_value)])
        self.assertEqual(provisioned_ports, 0)

        dirty_endpoint = Erad_Endpoint.get('99.99.99.99', '9999')
        self.assertEqual(dirty_endpoint.Group_ID, '::dirty')

        authenticators = [authenticator for authenticator in Erad_Authenticator2.account_group_index.query(
            'session-account-for-test',
            AccountGroupIDIndex2.Group_ID=='test-group-with-ports')
        ]
        self.assertEqual(len(authenticators), 0)

        groups = len([x for x in Erad_Group2.query('session-account-for-test')])
        self.assertEqual(groups, 0)

        # Autidor class have to save log records with index AccountGroup_ID = account_id + "::" + group_id
        # Checking that these record exists
        expected_messages = {
            "Delete Endpoint - 99.99.99.99:9999" : None,
            "Delete Group 'test-group-with-ports'": None,
            "Delete Authenticator - test-auth-with-ports": None,
        }
        log_records = Erad_Audit.accountgroupid_index.query('session-account-for-test::test-group-with-ports')
        for record in log_records:
            expected_messages[record.Message] = True

        empty_dict = {k: v for k, v in list(expected_messages.items()) if v is None}
        self.assertEqual(len(empty_dict), 0)

        # teardown test data
        dirty_endpoint.delete()
        Erad_Session.get('unit_test_session_delete').delete()

    def test_delete_group_with_ports_api_key(self):
        # setup test data
        Erad_Group2(
            Account_ID='apikey-account-for-test',
            ID='test-group-with-ports',
            Name='test-group-with-ports',
            RemoteServerUrl='127.0.0.1',
            SharedSecret='whatever',
            TimeZone='Asia/Qyzylorda',
            MaxDownloadSpeedBits=0,
            MaxUploadSpeedBits=0).save()

        Erad_Endpoint(
            IpAddress='99.99.99.99',
            Port='9999',
            Group_ID='apikey-account-for-test::test-group-with-ports',
            ClusterId='test-cluster-01',
            Secret='whatever',
            Region='us-west-2').save()

        Erad_Authenticator2(
            ID='test-auth-with-ports',
            Account_ID='apikey-account-for-test',
            Group_ID='test-group-with-ports',
            RadiusAttribute='called-station-id').save()

        Erad_ApiKey(
            ApiKey_ID='653b428caa11fcd9ed361cc5e800803382',
            Account_ID='apikey-account-for-test',
            Active=1,
            ApiKeyName='apikey-for-test'
            ).save()

        query_value = 'apikey-account-for-test::test-group-with-ports'

        provisioned_ports = len([x for x in Erad_Endpoint.group_id_index.query(query_value)])
        self.assertEqual(provisioned_ports, 1)

        authenticators = [authenticator for authenticator in Erad_Authenticator2.account_group_index.query(
            'apikey-account-for-test',
            AccountGroupIDIndex2.Group_ID=='test-group-with-ports')
        ]
        self.assertEqual(len(authenticators), 1)


        input = {
            "IntegrationKey": "653b428caa11fcd9ed361cc5e800803382",
            "Group": {
                "ID": "test-group-with-ports",
                "RemoteServerUrl": "127.0.0.1",
                "SharedSecret": "whatever"
            }
        }
        result = self.myAdminI.group_delete(input)
        self.assertEqual(result, None)

        provisioned_ports = len([x for x in Erad_Endpoint.group_id_index.query(query_value)])
        self.assertEqual(provisioned_ports, 0)

        dirty_endpoint = Erad_Endpoint.get('99.99.99.99', '9999')
        self.assertEqual(dirty_endpoint.Group_ID, '::dirty')

        authenticators = [authenticator for authenticator in Erad_Authenticator2.account_group_index.query(
            'apikey-account-for-test',
            AccountGroupIDIndex2.Group_ID=='test-group-with-ports')
        ]
        self.assertEqual(len(authenticators), 0)

        groups = len([x for x in Erad_Group2.query('apikey-account-for-test')])
        self.assertEqual(groups, 0)

        # Autidor class have to save log records with index AccountGroup_ID = account_id + "::" + group_id
        # Checking that these record exists
        expected_messages = {
            "Delete Endpoint - 99.99.99.99:9999" : None,
            "Delete Group 'test-group-with-ports'": None,
            "Delete Authenticator - test-auth-with-ports": None,
        }
        log_records = Erad_Audit.accountgroupid_index.query('apikey-account-for-test::test-group-with-ports')
        for record in log_records:
            expected_messages[record.Message] = True

            if record.Message == "Delete Group 'test-group-with-ports'":
                self.assertEqual(record.Identifier, '65***382')


        empty_dict = {k: v for k, v in list(expected_messages.items()) if v is None}
        self.assertEqual(len(empty_dict), 0)

        # teardown test data
        dirty_endpoint.delete()
        Erad_ApiKey.get('653b428caa11fcd9ed361cc5e800803382').delete()

    def test_group_with_high_capacity_ports(self):
        # setup test data
        group = Erad_Group2(
            Account_ID='session-account-for-test',
            ID='test-group-with-high-capacity-ports',
            Name='test-group-with-high-capacity-ports',
            RemoteServerUrl='127.0.0.1',
            SharedSecret='whatever',
            TimeZone='Asia/Qyzylorda',
            MaxDownloadSpeedBits=0,
            MaxUploadSpeedBits=0)
        group.save()

        high_capacity_endpoint = Erad_Endpoint(
            IpAddress='99.99.99.99',
            Port='40999',
            Group_ID='session-account-for-test::test-group-with-high-capacity-ports',
            ClusterId='test-cluster-01',
            Secret='whatever',
            Region='us-west-2')
        high_capacity_endpoint.save()

        authenticator = Erad_Authenticator2(
            ID='test-auth-with-ports',
            Account_ID='session-account-for-test',
            Group_ID='test-group-with-ports',
            RadiusAttribute='called-station-id')
        authenticator.save()

        session = Erad_Session(
            ID=str("unit_test_session_delete"),
            Group_ID_List=["test-group-with-high-capacity-ports"],
            Account_ID="session-account-for-test",
            Identifier="test-identifier",
            LastUsed=util.utcnow())
        session.save()

        input = {
            "Session": {"ID": "unit_test_session_delete"},
            "Group": {
                "ID": "test-group-with-high-capacity-ports",
                "RemoteServerUrl": "127.0.0.1",
                "SharedSecret": "whatever"
            }
        }
        result = self.myAdminI.group_list(input)['Group_List']
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0]['highCapacityProvisioned'], True)
        high_capacity_endpoint.delete()

        usual_capacity_endpoint = Erad_Endpoint(
            IpAddress='99.99.99.99',
            Port='9999',
            Group_ID='session-account-for-test::test-group-with-high-capacity-ports',
            ClusterId='test-cluster-01',
            Secret='whatever',
            Region='us-west-2')
        usual_capacity_endpoint.save()

        result = self.myAdminI.group_list(input)['Group_List']
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0]['highCapacityProvisioned'], False)
        usual_capacity_endpoint.delete()

        group.delete()
        authenticator.delete()
        session.delete()


    def test_supplicant_list(self):
        expect = { 'Supplicant_List' : [
                                        {
                                            'Group_ID':'elevenos::1',
                                            'Password':'secr3t',
                                            'UseRemote': True,
                                            'UseWildcard': True,
                                            'Username':'00:11:22:33:^',
                                            'Vlan':666,
                                            'CustomJsonData' : '''{"custom" : "json"}'''
                                        },
                                        {
                                            'Group_ID':'elevenos::1',
                                            'Password':'secr3t',
                                            'UseRemote': True,
                                            'UseWildcard': True,
                                            'Username':'00:11:22:^',
                                            'Vlan':666
                                        },
                                        {
                                            'Group_ID':'elevenos::1',
                                             'Password':'passw0rd',
                                             'UseRemote':False,
                                             'UseWildcard':False,
                                             'Username':'user_1',
                                             'Vlan':888
                                        },
                                        {
                                            'Group_ID':'elevenos::1',
                                            'Password':'secr3t',
                                            'UseRemote':True,
                                            'UseWildcard':False,
                                            'Username':'user_2',
                                            'Vlan':777
                                        }

                                            ] }

        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["1","2"], Account_ID="elevenos", Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )
        Erad_Group2.save( Erad_Group2(Account_ID="elevenos", ID="1", Name="Group1", RemoteServerUrl="1.2.3.4:50",
                                              SharedSecret="abc123", TimeZone="Asia/Qyzylorda", MaxDownloadSpeedBits=0, MaxUploadSpeedBits=0))

        Erad_Supplicant.save(Erad_Supplicant(Group_ID="elevenos::1", Password="passw0rd", UseRemote=False,
                                                        UseWildcard=False, Username="user_1", Vlan=888))
        Erad_Supplicant.save(Erad_Supplicant(Group_ID="elevenos::1", Password="secr3t", UseRemote=True,
                                                        UseWildcard=False, Username="user_2", Vlan=777))
        Erad_Supplicant.save(Erad_Supplicant(Group_ID="elevenos::1",
                                             Password="secr3t",
                                             UseRemote=True,
                                             UseWildcard=True,
                                             Username="00:11:22:^",
                                             Vlan=666))

        Erad_Supplicant.save(Erad_Supplicant(Group_ID="elevenos::1",
                                             Password="secr3t",
                                             UseRemote=True,
                                             UseWildcard=True,
                                             Username="00:11:22:33:^",
                                             Vlan=666,
                                             CustomJsonData = '''{"custom" : "json"}''' ))

        # Valid session with matching group entries and associated supplicants
        input = self.get_input_session_group("unit_test_session","1")
        result = self.myAdminI.supplicant_list(input)
        self.assert_is_superset( expect, result )

    def test_supplicant_find_by_parent(self):
        Erad_Group2.save(
            Erad_Group2(
                Account_ID="elevenos",
                ID="parent1",
                Name="parentGroup1",
                RemoteServerUrl="1.2.3.4:50",
                SharedSecret="321cba",
                TimeZone="Asia/Qyzylorda",
                MaxDownloadSpeedBits=0,
                MaxUploadSpeedBits=0
            )
        )

        Erad_Supplicant.save(
            Erad_Supplicant(
                Group_ID="elevenos::parent1",
                Password="passw0rd",
                UseRemote=False,
                UseWildcard=False,
                Username="user_1",
                Vlan=888,
                Parent_ID="test-supplicant-parent-id",
                Account_ID="elevenos"
            )
        )

        Erad_Supplicant.save(
            Erad_Supplicant(
                Group_ID="elevenos::parent1",
                Password="passw0rd",
                UseRemote=False,
                UseWildcard=False,
                Username="user_2",
                Vlan=888,
                Parent_ID="test-supplicant-parent-id",
                Account_ID="elevenos"
            )
        )

        # Valid session and group
        Erad_Session.save(
            Erad_Session(
                ID=str("unit_test_session_parent"),
                Group_ID_List=["parent1", "2"],
                Account_ID='elevenos',
                Identifier="jimmy.mcgill",
                LastUsed=util.utcnow()
            )
        )

        expect = {"Supplicants": [
            {
                'Account_ID': 'elevenos',
                'CustomJsonData': None,
                'Description': None,
                'DeviceName': None,
                'ExpirationDate': None,
                'Group_ID': 'elevenos::parent1',
                'Location': None,
                'MaxDownloadSpeedBits': None,
                'MaxUploadSpeedBits': None,
                'Parent_ID': 'test-supplicant-parent-id',
                'Password': 'passw0rd',
                'UseRemote': False,
                'UseWildcard': False,
                'Username': 'user_2',
                'Vlan': 888,
                'device_used': None,
                'profile_used': None
            },
            {
                'Account_ID': 'elevenos',
                'CustomJsonData': None,
                'Description': None,
                'DeviceName': None,
                'ExpirationDate': None,
                'Group_ID': 'elevenos::parent1',
                'Location': None,
                'MaxDownloadSpeedBits': None,
                'MaxUploadSpeedBits': None,
                'Parent_ID': 'test-supplicant-parent-id',
                'Password': 'passw0rd',
                'UseRemote': False,
                'UseWildcard': False,
                'Username': 'user_1',
                'Vlan': 888,
                'device_used': None,
                'profile_used': None
            }
        ]}

        input = {
            "Session" : {"ID" : "unit_test_session_parent"},
            "Supplicant": {'Group_ID':"parent1",
                        'Parent_ID':"test-supplicant-parent-id"}
        }

        result = self.myAdminI.supplicant_find_by_parent(input)
        self.assertEqual(expect, result)

        input = {
            "Session" : {"ID" : "unit_test_session_parent"},
            "Supplicant": {'Group_ID':"parent1",
                        'Parent_ID':"non-existent"}
        }

        result = self.myAdminI.supplicant_find_by_parent(input)
        self.assertEqual({"Supplicants": []}, result)

        input.update({"Session": {"ID": "jhsjdhjslhddjsk"}})
        self.assertRaisesRegex(EradSessionExpiredException, "", self.myAdminI.supplicant_find_by_parent, input)

        sess = Erad_Session.get("unit_test_session_parent")
        sess.delete()

        group = Erad_Group2.get("elevenos", "parent1")
        group.delete()

    @mock.patch('erad.api.interface_admin.get_client_info', return_value={'ClientInfo': {}})
    def test_supplicant_load(self, mock_class):
        expect = {
            "Supplicant": {
                'Group_ID': 'elevenos::1',
                'Password': 'passw0rd',
                'UseRemote': False,
                'UseWildcard': False,
                'Username': 'user_1',
                'Vlan': 888}
        }

        Erad_Group2.save(
            Erad_Group2(
                Account_ID="elevenos",
                ID="1",
                Name="Group1",
                RemoteServerUrl="1.2.3.4:50",
                SharedSecret="321cba",
                TimeZone="Asia/Qyzylorda",
                MaxDownloadSpeedBits=0,
                MaxUploadSpeedBits=0
            )
        )

        Erad_Supplicant.save(
            Erad_Supplicant(
                Group_ID="elevenos::1",
                Password="passw0rd",
                UseRemote=False,
                UseWildcard=False,
                Username="user_1",
                Vlan=888
            )
        )

        # Valid session and group
        Erad_Session.save(
            Erad_Session(
                ID=str("unit_test_session"),
                Group_ID_List=["1", "2"],
                Account_ID='elevenos',
                Identifier="jimmy.mcgill",
                LastUsed=util.utcnow()
            )
        )

        input = self.get_input_session_group_sup("unit_test_session", "1", "user_1")
        result = self.myAdminI.supplicant_load(input)
        self.assert_is_superset(expect, result)

        # Wildcarded MAC.
        expect = {
            "Supplicant": {
                'Group_ID': 'elevenos::1',
                'Password': 'passw0rd',
                'UseRemote': False,
                'UseWildcard': True,
                'Username': 'AA:BB:CC:^',
                'Vlan': 888
            },
        }

        expect2 = {
            "Supplicant": {
                'Group_ID': 'elevenos::1',
                'Password': 'passw0rd',
                'UseRemote': False,
                'UseWildcard': True,
                'Username': 'AA-00-11-^',
                'Vlan': 888},
        }

        Erad_Supplicant.save(
            Erad_Supplicant(
                Group_ID="elevenos::1",
                Password="passw0rd",
                UseRemote=False,
                UseWildcard=True,
                Username="AA:BB:CC:^",
                Vlan=888
            )
        )
        Erad_Supplicant.save(
            Erad_Supplicant(
                Group_ID="elevenos::1",
                Password="passw0rd",
                UseRemote=False,
                UseWildcard=True,
                Username="AA-00-11-^",
                Vlan=888
            )
        )
        input = self.get_input_session_group_sup("unit_test_session", "1", "AA:BB:CC:^")
        result = self.myAdminI.supplicant_load(input)
        self.assert_is_superset(expect, result)

        input = self.get_input_session_group_sup("unit_test_session", "1", "AA-00-11-^")
        result = self.myAdminI.supplicant_load(input)
        self.assert_is_superset(expect2, result)

        expect = {
            "Supplicant": {
                'Group_ID': 'elevenos::1',
                'Password': 'passw0rd',
                'UseRemote': False,
                'UseWildcard': False,
                'Username': 'user_with_custom_data',
                'Vlan': 888,
                'CustomJsonData': 'custom data'
            },
        }

        Erad_Supplicant.save(
            Erad_Supplicant(
                Group_ID="elevenos::1",
                Password="passw0rd",
                UseRemote=False,
                UseWildcard=False,
                Username="user_with_custom_data",
                Vlan=888,
                CustomJsonData="custom data"
            )
        )

        input = self.get_input_session_group_sup("unit_test_session", "1", "user_with_custom_data")
        result = self.myAdminI.supplicant_load(input)
        self.assert_is_superset(expect, result)


    @mock.patch('erad.api.interface_admin.send_log_sns')
    def test_supplicant_mac_save(self, mock_func):
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["222"], Account_ID='elevenos', Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )
        expect = {"Supplicant": {'Group_ID':'elevenos::222',
                                 'Password':'AA:BB:CC:^',
                                 'UseRemote':False,
                                 'Username':'AA:BB:CC:^',
                                 'Vlan':15,
                                 'MaxDownloadSpeedBits': 2,
                                 'MaxUploadSpeedBits': 1,}}


        input = {
                 "Session" : {"ID" : str("unit_test_session")},
                 "Supplicant": {'Group_ID':'222',
                                'UseRemote':False,
                                'UseWildcard':True,
                                'MacAddress':'AA:BB:CC:^',
                                'Vlan':15,
                                'MaxDownloadSpeedBits': 2,
                                'MaxUploadSpeedBits': 1,
                            }
                        }

        result = self.myAdminI.supplicant_mac_save(input)
        self.assert_is_superset(expect, result)

        mock_func.assert_called()

        expect = {"Supplicant": {'Group_ID':'elevenos::222',
                                 'UseRemote':False,
                                 'Username':'AA:BB:CC:DD:EE:FF',
                                 'Password':'AA:BB:CC:DD:EE:FF',
                                 'Vlan':15}}

        # Testing dashed mac
        input = {
                 "Session" : {"ID" : str("unit_test_session")},
                 "Supplicant": {'Group_ID':'222',
                                'UseRemote':False,
                                'MacAddress':'AA-BB-CC-DD:EE:FF',
                                'Vlan':15}
                }

        result = self.myAdminI.supplicant_mac_save(input)
        self.assert_is_superset(expect, result)


        # If wildcarded mac is passed but UseWildcard is not True, it must invalidate MAC
        input = {
                 "Session" : {"ID" : str("unit_test_session")},
                 "Supplicant": {'Group_ID':'222',
                                'UseRemote':False,
                                'MacAddress':'AA:BB:C:^',
                                'Vlan':15}
                }

        self.assertRaisesRegex(EradValidationException, "Invalid MacAddress.", self.myAdminI.supplicant_mac_save, input)


    @mock.patch('erad.api.interface_admin.send_log_sns')
    @mock.patch('erad.api.interface_admin.get_client_info', return_value={ 'ClientInfo': {} })
    def test_supplicant_save(self, mock_func, mockClient):
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["222"], Account_ID='elevenos', Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )
        expect = {"Supplicant": {'Group_ID':'elevenos::222',
                                 'Password':'thispass',
                                 'UseRemote':False,
                                 'Username':'user_9000',
                                 'Vlan':15,
                                }
                 }


        input = {"Group": {'ID':'222'},
                 "Session" : {"ID" : str("unit_test_session")},
                 "Supplicant": {'Group_ID':'222',
                                'Password':'thispass',
                                'UseRemote':False,
                                'UseWildcard':False,
                                'Username':'user_9000',
                                'Vlan':15},
                 "Site_Group_ID": "test-group"
                }

        result = self.myAdminI.supplicant_save(input)
        self.assert_is_superset(expect, result)

        # Is it actually in our DB?
        result_db = self.myAdminI.supplicant_load(input)
        self.assert_is_superset(expect, result_db)


        # Save OverrideConnectionSpeed settings
        expect = {"Supplicant": {'Group_ID':'elevenos::222',
                                 'Password':'thispass',
                                 'UseRemote':False,
                                 'Username':'user_9000',
                                 'Vlan':15,
                                 'MaxDownloadSpeedBits': 2,
                                 'MaxUploadSpeedBits': 1,
                                }
                 }


        input = {"Group": {'ID':'222'},
                 "Session" : {"ID" : str("unit_test_session")},
                 "Supplicant": {'Group_ID':'222',
                                'Password':'thispass',
                                'UseRemote':False,
                                'UseWildcard':False,
                                'Username':'user_9000',
                                'Vlan':15,
                                'MaxDownloadSpeedBits': 2,
                                'MaxUploadSpeedBits': 1,},
                 "Site_Group_ID": "test-group"
                }

        result = self.myAdminI.supplicant_save(input)
        self.assert_is_superset( expect, result )

        # Is it actually in our DB?
        result_db = self.myAdminI.supplicant_load(input)
        self.assert_is_superset( expect, result_db )


        # Missing UseRemote Param
        input = {"Group": {'ID':'222'},
                 "Session" : {"ID" : str("unit_test_session")},
                 "Supplicant": {'Group_ID':'222',
                                    'Password':'thispass',
                                    'UseWildcard':False,
                                    'Username':'user_9000',
                                    'Vlan':15}
                 }

        self.assertRaisesRegex(
            EradMalformedException,
            "Required parameter .* is missing.",
            self.myAdminI.supplicant_save,
            input
        )

        expect = {"Supplicant": {'Group_ID':'elevenos::222',
                                 'Password':'thispass',
                                 'UseRemote':False,
                                 'Username':'user_with_custom_data',
                                 'Vlan':15
                                }
                 }


        input = {"Group": {'ID':'222'},
                 "Session" : {"ID" : str("unit_test_session")},
                 "Supplicant": {'Group_ID':'222',
                                'Password':'thispass',
                                'UseRemote':False,
                                'UseWildcard':False,
                                'Username':'user_with_custom_data',
                                'Vlan':15,
                                'CustomJsonData': 'custom_data'},
                 "Site_Group_ID": "test-group"
                }
        self.myAdminI.supplicant_save(input)

        input = self.get_input_session_group_sup("unit_test_session", "222", "user_with_custom_data")
        result = self.myAdminI.supplicant_load(input)
        self.assert_is_superset(expect, result)

        # Save Account_ID
        expect = {
            "Supplicant": {
                'Group_ID': 'elevenos::222',
                'Password': 'thispass',
                'Account_ID': 'elevenos',
                'UseRemote': False,
                'Username': 'user_9000',
                'Vlan': 15,
                'MaxDownloadSpeedBits': 2,
                'MaxUploadSpeedBits': 1,
            }
        }

        input = {"Group": {'ID': '222'},
                 "Session": {"ID": str("unit_test_session")},
                 "Supplicant": {'Group_ID': '222',
                                'Password': 'thispass',
                                'UseRemote': False,
                                'UseWildcard': False,
                                'Username': 'user_9000',
                                'Vlan': 15,
                                'MaxDownloadSpeedBits': 2,
                                'MaxUploadSpeedBits': 1, },
                 "Site_Group_ID": "test-group"
                 }

        result = self.myAdminI.supplicant_save(input)
        self.assert_is_superset(expect, result)

        # Is it actually in our DB?
        result_db = self.myAdminI.supplicant_load(input)
        self.assert_is_superset(expect, result_db)

        # Save CustomJsonData
        expect = {
            "Supplicant": {
                'Group_ID': 'elevenos::222',
                'Password': 'thispass',
                'Account_ID': 'elevenos',
                'UseRemote': False,
                'Username': 'user_9000',
                'Vlan': 15,
                'MaxDownloadSpeedBits': 2,
                'MaxUploadSpeedBits': 1,
            }
        }

        input = {"Group": {'ID': '222'},
                 "Session": {"ID": str("unit_test_session")},
                 "Supplicant": {'Group_ID': '222',
                                'Password': 'thispass',
                                'UseRemote': False,
                                'UseWildcard': False,
                                'Username': 'user_9000',
                                'Vlan': 15,
                                'MaxDownloadSpeedBits': 2,
                                'MaxUploadSpeedBits': 1,
                                'DeviceType': "Android" },
                 "Site_Group_ID": "test-group"
                 }

        result = self.myAdminI.supplicant_save(input)
        self.assert_is_superset(expect, result)

        # Is it actually in our DB?
        result_db = self.myAdminI.supplicant_load(input)
        self.assert_is_superset(expect, result_db)

        supp = result_db.get("Supplicant")
        custom_json_data = supp.get("CustomJsonData")
        self.assertIsNotNone(custom_json_data)

        # Test that CustomJsonData passed is included in final output
        expect = {
            "Supplicant": {
                'Group_ID': 'elevenos::222',
                'Password': 'thispass',
                'Account_ID': 'elevenos',
                'UseRemote': False,
                'Username': 'user_9000',
                'Vlan': 15,
                'MaxDownloadSpeedBits': 2,
                'MaxUploadSpeedBits': 1,
            }
        }

        input = {"Group": {'ID': '222'},
                 "Session": {"ID": str("unit_test_session")},
                 "Supplicant": {'Group_ID': '222',
                                'Password': 'thispass',
                                'UseRemote': False,
                                'UseWildcard': False,
                                'Username': 'user_9000',
                                'Vlan': 15,
                                'MaxDownloadSpeedBits': 2,
                                'MaxUploadSpeedBits': 1,
                                'DeviceType': "Android",
                                "CustomJsonData": '{"name": "checkers"}' },
                 "Site_Group_ID": "test-group"
                 }

        result = self.myAdminI.supplicant_save(input)
        self.assert_is_superset(expect, result)

        # Is it actually in our DB?
        result_db = self.myAdminI.supplicant_load(input)
        self.assert_is_superset(expect, result_db)

        supp = result_db.get("Supplicant")
        custom_json_data = json.loads(supp.get("CustomJsonData"))

        self.assertIsNotNone(custom_json_data)
        self.assertEqual(custom_json_data["name"], "checkers")

        Erad_Supplicant.get('elevenos::222', 'user_9000').delete()

    @mock.patch('erad.api.interface_admin.send_log_sns')
    @mock.patch('erad.api.interface_admin.get_client_info', return_value={ 'ClientInfo': {} })
    def test_supplicant_save_profile_used(self, mock_func, mockClient):
        """Test new supplicant's profile_used is true
        if there exists a supplicant with same Parent_ID && profile_used==True"""


        new_session =  Erad_Session(
                ID=str("unit_test_session"),
                Group_ID_List=["222"],
                Account_ID='elevenos',
                Identifier="jimmy.mcgill",
                LastUsed=util.utcnow()
        )
        new_session.save()

        timestamp = datetime.datetime.utcnow().timestamp()

        # Saving supplicant record that indicate profile was used before

        new_supplicant =  Erad_Supplicant(
                Account_ID="elevenos",
                Group_ID=str("elevenos::222"),
                Username='user_8000',
                profile_used=timestamp,
                Parent_ID=str("test-parent-profile")
            )
        new_supplicant.save()

        expect = {"Supplicant": {'Group_ID': 'elevenos::222',
                                 'Password': 'thispass',
                                 'UseRemote': False,
                                 'Username': 'user_9000',
                                 'Vlan': 15,
                                 'Parent_ID': 'test-parent-profile',
                                 'profile_used': timestamp
                                 }
                  }

        input = {
            "Group": {'ID': '222'},
            "Session": {"ID": str("unit_test_session")},
            "Supplicant": {
                'Group_ID': '222',
                'Password': 'thispass',
                'UseRemote': False,
                'UseWildcard': False,
                'Username': 'user_9000',
                'Vlan': 15,
                'Parent_ID': 'test-parent-profile'
            },
            "Site_Group_ID": "test-group"
        }

        # saving new supplicant but because supplicant sharing ParrentId,
        # after save it will have a timestamp in
        # profile_used field
        result = self.myAdminI.supplicant_save(input)
        self.assert_is_superset(expect, result)

        # Is it actually in our DB?
        result_db = self.myAdminI.supplicant_load(input)
        self.assert_is_superset(expect, result_db)

        new_supplicant.delete()
        new_session.delete()

    def test_nofail_generate_message(self):

        site_group_id = "test-group"

        supp_model = Erad_Supplicant(
           "testaccount::testgroup",
            'user_8000',
            Account_ID="testaccount",
            Password="default",
            Parent_ID="me@example.com",
            DeviceName="me@example.com",
            Description=None,
            Location=None,
            CustomJsonData="{ \"DeviceType\": \"unknown\", \"CreatedAt\": \"2022-01-19T10:27:49.273329\"}"
        )

        output = json.loads(self.myAdminI._generate_message(supp_model, site_group_id))[0]
        expected_fields = [
            ("RequestType", "NewProfile"),
            "EventTimestamp_iso",
            ("UserName", "user_8000"),
            ("Account_ID", "testaccount"),
            ("Group_ID", "testaccount::testgroup"),
            ("Site_Group_ID", site_group_id),
            ("Description", None),
           "CustomJsonData",
            "CreatedAt"
        ]
        should_be_missing = [
            'Password'
        ]

        for field in expected_fields:
            if type(field) is tuple:
                self.assertEqual(output[field[0]], field[1])
            else:
                self.assertTrue(field in output)
        for field in should_be_missing:
            self.assertTrue(field not in output)

        # test if no siteGroup is passed
        output = json.loads(self.myAdminI._generate_message(supp_model))[0]
        self.assertEqual(output.get('Site_Group_ID'), None)

        output = json.loads(self.myAdminI._generate_message(supp_model, None))[0]
        self.assertEqual(output.get('Site_Group_ID'), None)

    def test_nofail_generate_certificate_message(self):
        model_cert = None

        site_group_id = "test-group"

        supp_model = Erad_Supplicant(
           "testaccount::testgroup",
            'user_8000',
            Account_ID="testaccount",
            Password="default",
            Parent_ID="me@example.com",
            DeviceName="me@example.com",
            Description=None,
            Location=None,
            CustomJsonData="{ \"DeviceType\": \"unknown\", \"CreatedAt\": \"2022-01-19T10:27:49.273329\"}"
        )

        output = json.loads(self.myAdminI._generate_certificate_message(model_cert, supp_model, site_group_id))[0]
        expected_fields = [
            ("RequestType", "NewProfileCert"),
            "EventTimestamp_iso",
            ("UserName", "user_8000"),
            ("Account_ID", "testaccount"),
            ("Group_ID", "testaccount::testgroup"),
            ("Site_Group_ID", site_group_id),
            ("Parent_ID", "me@example.com"),
            ("Description", None),
           "CustomJsonData",
            "CreatedAt"
        ]
        should_be_missing = [
            'Password',
            'Certificate',
            'Checksum',

        ]

        for field in expected_fields:
            if type(field) is tuple:
                self.assertEqual(output[field[0]], field[1])
            else:
                self.assertTrue(field in output)
        for field in should_be_missing:
            self.assertTrue(field not in output)


    @mock.patch('erad.api.interface_admin.get_client_info', return_value={ 'ClientInfo': {} })
    def test_supplicant_delete(self, mock_class):

        Erad_Group2.save( Erad_Group2(Account_ID="elevenos", ID="88", Name="eightyeight", RemoteServerUrl="1.2.3.4:50",
                                              SharedSecret="321cba", TimeZone="Asia/Qyzylorda", MaxDownloadSpeedBits=0, MaxUploadSpeedBits=0))
        Erad_Supplicant.save( Erad_Supplicant(Group_ID="88", Password="passw0rd", UseRemote=False,
                                                        UseWildcard=False, Username="user_1", Vlan=888))
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["10","88"], Account_ID='elevenos', Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )

        input = self.get_input_session_group_sup("unit_test_session", "88","user_1")

        self.myAdminI.supplicant_delete(input)

        self.assertRaisesRegex(DoesNotExist, "This item does not exist in the table.",
                                self.myAdminI.supplicant_load,input)

    def test_supplicant_log_group_list(self):
        """Checking that pagination working with using Last_Evaluated_Key.
        But actually this code is not exposed as API endpoint, so saving just in case"""
        now = util.utc_to_local(
            datetime.datetime.utcnow() + datetime.timedelta(hours=12),
            'America/Los_Angeles',
        )

        before = util.utc_to_local(
            datetime.datetime.utcnow() - datetime.timedelta(hours=12),
            'America/Los_Angeles',
        )

        after_date_instance = instance_pack(int(now.timestamp()), 0.0)
        before_date_instance = instance_pack(int(before.timestamp()), 0.0)

        radius_log = Erad_Radius_Log()

        Erad_Session.save(
            Erad_Session(
                ID=str("xxx123zzz456yyy789"),
                Group_ID_List=["paging-test"],
                Account_ID='elevenos',
                Identifier="jimmy.mcgill",
            )
        )

        today = datetime.datetime.today()
        timestamp = today.strftime("%b %d %Y %H:%M:%S UTC")

        Erad_Radius_Log.save(
            Erad_Radius_Log(
                Group_ID="paging-test",
                Instance=make_unique_instance(before.timestamp()),
                Group_Name="Paging Test",
                AccountGroup_ID="elevenos::paging-test",
                PacketType="Access-Request",
                Username=f"supplicant_babble",
                CalledStationId="00:11:22:33:44:55",
                EventTimestamp=timestamp,
                NASIpAddress="52.35.24.232",
                Access="Access-Accept",
            )
        )

        for i in range(3):
            Erad_Radius_Log.save(
                Erad_Radius_Log(
                    Group_ID="paging-test",
                    Instance=make_unique_instance(before.timestamp()),
                    Group_Name="Paging Test",
                    AccountGroup_ID="elevenos::paging-test",
                    PacketType="Access-Request",
                    Username=f"supplicant_{i}",
                    CalledStationId="00:11:22:33:44:55",
                    EventTimestamp=timestamp,
                    NASIpAddress="52.35.24.232",
                    Access="Access-Accept",
                )
            )

        args = self.get_input_supplicant_log_list(
            "xxx123zzz456yyy789",
            "paging-test",
            [before_date_instance, after_date_instance],
        )
        args['Limit'] = 1
        args['Radius_Log']['Include_Stray'] = False

        for i in range(3):
            result = self.myAdminI.supplicant_log_group_list(args)

            self.assertEqual(len(result['page']['Radius_Log']), 1)
            self.assertEqual(result['limit'], 1)
            self.assertTrue(result['last_evaluated_key'])

            args['Last_Evaluated_Key'] = result['last_evaluated_key']

        result = self.myAdminI.supplicant_log_group_list(args)

        self.assertEqual(len(result['page']), 0)
        self.assertEqual(result['limit'], 1)
        self.assertFalse(result['last_evaluated_key'])

    def test_supplicant_log_group_list_bubble(self):
        """Checking that pagination working with using Last_Evaluated_Key.
        Plus filtering bubble (system check) requests. """
        now = util.utc_to_local(
            datetime.datetime.utcnow() + datetime.timedelta(hours=12),
            'America/Los_Angeles',
        )

        before = util.utc_to_local(
            datetime.datetime.utcnow() - datetime.timedelta(hours=12),
            'America/Los_Angeles',
        )

        after_date_instance = instance_pack(int(now.timestamp()), 0.0)
        before_date_instance = instance_pack(int(before.timestamp()), 0.0)

        radius_log = Erad_Radius_Log()

        Erad_Session.save(
            Erad_Session(
                ID=str("xxx123zzz456yyy789"),
                Group_ID_List=["paging-test"],
                Account_ID='elevenos',
                Identifier="jimmy.mcgill",
            )
        )

        today = datetime.datetime.today()
        timestamp = today.strftime("%b %d %Y %H:%M:%S UTC")

        Erad_Radius_Log.save(
            Erad_Radius_Log(
                Group_ID="paging-test",
                Instance=make_unique_instance(before.timestamp()),
                Group_Name="Paging Test",
                AccountGroup_ID="elevenos::paging-test",
                PacketType="Access-Request",
                Username=f"supplicant_babble",
                CalledStationId="00:11:22:33:44:55",
                EventTimestamp=timestamp,
                NASIpAddress="52.35.24.232",
                Access="Access-Accept",
            )
        )

        for i in range(3):
            Erad_Radius_Log.save(
                Erad_Radius_Log(
                    Group_ID="paging-test",
                    Instance=make_unique_instance(before.timestamp()),
                    Group_Name="Paging Test",
                    AccountGroup_ID="elevenos::paging-test",
                    PacketType="Access-Request",
                    Username=f"supplicant_{i}",
                    CalledStationId="00:11:22:33:44:55",
                    EventTimestamp=timestamp,
                    NASIpAddress="52.35.24.232",
                    Access="Access-Accept",
                )
            )

        args = self.get_input_supplicant_log_list(
            "xxx123zzz456yyy789",
            "paging-test",
            [before_date_instance, after_date_instance],
        )
        args['Limit'] = 1
        args['Radius_Log']['Include_Stray'] = False
        args['Radius_Log']['Include_Bubble'] = True

        for i in range(3):
            result = self.myAdminI.supplicant_log_group_list(args)

            self.assertEqual(len(result['page']['Radius_Log']), 1)
            self.assertEqual(result['limit'], 1)
            self.assertTrue(result['last_evaluated_key'])

            args['Last_Evaluated_Key'] = result['last_evaluated_key']

        result = self.myAdminI.supplicant_log_group_list(args)

        self.assertEqual(len(result['page']), 1)
        self.assertEqual(result['limit'], 1)
        self.assertTrue(result['last_evaluated_key'])

    def test_supplicant_log_group_list_check_stray(self):
        """Checking that pagination working with using Last_Evaluated_Key.
        Plus filtering stray requests. Requests that were not matched to any authenticator."""
        now = util.utc_to_local(
            datetime.datetime.utcnow() + datetime.timedelta(hours=12),
            'America/Los_Angeles',
        )

        before = util.utc_to_local(
            datetime.datetime.utcnow() - datetime.timedelta(hours=12),
            'America/Los_Angeles',
        )

        after_date_instance = instance_pack(int(now.timestamp()), 0.0)
        before_date_instance = instance_pack(int(before.timestamp()), 0.0)

        radius_log = Erad_Radius_Log()

        Erad_Session.save(
            Erad_Session(
                ID=str("xxx123zzz456yyy789"),
                Group_ID_List=["paging-test",],
                Account_ID='elevenos',
                Identifier="jimmy.mcgill",
            )
        )

        today = datetime.datetime.today()
        timestamp = today.strftime("%b %d %Y %H:%M:%S UTC")

        Erad_Radius_Log.save(
            Erad_Radius_Log(
                Group_ID="paging-test",
                Instance=make_unique_instance(before.timestamp()),
                Group_Name="Paging Test",
                AccountGroup_ID="elevenos::paging-test",
                PacketType="Access-Request",
                Username="supplicant",
                CalledStationId="00:11:22:33:44:55",
                EventTimestamp=timestamp,
                NASIpAddress="52.35.24.232",
                Access="Access-Accept",
            )
        )

        Erad_Radius_Log.save(
            Erad_Radius_Log(
                Group_ID="None",
                Instance=make_unique_instance(before.timestamp()),
                Group_Name="Stray",
                AccountGroup_ID="elevenos::None",
                PacketType="Access-Request",
                Username=f"supplicant",
                CalledStationId="00:11:22:33:44:55",
                EventTimestamp=timestamp,
                NASIpAddress="52.35.24.232",
                Access="Access-Accept",
            )
        )

        args = self.get_input_supplicant_log_list(
            "xxx123zzz456yyy789",
            "paging-test",
            [before_date_instance, after_date_instance],
        )
        args['Radius_Log']['Include_Stray'] = True

        result = self.myAdminI.supplicant_log_group_list(args)

        self.assertEqual(len(result['page']['Radius_Log']), 1)
        self.assertEqual(result['page']['Radius_Log'][0]['Group_ID'], 'None')

    def test_get_export_fields(self):
        fields = {
            "Session_ID": "7018129031-123123123-1231123123123",
            "Group_ID": "test-group",
            "Include_Stray": "true",
            "Include_Bubble": "true",
            "Instance_From": "100",
            "Instance_To": "200"
        }

        expected = {
            "Session": { "ID": "7018129031-123123123-1231123123123" },
            "Radius_Log":
            {
                "Group_ID": "test-group",
                "Include_Stray": True,
                "Include_Bubble": True,
                "Instance_Interval": [100, 200]
            }
        }

        self.assertEqual(self.myAdminI.get_export_fields(fields), expected)

    def call_supplicant_log_list(self, fields):
        """Checks that method that return logs as stream works."""
        initial_query, log_generator = EradInterfaceAdmin().supplicant_log_list_generator(fields)

        for item in self.myAdminI.supplicant_log_list(initial_query, log_generator):
            yield json.loads(item.decode('utf-8'))

    def test_supplicant_log_list(self):

        # convert dates to instances in order to search with them on a interval of 12 hours
        now = util.utc_to_local( datetime.datetime.utcnow() + datetime.timedelta(hours=12), 'America/Los_Angeles' )
        before = util.utc_to_local( datetime.datetime.utcnow() - datetime.timedelta(hours=12), 'America/Los_Angeles' )

        after_date_instance =  Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(now), 0.0 )
        before_date_instance = Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(before), 0.0 )

        # make unique instances for each radius logs
        instance1 = Auditor().make_unique_instance()
        instance2 = Auditor().make_unique_instance()

        today = datetime.datetime.today()
        timestamp = today.strftime("%b %d %Y %H:%M:%S UTC")

        Erad_Group2.save( Erad_Group2(Account_ID="elevenos", ID="blacksmith", Name="Black Smith", RemoteServerUrl="1.2.3.4:50",
                                              SharedSecret="Black Smith", TimeZone="Asia/Qyzylorda", MaxDownloadSpeedBits=0, MaxUploadSpeedBits=0))
        Erad_Group2.save( Erad_Group2(Account_ID="elevenos", ID="henrysmith", Name="Henry Smith", RemoteServerUrl="1.2.3.4:50",
                                              SharedSecret="Henry Smith", TimeZone="Asia/Qyzylorda", MaxDownloadSpeedBits=0, MaxUploadSpeedBits=0))
        Erad_Supplicant.save( Erad_Supplicant(Group_ID="blacksmith", Password="testingpassword", UseRemote=False,
                                                        UseWildcard=True, Username="supplicant_1x", Vlan=1232))
        Erad_Supplicant.save( Erad_Supplicant(Group_ID="henrysmith", Password="testingpassword", UseRemote=False,
                                                        UseWildcard=True, Username="supplicant_2y", Vlan=1232))
        Erad_Authenticator2.save( Erad_Authenticator2(ID="00:11:22:33:44:55", Group_ID="blacksmith", Account_ID='elevenos',RadiusAttribute="called-station-id"))
        Erad_Authenticator2.save( Erad_Authenticator2(ID="00:11:22:33:44:55", Group_ID="henrysmith", Account_ID='elevenos',RadiusAttribute="called-station-id"))
        Erad_Session.save( Erad_Session(ID=str("xxx123zzz456yyy789"), Group_ID_List=["blacksmith","henrysmith"], Account_ID='elevenos', Identifier="jimmy.mcgill") )
        Erad_Radius_Log.save(Erad_Radius_Log(
            Group_ID="blacksmith", EndpointGroupID="XX-ZZZZ", EndpointGroupName="The ABC Hotel",
            Instance=instance1, Group_Name="Black Smith", AccountGroup_ID="elevenos::blacksmith", PacketType="Access-Request", Username="supplicant_1x", CalledStationId="00:11:22:33:44:55", EventTimestamp=timestamp, NASIpAddress="52.35.24.232", Access="Access-Accept"))
        Erad_Radius_Log.save(Erad_Radius_Log(Group_ID="henrysmith", Instance=instance2, Group_Name="Henry Smith", AccountGroup_ID="elevenos::henrysmith", PacketType="Access-Request", Username="supplicant_2y", CalledStationId="00:11:22:33:44:55", EventTimestamp=timestamp, NASIpAddress="52.35.24.232", Access="Access-Accept"))

        # input a single group
        input = self.get_input_supplicant_log_list("xxx123zzz456yyy789", "blacksmith", [before_date_instance, after_date_instance])

        expected = {"Radius_Log": [
                    {
                        'Access':'Access-Accept',
                        'AccountGroup_ID':'elevenos::blacksmith',
                        'CalledStationId':'00:11:22:33:44:55',
                        'CallingStationId': None,
                        'EventTimestamp': timestamp,
                        'Group_ID':'blacksmith',
                        'Group_Name':'Black Smith',
                        'Instance':instance1,
                        'IsProxied': None,
                        'NASIdentifier': None,
                        'NASIpAddress':'52.35.24.232',
                        'PacketType':'Access-Request',
                        'Parent_ID': None,
                        'EndpointIPAddress': None,
                        'EndpointPort': None,
                        'Username':'supplicant_1x',
                        'CustomJsonData': None,
                        'EndpointGroupID': "XX-ZZZZ",
                        'EndpointGroupName': "The ABC Hotel"
                    }
                ]
            }

        result = next(self.call_supplicant_log_list(input))
        self.assertEqual(result, expected["Radius_Log"][0])

        # input ::all:: for Group_ID
        input = self.get_input_supplicant_log_list("xxx123zzz456yyy789", "::all::", [before_date_instance, after_date_instance])

        expected = {"Radius_Log": [
                    {
                        'Access':'Access-Accept',
                        'AccountGroup_ID':'elevenos::blacksmith',
                        'CalledStationId':'00:11:22:33:44:55',
                        'CallingStationId': None,
                        'EventTimestamp': timestamp,
                        'Group_ID':'blacksmith',
                        'Group_Name':'Black Smith',
                        'Instance':instance1,
                        'IsProxied': None,
                        'NASIdentifier': None,
                        'NASIpAddress':'52.35.24.232',
                        'PacketType':'Access-Request',
                        'Parent_ID': None,
                        'EndpointIPAddress': None,
                        'EndpointPort': None,
                        'Username':'supplicant_1x',
                        'CustomJsonData': None,
                        'EndpointGroupID': "XX-ZZZZ",
                        'EndpointGroupName': "The ABC Hotel"
                    },
                    {
                        'Access':'Access-Accept',
                        'AccountGroup_ID':'elevenos::henrysmith',
                        'CalledStationId':'00:11:22:33:44:55',
                        'CallingStationId': None,
                        'EventTimestamp': timestamp,
                        'Group_ID':'henrysmith',
                        'Group_Name':'Henry Smith',
                        'Instance':instance2,
                        'IsProxied': None,
                        'NASIdentifier': None,
                        'NASIpAddress':'52.35.24.232',
                        'PacketType':'Access-Request',
                        'Parent_ID': None,
                        'EndpointIPAddress': None,
                        'EndpointPort': None,
                        'Username':'supplicant_2y',
                        'CustomJsonData': None,
                        'EndpointGroupID': None,
                        'EndpointGroupName': None
                    }

                ]
            }

        result = []
        for data in self.call_supplicant_log_list(input):
            result.append(data)
        # Lists equal without order
        self.assertCountEqual(result, expected["Radius_Log"])

        # Check for Invalid Session
        input = {"Session" : {"ID" : str("somerandomvalue")},
                "Radius_Log": {
                            'Group_ID':str("henrysmith"),
                            'Include_Stray':True,
                            'Instance_Interval': [before_date_instance, after_date_instance]
                            }
                }

        # checking that expired session leads to exception
        callable_ = lambda x: next(self.call_supplicant_log_list(x))

        self.assertRaises(EradSessionExpiredException, callable_, input)

        # Missing Include Stray Param
        input = {"Session" : {"ID" : str("xxx123zzz456yyy789")},
                "Radius_Log": {
                            'Group_ID':str("henrysmith"),
                            'Instance_Interval': [before_date_instance, after_date_instance]
                            }
                }

        self.assertRaisesRegex(
            EradRequiredFieldException,
            "Required parameter .* is missing.",
            callable_,
            input
        )

        # Missing Include Instance_Interval Param
        input = {"Session" : {"ID" : str("xxx123zzz456yyy789")},
                "Radius_Log": {
                            'Group_ID':str("henrysmith"),
                            'Include_Stray':True
                            }
                }

        self.assertRaisesRegex(
            EradRequiredFieldException,
            "Required parameter .* is missing.",
            callable_,
            input
        )

    def test_authenticator_load(self):

        expected = {"Authenticator": {'ID':'00:11:22:33:44:55',
                                      'Group_ID':'2',
                                      'Account_ID':'elevenos',
                                      'RadiusAttribute':'called-station-id'
                                     }
                   }
        Erad_Authenticator2.save(Erad_Authenticator2(ID="00:11:22:33:44:55", Group_ID="2", Account_ID='elevenos'))

        # Valid Authenticator load
        input = self.get_input_key_auth("00:11:22:33:44:55")
        result = self.myAdminI.authenticator_load(input)
        self.assertEqual(result, expected)

        # Valid Authenticator load compressed mac
        input = self.get_input_key_auth("001122334455")
        result = self.myAdminI.authenticator_load(input)
        self.assertEqual(result, expected)

        # Invalid Authenticator load
        input = self.get_input_key_auth("00:11:22:33:44:44")
        self.assertRaisesRegex(DoesNotExist, "This item does not exist in the table.",
                                self.myAdminI.authenticator_load,input)

        # Missing Param
        del(input["Authenticator"]["ID"])
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter .* is missing.",
                self.myAdminI.group_load, input)

        # check if nas-identifier is saved
        expected = {"Authenticator": {'ID':'dotelevensupplicant',
                                      'Group_ID':'2',
                                      'Account_ID':'elevenos',
                                      'RadiusAttribute':'nas-identifier'
                                     }
                   }
        Erad_Authenticator2.save( Erad_Authenticator2(ID="dotelevensupplicant", Group_ID="2", Account_ID='elevenos', RadiusAttribute='nas-identifier'))
        input = self.set_input_key_auth("dotelevensupplicant", "2", "nas-identifier")
        result = self.myAdminI.authenticator_load(input)
        self.assertEqual(result, expected)

    def test_authenticator_save(self):

        expected = {"Authenticator": {'ID':'AA:11:BB:22:CC:33',
                                      'Group_ID':'8',
                                      'Account_ID':'elevenos',
                                      'RadiusAttribute':'called-station-id'
                                     }
                   }

        # expected output for authenticator 2
        expected2 = {"Authenticator": {'ID':'AA:11:BB:22:CC:44',
                                      'Group_ID':'8',
                                      'Account_ID':'elevenos',
                                      'RadiusAttribute':'called-station-id'
                                     }
                   }

        # Valid input
        input = self.set_input_key_auth("AA:11:BB:22:CC:33", "8")
        result = self.myAdminI.authenticator_save(input)
        self.assertEqual(result, expected)

        # is it synced with Authenticator2?
        auth2_item = Erad_Authenticator2.get('AA:11:BB:22:CC:33', 'elevenos')
        self.assertEqual('8', auth2_item.Group_ID)
        self.assertEqual('called-station-id', auth2_item.RadiusAttribute)

        # lowercase Mac
        input = self.set_input_key_auth("aa:11:bb:22:cc:44", "8")
        result = self.myAdminI.authenticator_save(input)
        self.assertEqual(result, expected2)

        expected = {"Authenticator": {'ID':'AA:11:BB:22:CC:55',
                                      'Group_ID':'8',
                                      'Account_ID':'elevenos',
                                      'RadiusAttribute':'radiusTest'
                                     }
                   }

        input = self.set_input_key_auth("AA:11:BB:22:CC:55", "8", "radiusTest")
        result = self.myAdminI.authenticator_save(input)
        # show error on duplicate authenticator
        self.assertEqual(result, expected)

        expected = {"Authenticator": {'ID':'abcdefghijklmnop',
                                      'Group_ID':'8',
                                      'Account_ID':'elevenos',
                                      'RadiusAttribute':'nas-identifier'
                                     }
                   }

        input = self.set_input_key_auth("abcdefghijklmnop", "8", "nas-identifier")
        result = self.myAdminI.authenticator_save(input)
        self.assertEqual(result, expected)

    def test_authenticator_delete(self):

        Erad_Authenticator2.save( Erad_Authenticator2(ID="A1:B2:C3:D4:E5:F6", Group_ID="99", Account_ID='elevenos'))
        Erad_Authenticator2.save( Erad_Authenticator2(ID="dotelevensupplicant99", Group_ID="99", Account_ID='elevenos', RadiusAttribute='nas-identifier'))

        # Valid delete
        input = self.get_input_key_auth("A1:B2:C3:D4:E5:F6")
        self.myAdminI.authenticator_delete(input)

        # Is it actually deleted from DB?
        obj = Erad_Authenticator2(ID = "A1:B2:C3:D4:E5:F6", Account_ID='elevenos')
        self.assertRaisesRegex(DoesNotExist, "This item does not exist in the table.",obj.refresh)


        Erad_Authenticator2.save( Erad_Authenticator2(ID="A1:B2:C3:D4:E5:F6", Group_ID="99", Account_ID='elevenos'))
        Erad_Authenticator2.save( Erad_Authenticator2(ID="dotelevensupplicant99", Group_ID="99", Account_ID='elevenos', RadiusAttribute='nas-identifier'))

        # Valid delete, weird Mac
        input = self.get_input_key_auth("a1b2c3d4e5f6")
        self.myAdminI.authenticator_delete(input)

        # Is it actually deleted from DB?
        obj = Erad_Authenticator2(ID = "A1:B2:C3:D4:E5:F6", Account_ID='elevenos')
        self.assertRaisesRegex(DoesNotExist, "This item does not exist in the table.",obj.refresh)

    def test_endpoint_load(self):
        expected = {"Endpoint": {"IpAddress": "52.26.34.201",
                                "Port": "1812",
                                "Region": "us-west-1",
                                "Secret": "4905DVCA3B"
                                },
                    "Global": True
                    }

        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["Global"], Account_ID='elevenos', Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )

        # Valid Endpoint load
        input = {"Session": {"ID": str("unit_test_session")}}
        result = self.myAdminI.endpoint_load(input)
        self.assertEqual(result, expected)
        # Non existent group_id should do a Global match
        input = {"Session": {"ID": str("unit_test_session")},
                 "Group_ID": "non-existent-group-id"}
        result = self.myAdminI.endpoint_load(input)
        self.assertEqual(result, expected)

        # Invalid Endpoint load
        expected = {}
        input = {"Session": {"ID": str("unit_test_session123123213123123")}}
        self.assertRaises(EradSessionExpiredException, self.myAdminI.endpoint_load,input )
        expected = {"Endpoint": {"IpAddress": "52.26.34.201",
                                "Port": "20008",
                                "Region": "us-west-1",
                                "Secret": "secret20008"
                                },
                    "Global": False
                    }

        Erad_Session.save(Erad_Session(ID=str("unit_test_session1"), Group_ID_List=["FF-1925", "AB-CD-1002"],
                                       Account_ID='xptoaccount', Identifier="testidentifier", LastUsed=util.utcnow()))

        input = {"Session": {"ID": str("unit_test_session1")},
                 "Group_ID": "FF-1925"}
        result = self.myAdminI.endpoint_load(input)
        self.assertEqual(result, expected)

        expected = {"Endpoint": [
                                    {
                                        "IpAddress": "52.26.34.201",
                                        "Port": "20004",
                                        "Region": "us-west-1",
                                        "Secret": "secret20004"
                                    },
                                    {
                                        "IpAddress": "52.26.34.201",
                                        "Port": "20006",
                                        "Region": "us-west-1",
                                        "Secret": "secret20006"
                                    }
                                ],
                    "Global": False
                    }

        input = {"Session": {"ID": str("unit_test_session1")},
                 "Group_ID": "AB-CD-1002"}
        result = self.myAdminI.endpoint_load(input)
        self.assertEqual(result, expected)

    def test_endpoint_save_group_authorization(self):
        """Authorized session can change only authorized groups"""
        authorized_session = Erad_Session(
            ID=str("authorized_session"),
            Group_ID_List=["Group1"],
            Account_ID='unit_test_account',
            Identifier="unit_test_id",
            LastUsed=util.utcnow()
        )
        authorized_session.save()

        authorized_group = Erad_Group2(
            Account_ID="unit_test_account",
            ID="Group1",
            Name="authorizedGroup",
            TimeZone="America/Los_Angeles")
        authorized_group.save()

        not_authorized_group = Erad_Group2(
            Account_ID="another_unit_test_account",
            ID="not_authorized_group",
            Name="authorizedGroup",
            TimeZone="America/Los_Angeles")
        not_authorized_group.save()



        with mock.patch('erad.api.interface_admin.EradInterfaceAdmin._EradInterfaceAdmin__provision_endpoint') as mock_thing:

            # trying to add endpoint to authorized group

            fields = {
                "Session": {"ID": "authorized_session"},
                "Group_ID": "Group1",
                "Region": "us-west-1"
            }
            mock_thing.return_value = {'Endpoint': 'whatever'}
            result = EradInterfaceAdmin().endpoint_save(fields=fields)
            self.assertEqual(result, mock_thing.return_value)

            # trying to add endpoint that is not authorized group
            # should raise exception because we have to be sure that code can change only authorized groups

            fields = {
                "Session": {"ID": "authorized_session"},
                "Group_ID": "not_authorized_group",
                "Region": "us-west-1"
            }
            mock_thing.return_value = {'Endpoint': 'whatever'}
            with self.assertRaises(EradAccessException) as context:
                result = EradInterfaceAdmin().endpoint_save(fields=fields)

        not_authorized_group.delete()
        authorized_group.delete()
        authorized_session.delete()





    def test_endpoint_save(self):
        test_session = Erad_Session(
            ID=str("unit_test_session"),
            Group_ID_List=["Global", "validGroupId1"],
            Account_ID='xptoaccount1',
            Identifier="jimmy.mcgill",
            LastUsed=util.utcnow()
        )
        test_session.save()
        test_group = Erad_Group2(
            Account_ID="xptoaccount1",
            ID="validGroupId1",
            Name="ValidGroupName1",
            TimeZone="America/Los_Angeles")
        test_group.save()

        first_endpoint = Erad_Endpoint(
            '52.26.34.171',
            Port="1812",
            Group_ID="::free",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-dev-01")
        first_endpoint.save()

        group_ID = first_endpoint.Group_ID
        self.assertEqual(group_ID, "::free")

        # Global should not be allowed when saving
        input = {
                    "Session": {"ID": str("unit_test_session")},
                    "Group_ID": "Global"
                }
        self.assertRaisesRegex(EradValidationException, "Global site can't be modified.", self.myAdminI.endpoint_save, input)

        # # Checking casing sensitivity
        # input.update({"Group_ID": "glObal"})
        # self.assertRaisesRegex(EradValidationException, "Global site can't be modified.", self.myAdminI.endpoint_save, input)

        # Valid Endpoint load
        expected = {
            "Endpoint": {
                "IpAddress": "52.26.34.171",
                "Port": "1812",
                "Region": "us-west-dev-01",
                "Secret": "4905DVCA3B"
            }
        }
        input = {
                    "Session": {"ID": str("unit_test_session")},
                    "Group_ID": "validGroupId1",
                    "Region": "us-west-dev-01"
                }
        result = self.myAdminI.endpoint_save(input)
        self.assertEqual(result, expected)

        first_endpoint.refresh()
        group_ID_after = first_endpoint.Group_ID
        self.assertNotEqual(group_ID_after, group_ID)
        self.assertEqual(group_ID_after, "xptoaccount1::validGroupId1")


        # check no free endpoints - exception
        self.assertRaisesRegex(EradValidationException, "No ports left.", self.myAdminI.endpoint_save, input)

        second_endpoint = Erad_Endpoint(
            '52.26.34.171',
            Port="20001",
            Group_ID="::free",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-east-dev-01")
        second_endpoint.save()
        group_ID = second_endpoint.Group_ID
        self.assertEqual(group_ID, "::free")

        expected = {
            "Endpoint": {
                "IpAddress": "52.26.34.171",
                "Port": "20001",
                "Region": "us-east-dev-01",
                "Secret": "4905DVCA3B"
            }
        }

        input = {
            "Session": {"ID": str("unit_test_session")},
            "Group_ID": "validGroupId1",
            "Region": "us-east-dev-01"
        }
        result = self.myAdminI.endpoint_save(input)
        self.assertEqual(result, expected)

        second_endpoint.refresh()
        group_ID_after = second_endpoint.Group_ID
        self.assertNotEqual(group_ID_after, group_ID)

        # Group_ID changed from ::free and is correctly set
        self.assertEqual(group_ID_after, "xptoaccount1::validGroupId1")

        # third endpoint
        third_endpoint = Erad_Endpoint(
            '52.26.34.171',
            Port="20002",
            Group_ID="::free",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-east-dev-01")
        third_endpoint.save()
        group_ID = third_endpoint.Group_ID
        self.assertEqual(group_ID, "::free")

        # attempt to provision 3rd endpoint to group should not be allowed (2 endpoints per group limit)
        self.assertRaisesRegex(EradValidationException, "No ports left.", self.myAdminI.endpoint_save, input)


        input_denied = {
            "Session": {"ID": str("unit_test_session-invalid1")},
            "Group_ID": "validGroupId1"
        }

        # Invalid session
        self.assertRaises(EradSessionExpiredException, self.myAdminI.endpoint_save, input_denied)

    def test_premium_endpoint_save(self):
        session = Erad_Session(
            ID=str("unit_test_session"),
            Group_ID_List=["Global", "premium_group"],
            Account_ID='account_with_premium_ports',
            Identifier="unit_test_user",
            LastUsed=util.utcnow()
        )
        session.save()
        # Trying to provision endpoint with premium ports,
        # should return error because there is no free premium endpoint
        input = {
                    "Session": {"ID": str("unit_test_session")},
                    "port_service_type": "premium",
                    "BillingKey": "b_kWLlOeY1HT86FPwX3mVbPuZup6uA8JTG",
                    "Group_ID": "premium_group",
                    "Region": "us-west-dev-01"
                }
        with self.assertRaisesRegex(EradValidationException, expected_regex='No ports left.'):
            result = self.myAdminI.endpoint_save(input)

        # Creating free premium endpoint
        expected = {"Endpoint": {
                 "IpAddress": "52.26.34.171",
                 "Port": "44444",
                 "Region": "us-west-dev-01",
                 "Secret": "4905DVCA3B"
             }
        }

        group_with_premium = Erad_Group2(
            Account_ID="account_with_premium_ports",
            ID="premium_group", Name="premium_group_name",
            TimeZone="America/Los_Angeles"
        )
        group_with_premium.save()

        first_endpoint = Erad_Endpoint(
            '52.26.34.171',
            Port="44444",
            Group_ID="::premium",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-dev-01"
        )

        first_endpoint.save()
        group_ID = first_endpoint.Group_ID
        self.assertEqual(group_ID, "::premium")

        # Valid Endpoint load
        result = self.myAdminI.endpoint_save(input)
        self.assertEqual(result, expected)

        # failing with Access Denied exception
        input['BillingKey'] = 'wrong_value'
        self.assertRaisesRegex(
            EradAccessException, "Access Denied",
            self.myAdminI.endpoint_save, input
        )
        # failing with Required parameter exception
        del input['BillingKey']
        self.assertRaisesRegex(
            EradRequiredFieldException, "Required parameter",
            self.myAdminI.endpoint_save, input
        )

        group_with_premium.delete()
        session.delete()
        first_endpoint.delete()

    def test_premium_endpoint_save_ignore_counting(self):
        """If user reqested premium endpoing service not limiting user with only 2 active endpoints per group
        """
        session = Erad_Session(
            ID=str("unit_test_session"),
            Group_ID_List=["Global", "premium_group"],
            Account_ID='account_with_premium_ports',
            Identifier="unit_test_user",
            LastUsed=util.utcnow()
        )
        session.save()

        group_with_premium = Erad_Group2(
            Account_ID="account_with_premium_ports",
            ID="premium_group", Name="premium_group_name",
            TimeZone="America/Los_Angeles"
        )
        group_with_premium.save()

        # Trying to provision endpoint with premium ports,
        # should return error because there is no free premium endpoint
        input = {
                    "Session": {"ID": str("unit_test_session")},
                    "port_service_type": "premium",
                    "BillingKey": "b_kWLlOeY1HT86FPwX3mVbPuZup6uA8JTG",
                    "Group_ID": "premium_group",
                    "Region": "us-west-dev-01"
                }
        # Testing for no restriction on ::premium endpoints
        first_endpoint = Erad_Endpoint(
            '52.26.34.171',
            Port="44448",
            Group_ID="::premium",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-dev-01"
        )
        first_endpoint.save()

        expected_first = {"Endpoint": {
                 "IpAddress": "52.26.34.171",
                 "Port": "44448",
                 "Region": "us-west-dev-01",
                 "Secret": "4905DVCA3B"
             }
        }

        # trying to provision endpoint here instead of creating all endpoint first because
        # because ports picked randomly, so doing it here will lead to predicted port number
        result = self.myAdminI.endpoint_save(input)
        self.assertEqual(result, expected_first)

        second_endpoint = Erad_Endpoint(
            '52.26.34.171',
            Port="44450",
            Group_ID="::premium",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-dev-01"
        )
        second_endpoint.save()

        expected_second = {"Endpoint": {
                 "IpAddress": "52.26.34.171",
                 "Port": "44450",
                 "Region": "us-west-dev-01",
                 "Secret": "4905DVCA3B"
             }
        }

        result = self.myAdminI.endpoint_save(input)
        self.assertEqual(result, expected_second)


        third_endpoint = Erad_Endpoint(
            '52.26.34.171',
            Port="44452",
            Group_ID="::premium",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-dev-01"
        )
        third_endpoint.save()

        expected_third = {"Endpoint": {
                 "IpAddress": "52.26.34.171",
                 "Port": "44452",
                 "Region": "us-west-dev-01",
                 "Secret": "4905DVCA3B"
             }
        }


        # default limit is 2 endpoint per group, for premium this limit is ignored
        result = self.myAdminI.endpoint_save(input)
        self.assertEqual(result, expected_third)

        # Check error if no free endpoint available with restriction removed for premium endpoints.
        with self.assertRaisesRegex(EradValidationException, expected_regex='No ports left.'):
            result = self.myAdminI.endpoint_save(input)

        group_with_premium.delete()
        session.delete()
        first_endpoint.delete()
        second_endpoint.delete()
        third_endpoint.delete()

    def test_endpoint_delete(self):
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["Global"], Account_ID='xptoaccount1', Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )
        Erad_Group2(Account_ID="xptoaccount1", ID="FF-1925", Name="FF-1925", TimeZone="America/Los_Angeles").save()
        first_endpoint = Erad_Endpoint('52.26.34.171', Port="22222", Group_ID="xptoaccount1::FF-1925", ClusterId="uswest-dev-01", Secret="4905DVCA3B", Region="us-west-1")
        first_endpoint.save()

        input = {
                    "Session": {"ID": str("unit_test_session")},
                    "Group_ID": "invalidGroupId",
                    "Port": "22222"
                }
        # Invalid Group_ID
        self.assertRaisesRegex(EradAccessException, "Access Denied", self.myAdminI.endpoint_delete, input)

        input.update({"Group_ID": "FF-1925"})
        input.update({"Port": "20019"})
        # Invalid Port
        self.assertRaisesRegex(EradAccessException, "Access Denied", self.myAdminI.endpoint_delete, input)
        # Invalid Port and Group_ID
        input.update({"Group_ID": "invalidGroupId"})
        self.assertRaisesRegex(EradAccessException, "Access Denied", self.myAdminI.endpoint_delete, input)
        # Global group should not be modified
        input.update({"Group_ID": "Global"})
        self.assertRaisesRegex(EradValidationException, "Global site can't be modified.", self.myAdminI.endpoint_delete, input)
        input.update({"Group_ID": "GlObal"})
        self.assertRaisesRegex(EradValidationException, "Global site can't be modified.", self.myAdminI.endpoint_delete, input)
        # Valid endpoint
        input.update({"Port": "22222", "Group_ID": "FF-1925"})
        result = self.myAdminI.endpoint_delete(input)

        self.assertEqual(result, None)

        old_secret = first_endpoint.Secret
        first_endpoint.refresh()
        self.assertEqual(first_endpoint.Group_ID, "::dirty")
        self.assertNotEqual(first_endpoint.Secret, old_secret)
        self.assertTrue(len(first_endpoint.Secret) == 10)

    def test_authorize_signature(self):

        expected = {"Endpoint": {"IpAddress": "52.26.34.201",
                                "Port": "1812",
                                "Region": "us-west-1",
                                "Secret": "4905DVCA3B"
                                },
                    "Global": True
                    }

        # preparing valid signature
        key = self.std_integration_key
        timestamp = str(int(time.time()))
        url = "erad/api/endpoint/load"
        apiKeyName = "ElevenWireless"

        payload = {}
        payload['Timestamp'] = timestamp
        payload['ApiKeyName'] = apiKeyName
        payload['Url'] = url

        # sorting the payload
        tmplist = []
        for item in sorted(list(payload.items()), key=lambda item: item[0]):
            if type(item[1]) is dict:
                tmplist.append((item[0], sorted(list(item[1].items()), key=lambda innerItem: innerItem[0])))
            else:
                tmplist.append(item)

        # preparing the signature
        tempString = ""
        for item in tmplist:
            if type(item[1]) is list:
                tempString = tempString+item[0]
                for subitem in item[1]:
                    tempString = tempString+subitem[0]+subitem[1]
            else:
                tempString = tempString+item[0]+item[1]
        hmacstring = hmac.new(key.encode(), base64.b64encode(tempString.encode()), digestmod=hashlib.sha256)
        signature = hmacstring.hexdigest()

        # Valid Endpoint load
        input = {"Signature": str(signature), "Timestamp": str(timestamp), "ApiKeyName": str(apiKeyName)}
        result = self.myAdminI.endpoint_load(input)
        self.assertEqual(result, expected)

        # Invalid signature
        input = {"Signature": str("c54728114a6cd1074aeee8a89458e203530a816d74c2f995d5842b204253938f"), "Timestamp": timestamp, "ApiKeyName": str(apiKeyName)}
        self.assertRaisesRegex(EradAccessException,"Access Denied.",self.myAdminI.endpoint_load,input )

        # Expired request
        expected = {}
        input = {"Signature": str("c54728114a6cd1074aeee8a89458e203530a816d74c2f995d5842b204253938f"), "Timestamp": "1454015468", "ApiKeyName": str(apiKeyName)}
        self.assertRaisesRegex(EradAccessException,"Request expired. Server time: .*",self.myAdminI.endpoint_load,input )

        # Missing ApiKeyName
        expected = {}
        input = {"Signature": str("c54728114a6cd1074aeee8a89458e203530a816d74c2f995d5842b204253938f"), "Timestamp": timestamp}
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'ApiKeyName' is missing.",self.myAdminI.endpoint_load,input )

        # Missing Timestamp
        expected = {}
        input = {"Signature": str("c54728114a6cd1074aeee8a89458e203530a816d74c2f995d5842b204253938f"), "ApiKeyName": str(apiKeyName)}
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'Timestamp' is missing.",self.myAdminI.endpoint_load,input )


    def test_account_saveloadlist(self):

        expected = {
                      "AccountOwner": {
                        "Account_ID": "elevenos",
                        "Email": "unt@elevenos.com",
                      }
                    }

        Erad_Session.save( Erad_Session(ID=str("unit_test_session"),
                                        Group_ID_List=["Global"],
                                        Account_ID='elevenos',
                                        Identifier="jimmy.mcgill",
                                        LastUsed=util.utcnow() ))


        # Save account
        input = {
                    "Session":
                        {"ID": str("unit_test_session")},
                    "AccountOwner":
                        {
                            "Email": str("unt@elevenos.com"),
                            "Password": str("TheEleven")
                        }
                }

        result = self.myAdminI.account_save(input)
        self.assertEqual(result, None)

        # Load account
        input = {
                    "Session":
                        {"ID": str("unit_test_session")},
                    "AccountOwner":
                        {"Email": str("unt@elevenos.com")}
                }
        result = self.myAdminI.account_load(input)
        self.assertEqual(result['AccountOwner']['Account_ID'], expected['AccountOwner']['Account_ID'])
        self.assertEqual(result['AccountOwner']['Email'], expected['AccountOwner']['Email'])
        self.assertEqual('Salt' in result['AccountOwner'], True)

        # List accounts
        expected = {
            "Accounts": [
                'testaccount@elevenos.com',
                "owner@elevenos.com",
                "unt@elevenos.com"
              ]
        }
        input = {"Session": {"ID": str("unit_test_session")}}
        result = self.myAdminI.account_list(input)
        self.assertEqual(result, expected)

        # Invalid session
        expected = {}
        input = {"Session": {"ID": str("unit_test_session123123213123123")}}
        self.assertRaises(EradSessionExpiredException, self.myAdminI.account_save,input )

        # Invalid account
        expected = {}
        input = {"Session": {"ID": str("unit_test_session")}, "AccountOwner": {"Email": str("WRONG@elevenos.com")}}
        self.assertRaisesRegex(EradValidationException,"Account doesn't exist.",self.myAdminI.account_load,input )


    def test_account_delete(self):

        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["Global"], Account_ID='elevenos', Identifier="jimmy.mcgill", LastUsed=util.utcnow()))

        # Save account
        input = {"Session": {"ID": str("unit_test_session")}, "AccountOwner": {"Email": str("unt@elevenos.com"), "Password": str("TheEleven")}}
        result = self.myAdminI.account_save(input)
        self.assertEqual(result, None)

        # Invalid session
        input = {"Session": {"ID": str("unit_test_session123123213123123")}}
        self.assertRaises(EradSessionExpiredException,self.myAdminI.account_delete,input )

        # Delete account
        input = {"Session": {"ID": str("unit_test_session")}, "AccountOwner": {"Email": str("unt@elevenos.com")}}
        result = self.myAdminI.account_delete(input)
        self.assertEqual(result, None)

        # Invalid account
        expected = {}
        input = {"Session": {"ID": str("unit_test_session")}, "AccountOwner": {"Email": str("unt@elevenos.com")}}
        self.assertRaisesRegex(EradValidationException,"Account doesn't exist.",self.myAdminI.account_delete,input )


    def test_apikey_display_name(self):
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["Global"], Account_ID='elevenos', Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )

        # Save account without specifying display name
        input = {"Session": {"ID": str("unit_test_session"), "Identifier":"jimmy.mcgill"}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["DisplayName"] == "KeyName" )
        self.assertTrue(result["ApiKey"]["Active"] == 1)

        # Save account specifying display name
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"DisplayName": str("TestDisplayName"), "Active": 1}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["DisplayName"] == str("TestDisplayName") )
        self.assertTrue(result["ApiKey"]["Active"] == 1)

        result_api_key_name = result["ApiKey"]["ApiKeyName"]

        # Update without specifying display name
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str(result_api_key_name), "Active": 0}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["DisplayName"] == str("TestDisplayName") )
        self.assertTrue(result["ApiKey"]["Active"] == 0)

        # Update specifying display name
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str(result_api_key_name), "DisplayName": str("ModifiedDisplayName"),  "Active": 1}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["DisplayName"] == str("ModifiedDisplayName") )
        self.assertTrue(result["ApiKey"]["Active"] == 1)


    def test_apikey_save_without_value_for_active(self):
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["Global"], Account_ID='elevenos', Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )

        # Save account without specifying value for Active
        input = {"Session": {"ID": str("unit_test_session")}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["ApiKeyName"] is not None)
        self.assertTrue(result["ApiKey"]["Active"] == 1)

        # Save account specifying value for Active = 0
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"Active": 0}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["ApiKeyName"] is not None)
        self.assertTrue(result["ApiKey"]["Active"] == 0)

        result_api_key_name = result["ApiKey"]["ApiKeyName"]

        # Update apikey and set Active = 1
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str(result_api_key_name), "Active": 1}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["ApiKeyName"] == result_api_key_name)
        self.assertTrue(result["ApiKey"]["Active"] == 1)

        # Update apikey without passing Active value
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str(result_api_key_name)}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["ApiKeyName"] == result_api_key_name)
        self.assertTrue(result["ApiKey"]["Active"] == 1)

        # Update apikey and set Active = 0
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str(result_api_key_name), "Active": 0}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["ApiKeyName"] == result_api_key_name)
        self.assertTrue(result["ApiKey"]["Active"] == 0)

        # Update apikey without passing Active value
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str(result_api_key_name)}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["ApiKeyName"] == result_api_key_name)
        self.assertTrue(result["ApiKey"]["Active"] == 0)


    def test_apikey_saveloadupdatelist(self):

        expected = {
                      "ApiKey": {
                        "ApiKey_ID": ".*",
                        "ApiKeyName": ".*",
                        "Active": 1
                      }
                    }

        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["Global"], Account_ID='elevenos', Identifier="jimmy.mcgill", LastUsed=util.utcnow()) )

        # Save account
        input = {"Session": {"ID": str("unit_test_session"), "Identifier": "jimmy.mcgill"}, "ApiKey": {"Active": 1}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["ApiKeyName"] is not None)
        result_api_key_name = result["ApiKey"]["ApiKeyName"]

        # Load account
        expected["ApiKey"]["ApiKeyName"] = result_api_key_name
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str(result_api_key_name)}}
        result = self.myAdminI.apikey_load(input)
        self.assertTrue(result["ApiKey"]["ApiKeyName"] == expected["ApiKey"]["ApiKeyName"])

        # Update account
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str(result_api_key_name), "Active": 0}}
        result = self.myAdminI.apikey_save(input)
        self.assertTrue(result["ApiKey"]["ApiKeyName"] == result_api_key_name)
        self.assertTrue(result["ApiKey"]["Active"] == 0)


        # List accounts
        input = {"Session": {"ID": str("unit_test_session")}}
        result = self.myAdminI.apikey_list(input)
        self.assertTrue(len(result["ApiKeyNames"]) > 0)

        # Invalid session
        expected = {}
        input = {"Session": {"ID": str("unit_test_session123123213123123")}}
        self.assertRaises(EradSessionExpiredException,self.myAdminI.apikey_save,input )

        # Invalid account
        expected = {}
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str("WRONG@elevenos.com")}}
        self.assertRaisesRegex(EradValidationException,"ApiKey doesn't exist.",self.myAdminI.apikey_load,input )


    def test_apikey_delete(self):
        # TODO: currently this, and other tests related to API Keys create audit logs. These audit logs should contain
        #  masked API keys values. These tests does not check that. Although it is kinda "ok" because was test manually
        #  it would be better has checks for that.

        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["Global"], Account_ID='elevenos', Identifier='jimmy.mcgill', LastUsed=util.utcnow()) )

        # Save account
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str("unt@elevenos.com"), "Active": 1}}
        result = self.myAdminI.apikey_save(input)
        self.assertEqual(result["ApiKey"]["Active"], 1)
        result_api_key_name = result["ApiKey"]["ApiKeyName"]

        # Invalid session
        input = {"Session": {"ID": str("unit_test_session123123213123123")}}
        self.assertRaises(EradSessionExpiredException, self.myAdminI.apikey_delete,input )

        # Delete account
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str(result_api_key_name)}}
        result = self.myAdminI.apikey_delete(input)
        self.assertEqual(result, None)

        # Invalid account
        expected = {}
        input = {"Session": {"ID": str("unit_test_session")}, "ApiKey": {"ApiKeyName": str(result_api_key_name)}}
        self.assertRaisesRegex(EradValidationException,"ApiKey doesn't exist.",self.myAdminI.apikey_delete,input )


    @nottest
    def test_update_radiusconf_timestamp(self):
        # Tested on S3 dev account, skipped to reduce costs
        # if boto not find boto config file `aws_access_key_id` and `aws_secret_access_key`
        # variable will be equal None and then s3 will use aws config file
        # usually located in $HOME/.aws/ directory
        from boto import s3

        erad_cfg = util.erad_cfg()
        the_timestamp = str(int(time.time()))
        self.myAdminI.update_radiusconf_timestamp(the_timestamp)


        s3_connection = s3.connect_to_region(
                erad_cfg.proxy_conf.s3_region,
                aws_access_key_id=boto_config.get_value('Credentials', 'aws_access_key_id'),
                aws_secret_access_key=boto_config.get_value('Credentials', 'aws_secret_access_key'),
                calling_format=s3.connection.OrdinaryCallingFormat())


        bucket = s3_connection.get_bucket(erad_cfg.proxy_conf.bucket, validate=False)
        key = bucket.get_key(erad_cfg.proxy_conf.radiusconf_key)

        content = key.get_contents_as_string()
        self.assertEqual(content, the_timestamp)
        s3_connection.close()

    def test_group_overwrite_attrs(self):
        """Checking, that optional attributes overwrite only if post data contains new values.
        """
        erad_session = Erad_Session(
            ID="overwrite_unit_test_session",
            Group_ID_List=["group_with_overwrite"],
            Account_ID="elevenos",
            LastUsed=util.utcnow()
        )
        erad_session.save()

        group = Erad_Group2()
        group.MaxDownloadSpeedBits = 1
        group.MaxUploadSpeedBits = 2
        group.OverrideVlan = False
        group.OverrideConnectionSpeed = False

        group.Account_ID="elevenos"
        group.ID="group_with_overwrite"
        group.Name="overwriter_group_name"
        group.RemoteServerUrl="Remote.fake.com"
        group.SharedSecret="321cba"
        group.TimeZone="Asia/Qyzylorda"

        group.save()

        input_data = {
            "Group": {
                    'ID':  "group_with_overwrite",
                    'RemoteServerUrl': "Remote.fake.com",
                    'SharedSecret': "321cba",
                    'TimeZone': "Asia/Qyzylorda",
                },
            "Session" : {"ID" : "overwrite_unit_test_session"}
            }


        self.myAdminI.group_save(input_data)

        group.refresh()

        self.assertEqual(group.ID, "group_with_overwrite")
        self.assertEqual(group.Name, "overwriter_group_name")
        self.assertEqual(group.MaxDownloadSpeedBits, 1)
        self.assertEqual(group.MaxUploadSpeedBits, 2)
        self.assertEqual(group.OverrideVlan, False)
        self.assertEqual(group.OverrideConnectionSpeed, False)

        input_data = {
            "Group": {
                    'ID':  "group_with_overwrite",
                    'RemoteServerUrl': "Remote.fake.com",
                    'SharedSecret': "321cba",
                    'TimeZone': "Asia/Qyzylorda",
                    'OverrideConnectionSpeed': 'True',
                    'OverrideVlan': 'True',

                },
            "Session" : {"ID" : "overwrite_unit_test_session"}
            }


        self.myAdminI.group_save(input_data)

        group.refresh()

        self.assertEqual(group.ID, "group_with_overwrite")
        self.assertEqual(group.Name, "overwriter_group_name")
        self.assertEqual(group.MaxDownloadSpeedBits, 1)
        self.assertEqual(group.MaxUploadSpeedBits, 2)
        self.assertEqual(group.OverrideVlan, True)
        self.assertEqual(group.OverrideConnectionSpeed, True)

        input_data = {
            "Group": {
                    'ID':  "group_with_overwrite",
                    'RemoteServerUrl': "Remote.fake.com",
                    'SharedSecret': "321cba",
                    'TimeZone': "Asia/Qyzylorda",
                    'OverrideConnectionSpeed': 'True',
                    'OverrideVlan': 'True',
                    'MaxDownloadSpeedBits': 5,
                    'MaxUploadSpeedBits': 6,

                },
            "Session" : {"ID" : "overwrite_unit_test_session"}
            }


        self.myAdminI.group_save(input_data)

        group.refresh()

        self.assertEqual(group.ID, "group_with_overwrite")
        self.assertEqual(group.Name, "overwriter_group_name")
        self.assertEqual(group.MaxDownloadSpeedBits, 5)
        self.assertEqual(group.MaxUploadSpeedBits, 6)
        self.assertEqual(group.OverrideVlan, True)
        self.assertEqual(group.OverrideConnectionSpeed, True)


        group.delete()
        erad_session.delete()

    def test_include_bubble_logs(self):
        # convert dates to instances in order to search with them on a interval of 12 hours
        now = util.utc_to_local( datetime.datetime.utcnow() + datetime.timedelta(hours=12), 'America/Los_Angeles' )
        before = util.utc_to_local( datetime.datetime.utcnow() - datetime.timedelta(hours=12), 'America/Los_Angeles' )

        after_date_instance =  Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(now), 0.0 )
        before_date_instance = Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(before), 0.0 )

        # make unique instances for each radius logs
        instance1 = Auditor().make_unique_instance()
        instance2 = Auditor().make_unique_instance()
        instance3 = Auditor().make_unique_instance()

        today = datetime.datetime.today()
        timestamp1 = (today - datetime.timedelta(hours=2)).strftime("%b %d %Y %H:%M:%S UTC")
        timestamp2 = (today - datetime.timedelta(hours=1)).strftime("%b %d %Y %H:%M:%S UTC")
        timestamp3 = today.strftime("%b %d %Y %H:%M:%S UTC")

        Erad_Group2.save( Erad_Group2(Account_ID="elevenos", ID="bubbletest", Name="Bubble Test", RemoteServerUrl="1.2.3.4:50",
                                              SharedSecret="Bubble Test", TimeZone="Asia/Qyzylorda", MaxDownloadSpeedBits=0, MaxUploadSpeedBits=0))
        Erad_Supplicant.save( Erad_Supplicant(Group_ID="bubbletest", Password="testingpassword", UseRemote=False,
                                                        UseWildcard=True, Username="supplicant_1x", Vlan=1232))
        Erad_Authenticator2.save( Erad_Authenticator2(ID="00:11:22:33:44:55", Group_ID="bubbletest", Account_ID='elevenos',RadiusAttribute="called-station-id"))
        Erad_Session.save( Erad_Session(ID=str("xxx123zzz456yyy789"), Group_ID_List=["bubbletest"], Account_ID='elevenos', Identifier="jimmy.mcgill") )
        Erad_Radius_Log.save(Erad_Radius_Log(Group_ID="bubbletest", Instance=instance1, Group_Name="Bubble Test", AccountGroup_ID="elevenos::bubbletest", PacketType="Access-Request", Username="supplicant_1x", CalledStationId="00:11:22:33:44:55", EventTimestamp=timestamp1, NASIpAddress="52.35.24.232", Access="Access-Accept"))
        Erad_Radius_Log.save(Erad_Radius_Log(Group_ID="bubbletest", Instance=instance2, Group_Name="Bubble Test", AccountGroup_ID="elevenos::bubbletest", PacketType="Access-Request", Username="babble", CalledStationId="aa:bb:cc:dd:ee:aa", EventTimestamp=timestamp2, NASIpAddress="52.35.24.232", Access="Access-Accept"))
        Erad_Radius_Log.save(Erad_Radius_Log(Group_ID="bubbletest", Instance=instance3, Group_Name="Bubble Test", AccountGroup_ID="elevenos::bubbletest", PacketType="Access-Request", Username="dummy", CalledStationId="ba:bb:1e:ba:bb:1e", EventTimestamp=timestamp3, NASIpAddress="52.35.24.232", Access="Access-Accept"))

        # doesnt include by default
        input = self.get_input_supplicant_log_list("xxx123zzz456yyy789", "bubbletest", [before_date_instance, after_date_instance])
        expected = {"Radius_Log": [
                    {
                        'Access':'Access-Accept',
                        'AccountGroup_ID':'elevenos::bubbletest',
                        'CalledStationId':'00:11:22:33:44:55',
                        'CallingStationId': None,
                        'EventTimestamp': timestamp1,
                        'Group_ID':'bubbletest',
                        'Group_Name':'Bubble Test',
                        'Instance':instance1,
                        'IsProxied': None,
                        'NASIdentifier': None,
                        'NASIpAddress':'52.35.24.232',
                        'PacketType':'Access-Request',
                        'Parent_ID': None,
                        'EndpointIPAddress': None,
                        'EndpointPort': None,
                        'Username':'supplicant_1x',
                        'CustomJsonData': None,
                        'CustomJsonData': None,
                        'EndpointGroupID': None,
                        'EndpointGroupName': None
                    }
                ]
            }
        result = next(self.call_supplicant_log_list(input))
        self.assertEqual(result, expected["Radius_Log"][0])

        # include if requested
        input = self.get_input_supplicant_log_list("xxx123zzz456yyy789", "bubbletest", [before_date_instance, after_date_instance])
        input['Radius_Log']['Include_Bubble'] = True
        expected = {"Radius_Log": [
                    {
                        'Access':'Access-Accept',
                        'AccountGroup_ID':'elevenos::bubbletest',
                        'CalledStationId':'00:11:22:33:44:55',
                        'CallingStationId': None,
                        'EventTimestamp': timestamp1,
                        'Group_ID':'bubbletest',
                        'Group_Name':'Bubble Test',
                        'Instance':instance1,
                        'IsProxied': None,
                        'NASIdentifier': None,
                        'NASIpAddress':'52.35.24.232',
                        'PacketType':'Access-Request',
                        'Parent_ID': None,
                        'EndpointIPAddress': None,
                        'EndpointPort': None,
                        'Username':'supplicant_1x',
                        'CustomJsonData': None,
                        'EndpointGroupID': None,
                        'EndpointGroupName': None
                    },
                    {
                        'Access':'Access-Accept',
                        'AccountGroup_ID':'elevenos::bubbletest',
                        'CalledStationId':'aa:bb:cc:dd:ee:aa',
                        'CallingStationId': None,
                        'EventTimestamp': timestamp2,
                        'Group_ID':'bubbletest',
                        'Group_Name':'Bubble Test',
                        'Instance':instance2,
                        'IsProxied': None,
                        'NASIdentifier': None,
                        'NASIpAddress':'52.35.24.232',
                        'PacketType':'Access-Request',
                        'Parent_ID': None,
                        'EndpointIPAddress': None,
                        'EndpointPort': None,
                        'Username':'babble',
                        'CustomJsonData': None,
                        'EndpointGroupID': None,
                        'EndpointGroupName': None
                    },
                    {
                        'Access':'Access-Accept',
                        'AccountGroup_ID':'elevenos::bubbletest',
                        'CalledStationId':'ba:bb:1e:ba:bb:1e',
                        'CallingStationId': None,
                        'EventTimestamp': timestamp3,
                        'Group_ID':'bubbletest',
                        'Group_Name':'Bubble Test',
                        'Instance':instance3,
                        'IsProxied': None,
                        'NASIdentifier': None,
                        'NASIpAddress':'52.35.24.232',
                        'PacketType':'Access-Request',
                        'Parent_ID': None,
                        'EndpointIPAddress': None,
                        'EndpointPort': None,
                        'Username':'dummy',
                        'CustomJsonData': None,
                        'EndpointGroupID': None,
                        'EndpointGroupName': None
                    },
                ]
            }

        result = []
        for data in self.call_supplicant_log_list(input):
            result.append(data)
        self.assertCountEqual(result, expected["Radius_Log"])

    def test_authorize_session_or_key(self):
        """Testing that returned data is just account id without group id"""

        new_session = Erad_Session(
            ID=str("unit_test_session"),
            Group_ID_List=["1","2"],
            Account_ID="elevenos",
            Identifier="jimmy.mcgill",
            LastUsed=util.utcnow()
        )
        new_session.save()

        result = self.myAdminI.authorize_session_or_key({
            'Session': {'ID': 'unit_test_session'}
        })
        self.assertEqual(result, 'elevenos')


    def test_supplicant_certificate(self):
        """Testing that cert id will have realm if it was in supplicant username"""

        group = Erad_Group2(
            Account_ID="account_for_tests",
            ID="test_group",
            Name="test_group",
            RemoteServerUrl="remote.fake.com",
            SharedSecret="RandomKey",
            TimeZone="Asia/Qyzylorda"
        )
        group.save()

        supplicant = Erad_Supplicant(
            "account_for_tests::test_group",
            Username="testusername@myrealm",
            Password="test_pass",
            Account_ID="account_for_tests",
            Vlan=1,
            UseRemote=False,
            UseWildcard=False,
        )

        supplicant.save()

        test_session = Erad_Session(
            ID='temporary_cert_session',
            Account_ID="account_for_tests",
            Group_ID_List=["test_group"],
            Identifier="test_session_id",
            LastUsed = util.utcnow(),
            LogoutUrl="http://",
        )
        test_session.save()


        cert_interface_input = {
            "Session": {"ID": "temporary_cert_session"},
            "NoPassword": "true",
            "Supplicant":
                {
                    "Group_ID": "test_group",
                    "Username": "testusername@myrealm",
                    "Password": "test_pass",
                },
            "Site_Group_ID": "site-group-id-does-not-matter-for-tests"
        }

        response = self.myAdminI.supplicant_certificate(cert_interface_input)

        cert_record = Erad_SupplicantCertificate.supplicant_certificate_index.query(
            "testusername@myrealm", Erad_SupplicantCertificate.Group_ID == 'account_for_tests::test_group'
        )
        cert_record = cert_record.next()

        self.assertEqual(cert_record.Username, "testusername@myrealm")



if __name__ == '__main__':
    unittest.main()
