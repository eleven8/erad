#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
import base64
from OpenSSL import crypto

from .api_test import *


#---------- Integration Services ----------
class ApiIntegrationTest(ApiTest):

    def test_integration_session_save(self):
        self.json_post_expect_403(
            '/erad/integration/session/save',
            {
                "IntegrationKey": self.std_integration_key + "2", # modify key
                "Session":
                {
                    "Group_ID_List":
                    [
                        "LQ-924-27",
                        "FT-895-43",
                        "CS-542-33",
                        "GK-682-18"
                    ],
                    "ID": "7539e207-b8f2-4c55-9433-df83cb827b01",
                    "Identifier": "jimmy",
                    "LogoutUrl": "logout.fake.com"
                }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/integration/session/save',
            {
                "IntegrationKey": self.std_integration_key,
                "Session":
                {
                    "Group_ID_List":
                    [
                        "LQ-924-27",
                        "FT-895-43",
                        "CS-542-33",
                        "GK-682-18"
                    ],
                    "ID": "7539e207-b8f2-4c55-9433-df83cb827b01",
                    "Identifier": "jimmy",
                    "AccountOwner": 0,
                    "LogoutUrl": "logout.fake.com"
                }
            },
            {
                "Error": None,
                "Session":
                {
                    "AccountOwner": 0,
                    "Account_ID": "elevenos",
                    "Group_ID_List":
                    [
                        "LQ-924-27",
                        "FT-895-43",
                        "CS-542-33",
                        "GK-682-18"
                    ],
                    "ID": "7539e207-b8f2-4c55-9433-df83cb827b01",
                    "Identifier": "jimmy",
                    "LogoutUrl": "logout.fake.com"
                }
            })
        self.json_post_expect_200(
            '/erad/integration/session/save',
            {
                "IntegrationKey": self.std_integration_key,
                "Session":
                {
                    "Group_ID_List":
                    [
                        "LQ-924-27",
                        "FT-895-43",
                        "CS-542-33",
                        "GK-682-18"
                    ],
                    "ID": "7539e207-b8f2-4c55-9433-df83cb827b01",
                    "Identifier": "jimmy",
                    "AccountOwner": 1,
                    "LogoutUrl": "logout.fake.com"
                }
            },
            {
                "Error": None,
                "Session":
                {
                    "AccountOwner": 1,
                    "Account_ID": "elevenos",
                    "Group_ID_List":
                    [
                        "AB-CD-1002",
                        "FF-1925",
                        "Global",
                        "XX-YYYY",
                        "foo",
                        "fooremoteattr",
                        "group-test_admin_supplicant_import_csv_from_excel",
                        "group88888",
                        "scooby"
                    ],
                    "ID": "7539e207-b8f2-4c55-9433-df83cb827b01",
                    "Identifier": "jimmy",
                    "LogoutUrl": "logout.fake.com"
                }
            })

    def test_integration_group_load(self):
        self.json_post_expect_403(
            '/erad/integration/group/load',
            {
                "IntegrationKey": self.std_integration_key + "2", # modify key
                "Group":
                {
                    "ID": self.std_group
                }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/integration/group/load',
            {
                "IntegrationKey": self.std_integration_key,
                "Group":
                {
                    "ID": self.std_group
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "Account_ID": self.std_account_id,
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon",
                    "TimeZone": "Asia/Qyzylorda",
                    "OverrideVlan": False,
                    "OverrideConnectionSpeed": False,
                }
            })

    def test_integration_group_save_timezone_opt(self):
        self.json_post_expect_200(
            '/erad/integration/group/save',
            {
                "IntegrationKey": self.std_integration_key,
                "Group":
                {
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon",
                    "OverrideVlan": False,
                    "OverrideConnectionSpeed": False,

                }
            })

    def test_integration_group_save(self):
        self.json_post_expect_200(
            '/erad/integration/group/save',
            {
                "IntegrationKey": self.std_integration_key,
                "Group":
                {
                    "ID": "Xd::iiD", # Invalid GroupID
                    "Name": "The Invalid Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon",
                    "TimeZone": "Asia/Qyzylorda",
                }
            },
            {
                "Error": "Invalid ID."
            })


        self.json_post_expect_403(
            '/erad/integration/group/save',
            {
                "IntegrationKey": self.std_integration_key + "2", # modify key
                "Group":
                {
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon",
                    "TimeZone": "Asia/Qyzylorda",
                }
            },
            {
                "Error": "Access Denied."
            })

        self.json_post_expect_200(
            '/erad/integration/group/save',
            {
                "IntegrationKey": self.std_integration_key,
                "Group":
                {
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon",
                    "TimeZone": "Asia/Qyzylorda"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon",
                    "TimeZone": "Asia/Qyzylorda",
                    "OverrideVlan": False,
                    "OverrideConnectionSpeed": False,
                }
            })

    def test_integration_group_delete(self):
        # make group2 to delete
        model = Erad_Group2()
        model.Account_ID = "elevenos"
        model.ID = "88888"
        model.Name = "Gonna get deleted"
        model.RemoteServerUrl = "adwad awd awd awd"
        model.SharedSecret = "w9jd9jdwj9dw"
        model.TimeZone = "Asia/Qyzylorda"
        model.save()

        self.json_post_expect_403(
            '/erad/integration/group/delete',
            {
                "IntegrationKey": self.std_integration_key + "2", # modify key
                "Group":
                {
                    "ID": model.ID
                }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/integration/group/delete',
            {
                "IntegrationKey": self.std_integration_key,
                "Group":
                {
                    "ID": model.ID
                }
            },
            {
                "Error": None
            })

    def test_integration_authenticator_load(self):
        self.json_post_expect_403(
            '/erad/integration/authenticator/load',
            {
                "IntegrationKey": self.std_integration_key + "2", # modify key
                "Authenticator":
                {
                    "ID": self.std_authenticator
                }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/integration/authenticator/load',
            {
                "IntegrationKey": self.std_integration_key,
                "Authenticator":
                {
                    "ID": self.std_authenticator
                }
            },
            {
                "Error": None,
                "Authenticator":
                {
                    "ID": self.std_authenticator,
                    "Group_ID": self.std_group
                }
            })

    def test_integration_authenticator_save(self):
        self.json_post_expect_403(
            '/erad/integration/authenticator/save',
            {
                "IntegrationKey": self.std_integration_key + "2", # modify key
                "Authenticator":
                {
                    "ID": self.std_authenticator,
                    "Group_ID": self.std_group,
                    "Account_ID": self.std_account_id
                }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/integration/authenticator/save',
            {
                "IntegrationKey": self.std_integration_key,
                "Authenticator":
                {
                    "ID": self.std_authenticator3,
                    "Group_ID": self.std_group,
                    "Account_ID": self.std_account_id
                }
            },
            {
                "Error": None,
                "Authenticator":
                {
                    "ID": self.std_authenticator3,
                    "Group_ID": self.std_group
                }
            })

    def test_integration_authenticator_save_ssidname(self):
        self.json_post_expect_200(
            '/erad/integration/authenticator/save',
            {
                "IntegrationKey": self.std_integration_key,
                "Authenticator":
                {
                    "ID": "42-C9-A1-41-CC-AC:sullHS20",
                    "Group_ID": self.std_group,
                    "Account_ID": self.std_account_id
                }
            },
            {
                "Error": None,
                "Authenticator":
                {
                    "ID": "42:C9:A1:41:CC:AC",
                    "Group_ID": self.std_group
                }
            })

    def test_integration_authenticator_delete(self):
        # make group to delete
        model = Erad_Authenticator2()
        model.ID = util.sanitize_mac("FF:AA:BB:CC:44:33")
        model.Group_ID = "888888"
        model.Account_ID = 'elevenos'
        model.save()

        self.json_post_expect_403(
            '/erad/integration/authenticator/delete',
            {
                "IntegrationKey": self.std_integration_key + "2", # modify key
                "Authenticator":
                {
                    "ID": model.ID
                }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/integration/authenticator/delete',
            {
                "IntegrationKey": self.std_integration_key,
                "Authenticator":
                {
                    "ID": model.ID
                }
            },
            {
                "Error": None
            })

    def test_integration_supplicant_load(self):
        self.json_post_expect_200(
            '/erad/integration/supplicant/load',
            {
                "IntegrationKey": self.std_integration_key,
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username
                }
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "UseWildcard": True,
                    "Vlan": 1232
                }
            })

    def test_integration_supplicant_save(self):
        self.json_post_expect_200(
            '/erad/integration/supplicant/save',
            {
                "IntegrationKey": self.std_integration_key,
                "Supplicant":
                {
                    "Group_ID": "somegroup",
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+"somegroup",
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,

                    # optional fields
                    "Description": None,
                    "DeviceName": None,
                    "Location": None,
                    "ExpirationDate": None
                }
            })

    def test_integration_supplicant_certificate(self):
        self.json_post_expect_400(
            '/erad/integration/supplicant/certificate',
            {

            },
            {

            })

        self.json_post_expect_403(
            '/erad/integration/supplicant/certificate',
            {
                "IntegrationKey": "wrong-api-key",
                "NoPassword": "false",
                "Supplicant": {
                    "Group_ID": "Global",
                    "Username": "testuser"
                    },
                    "Site_Group_ID": "test-group"
            },
            {
                "Error": "Access Denied."
            })

        self.json_post_expect_403(
            '/erad/integration/supplicant/certificate',
            {
                "IntegrationKey": "disabled_inegration_key",
                "NoPassword": "false",
                "Supplicant": {
                    "Group_ID": "Global",
                    "Username": "testuser"
                    },
                    "Site_Group_ID": "test-group"
            },
            {
                "Error": "Key disabled."
            })


        self.json_post_expect_200(
            '/erad/integration/supplicant/certificate',
            {
                "IntegrationKey": "i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF",
                "NoPassword": "false",
                "Supplicant": {
                    "Group_ID": "Global",
                    "Username": "testuser"
                    },
                    "Site_Group_ID": "test-group"
            },
            {
                "Error": "This item does not exist in the table."
            })

        self.json_post_expect_200(
            '/erad/integration/supplicant/certificate',
            {
                "IntegrationKey": "i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF",
                "NoPassword": "false",
                "Supplicant": {
                    "Group_ID": "Global",
                    "Username": "globaltest"
                    },
                    "Site_Group_ID": "test-group"
            },
            {
                "Error": None
            })

        post_response =self.json_post(
           '/erad/integration/supplicant/certificate',
           {
                "IntegrationKey": "i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF",
                "NoPassword": "false",
                "Supplicant": {
                    "Group_ID": "Global",
                    "Username": "globaltest"
                    },
                    "Site_Group_ID": "test-group"
            }
        )
        self.assertEqual(post_response.status_code, 200)

        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)
        self.assertTrue('Sha256-fingerprint' in response_data)
        self.assertTrue('Certificate' in response_data)

        self.assertTrue(len(response_data['Sha256-fingerprint']) == 95 )
        self.assertTrue(len(response_data['Certificate']) > 10 )



        post_result = self.json_post(
            '/erad/integration/supplicant/certificate',
            {
                "IntegrationKey": "i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF",
                "NoPassword" : "true",
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232
                },
                "Site_Group_ID": "test-group"
            })

        json_data = post_result.json()
        self.assertEqual(json_data['Error'], None)
        self.assertEqual(len(json_data['Certificate']) > 0, True)

        decoded_cert = base64.decodebytes(json_data['Certificate'].encode())

        p12 = crypto.load_pkcs12(decoded_cert, "")
        extracted_cert = p12.get_certificate()

        self.assertEqual(extracted_cert.digest('sha256'), json_data['Sha256-fingerprint'].encode())
