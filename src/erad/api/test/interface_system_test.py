#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
import time
import unittest

from erad.exceptions import *
from ..interface_system import *
from ..interface_admin import *
from .interface_test import *
from erad import util
from unittest import mock
from pynamodb import exceptions as pynamodb_excp

from datetime import datetime
from datetime import timedelta


from nose.tools import set_trace

class MockedInstance(object):
    def __init__(self, properties):
        self.tags = {}
        self.tags['Name'] = properties['name']
        self.id = properties['id']
        self.ip_address = properties['ip']

def mock_get_ec2_instances(region):
    return [
        MockedInstance({'id': 'i-11', 'name': 'test-01_1-auth-i11', 'ip': '11.10.1.1'}),
        MockedInstance({'id': 'i-12', 'name': 'test-01_1-acct-i12', 'ip': '11.10.1.2'}),

        MockedInstance({'id': 'i-21', 'name': 'test-01_2-auth-i21', 'ip': '12.10.1.1'}),
        MockedInstance({'id': 'i-22', 'name': 'test-01_2-acct-i22', 'ip': '12.10.1.2'}),

        MockedInstance({'id': 'i-23', 'name': 'test-02_1-auth-i23', 'ip': '21.10.1.1'}),
        MockedInstance({'id': 'i-24', 'name': 'test-02_1-acct-i24', 'ip': '22.10.1.2'}),
    ]

def mock_return_existed_instances(cluster_id, region):
    cluster = Erad_Cluster.get(cluster_id)
    return cluster.Instances

def mock_get_ec2_instances_empty(region):
    return []

class InterfaceSystemTest(InterfaceTest):

    def setUp(self):
        self.mySystemI = EradInterfaceSystem()
        self.myAdminI = EradInterfaceAdmin()

    def test_authorize_system_key(self):
        # Test valid key
        input = self.get_input_system_key()
        self.mySystemI.authorize_system_key(input)

        # Test no key
        input = {"wrong":"a bad object"}
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'SystemKey' is missing.", self.mySystemI.authorize_system_key,input)

        # Test invalid key
        input = {"SystemKey" : "0987654321poiuytrewq"}
        self.assertRaisesRegex(EradAccessException, "Access Denied.", self.mySystemI.authorize_system_key,input)

    def test_convert_wildcard_to_regex(self):
        # REDTAG: this test was uncommented and now represent full rules that are used in wildcard match
        # Should be reviewd in case some rule is missing

        # Test a basic wildcard
        expected = "user(.+)org"
        input = "user^org"
        result = self.mySystemI.convert_wildcard_to_regex(input)
        self.assertEqual(expected, result)

        expected = "user(.+)org"
        input = "user*org"
        result = self.mySystemI.convert_wildcard_to_regex(input)
        self.assertEqual(expected, result)

        # Test escapes in a complex wildcard
        expected = "HOST/(.+)\\.(.+)/marrcorp\\.marriott\\.com"
        input = "HOST/^.^/marrcorp.marriott.com"
        result = self.mySystemI.convert_wildcard_to_regex(input)
        self.assertEqual(expected, result)

        expected = "HOST/(.+)\\.(.+)/marrcorp\\.marriott\\.com"
        input = "HOST/*.*/marrcorp.marriott.com"
        result = self.mySystemI.convert_wildcard_to_regex(input)
        self.assertEqual(expected, result)

        # Test a real (escaped) carat
        expected = r"carat\\\^name"
        input = r"carat\^name"
        result = self.mySystemI.convert_wildcard_to_regex(input)
        self.assertEqual(expected, result)

        expected = r"carat\\\*name"
        input = r"carat\*name"
        result = self.mySystemI.convert_wildcard_to_regex(input)
        self.assertEqual(expected, result)


    def test_supplicant_wildcard_match(self):
        # Test no wildcard
        supplicant_model = Erad_Supplicant("9", Username="boring_guy", Password="pass123", Vlan=456,
                                                UseRemote=False, UseWildcard=False)
        self.assertTrue(self.mySystemI.supplicant_wildcard_match(supplicant_model, "boring_guy"))
        self.assertFalse(self.mySystemI.supplicant_wildcard_match(supplicant_model, "no_one"))

        # Test wildcard with `^`
        supplicant_model = Erad_Supplicant("18", Username="HOST/^.^/marrcorp.marriott.com", Password="secret847", Vlan=789,
                                                UseRemote=False, UseWildcard=True)
        self.assertTrue(self.mySystemI.supplicant_wildcard_match(supplicant_model, "HOST/local.guy/marrcorp.marriott.com"))
        self.assertFalse(self.mySystemI.supplicant_wildcard_match(supplicant_model, "OST/local.guy/marrcorp.marriott.com"))

        supplicant_model = Erad_Supplicant("18", Username="sr-^", Password="secret847", Vlan=789,
                                                UseRemote=False, UseWildcard=True)
        self.assertTrue(self.mySystemI.supplicant_wildcard_match(supplicant_model, "sr-000-00/monitor"))
        self.assertFalse(self.mySystemI.supplicant_wildcard_match(supplicant_model, "r-000-00/monitor"))

        # Test wildcard with `*`
        supplicant_model = Erad_Supplicant("18", Username="HOST/*.*/marrcorp.marriott.com", Password="secret847", Vlan=789,
                                                UseRemote=False, UseWildcard=True)
        self.assertTrue(self.mySystemI.supplicant_wildcard_match(supplicant_model, "HOST/local.guy/marrcorp.marriott.com"))
        self.assertFalse(self.mySystemI.supplicant_wildcard_match(supplicant_model, "OST/local.guy/marrcorp.marriott.com"))

        # Test escaped wildcard characters `\^`
        supplicant_model = Erad_Supplicant("18", Username=r"HOST/^.^/marrcorp.marrio\^tt.com", Password="secret847", Vlan=789,
                                                UseRemote=False, UseWildcard=True)
        self.assertTrue(self.mySystemI.supplicant_wildcard_match(supplicant_model, r"HOST/local.guy/marrcorp.marrio\^tt.com"))
        self.assertFalse(self.mySystemI.supplicant_wildcard_match(supplicant_model, r"OST/local.guy/marrcorp.marrio\^tt.com"))

        # Test escaped wildcard characters `\*`
        supplicant_model = Erad_Supplicant("18", Username=r"HOST/*.*/marrcorp.marrio\*tt.com", Password="secret847", Vlan=789,
                                                UseRemote=False, UseWildcard=True)
        self.assertTrue(self.mySystemI.supplicant_wildcard_match(supplicant_model, r"HOST/local.guy/marrcorp.marrio\*tt.com"))
        self.assertFalse(self.mySystemI.supplicant_wildcard_match(supplicant_model, r"OST/local.guy/marrcorp.marrio\*tt.com"))


    @mock.patch('erad.api.interface_system.get_client_info', return_value={ 'ClientInfo': {} })
    def test_supplicant_find_expired(self, mock_class):
        supplicant = Erad_Supplicant(
                        "elevenos::foo",
                        Username="expired_supplicant",
                        Password="expired",
                        Vlan=111,
                        UseRemote=False,
                        UseWildcard=False,
                        ExpirationDate = datetime.utcfromtimestamp(0))

        supplicant.save()


        interface_input = {
            "SystemKey":  util.erad_cfg().system_key[0],
            "Authenticator": {
                  "ID": "foo"
            },
            "Group": {
              "ID" : "foo"
            },
            "Supplicant": {
              "Username": "expired_supplicant"
            }
        }

        with self.assertRaisesRegex(EradExpiredException, 'Supplicant is expired.'):
            self.mySystemI.supplicant_find(interface_input)


        supplicant.ExpirationDate = datetime.utcnow() + timedelta(days=1)
        supplicant.save()

        self.mySystemI.supplicant_find(interface_input)


        supplicant.ExpirationDate = datetime.utcfromtimestamp(0)
        supplicant.save()

        with self.assertRaisesRegex(EradExpiredException, 'Supplicant is expired.'):
            self.mySystemI.supplicant_find(interface_input)


        supplicant.delete()

        with self.assertRaisesRegex(pynamodb_excp.DoesNotExist, 'This item does not exist in the table.'):
            supplicant.refresh(consistent_read=True)

    @mock.patch('erad.api.interface_system.get_client_info', return_value={ 'ClientInfo': {} })
    def test_supplicant_find_expired_by_cert_id(self, mock_class):
        supplicant = Erad_Supplicant(
                        "elevenos::foo",
                        Username="expired_supplicant",
                        Password="expired",
                        Vlan=111,
                        UseRemote=False,
                        UseWildcard=False,
                        ExpirationDate = datetime.utcfromtimestamp(0))

        supplicant.save()

        the_session = Erad_Session()
        the_session.ID = 'temporary_cert_session'
        the_session.Account_ID = "elevenos"
        the_session.Group_ID_List = [ "foo" ]
        the_session.Identifier = "childish.gambino"
        the_session.LogoutUrl = "http://1.2.3.4"
        the_session.LastUsed = util.utcnow()
        the_session.save()


        # set_trace()
        cert_interface_input = {
            "Session": { "ID": "temporary_cert_session" },
            "NoPassword" : "true",
            "Supplicant":
                {
                    "Group_ID": "foo",
                    "Username": "expired_supplicant",
                    "Password": "expired",
                    "UseRemote": "false",
                    "Vlan": 111
                },
            "Site_Group_ID": "test-group"
        }

        self.myAdminI.supplicant_certificate(cert_interface_input)


        selected_cert = next(Erad_SupplicantCertificate.supplicant_certificate_index.query(
            'expired_supplicant',
            SupplicantCertificateIndex.Group_ID=='elevenos::foo'
        ))



        interface_input = {
            "SystemKey":  util.erad_cfg().system_key[0],
            "Authenticator": {
                  "ID": "foo"
            },
            "Group": {
              "ID" : "foo"
            },
            "SupplicantCertificate": {"ID" : selected_cert.ID}
        }

        with self.assertRaisesRegex(EradExpiredException, 'Supplicant is expired.'):
            self.mySystemI.supplicant_find_certificate(interface_input)


        supplicant.ExpirationDate = datetime.utcnow() + timedelta(days=1)
        supplicant.save()

        self.mySystemI.supplicant_find_certificate(interface_input)


        supplicant.ExpirationDate = datetime.utcfromtimestamp(0)
        supplicant.save()

        with self.assertRaisesRegex(EradExpiredException, 'Supplicant is expired.'):
            self.mySystemI.supplicant_find_certificate(interface_input)


        supplicant.delete()
        selected_cert.delete()
        the_session.delete()

        with self.assertRaisesRegex(pynamodb_excp.DoesNotExist, 'This item does not exist in the table.'):
            supplicant.refresh(consistent_read=True)

        with self.assertRaisesRegex(pynamodb_excp.DoesNotExist, 'This item does not exist in the table.'):
            selected_cert.refresh(consistent_read=True)

        with self.assertRaisesRegex(pynamodb_excp.DoesNotExist, 'This item does not exist in the table.'):
            the_session.refresh(consistent_read=True)

    @mock.patch('erad.api.interface_system.get_client_info', return_value={ 'ClientInfo': {} })
    def test_supplicant_find_by_cert(self, mock_class):
        """Base test to check that find by cert id working"""

        authenticator = Erad_Authenticator2(
            "test_authenticator_id",
            "account_for_tests",
            Group_ID="test_group"
        )
        authenticator.save()

        endpoint = Erad_Endpoint(
            '172.16.0.1',
            Port="20000",
            Group_ID="account_for_tests::test_group",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-2"
        )
        endpoint.save()

        # Global endpoint is required, because condition/code that check global or not global endpoint requires it
        endpoint_global = Erad_Endpoint(
            '172.16.0.1',
            Port="20001",
            Group_ID="account_for_tests::Global",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-2"
        )
        endpoint_global.save()

        group = Erad_Group2(
            Account_ID="account_for_tests",
            ID="test_group",
            Name="test_group",
            RemoteServerUrl="remote.fake.com",
            SharedSecret="RandomKey",
            TimeZone="Asia/Qyzylorda"
        )
        group.save()

        supplicant = Erad_Supplicant(
            "account_for_tests::test_group",
            Username="testusername",
            Password="test_pass",
            Account_ID="account_for_tests",
            Vlan=1,
            UseRemote=False,
            UseWildcard=False,
        )

        supplicant.save()
        test_session = Erad_Session(
            ID='temporary_cert_session',
            Account_ID="account_for_tests",
            Group_ID_List=["test_group"],
            Identifier="test_session_id",
            LastUsed = util.utcnow(),
            LogoutUrl="http://",
        )
        test_session.save()

        # creating cert addon for supplicant
        cert_interface_input = {
            "Session": {"ID": "temporary_cert_session"},
            "NoPassword": "true",
            "Supplicant":
                {
                    "Group_ID": "test_group",
                    "Username": "testusername",
                    "Password": "test_pass",
                },
            "Site_Group_ID": "site-group-id-does-not-matter-for-tests"
        }

        response = self.myAdminI.supplicant_certificate(cert_interface_input)

        cert_record = Erad_SupplicantCertificate.supplicant_certificate_index.query(
            "testusername", Erad_SupplicantCertificate.Group_ID == 'account_for_tests::test_group'
        )
        cert_id = cert_record.next().ID


        # serarch user by cert id
        interface_input = {
            "SystemKey":  util.erad_cfg().system_key[0],
            "Authenticator": {
                  "ID": "test_authenticator_id"
            },
            "Endpoint": {
                "IpAddress": "172.16.0.1",
                "Port": "20000"
            },
            "Group": {
              "ID" : "test_group"
            },
            "SupplicantCertificate": {"ID" : cert_id}
        }

        result = self.mySystemI.supplicant_find_certificate(interface_input)
        self.assertEqual(result['Supplicant']['Username'], 'testusername')
        self.assertEqual(result['Supplicant']['Group_ID'], 'account_for_tests::test_group')

        # cleanup
        authenticator.delete()
        endpoint.delete()
        endpoint_global.delete()
        group.delete()
        supplicant.delete()
        test_session.delete()
        Erad_SupplicantCertificate.get(cert_id).delete()

    @mock.patch('erad.api.interface_system.get_client_info', return_value={ 'ClientInfo': {} })
    def test_supplicant_find_by_cert_wrong_id(self, mock_class):
        """Base test to check that find by cert id working"""

        authenticator = Erad_Authenticator2(
            "test_authenticator_id",
            "account_for_tests",
            Group_ID="test_group"
        )
        authenticator.save()

        endpoint = Erad_Endpoint(
            '172.16.0.1',
            Port="20000",
            Group_ID="account_for_tests::test_group",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-2"
        )
        endpoint.save()

        # Global endpoint is required, because condition/code that check global or not global endpoint requires it
        endpoint_global = Erad_Endpoint(
            '172.16.0.1',
            Port="20001",
            Group_ID="account_for_tests::Global",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-2"
        )
        endpoint_global.save()

        group = Erad_Group2(
            Account_ID="account_for_tests",
            ID="test_group",
            Name="test_group",
            RemoteServerUrl="remote.fake.com",
            SharedSecret="RandomKey",
            TimeZone="Asia/Qyzylorda"
        )
        group.save()

        supplicant = Erad_Supplicant(
            "account_for_tests::test_group",
            Username="testusername",
            Password="test_pass",
            Account_ID="account_for_tests",
            Vlan=1,
            UseRemote=False,
            UseWildcard=False,
        )

        supplicant.save()
        test_session = Erad_Session(
            ID='temporary_cert_session',
            Account_ID="account_for_tests",
            Group_ID_List=["test_group"],
            Identifier="test_session_id",
            LastUsed = util.utcnow(),
            LogoutUrl="http://",
        )
        test_session.save()

        # creating cert addon for supplicant
        cert_interface_input = {
            "Session": {"ID": "temporary_cert_session"},
            "NoPassword": "true",
            "Supplicant":
                {
                    "Group_ID": "test_group",
                    "Username": "testusername",
                    "Password": "test_pass",
                },
            "Site_Group_ID": "site-group-id-does-not-matter-for-tests"
        }

        response = self.myAdminI.supplicant_certificate(cert_interface_input)

        cert_record = Erad_SupplicantCertificate.supplicant_certificate_index.query(
            "testusername", Erad_SupplicantCertificate.Group_ID == 'account_for_tests::test_group'
        )
        cert_id = cert_record.next().ID


        # serarch user by cert id
        interface_input = {
            "SystemKey":  util.erad_cfg().system_key[0],
            "Authenticator": {
                  "ID": "test_authenticator_id"
            },
            "Endpoint": {
                "IpAddress": "172.16.0.1",
                "Port": "20000"
            },
            "Group": {
              "ID" : "test_group"
            },
            "SupplicantCertificate": {"ID" : cert_id + "1"}
        }

        with self.assertRaises(Exception) as context:
            result = self.mySystemI.supplicant_find_certificate(interface_input)
            self.assertEqual(context, DoesNotExist)

        # cleanup
        authenticator.delete()
        endpoint.delete()
        endpoint_global.delete()
        group.delete()
        supplicant.delete()
        test_session.delete()
        Erad_SupplicantCertificate.get(cert_id).delete()

    def test_supplicant_find_by_cert_with_realm(self):
        """Base test to check that find by cert id working and cert with realm.
        This test does not uses deocrator to be able access mocked function and check passed arguments"""

        authenticator = Erad_Authenticator2(
            "test_authenticator_id",
            "account_for_tests",
            Group_ID="test_group"
        )
        authenticator.save()

        endpoint = Erad_Endpoint(
            '172.16.0.1',
            Port="20000",
            Group_ID="account_for_tests::test_group",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-2"
        )
        endpoint.save()

        # Global endpoint is required, because condition/code that check global or not global endpoint requires it
        endpoint_global = Erad_Endpoint(
            '172.16.0.1',
            Port="20001",
            Group_ID="account_for_tests::Global",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-2"
        )
        endpoint_global.save()

        group = Erad_Group2(
            Account_ID="account_for_tests",
            ID="test_group",
            Name="test_group",
            RemoteServerUrl="remote.fake.com",
            SharedSecret="RandomKey",
            TimeZone="Asia/Qyzylorda"
        )
        group.save()

        supplicant = Erad_Supplicant(
            "account_for_tests::test_group",
            Username="testusername@myrealm",
            Password="test_pass",
            Account_ID="account_for_tests",
            Vlan=1,
            UseRemote=False,
            UseWildcard=False,
        )

        supplicant.save()
        test_session = Erad_Session(
            ID='temporary_cert_session',
            Account_ID="account_for_tests",
            Group_ID_List=["test_group"],
            Identifier="test_session_id",
            LastUsed = util.utcnow(),
            LogoutUrl="http://",
        )
        test_session.save()

        # creating cert addon for supplicant
        cert_interface_input = {
            "Session": {"ID": "temporary_cert_session"},
            "NoPassword": "true",
            "Supplicant":
                {
                    "Group_ID": "test_group",
                    "Username": "testusername@myrealm",
                    "Password": "test_pass",
                },
            "Site_Group_ID": "site-group-id-does-not-matter-for-tests"
        }

        response = self.myAdminI.supplicant_certificate(cert_interface_input)

        cert_record = Erad_SupplicantCertificate.supplicant_certificate_index.query(
            "testusername@myrealm", Erad_SupplicantCertificate.Group_ID == 'account_for_tests::test_group'
        )
        cert_id = cert_record.next().ID


        # serarch user by cert id
        interface_input = {
            "SystemKey":  util.erad_cfg().system_key[0],
            "Authenticator": {
                  "ID": "test_authenticator_id"
            },
            "Endpoint": {
                "IpAddress": "172.16.0.1",
                "Port": "20000"
            },
            "Group": {
              "ID" : "test_group"
            },
            "SupplicantCertificate": {"ID" : cert_id},
            "Realm": "value_does_not_match_username_realm"
        }

        with mock.patch('erad.api.interface_system.get_client_info', return_value={ 'ClientInfo': {} }) as mocked_get_client_info:
            result = self.mySystemI.supplicant_find_certificate(interface_input)
            self.assertEqual(result['Supplicant']['Username'], 'testusername@myrealm')
            self.assertEqual(result['Supplicant']['Group_ID'], 'account_for_tests::test_group')


        # checking that value that freeradius extracted as realm is used in get_client_info
        self.assertEqual(mocked_get_client_info.mock_calls[0][1][4], "value_does_not_match_username_realm")


        # cleanup
        authenticator.delete()
        endpoint.delete()
        endpoint_global.delete()
        group.delete()
        supplicant.delete()
        test_session.delete()
        Erad_SupplicantCertificate.get(cert_id).delete()

    @mock.patch('erad.api.interface_system.get_client_info', return_value={ 'ClientInfo': {} })
    def test_supplicant_find_by_cert_with_realm_but_in_db_user_without_realm(self, mock_class):
        """Base test to check that find by cert id working and cert with realm"""

        authenticator = Erad_Authenticator2(
            "test_authenticator_id",
            "account_for_tests",
            Group_ID="test_group"
        )
        authenticator.save()

        endpoint = Erad_Endpoint(
            '172.16.0.1',
            Port="20000",
            Group_ID="account_for_tests::test_group",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-2"
        )
        endpoint.save()

        # Global endpoint is required, because condition/code that check global or not global endpoint requires it
        endpoint_global = Erad_Endpoint(
            '172.16.0.1',
            Port="20001",
            Group_ID="account_for_tests::Global",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-2"
        )
        endpoint_global.save()

        group = Erad_Group2(
            Account_ID="account_for_tests",
            ID="test_group",
            Name="test_group",
            RemoteServerUrl="remote.fake.com",
            SharedSecret="RandomKey",
            TimeZone="Asia/Qyzylorda"
        )
        group.save()

        supplicant = Erad_Supplicant(
            "account_for_tests::test_group",
            Username="testusername",
            Password="test_pass",
            Account_ID="account_for_tests",
            Vlan=1,
            UseRemote=False,
            UseWildcard=False,
        )

        supplicant.save()
        test_session = Erad_Session(
            ID='temporary_cert_session',
            Account_ID="account_for_tests",
            Group_ID_List=["test_group"],
            Identifier="test_session_id",
            LastUsed = util.utcnow(),
            LogoutUrl="http://",
        )
        test_session.save()

        # creating cert addon for supplicant
        cert_interface_input = {
            "Session": {"ID": "temporary_cert_session"},
            "NoPassword": "true",
            "Supplicant":
                {
                    "Group_ID": "test_group",
                    "Username": "testusername",
                    "Password": "test_pass",
                },
            "Site_Group_ID": "site-group-id-does-not-matter-for-tests"
        }

        response = self.myAdminI.supplicant_certificate(cert_interface_input)

        cert_record = Erad_SupplicantCertificate.supplicant_certificate_index.query(
            "testusername", Erad_SupplicantCertificate.Group_ID == 'account_for_tests::test_group'
        )
        cert_id = cert_record.next().ID


        # serarch user by cert id
        interface_input = {
            "SystemKey":  util.erad_cfg().system_key[0],
            "Authenticator": {
                  "ID": "test_authenticator_id"
            },
            "Endpoint": {
                "IpAddress": "172.16.0.1",
                "Port": "20000"
            },
            "Group": {
              "ID" : "test_group"
            },
            "SupplicantCertificate": {"ID" : cert_id + '@myrealm'} # here we trying to get cert id with realm
        }

        result = self.mySystemI.supplicant_find_certificate(interface_input)
        self.assertEqual(result['Supplicant']['Username'], 'testusername')
        self.assertEqual(result['Supplicant']['Group_ID'], 'account_for_tests::test_group')

        # cleanup
        authenticator.delete()
        endpoint.delete()
        endpoint_global.delete()
        group.delete()
        supplicant.delete()
        test_session.delete()
        Erad_SupplicantCertificate.get(cert_id).delete()


    @mock.patch('erad.api.interface_system.get_client_info', return_value={'ClientInfo': {}})
    def test_supplicant_find(self, mock_class):
        Erad_Supplicant.save(
            Erad_Supplicant(
                "elevenos::11",
                Username="Eleven",
                Password="uknow",
                Vlan=111,
                UseRemote=False,
                UseWildcard=False,
                Parent_ID='test-parent-id',
                Account_ID='elevenos'
            )
        )
        Erad_Authenticator2.save(Erad_Authenticator2("00:AA:11:BB:22:CC", 'elevenos', Group_ID="11"))
        Erad_Group2.save(
            Erad_Group2(
                Account_ID="elevenos",
                ID="11",
                Name="eWirelessGroup",
                RemoteServerUrl="remote.fake.com",
                SharedSecret="RandomKey",
                TimeZone="Asia/Qyzylorda"
            )
        )
        expected = {
            'Supplicant': {
                'UseWildcard': False,
                'Username': 'Eleven',
                'Password': 'uknow',
                'Vlan': 111,
                'UseRemote': False,
                'Group_ID': 'elevenos::11',
                'Parent_ID': 'test-parent-id',
                'Account_ID': 'elevenos',
                'ClientInfo': {}
            },
            'Group': {
                'Account_ID': 'elevenos',
                'RemoteServerUrl': 'remote.fake.com',
                'SharedSecret': 'RandomKey',
                'ID': '11',
                'Name': 'eWirelessGroup',
                'TimeZone': 'Asia/Qyzylorda'
            }
        }

        # Valid user on valid authenticator
        input = self.get_input_system_key_auth_sup("00:AA:11:BB:22:CC", "Eleven")
        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(expected, result)

        self.assertEqual(result['EndpointGroupName'], "Global")
        self.assertEqual(result['EndpointGroupID'], "Global")

        # Bad Authenticator
        input = self.get_input_system_key_auth_sup("00:88:77:66:55:44", "Eleven")
        self.assertRaisesRegex(EradConfigurationException,"Authenticator .* does not exist",
                                self.mySystemI.supplicant_find,input)

        # Bad Group in Auth
        Erad_Authenticator2.save(Erad_Authenticator2("ED:32:FA:4F:CC:67", 'elevenos', Group_ID="123123"))
        input = self.get_input_system_key_auth_sup("ED:32:FA:4F:CC:67", "Eleven")
        self.assertRaisesRegex(EradConfigurationException,"Group .* does not exist",
                                self.mySystemI.supplicant_find,input)

        # No Supplicant in Group
        Erad_Supplicant.save(Erad_Supplicant("13", Username="Thirteen", Password="uknow", Vlan=1313,
                                        UseRemote=False, UseWildcard=False))
        Erad_Authenticator2.save(Erad_Authenticator2("FF:EE:DD:CC:BB:AA", 'elevenos', Group_ID="12"))
        Erad_Group2.save(Erad_Group2(Account_ID="elevenos",ID="12", Name="twelveGroup", RemoteServerUrl="remote.fake.com",
                                              SharedSecret="TwelveRules", TimeZone="Asia/Qyzylorda"))
        input = self.get_input_system_key_auth_sup("FF:EE:DD:CC:BB:AA", "Thirteen")

        self.assertRaisesRegex(
            DoesNotExist,
            "Supplicant .* does not exist for group '12'",
            self.mySystemI.supplicant_find,
            input
        )

        # Testing Global Supplicant
        expected =  {
                        'Supplicant': {
                            'UseWildcard': False,
                            'Username': 'rambo',
                            'Password': 'uknow',
                            'Vlan': 1313,
                            'UseRemote': False,
                            'Group_ID': 'elevenos::Global',
                            'ClientInfo': {}
                        },
                        'Group': {
                            'RemoteServerUrl': 'remote.fake2.com',
                            'SharedSecret': 'RamboRulez!',
                            'ID': 'NotGlobal',
                            'Name': 'NOT Global Group',
                            'TimeZone' : 'Asia/Qyzylorda'
                        },

                    }

        Erad_Supplicant.save(Erad_Supplicant("elevenos::Global", Username="rambo", Password="uknow", Vlan=1313,
                                        UseRemote=False, UseWildcard=False))
        Erad_Authenticator2.save(Erad_Authenticator2("00:55:98:75:BB:AA", 'elevenos', Group_ID="NotGlobal"))
        Erad_Group2.save(Erad_Group2(Account_ID="elevenos", ID="NotGlobal", Name="NOT Global Group", RemoteServerUrl="remote.fake2.com",
                                              SharedSecret="RamboRulez!", TimeZone="Asia/Qyzylorda"))
        Erad_Group2.save(Erad_Group2(Account_ID="elevenos", ID="Global", Name="Global", RemoteServerUrl="remote.fake4.com",
                                              SharedSecret="RamboRulez!", TimeZone="Asia/Qyzylorda"))
        input = self.get_input_system_key_auth_sup("00:55:98:75:BB:AA", "Rambo")
        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(expected, result)

        self.assertEqual(result["EndpointGroupName"], "Global")
        self.assertEqual(result["EndpointGroupID"], "Global")

        # Test wildcarded MAC Username
        Erad_Supplicant.save( Erad_Supplicant("elevenos::11", Username="AA:BB:CC:^", Password="uknow", Vlan=111,
                                                UseRemote=False, UseWildcard=True))
        Erad_Authenticator2.save( Erad_Authenticator2("00:AA:11:BB:22:DD", 'elevenos', Group_ID="11") )
        Erad_Group2.save( Erad_Group2(Account_ID="elevenos",ID="11", Name="eWirelessGroup", RemoteServerUrl="remote.fake.com",
                                              SharedSecret="RandomKey", TimeZone = "Asia/Qyzylorda") )

        expected =  {
            'Supplicant': {
                'UseWildcard': True,
                'Username': 'AA:BB:CC:^',
                'Password': 'AA:BB:CC:AA:BB:CC',
                'Vlan': 111,
                'UseRemote': False,
                'Group_ID': 'elevenos::11',
                'ClientInfo': {}
            },
            'Group': {
                'Account_ID': 'elevenos',
                'RemoteServerUrl': 'remote.fake.com',
                'SharedSecret': 'RandomKey',
                'ID': '11',
                'Name': 'eWirelessGroup',
                'TimeZone' : 'Asia/Qyzylorda'
            }

        }

        # Valid user on valid authenticator
        input = self.get_input_system_key_auth_sup("00:AA:11:BB:22:DD", "AA-BB-CC-AA-BB-CC")
        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(expected, result)
        input = self.get_input_system_key_auth_sup("00:AA:11:BB:22:DD", "AABBCCAABBCC")
        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(expected, result)

        expected['Supplicant']['Password'] = "AA:BB:CC:AA:CC:CC"
        input = self.get_input_system_key_auth_sup("00:AA:11:BB:22:DD", "AA-BB:CC:AA-CC:CC")
        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(expected, result)
        expected['Supplicant']['Password'] = "AA:BB:CC:AA:CC:CC"
        input = self.get_input_system_key_auth_sup("00:AA:11:BB:22:DD", "AABBCCAACCCC")
        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(expected, result)

        input = self.get_input_system_key_auth_sup("00:AA:11:BB:22:DD", "AA:BB:CC:AA:CC:^")
        self.assertRaisesRegex(EradValidationException, "Invalid Mac", self.mySystemI.supplicant_find,input)

        # Test Global Group_ID
        # this should go through the normal behavior, having to look for authenticators
        # This resource is created in our scheme/__init__.py

        global_expected = {
            'Supplicant': {
                'UseWildcard': False,
                'Username': 'globaltest',
                'Password': 'globaltest',
                'Group_ID': 'fooaccount::Global',
                'ClientInfo': {}
            },
            'Group': {
                'Account_ID': 'fooaccount',
                'ID': 'Global',
                'Name': 'Global',
                'TimeZone': 'America/Los_Angeles'
            },

        }

        input = self.get_input_system_key_auth_sup("aa:55:44:33:22:11", "testing")
        input.update({"Group": {"ID": "Global"}})
        endpoint = {"IpAddress": "52.26.34.201", "Port": "20000"}
        input.update({"Endpoint": endpoint})
        self.assertRaisesRegex(DoesNotExist, "Supplicant 'testing' does not exist for group 'Global'", self.mySystemI.supplicant_find, input)
        input.update({'Authenticator': {'ID': 'bb:55:44:33:22:11'}, 'Supplicant': {'Username': 'globaltest'}})
        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(global_expected, result)

        # Not Global Group_ID but has the same port
        expected = {
            'Supplicant': {
                'UseWildcard': False,
                'Username': 'testing',
                'Password': 'testing',
                'Group_ID': 'fooaccount::bar',
                'ClientInfo': {}
            },
            'Group': {
                'Account_ID': 'fooaccount',
                'ID': 'bar',
                'Name': 'bar',
                'TimeZone': 'America/Los_Angeles'
            },

        }

        input = self.get_input_system_key_auth_sup("66:55:44:33:22:11", "testing")
        input.update({"Group": {"ID": "bar"}})
        endpoint = {"IpAddress": "52.26.34.201", "Port": "20000"}
        input.update({"Endpoint": endpoint})

        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(expected, result)
        self.assertEqual(result['EndpointGroupName'], 'Global')
        self.assertEqual(result['EndpointGroupID'], 'Global')

        # Non Global Group_ID and not global port
        # meaning the authenticator should not be regarded
        non_global_expected = {
            'Supplicant': {
                'UseWildcard': False,
                'Username': 'testing',
                'Password': 'testing',
                'Group_ID': 'fooaccount::bar',
                'ClientInfo': {}
            },
            'Group': {
                'Account_ID': 'fooaccount',
                'ID': 'bar',
                'Name': 'bar',
                'TimeZone': 'America/Los_Angeles'
            },

        }

        input = self.get_input_system_key_auth_sup("66:55:44:33:22:11", "testing")
        input.update({"Group": {"ID": "bar"}})
        endpoint = {"IpAddress": "52.26.34.201", "Port": "20014"}
        input.update({"Endpoint": endpoint})

        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(non_global_expected, result)
        self.assertEqual(result['EndpointGroupName'], 'bar')
        self.assertEqual(result['EndpointGroupID'], 'bar')

        input = self.get_input_system_key_auth_sup("CC:55:44:33:22:11", "testing")
        input.update({"Group": {"ID": "bar"}})
        endpoint = {"IpAddress": "52.26.34.201", "Port": "20014"}
        input.update({"Endpoint": endpoint})

        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(non_global_expected, result)
        self.assertEqual(result['EndpointGroupName'], 'bar')
        self.assertEqual(result['EndpointGroupID'], 'bar')

        expected = {
            'Supplicant': {
                'UseWildcard': False,
                'Username': 'user_with_custom_data',
                'Password': 'uknow',
                'Vlan': 111,
                'UseRemote': False,
                'Group_ID': 'elevenos::11',
                'CustomJsonData': 'custom data',
                'ClientInfo': {}
            },
            'Group': {
                'Account_ID': 'elevenos',
                'RemoteServerUrl': 'remote.fake.com',
                'SharedSecret': 'RandomKey',
                'ID': '11',
                'Name': 'eWirelessGroup',
                'TimeZone': 'Asia/Qyzylorda'
            },

        }

        Erad_Supplicant.save(
            Erad_Supplicant(
                "elevenos::11",
                Username="user_with_custom_data",
                Password="uknow",
                Vlan=111,
                UseRemote=False,
                UseWildcard=False,
                CustomJsonData='custom data'
            )
        )

        # Valid user on valid authenticator
        input = self.get_input_system_key_auth_sup("00:AA:11:BB:22:CC", "user_with_custom_data")
        result = self.mySystemI.supplicant_find(input)
        self.assert_is_superset(expected, result)

    # Other tests or tests about checking queries when username is stored without realm.
    # For inter-BSP roaming (Clix integration) we started to store realms in the username files.
    # The next 4 tests checking queries for that.

    @mock.patch('erad.api.interface_system.get_client_info', return_value={'ClientInfo': {}})
    def test_supplicant_find_with_realm_in_non_global_group(self, mock_class):
        """Possible variant. New supplicant record stored in non-global group. And query and username records using
        realm"""
        test_supplicant =Erad_Supplicant(
            "realm_customer::g1|sadfsd5",
            Username="9656450686103731@cevent.cevent.wifi-onthego.calixcloud.com",
            Password="testpass",
            Vlan=111,
            UseRemote=False,
            UseWildcard=False,
            Parent_ID='test-parent-id',
            Account_ID='realm_customer'
        )
        test_supplicant.save()

        group = Erad_Group2(
                Account_ID="realm_customer",
                ID="g1|sadfsd5",
                Name="eWirelessGroup",
                SharedSecret="RandomKey",
                TimeZone="Asia/Qyzylorda"
        )
        group.save()

        authenticator = Erad_Authenticator2("00:AA:11:BB:22:CC", 'realm_customer', Group_ID="g1|sadfsd5")
        authenticator.save()

        endpoint = Erad_Endpoint(
            '10.0.0.1',
            Port="30000",
            Group_ID="realm_customer::g1|sadfsd5",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-1")
        endpoint.save()


        payload = {
            "SystemKey": util.erad_cfg().system_key[0],
            "Authenticator": {'ID': '00:AA:11:BB:22:CC'},
            "Supplicant": {'Username': '9656450686103731@cevent.cevent.wifi-onthego.calixcloud.com'},
            "Endpoint": {
                "IpAddress": "10.0.0.1",
                "Port": "30000",
            }
        }

        # just to be sure that user definetly found
        try:
            self.mySystemI.supplicant_find(payload)
        except Exception as err:
            logger.exception(err)
            self.fail("self.mySystemI.supplicant_find(payload) raised exception")


        endpoint.delete()
        group.delete()
        authenticator.delete()
        test_supplicant.delete()

    @mock.patch('erad.api.interface_system.get_client_info', return_value={'ClientInfo': {}})
    def test_supplicant_find_with_realm_in_non_global_group_search_without_realm(self, mock_class):
        """Username in non-global group stored without realm, query without realm.
        Expected to fail. If user query without realm then supplicant also should be stored without it"""
        test_supplicant =Erad_Supplicant(
            "realm_customer::g1|sadfsd5",
            Username="9656450686103731@cevent.cevent.wifi-onthego.calixcloud.com",
            Password="testpass",
            Vlan=111,
            UseRemote=False,
            UseWildcard=False,
            Parent_ID='test-parent-id',
            Account_ID='realm_customer'
        )
        test_supplicant.save()

        group = Erad_Group2(
                Account_ID="realm_customer",
                ID="g1|sadfsd5",
                Name="eWirelessGroup",
                SharedSecret="RandomKey",
                TimeZone="Asia/Qyzylorda"
        )
        group.save()

        authenticator = Erad_Authenticator2("00:AA:11:BB:22:CC", 'realm_customer', Group_ID="g1|sadfsd5")
        authenticator.save()

        endpoint = Erad_Endpoint(
            '10.0.0.1',
            Port="30000",
            Group_ID="realm_customer::g1|sadfsd5",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-1")
        endpoint.save()


        payload = {
            "SystemKey": util.erad_cfg().system_key[0],
            "Authenticator": {'ID': '00:AA:11:BB:22:CC'},
            "Supplicant": {'Username': '9656450686103731'},
            "Endpoint": {
                "IpAddress": "10.0.0.1",
                "Port": "30000",
            }
        }

        # Expecting to fail.
        with self.assertRaises(DoesNotExist) as expected_err:
            self.mySystemI.supplicant_find(payload)

        endpoint.delete()
        group.delete()
        authenticator.delete()
        test_supplicant.delete()


    @mock.patch('erad.api.interface_system.get_client_info', return_value={'ClientInfo': {}})
    def test_supplicant_find_with_realm_supplicant_in_global_group(self, mock_class):
        """Supplicant in Global group with Realm, query with realm"""
        test_supplicant =Erad_Supplicant(
            "realm_customer::Global",
            Username="9656450686103731@cevent.cevent.wifi-onthego.calixcloud.com",
            Password="testpass",
            Vlan=111,
            UseRemote=False,
            UseWildcard=False,
            Parent_ID='test-parent-id',
            Account_ID='realm_customer'
        )
        test_supplicant.save()

        group = Erad_Group2(
                Account_ID="realm_customer",
                ID="g1|sadfsd5",
                Name="eWirelessGroup",
                SharedSecret="RandomKey",
                TimeZone="Asia/Qyzylorda"
        )
        group.save()

        authenticator = Erad_Authenticator2("00:AA:11:BB:22:CC", 'realm_customer', Group_ID="g1|sadfsd5")
        authenticator.save()

        endpoint = Erad_Endpoint(
            '10.0.0.1',
            Port="30000",
            Group_ID="realm_customer::g1|sadfsd5",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-1")
        endpoint.save()


        payload = {
            "SystemKey": util.erad_cfg().system_key[0],
            "Authenticator": {'ID': '00:AA:11:BB:22:CC'},
            "Supplicant": {'Username': '9656450686103731@cevent.cevent.wifi-onthego.calixcloud.com'},
            "Endpoint": {
                "IpAddress": "10.0.0.1",
                "Port": "30000",
            }
        }

        # just to be sure that user definetly found
        try:
            self.mySystemI.supplicant_find(payload)
        except Exception as err:
            logger.exception(err)
            self.fail("self.mySystemI.supplicant_find(payload) raised exception")


        endpoint.delete()
        group.delete()
        authenticator.delete()
        test_supplicant.delete()

    @mock.patch('erad.api.interface_system.get_client_info', return_value={'ClientInfo': {}})
    def test_supplicant_find_supplicant_in_global_query_without_realm(self, mock_class):
        """Supplicant in Global group with Realm, query with realm.
        Expected to fail. If user query without realm then supplicant also should be stored without it"""
        test_supplicant =Erad_Supplicant(
            "realm_customer::Global",
            Username="9656450686103731@cevent.cevent.wifi-onthego.calixcloud.com",
            Password="testpass",
            Vlan=111,
            UseRemote=False,
            UseWildcard=False,
            Parent_ID='test-parent-id',
            Account_ID='realm_customer'
        )
        test_supplicant.save()

        group = Erad_Group2(
                Account_ID="realm_customer",
                ID="g1|sadfsd5",
                Name="eWirelessGroup",
                SharedSecret="RandomKey",
                TimeZone="Asia/Qyzylorda"
        )
        group.save()

        authenticator = Erad_Authenticator2("00:AA:11:BB:22:CC", 'realm_customer', Group_ID="g1|sadfsd5")
        authenticator.save()

        endpoint = Erad_Endpoint(
            '10.0.0.1',
            Port="30000",
            Group_ID="realm_customer::g1|sadfsd5",
            ClusterId="uswest-dev-01",
            Secret="4905DVCA3B",
            Region="us-west-1")
        endpoint.save()


        payload = {
            "SystemKey": util.erad_cfg().system_key[0],
            "Authenticator": {'ID': '00:AA:11:BB:22:CC'},
            "Supplicant": {'Username': '9656450686103731'},
            "Endpoint": {
                "IpAddress": "10.0.0.1",
                "Port": "30000",
            }
        }

        # Expcected to fail
        with self.assertRaises(DoesNotExist) as expected_err:
            self.mySystemI.supplicant_find(payload)


        endpoint.delete()
        group.delete()
        authenticator.delete()
        test_supplicant.delete()


    def test_mac_format_support(self):
        username = 'AA:BB:CC:DD:EE:FF'
        authenticator = 'AA:BB:CC:DD:EE:00'
        account_id = 'elevenos'
        group = Erad_Group2(Account_ID="elevenos",ID="AABB-1", Name="Mac support test", RemoteServerUrl="remote.fake.com",
                                              SharedSecret="RandomKey", TimeZone = "Asia/Qyzylorda")
        auth = Erad_Authenticator2(authenticator, account_id, Group_ID="AABB-1")
        supp = Erad_Supplicant("elevenos::AABB-1", Username=username, Password=username, Vlan=0,
                                                UseRemote=False, UseWildcard=False)
        group.save()
        auth.save()
        supp.save()
        expected =  {
                        'Supplicant': {
                            'UseWildcard': False,
                            'Username': username,
                            'Password': username,
                            'Vlan': 0,
                            'UseRemote': False,
                            'Group_ID': 'elevenos::AABB-1'
                        },
                        'Group': {
                            'RemoteServerUrl': 'remote.fake.com',
                            'SharedSecret': 'RandomKey',
                            'ID': 'AABB-1',
                            'Name': 'Mac support test',
                            'TimeZone' : 'Asia/Qyzylorda'
                        }
                    }

        # testing case sensitivity
        expected['Supplicant']['Username'] = username.lower()
        result = self.mySystemI.supplicant_find(self.get_input_system_key_auth_sup(authenticator, username.lower()))
        self.assert_is_superset(expected, result)

        # testing - instead of :
        expected['Supplicant']['Username'] = username.replace(':', '-')
        result = self.mySystemI.supplicant_find(self.get_input_system_key_auth_sup(authenticator, username.replace(':', '-')))
        self.assert_is_superset(expected, result)

        # testing without - or :
        expected['Supplicant']['Username'] = username.replace(':', '')
        result = self.mySystemI.supplicant_find(self.get_input_system_key_auth_sup(authenticator, username.replace(':', '')))
        self.assert_is_superset(expected, result)

        # delete all
        group = Erad_Group2(Account_ID="elevenos",ID="AABB-1", Name="Mac support test", RemoteServerUrl="remote.fake.com",
                                              SharedSecret="RandomKey", TimeZone = "Asia/Qyzylorda")
        auth = Erad_Authenticator2(authenticator, account_id, Group_ID="AABB-1")
        supp = Erad_Supplicant("AABB-1", Username=username, Password=username, Vlan=0,
                                                UseRemote=False, UseWildcard=False)
        supp.delete()
        auth.delete()
        group.delete()

    def test_account_save(self):
        # Test valid save
        input = self.get_input_account_save("test@elevenwireless.com", "TheEleven", "Eleven Wireless")
        self.mySystemI.account_save(input)

        # Test no AccountOwner key
        input = self.get_input_account_save("test@elevenwireless.com", "TheEleven", "Eleven Wireless")
        del(input["AccountOwner"])
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'AccountOwner' is missing.", self.mySystemI.account_save,input)

        # Test no Email
        input = self.get_input_account_save("test@elevenwireless.com", "TheEleven", "Eleven Wireless")
        del(input["AccountOwner"]['Email'])
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'Email' is missing.", self.mySystemI.account_save,input)

        # Test no Password
        input = self.get_input_account_save("test@elevenwireless.com", "TheEleven", "Eleven Wireless")
        del(input["AccountOwner"]['Password'])
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'Password' is missing.", self.mySystemI.account_save,input)

        # Test no Account_ID
        input = self.get_input_account_save("test@elevenwireless.com", "TheEleven", "Eleven Wireless")
        del(input["AccountOwner"]['Account_ID'])
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'Account_ID' is missing.", self.mySystemI.account_save,input)

        # Test blacklisted password
        input = self.get_input_account_save("test@elevenwireless.com", "password", "Eleven Wireless")
        self.assertRaisesRegex(EradBlacklistedPasswordException, "That password was found in a public list of previously compromised passwords. Please choose a different password.", self.mySystemI.account_save,input)
        # Check if blacklisted password checks are case insensitive
        input = self.get_input_account_save("test@elevenwireless.com", "PaSsWoRd", "Eleven Wireless")
        self.assertRaisesRegex(EradBlacklistedPasswordException, "That password was found in a public list of previously compromised passwords. Please choose a different password.", self.mySystemI.account_save,input)

    def test_radius_finish(self):
        input_data = {"SystemKey" : "0987654321poiuytrewq"}
        self.assertRaisesRegexp(EradAccessException,
                                "Access Denied.",
                                self.mySystemI.radius_finish,
                                input_data)
        # Test valid key
        input_data = self.get_input_system_key()
        self.mySystemI.authorize_system_key(input_data)

        system_key = input_data.get("SystemKey")
        # Missing Cluster_ID
        input_data = {
            "SystemKey": system_key,
            }
        self.assertRaisesRegex(EradRequiredFieldException,
                                "Required parameter 'Cluster_ID' is missing.",
                                self.mySystemI.radius_finish,
                                input_data)
        # Invalid Cluster_ID
        input_data = {
            "SystemKey": system_key,
            "Cluster_ID": "invalid cluster id"
            }
        self.assertRaisesRegexp(EradValidationException, "Cluster not found.",
                                self.mySystemI.radius_finish, input_data)

        # Valid Cluster_ID
        input_data.update({'Cluster_ID': 'uswest-dev-03'})
        Erad_Endpoint('11.22.33.44.55', Port='2222', Group_ID='::dirty', ClusterId='uswest-dev-03', Secret='secretCluster', Region='us-west-1').save()
        Erad_Cluster('uswest-dev-03', AuthNum=2, AcctNum=2, Instances='instances_dicts').save()

        endpoint = Erad_Endpoint.get('11.22.33.44.55', '2222')
        group_id = endpoint.Group_ID
        self.assertEqual(group_id, '::dirty')
        self.mySystemI.radius_finish(input_data)
        endpoint.refresh()
        finished_cluster_group_id = endpoint.Group_ID
        self.assertEqual(finished_cluster_group_id, '::free')

        # Valid Cluster_ID that has no ::dirty Group_ID
        self.mySystemI.radius_finish(input_data)
        endpoint.delete()

    def test_radius_finish_premium_port(self):
        input_data = self.get_input_system_key()
        self.mySystemI.authorize_system_key(input_data)

        system_key = input_data.get("SystemKey")

        # Valid Cluster_ID
        input_data = {
            "SystemKey": system_key,
            "Cluster_ID": "uswest-dev-03"
        }

        Erad_Endpoint('11.22.33.44.55', Port='40000', Group_ID='::dirty', ClusterId='uswest-dev-03', Secret='secretCluster', Region='us-west-1').save()
        Erad_Cluster('uswest-dev-03', AuthNum=2, AcctNum=2, Instances='instances_dicts').save()
        endpoint = Erad_Endpoint.get('11.22.33.44.55', '40000')
        endpoint.refresh()
        group_id = endpoint.Group_ID
        self.assertEqual(group_id, '::dirty')
        self.mySystemI.radius_finish(input_data)
        endpoint.refresh()
        finished_cluster_group_id = endpoint.Group_ID
        self.assertEqual(finished_cluster_group_id, '::premium')

        # Valid Cluster_ID that has no ::dirty Group_ID
        self.mySystemI.radius_finish(input_data)
        endpoint.delete()

    def test_radius_region(self):
        input = {"SystemKey" : "0987654321poiuytrewq"}
        self.assertRaisesRegex(EradAccessException,
                                "Access Denied.",
                                self.mySystemI.radius_finish,
                                input)
        # Test valid key
        input = self.get_input_system_key()
        self.mySystemI.authorize_system_key(input)

        system_key = input.get("SystemKey")
        # Missing Cluster_ID
        input = {
            "SystemKey": system_key,
            }
        self.assertRaisesRegex(EradRequiredFieldException,
                                "Required parameter 'Cluster_ID' is missing.",
                                self.mySystemI.radius_finish,
                                input)
        # Invalid Cluster_ID
        input = {
            "SystemKey": system_key,
            "Cluster_ID": "invalid cluster id"
            }
        self.assertRaisesRegex(EradValidationException, "Cluster not found.",
                                self.mySystemI.radius_finish, input)

        # Valid Cluster_ID
        input.update({'Cluster_ID': 'uswest-dev-03'})
        endpoint = next(Erad_Endpoint().cluster_id_index.query('uswest-dev-03', limit=1))
        region = endpoint.Region
        self.assertEqual(region, 'us-west-1')
        self.mySystemI.radius_finish(input)


    # In production erad.util.get_ec2_instances() should return instances list
    # that will be parsed by erad.util.parse_cluster_instances()
    # This test check the basic functionality of radius_save() and did not check that
    # clusters that shared load balancer will be updated.
    # So for this test it is simpler to mock rad.util.get_ec2_instances
    # to return empty list. And mock erad.util.parse_cluster_instances
    # to return already stored values
    @mock.patch('erad.util.get_ec2_instances', side_effect=mock_get_ec2_instances_empty)
    @mock.patch('erad.util.parse_cluster_instances', side_effect=mock_return_existed_instances)
    def test_radius_save(self, mock_return_existed_instances, mock_get_ec2_instances_empty):
        input = {"SystemKey" : "0987654321poiuytrewq"}
        self.assertRaisesRegex(EradAccessException,
                                "Access Denied.",
                                self.mySystemI.radius_save,
                                input)
        # Test valid key
        input = self.get_input_system_key()
        self.mySystemI.authorize_system_key(input)

        system_key = input.get("SystemKey")
        # Missing Cluster_ID
        input = {
            "SystemKey": system_key,
            }
        self.assertRaisesRegex(EradRequiredFieldException,
                                "Required parameter 'Servers' is missing.",
                                self.mySystemI.radius_save,
                                input)

        input.update({'Servers': {}})
        self.assertRaisesRegex(EradValidationExceptionRadius,
                                "Invalid Servers parameter format.",
                                self.mySystemI.radius_save,
                                input)
        input.update({'Servers': {'useast-dev-01': {'auth': {}}}})
        self.assertRaisesRegex(EradValidationExceptionRadius,
                                "Invalid Servers parameter format.",
                                self.mySystemI.radius_save,
                                input)
        input.update({'Servers': { 'useast-dev-01': {'acct': {}}}})
        self.assertRaisesRegex(EradValidationExceptionRadius,
                                "Invalid Servers parameter format.",
                                self.mySystemI.radius_save,
                                input)
        input.update({'Servers': {
                        'useast-dev-01': {
                            'acct': {'instance-acct-id': '11.22.33.44.55'},
                            'auth': {}
                        }
                    }})

        cluster = Erad_Cluster().get('useast-dev-01')
        self.assertTrue('instance-acct-id' not in cluster.Instances)
        self.mySystemI.radius_save(input)
        cluster.refresh()
        self.assertTrue('instance-acct-id' in cluster.Instances)

        input.update({'Servers': {
                        'useast-dev-01': {
                            'auth': {'instance-auth-id': '11.22.33.44.55'},
                            'acct': {}
                        }
                    }})
        self.mySystemI.radius_save(input)
        cluster.refresh()
        self.assertTrue('instance-auth-id' in cluster.Instances)
        self.assertTrue('instance-acct-id' not in cluster.Instances)

        input.update({'Servers': {
                        'useast-dev-01': {
                            'auth': {},
                            'acct': {}
                        }
                    }})
        self.assertRaisesRegex(EradValidationExceptionRadius,
                                "Invalid Servers parameter format.",
                                self.mySystemI.radius_save,
                                input)
        input.update({'Servers': {
                        'useast-dev-01': {
                            'auth': {'instance-auth-id-1234': '11.22.33.44.55'},
                            'acct': {'instance-acct-id-1591': '11.22.33.44.55'}
                        }
                     }})
        cluster = Erad_Cluster().get('useast-dev-01')
        self.assertTrue('instance-auth-id-1234' not in cluster.Instances)
        self.assertTrue('instance-acct-id-1591' not in cluster.Instances)
        self.mySystemI.radius_save(input)
        cluster.refresh()
        # since the field is a string, for simplicity sake the update test will be the following
        self.assertTrue('instance-auth-id-1234' in cluster.Instances)
        self.assertTrue('instance-acct-id-1591' in cluster.Instances)
        input.update({'Servers': {
                        'dummy-cluster': {
                            'auth': {'instance-auth-id-1234': '11.22.33.44.55'},
                            'acct': {'instance-acct-id-1591': '11.22.33.44.55'}
                        }
                    }})
        self.mySystemI.radius_save(input)

        result = [x for x in Erad_Cluster.query('dummy-cluster')]
        self.assertEqual(len(result), 0)


    @mock.patch('erad.util.get_ec2_instances', side_effect=mock_get_ec2_instances)
    def test_save_radius_many_clusters(self, mock_get_ec2_instances):
        # prepare test clusters

        Erad_Cluster('test-01', AuthNum=1, AcctNum=1, Instances='''{'auth': {'i-1': '1.1.1.1'}, 'acct': {'i-2': '1.1.1.2'}}''').save()
        Erad_Cluster('test-02', AuthNum=1, AcctNum=1, Instances='''{'auth': {'i-3': '1.1.2.1'}, 'acct': {'i-3': '1.1.2.2'}}''').save()


        sys_key = self.get_input_system_key()['SystemKey']
        input = {
            "SystemKey": sys_key,
            'Servers': {
                'test-01': {
                    'acct': {'instance-acct-id': '11.22.33.44.55'},
                    'auth': {}
                },
                'test-02': {
                    'auth': {'instance-auth-id-1234': '11.22.33.44.55'},
                    'acct': {'instance-acct-id-1591': '11.22.33.44.55'}
                }
            }
        }
        self.mySystemI.radius_save(input)

        cluster_1 = Erad_Cluster().get('test-01')

        self.assertTrue('instance-acct-id' in cluster_1.Instances)

        cluster_2 = Erad_Cluster().get('test-02')
        self.assertTrue('instance-auth-id-1234' in cluster_2.Instances)
        self.assertTrue('instance-acct-id-1591' in cluster_2.Instances)

        # teardown test data
        cluster_1.delete()
        cluster_2.delete()

    def test_server_config_load(self):
        # Prepare test_clusters, forcing to creation records with matches ports to reproduction this situation
        # the_list =[
        #   (1814, {'auth': {}, 'acct': {'i1': '1.1.1.2'}}),
        #   (1815, {'auth': {}, 'acct': {'i1': '1.1.1.1'}}),
        #   (1814, {'auth': {}, 'acct': {'i1': '1.1.1.1'}})
        # ]
        # or just sorted([{'a': 1}, {'a': 2}])

        # In Python 3 dictionaries are no longer orderable and operation sorted(the_list) will throw exception.
        # Testing this edge case

        # This test case also probably showing error in logic that construct output

        Erad_Cluster('c-01', AuthNum=1, AcctNum=1, Instances='''{'auth': {}, 'acct': {'i1': '1.1.1.1'}}''').save()
        Erad_Cluster('c-02', AuthNum=1, AcctNum=1, Instances='''{'auth': {}, 'acct': {'i1': '1.1.1.2'}}''').save()

        Erad_Endpoint('1.0.0.1', Port='1815', ClusterId='c-01', Secret='whatever', Region='test-region').save()
        Erad_Endpoint('1.0.0.1', Port='1814', ClusterId='c-02', Secret='whatever', Region='test-region').save()

        fields = {"SystemKey": util.erad_cfg().system_key[0],  "ClusterId": "c-01", "ServerType": "load_balancer"}
        self.mySystemI.server_config_load(fields)

    def test_server_config_load_shared_secrets(self):
        """Testing that API to return shared secrets will return all shared secrets that served on load balancer
        that served give cluster id.

        :return:
        """

        Erad_Cluster('c-01', AuthNum=1, AcctNum=1, Instances='''{'auth': {}, 'acct': {'i1': '1.1.1.1'}}''').save()
        Erad_Cluster('c-02', AuthNum=1, AcctNum=1, Instances='''{'auth': {}, 'acct': {'i1': '1.1.1.2'}}''').save()

        Erad_Endpoint('1.0.0.1', Port='1816', ClusterId='c-01', Secret='whatever', Region='test-region').save()
        Erad_Endpoint('1.0.0.1', Port='1818', ClusterId='c-02', Secret='whatever2', Region='test-region').save()

        fields = {
            "SystemKey": util.erad_cfg().system_key[0],
            "ClusterId": "c-01",
            "ServerType": "radius_shared_secrets"
        }
        result = self.mySystemI.server_config_load(fields)

        secrets = result['SharedSecrets']
        self.assertEqual(secrets['1816'], 'whatever')
        self.assertEqual(secrets['1818'], 'whatever2')
        self.assertEqual(secrets['1814'], 'V72QSZ2CE3')

        fields = {
            "SystemKey": util.erad_cfg().system_key[0],
            "ClusterId": "not-existed",
            "ServerType": "radius_shared_secrets"}
        result = self.mySystemI.server_config_load(fields)
        self.assertEqual(result, {'SharedSecrets': {}})

    @mock.patch('erad.util.get_ec2_instances', side_effect=mock_get_ec2_instances)
    def test_save_radius_many_clusters_with_endpoints(self, parse_cluster_function):
        # Prepare test clusters, these value represent Erad_Cluster table dirty state.
        # Script `frmod_stage.sh` was finished but without switching to new instances, so table store one value but actually
        # other instances are handling radius request.
        # Actual "production" instances will be returned by mock_get_ec2_instances() function
        Erad_Cluster('c-01', AuthNum=1, AcctNum=1, Instances='''{'auth': {'i-1': '1.1.1.1'}, 'acct': {'i-2': '1.1.1.2'}}''').save()
        Erad_Cluster('test-01_1', AuthNum=1, AcctNum=1, Instances='''{'auth': {'i-01_1': '1.10.1.1'}, 'acct': {'i-02_1': '1.10.1.2'}}''').save()
        Erad_Cluster('test-01_2', AuthNum=1, AcctNum=1, Instances='''{'auth': {'i-01_2': '1.20.1.1'}, 'acct': {'i-02_2': '1.20.1.2'}}''').save()

        Erad_Cluster('c-02', AuthNum=1, AcctNum=1, Instances='''{'auth': {'i-021': '1.1.2.1'}, 'acct': {'i-022': '1.1.2.2'}}''').save()
        Erad_Cluster('test-02_1', AuthNum=1, AcctNum=1, Instances='''{'auth': {'i-02_1': '1.1.20.1'}, 'acct': {'i-02-2': '1.1.20.2'}}''').save()

        Erad_Endpoint('1.0.0.1', Port='1818', ClusterId='c-01', Secret='whatever', Region='test-region').save()
        Erad_Endpoint('1.0.0.1', Port='1819', ClusterId='test-01_1', Secret='whatever', Region='test-region').save()
        Erad_Endpoint('1.0.0.1', Port='1820', ClusterId='test-01_2', Secret='whatever', Region='test-region').save()

        Erad_Endpoint('1.0.0.2', Port='1821', ClusterId='c-02', Secret='whatever', Region='test-region').save()
        Erad_Endpoint('1.0.0.2', Port='1822', ClusterId='test-02_1', Secret='whatever', Region='test-region').save()

        sys_key = self.get_input_system_key()['SystemKey']
        input = {
            "SystemKey": sys_key,
            'Servers': {
                'c-01': {
                    'acct': {'instance-acct-id': '11.22.33.44.55'},
                    'auth': {}
                },
                'c-02': {
                    'auth': {'instance-auth-id-1234': '11.22.33.44.55'},
                    'acct': {'instance-acct-id-1591': '11.22.33.44.55'}
                }
            }
        }
        self.mySystemI.radius_save(input)

        clusters_list = {
            'c-01',
            'test-01_1',
            'test-01_2',
            'c-02',
            'test-02_1',
        }
        expected_data = {
            'c-01': {'auth': {'instance-acct-id': '11.22.33.44.55'}, 'acct': {}},
            'c-02': {'auth': {'instance-auth-id-1234': '11.22.33.44.55'}, 'acct': {'instance-acct-id-1591': '11.22.33.44.55'}},
        }


        for cluster_id in clusters_list:
            cur_cluster = Erad_Cluster.get(cluster_id)

            # will return instances for clusters that was not updated (data mocked)
            expected_result = util.parse_cluster_instances(cluster_id, mock_get_ec2_instances(''))

            # this instruction return instances that was updated c-01, c-02
            if expected_result['auth'] == {} and expected_result['acct'] == {}:
                expected_result = expected_data[cluster_id]

            for key, value in list(expected_result.items()):
                self.assertTrue(str(value) in cur_cluster.Instances)

        # teardown test data
        for cluster_id in clusters_list:
            cur_cluster = Erad_Cluster.get(cluster_id)
            cur_cluster.delete()

        for endpoint_ip in ['1.0.0.1', '1.0.0.2']:
            endpoint_list = Erad_Endpoint.query(endpoint_ip)
            for cur_endpoint in endpoint_list:
                cur_endpoint.delete()

    def test_valid_server_parameter(self):
        invalid_servers = {}
        self.assertFalse(self.mySystemI.is_valid_servers_parameter(invalid_servers))

        invalid_servers.update({'acct': {}})
        self.assertFalse(self.mySystemI.is_valid_servers_parameter(invalid_servers))

        del invalid_servers['acct']
        invalid_servers.update({'auth': {}})
        self.assertFalse(self.mySystemI.is_valid_servers_parameter(invalid_servers))

        invalid_servers.update({'acct': {}})
        self.assertFalse(self.mySystemI.is_valid_servers_parameter(invalid_servers))

        invalid_servers.update({'acct': {'instance-id': '11.22.33.44.55'}})
        self.assertTrue(self.mySystemI.is_valid_servers_parameter(invalid_servers))

        del invalid_servers['acct']

        invalid_servers.update({'auth': {'instance-id': '11.22.33.44.55'}})
        self.assertTrue(self.mySystemI.is_valid_servers_parameter(invalid_servers))

        valid_servers = {'auth': {'auth-instance-id': '11.22.33.44.55'},
                         'acct': {'acct-instance-id': '11.22.33.44.55'}
                        }
        self.assertTrue(self.mySystemI.is_valid_servers_parameter(valid_servers))

    @mock.patch('erad.api.interface_system.send_log_sns')
    def test_supplicant_log_save_with_parent_id(self, mock_func):
        bogus_auth = "11:11:11:11:11:11"
        global_endpoint = {"EndpointIPAddress": "52.26.34.201", "EndpointPort": "20006"}
        bogus_index_query = "xptoaccount::None"

        input = self.get_input_system_key_auth_sup(bogus_auth, "testing")
        log_input = {
            "Access": "Accept",
            "CalledStationId": bogus_auth,
            "CallingStationId": "dd:ee:cc:00:00:00",
            "EventTimestamp": "1",
            "IsProxied": "yes",
            "NASIdentifier": "identifier",
            "NASIPAddress": "200.100.100.100",
            "PacketType": "Access-Request",
            "Parent_ID": "testaccount@example.com",
            "Timestamp": "123456789",
            "UserName": "myuser",
            "EndpointGroupName": "The ABC Hotel",
            "EndpointGroupID": "XX-ZZZZ",
            "CustomJsonData": '{ "Parent_ID": "test-parent", "DeviceType": null }'

        }
        log_input.update(global_endpoint)
        input.update({"Logs": [log_input]})

        # Make sure there is no entry with that AccountGroup_ID
        count_before = Erad_Radius_Log().AccountGroupId_index.count(bogus_index_query)
        assert count_before == 0

        self.mySystemI.supplicant_log_save(input)
        count_after = Erad_Radius_Log().AccountGroupId_index.count(bogus_index_query)
        assert count_after == 1

        # Checking data
        entry = next(Erad_Radius_Log().AccountGroupId_index.query(bogus_index_query))
        assert entry.Parent_ID == "testaccount@example.com"
        self.assertEqual(entry.CustomJsonData, '{ "Parent_ID": "test-parent", "DeviceType": null }')

        # cleanup test record to not affect other tests
        entry.delete()
        count_before = Erad_Radius_Log().AccountGroupId_index.count(bogus_index_query)
        assert count_before == 0


    def test_get_authenticator_info(self):
        log_record = {
            "Access": "Accept",
            "CalledStationId": "11:22:33:44:55:66",
            "CallingStationId": "test",
            "EventTimestamp": "1",
            "IsProxied": "yes",
            "NASIdentifier": "foo",
            "NASIPAddress": "200.100.100.100",
            "PacketType": "Access-Request",
            "Timestamp": "123456789",
            "UserName": "user_8000",
            "EndpointIPAddress": "52.26.34.201",
            "EndpointPort": "20014",
        }

        payload = self.get_input_system_key_auth_sup("testaccount::testgroup", 'user_8000')
        payload.update({"Logs": [log_record]})

        authenticator_group_id, authenticator_group_name = self.mySystemI.get_authenticator_info(
            "elevenos", log_record)
        self.assertEqual(authenticator_group_id, 'foo')
        self.assertEqual(authenticator_group_name, 'foo')


    @mock.patch('erad.api.interface_system.send_log_sns')
    def test_mark_device_as_used_supplicant_without_parent_id(self, mock_func):
        """Checking that function will update information about last time profile/devide was used"""

        log_record = {
            "Access": "Accept",
            "CalledStationId": "foo",
            "CallingStationId": "dd:ee:cc:00:00:00",
            "EventTimestamp": "1",
            "IsProxied": "yes",
            "NASIdentifier": "identifier",
            "NASIPAddress": "200.100.100.100",
            "PacketType": "Access-Request",
            "Timestamp": "123456789",
            "UserName": "user_8000",
            "EndpointIPAddress": "52.26.34.201",
            "EndpointPort": "20014",
        }

        payload = self.get_input_system_key_auth_sup("testaccount::testgroup", 'user_8000')
        payload.update({"Logs": [log_record]})

        supplicant_with_parent_id = Erad_Supplicant(
            Account_ID="testaccount",
            Group_ID="testaccount::testgroup",
            Username='user_8000'
        )
        supplicant_with_parent_id.save()

        # log input old form - does not have information about Parent_ID, most of the existing users will generate
        # such record
        self.mySystemI.mark_device_as_used("testaccount", "testgroup", log_record)

        supplicant_from_db = Erad_Supplicant.get("testaccount::testgroup", "user_8000")

        # only device_used will be updated
        self.assertTrue(supplicant_from_db.device_used is not None)
        self.assertEqual(supplicant_from_db.profile_used, None)

        # the last [0] is to get supplicant from array
        supplicant_in_mock = json.loads(mock_func.call_args[0][0])[0]

        self.assertEqual(supplicant_in_mock['RequestType'], 'first_time_use_device')
        self.assertEqual(supplicant_in_mock['Group_ID'], 'testaccount::testgroup')
        self.assertEqual(supplicant_in_mock['Username'], 'user_8000')

        # log input with Parent_ID info, should update device used and profile_used to current time
        log_record.update(
            {'Parent_ID': 'test-parent-profile'}
        )

        self.mySystemI.mark_device_as_used("testaccount", "testgroup", log_record)

        supplicant_from_db = Erad_Supplicant.get("testaccount::testgroup", "user_8000")

        # the last [0] is to get supplicant from array
        supplicant_in_mock = json.loads(mock_func.call_args[0][0])[0]
        self.assertEqual(supplicant_in_mock['RequestType'], 'first_time_use_device')
        self.assertEqual(supplicant_in_mock['Group_ID'], 'testaccount::testgroup')
        self.assertEqual(supplicant_in_mock['Username'], 'user_8000')

        #profile_used not uped because thereis no information about parent id
        self.assertTrue(supplicant_from_db.device_used is not None)
        self.assertEqual(supplicant_from_db.profile_used, None)

        # edge case trying to use not existed supplicant
        mock_func.reset_mock()
        self.mySystemI.mark_device_as_used("testaccount", "unknown", log_record)
        self.mySystemI.mark_device_as_used("unknown", "unknown", log_record)

        mock_func.assert_not_called()


        # account id is None or "", account id is extracted from endpoint group info,
        # and if it equal to ::free then account is "", plust checking for None
        mock_func.reset_mock()
        self.mySystemI.mark_device_as_used("", "unknown", log_record)
        mock_func.assert_not_called()

        mock_func.reset_mock()
        self.mySystemI.mark_device_as_used(None, "unknown", log_record)
        mock_func.assert_not_called()

        supplicant_with_parent_id.delete()
        supplicant_from_db.delete()

    def test_nofail_generate_message(self):
        supp = Erad_Supplicant(
            Account_ID="testaccount",
            Group_ID="testaccount::testgroup",
            Username='user_8000',
            Parent_ID='parent-test-id',
            device_used=15000000
        )
        supp.save()

        # message is an list with 1 element [0] to extract it
        output = json.loads(self.mySystemI._generate_message(supp, 'first_time_use_device'))[0]

        expected_output = {
            'Account_ID': 'testaccount',
            'Parent_ID': 'parent-test-id',
            'Group_ID': 'testaccount::testgroup',
            'Username': 'user_8000',
            'device_used': 15000000,
            'profile_used': None,
            'RequestType': 'first_time_use_device',
            'CustomJsonData': None
        }
        self.assert_is_superset(expected_output, output)

        # Check EventTimestamp_iso key exists on output object.
        self.assertIn('EventTimestamp_iso', output)
        self.assertGreater(len(output['EventTimestamp_iso']), 1)

        # check message is not passed in output if _generate_message is called with no msg attribute.
        output = json.loads(self.mySystemI._generate_message(supp, None))[0]
        self.assertEqual(output['RequestType'], None)

        supp.delete()

    @mock.patch('erad.api.interface_system.send_log_sns')
    def test_mark_device_as_used_supplicant_with_parent_id(self, mock_func):
        mock_func.reset_mock()
        """Checking that function will update information about last time profile/devide was used"""

        log_record = {
            "Access": "Accept",
            "CalledStationId": "foo",
            "CallingStationId": "dd:ee:cc:00:00:00",
            "EventTimestamp": "1",
            "IsProxied": "yes",
            "NASIdentifier": "identifier",
            "NASIPAddress": "200.100.100.100",
            "PacketType": "Access-Request",
            "Timestamp": "123456789",
            "UserName": "user_8000",
            "EndpointIPAddress": "52.26.34.201",
            "EndpointPort": "20014",
        }

        payload = self.get_input_system_key_auth_sup("testaccount::testgroup", 'user_8000')
        payload.update({"Logs": [log_record]})

        supplicant_with_parent_id = Erad_Supplicant(
            Account_ID="testaccount",
            Group_ID="testaccount::testgroup",
            Username='user_8000',
            Parent_ID="test-parent-profile"
        )
        supplicant_with_parent_id.save()

        supplicant_with_global_group = Erad_Supplicant(
            Account_ID="testacc",
            Group_ID="testacc::Global",
            Username='user_8000',
            Parent_ID="test-parent-profile"
        )
        supplicant_with_global_group.save()

        # log input old form - does not have information about Parent_ID, most of the existing users will generate
        # such record
        self.mySystemI.mark_device_as_used("testaccount", "testgroup", log_record)

        supplicant_from_db = Erad_Supplicant.get("testaccount::testgroup", "user_8000")

        # only device_used will be updated
        self.assertTrue(supplicant_from_db.device_used is not None)
        self.assertEqual(supplicant_from_db.profile_used, None)

        # checking data that will be send to SNS, only device_used because log record does not have information about
        # parent_id

        # the last [0] is to get supplicant from array
        supplicant_in_mock = json.loads(mock_func.call_args[0][0])[0]

        self.assertEqual(supplicant_in_mock['RequestType'], 'first_time_use_device')
        self.assertEqual(supplicant_in_mock['Group_ID'], 'testaccount::testgroup')
        self.assertEqual(supplicant_in_mock['Username'], 'user_8000')

        # checking case when supplicant not found in DB
        mock_func.reset_mock()
        log_record.update(
            {
                'Parent_ID': 'notexists',
                'UserName': 'notexists'
            }
        )

        self.mySystemI.mark_device_as_used("testaccount", "testgroup", log_record)
        # because neirthe Parrent_ID or UserName exists in DB no calls to SNS
        self.assertEqual(mock_func.call_count, 0)

        # log input with Parent_ID info, should update device used and profile_used to current time
        mock_func.reset_mock()
        log_record.update(
            {
                'Parent_ID': 'test-parent-profile',
                'UserName': 'user_8000'
            }
        )

        self.mySystemI.mark_device_as_used("testaccount", "testgroup", log_record)

        # checking data that will be send to SNS

        # device_used already set, only profile_used will be send
        # the last [0] is to get supplicant from array
        supplicant_in_mock = json.loads(mock_func.call_args[0][0])[0]
        self.assertEqual(supplicant_in_mock['RequestType'], 'first_time_use_profile')
        self.assertEqual(supplicant_in_mock['Group_ID'], 'testaccount::testgroup')
        self.assertEqual(supplicant_in_mock['Username'], 'user_8000')

        supplicant_from_db = Erad_Supplicant.get("testaccount::testgroup", "user_8000")

        # device_used and profile_used should be updated
        self.assertTrue(supplicant_from_db.device_used is not None)
        self.assertTrue(supplicant_from_db.profile_used is not None)
        mock_func.reset_mock()


        # reseting supplicant state to test that both field can be set simulteneously
        supplicant_from_db.device_used = None
        supplicant_from_db.profile_used = None
        supplicant_from_db.save()


        self.mySystemI.mark_device_as_used("testaccount", "testgroup", log_record)
        # checking only messages, since supplicant is the same and was checked before
        self.assertTrue('first_time_use_profile' in mock_func.mock_calls[0][1][0])
        self.assertTrue('first_time_use_device' in mock_func.mock_calls[1][1][0])

        mock_func.reset_mock()

        # edge case trying to use not existed supplicant, should not send logs to sns
        self.mySystemI.mark_device_as_used("unknown", "unknown", log_record)
        self.mySystemI.mark_device_as_used("unknown", "unknown", log_record)

        mock_func.assert_not_called()

        # test that global group is checked if specific group not found.

        supplicant_from_db = Erad_Supplicant.get("testacc::Global", "user_8000")
        supplicant_from_db.device_used = None
        supplicant_from_db.save()

        self.mySystemI.mark_device_as_used("testacc", "jkjbfjqwjfnw", log_record)
        self.assertTrue('first_time_use_device' in mock_func.mock_calls[1][1][0])

        supplicant_from_db.refresh()
        self.assertIsNotNone(supplicant_from_db.device_used)

        supplicant_with_global_group.delete()
        supplicant_with_parent_id.delete()
        supplicant_from_db.delete()

    @mock.patch('erad.api.interface_system.send_log_sns')
    def test_supplicant_log_save(self, mock_func):
        global_endpoint = {"EndpointIPAddress": "52.26.34.201", "EndpointPort": "20000"}
        bogus_auth = "11:11:11:11:11:11"
        global_auth = "BB:55:44:33:22:11"
        global_index_query = "fooaccount::Global"
        bogus_index_query = "fooaccount::None"
        bar_index_query = "fooaccount::bar"

        # Global testing with bogus authenticator
        input = self.get_input_system_key_auth_sup(bogus_auth, "testing")
        log_input = {
            "Access": "Accept",
            "CalledStationId": bogus_auth,
            "CallingStationId": "dd:ee:cc:00:00:00",
            "EventTimestamp": "1",
            "IsProxied": "yes",
            "NASIdentifier": "identifier",
            "NASIPAddress": "200.100.100.100",
            "PacketType": "Access-Request",
            "Timestamp": "123456789",
            "UserName": "myuser",
            "EndpointGroupName": "The ABC Hotel",
            "EndpointGroupID": "XX-ZZZZ",
            "CustomJsonData": '{ "Parent_ID": null, "DeviceType": null }'
        }
        log_input.update(global_endpoint)
        input.update({"Logs": [log_input]})

        # Make sure there is no entry with that AccountGroup_ID
        count_before = Erad_Radius_Log().AccountGroupId_index.count(bogus_index_query)
        assert count_before == 0

        self.mySystemI.supplicant_log_save(input)
        count_after = Erad_Radius_Log().AccountGroupId_index.count(bogus_index_query)
        assert count_after == 1

        # Checking data
        entry = next(Erad_Radius_Log().AccountGroupId_index.query(
            bogus_index_query,
            Erad_Radius_Log.EventTimestamp=="1")
        )
        assert entry.Group_ID == "None"
        assert entry.Group_Name == "Site not found"
        self.assertEqual(entry.CustomJsonData, '{ "Parent_ID": null, "DeviceType": null }')

        # Global testing with good authenticator
        input.update({"Authenticator": {"ID": global_auth}})
        input.update({"Supplicant": {"Username": "globaltest"}})
        log_input.update({"EventTimestamp": "2"})
        log_input.update({"NASIdentifier": global_auth})
        input.update({"Logs": [log_input]})

        # Make sure there is no entry with that AccountGroup_ID
        count_before = Erad_Radius_Log().AccountGroupId_index.count(global_index_query)
        assert count_before == 0

        self.mySystemI.supplicant_log_save(input)
        count_after = Erad_Radius_Log().AccountGroupId_index.count(global_index_query)
        assert count_after == 1

        # Checking data
        entry = next(Erad_Radius_Log().AccountGroupId_index.query(
            global_index_query,
            Erad_Radius_Log.EventTimestamp=="2")
        )
        assert entry.Group_ID == "Global"
        assert entry.Group_Name == "Global"

        # Not Global group but with endpoint with Global port
        input.update({"Authenticator": "66:55:44:33:22:11"})
        input.update({"Supplicant": {"Username": "testing"}})
        log_input.update({"EventTimestamp": "3"})
        log_input.update({"NASIdentifier": "66:55:44:33:22:11"})

        input.update({"Logs": [log_input]})

        # Make sure there is no entry with that AccountGroup_ID
        count_before = Erad_Radius_Log().AccountGroupId_index.count(bar_index_query)
        assert count_before == 0

        self.mySystemI.supplicant_log_save(input)
        count_after = Erad_Radius_Log().AccountGroupId_index.count(bar_index_query)
        assert count_after == 1

        # Checking data
        entry = next(Erad_Radius_Log().AccountGroupId_index.query(
            bar_index_query,
            Erad_Radius_Log.EventTimestamp=="3")
        )
        assert entry.Group_ID == "bar"
        assert entry.Group_Name == "bar"

        # Not Global nor Global port, this should not regard the bogus authenticator
        input.update({"Authenticator": bogus_auth})
        input.update({"Supplicant": {"Username": "testing"}})
        log_input.update({"EventTimestamp": "4"})
        log_input.update({"NASIdentifier": bogus_auth})
        not_global_endpoint = {"EndpointIPAddress": "52.26.34.201", "EndpointPort": "20014"}
        log_input.update(not_global_endpoint)
        input.update({"Logs": [log_input]})

        # Make sure only 1 entry for fooaccount::bar
        count_before = Erad_Radius_Log().AccountGroupId_index.count(bar_index_query)
        assert count_before == 1

        self.mySystemI.supplicant_log_save(input)
        count_after = Erad_Radius_Log().AccountGroupId_index.count(bar_index_query)
        assert count_after == 2

        # Checking data
        entry = next(Erad_Radius_Log().AccountGroupId_index.query(
            bar_index_query,
            Erad_Radius_Log.EventTimestamp=="4")
        )
        assert entry.Group_ID == "bar"
        assert entry.Group_Name == "bar"

        input.update({"Authenticator": "66:55:44:33:22:11"})
        input.update({"Supplicant": {"Username": "testing"}})
        log_input.update({"EventTimestamp": "5"})
        log_input.update({"NASIdentifier": "66:55:44:33:22:11"})

        input.update({"Logs": [log_input]})

        # Make sure there are 2 matches
        count_before = Erad_Radius_Log().AccountGroupId_index.count(bar_index_query)
        assert count_before == 2

        self.mySystemI.supplicant_log_save(input)
        count_after = Erad_Radius_Log().AccountGroupId_index.count(bar_index_query)
        assert count_after == 3

        # Checking data
        entry = next(Erad_Radius_Log().AccountGroupId_index.query(
            bar_index_query,
            Erad_Radius_Log.EventTimestamp=="5")
        )
        assert entry.Group_ID == "bar"
        assert entry.Group_Name == "bar"

    def test_supplicant_acct_log_save(self):
        """
        if global site (Global Group_ID or Global port for the endpoint), go through authenticator and then look at group..
        otherwise go straight to group

        """
        log_record_template = '''{{
  "Timestamp": "{timestamp}",
  "Acct-Authentic": "RADIUS",
  "Acct-Link-Count": "40",
  "Acct-Multi-Session-Id": "00000000000000000000000000000000001a",
  "Acct-Session-Id": "5A6B6F44-2C0E4000",
  "Acct-Status-Type": "Start",
  "Called-Station-Id": "aa-bb-cc-dd-ee-ff:foo",
  "Calling-Station-Id": "00-00-00-74-AB-5D",
  "Class": "0x434143533a4844514e4341435330332f3238363536343833362f34373830383935",
  "Connect-Info": "CONNECT 802.11a/n/ac",
  "Event-Timestamp": "Jan  1 2018 18:11:12 UTC",
  "Framed-IP-Address": "10.0.0.1",
  "NAS-IP-Address": "10.0.1.1",
  "NAS-Identifier": "{nas_id}",
  "NAS-Port": "2",
  "NAS-Port-Type": "Wireless-802.11",
  "Proxy-State": "0x323334",
  "Ruckus-BSSID": "0xaabbccddeeff",
  "Ruckus-Location": "Floor 01",
  "Ruckus-SCG-CBlade-IP": "1",
  "Ruckus-SCG-DBlade-IP": "2",
  "Ruckus-SSID": "test_ssid",
  "Ruckus-VLAN-ID": "450",
  "Supplicant-Radius-Ip": "52.26.34.201",
  "Supplicant-Radius-Port": "{port}",
  "User-Name": "testing"
}}'''

        global_port = "20000"
        non_global_port = "20014"
        bogus_auth = "11:11:11:11:11:11"
        global_auth = "BB:55:44:33:22:11"
        nas_identifier = "66:55:44:33:22:11"

        input = self.get_input_system_key_auth_sup(bogus_auth, "testing")
        global_input = log_record_template.format(port=global_port, nas_id=bogus_auth, timestamp=16036946800000)
        input.update({"Logs": global_input})

        count_before = Erad_AccountingLogs().count("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=="None")
        assert count_before == 0

        self.mySystemI.supplicant_acct_log_save(input)
        count_after = Erad_AccountingLogs().count("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=='None')
        assert count_after == 1
        entry = next(Erad_AccountingLogs().query("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=='None'))
        assert entry.Log_Content == global_input

        input = self.get_input_system_key_auth_sup(global_auth, "globaltest")
        global_input = log_record_template.format(port=global_port, nas_id=global_auth, timestamp=16036946800000)
        input.update({"Logs": global_input})

        count_before = Erad_AccountingLogs().count("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=="None")
        assert count_before == 1
        count_before = Erad_AccountingLogs().count("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=="Global")
        assert count_before == 0

        self.mySystemI.supplicant_acct_log_save(input)
        count_after = Erad_AccountingLogs().count("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=='Global')
        assert count_after == 1
        entry = next(Erad_AccountingLogs().query("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=='Global'))
        assert entry.Log_Content == global_input

        # Not global but on global port
        input = self.get_input_system_key_auth_sup(nas_identifier, "testing")
        log_input = log_record_template.format(port=global_port, nas_id=nas_identifier, timestamp=16036946800000)
        input.update({"Logs": log_input})

        count_before = Erad_AccountingLogs().count("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=="bar")
        assert count_before == 0

        self.mySystemI.supplicant_acct_log_save(input)
        count_after = Erad_AccountingLogs().count("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=='bar')
        assert count_after == 1
        entry = next(Erad_AccountingLogs().query("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=='bar'))
        assert entry.Log_Content == log_input


        # Not global nor Global port, should not consider authenticator
        input = self.get_input_system_key_auth_sup(bogus_auth, "testing")
        log_input = log_record_template.format(port=non_global_port, nas_id=bogus_auth, timestamp=16036946800000)
        input.update({"Logs": log_input})

        count_before = Erad_AccountingLogs().count("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=="bar")
        assert count_before == 1

        # Since we are adding new log, we need to update Timestamp so we can guarantee it will be the second log
        # when we query it for test
        log_input = log_record_template.format(port=non_global_port, nas_id=bogus_auth, timestamp=16036946810000)
        input.update({"Logs": log_input})

        self.mySystemI.supplicant_acct_log_save(input)
        count_after = Erad_AccountingLogs().count("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=='bar')

        assert count_after == 2
        entry = list(Erad_AccountingLogs().query("fooaccount", filter_condition=Erad_AccountingLogs.Group_ID=='bar'))[1]
        assert entry.Log_Content == log_input
        assert global_port not in entry.Log_Content
        assert non_global_port in entry.Log_Content

    def test_get_account_certs_dict(self):
        result_dict = self.mySystemI.get_account_certs_dict()
        expected = {
            'AccountLogin': None,
            '2l2dsrrqre': None,
            'elevenos': None,
            'Eleven Wireless': None,
            'fooaccount': None,
            'sd9a3r9j8u': None,
            'baraccount': None,
            'Eleven Wireless 2': None,
            'Eleven Group': None
        }
        self.assertEqual(list(result_dict.keys()).sort(), list(expected.keys()).sort())
        self.assertEqual(len([i for i in list(expected.values()) if i is not None]), 0)

    def test_find_endpoint_caching(self):
        """Checking that caching for endpoint method is working and is being updated"""

        endpoint = Erad_Endpoint(
            '10.0.0.1',
            Port="30000",
            Group_ID="g1",
            ClusterId="uswest-dev-01",
            Secret="fake",
            Region="us-west-1")
        endpoint.save()

        # monkey patching cache timout to do tests, original is 2 minutes or check ENDPOINT_QEURY_CACHE_TIMEOUT variable
        original_cache_timeout = self.mySystemI.find_endpoint.cache_timeout
        self.mySystemI.find_endpoint.cache_timeout = 2
        # first query wll cache response for 5 seconds
        result = self.mySystemI.find_endpoint("10.0.0.1", "30000")
        self.assertEqual(result.Group_ID, "g1")

        # quickly updating endpoint and query again to confirm that data in cache is stalled
        endpoint.Group_ID = 'g1_updated'
        endpoint.save()

        result = self.mySystemI.find_endpoint("10.0.0.1", "30000")
        self.assertEqual(result.Group_ID, "g1")

        # waiting for cache to expire
        time.sleep(3)

        result = self.mySystemI.find_endpoint("10.0.0.1", "30000")

        # breakpoint()
        self.assertEqual(result.Group_ID, "g1_updated")

        self.mySystemI.find_endpoint.cache_timeout = original_cache_timeout


if __name__ == '__main__':
    unittest.main()
