#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import requests
import json
from erad import util, CoreTest
from ..erad_model import *
from datetime import datetime, timedelta
from ..spy import addToSpy

class ApiTest(CoreTest):

    std_session = "7018129031-123123123-123123123123"
    std_session_tests = "erad-tests-session"
    std_session2 = "e8d27ad6-abba-4468-909e-3c025657c0ec"
    std_xptosession = "e8d27ad6-abba-4468-909e-3c025657cxpto"
    std_account_id = "elevenos"
    std_account_owner = 1
    std_lastused = util.utcnow()
    std_group = "XX-YYYY"
    std_group2 = "FF-1925"
    std_group3 = "AB-CD-1002"
    std_group_tests = "foo"
    std_groupToDelete = "group88888"
    std_authenticator = "FF:FF:FF:33:44:AB"
    std_authenticator2 = "FF:FF:FF:33:44:55"
    std_authenticator3 = "CC:BB:AA:33:44:55"
    std_radiusattribute = "some-radius-attribute"
    std_username = r"HOST/^.^/marrcorp.marriott.com"
    std_username_asterisk = r"HOST/*.*/asterisk.marrcorp.marrio\^tt.com"
    std_username_custom_data = "username_custom_data"
    std_username_match = "HOST/jim.mcgill/marrcorp.marriott.com"
    std_username_match_custom_data = "username_custom_data"
    std_username_expired = "exp.dude"

    std_system_key = util.erad_cfg().system_key[0]

    def get(self, path, input):
        return requests.get(
                util.erad_cfg().api.host + path,
                params=input)

    def get_expect(self, path, input, expected_response, code):
        r = self.get(path, input)
        addToSpy(path, r.request.method, input, r.status_code, r.json())
        if r.status_code != code and r.status_code == 500:
            self.assertEqual(r.status_code, code, "Unexpected 500 error: {0}".format(r.json() or ''))
        else:
            self.assertEqual(r.status_code, code)
        actual = r.json()
        if not expected_response is None:
            self.assert_is_superset(expected_response, actual)
        return actual

    def get_expect_200(self, path, input, expected_response=None):
        return self.get_expect(path, input, expected_response, 200)

    def json_post( self, path, input ):
        return requests.post(
                util.erad_cfg().api.host + path,
                headers={ 'Content-type': 'application/json' },
                data=json.dumps(input))

    def json_post_expect(self, path, input, expected_response, code):
        response = self.json_post(path, input)
        addToSpy(path, response.request.method, input, response.status_code, response.json())
        if response.status_code != code and response.status_code == 500:
            self.assertEqual(response.status_code, code, "Unexpected 500 error: {0}".format(response.json() or ''))
        else:
            self.assertEqual(response.status_code, code)
        actual = response.json()
        if not expected_response is None:
            self.assert_is_superset(expected_response, actual)
        return actual

    def json_post_expect_200( self, path, input, expected_response=None ):
        return self.json_post_expect( path, input, expected_response, 200 )

    def json_post_expect_400( self, path, input, expected_response=None ):
        return self.json_post_expect( path, input, expected_response, 400 )

    def json_post_expect_403( self, path, input, expected_response=None ):
        return self.json_post_expect( path, input, expected_response, 403 )

    def json_post_expect_500( self, path, input, expected_response=None ):
        return self.json_post_expect( path, input, expected_response, 500 )


    def setUp(self):

        # Standard Group 2
        model = Erad_Group2()
        model.Account_ID = self.std_account_id
        model.ID = self.std_group
        model.Name = "The ABC Hotel"
        model.RemoteServerUrl = "123.123.122.129:1812"
        model.SharedSecret = "Eleven Bacon"
        model.TimeZone = "Asia/Qyzylorda"
        model.save()

        # other group2
        model = Erad_Group2()
        model.Account_ID = self.std_account_id
        model.ID = self.std_group2
        model.Name = "The XYZ Hotel"
        model.RemoteServerUrl = "55.44.33.22:800"
        model.SharedSecret = "XYZ Rules"
        model.TimeZone = "Asia/Qyzylorda"
        model.save()

        # other group2 3
        model = Erad_Group2()
        model.Account_ID = self.std_account_id
        model.ID = self.std_group3
        model.Name = "The Fun Times Hotel"
        model.RemoteServerUrl = "55.44.33.23:1812"
        model.SharedSecret = "Fun Rules"
        model.TimeZone = "America/Los Angeles"
        model.save()

        # group to delete
        model = Erad_Group2()
        model.Account_ID = self.std_account_id
        model.ID = self.std_groupToDelete
        model.Name = "Gonna get deleted"
        model.RemoteServerUrl = "adwad awd awd awd"
        model.SharedSecret = "w9jd9jdwj9dw"
        model.TimeZone = "Asia/Qyzylorda"
        model.save()

        # standard authenticator2
        model = Erad_Authenticator2()
        model.ID = self.std_authenticator
        model.Account_ID = 'elevenos'
        model.Group_ID = self.std_group
        model.RadiusAttribute = self.std_radiusattribute
        model.save()

        # authenticator2 for supplicant/find tests
        model = Erad_Authenticator2()
        model.ID = self.std_group3
        model.Account_ID = 'elevenos'
        model.Group_ID = self.std_group3
        model.RadiusAttribute = "nas-identifier"
        model.save()

        # standard supplicant
        model = Erad_Supplicant()
        model.Group_ID = self.std_account_id+"::"+self.std_group
        model.Account_ID = self.std_account_id
        model.Username = self.std_username
        model.Password = "aihjwd9awd098jawd9j8ad"
        model.Vlan = 1232
        model.UseRemote = False
        model.UseWildcard = True
        model.save()

        # standard supplicant with asterisk
        model = Erad_Supplicant()
        model.Group_ID = self.std_account_id+"::"+self.std_group
        model.Account_ID = self.std_account_id
        model.Username = self.std_username_asterisk
        model.Password = "aihjwd9awd098jawd9j8asterisk"
        model.Vlan = 1232
        model.UseRemote = False
        model.UseWildcard = True
        model.save()

        # standard supplicant with custom data
        model = Erad_Supplicant()
        model.Group_ID = self.std_account_id+"::"+self.std_group
        model.Account_ID = self.std_account_id
        model.Username = self.std_username_custom_data
        model.Password = "aihjwd9awd098jawd9j8ad"
        model.Vlan = 1232
        model.UseRemote = False
        model.UseWildcard = False
        model.CustomJsonData = "custom_json_data"
        model.save()

        # expired supplicant
        model = Erad_Supplicant()
        model.Group_ID = self.std_account_id+"::"+self.std_group3
        model.Account_ID = self.std_account_id
        model.Username = self.std_username_expired
        model.Password = "fooexpired"
        model.Vlan = 1024
        model.UseRemote = False
        model.UseWildcard = True
        model.ExpirationDate = (util.utcnow() - timedelta(hours=1))
        model.save()

        # standard session
        model = Erad_Session()
        model.ID = self.std_session
        model.Group_ID_List = [ self.std_group, self.std_group2, self.std_groupToDelete ]
        model.Identifier = "jimmy.mcgill"
        model.Account_ID = self.std_account_id
        model.AccountOwner = self.std_account_owner
        model.LogoutUrl = "http://1.1.1.1"
        model.LastUsed = self.std_lastused
        model.save()


        # session for tests
        model = Erad_Session()
        model.ID = self.std_session_tests
        model.Group_ID_List = [ self.std_group_tests ]
        model.Identifier = "jimmy.mcgill"
        model.Account_ID = self.std_account_id
        model.AccountOwner = self.std_account_owner
        model.LogoutUrl = "http://1.1.1.1"
        model.LastUsed = self.std_lastused
        model.save()

        # other session
        model = Erad_Session()
        model.ID = self.std_session2
        model.Account_ID = self.std_account_id
        model.Group_ID_List = [ self.std_group3 ]
        model.Identifier = "childish.gambino"
        model.LogoutUrl = "http://1.2.3.4"
        model.LastUsed = self.std_lastused
        model.save()

        # xptoaccount session
        model = Erad_Session()
        model.ID = self.std_xptosession
        model.Account_ID = 'xptoaccount'
        model.Group_ID_List = [ self.std_group3, self.std_group2 ]
        model.Identifier = "testidentifier"
        model.LogoutUrl = "http://1.2.3.4"
        model.LastUsed = self.std_lastused
        model.save()

        # erad apikey
        model = Erad_ApiKey()
        model.ApiKey_ID = self.std_integration_key_disabled
        model.ApiKeyName = "ElevenWirelessDisabled"
        model.Active = 0
        model.Account_ID = "elevenos"
        model.save()

        # erad apikey for delete
        model = Erad_ApiKey()
        model.ApiKey_ID = self.std_integration_key
        model.ApiKeyName = "ElevenWireless"
        model.Active = 1
        model.Account_ID = "elevenos"
        model.save()



