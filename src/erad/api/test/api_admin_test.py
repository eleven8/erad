#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from .api_test import *
from erad import util
from datetime import timedelta
from ..auditor import Auditor
import time
import datetime
import random

import base64
from OpenSSL import crypto

#---------- Administration Services ----------
class ApiAdminTest(ApiTest):
    maxDiff = None

    def test_admin_session_load(self):
        self.json_post_expect_200(
            '/erad/admin/session/load',
            {
                "Session": { "ID": self.std_session }
            },
            {
                "Error": None,
                "Session":
                {
                    "ID": self.std_session,
                    "Account_ID": "elevenos",
                    "AccountOwner": 1,
                    "Identifier": "jimmy.mcgill",
                    "LogoutUrl": "http://1.1.1.1",
                    "Group_ID_List":
                    [
                        self.std_group2,
                        self.std_group,
                        self.std_groupToDelete
                    ]
                }
            })

    def test_admin_audit_search(self):

        time.sleep( 1 )

        start_time = util.utcnow()

        session_id = "82828282-b8f2-4c55-9433-171661761"
        group_id = "scooby"
        identifier = "audittesterguy"
        username = "fred.flintstone"
        username_custom_data = username + "_custom_data"

        self.json_post_expect_200(
            '/erad/integration/session/save',
            {
                "IntegrationKey": self.std_integration_key,
                "Session":
                {
                    "Group_ID_List": [ group_id ],
                    "ID": session_id,
                    "Identifier":identifier,
                    "AccountOwner":0,
                    "Account_ID": "elevenos",
                    "LogoutUrl": "logout.fake.com"
                }
            },
            {
                "Error": None,
                "Session":
                {
                    "Group_ID_List": [ group_id ],
                    "ID": session_id,
                    "Identifier": identifier
                }
            })

        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": session_id },
                "Group":
                {
                    "ID": group_id,
                    "RemoteServerUrl": "1.1.1.1:800",
                    "SharedSecret": "Eleven",
                }
            },
            {
                "Error": None,
                "Group": {
                    "Account_ID": "elevenos",
                    "ID": group_id,
                    "SharedSecret" : "Eleven",
                    "RemoteServerUrl" : "1.1.1.1:800"
                }
            })

        # test optional fields
        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": session_id },
                "Group":
                {
                    "ID": group_id,
                    "RemoteServerUrl": "1.1.1.1:800",
                    "SharedSecret": "Eleven",
                    "OverrideVlan": True,
                    "OverrideConnectionSpeed": True,
                }
            },
            {
                "Error": None,
                "Group": {
                    "Account_ID": "elevenos",
                    "ID": group_id,
                    "SharedSecret" : "Eleven",
                    "RemoteServerUrl" : "1.1.1.1:800",
                    "OverrideVlan": True,
                    "OverrideConnectionSpeed": True,
                }
            })

        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": session_id },
                "Group":
                {
                    "ID": group_id,
                    "RemoteServerUrl": "1.1.1.1:800",
                    "SharedSecret": "Eleven",
                    "OverrideVlan": False,
                    "OverrideConnectionSpeed": False,
                }
            },
            {
                "Error": None,
                "Group": {
                    "Account_ID": "elevenos",
                    "ID": group_id,
                    "SharedSecret" : "Eleven",
                    "RemoteServerUrl" : "1.1.1.1:800",
                    "OverrideVlan": False,
                    "OverrideConnectionSpeed": False,
                }
            })

        # test what Mariot and ElevenOS settings are filtered
        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": session_id },
                "Group":
                {
                    "ID": group_id,
                    "RemoteServerUrl": "",
                    "SharedSecret": "Eleven",
                    "RemoteServer" : "ElevenOS"
                }
            },
            {
                "Error": None,
                "Group": {
                    "Account_ID": "elevenos",
                    "ID": group_id,
                    "SharedSecret" : "",
                    "RemoteServerUrl" : "",
                    "RemoteServer" : "ElevenOS",
                }
            })

        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": session_id },
                "Group":
                {
                    "ID": group_id,
                    "RemoteServerUrl": "",
                    "SharedSecret": "test_secret",
                    "RemoteServer" : "Marriott"
                }
            },
            {
                "Error": None,
                "Group": {
                    "Account_ID": "elevenos",
                    "ID": group_id,
                    "SharedSecret" : "",
                    "RemoteServerUrl" : "",
                    "RemoteServer" : "Marriott"
                }
            })
        # ---

        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": session_id },
                "Supplicant":
                {
                    "Group_ID": group_id,
                    "Username": username,
                    "Password": "11111",
                    "UseRemote": False,
                    "Vlan": 999
                }
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": 'elevenos::'+group_id,
                    "Username": username,
                    "Password": "11111",
                    "UseRemote": False,
                    "Vlan": 999
                }
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": session_id },
                "Supplicant":
                {
                    "Group_ID": group_id,
                    "Username": username_custom_data,
                    "Password": "11111",
                    "UseRemote": False,
                    "Vlan": 999,
                    "DeviceType": "Apple"
                }
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": 'elevenos::'+group_id,
                    "Username": username_custom_data,
                    "Password": "11111",
                    "UseRemote": False,
                    "Vlan": 999,
                }
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/delete',
            {
                "Session": { "ID": session_id },
                "Supplicant":
                {
                    "Group_ID": group_id,
                    "Username": username,
                }
            },
            {
                "Error": None
            })

        end_time = util.utcnow()

        self.json_post_expect_200(
            '/erad/admin/audit/search',
            {
                "Session": { "ID": session_id },
                "Group": {"ID": group_id },
                "AfterDate": util.utc_isoformat( start_time - timedelta(seconds=1) ),
                "BeforeDate": util.utc_isoformat( end_time + timedelta(seconds=1) )
            },
            {
                "Audit_List": [
                    {
                        "Group_ID": group_id,
                        "Identifier": identifier,
                        "Message": "Delete Supplicant '{0}'".format( username )
                    },
                    {
                        "Group_ID": group_id,
                        "Identifier": identifier,
                        "Message": "Save Supplicant '{0}'".format( username_custom_data  )
                    },
                    {
                        "Group_ID": group_id,
                        "Identifier": identifier,
                        "Message": "Save Supplicant '{0}'".format( username )
                    },
                    {
                        "Group_ID": group_id,
                        "Identifier": identifier,
                        "Message": "Edit Group"
                    },
                    {
                        "Group_ID": group_id,
                        "Identifier": identifier,
                        "Message": "Edit Group"
                    },
                    {
                        "Group_ID": group_id,
                        "Identifier": identifier,
                        "Message": "Edit Group"
                    },
                    {
                        "Group_ID": group_id,
                        "Identifier": identifier,
                        "Message": "Edit Group"
                    },
                    {
                        "Group_ID": group_id,
                        "Identifier": identifier,
                        "Message": "Edit Group"
                    },
                    {
                        "Group_ID": group_id,
                        "Identifier": identifier,
                        "Message": "Create Session"
                    }
                ],
                "Count": 9,
                "Error": None
            })

    def test_admin_group_list(self):
        self.json_post_expect_200(
            '/erad/admin/group/list',
            {
                "Session": { "ID": self.std_session }
            },
            {
                "Error": None,
                "Group_List":
                [
                    {
                        "Account_ID": self.std_account_id,
                        "ID": self.std_groupToDelete,
                        "Name": "Gonna get deleted",
                        "RemoteServerUrl": "adwad awd awd awd",
                        "SharedSecret": "w9jd9jdwj9dw",
                        "TimeZone": "Asia/Qyzylorda"
                    },
                    {
                        "Account_ID": self.std_account_id,
                        "ID": self.std_group,
                        "Name": "The ABC Hotel",
                        "RemoteServerUrl": "123.123.122.129:1812",
                        "SharedSecret": "Eleven Bacon",
                        "TimeZone": "Asia/Qyzylorda"
                    },
                    {
                        "Account_ID": self.std_account_id,
                        "ID": self.std_group2,
                        "Name": "The XYZ Hotel",
                        "RemoteServerUrl": "55.44.33.22:800",
                        "SharedSecret": "XYZ Rules",
                        "TimeZone": "Asia/Qyzylorda"
                    }
                ]
            })

    def test_admin_group_load(self):
        self.json_post_expect_200(
            '/erad/admin/group/load',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "Account_ID": self.std_account_id,
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon",
                    "TimeZone": "Asia/Qyzylorda"
                }
            })

    def test_admin_group_load_filtered(self):

        model_eleven = Erad_Group2()
        model_eleven.Account_ID = self.std_account_id
        model_eleven.ID = "test_group_eleven"
        model_eleven.Name = "Gonna get deleted"
        model_eleven.RemoteServerUrl = "52.41.63.155:1812"
        model_eleven.SharedSecret = "test_shared_secret"
        model_eleven.TimeZone = "Asia/Qyzylorda"
        model_eleven.save()

        model_mariot = Erad_Group2()
        model_mariot.Account_ID = self.std_account_id
        model_mariot.ID = "test_group_mariot"
        model_mariot.Name = "Gonna get deleted"
        model_mariot.RemoteServerUrl = "159.166.57.12:1812"
        model_mariot.SharedSecret = "test_shared_secret"
        model_mariot.TimeZone = "Asia/Qyzylorda"
        model_mariot.save()

        model_em_s = Erad_Session()
        model_em_s.ID = "test_session_eleven_mariot"
        model_em_s.Group_ID_List = [ "test_group_eleven", "test_group_mariot",  ]
        model_em_s.Identifier = "jimmy.mcgill"
        model_em_s.Account_ID = self.std_account_id
        model_em_s.AccountOwner = self.std_account_owner
        model_em_s.LogoutUrl = "http://1.1.1.1"
        model_em_s.LastUsed = self.std_lastused
        model_em_s.save()

        self.json_post_expect_200(
            '/erad/admin/group/load',
            {
                "Session": { "ID": "test_session_eleven_mariot" },
                "Group":
                {
                    "ID": "test_group_eleven"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "Account_ID": self.std_account_id,
                    "ID": "test_group_eleven",
                    "Name": "Gonna get deleted",
                    "RemoteServerUrl": "",
                    "SharedSecret": "",
                    "TimeZone": "Asia/Qyzylorda",
                    "RemoteServer" : "ElevenOS"
                }
            })

        self.json_post_expect_200(
            '/erad/admin/group/load',
            {
                "Session": { "ID": "test_session_eleven_mariot" },
                "Group":
                {
                    "ID": "test_group_mariot"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "Account_ID": self.std_account_id,
                    "ID": "test_group_mariot",
                    "Name": "Gonna get deleted",
                    "RemoteServerUrl": "",
                    "SharedSecret": "",
                    "TimeZone": "Asia/Qyzylorda",
                    "RemoteServer" : "Marriott"
                }
            })

        model_eleven.delete()
        model_mariot.delete()
        model_em_s.delete()



    def test_admin_group_save(self):
        self.json_post_expect_400(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session2 },
                "Group":
                {
                    "Account_ID": "elevenos",
                    "ID": self.std_group3,
                    "Name": "TEST",
                    "RemoteServerUrl": "55.44.33.23:1812",
                    "SharedSecret": "Fun Rules"
                }
            },
            {
                "Error": "Request Denied. Parameter 'Name' is restricted."
            })
        # check if the timezone with space in the name works and test case insensitive
        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session2 },
                "Group":
                {
                    "Account_ID": "elevenos",
                    "ID": self.std_group3,
                    "RemoteServerUrl": "55.44.33.23:1812",
                    "SharedSecret": "Fun Rules",
                    "TimeZone": "America/los_aNGeles"
                }
            },
            {
                "Error": None,
                "Group": {
                    "Account_ID": "elevenos",
                    "ID": self.std_group3,
                    "Name": "The Fun Times Hotel",
                    "RemoteServerUrl": "55.44.33.23:1812",
                    "SharedSecret": "Fun Rules",
                    "TimeZone": "America/Los Angeles"
                }
            })

        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group,
                    "Name": "The ABC Hotel Modified",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                }
            },
            {
                "Error": None,
                "Group": {
                    "Account_ID": "elevenos",
                    "ID": self.std_group,
                    "Name": "The ABC Hotel Modified",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon",
                    "TimeZone": "Asia/Qyzylorda"
                }
            })

        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group,
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "Account_ID": "elevenos",
                    "ID": self.std_group,
                    "Name": "The ABC Hotel Modified",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                }
            })

    def test_admin_group_save_default(self):
        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group,
                    "RemoteServerUrl": "123.123.122.129:",
                    "SharedSecret": "Eleven Bacon"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "Account_ID": "elevenos",
                    "ID": self.std_group,
                    "RemoteServerUrl": "123.123.122.129:1812",
                }
            })
    def test_admin_group_delete(self):
        self.json_post_expect_403(
            '/erad/admin/group/delete',
            {
                "Session": { "ID": self.std_session + "2" }, # modify key
                "Group":
                {
                    "ID": self.std_groupToDelete
                }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/admin/group/delete',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_groupToDelete
                }
            },
            {
                "Error": None
            })

    def test_admin_supplicant_list(self):
        self.json_post_expect_200(
            '/erad/admin/supplicant/list',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group
                }
            },
            {
                "Error": None,
                "Supplicant_List":
                [
                    {
                        "Group_ID": self.std_account_id+"::"+self.std_group,
                        "Username": "08:00:0F:4F:DA:3A",
                        "Password": "08:00:0F:4F:DA:3A",
                        "UseRemote": False,
                        "UseWildcard": False,
                        "Vlan": None
                    },
                    {
                        "Group_ID": self.std_account_id+"::"+self.std_group,
                        "Username": self.std_username_asterisk,
                        "Password": "aihjwd9awd098jawd9j8asterisk",
                        "UseRemote": False,
                        "UseWildcard": True,
                        "Vlan": 1232
                    },
                    {
                        "Group_ID": self.std_account_id+"::"+self.std_group,
                        "Username": self.std_username,
                        "Password": "aihjwd9awd098jawd9j8ad",
                        "UseRemote": False,
                        "UseWildcard": True,
                        "Vlan": 1232
                    },
                    {
                        "Group_ID": self.std_account_id+"::"+self.std_group,
                        "Username": self.std_username_custom_data,
                        "Password": "aihjwd9awd098jawd9j8ad",
                        "UseRemote": False,
                        "UseWildcard": False,
                        "Vlan": 1232
                    }
                ]
            })

    def test_admin_supplicant_export_csv(self):
        r = requests.get(util.erad_cfg().api.host + "/erad/admin/supplicant/export/csv?Session_ID={0}&Group_ID={1}".format( self.std_session, self.std_group ))
        self.assertEqual( r.status_code, 200 )
        self.assertEqual( r.text,
'Username,Password,Vlan,UseRemote,DeviceName,Description,Location,ExpirationDate,CustomJsonData,MaxDownloadSpeedBits,MaxUploadSpeedBits,Parent_ID,Account_ID,profile_used,device_used\r\n\
HOST/*.*/asterisk.marrcorp.marrio\\^tt.com,aihjwd9awd098jawd9j8asterisk,1232,False,,,,,,,,,elevenos,,\r\n\
HOST/^.^/marrcorp.marriott.com,aihjwd9awd098jawd9j8ad,1232,False,,,,,,,,,elevenos,,\r\n\
username_custom_data,aihjwd9awd098jawd9j8ad,1232,False,,,,,custom_json_data,,,,elevenos,,\r\n')

        self.assertEqual(r.headers['Content-type'], 'text/csv')

        self.get_expect(
            '/erad/admin/supplicant/export/csv',
            {
                'Session_ID': '123',
                'Group_ID': 'the_group'
            },
            None,
            403
            )

        self.get_expect(
            '/erad/admin/supplicant/export/csv',
            {},
            {
                "Error": "Required parameter 'Session_ID' is missing."
            },
            400
            )


        response = requests.get(util.erad_cfg().api.host + '/erad/admin/supplicant/export/csv'
            '?Session_ID=7018129031-123123123-123123123123')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json()['Error'], '''Required parameter 'Group_ID' is missing.''')

    def test_admin_supplicant_import_csv(self):
        # grab the initial state of the supplicant list
        initial = self.json_post(
            '/erad/admin/supplicant/list',
            {
                "Session": {"ID": self.std_session},
                "Group": {"ID": self.std_group}
            }).json()

        # export current list to CSV
        csv_text = requests.get(
            util.erad_cfg().api.host + "/erad/admin/supplicant/export/csv?Session_ID={0}&Group_ID={1}".format(
                self.std_session, self.std_group)).text

        # delete the supplicant
        self.json_post_expect_200(
            '/erad/admin/supplicant/delete',
            {
                "Session": {"ID": self.std_session},
                "Supplicant":
                    {
                        "Group_ID": self.std_group,
                        "Username": "HOST/^.^/marrcorp.marriott.com"
                    }
            },
            {
                "Error": None
            })

        # import the exported list and make sure it saved
        r = requests.post(
            util.erad_cfg().api.host + "/erad/admin/supplicant/import/csv?Session_ID={0}&Group_ID={1}".format(
                self.std_session, self.std_group),
            files={"file": csv_text})
        self.assertEqual(r.status_code, 200)

        # we expect the list hasn't changed
        self.json_post_expect_200(
            '/erad/admin/supplicant/list',
            {
                "Session": {"ID": self.std_session},
                "Group": {"Account_ID": "elevenos", "ID": self.std_group}
            },
            initial)

        mac_supplicant_str = "Username,Password,Vlan,UseRemote,DeviceName,Description,Location,ExpirationDate\n" \
                             "08:00:0F:4F:DA:3A,08:00:0F:4F:DA:3A,,,,,,"

        url_path = "/erad/admin/supplicant/import/csv?Session_ID={0}&Group_ID={1}".format(
            self.std_session,
            self.std_group
        )
        request = requests.post(util.erad_cfg().api.host + url_path, files={"file": mac_supplicant_str})
        self.assertEqual(request.status_code, 200)

        # self generating documentation test
        # not allowed send files, so test only errors
        self.json_post_expect_400(
            '/erad/admin/supplicant/import/csv',
            {

            },
            {
                "Error": "Malformed request"
            })



    # Excel tends to reformat 'True' and 'False' to 'TRUE' and 'FALSE'
    def test_admin_supplicant_import_csv_from_excel(self):
        test_session = "session-test_admin_supplicant_import_csv_from_excel"
        test_group = "group-test_admin_supplicant_import_csv_from_excel"
        self.json_post(
            '/erad/integration/session/save',
            {
                "IntegrationKey": self.std_integration_key,
                "Session":
                {
                    "Group_ID_List": [ test_group ],
                    "ID": test_session,
                    "Identifier":"awdaw",
                    "AccountOwner":0,
                    "LogoutUrl": "logout.fake.com"
                }
            })
        self.json_post(
            '/erad/admin/group/save',
            {
                "Session": { "ID": test_session },
                "Group":
                {
                    "Account_ID": "elevenos",
                    "ID": test_group,
                    "RemoteServerUrl": "1.1.1.1:800",
                    "SharedSecret": "Eleven"
                }
            })

        csv_text = (
"Username,Password,Vlan,UseRemote,DeviceName,Description,Location,ExpirationDate,CustomJsonData\r\n\
excelfileguy,pwd,1232,FALSE,,,,,custom data\r\n\
excelfileguy2,pwd,1232,TRUE,,,,,\r\n\
excelfileguy3,pwd,1232,,,,,,\r\n\
excelfileguy4,pwd,1232,true,,,,,\r\n\
excelfileguy5,pwd,1232,fALSE,,,,,\r\n")
        r = requests.post( util.erad_cfg().api.host + "/erad/admin/supplicant/import/csv?Session_ID={0}&Group_ID={1}".format( test_session, test_group ),
                           files={"file": csv_text})
        expected_response = (
            {
                "Error": None
            })
        self.assert_is_superset( expected_response, r.json() )

    def test_admin_supplicant_find_by_parent(self):
        Erad_Group2.save(
            Erad_Group2(
                Account_ID="elevenos",
                ID="parentapi",
                Name="parentGroup1",
                RemoteServerUrl="1.2.3.4:50",
                SharedSecret="321cba",
                TimeZone="Asia/Qyzylorda",
                MaxDownloadSpeedBits=0,
                MaxUploadSpeedBits=0
            )
        )

        Erad_Supplicant.save(
            Erad_Supplicant(
                Group_ID="elevenos::parentapi",
                Password="passw0rd",
                UseRemote=False,
                UseWildcard=False,
                Username="user_parent",
                Vlan=888,
                Parent_ID="test-supplicant-parent-id-api",
                Account_ID="elevenos"
            )
        )

        # Valid session and group
        Erad_Session.save(
            Erad_Session(
                ID=str("unit_test_session_parent_api"),
                Group_ID_List=["parentapi"],
                Account_ID='elevenos',
                Identifier="jimmy.mcgill",
                LastUsed=util.utcnow()
            )
        )

        self.json_post_expect_200(
            '/erad/admin/supplicant/find-by-parent-id',
            {
                "Session": { "ID": "unit_test_session_parent_api" },
                "Supplicant":
                {
                    "Group_ID": "parentapi",
                    "Parent_ID": "test-supplicant-parent-id-api"
                }
            },
            {
                "Supplicants": [
                    {
                        'Account_ID': 'elevenos',
                        'CustomJsonData': None,
                        'Description': None,
                        'DeviceName': None,
                        'ExpirationDate': None,
                        'Group_ID': 'elevenos::parentapi',
                        'Location': None,
                        'MaxDownloadSpeedBits': None,
                        'MaxUploadSpeedBits': None,
                        'Parent_ID': 'test-supplicant-parent-id-api',
                        'Password': 'passw0rd',
                        'UseRemote': False,
                        'UseWildcard': False,
                        'Username': 'user_parent',
                        'Vlan': 888,
                        'device_used': None,
                        'profile_used': None
                    }
                ]
            })

        sess = Erad_Session.get("unit_test_session_parent_api")
        sess.delete()

        group = Erad_Group2.get("elevenos", "parentapi")
        group.delete()

    def test_admin_supplicant_load(self):
        self.json_post_expect_200(
            '/erad/admin/supplicant/load',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username
                }
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "UseWildcard": True,
                    "Vlan": 1232
                }
            })

    def test_admin_supplicant_save(self):
        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,

                    # optional fields
                    "Description": None,
                    "DeviceName": None,
                    "Location": None,
                    "ExpirationDate": None,
                }
            })

    def test_admin_supplicant_certificate(self):
        post_result = self.json_post(
            '/erad/admin/supplicant/certificate',
            {
                "Session": { "ID": self.std_session },
                "NoPassword" : "true",
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232
                },
                "Site_Group_ID": "test-group"
            })

        json_data = post_result.json()
        self.assertEqual(json_data['Error'], None)
        self.assertEqual(len(json_data['Certificate']) > 0, True)

        decoded_cert = base64.decodebytes(json_data['Certificate'].encode())

        p12 = crypto.load_pkcs12(decoded_cert, "")
        extracted_cert = p12.get_certificate()

        self.assertEqual(extracted_cert.digest('sha256'), json_data['Sha256-fingerprint'].encode())

        self.json_post_expect_400(
            '/erad/admin/supplicant/certificate',
            {

            },
            {
                "Error": "Required parameter 'Supplicant' is missing."
            })


        self.json_post_expect_200(
            '/erad/admin/supplicant/certificate',
            {
                "Supplicant": {
                    "Group_ID": "foo",
                    "Username": "testing",
                    "Password": "testing",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "Description": "I am a description",
                    "DeviceName": "Steve's Machine",
                    "Location": "Conference Room",
                    "ExpirationDate": None
                },
                "Session": { "ID":  "erad-tests-session" },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None
            })

        post_response =self.json_post(
           '/erad/admin/supplicant/certificate',
           {
                "Supplicant": {
                    "Group_ID": "foo",
                    "Username": "testing",
                    "Password": "testing",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "Description": "I am a description",
                    "DeviceName": "Steve's Machine",
                    "Location": "Conference Room",
                    "ExpirationDate": None
                },
                "Session": { "ID":  "erad-tests-session" },
                "Site_Group_ID": "test-group"
            }
        )

        self.assertEqual(post_response.status_code, 200)

        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)
        self.assertEqual(len(response_data['Sha256-fingerprint']), 95)
        self.assertTrue(len(response_data['Certificate']) > 10)


        self.json_post_expect_403(
            '/erad/admin/supplicant/certificate',
            {
                "Supplicant": {
                    "Group_ID": "foo",
                    "Username": "testing",
                    "Password": "testing",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "Description": "I am a description",
                    "DeviceName": "Steve's Machine",
                    "Location": "Conference Room",
                    "ExpirationDate": None
                },
                "Session": { "ID":  "wrong-session" },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": "Access Denied.",
                "RedirectURL": "https://admin.enterpriseauth.com/#/login"
            })

        self.json_post_expect_403(
            '/erad/admin/supplicant/certificate',
            {
                "Supplicant": {
                    "Group_ID": "not-existing-group",
                    "Username": "testing",
                    "Password": "testing",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "Description": "I am a description",
                    "DeviceName": "Steve's Machine",
                    "Location": "Conference Room",
                    "ExpirationDate": None
                },
                "Session": { "ID":  "erad-tests-session" },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": "Access Denied.",
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/certificate',
            {
                "Supplicant": {
                    "Group_ID": "foo",
                    "Username": "not-existing-user",
                    "Password": "testing",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "Description": "I am a description",
                    "DeviceName": "Steve's Machine",
                    "Location": "Conference Room",
                    "ExpirationDate": None
                },
                "Session": { "ID":  "erad-tests-session" },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": "This item does not exist in the table."
            })



    def test_admin_supplicant_save_mac_like(self):
        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": 'aa-bb-cc-dd-ee-ff',
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": 'aa-bb-cc-dd-ee-ff',
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,

                    # optional fields
                    "Description": None,
                    "DeviceName": None,
                    "Location": None,
                    "ExpirationDate": None
                }
            })

    def test_admin_supplicant_save_optional(self):
        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "Description": "I am a description",
                    "DeviceName": "Steve's Machine",
                    "Location": "Conference Room",
                    "ExpirationDate": "2014-06-01T16:00:00Z",

                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "Description": "I am a description",
                    "DeviceName": "Steve's Machine",
                    "Location": "Conference Room",
                    "ExpirationDate": "2014-06-01T16:00:00Z",

                }
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "Description": "I am a description",
                    "DeviceName": "Steve's Machine",
                    "Location": "Conference Room",
                    "ExpirationDate": None,
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "Description": "I am a description",
                    "DeviceName": "Steve's Machine",
                    "Location": "Conference Room",
                    "ExpirationDate": None,
                }
            })

    def test_admin_supplicant_save_optional_none(self):
        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": 'optional',
                    "Password": 'optional',
                    "UseRemote": False,

                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": 'optional',
                    "Password": 'optional',
                    "UseRemote": False,
                    "Vlan": None,
                    "MaxDownloadSpeedBits": None,
                    "MaxUploadSpeedBits": None
                }
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": 'optional',
                    "Password": 'optional',
                    "UseRemote": False,
                    "Vlan": 1,
                    "MaxDownloadSpeedBits": 1,
                    "MaxUploadSpeedBits": 1
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": 'optional',
                    "Password": 'optional',
                    "UseRemote": False,
                    "Vlan": 1,
                    "MaxDownloadSpeedBits": 1,
                    "MaxUploadSpeedBits": 1
                }
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": 'optional',
                    "Password": 'optional',
                    "UseRemote": False,
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": 'optional',
                    "Password": 'optional',
                    "UseRemote": False,
                    "Vlan": 1,
                    "MaxDownloadSpeedBits": 1,
                    "MaxUploadSpeedBits": 1
                }
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": 'optional',
                    "Password": 'optional',
                    "UseRemote": False,
                    "Vlan": None,
                    "MaxDownloadSpeedBits": 0,
                    "MaxUploadSpeedBits": 0
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": 'optional',
                    "Password": 'optional',
                    "UseRemote": False,
                    "Vlan": None,
                    "MaxDownloadSpeedBits": 0,
                    "MaxUploadSpeedBits": 0
                }
            })

        self.json_post_expect_400(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": 'optional',
                    "Password": 'optional',
                    "UseRemote": False,
                    "MaxDownloadSpeedBits": None,
                    "MaxUploadSpeedBits": None
                },
                "Site_Group_ID": "test-group"
            })



        self.json_post_expect_200(
            '/erad/admin/supplicant/delete',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": 'optional',
                }
            },
            {
                "Error": None
            })


    def test_admin_supplicant_save_already_exists(self):
        """ Testing that ERAD API will not allow save supplicant, it there exists
        supplicant with the same name but different group.
        """
        model_em_s = Erad_Session()
        model_em_s.ID = "test_duplicate_session"
        model_em_s.Group_ID_List = [ "Global", "abcd", "other_group_abcd" ]
        model_em_s.Identifier = "test_duplicate_id"
        model_em_s.Account_ID = self.std_account_id
        model_em_s.AccountOwner = self.std_account_owner
        model_em_s.LogoutUrl = "http://1.1.1.1"
        model_em_s.LastUsed = self.std_lastused
        model_em_s.save()


        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": 'test_duplicate_session' },
                "Supplicant":
                {
                    "Group_ID": 'Global',
                    "Username": 'test_duplicate',
                    "Password": 'test_duplicate',
                    "UseRemote": False,
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::Global",
                    "Username": 'test_duplicate',
                    "Password": 'test_duplicate',
                    "UseRemote": False,
                    "Vlan": None,
                }
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": 'test_duplicate_session' },
                "Supplicant":
                {
                    "Group_ID": 'abcd',
                    "Username": 'test_duplicate',
                    "Password": 'test_duplicate',
                    "UseRemote": False,
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": 'Supplicant already exists in the Global site.',
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/delete',
            {
                "Session": { "ID": 'test_duplicate_session' },
                "Supplicant":
                {
                    "Group_ID": 'Global',
                    "Username": 'test_duplicate',
                }
            },
            {
                "Error": None
            })


        # change order normal -> Global
        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": 'test_duplicate_session' },
                "Supplicant":
                {
                    "Group_ID": 'abcd',
                    "Username": 'test_duplicate',
                    "Password": 'test_duplicate',
                    "UseRemote": False,
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::abcd",
                    "Username": 'test_duplicate',
                    "Password": 'test_duplicate',
                    "UseRemote": False,
                    "Vlan": None,
                }
            })

        # should allow save supplicant with same username but in different non Global group
        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": 'test_duplicate_session' },
                "Supplicant":
                {
                    "Group_ID": 'other_group_abcd',
                    "Username": 'test_duplicate',
                    "Password": 'test_duplicate',
                    "UseRemote": False,
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::other_group_abcd",
                    "Username": 'test_duplicate',
                    "Password": 'test_duplicate',
                    "UseRemote": False,
                    "Vlan": None,
                }
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": 'test_duplicate_session' },
                "Supplicant":
                {
                    "Group_ID": 'Global',
                    "Username": 'test_duplicate',
                    "Password": 'test_duplicate',
                    "UseRemote": False,
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": 'Supplicant already exists.',
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/delete',
            {
                "Session": { "ID": 'test_duplicate_session' },
                "Supplicant":
                {
                    "Group_ID": 'abcd',
                    "Username": 'test_duplicate',
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None
            })

        model_em_s.delete()

    def test_admin_supplicant_save_expiration_good(self):
        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "ExpirationDate": "2015-10-29T09:30:00Z"
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "ExpirationDate": "2015-10-29T09:30:00Z"
                }
            })

    def test_admin_supplicant_save_expiration_okay(self):
        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "ExpirationDate": "  2015-06-04T20:00Z  " # ugly format allowed
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "ExpirationDate": "2015-06-04T20:00:00Z" # properly formatted
                }
            })

    def test_admin_supplicant_save_expiration_badtype(self):
        self.json_post_expect_400(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "ExpirationDate": 10
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": "Parameter 'ExpirationDate' is incorrect type (expected unicode string)."
            })

    def test_admin_supplicant_save_expiration_badformat(self):
        self.json_post_expect_400(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "ExpirationDate": "scoobydoo"
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": "Parameter 'ExpirationDate' is incorrect type (expected datetime string in ISO format)."
            })

    def test_admin_supplicant_save_expiration_whitespace(self):
        self.json_post_expect_200(
            '/erad/admin/supplicant/save',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "ExpirationDate": "  " # white space should be ignored
                },
                "Site_Group_ID": "test-group"
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "ExpirationDate": None
                }
            })

    def test_admin_supplicant_mac_save(self):
        self.json_post_expect_400(
            '/erad/admin/supplicant/mac/save',
            {
                "Session": {"ID": self.std_session2},
                "Supplicant":
                {
                    "Group_ID": self.std_group3,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232
                }
            },
            {
                "Error": "Required parameter 'MacAddress' is missing."
            })
        self.json_post_expect_200(
            '/erad/admin/supplicant/mac/save',
            {
                "Session": {"ID": self.std_session2},
                "Supplicant":
                {
                    "Group_ID": self.std_group3,
                    "MacAddress": "ff-ff-aa-12-34-00-88",
                    "UseRemote": False,
                    "Vlan": 1232
                }
            },
            {
                "Error": "Invalid MacAddress."
            })
        self.json_post_expect_200(
            '/erad/admin/supplicant/mac/save',
            {
                "Session": {"ID": self.std_session2},
                "Supplicant":
                {
                    "Group_ID": self.std_group3,
                    "MacAddress": "ff-ff-aa-12-34-00",
                    "UseRemote": False,
                    "Vlan": 1232
                }
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group3,
                    "Username": "FF:FF:AA:12:34:00",
                    "Password": "FF:FF:AA:12:34:00",
                    "UseRemote": False,
                    "Vlan": 1232
                }
            })
        self.json_post_expect_200(
            '/erad/admin/supplicant/delete',
            {
                "Session": { "ID": self.std_session2 },
                "Supplicant":
                {
                    "Group_ID": self.std_group3,
                    "Username": "FF:FF:AA:12:34:00"
                }
            },
            {
                "Error": None
            })
        self.json_post_expect_200(
            '/erad/admin/supplicant/mac/save',
            {
                "Session": { "ID": self.std_session2 },
                "Supplicant":
                {
                    "Group_ID": self.std_group3,
                    "MacAddress": "ff-ff-aa-^",
                    "UseRemote": False,
                    "UseWildcard": True,
                    "Vlan": 1232
                }
            },
            {
                "Error": None,
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group3,
                    "Username": "FF:FF:AA:^",
                    "Password": "FF:FF:AA:^",
                    "UseRemote": False,
                    "Vlan": 1232
                }
            })
        self.json_post_expect_200(
            '/erad/admin/supplicant/delete',
            {
                "Session": { "ID": self.std_session2 },
                "Supplicant":
                {
                    "Group_ID": self.std_group3,
                    "Username": "FF:FF:AA:12:34:00"
                }
            },
            {
                "Error": None
            })
        self.json_post_expect_200(
            '/erad/admin/supplicant/mac/save',
            {
                "Session": { "ID": self.std_session2 },
                "Supplicant":
                {
                    "Group_ID": self.std_group3,
                    "MacAddress": "ff-ff-a-^",
                    "UseRemote": False,
                    "Vlan": 1232
                }
            },
            {
                "Error": "Invalid MacAddress."
            })
        self.json_post_expect_200(
            '/erad/admin/supplicant/mac/save',
            {
                "Session": { "ID": self.std_session2 },
                "Supplicant":
                {
                    "Group_ID": self.std_group3,
                    "MacAddress": "ff-ff-ag-^",
                    "UseRemote": False,
                    "Vlan": 1232
                }
            },
            {
                "Error": "Invalid MacAddress."
            })

    def test_admin_supplicant_delete(self):
        # make supplicant to delete
        model = Erad_Supplicant()
        model.Group_ID = self.std_group
        model.Username = "walter.white"
        model.Password = "awwwwdw"
        model.Vlan = 1024
        model.UseRemote = True
        model.UseWildcard = False
        model.save()

        self.json_post_expect_403(
            '/erad/admin/supplicant/delete',
            {
                "Session": { "ID": self.std_session + "2" }, # modify session ID
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": "walter.white"
                }
            },
            {
                "Error": "Access Denied."
            })

        self.json_post_expect_200(
            '/erad/admin/supplicant/delete',
            {
                "Session": { "ID": self.std_session },
                "Supplicant":
                {
                    "Group_ID": self.std_group,
                    "Username": "walter.white"
                }
            },
            {
                "Error": None
            })

    def test_admin_supplicant_log_list(self):
        # TODO: consider adding two more instances, one before the before_date_instance
        # and one after the after_date_instance, to verify that specifying a time
        # interval is actually excluding those instances and not simply retrieving all
        # available instances

        # new Session with 2 Groups
        model = Erad_Session()
        model.ID = "xxx123zzz456"
        model.Group_ID_List = [ self.std_group, self.std_group2 ]
        model.Identifier = "jimmy.mcgill"
        model.Account_ID = self.std_account_id
        model.AccountOwner = self.std_account_owner
        model.LogoutUrl = "http://1.1.1.1"
        model.LastUsed = self.std_lastused
        model.save()

        # Objects for supplicant/log/list
        # Supplicant for std_group for Radius Logs
        model = Erad_Supplicant()
        model.Group_ID = self.std_account_id+"::"+self.std_group
        model.Username = "skywalker"
        model.Password = "aihjwd9awd098jawd9j8ad"
        model.Vlan = 1232
        model.UseRemote = False
        model.UseWildcard = True
        model.save()

        # Supplicant for std_group2 for Radius Logs
        model = Erad_Supplicant()
        model.Group_ID = self.std_account_id+"::"+self.std_group2
        model.Username = "nightwatchman"
        model.Password = "aihjwd9awd098jawd9j8ad"
        model.Vlan = 1232
        model.UseRemote = False
        model.UseWildcard = True
        model.save()

        # Authenticator for std_group
        model = Erad_Authenticator2()
        model.ID = self.std_authenticator
        model.Account_ID = 'elevenos'
        model.Group_ID = self.std_group
        model.RadiusAttribute = "called-station-id"
        model.save()

        # Authenticator for std_group2
        model = Erad_Authenticator2()
        model.ID = self.std_authenticator
        model.Account_ID = 'elevenos'
        model.Group_ID = self.std_group2
        model.RadiusAttribute = "called-station-id"
        model.save()

        # convert dates to instances in order to search with them on a interval of 12 hours
        now = util.utc_to_local( datetime.datetime.utcnow() + datetime.timedelta(hours=12), 'America/Los_Angeles' )
        before = util.utc_to_local( datetime.datetime.utcnow() - datetime.timedelta(hours=12), 'America/Los_Angeles' )

        after_date_instance =  Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(now), 0.0 )
        before_date_instance = Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(before), 0.0 )

        # make unique instances for each radius logs
        instance1 = Auditor().make_unique_instance()
        instance2 = Auditor().make_unique_instance()

        timestamp1 = datetime.datetime(2020, 1, 1, 0, 1, 2, 100).strftime("%b %d %Y %H:%M:%S UTC")
        timestamp2 = datetime.datetime(2020, 1, 1, 0, 1, 3, 100).strftime("%b %d %Y %H:%M:%S UTC")

        # Radius Logs
        model = Erad_Radius_Log()
        model.Group_ID = self.std_group
        model.Instance = instance1
        model.Group_Name = "The ABC Hotel"
        model.AccountGroup_ID = self.std_account_id+"::"+self.std_group
        model.PacketType = "Access-Request"
        model.Username = "skywalker"
        model.CalledStationId = self.std_authenticator
        model.EventTimestamp = timestamp1
        model.NASIpAddress = "52.35.24.232"
        model.Access = "Access-Accept"
        model.save()

        model = Erad_Radius_Log()
        model.Group_ID = self.std_group2
        model.Instance = instance2
        model.Group_Name = "The XYZ Hotel"
        model.AccountGroup_ID = self.std_account_id+"::"+self.std_group2
        model.PacketType = "Access-Request"
        model.Username = "nightwatchman"
        model.CalledStationId = self.std_authenticator
        model.EventTimestamp = timestamp2
        model.NASIpAddress = "52.35.24.232"
        model.Access = "Access-Accept"
        model.save()

        self.json_post_expect_403(
            '/erad/admin/supplicant/log/list',
            {
                "Session": { "ID": "xxx123zzz456" + "2" }, # modify session ID
                "Radius_Log":
                {
                    "Group_ID": self.std_group,
                    "Include_Stray": True,
                    "Instance_Interval": [before_date_instance, after_date_instance]
                }
            },
            {
                "Error": "Access Denied."
            })

        # Checking no logs exists in DB
        self.json_post_expect_200(
            '/erad/admin/supplicant/log/list',
            {
                "Session": { "ID": "7018129031-123123123-123123123123" }, # modify session ID
                "Radius_Log":
                {
                    "Group_ID": self.std_group,
                    "Include_Stray": False,
                    "Instance_Interval": [0, 1]
                }
            },
            {
                "Error": "Cannot read the radius logs from database"
            })

        # Now logs returned as streamed response checking that Content-Type is octet-stream
        response = requests.post(
            util.erad_cfg().api.host + '/erad/admin/supplicant/log/list',
            json={
                "Session": {"ID": "xxx123zzz456"},
                "Radius_Log":
                    {
                        "Group_ID": self.std_group,
                        "Include_Stray": True,
                        "Instance_Interval": [before_date_instance, after_date_instance]
                    }
            })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'application/octet-stream')

        self.assertTrue('Access-Accept' in response.text)
        self.assertTrue('Access-Request' in response.text)
        self.assertTrue('skywalker' in response.text)
        self.assertTrue('XX-YYYY' in response.text)
        self.assertTrue('52.35.24.232' in response.text)
        self.assertTrue(f'{self.std_account_id + "::" + self.std_group}' in response.text)

        # checking that request with ::all:: group working
        response = requests.post(
            util.erad_cfg().api.host + '/erad/admin/supplicant/log/list',
            json={
                "Session": { "ID": "xxx123zzz456"},
                "Radius_Log":
                {
                    "Group_ID": "::all::",
                    "Include_Stray": True,
                    "Instance_Interval": [before_date_instance, after_date_instance]
                }
            })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'application/octet-stream')

        self.assertTrue('Access-Accept' in response.text)
        self.assertTrue('Access-Request' in response.text)
        self.assertTrue('skywalker' in response.text)
        self.assertTrue('XX-YYYY' in response.text)
        self.assertTrue('52.35.24.232' in response.text)
        self.assertTrue(f'{self.std_account_id + "::" + self.std_group}' in response.text)

        # Test for second group
        self.assertTrue('nightwatchman' in response.text)
        self.assertTrue(self.std_group2 in response.text)
        self.assertTrue(f'{self.std_account_id + "::" + self.std_group2}' in response.text)

        # test export to csv
        Instance_To = int(time.time() + 10) * 10000000

        # # wrong Instance_To
        response = requests.get(
                util.erad_cfg().api.host + '/erad/admin/supplicant/log/export/csv'
                '?Session_ID=7018129031-123123123-123123123123'
                '&Group_ID=XX-YYYY'
                '&Include_Stray=false'
                '&Instance_From=0'
                '&Instance_To=1')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['Error'], "Cannot read the radius logs from database")

        # # wrong session
        response = requests.get(
                util.erad_cfg().api.host + '/erad/admin/supplicant/log/export/csv'
                '?Session_ID=7018129031-123123123-1231123123123'
                '&Group_ID=XX-YYYY'
                '&Include_Stray=false'
                '&Instance_From=0'
                '&Instance_To=1')

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json()['Error'], "Access Denied.")
        self.assertEqual(response.json()['RedirectURL'], "https://admin.enterpriseauth.com/#/login")


        # wrong group
        response = requests.get(
                util.erad_cfg().api.host + '/erad/admin/supplicant/log/export/csv'
                '?Session_ID=7018129031-123123123-1231123123123'
                '&Group_ID=XX-YYYY1'
                '&Include_Stray=false'
                '&Instance_From=0'
                '&Instance_To=1')

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json()['Error'], "Access Denied.")



        get_response = requests.get(
                util.erad_cfg().api.host + '/erad/admin/supplicant/log/export/csv'
                '?Session_ID=7018129031-123123123-123123123123'
                '&Group_ID=XX-YYYY'
                '&Include_Stray=false'
                '&Instance_From=0'
                '&Instance_To={Instance_To}'.format(Instance_To = Instance_To)
                )

        self.assertEqual(get_response.status_code, 200)

        response_data = get_response.text
        self.assertTrue('Access-Accept' in response_data)
        self.assertEqual(get_response.headers['Content-type'], 'text/csv; charset=utf-8')
        self.assertTrue('XX-YYYY,Access-Request,skywalker' in response_data)


    # Make sure that we cant corrupt the proxy.conf with duplicate proxy info
    def test_admin_group_save_duplicate_proxy(self):
        # Configure the first group
        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group,
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "Account_ID": "elevenos",
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                }
            })
        # Configure the second group with different proxy info
        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group2,
                    "RemoteServerUrl": "55.44.33.22:800",
                    "SharedSecret": "XYZ Rules"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "Account_ID": "elevenos",
                    "ID": self.std_group2,
                    "Name": "The XYZ Hotel",
                    "RemoteServerUrl": "55.44.33.22:800",
                    "SharedSecret": "XYZ Rules"
                }
            })
        # Attempt to duplicate proxy but not SharedSecret (should raise Error)
        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group2,
                    "RemoteServerUrl": "123.123.122.129:1812",# Same as std_group
                    "SharedSecret": "XYZ Rules"
                }
            },
            {
                "Error": "The Shared Secret entered is incorrect."
            })
        # Also make sure the group wasn't modified
        self.json_post_expect_200(
            '/erad/admin/group/load',
            {
                "Session": { "ID": self.std_session },
                "Group": { "ID": self.std_group2 }
            },
            {
                "Error": None,
                "Group":
                {
                    "Account_ID": "elevenos",
                    "ID": self.std_group2,
                    "Name": "The XYZ Hotel",
                    "RemoteServerUrl": "55.44.33.22:800",
                    "SharedSecret": "XYZ Rules",
                    "TimeZone": "Asia/Qyzylorda"
                }
            })
        # Duplicate IP address but not Port (should be fine)
        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group2,
                    "RemoteServerUrl": "123.123.122.129:1813",
                    "SharedSecret": "XYZ Rules"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": self.std_group2,
                    "Name": "The XYZ Hotel",
                    "RemoteServerUrl": "123.123.122.129:1813",
                    "SharedSecret": "XYZ Rules"
                }
            })
        # Duplicate proxy *and* SharedSecret (should be fine)
        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group2,
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": self.std_group2,
                    "Name": "The XYZ Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                }
            })
        # Put second group back to normal (should be fine)
        self.json_post_expect_200(
            '/erad/admin/group/save',
            {
                "Session": { "ID": self.std_session },
                "Group":
                {
                    "ID": self.std_group2,
                    "RemoteServerUrl": "55.44.33.22:800",
                    "SharedSecret": "XYZ Rules"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": self.std_group2,
                    "Name": "The XYZ Hotel",
                    "RemoteServerUrl": "55.44.33.22:800",
                    "SharedSecret": "XYZ Rules"
                }
            })

    def test_admin_group_authenticator_list(self):
        self.json_post_expect_200(
            '/erad/admin/group/authenticator/list',
            {
                "Session": { "ID": self.std_session },
                "Group_ID": self.std_group
            },
            {
                "Error": None,
                "Count": 1,
                "Auth_List":
                [
                    {
                        self.std_radiusattribute: self.std_authenticator
                    }
                ]
            })

        self.json_post_expect_200(
            '/erad/admin/group/authenticator/list',
            {
                "Session": { "ID": self.std_session },
                "Group_ID": self.std_group2
            },
            {
                "Error": None,
                "Count": 0,
                "Auth_List": []
            })

        self.json_post_expect_403(
            '/erad/admin/group/authenticator/list',
            {
                "Session": { "ID": "wrong!" },
                "Group_ID": self.std_group
            },
            {
                "Error": "Access Denied."
            })

    def test_admin_authenticator_load(self):
        self.json_post_expect_403(
            '/erad/admin/authenticator/load',
            {
                "Session": { "ID": self.std_session + "2" },
                "Authenticator":
                {
                    "ID": self.std_authenticator
                }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/admin/authenticator/load',
            {
                "Session": { "ID": self.std_session },
                "Authenticator":
                {
                    "ID": self.std_authenticator
                }
            },
            {
                "Error": None,
                "Authenticator":
                {
                    "ID": self.std_authenticator,
                    "Group_ID": self.std_group
                }
            })
        self.json_post_expect_200(
            '/erad/admin/authenticator/load',
            {
                "Session": { "ID": self.std_session },
                "Authenticator":
                {
                    "ID": self.std_group3,
                    "RadiusAttribute": "nas-identifier"
                }
            },
            {
                "Error": None,
                "Authenticator":
                {
                    "ID": self.std_group3,
                    "Group_ID": self.std_group3,
                    "RadiusAttribute": "nas-identifier"
                }
            })

    def test_admin_authenticator_save(self):
        self.json_post_expect_403(
            '/erad/admin/authenticator/save',
            {
                "Session": { "ID": self.std_session + "2" }, # modify key
                "Authenticator":
                {
                    "ID": self.std_authenticator2,
                    "Group_ID": self.std_group3
                }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/admin/authenticator/save',
            {
                "Session": { "ID": self.std_session },
                "Authenticator":
                {
                    "ID": self.std_authenticator2,
                    "Group_ID": self.std_group3
                }
            },
            {
                "Error": None,
                "Authenticator":
                {
                    "ID": self.std_authenticator2,
                    "Group_ID": self.std_group3
                }
            })
        self.json_post_expect_200(
            '/erad/admin/authenticator/save',
            {
                "Session": { "ID": self.std_session },
                "Authenticator":
                {
                    "ID": "dotElevenSupplicant123",
                    "Group_ID": self.std_group3,
                    "RadiusAttribute": "nas-identifier"
                }
            },
            {
                "Error": None,
                "Authenticator":
                {
                    "ID": "dotElevenSupplicant123",
                    "Group_ID": self.std_group3,
                    "RadiusAttribute": "nas-identifier"
                }
            })

    def test_admin_authenticator_save_ssidname(self):
        self.json_post_expect_200(
            '/erad/admin/authenticator/save',
            {
                "Session": { "ID": self.std_session },
                "Authenticator":
                {
                    "ID": "24-C9-A1-41-CC-AC:sullHS20",
                    "Group_ID": self.std_group3
                }
            },
            {
                "Error": None,
                "Authenticator":
                {
                    "ID": "24:C9:A1:41:CC:AC",
                    "Group_ID": self.std_group3
                }
            })

    def test_admin_authenticator_delete(self):
        # make groups to delete
        model = Erad_Authenticator2()
        model.ID = util.sanitize_mac("FF:AA:BB:CC:44:33")
        model.Account_ID = 'elevenos'
        model.Group_ID = "888888"
        model.save()

        model2 = Erad_Authenticator2()
        model2.ID = "dotElevenAuthenticator908"
        model2.Account_ID = 'elevenos'
        model2.RadiusAttribute = 'nas-identifier'
        model2.Group_ID = "888888"
        model2.save()

        self.json_post_expect_403(
            '/erad/admin/authenticator/delete',
            {
                "Session": { "ID": self.std_session + "2" }, # modify key
                "Authenticator":
                {
                    "ID": model.ID
                }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/admin/authenticator/delete',
            {
                "Session": { "ID": self.std_session },
                "Authenticator":
                {
                    "ID": model.ID
                }
            },
            {
                "Error": None
            })
        self.json_post_expect_200(
            '/erad/admin/authenticator/delete',
            {
                "Session": { "ID": self.std_session },
                "Authenticator":
                {
                    "ID": model2.ID,
                    "RadiusAttribute": 'nas-identifier'
                }
            },
            {
                "Error": None
            })

    def test_admin_endpoint_load(self):
        self.json_post_expect_403(
            '/erad/admin/endpoint/load',
            {
                "Session": { "ID": self.std_session + "2" }
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/admin/endpoint/load',
            {
                "Session": { "ID": self.std_session },
                "Account_ID": "elevenos"
            },
            {
                "Error": None,
                "Endpoint": {"IpAddress": "52.26.34.201",
                            "Port": "1812",
                            "Region": "us-west-1",
                            "Secret": "4905DVCA3B"},
                "Global": True
            })

        # One must be able to have a group_ID parameter
        # A list in Endpoint if more than one found
        self.json_post_expect_200(
            '/erad/admin/endpoint/load',
            {
                "Session": {"ID": self.std_xptosession},
                "Account_ID": "xptoaccount",
                "Group_ID": self.std_group3
            },
            {
                "Error": None,
                "Endpoint": [
                                {
                                    "IpAddress": "52.26.34.201",
                                    "Port": "20004",
                                    "Region": "us-west-1",
                                    "Secret": "secret20004"
                                },
                                {
                                    "IpAddress": "52.26.34.201",
                                    "Port": "20006",
                                    "Region": "us-west-1",
                                    "Secret": "secret20006"
                                }
                            ],
                "Global": False
            })
        # If only one matching group_ID then instead of a list, Endpoint should be a dict
        self.json_post_expect_200(
            '/erad/admin/endpoint/load',
            {
                "Session": {"ID": self.std_xptosession},
                "Account_ID": "xptoaccount",
                "Group_ID": self.std_group2
            },
            {
                "Error": None,
                "Endpoint": {"IpAddress": "52.26.34.201",
                            "Port": "20008",
                            "Region": "us-west-1",
                            "Secret": "secret20008"},
                "Global": False
            })
        # When passing non matching Group_ID, Global should be used in the query
        self.json_post_expect_200(
            '/erad/admin/endpoint/load',
            {
                "Session": { "ID": self.std_session },
                "Account_ID": "elevenos",
                "Group_ID": "non-existent-group-id"
            },
            {
                "Error": None,
                "Endpoint": {"IpAddress": "52.26.34.201",
                            "Port": "1812",
                            "Region": "us-west-1",
                            "Secret": "4905DVCA3B"},
                "Global": True
            })

    def test_admin_endpoint_save(self):
        self.json_post_expect_403(
            '/erad/admin/endpoint/save',
            {
                "Session": { "ID": self.std_session + "2" },
                "Group_ID": "2323242535"
            },
            {
                "Error": "Access Denied."
            })

        sess = Erad_Session.get(self.std_session)
        sess.Group_ID_List.add("validGroupId")
        sess.Group_ID_List.add("Global")
        sess.save()

        # New group ID
        result = self.json_post(
                    '/erad/admin/endpoint/save',
                    {
                        "Session": { "ID": self.std_session },
                        "Group_ID": "validGroupId",
                        "Region": "us-west-1"
                    }
            )
        assert result.status_code == 200
        endpoint = result.json()['Endpoint']
        endpoint_port = endpoint['Port']
        possible_ports = ["20006", "20008", "20020"]
        assert endpoint_port in possible_ports
        endpoint_ip = endpoint['IpAddress']
        possible_ips = ["52.26.34.124", "52.26.34.255" ]
        assert endpoint_ip in possible_ips
        assert endpoint['Region'] == "us-west-1"
        endpoint_secret = endpoint['Secret']
        possible_secrets = ["4905DVCA3B", "secret20020"]
        assert endpoint_secret in possible_secrets

        result = self.json_post(
                    '/erad/admin/endpoint/save',
                    {
                        "Session": { "ID": self.std_session },
                        "Group_ID": "validGroupId",
                        "Region": "us-west-1"
                    }
            )
        assert result.status_code == 200
        endpoint = result.json()['Endpoint']
        endpoint_port = endpoint['Port']
        possible_ports = ["20006", "20008", "20020"]
        assert endpoint_port in possible_ports
        endpoint_ip = endpoint['IpAddress']
        possible_ips = ["52.26.34.124", "52.26.34.255" ]
        assert endpoint_ip in possible_ips
        assert endpoint['Region'] == "us-west-1"
        endpoint_secret = endpoint['Secret']
        possible_secrets = ["4905DVCA3B", "secret20020"]
        assert endpoint_secret in possible_secrets

        # No ::free entries left, there should be no ::free entries for region us-west-2
        self.json_post_expect_200(
            '/erad/admin/endpoint/save',
            {
                "Session": {"ID": self.std_session},
                "Group_ID": "validGroupId",
                "Region": "us-west-1"
            },
            {
                "Error": "No ports left."
            })
        # Changes to Global group should not be allowed.
        self.json_post_expect_200(
            '/erad/admin/endpoint/save',
            {
                "Session": {"ID": self.std_session},
                "Group_ID": "Global"
            },
            {
                "Error": "Global site can't be modified."
            })
        # Assuring casing insensitivity
        # self.json_post_expect_200(
        #     '/erad/admin/endpoint/save',
        #     {
        #         "Session": {"ID": self.std_session},
        #         "Group_ID": "glObal"
        #     },
        #     {
        #         "Error": "Global site can't be modified."
        #     })
        # No ::free entries for default region
        self.json_post_expect_200(
            '/erad/admin/endpoint/save',
            {
                "Session": {"ID": self.std_session},
                "Group_ID": "validGroupId"
            },
            {
                "Error": "No ports left."
            })

    def test_admin_endpoint_delete(self):
        self.json_post_expect_403(
            '/erad/admin/endpoint/delete',
            {
                "Session": { "ID": self.std_session + "2" }
            },
            {
                "Error": "Access Denied."
            })
        # Trying to delete non existent endpoint
        self.json_post_expect_403(
            '/erad/admin/endpoint/delete',
            {
                "Session": {"ID": self.std_xptosession},
                "Group_ID": "invalidGroup",
                "Port": "20011"
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_403(
            '/erad/admin/endpoint/delete',
            {
                "Session": {"ID": self.std_xptosession},
                "Group_ID": "AA-BCD",
                "Port": "20018"
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_403(
            '/erad/admin/endpoint/delete',
            {
                "Session": {"ID": self.std_xptosession},
                "Group_ID": "invalidGroup",
                "Port": "20018"
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/admin/endpoint/delete',
            {
                "Session": {"ID": self.std_xptosession},
                "Group_ID": "AA-BCD",
                "Port": "20012"
            },
            {
                "Error": None
            })
        self.json_post_expect_200(
            '/erad/admin/endpoint/delete',
            {
                "Session": {"ID": self.std_xptosession},
                "Group_ID": "Global",
                "Port": "20011"
            },
            {
                "Error": "Global site can't be modified."
            })
        self.json_post_expect_200(
            '/erad/admin/endpoint/delete',
            {
                "Session": {"ID": self.std_xptosession},
                "Group_ID": "GlObal",
                "Port": "20011"
            },
            {
                "Error": "Global site can't be modified."
            })

    def test_admin_account_saveloadlist(self):
        self.json_post_expect_200(
            '/erad/admin/account/save',
            {
                "Session": { "ID": self.std_session },
                "AccountOwner": {"Email": "testaccount@elevenos.com", "Password": "TheSomePass"}
            },
            {
                "Error": None
            })
        self.json_post_expect_403(
            '/erad/admin/account/save',
            {
                "Session": { "ID": self.std_session + "2"},
                "AccountOwner": {"Email": "testaccount@elevenos.com", "Password": "TheSomePass"}
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/admin/account/load',
            {
                "Session": { "ID": self.std_session },
                "AccountOwner": {"Email": "testaccount@elevenos.com"}
            },
            {
                "Error": None,
                "AccountOwner": {
                    "Account_ID": "elevenos",
                    "Email": "testaccount@elevenos.com"
                  }
            })
        self.json_post_expect_200(
            '/erad/admin/account/load',
            {
                "Session": { "ID": self.std_session },
                "AccountOwner": {"Email": "WRONG@elevenos.com"}
            },
            {
                "Error": "Account doesn't exist."
            })
        self.json_post_expect_200(
            '/erad/admin/account/list',
            {
                "Session": { "ID": self.std_session }
            },
            {
              "Error": None,
              "Accounts": [
                "testaccount@elevenos.com",
                "owner@elevenos.com"
              ]
            })

    def test_admin_account_delete(self):
        self.json_post_expect_200(
            '/erad/admin/account/save',
            {
                "Session": { "ID": self.std_session },
                "AccountOwner": {"Email": "testaccount@elevenos.com", "Password": "TheSomePass"}
            },
            {
                "Error": None
            })
        self.json_post_expect_403(
            '/erad/admin/account/delete',
            {
                "Session": { "ID": self.std_session + "2"},
                "AccountOwner": {"Email": "testaccount@elevenos.com"}
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/admin/account/delete',
            {
                "Session": { "ID": self.std_session },
                "AccountOwner": {"Email": "testaccount@elevenos.com"}
            },
            {
                "Error": None
            })
        self.json_post_expect_200(
            '/erad/admin/account/delete',
            {
                "Session": { "ID": self.std_session },
                "AccountOwner": {"Email": "testaccount@elevenos.com"}
            },
            {
                "Error": "Account doesn't exist."
            })

    def test_admin_apikey_saveloadlist(self):
        self.json_post_expect_200(
            '/erad/admin/apikey/save',
            {
                "Session": { "ID": self.std_session },
                "ApiKey": {"Active": 1}
            },
            {
                "Error": None
            })
        self.json_post_expect_403(
            '/erad/admin/apikey/save',
            {
                "Session": { "ID": self.std_session + "2"},
                "ApiKey": {"Active": 1}
            },
            {
                "Error": "Access Denied."
            })
        """self.json_post_expect_200(
            '/erad/admin/apikey/load',
            {
                "Session": { "ID": self.std_session },
                "ApiKey": {"ApiKeyName": "Test name"}
            },
            {
                "Error": None,
                "ApiKey": {
                    "ApiKeyName": "***********",
                    "ApiKey_ID": "***********",
                    "Active": 1
                  }
            })"""
        self.json_post_expect_200(
            '/erad/admin/apikey/load',
            {
                "Session": { "ID": self.std_session },
                "ApiKey": {"ApiKeyName": "WRONG@elevenos.com"}
            },
            {
                "Error": "ApiKey doesn't exist."
            })
        self.json_post_expect_200(
            '/erad/admin/apikey/list',
            {
                "Session": { "ID": self.std_session }
            },
            {
              "Error": None,
            })

    def test_admin_apikey_delete(self):
        self.json_post_expect_200(
            '/erad/admin/apikey/save',
            {
                "Session": { "ID": self.std_session },
                "ApiKey": {"Active": 1}
            },
            {
                "Error": None
            })
        self.json_post_expect_403(
            '/erad/admin/apikey/delete',
            {
                "Session": { "ID": self.std_session + "2"},
                "ApiKey": {"ApiKeyName": "testaccount"}
            },
            {
                "Error": "Access Denied."
            })
        """self.json_post_expect_200(
            '/erad/admin/apikey/delete',
            {
                "Session": { "ID": self.std_session },
                "ApiKey": {"ApiKeyName": "testaccount"}
            },
            {
                "Error": None
            })"""
        """self.json_post_expect_200(
            '/erad/admin/apikey/delete',
            {
                "Session": { "ID": self.std_session },
                "ApiKey": {"ApiKeyName": "testaccount"}
            },
            {
                "Error": "ApiKey doesn't exist."
            })"""




if __name__ == '__main__':
    unittest.main()
