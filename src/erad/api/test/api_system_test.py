#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
import time
from .api_test import *

from ..erad_model import Erad_AccountOwner
from ..erad_model import Erad_Endpoint

#---------- System Services ----------
class ApiSystemTest(ApiTest):

    def test_system_supplicant_found(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": { "ID": self.std_authenticator },
                "Supplicant": { "Username" : self.std_username_match }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                },
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232
                }
            })

        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": { "ID": self.std_authenticator },
                "Supplicant": { "Username" : self.std_username_match_custom_data }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                },
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": self.std_username_custom_data,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232,
                    "CustomJsonData" : "custom_json_data"
                }
            })

    def test_global_supplicant_found(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": {"ID": 'aa:55:44:33:22:11'},
                "Supplicant": {"Username" : 'testing'},
                "Group": {"ID": "Global"},
                "Endpoint": {
                    "IpAddress": "52.26.34.201",
                    "Port": "20000"
                }
            },
            {
                "Error": "Supplicant 'testing' does not exist for group 'Global'",
            })
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": {"ID": "bb:55:44:33:22:11"},
                "Supplicant": {"Username" : "globaltest"},
                "Group": {"ID": "Global"},
                "Endpoint": {
                    "IpAddress": "52.26.34.201",
                    "Port": "20000"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": 'Global',
                    "Name": 'Global',
                    'TimeZone': 'America/Los_Angeles',
                    'Account_ID': 'fooaccount'
                },
                "Supplicant":
                {
                    "Group_ID": 'fooaccount::Global',
                    "Username": 'globaltest',
                    "Password": "globaltest",
                    'UseWildcard': False
                }
            })


    def test_not_global_same_port_supplicant_found(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": {"ID": "66:55:44:33:22:11"},
                "Supplicant": {"Username" : "testing"},
                "Group": {"ID": "bar"},
                "Endpoint": {
                    "IpAddress": "52.26.34.201",
                    "Port": "20000"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": 'bar',
                    "Name": 'bar',
                    'TimeZone': 'America/Los_Angeles',
                    'Account_ID': 'fooaccount'
                },
                "Supplicant":
                {
                    "Group_ID": 'fooaccount::bar',
                    "Username": 'testing',
                    "Password": "testing",
                    'UseWildcard': False
                }
            })

    def test_not_global_supplicant_found(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": {"ID": "irrelevant"},
                "Supplicant": {"Username" : "testing"},
                "Group": {"ID": "bar"},
                "Endpoint": {
                    "IpAddress": "52.26.34.201",
                    "Port": "20014"
                }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": 'bar',
                    "Name": 'bar',
                    'TimeZone': 'America/Los_Angeles',
                    'Account_ID': 'fooaccount'
                },
                "Supplicant":
                {
                    "Group_ID": 'fooaccount::bar',
                    "Username": 'testing',
                    "Password": "testing",
                    'UseWildcard': False
                }
            })

    def test_system_supplicant_notfound(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": { "ID": self.std_authenticator },
                "Supplicant": { "Username" : "Not-a-user" }
            },
            {
                "Error": "Supplicant 'Not-a-user' does not exist for group '{0}'".format(self.std_group)
            })

    # make sure we can pass a bad group ID and it doesnt matter because we use Authenticator
    def test_system_supplicant_opt_groupid(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": { "ID": self.std_authenticator },
                "Supplicant": { "Username" : self.std_username_match },
                "Group": { "ID": "bogus-group" }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                },
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232
                }
            })

    # if Authenticator is bad, use Group ID
    def test_system_supplicant_use_groupid(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": { "ID": "FF:FF:FF:00:00:00" }, # does not exist
                "Supplicant": { "Username" : self.std_username_match },
                "Group": { "ID": self.std_group }
            },
            {
                "Error": None,
                "Group":
                {
                    "ID": self.std_group,
                    "Name": "The ABC Hotel",
                    "RemoteServerUrl": "123.123.122.129:1812",
                    "SharedSecret": "Eleven Bacon"
                },
                "Supplicant":
                {
                    "Group_ID": self.std_account_id+"::"+self.std_group,
                    "Username": self.std_username,
                    "Password": "aihjwd9awd098jawd9j8ad",
                    "UseRemote": False,
                    "Vlan": 1232
                }
            })

    # if both are bogus, we should fail to find anything
    def test_system_supplicant_both_notfound(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": { "ID": "FF:FF:FF:00:00:00" }, # does not exist
                "Supplicant": { "Username" : self.std_username_match },
                "Group": { "ID": "bogus-group" }
            },
            {
                "Error": "Neither Authenticator nor Group exist."
            })

    # NULL Group ID is ignored
    def test_system_supplicant_null_groupid(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": { "ID": self.std_authenticator },
                "Supplicant": { "Username" : "Not-a-user" },
                "Group": { "ID": None }
            },
            {
                "Error": "Supplicant 'Not-a-user' does not exist for group '{0}'".format(self.std_group)
            })

    def test_search_supplicant_in_not_allocated_group(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Endpoint": {
                    "IpAddress": "52.26.34.254",
                    "Port": "20021"
                },
                "Authenticator": {
                    "ID": "AA:55:44:33:22:11"
                }
            },
            {
                "Error": "Supplicant does not exist because group ::free is not allocated"
            })

    def test_system_supplicant_expiration(self):
        self.json_post_expect_200(
            '/erad/system/supplicant/find',
            {
                "SystemKey": self.std_system_key,
                "Authenticator": { "ID": "foo" },
                "Supplicant": { "Username" : self.std_username_expired },
                "Group": { "ID": self.std_group3 }
            },
            {
                "Error": "Supplicant is expired."
            })

    def test_system_server_config_notfound(self):
        self.json_post_expect_400(
            '/erad/system/server/config/load',
            {
                "SystemKey1": self.std_system_key,
                "ClusterId": "uswest-dev-01",
                "ServerType": "radius1"
            },
            {
                "Error": "Required parameter 'SystemKey' is missing."
            })

    def test_system_server_config_badkey(self):
        self.json_post_expect_403(
            '/erad/system/server/config/load',
            {
                "SystemKey": "asdasdasdasdasdasd"
            },
            {
                "Error": "Access Denied."
            })

    def test_system_server_config_badtype(self):
        self.json_post_expect_200(
            '/erad/system/server/config/load',
            {
                "SystemKey": self.std_system_key,
                "ClusterId": "uswest-dev-01",
                "ServerType": "radius1"
            },
            {
              "Error": None,
              "VirtualServers": {},
              "Certs": {}
            })

    def test_system_server_config_notfoundBadId(self):
        self.json_post_expect_200(
            '/erad/system/server/config/load',
            {
                "SystemKey": self.std_system_key,
                "ClusterId": "asdasd",
                "ServerType": "radius"
            },
            {
              "Error": None,
              "VirtualServers": {},
              "Certs": {}
            })

    def test_system_server_config_found(self):

        # password for this record: Eleven
        accountOwner = Erad_AccountOwner('account_with_cert@acme.example.com',
                                                    Password='nbtezyoogoHL8IIFR6pexw==',
                                                    Account_ID='test_account_with_cert',
                                                    Salt="SALT-PLACEHOLDER"
                                                    )
        accountOwner.save()

        item_cert = Erad_Endpoint(  '52.26.34.201',
                            Port="1822",
                            Group_ID="test_account_with_cert::Global",
                            ClusterId="uswest-dev-01",
                            Secret="4905DVCA3B",
                            Region="us-west-1")
        item_cert.save()

        self.json_post_expect_200(
            '/erad/system/server/config/load',
            {
                "SystemKey": self.std_system_key,
                "ClusterId": "uswest-dev-01",
                "ServerType": "radius",
            },
            {
              "Error": None,
              "VirtualServers": {
                "1812": "4905DVCA3B",
                "1814": "V72QSZ2CE3",
                "1822": "4905DVCA3B",
              },
              "Certs": {
                "1812" : None,
                "1814" : None,
                "1822" : 'RRR',
              }
            })

        item_cert.delete()
        accountOwner.delete()

    def test_system_server_config_LBfound_empty_cluster(self):
        self.json_post_expect_200(
            '/erad/system/server/config/load',
            {
                "SystemKey": self.std_system_key,
                "ClusterId": "useast-dev-03",
                "ServerType": "load_balancer"
            },
            {
              "Error": None,
              "VirtualServers": [
                (1814, {}),
                (20020, {})
              ] # new lb format, the old endpoints don't have any Cluster matching objects on the Erad_Cluster table yet.
            })

    def test_system_server_config_LBfound(self):
        self.json_post_expect_200(
            '/erad/system/server/config/load',
            {
                "SystemKey": self.std_system_key,
                "ClusterId": "uswest-dev-02",
                "ServerType": "load_balancer"
            },
            {
                "Error": None,
                "VirtualServers": [
                    (1814, {"auth": {"instance-id": "52.26.34.201"},
                            "acct": {"instance-id-acct": "52.26.34.201"}
                           }
                    ),
                    (3334, {"auth": {"instance-id": "52.26.34.201"},
                            "acct": {"instance-id-acct": "52.26.34.201"}
                           }
                    )
                ]
            })

    def test_system_server_config_LBnotfoundBadId(self):
        self.json_post_expect_200(
            '/erad/system/server/config/load',
            {
                "SystemKey": self.std_system_key,
                "ClusterId": "asdasd",
                "ServerType": "load_balancer"
            },
            {
              "Error": None,
              "VirtualServers": [],
            })

    def test_system_account_save(self):
        self.json_post_expect_403(
            '/erad/system/account/save',
            {
                "SystemKey": "asdasdasdasdasdasd"
            },
            {
                "Error": "Access Denied."
            })
        self.json_post_expect_200(
            '/erad/system/account/save',
            {
                "SystemKey": self.std_system_key,
                "AccountOwner": {"Email": "johndoe@eleven.com", "Password": "TheEleven", "Account_ID": "Eleven Group"}
            },
            {
              "Error": None
            })
        self.json_post_expect_400(
            '/erad/system/account/save',
            {
                "SystemKey": self.std_system_key
            },
            {
              "Error": "Required parameter 'AccountOwner' is missing."
            })
        self.json_post_expect_400(
            '/erad/system/account/save',
            {
                "SystemKey": self.std_system_key,
                "AccountOwner": {"Email1": "johndoe@eleven.com", "Password": "TheEleven", "Account_ID": "Eleven Group"}
            },
            {
              "Error": "Required parameter 'Email' is missing."
            })
        self.json_post_expect_400(
            '/erad/system/account/save',
            {
                "SystemKey": self.std_system_key,
                "AccountOwner": {"Email": "johndoe@eleven.com", "Password1": "TheEleven", "Account_ID": "Eleven Group"}
            },
            {
              "Error": "Required parameter 'Password' is missing."
            })
        self.json_post_expect_400(
            '/erad/system/account/save',
            {
                "SystemKey": self.std_system_key,
                "AccountOwner": {"Email": "johndoe@eleven.com", "Password": "TheEleven", "Account_ID1": "Eleven Group"}
            },
            {
              "Error": "Required parameter 'Account_ID' is missing."
            })
        self.json_post_expect_200(
            '/erad/system/account/save',
            {
                "SystemKey": self.std_system_key,
                "AccountOwner": {"Email": "johndoe@eleven.com", "Password": "password", "Account_ID": "Eleven Group"}
            },
            {
                "Error": "That password was found in a public list of previously compromised passwords. Please choose a different password."
            })
        self.json_post_expect_200(
            '/erad/system/account/save',
            {
                "SystemKey": self.std_system_key,
                "AccountOwner": {"Email": "johndoe@eleven.com", "Password": "PaSsWoRd", "Account_ID": "Eleven Group"}
            },
            {
                "Error": "That password was found in a public list of previously compromised passwords. Please choose a different password."
            })

    def test_radius_finish(self):
        # Invalid authorization
        self.json_post_expect_403(
            '/erad/system/radius/finish',
            {
                "SystemKey": "asdasdasdasdasdasd"
            },
            {
                "Error": "Access Denied."
            })
        # Missing Cluster_ID
        self.json_post_expect_400(
            '/erad/system/radius/finish',
            {
                "SystemKey": self.std_system_key
            },
            {
                "Error": "Required parameter 'Cluster_ID' is missing."
            })
        # Non existent Cluster_ID parameter
        self.json_post_expect_200(
            '/erad/system/radius/finish',
            {
                "SystemKey": self.std_system_key,
                "Cluster_ID": "non existing Cluster_ID"
            },
            {
                "Error": "Cluster not found."
            })

        # successful test
        self.json_post_expect_200(
            '/erad/system/radius/finish',
            {
                "SystemKey": self.std_system_key,
                "Cluster_ID": "uswest-dev-02"
            },
            {
                "Error": None
            })
        # The second time should not return any errors, even though there should not be any ::dirty group_ID
        self.json_post_expect_200(
            '/erad/system/radius/finish',
            {
                "SystemKey": self.std_system_key,
                "Cluster_ID": "uswest-dev-02"
            },
            {
                "Error": None
            })

    def test_radius_region(self):
        # Invalid authorization
        self.json_post_expect_403(
            '/erad/system/radius/region',
            {
                "SystemKey": "asdasdasdasdasdasd"
            },
            {
                "Error": "Access Denied."
            })
        # Missing Cluster_ID
        self.json_post_expect_400(
            '/erad/system/radius/region',
            {
                "SystemKey": self.std_system_key
            },
            {
                "Error": "Required parameter 'Cluster_ID' is missing."
            })
        # Non existent Cluster_ID parameter
        self.json_post_expect_200(
            '/erad/system/radius/region',
            {
                "SystemKey": self.std_system_key,
                "Cluster_ID": "non existing Cluster_ID"
            },
            {
                "Error": "Cluster not found."
            })

        # successful test
        self.json_post_expect_200(
            '/erad/system/radius/region',
            {
                "SystemKey": self.std_system_key,
                "Cluster_ID": "uswest-dev-02"
            },
            {
                "Error": None,
                "Region": "us-west-2"
            })

    def test_radius_save_load(self):
        """
        The Servers parameter must have the following format
	{
		"cluster-id-1": {
			"Instances": {
				"auth": {
					"instance_id": "ip address",
					"instance_id": "ip address"
				},
				"acct": {
					"instance_id": "ip address",
					"instance_id": "ip address",
					"instance_id": "ip address",
					"instance_id": "ip address"
				}
			}
		},
		"cluster-id-2": {
			"Instances": {
				"auth": {
					"instance_id": "ip address",
					"instance_id": "ip address"
				},
				"acct": {
					"instance_id": "ip address",
					"instance_id": "ip address",
					"instance_id": "ip address",
					"instance_id": "ip address"
				}
			}
		}
	}

	The second ClusterId section is optional.
        """
        # Invalid authorization
        self.json_post_expect_403(
            '/erad/system/radius/save',
            {
                "SystemKey": "asdasdasdasdasdasd"
            },
            {
                "Error": "Access Denied."
            })
        # Invalid empty Servers
        self.json_post_expect_400(
            '/erad/system/radius/save',
            {
                "SystemKey": self.std_system_key,
                "Servers": {},
            },
            {
                "Error": "Invalid Servers parameter format. No Servers found."
            })
        # Loading test
        self.json_post_expect_200(
            '/erad/system/radius/load',
            {
                "SystemKey": self.std_system_key,
                "Cluster_ID": "useast-dev-01"
            },
            {
                "Error": None,
                "Cluster": {
                            "ClusterId": "useast-dev-01",
                            "AuthNum": 1,
                            "AcctNum": 1,
                            "Instances": {"auth": {"instance-id": "52.26.34.201"},
                                          "acct": {"instance-id-acct": "52.26.34.201"}
                                         }
                           }
            })
        self.json_post_expect_403(
            '/erad/system/radius/load',
            {
                "SystemKey": "asdasdasdasdasdasd"
            },
            {
                "Error": "Access Denied."
            })
        # Load missing Cluster_ID
        self.json_post_expect_400(
            '/erad/system/radius/load',
            {
                "SystemKey": self.std_system_key
            },
            {
                "Error": "Required parameter 'Cluster_ID' is missing."
            })
        # Loading with invalid Cluster_ID
        self.json_post_expect_200(
            '/erad/system/radius/load',
            {
                "SystemKey": self.std_system_key,
                "Cluster_ID": "non-existing-cluster-id",
            },
            {
                "Error": "Cluster not found."
            })

    def test_supplicant_log_save(self):
        self.json_post_expect_403(
            '/erad/system/supplicant/log/save',
            {
                "SystemKey": "asdasdasdasdasdasd",
            },
            {
                "Error": "Access Denied."
            })


        timestamp = str(int(time.time()))
        self.json_post_expect_200(
            '/erad/system/supplicant/log/save',
            {
                "SystemKey": self.std_system_key,
                "Logs" : {
                    "EndpointIPAddress" : "127.1.1.1",
                    "EndpointPort": "1812",
                    "Timestamp" : timestamp,
                    "PacketType" : "the packet type",
                    "UserName" : "test_username",
                    "CalledStationId" : "test_called_stattion_id",
                    "CallingStationId" : "test_calling_station_id",
                    "EventTimestamp" : timestamp,
                    "NASIdentifier" : "test nas id",
                    "NASIPAddress" : "127.1.2.1",
                    "Access" : "Accept",
                    "IsProxied": "no"
                }
            },
            {
                "Error": None
            })



        log_list = [x for x in Erad_Radius_Log.query(
                        "None",
                        filter_condition=(Erad_Radius_Log.Username=="test_username")
                            & (Erad_Radius_Log.Timestamp==timestamp)
                        )
                ]

        log_entry = log_list[0]
        self.assertEqual(log_entry.EventTimestamp, timestamp)
        self.assertEqual(log_entry.PacketType, "the packet type")


    def test_supplicant_log_save(self):
        self.json_post_expect_403(
            '/erad/system/supplicant/acct/log/save',
            {
                "SystemKey": "asdasdasdasdasdasd",
            },
            {
                "Error": "Access Denied."
            })

        raw_log =  '''{
  "Acct-Authentic": "RADIUS",
  "Acct-Link-Count": "40",
  "Acct-Multi-Session-Id": "00000000000000000000000000000000001a",
  "Acct-Session-Id": "5A6B6F44-2C0E4000",
  "Acct-Status-Type": "Start",
  "Called-Station-Id": "aa-bb-cc-dd-ee-ff:foo",
  "Calling-Station-Id": "00-00-00-74-AB-5D",
  "Class": "0x434143533a4844514e4341435330332f3238363536343833362f34373830383935",
  "Connect-Info": "CONNECT 802.11a/n/ac",
  "Event-Timestamp": "Jan  1 2018 18:11:12 UTC",
  "Framed-IP-Address": "10.0.0.1",
  "NAS-IP-Address": "10.0.1.1",
  "NAS-Identifier": "foo",
  "NAS-Port": "2",
  "NAS-Port-Type": "Wireless-802.11",
  "Proxy-State": "0x323334",
  "Ruckus-BSSID": "0xaabbccddeeff",
  "Ruckus-Location": "Floor 01",
  "Ruckus-SCG-CBlade-IP": "1",
  "Ruckus-SCG-DBlade-IP": "2",
  "Ruckus-SSID": "test_ssid",
  "Ruckus-VLAN-ID": "450",
  "Supplicant-Radius-Ip": "127.1.1.1",
  "Supplicant-Radius-Port": "1812",
  "User-Name": "testing"
}'''

        self.json_post_expect_200(
            '/erad/system/supplicant/acct/log/save',
            {
                "SystemKey": self.std_system_key,
                "Logs" : raw_log
            },
            {
                "Error": None
            })

        log_list = [x for x in Erad_AccountingLogs.query(
                        "elevenos",
                        filter_condition=(
                        Erad_AccountingLogs.EndpointIPAddress=="127.1.1.1")
                        & (Erad_AccountingLogs.EndpointPort=="1812")
                    )
        ]


        log_entry = log_list[0]
        self.assertEqual(log_entry.EndpointIPAddress, "127.1.1.1")
        self.assertEqual(log_entry.EndpointPort, "1812")
        self.assertEqual(log_entry.Log_Content, raw_log)

    def test_supplicant_find_certificate(self):
        self.json_post_expect_403(
            '/erad/system/supplicant/find/certificate',
            {
                "SystemKey": "asdasdasdasdasdasd",
            },
            {
                "Error": "Access Denied."
            })

        self.json_post_expect_200(
            '/erad/system/supplicant/find/certificate',
            {
                "SystemKey":  self.std_system_key,
                "SupplicantCertificate" : {
                    "ID" : "not exist"
                }
            },
            {
                "Error": "Item does not exist"
            })



        for crt in Erad_SupplicantCertificate.scan():
            if crt.Group_ID == 'elevenos::Global' and crt.Username =='globaltest':
                break

        self.json_post_expect_200(
            '/erad/system/supplicant/find/certificate',
            {
                "SystemKey":  self.std_system_key,
                "SupplicantCertificate" : {
                    "ID" : crt.ID
                },
                "Authenticator": {"ID": self.std_authenticator}
            },
            {
                "Error": None
            })


    def test_account_certs_list(self):
        self.json_post_expect_200(
            '/erad/system/account/certs/list',
            {
                "SystemKey":  self.std_system_key,
            },
            {
                "Certs": {
                    "jsteele": "hilton",
                    "hilton": "hilton",
                    'rlhc': 'rlhc',
                    "test_account_with_cert": "RRR"
                },
                "Error": None
            })


        self.json_post_expect_403(
            '/erad/system/supplicant/find/certificate',
            {
                "SystemKey": "asdasdasdasdasdasd",
            },
            {
                "Error": "Access Denied."
            })

        # add test account with cert
        self.json_post_expect_200(
            '/erad/system/account/save',
            {
                "SystemKey": self.std_system_key,
                "AccountOwner": {
                    "Email": "test@test.example.com",
                    "Password": "Thetestpass",
                    "Account_ID": "test_account_with_cert",
                }
            },
            {
              "Error": None
            })

        self.json_post_expect_200(
            '/erad/system/account/certs/list',
            {
                "SystemKey":  self.std_system_key,
            },
            {
                "Certs": {
                    "test_account_with_cert": "RRR",
                },
                "Error": None
            })

        model = Erad_AccountOwner(
            Account_ID = "test_account_with_cert",
            Password = "fakepass",
            Email = "test@test.example.com"
        )
        model.delete()
