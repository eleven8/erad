#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import logging


from .api_test import *
from os import path

logger = logging.getLogger(__name__)


#---------- Public Services ----------
class ApiPublicTest(ApiTest):

    corrupted_signer_ios_pem = '''-----BEGIN CERTIFICATE-----
MIIGMzCCBRugAwIBAgIHBr6OjJRHyDANBgkqhkiG9w0BAQUFADCBjDELMAkGA1UE
BhMCSUwxFjAUBgNVBAoTDVN0YXJ0Q29tIEx0ZC4xKzApBgNVBAsTIlNlY3VyZSBE
aWdpdGFsIENlcnRpZmljYXRlIFNpZ25pbmcxODA2BgNVBAMTL1N0YXJ0Q29tIENs
YXNzIDEgUHJpbWFyeSBJbnRlcm1lZGlhdGUgU2VydmVyIENBMB4XDTE1MTIxMzIx
NDcxMVoXDTE2MTIxNDA0NTgyOVowSDELMAkGA1UEBhMCR0IxFTATBgNVBAMTDHd3
dy40MnRtLmNvbTEiMCAGCSqGSIb3DQEJARYTcG9zdG1hc3RlckA0MnRtLmNvbTCC
ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANCHK8eAHE9gySJ1I7MO9m90
Y3FazeNJD37uF4cFm1NxWa8aSpkdr/TW7FmDxDxgWtMkOfArM2LFkkccfGvwkLeE
trKXojaek5nJpLqcAr1LpeZXp17XIl7WDfBVgWL5sdw5tauSNhYCS72EMlVk7zCs
g1pLKjufZ27u4wGwo0D1eaTShJPNu5uFu48UnWJGJzyhdP/SilxIJHrjKwzd3wN7
pSPJTaqfanvQIyLt7hWqZmQR8h0WhxdLQ5lbkAiSJ8rJqEEMFXIKRc397/hZV2Dt
PD6pxPviOpFvK0sTaxCeOoF61Vd+AJg37RWmR8LNXIm0ko7NHozTsn3HmAXAfnUC
AwEAAaOCAtswggLXMAkGA1UdEwQCMAAwCwYDVR0PBAQDAgOoMBMGA1UdJQQMMAoG
CCsGAQUFBwMBMB0GA1UdDgQWBBRIDos2z1XqT3PKZLsgg8pzBDb+QzAfBgNVHSME
GDAWgBTrQjTQmLCrn/Qbawj3zGQu7w4sRTAhBgNVHREEGjAYggx3d3cuNDJ0bS5j
b22CCDQydG0uY29tMIIBVgYDVR0gBIIBTTCCAUkwCAYGZ4EMAQIBMIIBOwYLKwYB
BAGBtTcBAgMwggEqMC4GCCsGAQUFBwIBFiJodHRwOi8vd3d3LnN0YXJ0c3NsLmNv
bS9wb2xpY3kucGRmMIH3BggrBgEFBQcCAjCB6jAnFiBTdGFydENvbSBDZXJ0aWZp
Y2F0aW9uIEF1dGhvcml0eTADAgEBGoG+VGhpcyBjZXJ0aWZpY2F0ZSB3YXMgaXNz
dWVkIGFjY29yZGluZyB0byB0aGUgQ2xhc3MgMSBWYWxpZGF0aW9uIHJlcXVpcmVt
ZW50cyBvZiB0aGUgU3RhcnRDb20gQ0EgcG9saWN5LCByZWxpYW5jZSBvbmx5IGZv
ciB0aGUgaW50ZW5kZWQgcHVycG9zZSBpbiBjb21wbGlhbmNlIG9mIHRoZSByZWx5
aW5nIHBhcnR5IG9ibGlnYXRpb25zLjA1BgNVHR8ELjAsMCqgKKAmhiRodHRwOi8v
Y3JsLnN0YXJ0c3NsLmNvbS9jcnQxLWNybC5jcmwwgY4GCCsGAQUFBwEBBIGBMH8w
OQYIKwYBBQUHMAGGLWh0dHA6Ly9vY3NwLnN0YXJ0c3NsLmNvbS9zdWIvY2xhc3Mx
L3NlcnZlci9jYTBCBggrBgEFBQcwAoY2aHR0cDovL2FpYS5zdGFydHNzbC5jb20v
Y2VydHMvc3ViLmNsYXNzMS5zZXJ2ZXIuY2EuY3J0MCMGA1UdEgQcMBqGGGh0dHA6
Ly93d3cuc3RhcnRzc2wuY29tLzANBgkqhkiG9w0BAQUFAAOCAQEAtOpXhkMQpOXw
21HY1sm5vyG/PxkhpR6dqI9Jrub2iyDVOHlpNGCmvBcQH+Wexb3u3sJVMWtHE4hO
pHOawCDeOop2V+VIlpbfXMIezKUnvEcrRUQyxK73My115nfPFk+9NDHjF68AbqFb
kJAKWlJQeV6FMuwb/v0GHOXaY5lNAAatMW3IiPz6cDiz8CrJpGtPKziBDVSbPois
nu98Y7xT/iHD6yoXvEUJiaSZGJVbqm+CSaUREvj/yeC3ss5iRpI5pl/1PJ4GrBMe
BJCTGAngn+ozAN41EVv4MU3NFdBwaqK20aXwqgGkxurNkK6yQVn30UmlSa82kKX/
v6rKf+ASag==
-----END CERTIFICATE-----
'''

    def test_public_account_login(self):
        # test data
        self.json_post_expect_200(
            '/erad/system/account/save',
            {
                "SystemKey": self.std_system_key,
                "AccountOwner": {"Email": "johndoe2@eleven.com", "Password": "TheEleven", "Account_ID": "AccountLogin"}
            },
            {
              "Error": None
            })

        result = self.json_post(
            '/erad/public/account/login',
            {
                "AccountOwner": {"Email": "johndoe2@eleven.com", "Password": "TheEleven"}
            })
        temp_data = result.json()
        self.json_post_expect_200(
            '/erad/admin/session/load',
            {
                "Session": {"ID": str(temp_data['Session_ID'])}
            },
            {
              "Error": None,
              "Session": {
                "AccountOwner": 1,
                "Account_ID": "AccountLogin",
                "Group_ID_List": [
                  "Global"
                ],
                "ID": str(temp_data['Session_ID']),
                "Identifier": "johndoe2@eleven.com",
                "LogoutUrl": "https://admin.enterpriseauth.com/#/login"
              }
            })

        self.json_post_expect_403(
            '/erad/public/account/login',
            {
                "AccountOwner": {"Email": "WRONG_USERNAME", "Password": "Eleven"}
            },
            {
                "Error": "Login failed."
            })
        self.json_post_expect_403(
            '/erad/public/account/login',
            {
                "AccountOwner": {"Email": "johndoe2@eleven.com", "Password": "WRONG_PASSWORD"}
            },
            {
                "Error": "Login failed."
            })
        self.json_post_expect_400(
            '/erad/public/account/login',
            {

            },
            {
              "Error": "Required parameter 'AccountOwner' is missing."
            })
        self.json_post_expect_400(
            '/erad/public/account/login',
            {
                "AccountOwner": {"Email1": "johndoe@eleven.com", "Password": "Eleven"}
            },
            {
              "Error": "Required parameter 'Email' is missing."
            })
        self.json_post_expect_400(
            '/erad/public/account/login',
            {
                "AccountOwner": {"Email": "johndoe@eleven.com", "Password1": "Eleven"}
            },
            {
              "Error": "Required parameter 'Password' is missing."
            })

    def test_public_accont_login_without_password(self):
        self.json_post_expect_400(
            '/erad/public/account/login',
            {
                "AccountOwner": {"Email": "johndoe2@eleven.com", "Password": ""}
            },
            {
                "Error": "Parameter 'Password' cannot be empty string."
            }
        )

        self.json_post_expect_400(
            '/erad/public/account/login',
            {
                "AccountOwner": {"Email": "johndoe2@eleven.com", "Password": None}
            },
            {
                "Error": "Parameter 'Password' is incorrect type (expected unicode string)."
            }
        )

    def test_public_account_save(self):
        self.json_post_expect_400(
            '/erad/public/account/save',
            {
                'NoField': None
            },
            {
              "Error": "Required parameter 'AccountOwner' is missing."
            }
        )
        self.json_post_expect_400(
            '/erad/public/account/save',
            {
                "AccountOwner": {"Password": "Eleven", "Account_ID": "Eleven Group"}
            },
            {
              "Error": "Required parameter 'Email' is missing."
            }
        )
        self.json_post_expect_400(
            '/erad/public/account/save',
            {
                "AccountOwner": {"Email": "johndoe@eleven.com", "Password1": "Eleven", "Account_ID": "Eleven Group"}
            },
            {
              "Error": "Required parameter 'Password' is missing."
            }
        )
        self.json_post_expect_200(
            '/erad/public/account/save',
            {
                "AccountOwner": {"Email": "johndoe@eleven.com", "Password": "password", "Account_ID": "Eleven Group"}
            },
            {
                "Error": "That password was found in a public list of previously compromised passwords. Please choose a different password."
            }
        )
        self.json_post_expect_200(
            '/erad/public/account/save',
            {
                "AccountOwner": {"Email": "johndoe@eleven.com", "Password": "PaSsWoRd", "Account_ID": "Eleven Group"}
            },
            {
                "Error": "That password was found in a public list of previously compromised passwords. Please choose a different password."
            }
        )
        self.json_post_expect_200(
            '/erad/public/account/save',
            {
                "AccountOwner": {"Email": "johndoe@eleven.com", "Password": "TheEleven", "Account_ID": "Eleven Group"}
            },
            {
              "Error": None
            }
        )
        self.json_post_expect_200(
            '/erad/public/account/save',
            {
                "AccountOwner": {"Email": "johndoe+no+accountid@eleven.com", "Password": "TheEleven"}
            },
            {
              "Error": None
            }
        )

    def test_public_accont_save_without_password(self):
        self.json_post_expect_400(
            '/erad/public/account/save',
            {
                "AccountOwner": {"Email": "johndoe2@eleven.com", "Password": ""}
            },
            {
                "Error": "Parameter 'Password' cannot be empty string."
            }
        )

        self.json_post_expect_400(
            '/erad/public/account/save',
            {
                "AccountOwner": {"Email": "johndoe2@eleven.com", "Password": None}
            },
            {
                "Error": "Parameter 'Password' is incorrect type (expected unicode string)."
            }
        )


    def test_apple_profile_create_tls(self):
        # can't currently test success because it tries to write a to a non-existent directory
        #self.json_post_expect_200(
        #    '/erad/public/apple/profile/create/tls',
        #    {
        #        "cacert": "YWRhc2Rkc2Fkc3M=",
        #        "clientCert": "YWRhc2Rkc2Fkc3M=",
        #        "profileName": "testuser",
        #        "vendor": "apple",
        #        "domain": "hilton.com",
        #        "isPasspoint": "true"
        #    }
        #)




        cert_dir = path.join(path.dirname(path.dirname(__file__)), 'certs')
        ios_signer_filename = path.join(cert_dir, 'signer-ios.pem')
        with open(ios_signer_filename, 'ab+') as f:
          f.seek(0)
          backup_content = f.read()

          f.seek(0)
          f.truncate()

          try:
            f.write(self.corrupted_signer_ios_pem)

            self.json_post_expect_500(
                '/erad/public/apple/profile/create/tls',
                {
                  "clientCert": "dGVzdAo=",
                  "cacert" : "dGVzdAo=",
                  "profileName": "testuser",
                  "vendor": "apple",
                  "domain": "example.com",
                  "username": "testing",
                  "Group_ID": "elevenos::Global",
                  "Account_ID": "elevenos"
                },
                {
                    "Error": "Exception"
                }
            )
            f.seek(0)
            f.truncate()
            f.write(backup_content)
            f.close()
          except Exception:
            f.seek(0)
            f.truncate()
            f.write(backup_content)
            f.close()



        self.json_post_expect_400(
            '/erad/public/apple/profile/create/tls',
            {
                "clientCert": "YWRhc2Rkc2Fkc3M=",
                "profileName": "testuser",
                "domain": "hilton.com",
                "isPasspoint": "true",
                "username": "testing",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            },
            {
                "Error": "Required parameter 'srv_cert_cn_or_cn_wildcard' is missing."
            }
        )
        self.json_post_expect_400(
            '/erad/public/apple/profile/create/tls',
            {
                "cacert": "YWRhc2Rkc2Fkc3M=",
                "profileName": "testuser",
                "domain": "hilton.com",
                "isPasspoint": "true",
                "username": "testing",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            },
            {
                "Error": "Required parameter 'clientCert' is missing."
            }
        )

        post_response =self.json_post(
           '/erad/public/apple/profile/create/tls',
           {
               "clientCert": "dGVzdAo=",
               "cacert" : "dGVzdAo=",
               "profileName": "testuser",
               "domain": "example.com",
               "username": "globaltest",
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
           }
        )
        self.assertEqual(post_response.status_code, 200)

        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)

        post_response =self.json_post(
           '/erad/public/apple/profile/create/tls',
           {
               "clientCert": "dGVzdAo=",
               "profileName": "testuser",
               "domain": "example.com",
               "srv_cert_cn_or_cn_wildcard": "example.com",
               "username": "globaltest",
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
           }
        )
        self.assertEqual(post_response.status_code, 200)
        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)

    def test_apple_profile_create(self):
        cert_dir = path.join(path.dirname(path.dirname(__file__)), 'certs')
        ios_signer_filename = path.join(cert_dir, 'signer-ios.pem')
        with open(ios_signer_filename, 'ab+') as f:
          f.seek(0)
          backup_content = f.read()
          f.seek(0)
          f.truncate()

          try:
            f.write(self.corrupted_signer_ios_pem)

            self.json_post_expect_500(
                '/erad/public/apple/profile/create',
                {
                   "cacert" : "dGVzdAo=",
                   "profileName": "testuser",
                   "username": "globaltest",
                   "password": "globaltest",
                   "vendor": "apple",
                   "ssid" : "test-ssid",
                   "domain": "example.com",
                   "Group_ID": "elevenos::Global",
                   "Account_ID": "elevenos"
                },
                {
                    "Error": "Exception"
                }
            )

            f.seek(0)
            f.truncate()
            f.write(backup_content)
            f.close()
          except Exception:
            f.seek(0)
            f.truncate()
            f.write(backup_content)
            f.close()


        self.json_post_expect_400(
            '/erad/public/apple/profile/create',
            {
                "profileName": "testuser",
                "domain": "hilton.com",
                "isPasspoint": "true",
                "username": "testing",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            },
            {
                "Error": "Required parameter 'srv_cert_cn_or_cn_wildcard' is missing."
            }
        )

        self.json_post_expect_400(
            '/erad/public/apple/profile/create',
            {
               "cacert" : "dGVzdAo=",
               "profileName": "testuser",
               "username" : "testing",
               "password" : "testing",
               "domain": "example.com",
               "username": "testing",
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
           },
            {
                "Error": "Required parameter 'ssid' is missing."
            }
        )

        post_response =self.json_post(
           '/erad/public/apple/profile/create',
           {
               "cacert" : "dGVzdAo=",
               "profileName": "testuser",
               "username": "globaltest",
               "password": "globaltest",
               "ssid" : "test-ssid",
               "domain": "example.com",
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
           }
        )
        self.assertEqual(post_response.status_code, 200)

        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)

        post_response =self.json_post(
           '/erad/public/apple/profile/create',
            {
               "srv_cert_cn_or_cn_wildcard": "example.com",
               "profileName": "testuser",
               "password" : "globaltest",
               "ssid" : "test-ssid",
               "domain": "example.com",
               "username": "globaltest",
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
           }
        )
        self.assertEqual(post_response.status_code, 200)
        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)

    def test_android_profile_create_tls(self):

        # bW9ja2NhCg==             - 'mockca'
        # bW9ja2NsaWVudGNlcnQK     - 'mockclientcert'

        post_response = self.json_post(
            '/erad/public/android/profile/create/tls',
            {
                "cacert": "bW9ja2NhCg==",
                "clientCert": "bW9ja2NsaWVudGNlcnQK",
                "Sha256-fingerprint": "aa:bb:cc:dd",
                "domain": "example.com",
                "username": "globaltest",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            }
        )
        self.assertEqual(post_response.status_code, 200)

        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)

        # Android 11 support profiles without root ca
        post_response = self.json_post(
            '/erad/public/android/profile/create/tls',
            {
                "cacert": "bW9ja2NhCg==",
                "clientCert": "bW9ja2NsaWVudGNlcnQK",
                "Sha256-fingerprint": "aa:bb:cc:dd",
                "domain": "example.com",
                "is_android_gte_11": True,
                "username": "globaltest",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            }
        )
        self.assertEqual(post_response.status_code, 200)

        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)

        post_response = self.json_post(
            '/erad/public/android/profile/create/tls',
            {
                "cacert": "testfreeradius.tk",
                "clientCert": "bW9ja2NsaWVudGNlcnQK",
                "Sha256-fingerprint": "aa:bb:cc:dd",
                "domain": "example.com",
                "username": "globaltest",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            }
        )
        self.assertEqual(post_response.status_code, 200)

        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)

        self.json_post_expect_200(
            '/erad/public/android/profile/create/tls',
            {
                "cacert": "bW9ja2NhCg==",
                "clientCert": "bW9ja2NsaWVudGNlcnQK",
                "Sha256-fingerprint": "aa:bb:cc:dd",
                "domain": "example.com",
                "username": "globaltest",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            },
            {
                "Error": None
            }
        )
        self.json_post_expect_400(
            '/erad/public/android/profile/create/tls',
            {
                "profileName": "testuser",
                "ssid": "test-ssid",
                "cacert": "testfreeradius.tk",
                "domain": "example.com",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            },
            {
                "Error": "Required parameter 'Sha256-fingerprint' is missing."
            }
        )

        self.json_post_expect_400(
            '/erad/public/android/profile/create/tls',
            {
                "profileName": "testuser",
                "ssid": "test-ssid",
                "Sha256-fingerprint": "aa:bb:cc:dd",
                "domain": "example.com",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            },
            {
                "Error": "Required parameter 'cacert' is missing."
            }
        )

    def test_android_profile_create(self):

        # bW9ja2NhCg==             - 'mockca'
        # bW9ja2NsaWVudGNlcnQK     - 'mockclientcert'

        self.json_post_expect_400(
            '/erad/public/android/profile/create',
            {
                "profileName": "testuser",
                "username": "testing",
                "password": "testing",
                "ssid": "test-ssid",
                "domain": "example.com",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            },
            {
                "Error": "Required parameter 'cacert' is missing."
            }
        )
        self.json_post_expect_200(
            '/erad/public/android/profile/create',
            {
                "profileName": "testuser",
                "username": "globaltest",
                "password": "globaltest",
                "ssid": "test-ssid",
                "cacert": "bW9ja2NhCg==",
                "domain": "example.com",
                "is_android_gte_11": True,
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            },
            {
                "Error": None
            }
        )
        # Android 11 support profiles without root ca
        self.json_post_expect_200(
            '/erad/public/android/profile/create',
            {
                "profileName": "testuser",
                "username": "globaltest",
                "password": "globaltest",
                "ssid": "test-ssid",
                "cacert": "bW9ja2NhCg==",
                "domain": "example.com",
                "Group_ID": "elevenos::Global",
                "Account_ID": "elevenos"
            },
            {
                "Error": None
            }
        )


    def test_windows_profile_create_return_url_to_file(self):
        """Check that API can return windows profile as file"""
        api_path = '/erad/public/windows/profile/create'

        post_response =self.json_post(
           api_path,
           {
               "profileName": "testuser",
               "username": "globaltest",
               "password": "globaltest",
               "ssid" : "test-ssid",
               "domain": "example.com",
               "srv_cert_cns": "testfreeradius.tk",
               "isPasspoint" : True,
               "return_file_content": False,
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
            }
        )
        self.assertEqual(post_response.status_code, 200)
        self.assertTrue('Url' in post_response.json())

    def test_windows_profile_create(self):

        api_path = '/erad/public/windows/profile/create'

        post_response =self.json_post(
           api_path,
           {
               "profileName": "testuser",
               "username": "globaltest",
               "password": "globaltest",
               "ssid" : "test-ssid",
               "domain": "example.com",
               "isPasspoint" : True,
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
            }
        )
        self.assertEqual(post_response.status_code, 200)

        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)
        self.assertTrue('NAIRealm' in response_data['Url'])
        self.assertTrue('enterpriseauth.com;www.enterpriseauth.com' in response_data['Url'])


        post_response =self.json_post(
           api_path,
           {
               "profileName": "testuser",
               "username" : "testing",
               "password" : "testing",
               "ssid" : "test-ssid",
               "domain": "example.com",
               "isPasspoint" : False,
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
            }
        )
        self.assertEqual(post_response.status_code, 200)

        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)
        self.assertFalse('NAIRealm' in response_data['Url'])
        self.assertTrue('enterpriseauth.com;www.enterpriseauth.com' in response_data['Url'])

        post_response =self.json_post(
           api_path,
           {
               "profileName": "testuser",
               "username" : "testing",
               "password" : "testing",
               "ssid" : "test-ssid",
               "domain": "example.com",
               "srv_cert_cns": "testfreeradius.tk",
               "isPasspoint" : True,
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
            }
        )
        self.assertEqual(post_response.status_code, 200)

        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)
        self.assertTrue('testfreeradius.tk' in response_data['Url'])

        self.json_post_expect_400(
            api_path,
            {
               "profileName": "testuser",
               "username" : "testing",
               "password" : "testing",
               "domain": "example.com",
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
            },
            {
                "Error": "Required parameter 'ssid' is missing."
            }
        )

        self.json_post_expect_200(
            api_path,
            {
               "profileName": "testuser",
               "username" : "testing",
               "password" : "testing",
               "ssid" : "test-ssid",
               "domain": "example.com",
               "isPasspoint" : False,
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
            },
            {
                "Error": None
            }
        )

    def test_status_check(self):
        # this method checks group existence
        # Erad_Group2.get('elevenos','SR-980-83')
        # in test db group is not exists so it return Error
        self.get_expect_200(
            '/erad/public/status/check',
            {
                "Error": "Item does not exist"
            }
        )


    def test_profile_download(self):
        self.get_expect(
            '/erad/public/profile/download',
            {
              "file": "filename"
            },
            None,
            403
        )

        response = requests.get(util.erad_cfg().api.host + '/erad/public/profile/download?file=asdfasdf')
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json()['Error'], 'The requested file does not exist.')



        post_response =self.json_post(
           '/erad/public/apple/profile/create',
            {
               "srv_cert_cn_or_cn_wildcard": "example.com",
               "profileName": "testuser",
               "username": "globaltest",
               "password": "globaltest",
               "ssid" : "test-ssid",
               "domain": "example.com",
               "Group_ID": "elevenos::Global",
               "Account_ID": "elevenos"
           }
        )
        self.assertEqual(post_response.status_code, 200)
        response_data = post_response.json()
        self.assertEqual(response_data['Error'], None)
        url = response_data['Url'].replace('https://api.enterpriseauth.com', '')

        get_response =self.get(url, {})

        self.assertEqual(get_response.status_code, 200)
        self.assertEqual(get_response.headers['Content-type'], 'application/octet-stream;charset=UTF-8')


    # def test_samsung_profile_create(self):
    #     self.json_post_expect_400(
    #         '/erad/public/samsung/profile/create',
    #         {
    #             "profileName": "testuser",
    #             "domain": "hilton.com",
    #             "isPasspoint": "true"
    #         },
    #         {
    #             "Error": "Required parameter 'username' is missing."
    #         }
    #     )
    #
    #     self.json_post_expect_200(
    #         '/erad/public/samsung/profile/create',
    #         {
    #            "cacert" : "dGVzdAo=",
    #            "profileName": "testuser",
    #            "username" : "testing",
    #            "password" : "testing",
    #            "domain": "example.com",
    #            "Group_ID": "elevenos::Global",
    #            "Account_ID": "elevenos"
    #         },
    #         {
    #             "Error": None
    #         }
    #     )

    def test_check_passworde(self):
        self.json_post_expect_200(
            '/erad/public/check_password',
            {
                "password": "password",
            },
            {
                "pwned": True
            }
        )

        self.json_post_expect_400(
            '/erad/public/check_password',
            {
            },
            {
                "Error": "Required parameter 'password' is missing."
            }
        )
