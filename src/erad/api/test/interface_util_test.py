#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad.exceptions import *
from ..interface_util import *
from .interface_test import *
from erad import util
from datetime import datetime, timedelta, timezone
import pytz


class InterfaceUtilTest(InterfaceTest):

    def setUp(self):
        self.utilI = EradInterfaceUtil()

    def test_expect_parameter(self):
        expect = "value"
        fields = {"key": "value"}
        result = self.utilI.expect_parameter(fields, "key")

        # Finds value at key
        self.assertEqual(result, expect )

        # Key not found
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter '.*' is missing.",
                                self.utilI.expect_parameter, fields, "missingno")


    def test_expect_dict(self):
        expect = {"k1":"v1", "k2":"v2"}
        fields = {"key": {"k1":"v1", "k2":"v2"}}
        result = self.utilI.expect_dict(fields, "key")

        # Finds dictionary at key
        self.assertEqual(result, expect )

        # Fail to find key
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter '.*' is missing.",
                                self.utilI.expect_dict, fields, "missingno")

        # Fail, find single value, not object
        bad_fields = {"key": "single value"}
        self.assertRaisesRegex(EradWrongTypeException, "Parameter '.*' is incorrect type \(expected an object\).",
                                self.utilI.expect_dict, bad_fields, "key")

    def test_set_unicode_value(self):
        model = Erad_Group2()
        fields = {"ID": str('value')}

        # Valid assignment
        self.utilI.set_unicode_value( model, fields, "ID")

        # Key not found
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter '.*' is missing.",
                                self.utilI.set_unicode_value, model, fields, "missingno")

        # Non-Unicode
        fields = {"ID": b"new"}
        self.assertRaisesRegex(EradWrongTypeException, "Parameter '.*' is incorrect type \(expected unicode string\).",
                                self.utilI.set_unicode_value, model, fields, "ID")

    def test_set_integer_value(self):
        model = Erad_Group2()

        # Valid assignment (int)
        fields = {"ID": int(42)}
        self.utilI.set_integer_value( model, fields, "ID")

        # Valid assignment
        fields = {"ID": int(4265498719816989898498498416549857951)}
        self.utilI.set_integer_value( model, fields, "ID")

        # Key not found
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter '.*' is missing.",
                                self.utilI.set_integer_value, model, fields, "missingno")

        # Non-Number
        fields = {"ID": "new"}
        self.assertRaisesRegex(EradWrongTypeException, "Parameter '.*' is incorrect type \(expected number\).",
                                self.utilI.set_integer_value, model, fields, "ID")

    def test_set_boolean_value(self):
        model = Erad_Group2()
        fields = {"ID": False}

        # Valid assignment
        self.utilI.set_boolean_value( model, fields, "ID", False)

        # Key not found
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter '.*' is missing.",
                                self.utilI.set_boolean_value, model, fields, "missingno", False)

        # Non-Unicode
        fields = {"ID": "new"}
        self.assertRaisesRegex(EradWrongTypeException, "Parameter '.*' is incorrect type \(expected bool\).",
                                self.utilI.set_boolean_value, model, fields, "ID", False)

    def test_set_unicode_array(self):
        model = Erad_Group2()

        # Valid assignment
        fields = {"ID": [str('val1'),str('val2')] }
        self.utilI.set_unicode_array( model, fields, "ID")

        # Valid assignment (single list item)
        fields = {"ID": [str('val1')] }
        self.utilI.set_unicode_array( model, fields, "ID")

        # single unicode failure
        fields = {"ID":str('val')}
        self.assertRaisesRegex(EradWrongTypeException, "Parameter '.*' is incorrect type \(expected array of unicode strings\).",
                                self.utilI.set_unicode_array, model, fields, "ID")

        # Key not found
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter '.*' is missing.",
                                self.utilI.set_unicode_array, model, fields, "missingno")

        # Non-Unicode
        fields = {"ID": "new"}
        self.assertRaisesRegex(EradWrongTypeException, "Parameter '.*' is incorrect type \(expected array of unicode strings\).",
                                self.utilI.set_unicode_array, model, fields, "ID")

    def test_get_older_sessions(self):
        old_session = Erad_Session()
        old_session.ID = "xxx123zzz456"
        old_session.Group_ID_List = [ "XX-YYYY", "AA-BBBB" ]
        old_session.Identifier = "jimmy.mcgill"
        old_session.Account_ID = "elevenos"
        old_session.AccountOwner = 1
        old_session.LogoutUrl = "http://1.1.1.1"
        date = datetime.now(pytz.utc) - timedelta(days=2)
        date.replace(tzinfo = pytz.utc)
        old_session.LastUsed = date
        old_session.save()


        today_session = Erad_Session()
        today_session.ID = "xxx123zzz458"
        today_session.Group_ID_List = [ "XX-YYYY", "AA-BBBB" ]
        today_session.Identifier = "jimmy.mcgill"
        today_session.Account_ID = "elevenos"
        today_session.AccountOwner = 1
        today_session.LogoutUrl = "http://1.1.1.1"
        today_session.LastUsed = datetime.now(pytz.utc)
        today_session.save()

        self.assertTrue(len(self.utilI.get_older_matching_sessions(today_session)) == 1)

        old_session1 = Erad_Session()
        old_session1.ID = "xxx123zzz459"
        old_session1.Group_ID_List = [ "XX-YYYY", "AA-BBBB" ]
        old_session1.Identifier = "jimmy.mcgill"
        old_session1.Account_ID = "elevenos"
        old_session1.AccountOwner = 1
        old_session1.LogoutUrl = "http://1.1.1.1"
        date = datetime.now(pytz.utc) - timedelta(days=1)
        date.replace(tzinfo = pytz.utc)
        old_session1.LastUsed = date
        old_session1.save()

        self.assertTrue(len(self.utilI.get_older_matching_sessions(today_session)) == 2)

        not_matching_old_session = Erad_Session()
        not_matching_old_session.ID = "xxx123zzz460"
        not_matching_old_session.Group_ID_List = [ "XX-YYYY", "AA-BBBB" ]
        not_matching_old_session.Identifier = "other-identifier"
        not_matching_old_session.Account_ID = "elevenos-other"
        not_matching_old_session.AccountOwner = 1
        not_matching_old_session.LogoutUrl = "http://1.1.1.1"
        date = datetime.now(pytz.utc) - timedelta(days=1)
        date.replace(tzinfo = pytz.utc)
        not_matching_old_session.LastUsed = date
        not_matching_old_session.save()

        self.assertTrue(len(self.utilI.get_older_matching_sessions(today_session)) == 2)

        not_old_enough_session = Erad_Session()
        not_old_enough_session.ID = "xxx123zzz460"
        not_old_enough_session.Group_ID_List = [ "XX-YYYY", "AA-BBBB" ]
        not_old_enough_session.Identifier = "jimmy.mcgill"
        not_old_enough_session.Account_ID = "elevenos"
        not_old_enough_session.AccountOwner = 1
        not_old_enough_session.LogoutUrl = "http://1.1.1.1"
        date = datetime.now(pytz.utc) - timedelta(hours=15)
        date.replace(tzinfo = pytz.utc)
        not_old_enough_session.LastUsed = date
        not_old_enough_session.save()

        self.assertTrue(len(self.utilI.get_older_matching_sessions(today_session)) == 2) # not_old_enough_session is not old enough to be added to the list.

        very_old_session = Erad_Session()
        very_old_session.ID = "xxx123zzz460"
        very_old_session.Group_ID_List = [ "XX-YYYY", "AA-BBBB" ]
        very_old_session.Identifier = "jimmy.mcgill"
        very_old_session.Account_ID = "elevenos"
        very_old_session.AccountOwner = 1
        very_old_session.LogoutUrl = "http://1.1.1.1"
        date = datetime.now(pytz.utc) - timedelta(days=91)
        date.replace(tzinfo = pytz.utc)
        very_old_session.LastUsed = date
        very_old_session.save()

        self.assertTrue(self.utilI._is_older_than(very_old_session, 90))
        self.assertFalse(self.utilI._is_older_than(very_old_session, 91))

        # Check if every attribute except LastUsed match
        self.assertTrue(self.utilI.session_matches(today_session, old_session))
        self.assertTrue(self.utilI.session_matches(today_session, not_old_enough_session)) # the sessions match, but it is not old enough.
        self.assertTrue(self.utilI.session_matches(today_session, old_session1))
        self.assertFalse(self.utilI.session_matches(today_session, not_matching_old_session))

        # Deletes matching sessions
        self.utilI.delete_older_sessions(today_session)

        self.assertTrue(len(self.utilI.get_older_matching_sessions(today_session)) == 0) # no more matching sessions.

    def test_is_password_blacklisted(self):
        self.assertEqual(self.utilI.is_password_blacklisted('password'), True)
        self.assertEqual(self.utilI.is_password_blacklisted('3bd6cf5ee8592c353ee85950b0d8b402'), False)

    def test_get_unique_devices_count_all_records_unique_calling_station_id(self):
        # empty log table
        start_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        end_time = start_time + timedelta(days=1)

        # multiply on 1000_0000 because AccountGroupIdIndex2.Instance field
        # consist of 2 values:
        # Number of seconds since the epoch UNIX timestamp
        # and a 4 (10 ^-4) digit random identifier.
        start_timestamp = util.get_log_instance_time(start_time)
        end_timestamp = util.get_log_instance_time(end_time)
        result = self.utilI.get_unique_devices_count(
            'test_account_id', 'test_group_id', (start_timestamp, end_timestamp)
        )
        self.assertEqual(result, 0)

        # Adding logs, all unique devices
        log_record_time_one = datetime(2020, 1, 1, 0, 1, tzinfo=timezone.utc)

        log_1 = Erad_Radius_Log(
            Group_ID="test_group_id",
            Instance=util.make_unique_instance(int(log_record_time_one.timestamp())),
            Group_Name="Test Group ID",
            AccountGroup_ID="test_account_id::test_group_id",
            PacketType="Access-Request",
            Username="supplicant_1x",
            CalledStationId="00:11:22:33:44:55",
            CallingStationId="00:00:00:00:00:01",
            EventTimestamp=log_record_time_one.strftime("%b %d %Y %H:%M:%S UTC"),
            NASIpAddress="1.1.1.1",
            Access="Access-Accept"
        )
        log_1.save()
        log_2 = Erad_Radius_Log(
            Group_ID="test_group_id",
            Instance=util.make_unique_instance(int(log_record_time_one.timestamp())),
            Group_Name="Test Group ID",
            AccountGroup_ID="test_account_id::test_group_id",
            PacketType="Access-Request",
            Username="supplicant_1x",
            CalledStationId="00:11:22:33:44:55",
            CallingStationId="00:00:00:00:00:02",
            EventTimestamp=log_record_time_one.strftime("%b %d %Y %H:%M:%S UTC"),
            NASIpAddress="1.1.1.1",
            Access="Access-Accept"
        )
        log_2.save()

        log_3 = Erad_Radius_Log(
            Group_ID="test_group_id",
            Instance=util.make_unique_instance(int(log_record_time_one.timestamp())),
            Group_Name="Test Group ID",
            AccountGroup_ID="test_account_id::test_group_id",
            PacketType="Access-Request",
            Username="supplicant_1x",
            CalledStationId="00:11:22:33:44:55",
            CallingStationId="00:00:00:00:00:03",
            EventTimestamp=log_record_time_one.strftime("%b %d %Y %H:%M:%S UTC"),
            NASIpAddress="1.1.1.1",
            Access="Access-Accept"
        )
        log_3.save()

        result = self.utilI.get_unique_devices_count(
            'test_account_id', 'test_group_id', (start_timestamp, end_timestamp)
        )
        self.assertEqual(result, 3)
        log_1.delete()
        log_2.delete()
        log_3.delete()

    def test_get_unique_devices_count_calling_station_id_repeated(self):
        # empty log table
        start_time = datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc)
        end_time = start_time + timedelta(days=1)

        # multiply on 1000_0000 because AccountGroupIdIndex2.Instance field
        # consist of 2 values:
        # Number of seconds since the epoch UNIX timestamp
        # and a 4 (10 ^-4) digit random identifier.
        start_timestamp = util.get_log_instance_time(start_time)
        end_timestamp = util.get_log_instance_time(end_time)
        result = self.utilI.get_unique_devices_count(
            'test_account_id', 'test_group_id', (start_timestamp, end_timestamp)
        )
        self.assertEqual(result, 0)

        # Adding logs, all unique devices
        log_record_time_one = datetime(2020, 1, 1, 0, 1, tzinfo=timezone.utc)

        log_1 = Erad_Radius_Log(
            Group_ID="test_group_id",
            Instance=util.make_unique_instance(int(log_record_time_one.timestamp())),
            Group_Name="Test Group ID",
            AccountGroup_ID="test_account_id::test_group_id",
            PacketType="Access-Request",
            Username="supplicant_1x",
            CalledStationId="00:11:22:33:44:55",
            CallingStationId="00:00:00:00:00:01",
            EventTimestamp=log_record_time_one.strftime("%b %d %Y %H:%M:%S UTC"),
            NASIpAddress="1.1.1.1",
            Access="Access-Accept"
        )
        log_1.save()
        log_11 = Erad_Radius_Log(
            Group_ID="test_group_id",
            Instance=util.make_unique_instance(int(log_record_time_one.timestamp())),
            Group_Name="Test Group ID",
            AccountGroup_ID="test_account_id::test_group_id",
            PacketType="Access-Request",
            Username="supplicant_1x",
            CalledStationId="00:11:22:33:44:55",
            CallingStationId="00:00:00:00:00:01",
            EventTimestamp=log_record_time_one.strftime("%b %d %Y %H:%M:%S UTC"),
            NASIpAddress="1.1.1.1",
            Access="Access-Accept"
        )
        log_11.save()
        log_2 = Erad_Radius_Log(
            Group_ID="test_group_id",
            Instance=util.make_unique_instance(int(log_record_time_one.timestamp())),
            Group_Name="Test Group ID",
            AccountGroup_ID="test_account_id::test_group_id",
            PacketType="Access-Request",
            Username="supplicant_1x",
            CalledStationId="00:11:22:33:44:55",
            CallingStationId="00:00:00:00:00:02",
            EventTimestamp=log_record_time_one.strftime("%b %d %Y %H:%M:%S UTC"),
            NASIpAddress="1.1.1.1",
            Access="Access-Accept"
        )
        log_2.save()
        log_22 = Erad_Radius_Log(
            Group_ID="test_group_id",
            Instance=util.make_unique_instance(int(log_record_time_one.timestamp())),
            Group_Name="Test Group ID",
            AccountGroup_ID="test_account_id::test_group_id",
            PacketType="Access-Request",
            Username="supplicant_1x",
            CalledStationId="00:11:22:33:44:55",
            CallingStationId="00:00:00:00:00:02",
            EventTimestamp=log_record_time_one.strftime("%b %d %Y %H:%M:%S UTC"),
            NASIpAddress="1.1.1.1",
            Access="Access-Accept"
        )
        log_22.save()

        log_3 = Erad_Radius_Log(
            Group_ID="test_group_id",
            Instance=util.make_unique_instance(int(log_record_time_one.timestamp())),
            Group_Name="Test Group ID",
            AccountGroup_ID="test_account_id::test_group_id",
            PacketType="Access-Request",
            Username="supplicant_1x",
            CalledStationId="00:11:22:33:44:55",
            CallingStationId="00:00:00:00:00:03",
            EventTimestamp=log_record_time_one.strftime("%b %d %Y %H:%M:%S UTC"),
            NASIpAddress="1.1.1.1",
            Access="Access-Accept"
        )
        log_3.save()

        result = self.utilI.get_unique_devices_count(
            'test_account_id', 'test_group_id', (start_timestamp, end_timestamp)
        )
        self.assertEqual(result, 3)
        log_1.delete()
        log_11.delete()
        log_2.delete()
        log_22.delete()
        log_3.delete()

    def test_get_log_instance_time(self):
        """
        Testing that get_log_instance_time() produce values in same dimensions as `util.make_unique_instance()`
        :return:
        """
        now = datetime.now()

        util_time = util.get_log_instance_time(now)
        auditor_time = util.make_unique_instance(now.timestamp())

        str_util_time = str(util_time)
        str_auditor_time = str(auditor_time)
        self.assertEqual(len(str_util_time), len(str_auditor_time))

    def test_load_client_arns(self):
        self.assertEqual(util.load_client_arns(None), (None, None))
        self.assertEqual(util.load_client_arns(""), (None, None))
        self.assertEqual(util.load_client_arns({}), (None, None))
        self.assertEqual(util.load_client_arns("[2]"), (None, None))
        self.assertEqual(util.load_client_arns("{\"test\": 2}"), ("test", 2))


if __name__ == '__main__':
    unittest.main()
