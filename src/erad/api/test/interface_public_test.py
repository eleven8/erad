#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad.exceptions import *
from ..interface_public import *
from ..interface_admin import *
from ..interface_system import *
from .interface_test import *
from os import path


import logging

logging.basicConfig()

logger = logging.getLogger(__name__)


class InterfacePublicTest(InterfaceTest):

    def setUp(self):
        self.myPublicI = EradInterfacePublic()
        self.myAdminI = EradInterfaceAdmin()
        self.mySystemI = EradInterfaceSystem()

    def test_account_login(self):
        # add test data
        input = self.get_input_account_save("test@elevenwireless.com", "TheEleven", "Eleven Wireless")
        self.mySystemI.account_save(input)

        group = Erad_Group2("Eleven Wireless", ID='AccountGroup1', Name="Mac support test", RemoteServerUrl="remote.fake.com",
                                              SharedSecret="RandomKey", TimeZone = "Asia/Qyzylorda")
        group.save()

        expected = ["AccountGroup1", "Global"]

        # Test valid login
        input = self.get_input_account_login("test@elevenwireless.com", "TheEleven")
        results = self.myPublicI.account_login(input)
        session = self.myAdminI.session_load({"Session": {"ID": str(results['Session_ID'])}})
        self.assert_is_superset(expected, session['Session']['Group_ID_List'])

        # Test no AccountOwner key
        input = self.get_input_account_login("test@elevenwireless.com", "TheEleven")
        del(input["AccountOwner"])
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'AccountOwner' is missing.", self.myPublicI.account_login,input)

        # Test no Email
        input = self.get_input_account_login("test@elevenwireless.com", "TheEleven")
        del(input["AccountOwner"]['Email'])
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'Email' is missing.", self.myPublicI.account_login,input)

        # Test no Password
        input = self.get_input_account_login("test@elevenwireless.com", "TheEleven")
        del(input["AccountOwner"]['Password'])
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'Password' is missing.", self.myPublicI.account_login,input)

        # Test wrong email
        input = self.get_input_account_login("test@WRONG.com", "TheEleven")
        self.assertRaisesRegex(EradAccessException, "Login failed.", self.myPublicI.account_login,input)

        # Test wrong password
        input = self.get_input_account_login("test@elevenwireless.com", "WRONG!")
        self.assertRaisesRegex(EradAccessException, "Login failed.", self.myPublicI.account_login,input)

    def test_account_save(self):
        # Test no AccountOwner key
        payload = self.get_input_account_save(
            "test+account+save@elevenwireless.com",
            "Eleven",
            "Eleven Wireless"
        )

        del (payload["AccountOwner"])

        self.assertRaisesRegex(
            EradRequiredFieldException,
            "Required parameter 'AccountOwner' is missing.",
            self.myPublicI.account_save,
            payload
        )

        # Test no Email
        payload = self.get_input_account_save(
            "test+account+save@elevenwireless.com",
            "Eleven",
            "Eleven Wireless"
        )

        del (payload["AccountOwner"]['Email'])

        self.assertRaisesRegex(
            EradRequiredFieldException,
            "Required parameter 'Email' is missing.",
            self.myPublicI.account_save,
            payload
        )

        # Test no Password
        payload = self.get_input_account_save(
            "test+account+save@elevenwireless.com",
            "Eleven",
            "Eleven Wireless"
        )

        del (payload["AccountOwner"]['Password'])

        self.assertRaisesRegex(
            EradRequiredFieldException,
            "Required parameter 'Password' is missing.",
            self.myPublicI.account_save,
            payload
        )

        # Test blacklisted password
        payload = self.get_input_account_save(
            "test+account+save@elevenwireless.com",
            "password",
            "password"
        )

        self.assertRaisesRegex(
            EradBlacklistedPasswordException,
            "That password was found in a public list of previously compromised passwords. Please choose a different password.",
            self.myPublicI.account_save,
            payload
        )

        # Check if blacklisted password checks are case insensitive
        payload = self.get_input_account_save(
            "test+account+save@elevenwireless.com",
            "PaSsWoRd",
            "Eleven Wireless"
        )

        self.assertRaisesRegex(
            EradBlacklistedPasswordException,
            "That password was found in a public list of previously compromised passwords. Please choose a different password.",
            self.myPublicI.account_save,
            payload
        )

        # Test valid save
        payload = self.get_input_account_save(
            "test+account+save@elevenwireless.com",
            "TheEleven",
            "Eleven Wireless 2"
        )
        payload_existing_account_id = self.get_input_account_save(
            "non+existing+email@elevenwireless.com",
            "TheEleven",
            "Eleven Wireless"
        )

        self.myPublicI.account_save(payload)

        # Try to create an account with the email that already exists.
        self.assertRaisesRegex(
            EradValidationException,
            'The email already exists.',
            self.myPublicI.account_save,
            payload
        )

        # Try to create an account ID that already exists.
        self.assertRaisesRegex(
            EradValidationException,
            'The account ID already exists.',
            self.myPublicI.account_save,
            payload_existing_account_id
        )

        # Test no Account_ID
        payload = self.get_input_account_save(
            "test+no+accountid@elevenwireless.com",
            "TheEleven",
            "Eleven Wireless"
        )

        del (payload["AccountOwner"]['Account_ID'])

        self.myPublicI.account_save(payload)

        # Test create account without available endpoint
        payload = self.get_input_account_save(
            "test+no+endpoint@elevenwireless.com",
            "noendpoint",
            "No endpoint"
        )

        # Try to create an account that already exists.
        # self.assertRaisesRegexp(
        #     EradValidationException,
        #     "No more endpoints available",
        #     self.myPublicI.account_save,
        #     payload
        # )

    def test_account_save_with_blank_password(self):
        payload = self.get_input_account_save(
            "test+blank+pwd@elevenwireless.com",
            "",
            "Blank password"
        )

        self.assertRaisesRegex(
            EradWrongTypeException,
            "Parameter 'Password' cannot be empty string.",
            self.myPublicI.account_save,
            payload
        )

    def test_account_save_with_nonetype_password(self):
        payload = self.get_input_account_save(
            "test+blank+pwd@elevenwireless.com",
            None,
            "Blank password"
        )

        # Set is as None, not unicode(None)
        payload['AccountOwner']['Password'] = None

        self.assertRaisesRegex(
            EradWrongTypeException,
            "Parameter 'Password' is incorrect type \(expected unicode string\).",
            self.myPublicI.account_save,
            payload
        )

    def test_account_save_with_invalid_email(self):
        payload = self.get_input_account_save(
            "invalid-email",
            "invalid-email",
            "Invalid email"
        )

        self.assertRaisesRegex(
            EradValidationException,
            "The email invalid-email is not valid.",
            self.myPublicI.account_save,
            payload
        )

    def test_account_login_with_blank_password(self):
        payload = self.get_input_account_login(
            "test+blank+pwd@elevenwireless.com",
            ""
        )

        self.assertRaisesRegex(
            EradWrongTypeException,
            "Parameter 'Password' cannot be empty string.",
            self.myPublicI.account_login,
            payload
        )

    def test_account_login_with_nonetype_password(self):
        payload = self.get_input_account_login(
            "test+blank+pwd@elevenwireless.com",
            None
        )

        # Set is as None, not unicode(None)
        payload['AccountOwner']['Password'] = None

        self.assertRaisesRegex(
            EradWrongTypeException,
            "Parameter 'Password' is incorrect type \(expected unicode string\).",
            self.myPublicI.account_login,
            payload
        )

    def test_store_metadata(self):
        # Create Supplicant
        supplicant = Erad_Supplicant()
        supplicant.Username = "metadata-1"
        supplicant.Group_ID = "elevenos"+"::"+"Global"
        supplicant.Account_ID = "elevenos"
        supplicant.Password = "sdkmskdsd"
        supplicant.save()

        input = {
            'username': "metadata-1",
            'password': "sdkmskdsd",
            'vendor': "apple",
            'isPasspoint': True,
            "domain": "enterpriseauth.com",
            "Group_ID": "elevenos::Global",
            "Account_ID": "elevenos"
        }

        metadata = {
            "domain": "enterpriseauth.com",
            "rcoi": "8ada26",
            "nairealm": "enterpriseauth.com"
        }

        self.assertIsNone(supplicant.CustomJsonData)

        # Save metadata to supplicant
        self.myPublicI.store_metadata(input, "metadata-1", **metadata)
        supplicant.refresh()
        self.assertIsNotNone(supplicant.CustomJsonData)

        self.assert_is_superset(metadata, json.loads(supplicant.CustomJsonData))

        # Confirm that field in CustomJsonData gets updated if passed.
        metadata.update({"domain": "test.com"})
        self.myPublicI.store_metadata(input, "metadata-1", **metadata)
        supplicant.refresh()

        self.assert_is_superset(metadata, json.loads(supplicant.CustomJsonData))

    def test_get_android_rcois(self):
        # test default value
        value = self.myPublicI.get_android_rcois(None)
        self.assertEqual(value, False)

        value = self.myPublicI.get_android_rcois("")
        self.assertEqual(value, False)

        value = self.myPublicI.get_android_rcois([])
        self.assertEqual(value, False)

        value = self.myPublicI.get_android_rcois(["8ada26", "90df2s"])
        self.assertEqual(value, "8ada26,90df2s")

    def test_get_apple_rcois(self):
        # test default value
        value = self.myPublicI.get_apple_rcois(None)
        self.assertEqual(value, False)

        value = self.myPublicI.get_apple_rcois("")
        self.assertEqual(value, False)

        value = self.myPublicI.get_apple_rcois([])
        self.assertEqual(value, False)

        value = self.myPublicI.get_apple_rcois(["8ada26", "90df2s"])
        self.assertEqual(value, "<string>8ada26</string><string>90df2s</string>")

    def test_get_windows_rcois(self):
        # test default value
        value = self.myPublicI.get_windows_rcois(None)
        self.assertEqual(value, "")

        value = self.myPublicI.get_windows_rcois("")
        self.assertEqual(value, "")

        value = self.myPublicI.get_windows_rcois([])
        self.assertEqual(value, "")

        value = self.myPublicI.get_windows_rcois(["8ada26", "90df2s"])

        expecected = '''<RoamingConsortium>
        <OUI>8ada26</OUI><OUI>90df2s</OUI>
    </RoamingConsortium>'''

        self.assertEqual(value, expecected)

    def test_get_sing_cert_path(self):

        # try to use not supported devices
        try:
            self.myPublicI.get_sing_cert_path('default', 'android')
        except Exception as err:
            self.assertEqual(err.args[0], "Device type is not supported for signing")

        # try get cert for ios
        result = self.myPublicI.get_sing_cert_path('default', 'ios')
        result = tuple(map(lambda x: path.split(x)[1], result))

        expected = (
            'signer-ios.pem',
            'signer-ios.key',
            'signer-ios.ca'
        )

        self.assertEqual(expected, result)

        result = self.myPublicI.get_sing_cert_path('default', 'windows')
        result = tuple(map(lambda x: path.split(x)[1], result))

        expected = (
            'signer-windows.pem',
            'signer-windows.key',
            'signer-windows.ca'
        )

        self.assertEqual(expected, result)

        result = self.myPublicI.get_sing_cert_path('calix-staging', 'windows')
        result = tuple(map(lambda x: path.split(x)[1], result))

        expected = (
            'signer-windows-calix.pem',
            'signer-windows-calix.key',
            'signer-windows-calix.ca'
        )

        self.assertEqual(expected, result)

        result = self.myPublicI.get_sing_cert_path('calix-staging', 'ios')
        result = tuple(map(lambda x: path.split(x)[1], result))

        expected = (
            'signer-ios-calix.pem',
            'signer-ios-calix.key',
            'signer-ios-calix.ca'
        )

        self.assertEqual(expected, result)

        result = self.myPublicI.get_sing_cert_path('calix', 'ios')
        result = tuple(map(lambda x: path.split(x)[1], result))

        expected = (
            'signer-ios-calix-prod.pem',
            'signer-ios-calix-prod.key',
            'signer-ios-calix-prod.ca'
        )

        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()
