from erad.api.erad_model import Erad_Supplicant
from erad.api.erad_model import model_to_pod
from unittest import TestCase


class TestModels(TestCase):
	def test_parent_index(self):
		"""Just to make sure that index that uses fields that can be null return empty result, but data still exists"""

		new_supplicant = Erad_Supplicant(
			'testaccount::testgroup',
			'testusername'
		)
		new_supplicant.save()

		result = Erad_Supplicant.Parent_ID_index.query('new@example.com')
		self.assertEqual(len([x for x in result]), 0)

		# no exception data exists
		result = Erad_Supplicant.get('testaccount::testgroup', 'testusername')

		new_supplicant2 = Erad_Supplicant(
			'testaccount::testgroup',
			'testusername2',
			Parent_ID='new@example.com',
			Account_ID='testaccount'

		)
		new_supplicant2.save()

		result = Erad_Supplicant.Parent_ID_index.query(
			'new@example.com', (Erad_Supplicant.Account_ID == 'testaccount')
		)
		self.assertEqual(len([x for x in result]), 1)

		new_supplicant.delete()
		new_supplicant2.delete()

	def test_model_to_prod(self):
		new_supplicant = Erad_Supplicant(
			'testaccount::testgroup',
			'testusername'
		)

		expected = {
			'Account_ID': None,
			'CustomJsonData': None,
			'Description': None,
			'DeviceName': None,
			'ExpirationDate': None,
			'Group_ID': 'testaccount::testgroup',
			'Location': None,
			'MaxDownloadSpeedBits': None,
			'MaxUploadSpeedBits': None,
			'Parent_ID': None,
			'Password': None,
			'UseRemote': None,
			'UseWildcard': None,
			'Username': 'testusername',
			'Vlan': None,
			'device_used': None,
			'profile_used': None
		}

		result = model_to_pod(new_supplicant)
		self.assertEqual(result, expected)

