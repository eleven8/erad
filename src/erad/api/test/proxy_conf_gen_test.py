#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad import util, CoreTest

class ProxyConfGenTest(CoreTest):

   def test_proxy_conf_gen(self):
      pass
#      proxy_servers = []
#      proxy_server_info = {}
#      proxy_server_info2 =  {}
#      proxy_server_info['ipaddr'] = '10.11.28.165'
#      proxy_server_info['port'] = 1812
#      proxy_server_info['secret'] = 'noneyadb'
#      proxy_server_info['name'] = 'marriott_proxy_server'
#      proxy_server_info2['ipaddr'] = '165.28.11.10'
#      proxy_server_info2['port'] = 1813
#      proxy_server_info2['secret'] = 'doteleven123'
#      proxy_server_info2['name'] = 'eleven_proxy_server'
#      proxy_servers.append(proxy_server_info)
#      proxy_servers.append(proxy_server_info2)
#
#      actual = EradInterfaceAdmin().build_proxy_conf_from_server_list(proxy_servers)
#
#      expected = """
#proxy server {
#    default_fallback = no
#}
#
#
#
#
#
#
#home_server marriott_proxy_server {
#    type = auth
#    ipaddr = 10.11.28.165
#    port = 1812
#    secret = noneyadb
#    response_window = 20
#    zombie_period = 40
#    revive_interval = 120
#    status_check = status-server
#    check_interval = 30
#    check_timeout = 4
#    num_answers_to_alive = 3
#    max_outstanding = 65536
#}
#
#
#home_server eleven_proxy_server {
#    type = auth
#    ipaddr = 165.28.11.10
#    port = 1813
#    secret = doteleven123
#    response_window = 20
#    zombie_period = 40
#    revive_interval = 120
#    status_check = status-server
#    check_interval = 30
#    check_timeout = 4
#    num_answers_to_alive = 3
#    max_outstanding = 65536
#}
#
#
#
#
#
#
#home_server_pool marriott_proxy_server_pool {
#    type = fail-over
#    home_server = marriott_proxy_server
#}
#home_server_pool eleven_proxy_server_pool {
#    type = fail-over
#    home_server = eleven_proxy_server
#}
#
#
#
#realm LOCAL {
#    #  If we do not specify a server pool, the realm is LOCAL, and
#    #  requests are not proxied to it.
#}
#
#
#
#
#realm marriott_proxy_server {
#    auth_pool = marriott_proxy_server_pool
#}
#realm eleven_proxy_server {
#    auth_pool = eleven_proxy_server_pool
#}
#
#        """
#
#      self.assertEqual( expected, actual, "proxy conf output doesn't match test input")

