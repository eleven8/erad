#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad.exceptions import *
from ..interface_integration import *
from ..interface_admin import *
from .interface_test import *
from erad.api.erad_model import Erad_ApiKey
from erad.api.erad_model import Erad_Group2
from pynamodb import exceptions as pynamodb_excp

import datetime




class InterfaceIntegrationTest(InterfaceTest):

    def setUp(self):
        self.maxDiff = None
        self.myIntegrationI = EradInterfaceIntegration()
        self.myAdminI = EradInterfaceAdmin()

    def test_authorize_integration_key(self):
        # Test valid key
        input = self.get_input_integration_key()
        self.myIntegrationI.authorize_integration_key(input)

        # Test no key
        input = {"wrong":"a bad object"}
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter 'IntegrationKey' is missing.", self.myIntegrationI.authorize_integration_key,input)

        # Test invalid key
        input = {"IntegrationKey" : "0987654321poiuytrewq"}
        self.assertRaisesRegex(EradAccessException, "Access Denied.", self.myIntegrationI.authorize_integration_key,input)

        # Test disabled key
        input = {"IntegrationKey" : self.std_integration_key_disabled}
        self.assertRaisesRegex(EradAccessException, "Key disabled.", self.myIntegrationI.authorize_integration_key,input)

    def test_session_save(self):
        # test with AccountOwner false
        expect = self.set_input_session("guid2", "test_guy", "fake.logout.com", ['1', '2'], 0)

        # Valid Input
        input = self.set_input_key_session("guid2", "test_guy", "fake.logout.com", ['1', '2'], 0)
        result = self.myIntegrationI.session_save(input)

        self.assertEqual(result["Session"]["ID"], expect["Session"]["ID"])
        self.assertEqual(result["Session"]["Identifier"], expect["Session"]["Identifier"])
        self.assertEqual(result["Session"]["LogoutUrl"], expect["Session"]["LogoutUrl"])
        self.assertEqual(result["Session"]["Group_ID_List"], expect["Session"]["Group_ID_List"])

        # Is audit recorded?

        # convert dates to instances in order to search with them
        now = util.utc_to_local( datetime.datetime.utcnow(), 'America/Los_Angeles' )
        before = util.utc_to_local( datetime.datetime.utcnow() - datetime.timedelta(seconds=5), 'America/Los_Angeles' )

        after_date_instance =  Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(now), 0.0 )
        before_date_instance = Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(before), 0.0 )

        for group in expect["Session"]["Group_ID_List"]:
            # search for audit records
            audit = Erad_Audit.accountgroupid_index.query(
                group,
                AccountGroupIdIndex.Instance.between(after_date_instance, before_date_instance)
            )
            self.assertIsNotNone(audit)

        # Is it actually in our DB?
        obj = Erad_Session(ID = "guid2")
        obj.refresh(consistent_read=True)
        self.assertIsNotNone(obj)

        # Invalid parameter doesn't save
        input = self.set_input_key_session("guid3", 1, "fake.logout.com", "so Invalid", 0)
        self.assertRaisesRegex(EradWrongTypeException, "Parameter .* is incorrect type \(expected .*\).", self.myIntegrationI.session_save,input)
        obj = Erad_Session(ID = "guid3")
        self.assertRaisesRegex(DoesNotExist, "This item does not exist in the table.",obj.refresh)

        # Is audit recorded?

        # convert dates to instances in order to search with them
        now = util.utc_to_local( datetime.datetime.utcnow(), 'America/Los_Angeles' )
        before = util.utc_to_local( datetime.datetime.utcnow() - datetime.timedelta(seconds=5), 'America/Los_Angeles' )

        after_date_instance =  Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(now), 0.0 )
        before_date_instance = Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(before), 0.0 )

        for group in expect["Session"]["Group_ID_List"]:
            # search for audit records
            audit = Erad_Audit.accountgroupid_index.query(
                group,
                AccountGroupIdIndex.Instance.between(after_date_instance, before_date_instance)
            )
            self.assertIsNotNone(audit)

        # test with AccountOwner true
        expect = self.set_input_session(
                    "guid2",
                    "test_guy",
                    "fake.logout.com",
                    [
                        '1',
                        '2',
                        '222',
                        '88',
                        '88:88:88:88:88:88',
                        'AB-CD-1002',
                        'FF-1925',
                        'Global',
                        'XX-YYYY',
                        'blacksmith',
                        'bubbletest',
                        'foo',
                        'fooremoteattr',
                        'group-test_admin_supplicant_import_csv_from_excel',
                        'group88888',
                        'henrysmith',
                        'scooby',
                        'test_group_save__optional_fields'
                    ],
                    1
                )
        # Valid Input
        input = self.set_input_key_session("guid2", "test_guy", "fake.logout.com", ['1', '2'], 1)
        result = self.myIntegrationI.session_save(input)

        self.assertEqual(result["Session"]["ID"], expect["Session"]["ID"])
        self.assertEqual(result["Session"]["Identifier"], expect["Session"]["Identifier"])
        self.assertEqual(result["Session"]["LogoutUrl"], expect["Session"]["LogoutUrl"])
        self.assertEqual(set(result["Session"]["Group_ID_List"]), set(expect["Session"]["Group_ID_List"]))

        # Is it actually in our DB?
        obj = Erad_Session(ID = "guid2")
        obj.refresh(consistent_read=True)
        self.assertIsNotNone(obj)

        # Is audit recorded?

        # convert dates to instances in order to search with them
        now = util.utc_to_local( datetime.datetime.utcnow(), 'America/Los_Angeles' )
        before = util.utc_to_local( datetime.datetime.utcnow() - datetime.timedelta(seconds=5), 'America/Los_Angeles' )

        after_date_instance =  Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(now), 0.0 )
        before_date_instance = Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch(before), 0.0 )

        for group in expect["Session"]["Group_ID_List"]:
            # search for audit records
            audit = Erad_Audit.accountgroupid_index.query(
                group,
                AccountGroupIdIndex.Instance.between(after_date_instance, before_date_instance)
            )
            self.assertIsNotNone(audit)


    def test_group_load(self):
        expect = {"Group": {'Account_ID':'elevenos',
                            'ID':'1',
                            'Domain': None,
                            'MaxDownloadSpeedBits':0,
                            'MaxUploadSpeedBits':0,
                            'Name':'Group1',
                            'RemoteServerUrl':'Remote.com',
                            'SharedSecret':'321cba',
                            'TimeZone':'Asia/Qyzylorda',
                            'OverrideVlan': False,
                            'OverrideConnectionSpeed': False,
                            'ClientInfoArn': "{\"test\": 2}",
                            }}

        Erad_Group2.save( Erad_Group2(Account_ID = "elevenos", ID="1", Name="Group1", RemoteServerUrl="Remote.com",
                                              SharedSecret="321cba", TimeZone="Asia/Qyzylorda", MaxDownloadSpeedBits=0, MaxUploadSpeedBits=0, ClientInfoArn="{\"test\": 2}") )

        # Valid group
        input = self.get_input_key_group("1")
        result = self.myIntegrationI.group_load(input)
        self.assertEqual( result, expect )

        # Invalid group
        input = self.get_input_key_group("22222")
        self.assertRaisesRegex(DoesNotExist, "Item does not exist",
                                self.myIntegrationI.group_load, input)

    def test_group_save__success(self):
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["222"], Account_ID="elevenos", LastUsed=util.utcnow()) )
        expect = {"Group": {'Account_ID':'elevenos',
                    'ID':'2',
                    'Domain':'clientdomain.com',
                    'MaxDownloadSpeedBits':0,
                    'MaxUploadSpeedBits':0,
                    'Name':'Group2',
                    'RemoteServerUrl':'Remote.com',
                    'SharedSecret':'abc123',
                    'TimeZone':'Asia/Qyzylorda',
                    'OverrideVlan': False,
                    'OverrideConnectionSpeed': False,
                    'ClientInfoArn': "{\"test\": 2}",
                    }}

        # Valid group
        # input = self.set_input_key_group("2", "Group2", "Remote.com", "abc123","Asia/Qyzylorda")
        input = {"IntegrationKey" :"i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF",
                 "Group": {'ID':'2',
                           'Domain':'clientdomain.com',
                           'MaxDownloadSpeedBits':0,
                           'MaxUploadSpeedBits':0,
                           'Name':'Group2',
                           'RemoteServerUrl':'Remote.com',
                           'SharedSecret':'abc123',
                           'TimeZone':"Asia/Qyzylorda",
                           'ClientInfoArn': "{\"test\": 2}"}
                 }
        result = self.myIntegrationI.group_save(input)
        self.assertEqual(result, expect)

        # Is it actually in our DB?
        obj = Erad_Group2(Account_ID="elevenos", ID = "2")
        obj.refresh(consistent_read=True)
        self.assertIsNotNone(obj)

        # Is the Authenticator in our DB?
        obj = Erad_Authenticator2(ID = "2", Account_ID='elevenos')
        obj.refresh(consistent_read=True)
        self.assertIsNotNone(obj)

    def test_group_save_optional_fields__success(self):
        test_session = Erad_Session(
                            ID=str("unit_test_session"),
                            Group_ID_List=["222"],
                            Account_ID="elevenos",
                            LastUsed=util.utcnow()
                        )
        test_session.save()

        expect = {"Group": {'Account_ID':'elevenos',
                    'ID':'2',
                    'Domain':'clientdomain.com',
                    'MaxDownloadSpeedBits':0,
                    'MaxUploadSpeedBits':0,
                    'Name':'Group2',
                    'RemoteServerUrl': None,
                    'SharedSecret': None,
                    'TimeZone':'Asia/Qyzylorda',
                    'OverrideVlan': True,
                    'OverrideConnectionSpeed': True,
                    'ClientInfoArn': "{\"test\": 2}",
                    }}

        # Valid group
        # input = self.set_input_key_group("2", "Group2", "Remote.com", "abc123","Asia/Qyzylorda")
        input = {"IntegrationKey" :"i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF",
                 "Group": {
                            'ID':'2',
                            'Domain':'clientdomain.com',
                            'MaxDownloadSpeedBits':0,
                            'MaxUploadSpeedBits':0,
                            'Name':'Group2',
                            'RemoteServerUrl': None,
                            'SharedSecret': None,
                            'TimeZone':"Asia/Qyzylorda",
                            'OverrideVlan': True,
                            'OverrideConnectionSpeed': True,
                            'ClientInfoArn': "{\"test\": 2}",
                        }
                 }
        result = self.myIntegrationI.group_save(input)
        self.assertEqual(result, expect)

        # Is it actually in our DB?
        obj = Erad_Group2(Account_ID="elevenos", ID = "2")
        obj.refresh(consistent_read=True)
        self.assertIsNotNone(obj)

        # Is the Authenticator in our DB?
        obj = Erad_Authenticator2(ID = "2", Account_ID='elevenos')
        obj.refresh(consistent_read=True)
        self.assertIsNotNone(obj)

    def test_group_save__fail(self):
        apikey = Erad_ApiKey(
                    'the_api_key',
                    Account_ID='the_account_id',
                    Active=1,
                    ApiKeyName='the_api_key_name'
                )
        apikey.save()

        input = {"IntegrationKey" :"wront_integration_key",
                 "Group": {'ID':'the_group_id',
                           'Domain':'clientdomain.com',
                           'MaxDownloadSpeedBits':0,
                           'MaxUploadSpeedBits':0,
                           'Name':'the_group_name',
                           'RemoteServerUrl':'Remote.com',
                           'SharedSecret':'abc123',
                           'TimeZone':"Asia/Qyzylorda"}
                 }

        with self.assertRaisesRegex(EradAccessException, 'Access Denied.'):
            self.myIntegrationI.group_save(input)


        with self.assertRaisesRegex(pynamodb_excp.DoesNotExist, 'This item does not exist in the table.'):
            obj = Erad_Group2(Account_ID="the_account_id", ID = "the_group_id")
            obj.refresh(consistent_read=True)


        with self.assertRaisesRegex(pynamodb_excp.DoesNotExist, 'This item does not exist in the table.'):
            obj = Erad_Authenticator2(ID = "the_group_id", Account_ID='the_account_id')
            obj.refresh(consistent_read=True)

        # wrong group_id
        input = {"IntegrationKey" :"the_api_key",
                         "Group": {'ID':'AA::BB',
                           'Domain':'clientdomain.com',
                           'MaxDownloadSpeedBits':0,
                           'MaxUploadSpeedBits':0,
                           'Name':'Group2',
                           'RemoteServerUrl':'Remote.com',
                           'SharedSecret':'abc123',
                           'TimeZone':"Asia/Qyzylorda"}
                 }

        with self.assertRaisesRegex(EradValidationException, 'Invalid ID.'):
            self.myIntegrationI.group_save(input)

        with self.assertRaisesRegex(pynamodb_excp.DoesNotExist, 'This item does not exist in the table.'):
            obj = Erad_Group2(Account_ID="the_account_id", ID = "the_group_id")
            obj.refresh(consistent_read=True)


        with self.assertRaisesRegex(pynamodb_excp.DoesNotExist, 'This item does not exist in the table.'):
            obj = Erad_Authenticator2(ID = "the_group_id", Account_ID='the_account_id')
            obj.refresh(consistent_read=True)

        apikey.delete()


    def test_group_save__optional_fields(self):
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["222"], Account_ID="elevenos", LastUsed=util.utcnow()) )

        input = {"IntegrationKey" :"i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF",
                 "Group": {'ID':'test_group_save__optional_fields',
                           'Name':'Group2',
                           'RemoteServerUrl':'Remote.com',
                           'SharedSecret':'abc123'}}

        expect = {"Group": {'Account_ID':'elevenos',
                    'ID':'test_group_save__optional_fields',
                    'Domain': None,
                    'MaxDownloadSpeedBits':0,
                    'MaxUploadSpeedBits':0,
                    'Name':'Group2',
                    'RemoteServerUrl':'Remote.com',
                    'SharedSecret':'abc123',
                    'TimeZone': None,
                    'OverrideVlan': False,
                    'OverrideConnectionSpeed': False,
                    'ClientInfoArn': None,
                    }}

        result = self.myIntegrationI.group_save(input)
        self.assertEqual(result, expect)

        # Is it actually in our DB?
        obj = Erad_Group2(Account_ID="elevenos", ID = "test_group_save__optional_fields")
        obj.refresh(consistent_read=True)
        self.assertIsNotNone(obj)


    def test_group_save__exceptions(self):
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["222"], Account_ID="elevenos", LastUsed=util.utcnow()) )
        input = {"IntegrationKey" :"i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF",
                 "Group": {'ID':'2',
                           'MaxDownloadSpeedBits':0,
                           'MaxUploadSpeedBits':0,
                           'Name':'Group2',
                           'RemoteServerUrl':'Remote.com',
                           'SharedSecret':'abc123',
                           'TimeZone':"Asia/Qyzylorda"}
                 }

        # Invalid param
        input["Group"]["RemoteServerUrl"] = 1
        self.assertRaisesRegex(EradWrongTypeException, "Parameter .* is incorrect type \(.*\).",
                                self.myIntegrationI.group_save, input)

        # Missing param
        del(input["Group"]["Name"])
        self.assertRaisesRegex(EradRequiredFieldException, "Required parameter .* is missing.",
                        self.myIntegrationI.group_save, input)


    def test_group_save__do_not_add_authenticator(self):
        Erad_Session.save( Erad_Session(ID=str("unit_test_session"), Group_ID_List=["222"], Account_ID="elevenos", LastUsed=util.utcnow()) )
        expect = {"Group": {'Account_ID': 'elevenos',
                    'ID':'88:88:88:88:88:88',
                    'Domain': None,
                    'MaxDownloadSpeedBits':0,
                    'MaxUploadSpeedBits':0,
                    'Name':'Group12',
                    'RemoteServerUrl':'Remote.com',
                    'SharedSecret':'abc123',
                    'TimeZone':'Asia/Qyzylorda',
                    'OverrideVlan': False,
                    'OverrideConnectionSpeed': False,
                    'ClientInfoArn': None,
                    }}

        # Valid group and AddAuthenticator = False
        input = self.set_input_key_group("88:88:88:88:88:88", "Group12", "Remote.com", "abc123","Asia/Qyzylorda","False")
        result = self.myIntegrationI.group_save(input)
        self.assertEqual(result, expect)

        # Is the Authenticator in our DB?
        input = self.get_input_key_auth("88:88:88:88:88:88")
        self.assertRaisesRegex(DoesNotExist, "This item does not exist in the table.",
                                self.myAdminI.authenticator_load,input)


    def test_group_delete(self):

        Erad_Authenticator2.save(
            Erad_Authenticator2(
                ID="00:11:22:33:44:55",
                Group_ID="99",
                Account_ID='elevenos'
            )
        )

        Erad_Supplicant.save(
            Erad_Supplicant(
                Group_ID="99",
                Username="user_1",
                Password="passw0rd",
                UseRemote=False,
                UseWildcard=False,
                Vlan=888
            )
        )

        Erad_Group2.save(
            Erad_Group2(
                Account_ID="elevenos",
                ID="99",
                Name="ninetynine",
                RemoteServerUrl="Remote.fake.com",
                SharedSecret="321cba",
                TimeZone="Asia/Qyzylorda",
                MaxDownloadSpeedBits=0,
                MaxUploadSpeedBits=0
            )
        )

        # Valid delete
        input = self.get_input_key_group("99")
        self.myAdminI.group_delete(input)

        # Is it actually deleted from DB?
        group2 = Erad_Group2(Account_ID="elevenos", ID = "99")
        self.assertRaisesRegex(DoesNotExist, "This item does not exist in the table.", group2.refresh)

        auth = Erad_Authenticator2(ID = "00:11:22:33:44:55", Group_ID = "99", Account_ID='elevenos')
        self.assertRaisesRegex(DoesNotExist, "This item does not exist in the table.", auth.refresh)

        supp = Erad_Supplicant(Group_ID = "99", Username = "user_1" )
        self.assertRaisesRegex(DoesNotExist, "This item does not exist in the table.", supp.refresh)

    def test_group_overwrite_attrs(self):
        """Checking, that optional attributes overwrite only if post data contains new values.
        """
        erad_session = Erad_Session(
            ID="overwrite_unit_test_session",
            Group_ID_List=["group_with_overwrite"],
            Account_ID="elevenos",
            LastUsed=util.utcnow()
        )
        erad_session.save()

        group = Erad_Group2()
        group.MaxDownloadSpeedBits = 1
        group.MaxUploadSpeedBits = 2
        group.OverrideVlan = False
        group.OverrideConnectionSpeed = False

        group.Account_ID="elevenos"
        group.ID="group_with_overwrite"
        group.Name="ninetynine"
        group.RemoteServerUrl="Remote.fake.com"
        group.SharedSecret="321cba"
        group.TimeZone="Asia/Qyzylorda"

        group.save()

        input_data = {
            "IntegrationKey" :  self.std_integration_key,
            "Group": {'ID':  "group_with_overwrite",
                      'Name': "new-name",
                      'RemoteServerUrl': "Remote.fake.com",
                      'SharedSecret': "321cba",
                      'TimeZone': "Asia/Qyzylorda",
                      'AddAuthenticator': "False"}
            }


        self.myIntegrationI.group_save(input_data)

        group.refresh()

        self.assertEqual(group.ID, "group_with_overwrite")
        self.assertEqual(group.Name, "new-name")
        self.assertEqual(group.MaxDownloadSpeedBits, 1)
        self.assertEqual(group.MaxUploadSpeedBits, 2)
        self.assertEqual(group.OverrideVlan, False)
        self.assertEqual(group.OverrideConnectionSpeed, False)


        input_data = {
            "IntegrationKey" :  self.std_integration_key,
            "Group": {
                'ID':  "group_with_overwrite",
                'Name': "new-name",
                'RemoteServerUrl': "Remote.fake.com",
                'SharedSecret': "321cba",
                'TimeZone': "Asia/Qyzylorda",
                'AddAuthenticator': "False",
                'OverrideVlan': "True",
                'OverrideConnectionSpeed': "True",
                }
            }


        self.myIntegrationI.group_save(input_data)

        group.refresh()

        self.assertEqual(group.ID, "group_with_overwrite")
        self.assertEqual(group.Name, "new-name")
        self.assertEqual(group.MaxDownloadSpeedBits, 1)
        self.assertEqual(group.MaxUploadSpeedBits, 2)
        self.assertEqual(group.OverrideVlan, True)
        self.assertEqual(group.OverrideConnectionSpeed, True)


        input_data = {
            "IntegrationKey" :  self.std_integration_key,
            "Group": {
                'ID':  "group_with_overwrite",
                'Name': "new-name",
                'RemoteServerUrl': "Remote.fake.com",
                'SharedSecret': "321cba",
                'TimeZone': "Asia/Qyzylorda",
                'AddAuthenticator': "False",
                'OverrideVlan': "True",
                'OverrideConnectionSpeed': "True",
                'MaxDownloadSpeedBits': 5,
                'MaxUploadSpeedBits': 6,
                }
            }


        self.myIntegrationI.group_save(input_data)


        group.refresh()

        self.assertEqual(group.ID, "group_with_overwrite")
        self.assertEqual(group.Name, "new-name")
        self.assertEqual(group.MaxDownloadSpeedBits, 5)
        self.assertEqual(group.MaxUploadSpeedBits, 6)
        self.assertEqual(group.OverrideVlan, True)
        self.assertEqual(group.OverrideConnectionSpeed, True)

        group.delete()
        erad_session.delete()


if __name__ == '__main__':
    unittest.main()
