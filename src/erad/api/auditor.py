#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad import util
from erad.exceptions import *
from .erad_model import *
import time, random

class Auditor:

    offset_size = int(10000)

    # milliseconds_since_epoch should be a long.
    # value should be a float greater than or equal to 0.0 and less than 1.0.
    def instance_pack( self, milliseconds_since_epoch, value ):
        # milliseconds becomes the most significant digits
        # value is mapped to the range 0 to (offset_size-1)
        # these values can then be added together without loss of information
        return milliseconds_since_epoch * self.offset_size + int(value * self.offset_size)

    # instance must be a number previously generated with instance_pack
    def instance_unpack( self, instance ):
        milliseconds_since_epoch = int(instance) / self.offset_size
        value = float( (int(instance) % self.offset_size) ) / float(self.offset_size)
        return milliseconds_since_epoch, value

    def make_unique_instance( self ):
        return self.instance_pack( int(time.time() * 1000), random.random() )

    def record( self, message, group_id, identifer, account_id = "elevenos" ):
        try:
            audit_model = Erad_Audit()
            audit_model.Group_ID = group_id
            audit_model.Instance = self.make_unique_instance()
            audit_model.Identifier = identifer
            audit_model.Message = message
            audit_model.Date = util.utcnow()
            audit_model.AccountGroup_ID = account_id + "::" + group_id
            audit_model.save()
        except:
            # auditing is not required at the moment.
            raise











