#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from os import system
import re
from erad.api import app
from .interface_util import *
import json
import ast
import logging
import time
import boto3

from ..util import make_unique_instance

from ..sns_upload import send_log_sns

from ..client_info import get_client_info

IS_ALIVE_TEST_PORT = "1814"
IS_ALIVE_TEST_SECRET = "V72QSZ2CE3"

# 120 because timeout between reloading ports confgiruation on radius servers 120 seconds in cron
ENDPOINT_QEURY_CACHE_TIMEOUT = 120

logger = logging.getLogger(__name__)




class EradInterfaceSystem(EradInterfaceUtil):

    group_possible_none_fields = {
        'OverrideVlan',
        'OverrideConnectionSpeed',
    }
    # internal method
    def authorize_system_key( self, fields ):
        key_input = self.expect_parameter( fields, 'SystemKey' )
        found = False
        for key in util.erad_cfg().system_key:
            if key_input == key:
                found = True
        if not found:
            raise EradAccessException( "Access Denied." )

    @staticmethod
    @app.cache.memoize(timeout=ENDPOINT_QEURY_CACHE_TIMEOUT)
    def find_endpoint(ip, port):

        endpoint = Erad_Endpoint().get(
            ip,
            port
        )

        return endpoint

    def _get_endpoint_group_id(self, fields ):
        # Default values
        IPADDR = "52.11.20.214"
        PORT = "1812"
        GROUP_ID = "elevenos::Global"
        DEFAULT_ENDPOINT = {
            "IpAddress": IPADDR,
            "Port": PORT
        }

        endpoint = fields.get('Endpoint', DEFAULT_ENDPOINT)
        ip_address = endpoint.get('IpAddress') or IPADDR
        port = endpoint.get('Port') or PORT
        group_model = None

        if port == "1814":
            return group_model, "eradtests::erad-tests"

        try:
            endpoint = EradInterfaceSystem.find_endpoint(ip_address, port)
            group_id = endpoint.Group_ID
        except DoesNotExist:
            group_id = GROUP_ID

        account_id, tmp_group_id = group_id.split('::')
        group_model = self._group_exists(account_id, tmp_group_id)

        if 'Group' in fields and self._not_global(fields['Group']):
            tmp_group_id = fields['Group'].get('ID')
            if self._group_exists(account_id, tmp_group_id):
                group_id = account_id + "::" + tmp_group_id

        return group_model, group_id

    def convert_wildcard_to_regex( self, wildcard ):
        sentinel = r'54774D5EE10744DEB581E847FF916461'
        wildcard_symbols = r'(?<!\\)[\^\*]'  # to replace `^` and `*` only if these symbols are not preceded by `\` symbol `examp\^le`
        regex_wildcard = r'(.+)'

        # Start with HOST/^.^/marrcorp.marriott.com
        # or with HOST/*.*/marrcorp.marriott.com
        # Then find and temporary replace all wildcard symbols with sentinel value.
        # Sentinel value is used to leave symbols in regexp groups unescaped.

        tmp_str = re.sub(wildcard_symbols, sentinel, wildcard)
        tmp_str = re.escape(tmp_str)
        return tmp_str.replace(sentinel, regex_wildcard)

    def supplicant_wildcard_match( self, model, Username ):
        if not model.UseWildcard:
            return model.Username == Username
        regex_pattern = self.convert_wildcard_to_regex( model.Username )
        # we ignore case and accept Unicode
        if re.match( regex_pattern, Username, re.I | re.U ):
            return True
        return False

    def get_account_certs_dict(self):
        # format is "Account_ID: Certificate Name"
        certs_dict = {
            'jsteele': 'hilton',
            'hilton': 'hilton',
            'rlhc': 'rlhc',
            'test_account_with_cert': 'RRR',
            'calix-staging': 'stagingcalix',
            'calix': 'prodcalix',
            'spectrum-staging': 'spectrumstaging',
            'spectrum': 'spectrumprod',
        }

        return certs_dict

    def get_radius_shared_secrets(self, table, cluster_id):
        """ FreeRADIUS loadbalancer need shared secrets.
        This method take cluster_id, find load balancer that serve cluster_id. Then construct list of all clusters which
        sharing same load balancer and return shared secrets for all ports served by founded clusters/load balancer

        :param table: Class, referencing pynamodb table model, in general it should be `Erad_Endpoint`
        :param cluster_id: str
        :return: dict
        """
        no_of_matches = table.cluster_id_index.count(cluster_id)
        if no_of_matches <= 0:
            return {'SharedSecrets': {}}

        load_balancer_ip_address = ""
        for endpoint in table.cluster_id_index.query(cluster_id, limit=1):
            load_balancer_ip_address = endpoint.IpAddress

        output_data = {}

        for endpoint in table.query(load_balancer_ip_address):
            port = endpoint.Port
            shared_secret = endpoint.Secret

            if port in output_data:
                logger.warning('Port duplicates %s', port)

            output_data[port] = shared_secret

        if len(output_data) > 0:
            output_data[IS_ALIVE_TEST_PORT] = IS_ALIVE_TEST_SECRET

        return {'SharedSecrets': output_data}


    # returns the list of VirtualServers
    def server_config_load(self, fields):
        # authorize the caller
        self.authorize_system_key(fields)

        certs_dict = self.get_account_certs_dict()

        vs_list = {}
        cert_list = {}
        server_ip = ""
        cluster_id = fields.get('ClusterId')
        serverId = fields.get('ServerId')
        serverType = fields.get('ServerType')
        if serverType:
            table = Erad_Endpoint
            if serverType == "radius":
                for item in table.cluster_id_index.query(cluster_id):
                    account_id = item.Group_ID.split('::')[0]
                    vs_list[item.Port] = item.Secret
                    cert_list[item.Port] = certs_dict.get(account_id, None)
                    server_ip = item.IpAddress
            elif serverType == "radius_shared_secrets":
                return self.get_radius_shared_secrets(table, cluster_id)
            elif serverType == "load_balancer":
                load_balancer_list = []

                # check for matching cluster first
                no_of_matches = table.cluster_id_index.count(cluster_id)
                cluster_exists = no_of_matches > 0
                if cluster_exists:
                    # get ip address from endpoint
                    ip_address = ""
                    for endpoint in table.cluster_id_index.query(cluster_id, limit=1):
                        ip_address = endpoint.IpAddress

                    for endpoint in table.query(ip_address):
                        port = int(endpoint.Port)
                        endpoint_cluster = endpoint.ClusterId
                        try:
                            matching_cluster = Erad_Cluster.get(endpoint_cluster)
                        except:
                            load_balancer_list.append((port, {}))
                            continue
                        instances_unicode = matching_cluster.Instances.replace("\'", "\"")
                        # json loads complains about single quotes
                        instances_dict = ast.literal_eval(matching_cluster.Instances)
                        load_balancer_list.append((port, instances_dict))
                # add default test port if the payload has values
                if len(load_balancer_list) > 0 :
                    # default test port
                    test_port = 1814
                    try:
                        matching_cluster = Erad_Cluster.get(cluster_id)
                        instances_unicode = matching_cluster.Instances.replace("\'", "\"")
                        # json loads complains about single quotes
                        instances_dict = ast.literal_eval(matching_cluster.Instances)
                        load_balancer_list.append((test_port, instances_dict))
                    except:
                        load_balancer_list.append((test_port, {}))
                # return for load balancer
                # using only port x[0] to do sorting (1814, {'auth': {}, 'acct': {'i1': '1.1.1.2'}})
                return {'VirtualServers': sorted(load_balancer_list, key=lambda x: x[0])}


        # add default test port if the payload has values
        if len(vs_list) > 0 :
            vs_list[IS_ALIVE_TEST_PORT] = IS_ALIVE_TEST_SECRET
            cert_list[IS_ALIVE_TEST_PORT] = None

        # sanity check
        assert(len(vs_list) == len(cert_list))

        # return for radius
        return {'ServerIP': server_ip, 'VirtualServers': vs_list, 'Certs':  cert_list}

    def _find_by_authenticator(self, fields, account_id, endpoint_group_id):
        """ some of code expect unicode values in fields
        or function will trigger on EradWrongTypeException and return,
        that the search value does not exist in the database
        """
        # find the authenticator
        auth_model = Erad_Authenticator2()
        group_model = Erad_Group2()
        try:
            auth_fields = self.expect_dict(fields, auth_model.Meta.name)
            self.set_unicode_value_nonempty(auth_model, auth_fields, "ID")
            auth_model.ID = util.sanitize_mac(auth_model.ID)
            auth_model.Account_ID = account_id
            auth_model.RadiusAttribute = "called-station-id"
            auth_model.refresh(consistent_read=True)
        except (EradValidationException, EradRequiredFieldException, EradWrongTypeException, DoesNotExist):
            if (group_model.Meta.name in fields):
                try:
                    group_fields = self.expect_dict(fields, group_model.Meta.name)
                    self.set_unicode_value_nonempty(group_model, group_fields, "ID")
                    auth_model.ID = group_model.ID
                    auth_model.Account_ID = account_id
                    auth_model.RadiusAttribute = "nas-identifier"
                    auth_model.refresh(consistent_read=True)

                    group_model = Erad_Group2.get(auth_model.Account_ID, auth_model.Group_ID)
                    group_model.refresh(consistent_read=True)

                except (EradValidationException,EradRequiredFieldException,EradWrongTypeException,DoesNotExist):
                    raise EradConfigurationException("Neither Authenticator nor Group exist.")
            else:
                raise EradConfigurationException("Authenticator '{0}' does not exist".format(auth_model.ID))

        # this else only runs if the except doesnt run
        else:
            # find the group
            group_model = Erad_Group2(auth_model.Account_ID, auth_model.Group_ID)
            try:
                group_model.refresh(consistent_read=True)
            except DoesNotExist:
                raise EradConfigurationException("Group '{0}' does not exist".format(auth_model.Group_ID))

            # check if Group_ID from the endpoint equals auth_model.Group_ID or Global
            if (endpoint_group_id != auth_model.Group_ID and endpoint_group_id != "Global"):
                raise EradConfigurationException("Group '{0}' does not exist".format(auth_model.Group_ID))
        return group_model

    def _find_supplicant(self, fields, account_id, group_id):
        # try to find the supplicant by exact Username match
        sup_model = Erad_Supplicant()
        if isinstance(group_id, Erad_Group2):
            group_id = group_id.ID
        sup_model.Group_ID = account_id + "::" + group_id
        sup_fields = self.expect_dict(fields, sup_model.Meta.name)
        self.set_unicode_value(sup_model, sup_fields, "Username")

        # we need to check both variants of the username with and without realm
        original_username = sup_model.Username

        # stripping the realm if provided in the username to check first without realm
        sup_model.Username = sup_model.Username.split('@')[0]

        org_username = sup_model.Username
        # if the username is mac, sanitize
        try:
            tmp_mac_username = util.wild_sanitize_mac(sup_model.Username)
        except EradValidationException:
            tmp_mac_username = False
        if tmp_mac_username != False:
            sup_model.Username = tmp_mac_username
        else:
            sup_model.Username = sup_model.Username.lower()

        # try to find the Global supplicant by Account_ID
        # breakpoint()
        found_in_global_group = False

        for username_variant in [sup_model.Username, original_username]:
            try:
                global_sup = Erad_Supplicant()
                global_sup.Group_ID = account_id + '::Global'
                global_sup.Username = username_variant
                global_sup.refresh(consistent_read=True)
                found_in_global_group = True
                break
            except DoesNotExist:
                pass

        if found_in_global_group is False:
            # didn't find the global supplicant so continue with the standard procedure
            try:
                # sup model here store username without realm, checking this variant first
                sup_model.refresh(consistent_read=True)
                # restore original username from request that can have realm
                self.set_unicode_value(sup_model, sup_fields, "Username")
            except DoesNotExist:
                # We didn't find an exact match without realm, but variant with realm will be chekced later
                # What if there is a wildcard match?
                match = None

                # replace with original
                self.set_unicode_value(sup_model, sup_fields, "Username")
                # Check if provided by supplicant username looks like mac address
                if util.is_mac_address(sup_model.Username) is True:
                    sanitized_mac = util.wild_sanitize_mac(sup_model.Username)
                    possible_wildcards = util.add_wild_card(sanitized_mac)
                    for possible_mac in possible_wildcards:
                        try:
                            tmp_model = Erad_Supplicant.get(account_id+"::"+group_id, possible_mac)
                            match = tmp_model
                        except DoesNotExist:
                            pass
                # REVIEWTAG: Can we optimize this by having Dynamo filter out UseWildcard = False?
                if match is None:
                    # first query all users that belong to the endpoint group
                    # username with realm was restored few lines above, and now stored in sup_model.Username.
                    # If supplicant is not marked as UseWildcard method self.supplicant_wildcard_match will look for 
                    # exact math and eventually if user exists code will find it.

                    # If supplicant record marked as wildcard, method self.supplicant_wildcard_match will try to use
                    # regexp to find a match.
                    potential_matches = sup_model.query(account_id+"::"+group_id)
                    for model in potential_matches:
                        if self.supplicant_wildcard_match(model, sup_model.Username):
                            match = model
                            break
                if match is None:
                    raise DoesNotExist("Supplicant '{0}' does not exist for group '{1}'".format(sup_model.Username, group_id))
                else:
                    sup_model = match

            self._check_supplicant_expiration(sup_model)

            # add the supplicant to the output
            # check if MAC auth
            if tmp_mac_username != False:
                sup_model.Password = util.sanitize_mac(org_username)
            return sup_model
        # run only of there was no exception
        else:
            self._check_supplicant_expiration(global_sup)

            # add the global supplicant to the output
            # check if MAC auth
            if tmp_mac_username != False:
                global_sup.Password = util.sanitize_mac(org_username)
            return global_sup

    def _check_supplicant_expiration(self, supp_model):
        if (supp_model.ExpirationDate is not None) and \
            supp_model.ExpirationDate.replace(tzinfo=pytz.utc) < util.utcnow().replace(tzinfo=pytz.utc):
                raise EradExpiredException("Supplicant is expired.")

    def _not_global(self, group):
        group_id = group.get('ID', None)
        return group_id and group_id != 'Global'

    def _is_global_port_or_group(self, account_id, group_id):
        """
        Return True if Global group_id or if the endpoint where the group_id equal to Account_ID::Group_ID
        has the same Port as Account_ID::Global.
        """
        if group_id == 'Global':
            return True
        endpoint_group_id = account_id + "::" + group_id
        try:
            non_global = next(Erad_Endpoint().group_id_index.query(endpoint_group_id))
            global_endpoint = next(Erad_Endpoint().group_id_index.query(account_id + "::Global"))
            if global_endpoint.Port == non_global.Port:
                return True
            else:
                return False
        except StopIteration:
            return True

    def _group_exists(self, account_id, group_id):
        try:
            return Erad_Group2().get( account_id, group_id)
        except Exception as e:
            logger.exception(e)
            return None

    def get_endpoint_object(self, group_model):
        name = group_model.Name if group_model is not None else None
        group_id = group_model.ID if group_model is not None else None
        return {
            "EndpointGroupName": name,
            "EndpointGroupID": group_id
        }

    # returns a tuple of Group, Supplicant
    def supplicant_find(self, fields):
        # authorize the caller
        self.authorize_system_key(fields)
        endpoint_group, endpoint_group_id = self._get_endpoint_group_id(fields)

        realm = self.expect_optional_parameter_not_none(fields, "Realm", "")
        # pulling the Account_ID
        account_id = endpoint_group_id.split('::')[0]
        group_id = endpoint_group_id.split('::')[1]
        if not account_id:
            logger.warning("Empty Account_ID exists while finding Supplicant.")
            raise DoesNotExist("Supplicant does not exist because group {0} is not allocated".format(endpoint_group_id))

        use_authenticator = self._is_global_port_or_group(account_id, group_id)

        if use_authenticator:
            group_model = self._find_by_authenticator(fields, account_id, group_id)
            output = self.standard_output_object_none2false(group_model, self.group_possible_none_fields)
        else:
            group_model = Erad_Group2().get(account_id, group_id)
            output = self.standard_output_object_none2false(group_model, self.group_possible_none_fields)

        sup_model = self._find_supplicant(fields, account_id, group_model)
        output.update(self.standard_output_object(sup_model))


        client_info = get_client_info(group_id, account_id, sup_model.Parent_ID, endpoint_group, realm)
        if client_info:
            output['Supplicant'].update(client_info)

        # Send group id and name
        endpoint_object = self.get_endpoint_object(endpoint_group)
        output.update(endpoint_object)
        return output

    def supplicant_find_certificate(self, fields):
        self.authorize_system_key(fields)

        # if freeradius was able to extract REALM it will be passed here
        realm = self.expect_optional_parameter_not_none(fields, "Realm", "")

        cert_model = Erad_SupplicantCertificate()
        certificate_fields = self.expect_dict(fields, cert_model.Meta.name)

        # if cert CN value has REALM ID value also will be with REALM but it is only used to find cert in DB
        self.set_unicode_value(cert_model, certificate_fields, "ID")

        # Find supplicant certificate by realm-attached id, else fall back to id without realm.
        cert_id_without_realm = cert_model.ID.split("@")[0]

        found = False
        for cID in [cert_model.ID, cert_id_without_realm]:
            try:
                cert_model2 = Erad_SupplicantCertificate.get(cID)
                cert_model2.refresh(consistent_read=True)
                found = True
                break
            except Exception:
                # if default id (possible with realm) is not found, try without realm
                pass

        if not found:
            raise DoesNotExist("Item does not exist")

        endpoint_group, endpoint_group_id = self._get_endpoint_group_id(fields)
        account_id = endpoint_group_id.split('::')[0]
        group_id = endpoint_group_id.split('::')[1]

        if account_id != cert_model2.Group_ID.split("::")[0]:
            raise EradConfigurationException("Account ID didn't match.")

        cert_group_id = cert_model2.Group_ID.split("::")[1]
        if cert_group_id != "Global" and cert_group_id != group_id:
            raise EradConfigurationException("Group ID didn't match.")

        use_authenticator = self._is_global_port_or_group(account_id, group_id)
        if use_authenticator:
            group_model = self._find_by_authenticator(fields, account_id, group_id)
            output = self.standard_output_object_none2false(group_model, self.group_possible_none_fields)
        else:
            group_model = Erad_Group2().get(account_id, group_id)
            output = self.standard_output_object_none2false(group_model, self.group_possible_none_fields)

        sup_model = Erad_Supplicant.get(cert_model2.Group_ID, cert_model2.Username)

        if sup_model:
            self._check_supplicant_expiration(sup_model)
            saved_account_id = sup_model.Group_ID.split("::")[0]
            saved_group_id = sup_model.Group_ID.split("::")[1]
            checksum = self.password_based_checksum(
                saved_account_id+saved_group_id+sup_model.Username+sup_model.Password,
                saved_account_id)
            if checksum != cert_model2.Checksum:
                raise EradValidationException("Certificate has been revoked.")
        output.update(self.standard_output_object(sup_model))

        client_info = get_client_info(group_id, account_id, sup_model.Parent_ID, endpoint_group, realm)
        if client_info:
            output['Supplicant'].update(client_info)

        # Get endpoint object
        endpoint_object = self.get_endpoint_object(endpoint_group)
        output.update(endpoint_object)
        return output

    def _generate_message(self, supplicant, msg, log=None):
        """Generate message about first time use of device or profile.
        Device means - on each user device installed profile with unique supplicant username.
        All profiles has the same Parent_ID, usually email address or another external id of the user

        Not always supplicant.Group_ID is the group that supplicant used to authenticate. This is because supplicant
        records can be queried from DB by Parent_ID field that does not have to match.
        If from log record it is known Group_ID use it, if not fallback to info from the supplicant record.
        """
        if log is None:
            log = {}

        data = [{
            'RequestType': msg,
            'EventTimestamp_iso': datetime.utcnow().isoformat(),
            'Account_ID': supplicant.Account_ID,
            'Parent_ID': supplicant.Parent_ID,
            'Group_ID': supplicant.Group_ID,
            'Username': supplicant.Username,
            'device_used': supplicant.device_used,
            'profile_used': supplicant.profile_used,
            'EndpointGroupID': log.get('EndpointGroupID'),
            'EndpointGroupName': log.get('EndpointGroupName'),
            'CustomJsonData': supplicant.CustomJsonData
        }]

        return json.dumps(data)

    def get_authenticator_info(self, account_id, log):
        try:
            authenticator = Erad_Authenticator2()
            authenticator.ID = util.sanitize_mac(log['CalledStationId'])
            authenticator.Account_ID = account_id
            authenticator.RadiusAttribute = "called-station-id"
            # remove BSSID part of the calledstationid
            if authenticator.ID.count(":") == 1 or authenticator.ID.count(":") == 6:
                authenticator.ID, ssidName = authenticator.ID.rsplit(':', 1)

            authenticator.refresh(consistent_read=True)
            authenticator_Group_ID = authenticator.Group_ID
            group = Erad_Group2(account_id, authenticator_Group_ID)
            group.refresh(consistent_read=True)
            authenticator_Group_ID = authenticator.Group_ID
            authenticator_Group_Name = group.Name
        except:
            authenticator_Group_ID = 'None'
            authenticator_Group_Name = 'Site not found'
        try:
            if authenticator_Group_ID == 'None' and log['NASIdentifier'] != '':
                auth = Erad_Authenticator2()
                auth.ID = log['NASIdentifier']
                auth.Account_ID = account_id
                auth.RadiusAttribute = "nas-identifier"
                auth.refresh(consistent_read=True)

                group = Erad_Group2(account_id, auth.Group_ID)
                group.refresh(consistent_read=True)
                authenticator_Group_Name = group.Name
                authenticator_Group_ID = auth.Group_ID
        except:
            authenticator_Group_ID = 'None'
            authenticator_Group_Name = 'Site not found'
        return authenticator_Group_ID, authenticator_Group_Name

    def supplicant_log_save(self, fields):
        """Save auth logs into db"""
        # authorize the caller
        self.authorize_system_key(fields)

        logs = self.expect_parameter(fields, 'Logs')

        for log in logs:
            # pulling the Account_ID
            endpoint_info = {
                "Endpoint": {
                    "IpAddress": log['EndpointIPAddress'],
                    "Port": log['EndpointPort']
                }
            }
            group_model, endpoint_group_id = self._get_endpoint_group_id(endpoint_info)
            account_id, group_id = endpoint_group_id.split("::")
            use_authenticator = self._is_global_port_or_group(account_id, group_id)
            if use_authenticator:
                group_id, group_name = self.get_authenticator_info(account_id, log)
            else:
                group = Erad_Group2().get(account_id, group_id)
                group_name = group.Name

            try:
                system_log = Erad_Radius_Log()
                system_log.Group_ID = group_id
                system_log.Group_Name = group_name
                system_log.AccountGroup_ID = account_id + '::' + group_id
                system_log.Instance = make_unique_instance(log['Timestamp'])
                system_log.Parent_ID = log.get('Parent_ID')
                system_log.PacketType = log['PacketType']
                system_log.Username = log['UserName']
                system_log.CalledStationId = log['CalledStationId']
                system_log.CallingStationId = log['CallingStationId']
                system_log.EventTimestamp = log['EventTimestamp']
                system_log.NASIdentifier = log['NASIdentifier']
                system_log.NASIpAddress = log['NASIPAddress']
                system_log.Access = log['Access']
                system_log.EndpointIPAddress = log['EndpointIPAddress']
                system_log.EndpointPort = log['EndpointPort']
                system_log.EndpointGroupID = log['EndpointGroupID']
                system_log.EndpointGroupName = log['EndpointGroupName']
                system_log.CustomJsonData = log['CustomJsonData']
                # Convert IsProxied Value to Boolean
                isproxy = False
                if log['IsProxied'] == "yes":
                    isproxy = True
                system_log.IsProxied = isproxy
                system_log.save()
            except Exception as err:
                logger.exception(err)
                raise EradConfigurationException('Cannot save the system logs to database')

            self.mark_device_as_used(account_id, group_id, log)

    def mark_device_as_used(self, account_id, group_id, log):
        """Analize log records, and if user device was authenticated for the first time, mark devise as used and send
        SNS notification about first use event """
        logger.debug("%s %s %s", account_id, group_id, log)
        timestamp = datetime.utcnow().timestamp()
        profile_used = log.get('ProfileUsed')
        device_used = log.get('DeviceUsed')
        try:
            parent_id = log.get('Parent_ID')
            if not profile_used and parent_id and account_id:
                supplicants = Erad_Supplicant.Parent_ID_index.query(
                    parent_id,
                    (SupplicantParentIdIndex.Account_ID == account_id)
                )
                last_supplicant = None
                for supplicant in supplicants:
                    last_supplicant = supplicant
                    if not supplicant.profile_used:
                        supplicant.profile_used = timestamp
                        supplicant.save()


                # this block out of loop to generate only one message first_time_use_profile
                if last_supplicant is not None:
                    message = self._generate_message(
                        last_supplicant,
                        msg='first_time_use_profile',
                        log=log
                    )
                    logger.debug("sns msg: %s", message)
                    send_log_sns(message, "Auth-Log-Session")

            if not device_used:

                groups = [group_id, "Global"]

                supplicant = None
                for group in groups:
                    try:
                        logger.debug(f"Searching for supplicant in `{group}` group")
                        supplicant = Erad_Supplicant.get(
                            f'{account_id}::{group}', log["UserName"]
                        )
                        break
                    except DoesNotExist as err:
                        pass  # expected no need to do anything
                    except Exception as err:
                        logger.exception(err)

                if supplicant is None:
                    logger.error("Supplicant %s account id: '%s'"
                                 " not found in groups %s", log["UserName"], account_id, groups)
                    return

                if not supplicant.device_used:
                    supplicant.device_used = timestamp
                    supplicant.save()
                    message = self._generate_message(
                        supplicant,
                        msg='first_time_use_device',
                        log=log
                    )
                    logger.debug("sns msg: %s", message)
                    send_log_sns(message, "Auth-Log-Session")

        except Exception as err:
            logger.exception(err)
            raise EradConfigurationException('Cannot update supplicant info to database')

    # save the accounting logs
    def supplicant_acct_log_save(self, fields):
        # authorize the caller
        self.authorize_system_key(fields)

        log_record = self.expect_parameter(fields, 'Logs')
        parsed = json.loads(log_record)

        # searching for either Called-Station-Id or NAS-Identifier
        calledstationid = parsed.get('Called-Station-Id')
        nasidentifier = parsed.get('NAS-Identifier')
        endpoint_ip = parsed.get('Supplicant-Radius-Ip')
        endpoint_port = parsed.get('Supplicant-Radius-Port')
        group_id = None
        group_name = None

        # searching for the account_id
        endpoint_info = {
            "Endpoint": {
                "IpAddress": endpoint_ip,
                "Port": endpoint_port
            }
        }
        group_model, account_group_id = self._get_endpoint_group_id(endpoint_info)
        account_id, group_id = account_group_id.split("::")

        # Try to get Group_ID from Erad_Authenticator2 table.
        def get_groupid(account_id, auth_id):
            try:
                authenticator = Erad_Authenticator2()
                authenticator.ID = auth_id
                authenticator.Account_ID = account_id
                authenticator.refresh(consistent_read=True)

                group_id = authenticator.Group_ID

            # Return "None" if it could not be found.
            except:
                group_id = 'None'

            return group_id

        use_authenticator = self._is_global_port_or_group(account_id, group_id)
        if use_authenticator:
            # First, try to get Group_ID by CalledStationId.
            group_id = get_groupid(
                account_id,
                util.sanitize_mac(calledstationid)
            )

            # If it could not be found by CalledStationId, try using NASIdentifier.
            if group_id == 'None' and nasidentifier is not None:
                group_id = get_groupid(
                    account_id,
                    nasidentifier
                )

         # get Group Name
        try:
            tmp_group = Erad_Group2()
            tmp_group.ID = group_id
            tmp_group.Account_ID = account_id
            tmp_group.refresh(consistent_read=True)

            group_name = tmp_group.Name
        except:
            group_name = None

        # saving the log

        # First expecting that acct log has timestamp attribute,
        # but if not using current time as a timestamp
        timestamp_from_log = parsed.get('Timestamp')

        if timestamp_from_log is None:
            timestamp_from_log = int(time.time())

        instance_value = make_unique_instance(timestamp_from_log)

        log_model = Erad_AccountingLogs()
        log_model.Account_ID = account_id
        log_model.Group_ID = group_id
        log_model.Group_Name = group_name
        log_model.Log_Content = log_record
        log_model.Instance = instance_value
        log_model.EndpointIPAddress = endpoint_ip
        log_model.EndpointPort = endpoint_port
        # Raw_Log field is kept for historical reasons to keep records sorted
        log_model.Raw_Log = str(instance_value)

        log_model.save()


    # Account save
    def account_save ( self, fields ):
        erad_cfg = util.erad_cfg()

        # authorize the caller
        self.authorize_system_key( fields )

        account_model = Erad_AccountOwner()
        account_fields = self.expect_dict( fields, account_model.Meta.name )


        self.set_unicode_value_nonempty( account_model, account_fields, "Email" )
        self.set_unicode_value( account_model, account_fields, "Password" )
        if(self.is_password_blacklisted(account_model.Password)):
            raise EradBlacklistedPasswordException("That password was found in a public list of previously compromised passwords. Please choose a different password.")
        self.set_unicode_value(account_model, account_fields, "Account_ID")

        salt = util.get_salt()
        account_model.Salt = salt

        account_model.Password = self.pass_hash(account_model.Password,
                                                salt,
                                                erad_cfg.crypto_params.sitewide_salt_secret_key)


        account_model.save()

        # save to Erad_Group2
        Erad_Group2(account_model.Account_ID, ID="Global", Name="Global", TimeZone="America/Los Angeles").save()

    def radius_finish(self, fields):
        # authorize the caller
        self.authorize_system_key(fields)

        cluster_id = self.expect_parameter(fields, 'Cluster_ID')
        if Erad_Cluster.count(cluster_id) == 0:
            raise EradValidationException("Cluster not found.")

        with Erad_Endpoint.batch_write() as batch:
            endpoints = [endpoint for
                         endpoint in Erad_Endpoint().cluster_id_index.query(
                            cluster_id,
                            filter_condition=Erad_Endpoint.Group_ID=="::dirty")
                        ]
            for endpoint in endpoints:
                endpoint.Group_ID = "::free" if int(endpoint.Port) < 40000 else '::premium'
                batch.save(endpoint)

    def radius_save(self, fields):
        # authorize the caller
        self.authorize_system_key(fields)
        servers = self.expect_dict(fields, 'Servers')
        if len(servers) == 0:
            raise EradValidationExceptionRadius("Invalid Servers parameter format. No Servers found.")

        clusters = Erad_Cluster.scan()

        instances_cache = {}

        for cluster in clusters:
            instances = None
            if cluster.ClusterId in servers:
                new_cluster_instances = servers[cluster.ClusterId]
                if self.is_valid_servers_parameter(new_cluster_instances):
                    instances = new_cluster_instances
                else:
                    raise EradValidationExceptionRadius("Invalid Servers parameter format.")
            if instances == None:
                region = next(Erad_Endpoint().cluster_id_index.query(cluster.ClusterId)).Region

                if region not in instances_cache:
                    instances_cache[region] = util.get_ec2_instances(region)

                instances = util.parse_cluster_instances(cluster.ClusterId, instances_cache[region])
            cluster.Instances = str(instances)
            cluster.save()

    def radius_load(self, fields):
        # authorize the caller
        self.authorize_system_key(fields)
        cluster_id = self.expect_parameter(fields, 'Cluster_ID')

        if Erad_Cluster.count(cluster_id) == 0:
            raise EradValidationException("Cluster not found.")

        cluster = Erad_Cluster.get(cluster_id)
        instances_dict = ast.literal_eval(cluster.Instances)
        result = {}
        result['ClusterId'] = cluster.ClusterId
        result['AuthNum'] = cluster.AuthNum
        result['AcctNum'] = cluster.AcctNum
        result['Instances'] = instances_dict
        return {'Cluster': result}

    def is_valid_servers_parameter(self, servers):
        """
        The Servers parameter from radius_save must have both auth and acct keys and both of them must have at least one pair of key, valeu.
        """
        if 'auth' not in servers and 'acct' not in servers:
            return False
        auth_servers = servers.get('auth', {})
        acct_servers = servers.get('acct', {})

        if len(auth_servers) < 1 and len(acct_servers) < 1:
            return False
        return True

    def radius_region(self, fields):
        # authorize the caller
        self.authorize_system_key(fields)
        cluster_id = self.expect_parameter(fields, 'Cluster_ID')

        if Erad_Cluster.count(cluster_id) == 0:
            raise EradValidationException("Cluster not found.")

        region = ""
        for endpoint in Erad_Endpoint().cluster_id_index.query(cluster_id, limit=1):
            region = endpoint.Region

        return {'Region': region}


    def certs_list(self, fields):
        # authorize the caller
        self.authorize_system_key(fields)

        certs_dict = self.get_account_certs_dict()

        # commented out because default eap module created manually
        # add default value to create default `eap` module
        # certs_dict['default'] = 'default'

        return {'Certs': certs_dict}
