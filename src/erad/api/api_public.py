#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad.api import *
from .interface_public import EradInterfacePublic

#---------- Administration Services ----------

# http://localhost:5000/erad/public/status/check
@app.route('/erad/public/status/check', methods=['OPTIONS','GET'])
@crossdomain()
def public_status_check():
    return ok_response({ 'status_check': EradInterfacePublic().status_check() })

# http://localhost:5000/erad/public/account/login
@app.route('/erad/public/account/login', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def public_account_login():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfacePublic().account_login( request.json ) )


# http://localhost:5000/erad/public/account/save
@app.route('/erad/public/account/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def public_account_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfacePublic().account_save( request.json, request.environ ) )


#
# HS20 - Profile Generator
#

# http://localhost:9090/erad/public/profile/create
@app.route('/erad/public/profile/download', methods=['HEAD', 'GET', 'OPTIONS'])
@crossdomain()
def public_profile_download():
    if request.method != 'GET':
        return ok_response()
    # args is used here instead of json because this is a GET request
    response = make_response(EradInterfacePublic().profile_download(request.args), 200)

    filename = "cred.conf"
    content_type = "application/octet-stream;charset=UTF-8"

    if "vendor" in request.args:
        if request.args['vendor'] == 'android':
            response.headers["Content-Transfer-Encoding"] = "base64"
            response.headers["Content-type"] = "application/x-wifi-config"
            return response

        if request.args['vendor'] == 'apple':
            response.headers["Content-Disposition"] = "attachment; filename=passpoint.mobileconfig"
            response.headers["Content-type"] = "application/octet-stream;charset=UTF-8"
            return response

    # the response should be downloaded
    response.headers["Content-Disposition"] = "attachment; filename=" + filename
    response.headers["Content-type"] = content_type
    return response

# http://localhost:9090/erad/public/apple/profile/create
# Deprecated, Samsung now uses the standard Android profiles
# @app.route('/erad/public/samsung/profile/create', methods=['HEAD', 'POST', 'OPTIONS'])
# @crossdomain()
# def public_samsung_profile_create():
#     if request.method != 'POST':
#         return ok_response()
#     return ok_response( EradInterfacePublic().samsung_profile_create( request.json ) )

# http://localhost:9090/erad/public/marshmallow/profile/create
@app.route('/erad/public/android/profile/create', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def public_android_profile_create():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfacePublic().android_profile_create( request.json ) )


@app.route('/erad/public/android/profile/create/tls', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def public_android_profile_create_tls():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfacePublic().android_profile_create_tls( request.json ) )

# http://localhost:9090/erad/public/apple/profile/create
@app.route('/erad/public/apple/profile/create', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def public_apple_profile_create():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfacePublic().apple_profile_create( request.json ) )

@app.route('/erad/public/apple/profile/create/tls', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def public_apple_profile_create_tls():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfacePublic().apple_profile_create_tls( request.json ) )


# http://localhost:9090/erad/public/windows/profile/create
@app.route('/erad/public/windows/profile/create', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def public_windows_profile_create():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfacePublic().windows_profile_create( request.json ) )


@app.route('/erad/public/check_password', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def public_check_password():
    return ok_response(EradInterfacePublic().check_password(request.json))

