#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from datetime import timedelta
from functools import update_wrapper
from logging.config import dictConfig
from os import environ

import redis
from celery import Celery
from flask import Flask, request, Response, make_response, abort, jsonify, current_app
from flask_caching import Cache
from flask_compress import Compress

from . import tasks
from .erad_model import *
from ..exceptions import *

LOG_LEVEL = environ.get('LOG_LEVEL', 'INFO')

dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(name)s.%(module)s:%(lineno)s %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'ERROR',
        'handlers': ['wsgi'],
        'propagate': False
    },
    'loggers': {
        'werkzeug': {
            'level': 'ERROR',
            'handlers': ['wsgi'],
            'propagate': False
        },
        'erad': {
            'level': LOG_LEVEL,
            'handlers': ['wsgi'],
            'propagate': False
        }
    }
})

redis_client = redis.StrictRedis()

# turn off annoying DEBUG logs

# logging.getLogger('botocore').setLevel(logging.CRITICAL)
# logging.getLogger('pynamodb.connection.base').setLevel(logging.CRITICAL)
# logging.getLogger('werkzeug').setLevel(logging.ERROR)

# ---------------------- Initialization -----------------------
# this should go in a config or be part of debug mode.
# it enables stack traces in errors.
show_tracebacks = True if environ.get('ERAD_DEBUG') == 'True' else False

# create the Flask app object, override default configurations
app = Flask(__name__)

app.config.update(
    # pass through unicode when serializing, do not convert it to ASCII
    JSON_AS_ASCII=False,
    # always pass exceptions to our server_error() function (even in Debug mode)
    PROPAGATE_EXCEPTIONS=False
)

app.config['CACHE_TYPE'] = 'redis'
app.config['CACHE_REDIS_URL'] = 'redis://localhost:6379/2'
app.config['CACHE_KEY_PREFIX'] = 'ENDPOINT-'
app.config['CACHE_DEFAULT_TIMEOUT'] = 60 * 60 * 4
app.cache = Cache(app)

# turn on the Compress module which does automatic gzipping for us
Compress(app)

celery = Celery(app.name)
celery.config_from_object('celeryconfig')


# ---------------------- CORS Decorator -----------------------
# see http://en.wikipedia.org/wiki/Cross-origin_resource_sharing
def crossdomain(origin='*', methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, str):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, str):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS'
            h['Access-Control-Max-Age'] = str(max_age)
            h['Access-Control-Allow-Headers'] = 'origin, x-requested-with, content-type'
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)

    return decorator


# ---------------------- Utility Methods -----------------------
def ok_response(data=None):
    ret = {"Error": None}
    if not data is None:
        ret.update(data)
    return make_response(jsonify(ret), 200)


def string_input(name):
    if not request.json:
        abort(400)
    if not name in request.json:
        abort(400)
    return str(request.json[name]);


# ---------------------- Error Handlers -----------------------

# if user does not pass us JSON, they end up here
@app.errorhandler(400)
@crossdomain()
def error_malformed(e):
    return make_response(jsonify({"Error": "Malformed request"}), 400)


# if user accesses nonexistent path, they end up here
@app.errorhandler(404)
@crossdomain()
def error_not_found(Error):
    return make_response(jsonify({"Error": 'Not found'}), 404)


# if user uses GET instead of POST (for example), they end up here
@app.errorhandler(405)
@crossdomain()
def error_method_not_allowed(Error):
    return make_response(jsonify({"Error": 'Method Not Allowed'}), 405)


# all uncaught exceptions end up here.
@app.errorhandler(500)
@crossdomain()
def error_exception(e):
    e = e.original_exception

    # build return object
    ret = {"Error": (str(e) or "Exception")}

    # handle Erad exceptions which may need a different http code
    etype = type(e)
    if issubclass(etype, EradMalformedException):
        httpcode = 400
    elif issubclass(etype, EradRestrictedFieldException):
        httpcode = 400
    elif issubclass(etype, EradAccessException):
        httpcode = 403
    elif issubclass(etype, EradSessionExpiredException):
        ret = e.to_dict()
        httpcode = 403
    elif issubclass(etype, EradConfigurationException):
        httpcode = 200
    elif issubclass(etype, EradValidationException):
        httpcode = 200
    elif issubclass(etype, EradExpiredException):
        httpcode = 200
    elif issubclass(etype, DoesNotExist):
        httpcode = 200
    elif issubclass(etype, EradValidationExceptionRadius):
        httpcode = 400
    elif issubclass(etype, EradGeneralException):
        httpcode = 500
    else:
        httpcode = 500

    if httpcode == 500:
        app.logger.exception(e)
        # only in the case of an unexpected Error might we show a stack trace
        # if show_tracebacks is True:
        #     ret['stack'] = traceback.format_exc()

    # return json Error to client
    return make_response(jsonify(ret), httpcode)


# 200
@app.errorhandler(EradValidationException)
@crossdomain()
def new_exception_handler(e):
    app.logger.debug(e, exc_info=True)
    httpcode = 200
    ret = {"Error": (str(e) or "Exception")}
    return make_response(jsonify(ret), httpcode)


@app.errorhandler(DoesNotExist)
@crossdomain()
def new_exception_handler(e):
    app.logger.debug(e, exc_info=True)
    httpcode = 200
    ret = {"Error": (str(e) or "Exception")}
    return make_response(jsonify(ret), httpcode)


@app.errorhandler(EradConfigurationException)
@crossdomain()
def new_exception_handler(e):
    app.logger.debug(e, exc_info=True)
    httpcode = 200
    ret = {"Error": (str(e) or "Exception")}
    return make_response(jsonify(ret), httpcode)


@app.errorhandler(EradExpiredException)
@crossdomain()
def new_exception_handler(e):
    app.logger.debug(e, exc_info=True)
    httpcode = 200
    ret = {"Error": (str(e) or "Exception")}
    return make_response(jsonify(ret), httpcode)


# 400
@app.errorhandler(EradMalformedException)
@crossdomain()
def new_exception_handler(e):
    app.logger.debug(e, exc_info=True)
    httpcode = 400
    ret = {"Error": (str(e) or "Exception")}
    return make_response(jsonify(ret), httpcode)


@app.errorhandler(EradValidationExceptionRadius)
@crossdomain()
def new_exception_handler(e):
    app.logger.debug(e, exc_info=True)
    httpcode = 400
    ret = {"Error": (str(e) or "Exception")}
    return make_response(jsonify(ret), httpcode)


# 403
@app.errorhandler(EradSessionExpiredException)
@crossdomain()
def new_exception_handler(e):
    app.logger.debug(e, exc_info=True)
    httpcode = 403
    ret = e.to_dict()
    return make_response(jsonify(ret), httpcode)


@app.errorhandler(EradAccessException)
@crossdomain()
def new_exception_handler(e):
    app.logger.debug(e, exc_info=True)
    httpcode = 403
    ret = {"Error": (str(e) or "Exception")}
    return make_response(jsonify(ret), httpcode)


# 500
@app.errorhandler(EradGeneralException)
@crossdomain()
def new_exception_handler(e):
    app.logger.debug(e, exc_info=True)
    httpcode = 500
    ret = {"Error": (str(e) or "Exception")}
    return make_response(jsonify(ret), httpcode)


# ---------------------- Import API Routes -----------------------
# TO_BE_CHECKED
from . import profile
from . import api_internal
from . import api_admin
from . import api_integration
from . import api_system
from . import api_public

__all__ = ["api_internal", "api_admin", "api_integration", "api_system", "api_public"]
