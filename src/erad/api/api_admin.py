#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import datetime

from requests.api import request # radius log export filename
from erad.api import *
from .interface_admin import EradInterfaceAdmin
from flask import stream_with_context

#---------- Administration Services ----------

# http://localhost:5000/erad/admin/audit/search
@app.route('/erad/admin/audit/search', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_audit_search():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().audit_search( request.json ) )

# http://localhost:5000/erad/admin/session/load
@app.route('/erad/admin/session/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_session_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().session_load( request.json ) )

# http://localhost:5000/erad/admin/group/list
@app.route('/erad/admin/group/list', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_group_list():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().group_list( request.json ) )

# http://localhost:5000/erad/admin/group/load
@app.route('/erad/admin/group/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_group_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().group_load( request.json ) )


# http://localhost:5000/erad/admin/group_unique_devices/load
@app.route('/erad/admin/group_unique_devices/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_group_unique_devices_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response(EradInterfaceAdmin().get_group_unique_devices(request.json))

# http://localhost:5000/erad/admin/group/save
@app.route('/erad/admin/group/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_group_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().group_save( request.json ) )

# http://localhost:5000/erad/admin/group/delete
@app.route('/erad/admin/group/delete', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_group_delete():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().group_delete( request.json ) )


# http://localhost:5000/erad/admin/supplicant/list
@app.route('/erad/admin/supplicant/list', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_supplicant_list():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().supplicant_list( request.json ) )

# http://localhost:5000/erad/admin/supplicant/import/csv
@app.route('/erad/admin/supplicant/import/csv', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_supplicant_import_csv():
    if request.method != 'POST':
        return ok_response()
    # args is used here instead of json because the POST body contains the file
    return ok_response( EradInterfaceAdmin().supplicant_import_csv( request.args, request.files['file'] ) )

# http://localhost:5000/erad/admin/supplicant/export/csv
@app.route('/erad/admin/supplicant/export/csv', methods=['HEAD', 'GET', 'OPTIONS'])
@crossdomain()
def admin_supplicant_export_csv():
    if request.method != 'GET':
        return ok_response()
    # args is used here instead of json because this is a GET request
    response = make_response( EradInterfaceAdmin().supplicant_export_csv( request.args ), 200 )
    # the response should be downloaded
    response.headers["Content-Disposition"] = "attachment; filename=supplicants.csv"
    response.headers["Content-type"] = "text/csv"
    return response

# http://localhost:5000/erad/admin/supplicant/find-by-parent
@app.route('/erad/admin/supplicant/find-by-parent-id', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_supplicant_find_by_parent():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().supplicant_find_by_parent( request.json ) )

# http://localhost:5000/erad/admin/supplicant/load
@app.route('/erad/admin/supplicant/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_supplicant_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().supplicant_load( request.json ) )

# http://localhost:5000/erad/admin/supplicant/save
@app.route('/erad/admin/supplicant/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_supplicant_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().supplicant_save( request.json ) )

# http://localhost:5000/erad/admin/supplicant/certificate
@app.route('/erad/admin/supplicant/certificate', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_supplicant_certificate():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().supplicant_certificate( request.json ) )

# http://localhost:5000/erad/admin/supplicant/mac/save
@app.route('/erad/admin/supplicant/mac/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_supplicant_mac_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().supplicant_mac_save( request.json ) )

# http://localhost:5000/erad/admin/supplicant/delete
@app.route('/erad/admin/supplicant/delete', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_supplicant_delete():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().supplicant_delete( request.json ) )

# http://localhost:5000/erad/admin/supplicant/log/group/list
@app.route('/erad/admin/supplicant/log/group/list', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_supplicant_log_group_list():
    if request.method != 'POST':
        return ok_response()

    initial_query, log_generator = EradInterfaceAdmin().supplicant_log_list_generator(request.json)
    myAdmin = EradInterfaceAdmin()

    result = myAdmin.wrap_log_generator(
        myAdmin.supplicant_log_list(initial_query, log_generator)
    )

    return Response(
        result,
        mimetype='application/octet-stream'
    )


# http://localhost:5000/erad/admin/supplicant/log/list
@app.route('/erad/admin/supplicant/log/list', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_supplicant_log_list():
    if request.method != 'POST':
        return ok_response()

    # If just use generator then any exception that will hapen in the generator will not be processed
    # return first item from generator to trigger the exception, if no exception it will continue
    # streaming response.
    initial_query, log_generator = EradInterfaceAdmin().supplicant_log_list_generator(request.json)

    result = EradInterfaceAdmin().supplicant_log_list(
        initial_query, log_generator)

    return Response(
        result,
        mimetype='application/octet-stream'
    )



# http://localhost:5000/erad/admin/supplicant/log/export/csv
@app.route('/erad/admin/supplicant/log/export/csv', methods=['HEAD', 'GET', 'OPTIONS'])
@crossdomain()
def admin_supplicant_log_export_csv():
    if request.method != 'GET':
        return ok_response()

    headers = {}
    log_date_start = int(EradInterfaceAdmin().expect_parameter( request.args, "Instance_From" ))/10000
    filename = datetime.datetime.fromtimestamp(log_date_start).strftime('%Y%m%d') + '-radius-log.csv'
    headers["Content-Disposition"] = "attachment; filename=" + filename

    fields = EradInterfaceAdmin().get_export_fields(request.args)

    initial_query, log_generator = EradInterfaceAdmin().supplicant_log_list_generator(fields)

    return Response(
        EradInterfaceAdmin().supplicant_log_export_csv( initial_query, log_generator ),
        headers=headers,
        mimetype='text/csv'
    )

# http://localhost:5000/erad/admin/group/authenticator/list
@app.route('/erad/admin/group/authenticator/list', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_group_authenticator_list():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().group_authenticator_list( request.json ) )

# http://localhost:5000/erad/admin/authenticator/load
@app.route('/erad/admin/authenticator/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_authenticator_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().authenticator_load( request.json ) )

# http://localhost:5000/erad/admin/authenticator/save
@app.route('/erad/admin/authenticator/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_authenticator_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().authenticator_save( request.json ) )

# http://localhost:5000/erad/admin/authenticator/delete
@app.route('/erad/admin/authenticator/delete', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_authenticator_delete():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().authenticator_delete( request.json ) )

# http://localhost:5000/erad/admin/endpoint/load
@app.route('/erad/admin/endpoint/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_endpoint_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().endpoint_load( request.json ) )

# http://localhost:5000/erad/admin/endpoint/save
@app.route('/erad/admin/endpoint/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_endpoint_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().endpoint_save(request.json))

# http://localhost:5000/erad/admin/endpoint/delete
@app.route('/erad/admin/endpoint/delete', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_endpoint_delete():
    if request.method != 'POST':
        return ok_response()
    return ok_response(EradInterfaceAdmin().endpoint_delete(request.json))

# http://localhost:5000/erad/admin/account/save
@app.route('/erad/admin/account/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_account_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().account_save( request.json ) )

# http://localhost:5000/erad/admin/account/load
@app.route('/erad/admin/account/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_account_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().account_load( request.json ) )

# http://localhost:5000/erad/admin/account/list
@app.route('/erad/admin/account/list', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_account_list():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().account_list( request.json ) )

# http://localhost:5000/erad/admin/account/delete
@app.route('/erad/admin/account/delete', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_account_delete():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().account_delete( request.json ) )

# http://localhost:5000/erad/admin/apikey/save
@app.route('/erad/admin/apikey/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_apikey_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().apikey_save( request.json ) )

# http://localhost:5000/erad/admin/apikey/load
@app.route('/erad/admin/apikey/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_apikey_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().apikey_load( request.json ) )

# http://localhost:5000/erad/admin/apikey/list
@app.route('/erad/admin/apikey/list', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_apikey_list():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().apikey_list( request.json ) )

# http://localhost:5000/erad/admin/apikey/delete
@app.route('/erad/admin/apikey/delete', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def admin_apikey_delete():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().apikey_delete( request.json ) )

