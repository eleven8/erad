# measuring number of calls and the time each call takes
import time
from erad.api import *
from flask import g


@app.before_request
def before_request():
    g.start_request_time = time.time()


@app.teardown_request
def teardown_request(exception=None):
    # Get duration in milliseconds
    duration = (time.time() - g.start_request_time) * 1000
    redis_client.lpush('api-call-duration', duration)
