#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad.api import *
from erad import util
from .interface_internal import EradInterfaceInternal

#---------- Internal Services ----------
def abort_on_production():
    if not util.erad_cfg().api.allow_internal:
        abort(404)

# this message is used for alerting, changing it will cause a "code red" alert
@app.route('/')
def internal_index():
    return ok_response({ 'status': "Erad Webservices are up and running." })

# http://localhost:5000/erad/schema
@app.route('/erad/schema', methods=['OPTIONS','GET'])
def internal_schema_retrieve():
    abort_on_production()
    return ok_response({ 'schema': EradInterfaceInternal().schema_retrieve() })

# http://localhost:5000/erad/schema/status
@app.route('/erad/schema/status', methods=['OPTIONS','GET'])
def internal_schema_status():
    abort_on_production()
    return ok_response({ 'schema': EradInterfaceInternal().schema_status() })

# http://localhost:5000/erad/schema/describe
@app.route('/erad/schema/describe', methods=['OPTIONS','GET'])
def internal_schema_describe():
    abort_on_production()
    return ok_response({ 'schema': EradInterfaceInternal().schema_describe() })

@app.route('/erad/database', methods=['OPTIONS','GET'])
def internal_dump_database():
    abort_on_production()
    return ok_response({ 'database': EradInterfaceInternal().dump_database() })







