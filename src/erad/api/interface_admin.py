#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
import codecs
from typing import Generator

from mako.template import Template
from erad import util
from erad.exceptions import *
from .erad_model import *
from .interface_util import *
from .auditor import Auditor
import random, string
from ..util import utcnow

from ..sns_upload import send_log_sns

from ..client_info import get_client_info

# for ingegration key authentication
from .interface_integration import EradInterfaceIntegration

# for signature verification
import hmac
import base64
import hashlib
import time

# for csv export
import csv, io, json

# for certificate generation
import os
import shlex
from subprocess import Popen, PIPE, STDOUT, check_output

from boto import s3
from boto import config as boto_config

import logging
import dateutil

logger = logging.getLogger(__name__)

HIGH_PROVISIONED_PORT_GREATER_EQUAL = 40000

class EradInterfaceAdmin(EradInterfaceUtil):

    remote_authentication_options = {
        'ElevenOS' : {
            'title': 'ElevenOS',
            'RemoteServerIp': '52.41.63.155',
            'RemoteServerPort': '1812',
            'RemoteServerUrl': '52.41.63.155:1812',
            'SharedSecret': 'ElevenRules'
        },
        'Marriott' : {
            'title': 'Marriott',
            'RemoteServerIp' : '159.166.57.12',
            'RemoteServerPort': '1812',
            'RemoteServerUrl': '159.166.57.12:1812',
            'SharedSecret': 'ELW4GM2J'
        }
    }
    remote_authentication_options_index = {
        '159.166.57.12:1812' :  'Marriott',
        '52.41.63.155:1812' : 'ElevenOS'
    }

    group_possible_none_fields = {
        'OverrideVlan',
        'OverrideConnectionSpeed',
    }


    # internal method
    # load a session object from input fields containing the ID
    # if the session does not exist, raise Access Denied.
    def load_and_use_session(self, fields):
        cfg = util.erad_cfg()

        for count in range(cfg.api.session.retries):
            try:
                model = Erad_Session()
                session_fields = self.expect_dict(fields, model.Meta.name)
                self.set_unicode_value_nonempty(model, session_fields, "ID")
                # This needs to be consistent because the session was just created.
                model.refresh(consistent_read=True)

                # check for expired
                if util.session_time_is_expired(model.LastUsed):
                    logouturl = "https://admin.enterpriseauth.com/#/login"
                    if model.LogoutUrl is not None:
                        logouturl = model.LogoutUrl
                    raise EradSessionExpiredException("Access Denied.", logouturl)

                model.LastUsed = util.utcnow()
                model.save()

                return model
            except DoesNotExist:
                time.sleep(cfg.api.session.timeout)

        raise EradSessionExpiredException("Access Denied.", "https://admin.enterpriseauth.com/#/login")

    # internal method
    def authorize_signature( self, fields, url ):
        # check if signature verification
        if "Signature" in fields:
            # saving signature and deleting from the payload
            signature = self.expect_unicode_value( fields, 'Signature')
            del fields['Signature']

            # checking the timestamp - has to be +/- 10 min server time
            timestamp = int(self.expect_parameter( fields, 'Timestamp'))
            if not (int(time.time())-600) <= timestamp <= (int(time.time())+600):
                raise EradAccessException( "Request expired. Server time: "+str(int(time.time())) )

            # searching for the ApiKey - using ApiKeyName
            apiKeyName = self.expect_unicode_value( fields, 'ApiKeyName')
            key = None
            for item in Erad_ApiKey().apiKeyName_index.query(apiKeyName):
                key = item.ApiKey_ID

            #adding URL to the payload
            fields['Url'] = url

            # sorting the payload
            tmplist = []
            for item in sorted(list(fields.items()), key=lambda item: item[0]):
                if type(item[1]) is dict:
                    tmplist.append((item[0], sorted(list(item[1].items()), key=lambda innerItem: innerItem[0])))
                else:
                    tmplist.append(item)

            # preparing our signature
            tempString = ""
            for item in tmplist:
                if type(item[1]) is list:
                    tempString = tempString+item[0]
                    for subitem in item[1]:
                        tempString = tempString+subitem[0]+subitem[1]
                else:
                    tempString = tempString+item[0]+item[1]
            hmacstring = hmac.new(str(key).encode(), base64.b64encode(tempString.encode()), digestmod=hashlib.sha256)

            # comparing signatures
            if not hmac.compare_digest(hmacstring.hexdigest(), str(signature)):
                raise EradAccessException( "Access Denied." )
            else:
                # adding IntegrationKey for later authorization - to keep compatibility
                fields['IntegrationKey'] = key


    # internal method
    # check that a session or IntegrationKey has access
    # if it does not, raise Access Denied.
    def authorize_session_or_key(self, fields, group_id=None):
        account_id = ''
        # authorize the caller with integration key
        if "IntegrationKey" in fields:
            EradInterfaceIntegration().authorize_integration_key(fields)
            key_input = self.expect_parameter(fields, 'IntegrationKey')
            account_id = Erad_ApiKey().get(key_input).Account_ID
        # make sure the session has access for admin users
        elif "Session" in fields:
            session_model = self.load_and_use_session(fields)
            if not group_id == None:
                self.authorize_session_for_group(session_model, group_id)
            account_id = session_model.Account_ID
        else:
            raise EradAccessException("Access Denied.")
        return account_id

    def authorize_billing_key(self, fields):
        key_input = self.expect_parameter(fields, 'BillingKey')
        for key in util.erad_cfg().billing_key:
            if hmac.compare_digest(key_input, key):
                return True
        else:
            raise EradAccessException("Access Denied.")

    # internal method
    # check that a session has access to a given Group
    # if it does not, raise Access Denied.
    def authorize_session_for_group( self, session_model, group_id ):
        if not group_id in session_model.Group_ID_List:
            raise EradAccessException( "Access Denied." )

    # internal method
    # scan the Group table for all groups which should have a proxy configuration
    # add them all to the same file.
    def deploy_latest_proxy_conf( self ):
        proxy_servers = self.get_proxy_servers_from_db()
        proxy_conf = self.build_proxy_conf_from_server_list( proxy_servers )
        # write the string to S3
        return util.write_proxy_conf_to_s3( proxy_conf )

    # internal method
    # scan the Group table and parse the DISTINCT proxy server information
    def get_proxy_servers_from_db( self ):
        proxy_server_map = {}
        proxy_servers = []
        for group in Erad_Group2().scan():
            if util.is_valid_proxy(group.RemoteServerUrl):
                if not group.ID is None:
                    proxy =  {
                        "name": util.construct_proxy_id( group.RemoteServerUrl ),
                        "ipaddr": util.parse_ipaddr(group.RemoteServerUrl),
                        "port": util.parse_port(group.RemoteServerUrl),
                        "secret": util.parse_shared_secret(group.SharedSecret)
                        }
                    if util.is_valid_ipaddr(proxy["ipaddr"]):
                        if not proxy["name"] in proxy_server_map:
                            proxy_server_map[proxy["name"]] = True
                            proxy_servers.append( proxy )
        return proxy_servers

    # internal method
    # convert a list of proxy servers into a free radius proxy.conf file as a string
    def build_proxy_conf_from_server_list( self, proxy_servers ):
        template = Template("""
proxy server {
    default_fallback = no
}

<%def name="generate_server(server)">
home_server ${server['name']} {
    type = auth
    ipaddr = ${server['ipaddr']}
    port = ${server['port']}
    secret = ${server['secret']}
    response_window = 20
    zombie_period = 40
    revive_interval = 120
    status_check = request
    username = ping
    password = test
    check_interval = 30
    check_timeout = 4
    num_answers_to_alive = 3
    max_outstanding = 65536
}
</%def>

<%def name="generate_servers()">
   % for server in proxy_servers:
      ${generate_server(server)}
   % endfor
</%def>
${generate_servers()}

<%def name="generate_pools()">
% for server in proxy_servers:
home_server_pool ${server['name']}_pool {
    type = fail-over
    home_server = ${server['name']}
}
% endfor

</%def>

${generate_pools()}

realm LOCAL {
    #  If we do not specify a server pool, the realm is LOCAL, and
    #  requests are not proxied to it.
}

<%def name="generate_realms()">
% for server in proxy_servers:
realm ${server['name']} {
    auth_pool = ${server['name']}_pool
}
% endfor
</%def>

${generate_realms()}

realm "~(.*\.)*wifi-onthego\.calixcloud\.com$" {
}
realm "~(.*\.)*wifi-onthego\.com$" {
}

realm "~(.*\.)*staging\.passpoint\.stie" {
}
realm "~(.*\.)*passpoint\.site$" {
}
        """)
        # generate the proxy.conf use mako
        return template.render(proxy_servers = proxy_servers)


    def session_load( self, fields ):
        # use session to determine groups
        session_model = self.load_and_use_session( fields )
        obj = self.standard_output_object( session_model )
        # sort the model's group ID list
        obj['Session']['Group_ID_List'].sort()
        # Omit LastUsed field
        obj['Session'].pop("LastUsed", None)
        return obj

    def audit_search( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/audit/search' )

        # get the group ID
        group_fields = self.expect_dict( fields, Erad_Group2.Meta.name )
        Group_ID = self.expect_unicode_value( group_fields, "ID" )

        # make sure the session has access
        session_model = self.load_and_use_session( fields )
        self.authorize_session_for_group( session_model, Group_ID )

        # get search parameters
        after_date = self.expect_datetime_value_utc( fields, "AfterDate" )
        before_date = self.expect_datetime_value_utc( fields, "BeforeDate" )

        # convert dates to instances in order to search with them
        after_date_instance =  Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch( after_date ), 0.0 )
        before_date_instance = Auditor().instance_pack( util.datetime_to_milliseconds_since_epoch( before_date ), 0.0 )

        # search for audit records
        audit_models = Erad_Audit.accountgroupid_index.query(
            session_model.Account_ID+"::"+Group_ID,
            AccountGroupIdIndex.Instance.between(after_date_instance, before_date_instance)
        )
        audits = []
        for model in audit_models:
            audits.append( model_to_pod( model ) )
        # sort audits in reverse chronological order
        audits.sort(key=lambda m: -(m['Instance'] or 0))

        return { 'Audit_List': audits, "Count": len(audits) }

    def group_list( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/group/list' )

        # use session to determine groups
        session_model = self.load_and_use_session( fields )
        group2_account_id = session_model.Account_ID
        groups = []

        instance_interval = self.expect_optional_parameter(fields, "Instance_Interval", None)

        for group_id in session_model.Group_ID_List:
            try:
                group_model = Erad_Group2().get(group2_account_id, group_id)
                group_model.TimeZone = group_model.TimeZone or "America/Los_Angeles";

                js_obj = self.filter_private_fields(model_to_pod( group_model ))
                js_obj = util.none_to_false(js_obj, self.group_possible_none_fields)

                js_obj['highCapacityProvisioned'] = self.group_has_high_provisioned_port(group2_account_id, group_id)

                groups.append(js_obj)
            except:
                pass
        # return groups always in the same order
        groups.sort(key=lambda g: (g['Name'] or g['ID']))
        return { 'Group_List': groups, "Count": len(groups) }

    def filter_private_fields(self, js_obj):
        """ Remove private fields from group object
        and mark group with remote server name
        """
        filtered_data = set([
            '52.41.63.155:1812',
            '159.166.57.12:1812'
        ])

        filtered_fileds = set([
            'SharedSecret',
            'RemoteServerUrl',
        ])


        if js_obj['RemoteServerUrl'] in filtered_data:
            js_obj['RemoteServer'] = self.remote_authentication_options_index[js_obj['RemoteServerUrl']]

            for field in filtered_fileds:
                js_obj[field] = ''

        return js_obj

    def group_has_high_provisioned_port(self, group2_account_id, group_id):
        endpoints_model = Erad_Endpoint().group_id_index.query(group2_account_id + "::" + group_id)
        for endpoint in endpoints_model:
            if int(endpoint.Port) >= HIGH_PROVISIONED_PORT_GREATER_EQUAL:
                return True
        return False

    def group_load( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/group/load' )

        group_model = Erad_Group2()

        # get the group
        group_fields = self.expect_dict( fields, group_model.Meta.name )
        self.set_unicode_value_nonempty( group_model, group_fields, "ID" )

        # make sure the session has access
        session_model = self.load_and_use_session( fields )
        self.authorize_session_for_group( session_model, group_model.ID )

        # retrieve the group
        group2_account_id = session_model.Account_ID
        group_model = Erad_Group2.get(group2_account_id, group_model.ID)
        # save the group
        group_model.refresh(consistent_read=True)
        group_model.TimeZone = group_model.TimeZone or "America/Los_Angeles";
        js_obj = self.standard_output_object( group_model )['Group']
        js_obj = self.filter_private_fields(js_obj)

        js_obj = util.none_to_false(js_obj, self.group_possible_none_fields)

        js_obj['highCapacityProvisioned'] = self.group_has_high_provisioned_port(group2_account_id, group_model.ID)

        return {'Group' : js_obj}

    def get_group_unique_devices(self, fields):
        # authorize if signature verification
        self.authorize_signature(fields, 'erad/api/group_unique_devices/load')

        group_model = Erad_Group2()

        # get the group
        group_fields = self.expect_dict(fields, group_model.Meta.name)
        self.set_unicode_value_nonempty(group_model, group_fields, "ID")

        # make sure the session has access
        session_model = self.load_and_use_session(fields)
        self.authorize_session_for_group(session_model, group_model.ID)

        # retrieve the group
        group2_account_id = session_model.Account_ID
        group_model = Erad_Group2.get(group2_account_id, group_model.ID)
        # save the group
        group_model.refresh(consistent_read=True)
        group_model.TimeZone = group_model.TimeZone or "America/Los_Angeles";
        js_obj = self.standard_output_object(group_model)['Group']
        js_obj = self.filter_private_fields(js_obj)

        js_obj = util.none_to_false(js_obj, self.group_possible_none_fields)

        js_obj['highCapacityProvisioned'] = self.group_has_high_provisioned_port(group2_account_id, group_model.ID)
        js_obj['UniqueDevices'] = self.get_unique_devices_count(
            group2_account_id, group_model.ID,
            self.expect_optional_parameter(fields, "Instance_Interval", None)
        )
        return {'Group': js_obj}

    def group_save( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/group/save' )

        group_model = Erad_Group2()

        # get the group
        group_fields = self.expect_dict( fields, group_model.Meta.name )
        self.set_unicode_value_nonempty( group_model, group_fields, "ID" )

        # make sure the session has access
        session_model = self.load_and_use_session( fields )

        # Always authirize if AccountOwner=1. If AccountOwner=0 then check against the session
        if not session_model.AccountOwner:
            self.authorize_session_for_group( session_model, group_model.ID )

        # Get Account_ID
        group_model.Account_ID = Erad_Session().get(session_model.ID).Account_ID

        # retrieve existing group attributes
        # TODO: this could be optimized by using dynamo partial updates
        proxy_changed = False
        old_proxy = ""
        old_secret = ""
        old_name = ""
        old_timezone = ""
        try:
            group_model = Erad_Group2.get(group_model.Account_ID, group_model.ID)
            group_model.refresh(consistent_read=True)
            group_exists = True
            old_proxy = group_model.RemoteServerUrl or ""
            old_secret = group_model.SharedSecret or ""
            old_name = group_model.Name
            old_timezone = group_model.TimeZone
        except DoesNotExist:
            group_exists = False;
            # Slightly odd spec here. We do not want the admin to be able to edit the Name
            # However, if the group does not exist, we will create it.
            # This group name will be overitten by the integration services.
            group_model.Name = "Group " + group_model.ID
            group_model.TimeZone = "America/Los_Angeles"

            # Seting default values for optional fields of new group.
            # Because functions set_optional_*() will not set values for optional fields.
            # If not do this db will store values as None
            group_model.MaxDownloadSpeedBits = 0
            group_model.MaxUploadSpeedBits = 0
            group_model.OverrideVlan = False
            group_model.OverrideConnectionSpeed = False

        # now override attributes
        self.set_unicode_value(group_model, group_fields, "RemoteServerUrl")
        self.set_unicode_value(group_model, group_fields, "SharedSecret")
        self.set_optional_unicode_value(group_model, group_fields, "Domain")
        self.set_optional_unicode_value(group_model, group_fields, "Name")
        self.set_optional_unicode_value(group_model, group_fields, "TimeZone")
        self.set_optional_integer_value(group_model, group_fields, "MaxDownloadSpeedBits")
        self.set_optional_integer_value(group_model, group_fields, "MaxUploadSpeedBits")
        self.set_optional_boolean_value(group_model, group_fields, "OverrideVlan")
        self.set_optional_boolean_value(group_model, group_fields, "OverrideConnectionSpeed")
        self.set_optional_unicode_value(group_model, group_fields, "ClientInfoArn")

        remote_server_url = str(self.expect_optional_parameter(group_fields, "RemoteServer", None))

        if remote_server_url is not None and remote_server_url in self.remote_authentication_options:
            remote_auth_options = self.remote_authentication_options[remote_server_url]
            group_model.SharedSecret = remote_auth_options['SharedSecret']
            group_model.RemoteServerUrl = remote_auth_options['RemoteServerUrl']

        if not session_model.AccountOwner:
            # User cannot pass 'Name'
            restricted_field = 'Name'
            if restricted_field in group_fields and old_name != group_model.Name:
                raise EradRestrictedFieldException( "Request Denied. Parameter '{0}' is restricted.".format( restricted_field ) )

            # User cannot pass 'TimeZone'
            restricted_field = 'TimeZone'
            if restricted_field in group_fields and old_timezone.lower() not in (group_model.TimeZone.lower(), group_model.TimeZone.lower().replace(' ', '_'), group_model.TimeZone.lower().replace('_', ' ')):
                raise EradRestrictedFieldException( "Request Denied. Parameter '{0}' is restricted.".format( restricted_field ) )
            else:
                group_model.TimeZone = old_timezone

        # force a consistent format for RemoteServerUrl
        group_model.RemoteServerUrl = util.sanitize_proxy( group_model.RemoteServerUrl )

        new_proxy = group_model.RemoteServerUrl or ""
        new_secret = group_model.SharedSecret or ""
        is_new_proxy_valid = util.is_valid_proxy( new_proxy )
        proxy_changed = (old_proxy != new_proxy) or (old_secret != new_secret)
        proxy_conf_dirty = ((not group_exists) and is_new_proxy_valid) or (group_exists and proxy_changed)

        # Before saving, check to see if we might corrupt the proxy.conf
        if proxy_conf_dirty:
            # skip checking if group, without RemoteServerUrl
            if group_model.RemoteServerUrl is None:
                group_model.SharedSecret = None
            else:
                for other_group_model in Erad_Group2().scan():
                    # skip ourself
                    if other_group_model.ID == group_model.ID and other_group_model.Account_ID == group_model.Account_ID:
                        continue
                    # check if we have a duplicate
                    if other_group_model.RemoteServerUrl == group_model.RemoteServerUrl:
                        # if the Shared Secret is different, throw an error
                        if other_group_model.SharedSecret != group_model.SharedSecret:
                            raise EradValidationException( "The Shared Secret entered is incorrect." )

        # Save the group
        group_model.save()

        # If a new group is created - add group_id to the current session
        if not group_exists:
            session_model.Group_ID_List.add(group_model.ID)
            session_model.save()

        # audit save of group
        if util.erad_cfg().audit.admin.group:
            Auditor().record( "Edit Group", group_model.ID, session_model.Identifier, group_model.Account_ID )

        # if the proxy configuration is dirty, we need to regenerate it on S3
        if util.erad_cfg().proxy_conf.can_write and proxy_conf_dirty:
            self.deploy_latest_proxy_conf()

        js_obj = self.standard_output_object( group_model )['Group']
        js_obj = self.filter_private_fields(js_obj)
        js_obj = util.none_to_false(js_obj, self.group_possible_none_fields)

        js_obj['highCapacityProvisioned'] = self.group_has_high_provisioned_port(group_model.Account_ID, group_model.ID)

        return {'Group' : js_obj}

        # return self.standard_output_object( group_model )

    def group_delete(self, fields):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/group/delete' )

        # get the group
        group_model = Erad_Group2()
        group_fields = self.expect_dict(fields, group_model.Meta.name)
        self.set_unicode_value_nonempty(group_model, group_fields, "ID")

        # authorize the caller with integration key or session + check the group permission
        account_id = self.authorize_session_or_key(fields, group_model.ID)

        # Raise an exception when the group is a Global group.
        if group_model.ID in ('Global', 'global'):
            raise EradValidationException('Global site cannot be deleted.')

        session_model = None
        group_account_id = ''
        if 'IntegrationKey' in fields:
            key_input = self.expect_parameter(fields, 'IntegrationKey')
            api_key = Erad_ApiKey().get(key_input)
            group_account_id = api_key.Account_ID
            identifier = util.mask_identifier(api_key.ApiKey_ID)
        else:
            session_model = self.load_and_use_session(fields)
            group_account_id = session_model.Account_ID
            identifier = session_model.Identifier

        # Before delete group, delete all authenticators and supplicant devices.
        for authenticator in Erad_Authenticator2.account_group_index.query(
            account_id,
            AccountGroupIDIndex2.Group_ID==group_model.ID):
            authenticator.delete()
            if util.erad_cfg().audit.admin.authenticator:
                Auditor().record(
                    "Delete Authenticator - {}".format(authenticator.ID),
                    authenticator.Group_ID,
                    account_id,
                    account_id
                )

        for supplicant in Erad_Supplicant.query(group_model.ID):
            supplicant.delete()
            if util.erad_cfg().audit.admin.supplicant:
                Auditor().record(
                    "Delete Supplicant '{0}'".format(supplicant.Username or ''),
                    supplicant.Group_ID,
                    identifier,
                    account_id
                )

        # freeing endpoints
        endpoint_group_id_index = account_id + "::" + group_model.ID
        endpoints = [endpoint for endpoint in Erad_Endpoint.group_id_index.query(endpoint_group_id_index)]
        self.__remove_endpoints(endpoints, account_id)

        # delete the group
        if session_model is not None:
            # remove the group from the session
            session_model.Group_ID_List.remove(group_model.ID)
            session_model.save()

        group_model = Erad_Group2.get(group_account_id, group_model.ID)
        group_model.delete()
        if util.erad_cfg().audit.admin.group:
            Auditor().record(
                "Delete Group '{0}'".format(group_model.ID),
                group_model.ID,
                identifier,
                account_id
            )

    def supplicant_list(self, fields):
        # authorize if signature verification
        self.authorize_signature(fields, 'erad/api/supplicant/list')

        # get the Group_ID parameter
        group_fields = self.expect_dict(fields, Erad_Group2.Meta.name)
        Group_ID = self.expect_parameter(group_fields, "ID")

        # make sure the session has access
        session_model = self.load_and_use_session(fields)
        self.authorize_session_for_group(session_model, Group_ID)

        # use Account_ID::Group_ID to retrieve supplicants
        sup_models = Erad_Supplicant().query(session_model.Account_ID + "::" + Group_ID)
        sups = []
        for model in sup_models:
            sups.append(model_to_pod(model))

        # return groups always in the same order
        sups.sort(key=lambda g: g['Username'])

        return {'Supplicant_List': sups}

    def supplicant_import_csv(self, fields, file):
        if not file:
            raise EradInvalidFileException("You must upload a file.")

        # authorize if signature verification
        self.authorize_signature(fields, 'erad/api/supplicant/import/csv')

        # get the Session_ID and Group_ID parameters
        Session_ID = self.expect_parameter(fields, "Session_ID")
        Group_ID = self.expect_parameter(fields, "Group_ID")

        # make sure the session has access
        session_model = self.load_and_use_session({"Session": {"ID": Session_ID}})
        self.authorize_session_for_group(session_model, Group_ID)

        # parse CSV file
        stream = codecs.iterdecode(file.stream, 'utf-8')
        dict_lines = csv.DictReader(stream)

        import_count = 0

        may_be_None = {'MaxDownloadSpeedBits', 'MaxUploadSpeedBits'}

        # save each supplicant
        for line in dict_lines:
            new_fields = {"Session": {"ID": Session_ID}, "Supplicant": {"Group_ID": Group_ID}}
            for k in line:
                if k in may_be_None:
                    value = (line[k] or '')
                    if value == '':
                        continue  # skiping None value
                else:
                    # set the appropriate field and try to decode the value as utf-8, ignoring anything that is not utf8.
                    new_fields["Supplicant"][k] = (line[k] or '')

            username = new_fields['Supplicant']['Username']
            if util.is_valid_mac(username) or \
                    util.is_valid_mac_hex12(username) or \
                    util.is_valid_wildcarded_mac(username):
                new_fields.get("Supplicant").update({"MacAddress": username})
                self.supplicant_mac_save(new_fields, is_import=True)
            else:
                self.supplicant_save(new_fields, is_import=True)
            import_count += 1

        # audit import of supplicant csv
        if util.erad_cfg().audit.admin.supplicant:
            Auditor().record("Import Supplicant CSV - {} imported supplicants".format(import_count or ''), Group_ID,
                             session_model.Identifier, session_model.Account_ID)
        return {"Count": import_count}

    def supplicant_export_csv(self, fields):
        # authorize if signature verification
        self.authorize_signature(fields, 'erad/api/supplicant/export/csv')

        # get the Session_ID and Group_ID parameters
        Session_ID = self.expect_parameter(fields, "Session_ID")
        Group_ID = self.expect_parameter(fields, "Group_ID")

        # make sure the session has access
        session_model = self.load_and_use_session({"Session": {"ID": Session_ID}})
        self.authorize_session_for_group(session_model, Group_ID)

        # use Account_ID::Group_ID to retrieve supplicants
        sup_models = Erad_Supplicant().query(session_model.Account_ID + "::" + Group_ID)
        sups = []
        for model in sup_models:
            sups.append(model_to_pod(model))

        # return groups always in the same order
        sups.sort(key=lambda g: g['Username'])

        csv_fields = supplicant_import_export_fields()

        # convert to array of arrays
        sup_list = []

        # first row is headers
        sup_header = []
        for csv_field in csv_fields:
            sup_header.append(csv_field)
        sup_list.append(sup_header)

        # add each supplicant as a separate array
        for sup in sups:
            sup_array = []
            for csv_field in csv_fields:
                sup_array.append(sup[csv_field])
            sup_list.append(sup_array)

        # return a stream containing the CSV file
        si = io.StringIO()
        cw = csv.writer(si)
        cw.writerows(sup_list)
        return si.getvalue()

    def supplicant_find_by_parent(self, fields):
        # authorize caller
        self.authorize_signature( fields, 'erad/api/supplicant/find-by-parent-id' )

        sup_fields = self.expect_dict(fields, Erad_Supplicant().Meta.name)
        group_id = self.expect_unicode_value(sup_fields, "Group_ID")
        parent_id = self.expect_unicode_value(sup_fields, "Parent_ID")

        account_id = self.authorize_session_or_key(fields, group_id)

        output = []

        try:
            supplicants = Erad_Supplicant.Parent_ID_index.query(
                parent_id,
                SupplicantParentIdIndex.Account_ID == account_id
            )
            for supplicant in supplicants:
                output.append(model_to_pod(supplicant))
        except Exception as e:
            logger.exception(e)
            raise EradConfigurationException('Cannot fetch supplicants from database') 

        return {"Supplicants": output}

    def supplicant_load( self, fields ):

        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/supplicant/load' )

        sup_model = Erad_Supplicant()

        # get the supplicant
        sup_fields = self.expect_dict( fields, sup_model.Meta.name )
        self.set_unicode_value_nonempty( sup_model, sup_fields, "Group_ID" )
        self.set_unicode_value( sup_model, sup_fields, "Username" )
        # make sure the session has access
        account_id = self.authorize_session_or_key( fields, sup_model.Group_ID )

        notfound = True
        output = {}

        try:
            sup2_model = Erad_Supplicant.get(account_id+"::"+sup_model.Group_ID, sup_model.Username)
            output = self.standard_output_object(sup2_model)

            notfound = False
        except DoesNotExist:
            pass

        if notfound:
            raise DoesNotExist('This item does not exist in the table.')
        return output

    def _generate_message(self, sup_model, site_group_id=None):
        try:
            custom_json_data = json.loads(sup_model.CustomJsonData)
            data = {
                "UserName": sup_model.Username,
                "Parent_ID": sup_model.Parent_ID,
                "Account_ID": sup_model.Account_ID,
                "Group_ID": sup_model.Group_ID,
                "Site_Group_ID": site_group_id,
                "Description": sup_model.Description,
                "CustomJsonData": custom_json_data,
                "CreatedAt": custom_json_data.get('CreatedAt'),
                "RequestType": "NewProfile",
                "EventTimestamp_iso": datetime.utcnow().isoformat()
            }
            return json.dumps([data])
        except Exception as err:
            logger.exception(err)
            return json.dumps([])

    def supplicant_save(self, fields, is_mac_address=False, is_import=False):
        # authorize if signature verification
        self.authorize_signature(fields, 'erad/api/supplicant/save')

        sup_fields = self.expect_dict(fields, Erad_Supplicant.Meta.name)
        _group_id = self.expect_unicode_value(sup_fields, 'Group_ID')
        username = self.expect_unicode_value(sup_fields, 'Username')

        # Get group_id if passed from site
        site_group_id = self.expect_optional_parameter_not_none(fields, 'Site_Group_ID', None)

        # make sure the session has access
        account_id = self.authorize_session_or_key(fields, _group_id)
        identifier = "IntegrationKey"

        if ("Session" in fields):
            session_model = self.load_and_use_session(fields)
            identifier = session_model.Identifier

        group_id = '::'.join([account_id, _group_id])

        try:
            sup_model = Erad_Supplicant.get(group_id, username)
            sup_exists = True
        except DoesNotExist:
            sup_exists = False
            sup_model = Erad_Supplicant(group_id, username)

        try:
            self.set_unicode_value(sup_model, sup_fields, "Password")
            self.set_optional_unicode_value(sup_model, sup_fields, "Parent_ID")
            self.set_optional_unicode_value(sup_model, sup_fields, "DeviceName")
            self.set_optional_unicode_value(sup_model, sup_fields, "Description")
            self.set_optional_unicode_value(sup_model, sup_fields, "Location")
            self.set_optional_datetime_value_utc_none(sup_model, sup_fields, "ExpirationDate")
            self.set_optional_integer_value(sup_model, sup_fields, "MaxDownloadSpeedBits")
            self.set_optional_integer_value(sup_model, sup_fields, "MaxUploadSpeedBits")
            self.set_optional_integer_value_none(sup_model, sup_fields, "Vlan")
            self.set_boolean_value(sup_model, sup_fields, "UseRemote", False)
            self.set_optional_unicode_value(sup_model, sup_fields, "CustomJsonData")
        except Exception as err:
            logger.debug(err, exc_info=True)
            raise EradMalformedException(err)

        # check for wildcard here
        sup_model.UseWildcard = util.is_wild_carded(sup_model.Username)
        if not sup_model.UseWildcard and not is_mac_address:
            sup_model.Username = sup_model.Username.lower()

        # save account_id if none exists
        if not sup_model.Account_ID:
            sup_model.Account_ID = account_id

        # If new supplicant belongs to Global group check that there is no supplicant with the same name in non Global groups
        # if new supplicant belongs to non Global group check that there is no supplicant with the same name in Global groups
        # It is acceptable to have supplicants with the same name but in different non Global groups
        if sup_model.Group_ID == account_id+"::Global":
            try:
                existing_sup = Erad_Supplicant().scan(
                    filter_condition=Erad_Supplicant.Group_ID.startswith(account_id+"::") & (Erad_Supplicant.Username==sup_model.Username)
                )
            except:
                pass
            else:
                for item in existing_sup:
                    if item.Group_ID != account_id+"::Global":
                        raise EradValidationException( "Supplicant already exists." )
        else:
            try:
                existing_sup = Erad_Supplicant().get(account_id+"::Global", sup_model.Username)
            except:
                pass
            else:
                raise EradValidationException( "Supplicant already exists in the Global site." )

        external_id = sup_fields.get('Parent_ID')

        if external_id:
            try:
                supplicants = Erad_Supplicant.Parent_ID_index.query(
                    external_id,
                    SupplicantParentIdIndex.Account_ID == account_id
                )
            except:
                pass
            else:
                for supplicant in supplicants:
                    if supplicant.profile_used:
                        sup_model.profile_used = supplicant.profile_used
                        break

        # If user do export/import this code will override data in DB.
        # So doing it only if it is not import.
        if is_import is False:
            # Add custom data (device_type and profile creation time)
            device_type = sup_fields.get('DeviceType', 'unknown')
            nai_realm = sup_fields.get('nair_realm', None)
            rcoi = sup_fields.get('rcoi', None)
            created_at = datetime.utcnow().isoformat()

            try:
                json_data = json.loads(sup_model.CustomJsonData)
            except Exception:
                json_data = {}
            finally:
                custom_json_data = json.dumps({
                    **json_data,
                    "DeviceType": device_type,
                    "CreatedAt": created_at,
                    "rcoi": rcoi,
                    "nai_realm": nai_realm,
                })

                sup_model.CustomJsonData = custom_json_data

        sup_model.save()

        # Generate and send supplicant data to SNS.

        sns_data = self._generate_message(sup_model, site_group_id)
        logger.debug("SNS data: %s", sns_data)

        # Send supplicant data to SNS.
        send_log_sns(sns_data, "Supplicant-Session")

        # audit save of supplicant
        if util.erad_cfg().audit.admin.supplicant:
            Auditor().record("Save Supplicant '{0}'".format(sup_model.Username or '' ), _group_id, identifier, account_id)


        # Remove field "UseWildcard" from response
        response = self.standard_output_object( sup_model )
        del response["Supplicant"]["UseWildcard"]

        return response

    def _generate_certificate_message(self, model_cert, model_supp, site_group_id):
        try:
            custom_json_data = json.loads(model_supp.CustomJsonData)

            data = [{
                "RequestType": "NewProfileCert",
                "EventTimestamp_iso": datetime.utcnow().isoformat(),
                "Parent_ID": model_supp.Parent_ID,
                "UserName": model_supp.Username,
                "Account_ID": model_supp.Account_ID,
                "Group_ID": model_supp.Group_ID,
                "Site_Group_ID": site_group_id,
                "Description": model_supp.Description,
                "CustomJsonData": custom_json_data,
                "CreatedAt": custom_json_data.get("CreatedAt"),
            }]

            return json.dumps(data)
        except Exception as err:
            logger.exception(err)
            return json.dumps([])

    def supplicant_certificate(self, fields):
        """This is optional step for supplicant model. It expects that supplicant model (username and password)
        already exists, and will raise exception if attempt to create cefiticate will be made"""
        # authorize if signature verification
        self.authorize_signature(fields, 'erad/api/supplicant/certificate')

        # mostrly used just to store payload data for this function
        sup_model = Erad_Supplicant()

        # get the supplicant
        sup_fields = self.expect_dict(fields, sup_model.Meta.name)
        no_password = self.expect_optional_parameter(fields, "NoPassword", None)

        # Get group_id from site
        site_group_id = self.expect_optional_parameter_not_none(fields, 'Site_Group_ID', None)

        if no_password is not None and no_password.strip().lower() == 'true':
            no_password = True
        else:
            no_password = False


        self.set_unicode_value_nonempty(sup_model, sup_fields, "Group_ID")
        self.set_unicode_value(sup_model, sup_fields, "Username")

        # make sure the session has access
        account_id = self.authorize_session_or_key(fields, sup_model.Group_ID)

        notfound = True

        account_group_id = account_id + "::" + sup_model.Group_ID
        try:
            # username for passpoint will be with realm
            sup2_model = Erad_Supplicant.get(account_group_id, sup_model.Username)
            notfound = False
        except DoesNotExist:
            pass

        if notfound:
            raise DoesNotExist('This item does not exist in the table.')

        current_dir = os.path.dirname(os.path.realpath(__file__))
        rand_str = util.generate_random_string(32)

        # username in payload can have realm, in this case extract realm to attach it to CN value in cert
        user = sup2_model.Username.split("@")
        common_name = rand_str
        # if realm was passed using it
        if len(user) > 1:
            realm = user[1]
            common_name = f"{rand_str}@{realm}"

        # Generate certificate
        create_key = Popen(shlex.split('openssl req -newkey rsa:4096 '
                                       '-out {0}/certs/client-{1}.csr '
                                       '-keyout {0}/certs/client-{1}.key '
                                       '-nodes '
                                       '-subj '
                                       '"/C=US/L=Secure Wi-Fi/O=enterpriseauth.com/CN={2}"'.format(current_dir, rand_str, common_name)),
                           stdout=PIPE,
                           stderr=STDOUT)
        (stdoutdata, stderrdata) = create_key.communicate()

        # Sign the generated certificate
        # password will be updated on production systems during deployment
        sign_client_cert = Popen(shlex.split("openssl x509 -req -days 3652 "
                                             "-CAkey {0}/certs/ca.key "
                                             "-CA {0}/certs/ca.pem "
                                             "-in {0}/certs/client-{1}.csr "
                                             "-out {0}/certs/client-{1}.crt "
                                             "-CAcreateserial "
                                             "-passin pass:non3y4db".format(current_dir, rand_str)),
                                 stdout=PIPE,
                                 stderr=STDOUT)
        (stdoutdata, stderrdata) = sign_client_cert.communicate()

        # Convert signed certificate to p12
        # consider adding field to EradSupplicant model to indicate what certificate with empty password
        p12_password = '' if no_password is True else 'WYPCMS6T'
        p12_convert = Popen(shlex.split("openssl pkcs12 -export "
                                        "-in {0}/certs/client-{1}.crt "
                                        "-inkey {0}/certs/client-{1}.key "
                                        "-out {0}/certs/client-{1}.p12 "
                                        "-password pass:{2}".format(current_dir, rand_str, p12_password)),
                            stdout=PIPE,
                            stderr=STDOUT)
        (stdoutdata, stderrdata) = p12_convert.communicate()


        sha256_fingerprint_proc = Popen(shlex.split("openssl x509 -noout "
                                        "-fingerprint "
                                        "-sha256 "
                                        "-inform pem "
                                        "-in {0}/certs/client-{1}.crt ".format(current_dir, rand_str)),
                            stdout=PIPE,
                            stderr=STDOUT)
        (stdoutdata, stderrdata) = sha256_fingerprint_proc.communicate()


        sha256_fingerprint = stdoutdata.decode().replace('SHA256 Fingerprint=', '').replace('\n', '').replace('sha256 Fingerprint=', '').replace('\n', '')

        # Return p12 as base64 encoded
        base64_encode = check_output(shlex.split("base64 {0}/certs/client-{1}.p12".format(current_dir, rand_str))).decode().replace('\n', '')

        saved_account_id = sup2_model.Group_ID.split("::")[0]
        saved_group_id = sup2_model.Group_ID.split("::")[1]

        # save the certificate
        supcert_model = Erad_SupplicantCertificate()

        # common name with or without realm, realm is necessry to save to be able implement (inter-BSP roaming)
        # supplicant search using realm instead of group/endpoint
        supcert_model.ID = common_name

        # in model for cert saving username with realm
        supcert_model.Username = sup_model.Username
        supcert_model.Group_ID = account_group_id
        supcert_model.Certificate = base64_encode
        supcert_model.Format = "p12"
        supcert_model.Timestamp = utcnow()
        supcert_model.Checksum = self.password_based_checksum(
            saved_account_id+saved_group_id+sup2_model.Username+sup2_model.Password,
            saved_account_id)
        supcert_model.save()

        # sup2_model - main supplicant record with other info
        msg = self._generate_certificate_message(supcert_model, sup2_model, site_group_id)
        logger.debug("sns message: %s", msg)
        send_log_sns(msg, "Supplicant-Certificate-Session")

        # remove generated files
        for ext in ["crt", "csr", "key", "p12"]:
            os.remove("{0}/certs/client-{1}.{2}".format(current_dir, rand_str, ext))

        return {"Certificate": base64_encode, 'Sha256-fingerprint': sha256_fingerprint}

    def supplicant_mac_save(self, fields, is_import=False):
        # use the Mac Address as the
        sup_fields = self.expect_dict(fields, Erad_Supplicant().Meta.name)

        MacAddress = self.expect_unicode_value(sup_fields, "MacAddress")
        is_wildcarded = ("^" in MacAddress) or ("*" in MacAddress)
        try:
            if is_wildcarded:
                MacAddress = util.wild_sanitize_mac(MacAddress)
            else:
                MacAddress = util.sanitize_mac(MacAddress)
        except:
            raise EradValidationException("Invalid MacAddress.")

        sup_fields["Username"] = MacAddress
        sup_fields["Password"] = MacAddress
        supplicant_save_parameters = {}
        supplicant_save_parameters['Supplicant'] = sup_fields
        supplicant_save_parameters['Session'] = fields['Session']
        supplicant_save_parameters['Group'] = {'ID': sup_fields['Group_ID']}

        account_id = self.authorize_session_or_key(supplicant_save_parameters, sup_fields['Group_ID'])
        # audit save of supplicant mac
        if util.erad_cfg().audit.admin.supplicant:
            Auditor().record("Save Supplicant MAC'{0}'".format(MacAddress or '' ), sup_fields['Group_ID'], account_id, account_id)
        return self.supplicant_save(supplicant_save_parameters, is_mac_address=True, is_import=is_import)

    def supplicant_delete( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/supplicant/delete' )

        sup_model = Erad_Supplicant()

        # make the supplicant
        sup_fields = self.expect_dict( fields, sup_model.Meta.name )
        self.set_unicode_value_nonempty( sup_model, sup_fields, "Group_ID" )
        self.set_unicode_value( sup_model, sup_fields, "Username" )

        # make sure the session has access
        session_model = self.load_and_use_session( fields )
        self.authorize_session_for_group( session_model, sup_model.Group_ID )

        # delete the Account_ID::Group_ID supplicant
        old_group_id = sup_model.Group_ID
        sup_model.Group_ID = session_model.Account_ID+"::"+sup_model.Group_ID

        # delete the supplicant
        sup_model.delete()


        # audit delete of supplicant
        if util.erad_cfg().audit.admin.supplicant:
            Auditor().record("Delete Supplicant '{0}'".format(sup_model.Username or ''), old_group_id, session_model.Identifier, session_model.Account_ID)

    def supplicant_log_group_list(self, fields):
        """Querying logs from DB and returning paginated response, but it is not used anywhere
        and not exosed as API endpoint"""
        self.authorize_signature(fields, 'erad/api/supplicant/log/group/list')
        session_model = self.load_and_use_session(fields)

        # Using default value to not overload server
        limit = self.expect_optional_parameter(fields, "Limit", 100)
        last_evaluated_key = self.expect_optional_parameter(
            fields,
            "Last_Evaluated_Key",
            None,
        )

        log_fields = self.expect_dict(fields, Erad_Radius_Log.Meta.name)
        group_id = self.expect_parameter(log_fields, "Group_ID")
        interval = self.expect_parameter(log_fields, "Instance_Interval")
        include_bubble = self.expect_optional_parameter(
            log_fields,
            'Include_Bubble',
            False,
        )

        include_stray = self.expect_optional_parameter(
            log_fields,
            "Include_Stray",
            False,
        )

        # First checking group then changing group id to None if include_stray == True
        # Necessary because user does not have explicit group <account_id>::None in session object
        self.authorize_session_for_group(session_model, group_id)

        if include_stray:
            group_id = 'None'

        filter_attrs = []

        # Although interval store in filter_attrs actually instance is a range field in index
        if interval:
            filter_attrs.append(AccountGroupIdIndex2.Instance.between(*interval))

        if not include_bubble:
            filter_attrs.append(
                ~Erad_Radius_Log.Username.contains('babble') &
                ~Erad_Radius_Log.CalledStationId.contains('ba:bb:1e:ba:bb:1e')
            )

        radius_log = Erad_Radius_Log()
        # expected dict: {Group_ID: <group id>, Instance_Interval: <null or a list [from, to]>
        hash_key = f'{session_model.Account_ID}::{group_id}'
        result = radius_log.AccountGroupId_index.query(
            hash_key,
            limit=limit,
            last_evaluated_key=last_evaluated_key,
            *filter_attrs
        )
        return self.paginate_query(result)

    def authenticate_log_fields(self, fields):

        # authorize if signature verification
        self.authorize_signature(fields, 'erad/api/supplicant/log/list')

        # get the Group_ID parameter
        log_fields = self.expect_dict(fields, Erad_Radius_Log.Meta.name)
        Group_ID = self.expect_parameter(log_fields, "Group_ID")

        # make sure the session has access
        session_model = self.load_and_use_session(fields)
        # validate session only if group is not ::all::
        if Group_ID != "::all::":
            self.authorize_session_for_group(session_model, Group_ID)

        return Group_ID, log_fields, session_model

    def supplicant_log_list_generator(self, fields):
        # Function tries to query database to throw Error if no item is found
        # else returns first item found and the active generator function.

        Group_ID, log_fields, session_model = EradInterfaceAdmin().authenticate_log_fields(fields)

        log_generator = EradInterfaceAdmin().query_supplicant_logs(Group_ID, log_fields, session_model)

        initial_query = next(log_generator)

        return initial_query, log_generator

    def supplicant_log_list(self, initial_query, log_generator: Generator):

        yield initial_query

        for item in log_generator:
            yield item

    def wrap_log_generator(self, generator):
        """ Wrapping query generator so stream data can be sent as a list of objects. """
        yield "[".encode('utf-8')

        count = 0
        for i in generator:
            if count != 0: yield ",".encode('utf-8')
            yield i
            count += 1

        yield "]".encode('utf-8')

    def query_supplicant_logs(self, Group_ID, log_fields, session_model):
        Include_Stray = self.expect_parameter(log_fields, "Include_Stray")
        Include_Bubble = self.expect_optional_parameter(log_fields, 'Include_Bubble', False)
        # query for instance range key between Instance_Interval: [from, to]
        Instance_Interval = self.expect_parameter(log_fields, "Instance_Interval")
        # include Group_ID 'None' too
        if Include_Stray:
            keys = [Group_ID, 'None']
        else:
            keys = [Group_ID]
        # if Group is ::all:: then include all Group_IDs in the list to which the session has access
        if Group_ID == "::all::":
            for group in session_model.Group_ID_List:
                keys.append(group)
        filter_attrs = []
        if Instance_Interval:
            filter_attrs.append(AccountGroupIdIndex2.Instance.between(*Instance_Interval))
        if not Include_Bubble:
            filter_attrs.append(
                ~Erad_Radius_Log.Username.contains('babble') & ~Erad_Radius_Log.CalledStationId.contains(
                    'ba:bb:1e:ba:bb:1e'))
        radius_log = Erad_Radius_Log()
        logs = 0
        for group_id in keys:
            query = radius_log.AccountGroupId_index.query(
                session_model.Account_ID + "::" + group_id,
                *filter_attrs
            )
            for log in query:
                yield json.dumps(self.standard_output_object(log)[log.Meta.name]).encode('utf-8')
                logs += 1
        if logs == 0:
            raise EradConfigurationException('Cannot read the radius logs from database')

    def get_export_fields(self, fields):
        # get the Session_ID and Group_ID parameters
        Session_ID = self.expect_parameter(fields, "Session_ID")
        Group_ID = self.expect_parameter(fields, "Group_ID")
        Include_Stray = self.expect_parameter(fields, "Include_Stray")
        Include_Bubble = self.expect_optional_boolean_parameter(fields, "Include_Bubble") or False
        Instance_From = self.expect_parameter(fields, "Instance_From")
        Instance_To = self.expect_parameter(fields, "Instance_To")

        if Include_Stray != 'false':
            Include_Stray = True
        else:
            Include_Stray = False


        log_fields = {"Session": {"ID": Session_ID},
                      Erad_Radius_Log.Meta.name: {"Group_ID": Group_ID, 'Include_Stray': Include_Stray, 'Include_Bubble': Include_Bubble}}
        if Instance_From and Instance_To:
            log_fields[Erad_Radius_Log.Meta.name]['Instance_Interval'] = [int(Instance_From), int(Instance_To)]
        else:
            log_fields[Erad_Radius_Log.Meta.name]['Instance_Interval'] = None

        return log_fields

    def supplicant_log_export_csv(self, initial_query, log_generator: Generator):

        output = self.supplicant_log_list(initial_query, log_generator)

        csv_fields = supplicant_log_export_fields()

        # Stream header row
        yield ','.join(csv_fields) + '\n'

        # Loop through generator and stream remaining rows from db query.
        for sup in output:
            sup_array = []
            obj = json.loads(sup)
            for csv_field in csv_fields:
                data = obj[csv_field]
                data = '' if data is None else data
                sup_array.append(data)

            yield ','.join(sup_array) + '\n'

    def group_authenticator_list( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/group/authenticator/list' )

        # use session to determine groups
        session_model = self.load_and_use_session( fields )
        Group_ID = self.expect_parameter( fields, "Group_ID" )

        auth_list = []
        for item in Erad_Authenticator2.account_group_index.query(
            session_model.Account_ID,
            AccountGroupIDIndex2.Group_ID==Group_ID):
            auth_list.append( { item.RadiusAttribute: item.ID} )
        return { 'Auth_List': auth_list, 'Count': len(auth_list) }

    def authenticator_load( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/authenticator/load' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        # make the authenticator
        auth_model = Erad_Authenticator2()
        auth_fields = self.expect_dict( fields, auth_model.Meta.name )
        self.set_unicode_value_nonempty( auth_model, auth_fields, "ID" )
        self.set_optional_unicode_value( auth_model, auth_fields, "RadiusAttribute" )

        # if RadiusAttribute is not set or equals 'called-station-id' enforce Mac sanitization
        if(auth_model.RadiusAttribute == None or auth_model.RadiusAttribute == 'called-station-id'):
            auth_model.RadiusAttribute = "called-station-id"
            # Enforce Mac Address format here considering wildcards
            auth_model.ID = util.sanitize_mac( auth_model.ID )

        auth_model.Account_ID = account_id

        # load the authenticator
        exists = True
        try:
            auth_model.refresh(consistent_read=True)
        except DoesNotExist:
            for possible_mac in util.add_wild_card(auth_model.ID):
                try:
                    tmp_model = Erad_Authenticator2.get(possible_mac, account_id)
                    return self.standard_output_object(tmp_model)
                except DoesNotExist:
                    pass
            exists = False
        if not exists:
            raise DoesNotExist("This item does not exist in the table.")
        return self.standard_output_object( auth_model )

    def authenticator_save( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/authenticator/save' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        # make the authenticator
        auth_model = Erad_Authenticator2()
        auth_fields = self.expect_dict( fields, auth_model.Meta.name )
        self.set_unicode_value_nonempty( auth_model, auth_fields, "ID" )
        self.set_unicode_value( auth_model, auth_fields, "Group_ID" )
        self.set_optional_unicode_value( auth_model, auth_fields, "RadiusAttribute" )

        auth_model.Account_ID = account_id

        # if RadiusAttribute is not set or equals 'called-station-id' enforce Mac sanitization
        if(auth_model.RadiusAttribute == None or auth_model.RadiusAttribute == 'called-station-id'):
            auth_model.RadiusAttribute = "called-station-id"
            # Enforce Mac Address format here
            auth_model.ID = util.sanitize_mac(auth_model.ID)

        # check for existing authenticators on the account
        if Erad_Authenticator2().count(
            auth_model.ID,
            range_key_condition=Erad_Authenticator2.Account_ID==auth_model.Account_ID) > 0:
            raise EradValidationException( "DUPLICATE_RECORD" )
        else:
            # save the authenticator
            # audit save of authenticator
            if util.erad_cfg().audit.admin.authenticator:
                Auditor().record("Edit Authenticator - {}".format(auth_model.ID), auth_model.Group_ID, auth_model.Account_ID, auth_model.Account_ID)
            auth_model.save()

        return self.standard_output_object( auth_model )

    def authenticator_delete( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/authenticator/delete' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        # get the authenticator
        auth_model = Erad_Authenticator2()
        auth_fields = self.expect_dict( fields, auth_model.Meta.name )
        self.set_unicode_value_nonempty( auth_model, auth_fields, "ID" )
        self.set_optional_unicode_value( auth_model, auth_fields, "RadiusAttribute" )

        # if RadiusAttribute is not set or equals 'called-station-id' enforce Mac sanitization
        if(auth_model.RadiusAttribute == None or auth_model.RadiusAttribute == 'called-station-id'):
            auth_model.RadiusAttribute = "called-station-id"
            # Enforce Mac Address format here
            auth_model.ID = util.sanitize_mac(auth_model.ID)

        auth_model.Account_ID = account_id
        if util.erad_cfg().audit.admin.authenticator:
            if auth_model.Group_ID:
                group_id = auth_model.Group_ID
            else:
                group_id = 'Global'
            Auditor().record("Delete Authenticator - {}".format(auth_model.ID), group_id, auth_model.Account_ID, auth_model.Account_ID)

        # delete the group
        auth_model.delete()


    def endpoint_load( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/endpoint/load' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        group_id_field = self.expect_optional_parameter(fields, "Group_ID", 'Global')
        endpoints = []
        endpoints_model = Erad_Endpoint().group_id_index.query(account_id + "::" + group_id_field)
        for endpoint in endpoints_model:
            item = {"IpAddress": endpoint.IpAddress, "Port": endpoint.Port, "Secret": endpoint.Secret, "Region": endpoint.Region}
            endpoints.append(item)

        if len(endpoints) == 0 and group_id_field != 'Global':
            group_id_field = 'Global'
            endpoints_model = Erad_Endpoint().group_id_index.query(account_id + "::" + group_id_field)
            for endpoint in endpoints_model:
                item = {"IpAddress": endpoint.IpAddress, "Port": endpoint.Port, "Secret": endpoint.Secret, "Region": endpoint.Region}
                endpoints.append(item)
        if len(endpoints) == 0:
            return {}

        is_global = (group_id_field == 'Global')
        result = {'Global': is_global}
        if len(endpoints) == 1:
            result.update({'Endpoint': endpoints[0]})
        else:
            result.update({'Endpoint': endpoints})
        return result

    def is_aws_key_dev(self):
        aws_access_key_id = boto_config.get_value('Credentials', 'aws_access_key_id')
        if aws_access_key_id is None:
            return True
        erad_cfg = util.erad_cfg()
        if aws_access_key_id in erad_cfg.proxy_conf.dev_aws_access_key_id or []:
            return True

        return False

    def update_radiusconf_timestamp(self, timestamp = None):
        """ Update timestamp in file stored in s3 bucket,
        """
        if self.is_aws_key_dev() is True:
            return

        timestamp = str(timestamp)
        erad_cfg = util.erad_cfg()

        # force using credentials from boto config file
        s3_connection = s3.connect_to_region(
            erad_cfg.proxy_conf.s3_region,
            aws_access_key_id=boto_config.get_value('Credentials', 'aws_access_key_id'),
            aws_secret_access_key=boto_config.get_value('Credentials', 'aws_secret_access_key'),
            calling_format=s3.connection.OrdinaryCallingFormat()
            )

        bucket = s3_connection.get_bucket(erad_cfg.proxy_conf.bucket, validate=False)
        key = bucket.get_key(erad_cfg.proxy_conf.radiusconf_key)

        if key is None:
            key = bucket.new_key(erad_cfg.proxy_conf.radiusconf_key)
        key.set_contents_from_string(timestamp)
        s3_connection.close()

    def endpoint_save(self, fields):
        # authorize if signature verification
        self.authorize_signature(fields, 'erad/api/endpoint/save')

        # high capacity ports check
        port_service_type = self.expect_optional_parameter(
            fields, 'port_service_type', 'standard'
        )

        group_id = self.expect_parameter(fields, "Group_ID")

        # authorize the caller
        account_id = self.authorize_session_or_key(fields, group_id)

        # determine service type
        if port_service_type == 'premium':
            self.authorize_billing_key(fields)
            endpoint_source = '::premium'
        else:
            endpoint_source = '::free'

        self.validate_group_change(group_id)

        try:
            return self.__provision_endpoint(account_id, endpoint_source, fields, group_id)
        except ValueError:
            raise EradValidationException("No ports left.")

    def __provision_endpoint(self, account_id, endpoint_source, fields, group_id):
        logger.info('PROVISIONING ENDPOINT')

        endpoint_group_id_index = account_id + "::" + group_id
        region = self.expect_optional_parameter(fields, "Region", "us-west-2")

        logger.info(
            'account_id %s endpoint source %s group_id %s region %s', account_id, endpoint_source, group_id, region
        )

        # The limit of endpoints per group_ID is 2
        result = Erad_Endpoint.group_id_index.query(endpoint_group_id_index)
        endpoints = [endpoint for endpoint in result]
        if len(endpoints) >= 2 and endpoint_source != "::premium":
            logger.info('No ports left')
            raise EradValidationException("No ports left.")
        free_endpoints = [e for e in
                          Erad_Endpoint().group_id_index.query(endpoint_source, Erad_Endpoint.Region == region)]
        endpoint = random.sample(free_endpoints, 1).pop()
        logger.info('Selected endpoint %s %s %s', endpoint.IpAddress, endpoint.Port, endpoint.Region)
        endpoint.Group_ID = endpoint_group_id_index
        endpoint.save()
        item = {
            "IpAddress": endpoint.IpAddress,
            "Port": endpoint.Port,
            "Secret": endpoint.Secret,
            "Region": endpoint.Region
        }
        if util.erad_cfg().audit.admin.endpoint:
            Auditor().record(
                "Edit Endpoint - {}:{}".format(
                    endpoint.IpAddress, endpoint.Port
                ),
                endpoint.Group_ID,
                account_id,
                account_id
            )

        self.update_radiusconf_timestamp(timestamp=int(time.time()))
        logger.info('Provision complete')
        return {'Endpoint': item}

    def endpoint_delete(self, fields):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/endpoint/delete' )
        # authorize the caller
        account_id = self.authorize_session_or_key(fields)

        group_id = self.expect_parameter(fields, "Group_ID")
        port = self.expect_parameter(fields, "Port")

        self.validate_group_change(group_id)

        endpoint_group_id_str = "{0}::{1}"
        endpoint_group_id_index = endpoint_group_id_str.format(account_id, group_id)

        endpoints = [endpoint for
            endpoint in Erad_Endpoint().group_id_index.query(endpoint_group_id_index)
            if endpoint.Port == port
        ]
        if not endpoints:
            raise EradAccessException("Access Denied.")
        self.__remove_endpoints(endpoints, account_id)


    def __remove_endpoints(self, endpoints, account_id):

        for endpoint in endpoints:
            # It is awkward, but endpoint already uses Group_ID with account id, but Auditor() wants
            # only group id. So spliting endpoint group id to get necessary value
            current_group_id = endpoint.Group_ID.split('::')[1]
            endpoint.Group_ID = '::dirty'
            endpoint.Secret = util.generate_random_string(size=10)

            if util.erad_cfg().audit.admin.endpoint:
                Auditor().record(
                    "Delete Endpoint - {}:{}".format(endpoint.IpAddress, endpoint.Port),
                    current_group_id,
                    "System",
                    account_id)
            endpoint.save()

        # update config version/timestamp only if changes were made
        if endpoints:
            self.update_radiusconf_timestamp(timestamp = int(time.time()))


    def validate_group_change(self, group_id):
        if group_id.lower() == 'global':
            raise EradValidationException("Global site can't be modified.")


    def account_save( self, fields ):
        erad_cfg = util.erad_cfg()
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/account/save' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        account_model = Erad_AccountOwner()
        account_fields = self.expect_dict( fields, account_model.Meta.name )
        self.set_unicode_value_nonempty( account_model, account_fields, "Email" )
        self.set_unicode_value( account_model, account_fields, "Password" )


        if(self.is_password_blacklisted(account_model.Password)):
            raise EradBlacklistedPasswordException("That password was found in a public list of previously compromised passwords. Please choose a different password.")
        account_model.Account_ID = account_id

        salt = util.get_salt()
        account_model.Salt = salt

        account_model.Password = self.pass_hash(account_model.Password,
                                                salt,
                                                erad_cfg.crypto_params.sitewide_salt_secret_key)
        if util.erad_cfg().audit.admin.account:
            Auditor().record("Edit Account - {}".format(account_model.Account_ID), '{}::Global'.format(account_id), account_id, account_id)

        account_model.save()

    def account_load( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/account/load' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        account_model = Erad_AccountOwner()
        account_fields = self.expect_dict( fields, account_model.Meta.name )
        self.set_unicode_value_nonempty( account_model, account_fields, "Email" )

        try:
            requested_account = Erad_AccountOwner()
            requested_account.Email = account_model.Email
            requested_account.Account_ID = account_id
            requested_account.Password = "fake_password"
            requested_account.Salt = "fake_salt"
            requested_account.refresh(consistent_read=True)
        except:
            raise EradValidationException( "Account doesn't exist." )
        else:
            #requested_account.Password = None
            return_object = self.standard_output_object( requested_account )
            del return_object["AccountOwner"]["Password"]
            return return_object

    def account_list( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/account/list' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        accounts_model = Erad_AccountOwner().account_index.query(account_id)
        accounts = [account.Email for account in accounts_model]

        return {"Accounts": accounts}

    def account_delete( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/account/delete' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        account_model = Erad_AccountOwner()
        account_fields = self.expect_dict( fields, account_model.Meta.name )
        self.set_unicode_value_nonempty( account_model, account_fields, "Email" )

        try:
            requested_account = Erad_AccountOwner()
            requested_account.Email = account_model.Email
            requested_account.Account_ID = account_id
            requested_account.Password = "fake_password"
            requested_account.Salt = "fake_salt"
            requested_account.refresh(consistent_read=True)
        except:
            raise EradValidationException( "Account doesn't exist." )
        else:
            if util.erad_cfg().audit.admin.account:
                Auditor().record("Delete Account - {}".format(requested_account.Account_ID), '{}::Global'.format(account_id), account_id, account_id)
            requested_account.delete()

    def apikey_save( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/apikey/save' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        apikey_model = Erad_ApiKey()

        # get ApiKey dict from fields json
        apikey_fields = self.expect_optional_parameter( fields, apikey_model.Meta.name, {} )

        # get ApiKeyName value from input ApiKey dict
        apikey_name_field = self.expect_optional_parameter( apikey_fields, "ApiKeyName", '' )

        # check if the apikey exists - if so update instead of save
        update_call = False
        try:
            apikeys_model = Erad_ApiKey().account_index.query(account_id)
            apikeys = {apikey.ApiKeyName: [apikey.ApiKey_ID, apikey.Active, apikey.DisplayName] for apikey in apikeys_model}

            # try to set ApiKey_ID - if not found - raise an exception
            apikey_model.ApiKey_ID = apikeys[apikey_name_field][0]
            apikey_model.Active = apikeys[apikey_name_field][1]
            apikey_model.DisplayName = apikeys[apikey_name_field][2]
            apikey_model.ApiKeyName = apikey_name_field
            update_call = True
        except:
            # apikey does NOT exist

            # generate new ApiKeyName
            new_name = ''
            while True:
                new_name = "".join(random.SystemRandom().choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(10))
                try:
                    apikeys_model = Erad_ApiKey().apiKeyName_index.query(new_name)
                    apikey_model = next(apikeys_model)
                except:
                    pass
                    break

            assert new_name != ''
            apikey_model.ApiKeyName = new_name

            # generate new ApiKey_ID
            new_apikey_id = ''
            while True:
                new_apikey_id = "i_"+ "".join(random.SystemRandom().choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(32))

                try:
                    apikeys_model = Erad_ApiKey().ApiKey_ID.query(new_apikey_id)
                    apikey_model = next(apikeys_model)
                except:
                    pass
                    break

            assert new_apikey_id != ''
            apikey_model.ApiKey_ID = new_apikey_id

        apikey_model.Account_ID = account_id

        default_active_value = 1
        default_display_name_value = 'KeyName'
        if update_call:
            # in update call, if no value is provided then don't change existing value
            default_active_value = None
            default_display_name_value = None

        # get Active value from input ApiKey dict
        apikey_active_field = self.expect_optional_parameter( apikey_fields, "Active", default_active_value )
        if apikey_active_field is not None:
            apikey_model.Active = apikey_active_field

        # get display name value from input ApiKey dict
        apikey_display_name_field = self.expect_optional_parameter( apikey_fields, "DisplayName", default_display_name_value )
        if apikey_display_name_field is not None:
            apikey_model.DisplayName = apikey_display_name_field

        apikey_model.save()

        # return newly created ApiKey
        return_object = self.standard_output_object( apikey_model )
        del return_object['ApiKey']['Account_ID']
        if util.erad_cfg().audit.admin.apikey:
            Auditor().record("Edit ApiKey - {}".format(util.mask_identifier(apikey_model.ApiKey_ID)), '{}::Global'.format(account_id), account_id, account_id)
        return return_object

    def apikey_load( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/apikey/load' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        apikey_fields = self.expect_dict( fields, Erad_ApiKey().Meta.name )
        apikey_name_field = self.expect_unicode_value_nonempty( apikey_fields, "ApiKeyName" )

        apikey_model = None
        try:
            apikeys_model = Erad_ApiKey().account_index.query(account_id)
            for apikey in apikeys_model:
                if apikey.ApiKeyName == apikey_name_field:
                    apikey_model = apikey
        except:
            raise EradValidationException( "ApiKey doesn't exist." )

        if apikey_model is None:
            raise EradValidationException( "ApiKey doesn't exist." )
        else:
            return_object = self.standard_output_object( apikey_model )
            del return_object['ApiKey']['Account_ID']
            return return_object

    def apikey_list( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/apikey/list' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        apikeys_model = Erad_ApiKey().account_index.query(account_id)
        apikeys = []
        apikeys_map = []

        for apikey in apikeys_model:
            apikeys.append(apikey.ApiKeyName)
            apikeys_map.append(dict([("ApiKeyName", apikey.ApiKeyName), ("DisplayName", apikey.DisplayName)]))

        apikeys.sort()
        return dict([("ApiKeyNames", apikeys), ("ApiKeys", apikeys_map)])

    def apikey_delete( self, fields ):
        # authorize if signature verification
        self.authorize_signature( fields, 'erad/api/apikey/delete' )

        # authorize the caller
        account_id = self.authorize_session_or_key( fields )

        apikey_fields = self.expect_dict( fields, Erad_ApiKey().Meta.name )
        apikey_name_field = self.expect_unicode_value_nonempty( apikey_fields, "ApiKeyName" )

        apikey_model = None
        try:
            apikeys_model = Erad_ApiKey().account_index.query(account_id)
            for apikey in apikeys_model:
                if apikey.ApiKeyName == apikey_name_field:
                    apikey_model = apikey
        except:
            raise EradValidationException( "ApiKey doesn't exist." )

        if apikey_model is None:
            raise EradValidationException( "ApiKey doesn't exist." )
        else:
            if util.erad_cfg().audit.admin.apikey:
                Auditor().record("Delete ApiKey - {}".format(util.mask_identifier(apikey_model.ApiKey_ID)), '{}::Global'.format(account_id), account_id, account_id)
            apikey_model.delete()
