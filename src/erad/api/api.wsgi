#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import sys

sys.path.insert(0, '/usr/local/webservices/')
sys.path.extend(['/usr/local/lib64/python3.7/site-packages', '/usr/local/lib/python3.7/site-packages', '/usr/lib64/python3.7/site-packages', '/usr/lib/python3.7/site-packages', '/usr/lib64/python3.7/dist-packages', '/usr/lib/python3.7/dist-packages'])
from erad.api import app as application
