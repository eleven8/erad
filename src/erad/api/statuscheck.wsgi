#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import sys
sys.path.insert(0, '/usr/local/webservices/')
sys.path.extend(['/usr/local/lib64/python3.7/site-packages', '/usr/local/lib/python3.7/site-packages', '/usr/lib64/python3.7/site-packages', '/usr/lib/python3.7/site-packages', '/usr/lib64/python3.7/dist-packages', '/usr/lib/python3.7/dist-packages'])
import json
import requests

def application(environ, start_response):
    status = '200 OK'
    response_headers = [('Content-type', 'text/html')]
    start_response(status, response_headers)
    output = "Error 404"

    r = requests.get("http://api.enterpriseauth.com/erad/public/status/check")

    if(r.status_code == 200):
        data = r.json()
        if(data['status_check']['Name'] != None):
            output = data['status_check']['Name']
        else:
            output = "Service unavailable!"

    return [output.encode('utf-8')]

