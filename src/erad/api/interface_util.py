#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad import util
from erad.exceptions import *
from .erad_model import *
from datetime import datetime, timedelta
import pytz
import hashlib, binascii
import requests
from hashlib import sha1


import logging

logger = logging.getLogger(__name__)


class EradInterfaceUtil:
    #TODO: Autodetect parameter type and call a generic 'set_input_value'

    def paginate_query(self, result):
        limit = result._limit
        page = [i for i in result]
        last_evaluated_key = result.last_evaluated_key

        if len(page):
            log_type = page[-1].Meta.name
            page = [self.standard_output_object(i)[i.Meta.name] for i in page]
            page = {log_type: page}

        return dict(
            page=page,
            limit=limit,
            last_evaluated_key=last_evaluated_key,
        )

    def expect_parameter( self, fields, field_name ):
        if (fields is None) or (not field_name in fields):
            raise EradRequiredFieldException( "Required parameter '{0}' is missing.".format( field_name ) )
        return fields[field_name]

    def expect_optional_parameter( self, fields, field_name, default_value ):
        """Working in unexpected way, if field was passed then that value will be used even it is equal None"""
        if (not fields is None) and (field_name in fields):
            return fields[field_name]
        else:
            return default_value

    def expect_optional_parameter_not_none(self, fields, field_name, default_value):
        if (not fields is None) and (field_name in fields):
            if fields[field_name] is None or fields[field_name] == "":
                return default_value
            else:
                return fields[field_name]
        else:
            return default_value

    def expect_unicode_value( self, fields, field_name ):
        val = self.expect_parameter( fields, field_name )
        if val is None:
            return val
        if not type(val) is str:
            raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected unicode string).".format( field_name ) )
        return val if val else None

    def expect_unicode_value_nonempty( self, fields, field_name ):
        val = self.expect_parameter( fields, field_name )
        if not type(val) is str:
            raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected unicode string).".format( field_name ) )
        if not val:
            raise EradWrongTypeException( "Parameter '{0}' cannot be empty string.".format( field_name ) )
        return val

    def expect_dict( self, fields, field_name ):
        val = self.expect_parameter( fields, field_name )
        if not isinstance(val, dict):
            raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected an object).".format( field_name ) )
        return val

    def expect_date_value_from_tz( self, fields, field_name, str_tz ):
        val = self.expect_unicode_value( fields, field_name ) or ''
        try:
            val = util.parse_human_date_str( val.strip() )
        except:
            raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected date string in format MM/DD/YYYY).".format( field_name ) )
        return util.local_to_utc( val, str_tz )

    def expect_datetime_value_utc( self, fields, field_name ):
        val = self.expect_unicode_value( fields, field_name ) or ''
        try:
            return util.from_isoformat( val.strip() ).replace(tzinfo=pytz.utc)
        except:
            raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected datetime string in ISO format).".format( field_name ) )

    def set_unicode_value( self, model, fields, field_name ):
        setattr(model, field_name, self.expect_unicode_value(fields, field_name) )

    def set_unicode_value_nonempty( self, model, fields, field_name ):
        setattr(model, field_name, self.expect_unicode_value_nonempty(fields, field_name) )

    def set_optional_unicode_value( self, model, fields, field_name ):
        if (not fields is None) and (field_name in fields):
            setattr(model, field_name, self.expect_unicode_value(fields, field_name) )

    def set_integer_value( self, model, fields, field_name ):
        val = self.expect_parameter( fields, field_name ) or 0
        try:
            val = int(val)
        except ValueError:
            raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected number).".format( field_name ) )
        setattr(model, field_name, val )

    def set_boolean_value( self, model, fields, field_name, default_value ):
        val = self.expect_parameter( fields, field_name )
        if type(val) is str:
            if val.lower() == "true":
                val = True
            elif val.lower() == "false":
                val = False
        if not type(val) is bool:
            if not val:
                val = default_value
            else:
                raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected bool).".format( field_name ) )
        setattr(model, field_name, val )

    def set_unicode_array( self, model, fields, field_name ):
        val = self.expect_parameter( fields, field_name )
        if not isinstance( val, list ):
            raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected array of unicode strings).".format( field_name ) )
        setattr(model, field_name, val )

    def set_optional_date_value_from_tz( self, model, fields, field_name, str_tz ):
        if (not fields is None) and (field_name in fields) and (len(str(fields[field_name]).strip()) > 0):
            setattr(model, field_name, self.expect_date_value_from_tz(fields, field_name, str_tz) )

    def set_optional_datetime_value_utc( self, model, fields, field_name ):
        if (not fields is None) and (field_name in fields) and (fields[field_name] is not None) and (len(str(fields[field_name]).strip()) > 0):
            setattr(model, field_name, self.expect_datetime_value_utc(fields, field_name) )

    def set_optional_datetime_value_utc_none(self, model, fields, field_name):
        if (not fields is None) and (field_name in fields):
            if fields[field_name] is None:
                setattr(model, field_name, None)
            else:
                if len(str(fields[field_name]).strip()) > 0:
                    setattr(model, field_name, self.expect_datetime_value_utc(fields, field_name))

    def standard_output_object( self, model ):
        ret = {}
        ret[model.Meta.name] = model_to_pod( model )
        return ret

    def standard_output_object_tz( self, model, str_tz ):
        ret = {}
        ret[model.Meta.name] = model_to_pod( model, str_tz )
        return ret

    def standard_output_object_none2false( self, model, possible_filed_set):
        """
            Checks the fields specified in possible_filed_set. If any field from this set
            after the model_to_pod() function call has value equal `None`, it will be converted ot False.
        """
        ret = {}
        js_obj = model_to_pod(model)
        js_obj = util.none_to_false(js_obj, possible_filed_set)
        ret[model.Meta.name] = js_obj
        return ret

    # THIS IS NOT HASHING it is ENCRYPTION
    # DEPRECATED, AND SHOULD USED ONLY FOR CALCULATE CERTS CHECKSUM
    # USE `psss_hash()` function
    def password_based_checksum( self, password, key ):
        from Crypto.Cipher import AES
        from Crypto.Util import randpool
        import base64

        block_size = 16
        key_size = 32
        mode = AES.MODE_ECB

        padding = lambda s: s + (key_size - len(s) % key_size) * "0"
        key_bytes = padding(key)

        pad = block_size - len(password) % block_size
        data = password + pad * chr(pad)

        encrypted_bytes = AES.new(key_bytes, mode).encrypt(data)
        encrypted_string = base64.urlsafe_b64encode(encrypted_bytes).decode()

        return encrypted_string


    # crypto parameters defined in config function/file
    def pass_hash(self, pass_text, salt, sitewide_salt_secret_key):
        final_salt = hashlib.sha256()
        final_salt.update((salt + sitewide_salt_secret_key).encode())

        crypto_params = util.erad_cfg().crypto_params
        dk = hashlib.pbkdf2_hmac(
            crypto_params.digest,
            pass_text.encode('utf-8'),
            final_salt.digest(),
            crypto_params.iterations
        )
        return binascii.hexlify(dk).decode('utf-8')


    def is_password_blacklisted(self, password):
        """
        :return: True means password are pwned, and using it may be is insecure
        :rtype: bool

        If API raise exception/return error or unavailable then return False, so user will be able to save password
        """
        _cfg = util.erad_cfg()

        pass_hash = sha1()
        pass_hash.update(password.encode())
        pass_hash = pass_hash.hexdigest().upper()

        first_5_symbols = pass_hash[:5]
        rest_symbols = pass_hash[5:]
        try:
            response = requests.get(
                _cfg.api.password_check + first_5_symbols
            )
        except Exception as err:
            print(err)
            return False

        if response.status_code != 200:
            return False

        pwned = False
        if response.status_code == 200:
            for line in response.iter_lines(decode_unicode=True):
                if rest_symbols in line:
                    rest_symbols, count = line.split(':')
                    if int(count) > _cfg.api.password_check_maximum_count_allowed:
                        pwned = True
                    break

        return pwned


    def get_older_matching_sessions(self, new_session):
        """Returns a list of sessions that are older than a day which have a common Identifier and Account_ID"""
        query_result = Erad_Session().Account_ID_Identifier.query(new_session.Account_ID,
                                                                  Account_ID_Identifier_index.Identifier==new_session.Identifier)
        older_sessions = []
        for session in query_result:
            if self._is_older_than(session, 0):
                older_sessions.append(session)
        return older_sessions

    def session_matches(self, new_session, old_session):
        """Returns True if two sessions have matching AccountOwner, Group_ID_List, LogoutUrl, Account_ID and Identifier and False otherwise."""
        new_session_group_id_set = set(new_session.Group_ID_List or [])
        old_session_group_id_set = set(old_session.Group_ID_List or [])

        if (new_session.AccountOwner == old_session.AccountOwner and
                new_session_group_id_set == old_session_group_id_set and
                new_session.LogoutUrl == old_session.LogoutUrl and
                new_session.Account_ID == old_session.Account_ID and
                new_session.Identifier == old_session.Identifier):
            return True
        return False

    def delete_all_older_sessions(self):
        """Deletes all sessions that are older than 90 days."""
        for session in Erad_Session.scan():
            if self._is_older_than(session, 90):
                session.delete

    def _is_older_than(self, session, days):
        """Returns whether a session is older than a certain number of days."""
        try:
            now = datetime.now(pytz.utc)
            age = now - session.LastUsed.replace(tzinfo=pytz.utc)
            return age.days > days
        except Exception:
            """This session lacks a LastUsed attribute."""
            return False

    def delete_older_sessions(self, new_session):
        """Deletes older sessions matching the new_session."""
        self.delete_all_older_sessions()
        older_matching_sessions = self.get_older_matching_sessions(new_session)
        for session in older_matching_sessions:
            if self.session_matches(session, new_session):
                session.delete()

    def set_optional_boolean_value(self, model, fields, field_name):
        """Will change boolean model field value only if new value present in fields.
        """
        if (fields is not None) and (field_name in fields):
            value = self.expect_parameter( fields, field_name )
            if type(value) is bool:
                setattr(model, field_name, value)
            elif type(value) is str:
                if value.lower() == "true":
                    value = True
                elif value.lower() == "false":
                    value = False
            else:
                raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected bool).".format(field_name))
            setattr(model, field_name, value )

    def set_optional_integer_value(self, model, fields, field_name):
        """Will change boolean integer field value only if new value present in fields.
        """
        if (fields is not None) and (field_name in fields):
            try:
                value = int(fields[field_name])
                setattr(model, field_name, value )
            except ValueError:
                raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected number).".format(field_name))

    def set_optional_integer_value_none(self, model, fields, field_name):
        """Will change integer field value only if new value present in fields, can accept None.
        It is intended for fields that can be null in DynamoDB.
        """
        if (fields is not None) and (field_name in fields):
            try:
                _value = fields[field_name]
                if (_value is None):
                    value = None
                elif type(_value) in {str, str}:
                    if len(_value.strip()) == 0:
                        value = None
                    else:
                        value = int(_value)
                else:
                    value = int(_value)


                setattr(model, field_name, value)
            except ValueError:
                raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected number).".format(field_name))

    def expect_optional_boolean_parameter(self, fields, field_name):
        """Return field value. If field name not in fields return None.
        """
        if (not fields is None) and (field_name in fields):
            value = fields[field_name].lower()

            if value == 'true':
                value = True
            elif value == 'false':
                value = False
            else:
                raise EradWrongTypeException( "Parameter '{0}' is incorrect type (expected bool).".format(field_name))

            return value

    def get_unique_devices_count(self, account_id, group_id, period, rate_limit=None) -> int:
        """Returns a number of unique devices for `period`"""

        delta = timedelta(hours=24)

        if isinstance(period, timedelta):
            delta = period
            period = None

        if period is None:
            ts_to = datetime.now(pytz.utc)
            ts_from = ts_to - delta
            period = (util.get_log_instance_time(ts_from), util.get_log_instance_time(ts_to))

        # Requests from test/internal utils may not send CallingStagionID
        # and log record will not have information about it
        # do not accounting such records as instructed from conversation in Slack 2020-03-13

        query = Erad_Radius_Log().AccountGroupId_index.query(
            account_id + "::" + group_id,
            AccountGroupIdIndex2.Instance.between(*period),
            filter_condition=(Erad_Radius_Log.Access == 'Access-Accept'),  # skipping rejected radius request
            rate_limit=rate_limit
        )

        uniq = set()
        for x in query:
            logger.debug(f'{x}, {x.EventTimestamp} {x.Access}')
            uniq.add(x.CallingStationId)

        return len(uniq)

