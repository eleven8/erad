
## Passpoint Profile
This doc elaborates on profile downloading, various device type endpoints, expected payload format and fields.

## Android
Endpoint: `/erad/public/android/profile/create`
Payload
```
{
    "cacert": "MID=======",
    "username": "coder",
    "password": "rlhc123",
    "domain": "rlh.guestinternet.com",
    "is_android_gte_11": true,
    "nairealm": "rlh.guestinternet.com",
    "rcoi": ["111111"],
    "friendly_name": "RLHC service 2"
}
```
Profile template is of XML format. API payload fields are injected in profile template and template is encoded in base64. 
Encoded profile template is uploaded to s3 and link sent to user for download.

### Payload Fields
- cacert
    This field expects a CA certificate to include in passpoint profile.
    It's used for scenarios where android version < 11. Android  checks that certificated presented by AAA server 
  issues with this CA cert.
- trusted_AAA_server_name Android under the hood using wpa_supplicant program, this field define domain_suffix_match parameter for wpa_supplicant. Android <= 10 uses inexplictly FQDN value that defined by nairealm field. Certificate's CN or SANs presented by AAA server should match exactly this parameter.
- username
    This field is required for Authentication.
- password
    This field is required for Authentication.
- domain
    If no domain is passed, default is `enterpriseauth.com`. This field can be considred deprecated - legacy from the previous versions of the API. Previously it defined nairealm that Android should try to connect.
- nairealm
  Used by `Realm` and `FQDN` fields in profile template. If value is not provided then code will use value of the `domain` field. Define nairealm that Android will try to connect.
- rcoi
  This field expects a list of OIs. RCOI value is 3 or 5 byes long ("aabbcc" or "aabbccddeeff"). Android will try to use it to connect if AP will braoadcast it. Used by `RoamingConsortiumOI` field in profile template.
- is_android_gte_11
  Selects different profile templates if android version is >= 11 or vice-versa. Android <= 10 uses `cacert` instead of  `trusted_AAA_server_name`
- friendly_name
    Name of the profile, used by `FriendlyName` field in profile template. Profiles with the same name will be overwritten.


## Android (TLS)
Endpoint: `/erad/public/android/profile/create/tls`
Payload
```
{
    "cacert": "MIIDrzCCApegAwIBAgIQCDv0KPMbp1ZWVbd4=",
    "domain": "rlh.guestinternet.com",
    "is_android_gte_11": true,
    "nairealm": "rlh.guestinternet.com",
    "rcoi": ["890sd", "90oso0d"],
    "friendly_name": "RLHC service 2",
    "Sha256-fingerprint":  "D6:9F:0C:17:4B:FA:3F:78:9E:04:5A:1D:62:C6:AC:7C:E6:65:55:C9:4A:B2:F4:58:09:96:D1:2A:58:75:6B:77",
    "trusted_AAA_server_name": "enterpriseauth.com",
    "clientCert": "MIIDrzCCApegAwIBAgIQCDv0KPMbp1ZWVbd4=="
}

```

API payload fields are injected in profile template and template is encoded in base64.
A Client Certificate is included in final template, uploaded to s3 and link sent to user for download.

### Payload Fields
Includes all fields from Regular Android payload except `username` and `password` fields.
There are few additions for Android (TLS).

- clientCert
    Username/Password fields is not used, instead user passes a client certificate which is added to final template after encoding profile template in base64.
- Sha256-fingerprint
    Used by `CertSHA256Fingerprint` field in profile template.

## Apple
Endpoint: `/erad/public/apple/profile/create`
Payload
```
{
    "cacert": "MIIDrzCCApegAwI9C79Fv1C5qfPrmAESrciIxpg0X40KPMbp1ZWVbd4=",
    "srv_cert_cn_or_cn_wildcard": "sdsfds",
    "profileName": "rlhc12",
    "expDate": "",
    "ssid": "WIFI2",
    "username": "coder",
    "password": "rlhc123",
    "domain": "rlh.guestinternet.com",
    "isPasspoint": true,
    "nairealm": "rlh.guestinternet.com",
    "rcoi": ["890sd", "90oso0d"],
    "display_name": "RLHC service network",
    "friendly_name": "RLHC service 2",
    "account": ""
}
```

### Payload Fields
- cacert
    In older versions of iOS or profiles for iOS this field is used to default CA cert that will be used to verify AAA server certificate. Deprecated and field `srv_cert_cn_or_cn_wildcard` should be used instead.
- srv_cert_cn_or_cn_wildcard
    Maps to `TLSTrustedServerNames` field in profile template. iOS uses it to check that certificate(CN or SANs) presented by AAA server matches this value. Can be wildcarded. For more information check https://developer.apple.com/business/documentation/Configuration-Profile-Reference.pdf
- profileName 
    Deprecated, is used as a fallback if value for friendly_name not provided. 
- expDate
    If an expiry date is passed, template is updated with the value. iOS allows to configure when profiles will expired
- ssid
    Maps to `SSID_STR` and used in non passpoint-based templates.
    It is required if `isPasspoint` is false.
- username
- password
- domain
    If no domain is passed, default is `enterpriseauth.com`. Used to configue anonymous identity for radius request ie anonymous@<domain> In other places is used only as a fallback value if other values was not provided.
- isPasspoint
    Controls template to be used for profile. If `true`, use passpoint-based template.
- nairealm
    Expects a string containing realm names to be added in profile. If not provided value "enterpriseauth.com" is used. If AP broadcast the same nairealm value iOS will try to connect.
- rcoi
    This field expects a list of OIs. Used by `RoamingConsortiumOIs` field in profile template. If AP broadcast the same nairealm value iOS will try to connect. RCOI value is 3 or 5 byes long ("aabbcc" or "aabbccddeeff"). 
- friendly_name
    Name of profile, used by `DisplayedOperatorName` `Payload Org`, `Cert payload display name`, fields in profile template.  Will be visible for users (branding).
- display_name
    Value for `Top level payload name` field.
- account
    This field is used to get signing credentials depending on customer.

Profile template is of XML format. 
API payload fields are injected in profile template and template is signed by `openssl smime` command.
Signed profile is uploaded to s3 and response sent back to user containing link to download profile.

Depending on profile type, the following templates can be used.
- Passpoint
- Passpoint Deprecated
- Not passpoint
- Not passpoint Deprecated

These templates depend on the `isPasspoint` field.

If `isPasspoint` returns `true`, check the `srv_cert_cn_or_cn_wildcard` field for a value
    - If `srv_cert_cn_or_cn_wildcard` present, use passpoint-based template.
    - If `cacert` present (there is a CA certificate), use deprecated passpoint-based template.

If `isPasspoint` returns `false`, check the `srv_cert_cn_or_cn_wildcard` field for a value
    - If `srv_cert_cn_or_cn_wildcard` present, use non-passpoint based template.
    - If `cacert` present (there is a CA certificate), use non-passpoint based deprecated template.

Resulting template is updated with Expiry date if any exists via `expDate` field.
We sign profile, upload to s3 and send profile download link back to user.

## Apple (TLS)
Endpoint: `/erad/public/apple/profile/create/tls`
Payload
```
{
    "cacert": "MIIDrzCCApegAwI9C79Fv1C5qfPrmAESrciIxpg0X40KPMbp1ZWVbd4=",
    "srv_cert_cn_or_cn_wildcard": "sdsfds",
    "profileName": "rlhc12",
    "expDate": "",
    "ssid": "WIFI2",
    "domain": "rlh.guestinternet.com",
    "is_not_passpoint": true,
    "nairealm": "rlh.guestinternet.com",
    "rcoi": ["890sd", "90oso0d"],
    "display_name": "RLHC service network",
    "friendly_name": "RLHC service 2",
    "account": "",
    "clientCert": "MIIDrzCCApegAwI9C79Fv1C5qfPrmAESrciIxpg0X40KPMbp1ZWVbd4=="
}
```

### Payload Fields
Includes all fields from Regular Apple payload except `username` and `password` fields.
New field `clientCert` added.

- clientCert
    Expects a client certificate which is added to profile template for authentication.

## Windows
Endpoint: `/erad/public/windows/profile/create`
Payload
```
{
    "profileName": "rlhc12",
    "ssid": "RLHC WIFI1",
    "username": "coder",
    "password": "rlhc123",
    "domain": "rlh.guestinternet.com",
    "is_passpoint": true,
    "return_file_content": false,
    "nairealm": "rlh.guestinternet.com",
    "rcoi": ["890sd", "90oso0d"],
    "display_name": "RLHC service network",
    "friendly_name": "RLHC service 2",
    "srv_cert_cns": "enterpriseauth.com;www.enterpriseauth.com",
    "root_ca_hash": "91 c6 d6 ee 3e 8a c8 63 84 e5 48 c2 99 29 5c 75 6c 81 7b 81",
    "account": ""
}
```
### Payload Fields
- srv_cert_cns
    Maps to `ServerNames` field in profile template. Windows uses this field to check that certicate presnted by AAA servers match this value. Windows ignores SANs values in certifcate and uses only CN from it. Also Windows require FQDN in CN i.e. certs with wildcard value in CN will be rejected. Can be more than one value, in this case seprated by comma without spaces.
- root_ca_hash
    Maps to `TrustedRootCAHash` field in profile template. SHA1 fingreprint of root CA cert that was used to issue AAA server certificate. (Root means self-signed top-level CA cert)
- profileName
    Name of profile. Deprecated not used was replaced by friendly_name paramter.
- ssid
    Maps to `SSID` node in template. Usually if profile is passpoint, this field is does not matter, Windows will use nairealm to select appropriate AP. But value for this field has limitation in 32 characters (SSID standard)
- username
- password
- domain
    If no domain is passed, default is `enterpriseauth.com`. This value is uses as a fallback for display_name and friendly_name fields.
- is_passpoint
    If `true`, passpoint section is added to profile template.
- nairealm
    Expects a string containing realm names to be added in profile.
    Maps to `NAIRealm` field in passpoint block.
- rcoi (`Field not required`)
    Used by `RoamingConsortiumOIs` field in profile template. If AP broadcast the same nairealm value Windows will try to connect.
- friendly_name
    Name of profile. Will be used by Windows to dispaly profile. Also profiles with the same name will overwrite each other.
- display_name (`Field not required`)
    deprecated will be removed
- account
    This field is used to get signing credentials depending on customer. Windows require to have signed profiles.
- return_file_content
    If `true`, content of signed profile is returned, else link to download profile is returned after upload to s3. Windows 10, before version 2004 (Windows 7 and 8 too), was able to install profiels only with private Windows Javascript API window.external.msProvisionNetworks https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/dn529170(v=vs.85) This API exped full xml file to be passed in function. 
    Now Windows 10 (11) >= 2004 is capable to use new API https://docs.microsoft.com/en-us/windows/win32/nativewifi/prov-wifi-profile-via-website

Profile template is of XML format. API payload fields are injected in profile template.
We get signing credentials for windows, sign profile, upload to s3 and send response with profile download link back to user.
Windows require special values of namespaces in signed xml file, because of that we using fork of this library https://github.com/XML-Security/signxml Fork is here https://bitbucket.org/teamfortytwo/signxml/get/master.zip. Issue described here https://github.com/XML-Security/signxml/issues/43#issuecomment-362235181

