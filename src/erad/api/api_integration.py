#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad.api import *
from .interface_integration import EradInterfaceIntegration
from .interface_admin import EradInterfaceAdmin

#---------- Integration Services ----------

# http://localhost:5000/erad/integration/session/save
@app.route('/erad/integration/session/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def integration_session_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceIntegration().session_save( request.json ) )

# http://localhost:5000/erad/integration/group/load
@app.route('/erad/integration/group/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def integration_group_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceIntegration().group_load( request.json ) )

# http://localhost:5000/erad/integration/group/save
@app.route('/erad/integration/group/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def integration_group_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceIntegration().group_save( request.json ) )

# http://localhost:5000/erad/integration/group/delete
@app.route('/erad/integration/group/delete', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def integration_group_delete():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().group_delete( request.json ) )

# http://localhost:5000/erad/integration/authenticator/load
@app.route('/erad/integration/authenticator/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def integration_authenticator_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().authenticator_load( request.json ) )

# http://localhost:5000/erad/integration/authenticator/save
@app.route('/erad/integration/authenticator/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def integration_authenticator_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().authenticator_save( request.json ) )

# http://localhost:5000/erad/integration/authenticator/delete
@app.route('/erad/integration/authenticator/delete', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def integration_authenticator_delete():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().authenticator_delete( request.json ) )

# http://localhost:5000/erad/integration/supplicant/load
@app.route('/erad/integration/supplicant/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def integration_supplicant_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().supplicant_load( request.json ) )

# http://localhost:5000/erad/integration/supplicant/save
@app.route('/erad/integration/supplicant/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def integration_supplicant_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().supplicant_save( request.json ) )


# http://localhost:5000/erad/integration//supplicant/certificate
@app.route('/erad/integration/supplicant/certificate', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def integration_supplicant_certificate():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceAdmin().supplicant_certificate( request.json ) )
