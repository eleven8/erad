import json

parameters = {}
definitions = {}
paths = {}
responses = {}
requiredParams = {}
reqMap = {}

def getType(obj):
    if type(obj) is str:
        return 'string'
    elif type(obj) is str:
        return 'string'
    elif type(obj) is int:
        return 'integer'
    elif type(obj) is bool:
        return 'boolean'
    elif type(obj) is dict:
        return 'object'
    elif type(obj) is list:
        return 'array'
    else:
        return 'null'

def sanitize(obj):
    paramType = getType(obj)
    if paramType == 'object':
        for l in obj:
            if getType(obj[l]) in ['string', 'null']:
                obj[l] = 'string'
            else:
                sanitize(obj[l])
    if paramType == 'array':
        for i in range(len(obj)):
            if getType(obj[i]) in ['string', 'null']:
                obj[i] = 'string'
            else:
                sanitize(obj[i])

def getRequired(callName, reqs):
    accepted = [json.loads(jsonReq) for jsonReq in reqs if 200 in reqs[jsonReq]]
    required = {}
    for req in accepted:
        for param in req:
            if param not in required:
                if len(list(required.keys())) == 0:
                    required[param] = True
                else:
                    required[param] = False
    requiredParams[callName] = [i for i in required if required[i]]

def getDef(callName, req):
    props = {}
    sanitize(req)
    for k in req:
        paramType = getType(req[k])
        props[k] = {
                'type': paramType,
                }
        props[k]['example'] = req[k]
    definitions[callName] = {
            'type': 'object',
            'properties': props
            }
    if len(requiredParams[callName]) != 0:
        definitions[callName]['required'] = requiredParams[callName]

def getParam(callName, method, req):
    params = []
    if method == 'get':
        for k in req:
            callName = callName + '_' + k.lower()
            params.append(callName)
            paramType = getType(req[k])
            parameters[callName] = {
                    'in': 'query',
                    'name': k,
                    'type': paramType,
                    'required': True, # determine dynamically
                    }
    else:
        params.append(callName)
        parameters[callName] = {
                'in': 'body',
                'name': callName,
                'required': True, # determine dynamically
                'schema': {
                    '$ref': '#/definitions/' + callName
                    }
                }
        getDef(callName, req)
    return params

def respToModel(respData):
    model = {
            'properties': {}
            }
    for i in respData:
        dataType = getType(respData[i])
        if dataType == 'array':
            model['properties'][i] = {
                    'type': 'array',
                    }
            if len(respData[i]) > 0:
                model['properties'][i]['items'] = {
                        'type': getType(respData[i][0])
                        }
            else:
                model['properties'][i]['items'] = {
                        'type': 'string'
                        }

        elif dataType == 'object':
            model['properties'][i] = respToModel(respData[i])
        else:
            model['properties'][i] = {
                    'type': getType(respData[i])
                    }
    return model

def getResp(callName, resp):
    if callName not in responses:
        responses[callName] = {}
    for status_code in resp:
        desc = 'HTTP Error'
        if status_code == 200:
            desc = 'Successful'
        elif status_code == 400:
            desc = 'Bad Request'
        elif status_code == 403:
            desc = 'Access Denied'
        elif status_code == 404:
            desc = 'Not Found'
        responses[callName][str(status_code)] = {
                'description': desc,
                'schema': respToModel(resp[status_code])
                }

def getPath():
    for path in reqMap:
        paths[path] = {}
        for method in reqMap[path]:
            params = []
            callName = path[6:].replace('/', '_') + '_' + method
            getRequired(callName, reqMap[path][method])
            for jsonReq in reqMap[path][method]:
                params += getParam(callName, method, json.loads(jsonReq))
                getResp(callName, reqMap[path][method][jsonReq])
            params = [{'$ref': '#/parameters/' + name} for name in set(params)]
            paths[path][method] = {
                    'parameters': params,
                    'responses': responses[callName],
                    'tags': [path.split('/')[2]]
                    }


def yamlify(obj):
    if type(obj) is dict:
        return '$ref: ' + '"' + obj['$ref'] + '"'
    if type(obj) is bool:
        if obj:
            return 'true'
        else:
            return 'false'
    if obj == 'null':
        return '[string, "null"]'
    return str(obj)


def objToYaml(obj, level=0):
    for key in sorted(obj.keys()):
        if type(obj[key]) is dict:
            if len(list(obj[key].keys())) == 0:
                print('  ' * level + key + ': {}')
            else:
                print('  ' * level + key + ':')
                objToYaml(obj[key], level+1)
        elif type(obj[key]) is list:
            print('  ' * level + key + ':')
            for i in obj[key]:
                print('  ' * (level+1) + '- ' + yamlify(i))
        else:
            if key == '$ref':
                print('  ' * level + yamlify(obj))
            else:
                print('  ' * level + key + ': ' + yamlify(obj[key]))

def generateYamlDefs():
    print('definitions:')
    objToYaml(definitions, 1)

def generateYamlParams():
    print('parameters:')
    objToYaml(parameters, 1)

def generateYaml():
    print('''swagger: '2.0'
info:
  title: "ERAD API - Beta version of documentation - not all endpoints are up-to-date."
  version: "0.1"
consumes:
  - application/json
produces:
  - application/json
''')
    print('paths:')
    objToYaml(paths, 1)
    generateYamlParams()
    generateYamlDefs()

def generateJson():
    resultObj = {
            'swagger': '2.0',
            'info': {
                'title': 'ERAD API - Beta version of documentation - not all endpoints are up-to-date.',
                'version': '0.1'
                },
                'consumes': [
                        'application/json'
                ],
                'produces': [
                        'application/json'
                ],
                'paths': paths,
                'parameters': parameters,
                'definitions': definitions,
            }
    print(json.dumps(resultObj, indent=4, separators=(',', ': '), sort_keys=True))

def addToSpy(path, method, req, status_code, resp):
    jsonReq = json.dumps(req)
    if path not in reqMap:
        reqMap[path] = {}
    if method.lower() not in reqMap[path]:
        reqMap[path][method.lower()] = {}
    if jsonReq not in reqMap[path][method.lower()]:
        reqMap[path][method.lower()][jsonReq] = {}
    reqMap[path][method.lower()][jsonReq][status_code] = resp
    getPath()

