from datetime import timedelta

CELERYBEAT_SCHEDULE = {
    'every-minute': {
        'task': 'tasks.send_call_duration',
        'schedule': timedelta(seconds=60)
    },
}

BROKER_URL = 'redis://localhost:6379/0'

CELERY_TIMEZONE = 'UTC'
