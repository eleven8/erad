#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import inspect
from pynamodb.models import Model
from pynamodb.exceptions import DoesNotExist
from pynamodb.indexes import *
from pynamodb.attributes import (
        Attribute,
        NumberAttribute,
        UnicodeAttribute,
        LegacyBooleanAttribute,
        UTCDateTimeAttribute,
        UnicodeSetAttribute,
        NumberSetAttribute,
        BinarySetAttribute
        #BinaryAttribute,
    )

from .. import util

import warnings
warnings.filterwarnings( "ignore", message="(.+)RFC 2818(.+)" )


# erad_table_list:
#  return the list of all the tables in the Erad database.
def erad_table_list():
    return [
        Erad_Group2,
        Erad_ApiKey,
        Erad_AccountOwner,
        Erad_Authenticator2,
        Erad_Supplicant,
        Erad_Session,
        Erad_Audit,
        Erad_AccountingLogs,
        Erad_Radius_Log,
        Erad_Cluster,
        Erad_Endpoint,
        Erad_SupplicantCertificate
    ]


# Erad ApiKey table
class AccountIdIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'Account_ID-index'
        read_capacity_units = 3
        write_capacity_units = 2
        projection = AllProjection()

    Account_ID = UnicodeAttribute(null=False, hash_key=True)


class ApiKeyNameIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'ApiKeyName-index'
        read_capacity_units = 3
        write_capacity_units = 2
        projection = AllProjection()

    ApiKeyName = UnicodeAttribute(null=False, hash_key=True)


class Erad_ApiKey(Model):
    class Meta:
        table_name = "Erad_ApiKey_Global"
        name = "ApiKey"
        read_capacity_units=3
        write_capacity_units=2
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    # Api Access Key
    ApiKey_ID = UnicodeAttribute(hash_key=True)

    # Account_ID
    account_index = AccountIdIndex()
    Account_ID = UnicodeAttribute(null=False)

    # ApiKeyName
    apiKeyName_index = ApiKeyNameIndex()
    ApiKeyName = UnicodeAttribute(null=False)

    # Active
    Active = NumberAttribute(null=False)

    # Name: A human readable display name for the ApiKey.
    #  Uniqueness is not enforced.
    DisplayName = UnicodeAttribute(null=True, default="KeyName")


# Account Owner table
class OwnerAccountIdIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'OwnerAccount_ID-index'
        read_capacity_units = 3
        write_capacity_units = 2
        projection = AllProjection()

    Account_ID = UnicodeAttribute(null=False, hash_key=True)


class Erad_AccountOwner(Model):
    class Meta:
        table_name = "Erad_AccountOwner_Global"
        name = "AccountOwner"
        read_capacity_units=3
        write_capacity_units=2
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    # Account email
    Email = UnicodeAttribute(hash_key=True)

    # Password hash, PBKDF2
    Password = UnicodeAttribute(null=False)

    # Password Salt
    # SHALL be at least 32 bits in length by NIST guildelines
    # https://pages.nist.gov/800-63-3/sp800-63b.html#memsecretver
    Salt = UnicodeAttribute(null=False)

    # Account_ID
    account_index = OwnerAccountIdIndex()
    Account_ID  = UnicodeAttribute(null=False)


# An ElevenRadius Group2 in DynamoDB
class Erad_Group2(Model):
    class Meta:
        table_name = "Erad_Group_Global"
        name = "Group"
        read_capacity_units=3
        write_capacity_units=2
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    # Account_ID: a unique identifier for the group
    Account_ID = UnicodeAttribute(hash_key=True)

    #ID (range, string)
    ID = UnicodeAttribute(range_key=True)

    # Domain: a "subnet-esque" partition for clients
    Domain = UnicodeAttribute(null=True)

    # Name: A human readable display name for the group.
    #  Uniqueness is not enforced.
    Name = UnicodeAttribute(null=True)

    # RemoteServerUrl: Represents a Url to forward authentication
    #  requests to if the Supplicant.UseRemote is true.
    RemoteServerUrl = UnicodeAttribute(null=True)

    # SharedSecret:
    SharedSecret = UnicodeAttribute(null=True)

    # TimeZone: Olson ID for the sites
    TimeZone = UnicodeAttribute(null=True)

    # Max download speed
    MaxDownloadSpeedBits = NumberAttribute(null=True)

    # Max upload speed
    MaxUploadSpeedBits = NumberAttribute(null=True)

    # If ture, supplicant should receive vlan settings from home server
    OverrideVlan = LegacyBooleanAttribute(null=True)

    # If true, supplicant shoud receive connection speed settings from home server
    OverrideConnectionSpeed = LegacyBooleanAttribute(null=True)

    # ClientInfoArn: Stores mapping of regions to client-lambda arns
    ClientInfoArn = UnicodeAttribute(null=True)


# An ElevenRadius Authenticator2 in DynamoDB
class AccountGroupIDIndex2(GlobalSecondaryIndex):
    class Meta:
        index_name = 'Account_ID-Group_ID-index'
        read_capacity_units = 3
        write_capacity_units = 2
        projection = AllProjection()

    Account_ID = UnicodeAttribute(hash_key=True)
    Group_ID = UnicodeAttribute(range_key=True)


class Erad_Authenticator2(Model):
    class Meta:
        table_name = "Erad_Authenticator_Global"
        name = "Authenticator"
        read_capacity_units=3
        write_capacity_units=2
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    # ID: A unique identifier for Authenticator. Probably CalledStationId.
    ID = UnicodeAttribute(hash_key=True)

    account_group_index = AccountGroupIDIndex2()

    # Account_ID (range, string)
    Account_ID = UnicodeAttribute(range_key=True)

    # Group_ID: The ID of the Group that manages access for this Authenticator.
    Group_ID = UnicodeAttribute(null=True)

    # RadiusAttribute: Typical values expected for this field include called-station-id, nas-identifier, and nas-ip-address.
    RadiusAttribute = UnicodeAttribute(default="called-station-id")


class Erad_Cluster(Model):
    class Meta:
        table_name = "Erad_Cluster_Global"
        name = "Cluster"
        read_capacity_units = 3
        write_capacity_units = 2
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    ClusterId = UnicodeAttribute(hash_key=True)
    AuthNum = NumberAttribute(null=False)
    AcctNum = NumberAttribute(null=False)
    Instances = UnicodeAttribute(null=False)


# An ElevenRadius Endpoint in DynamoDB
class EndpointGroupIDIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'Group_ID-index'
        read_capacity_units = 3
        write_capacity_units = 2
        projection = AllProjection()

    Group_ID = UnicodeAttribute(hash_key=True)


class EndpointClusterIdIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'ClusterId-index'
        read_capacity_units = 3
        write_capacity_units = 2
        projection = AllProjection()

    ClusterId = UnicodeAttribute(hash_key=True)


class Erad_Endpoint(Model):
    class Meta:
        table_name = "Erad_Endpoint_Global"
        name = "Endpoint"
        read_capacity_units=3
        write_capacity_units=2
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    # IpAddress: A unique identifier - IP.
    IpAddress = UnicodeAttribute(hash_key=True)

    # Port: The port for connection.
    Port = UnicodeAttribute(range_key=True)

    # Group_ID: The site this endpoint is assigned to, or none.
    group_id_index = EndpointGroupIDIndex()
    Group_ID = UnicodeAttribute(null=True)

    # ClusterId: The radius cluster this endpoint will be hosted by.
    cluster_id_index = EndpointClusterIdIndex()
    ClusterId = UnicodeAttribute(null=False)

    # Secret: The radius shared secret. Unique to each endpoint.
    Secret = UnicodeAttribute(null=False)

    # Region: The physical location of the endpoint.
    Region = UnicodeAttribute(null=False)


class SupplicantParentIdIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'Parent_ID-index'
        read_capacity_units = 3
        write_capacity_units = 1
        projection = AllProjection()

    Parent_ID = UnicodeAttribute(null=False, hash_key=True)
    Account_ID = UnicodeAttribute(null=False, range_key=True)



# An ElevenRadius Supplicant in DynamoDB
class Erad_Supplicant(Model):
    class Meta:
        table_name = "Erad_Supplicant_Global"
        name = "Supplicant"
        read_capacity_units=3
        write_capacity_units=1
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    # Group_ID: A unique identifier for Authenticator.
    #  Probably equivalent to CalledStationId.
    Group_ID = UnicodeAttribute(hash_key=True)

    # Username: A specific Username, or a Wildcarded Username,
    #  or a Mac Address.
    Username = UnicodeAttribute(range_key=True)

    # Account_ID: User's Account ID
    Account_ID = UnicodeAttribute(null=True)

    # Password: A User's Password, or a Mac Address
    Password = UnicodeAttribute(null=True)

    # Parent_ID: An optional parent id to group supplicants
    Parent_ID_index = SupplicantParentIdIndex()
    Parent_ID = UnicodeAttribute(null=True)

    # DeviceName: An optional device name
    DeviceName = UnicodeAttribute(null=True)

    # Description: An optional description
    Description = UnicodeAttribute(null=True)

    # Location: An optional physical location of the device or access
    Location = UnicodeAttribute(null=True)

    # ExpirationDate: a UtcStamp which specifies when this supplicant's
    #  access is expired.
    ExpirationDate = UTCDateTimeAttribute(null=True)

    # Vlan: A number representing the Vlan the supplicant should be sent to
    #  on Access-Accept.
    Vlan = NumberAttribute(null=True)

    # UseRemote: A Boolean specifying if Group.RemoteServerUrl should be used
    #  as a proxy for Radius Authentication.
    UseRemote = LegacyBooleanAttribute(null=True)

    # UseWildcard: A Boolean specifying if Username is a Wildcarded Username.
    UseWildcard = LegacyBooleanAttribute(null=True)

    # CustomJsonData: An optional, stores whatever text the user sends to it.
    CustomJsonData = UnicodeAttribute(null=True)

    # Max download speed
    MaxDownloadSpeedBits = NumberAttribute(null=True)

    # Max upload speed
    MaxUploadSpeedBits = NumberAttribute(null=True)

    # Timestamp for Tracking when Profile was first used.
    profile_used = NumberAttribute(null=True)

    # Timestamp for Tracking when Device was first used.
    device_used = NumberAttribute(null=True)


# This function specifies which of the above fields are allowed for
#  import/export of supplicants.
#  Group_ID and UseWildcard are omitted.
def supplicant_import_export_fields():
    return [
        "Username",
        "Password",
        "Vlan",
        "UseRemote",
        "DeviceName",
        "Description",
        "Location",
        "ExpirationDate",
        "CustomJsonData",
        "MaxDownloadSpeedBits",
        "MaxUploadSpeedBits",
        "Parent_ID",
        "Account_ID",
        "profile_used",
        "device_used"
    ]


class Account_ID_Identifier_index(GlobalSecondaryIndex):
    class Meta:
        index_name = 'Account_ID-Identifier-index'
        read_capacity_units = 1
        write_capacity_units = 1
        projection = AllProjection()

    Account_ID = UnicodeAttribute(hash_key=True)
    Identifier = UnicodeAttribute(range_key=True)


# An ElevenRadius Session in DynamoDB
class Erad_Session(Model):
    class Meta:
        table_name = "Erad_Session_Global"
        name = "Session"
        read_capacity_units=1
        write_capacity_units=1
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    # ID: A string GUID which uniquely identifies the session.
    ID = UnicodeAttribute(hash_key=True)

    # Group_ID_List: A list of Group_IDs that this Session has access to read/write.
    Group_ID_List = UnicodeSetAttribute(null=True)

    Account_ID_Identifier = Account_ID_Identifier_index()

    # Account_ID (string)
    Account_ID = UnicodeAttribute(null=True)

    # AccountOwner (boolean)
    AccountOwner = NumberAttribute(default=0)

    # Identifier: From an external system. Specifies who is using the session in some way.
    Identifier = UnicodeAttribute(null=True)

    # LogoutUrl: From an external system. Specifies where the user should land on session expiration.
    LogoutUrl = UnicodeAttribute(null=True)

    # LastUsed: a UtcStamp which specifies when this session was last used by the user.
    #   It may be used to expire the session if the LastUsed time is too old.
    LastUsed = UTCDateTimeAttribute(null=True)


# An ElevenRadius Audit in DynamoDB
class AccountGroupIdIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'AccountGroup_ID-Instance-index'
        read_capacity_units = 3
        write_capacity_units = 2
        projection = AllProjection()

    AccountGroup_ID = UnicodeAttribute(null=False, hash_key=True)
    Instance = NumberAttribute(null=False, range_key=True)


class Erad_Audit(Model):
    class Meta:
        table_name = "Erad_Audit_Global"
        name = "Audit"
        read_capacity_units=1
        write_capacity_units=2
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    # Group_ID: Audits are partitioned into the related Group_ID
    Group_ID = UnicodeAttribute(hash_key=True)

    # Instance: Individual Audits are located by their Instance number.
    #  - Contains 2 values: Number of UTC milliseconds since the epoch and a 5 digit random identifier.
    #  - Use the Auditor.instance_pack, and Auditor.instance_unpack functions to read/write
    Instance = NumberAttribute(range_key=True)

    # Identifier: An ElevenOS Username or other way to identify the user
    Identifier = UnicodeAttribute(null=True)

    # Message: A message describing what occurred.
    Message = UnicodeAttribute(null=True)

    # Date: a UtcStamp which specifies when this action occurred
    Date = UTCDateTimeAttribute(null=True)

    # AccountGroup_ID: A String Field to hold AccountGroupID.
    accountgroupid_index = AccountGroupIdIndex()
    AccountGroup_ID = UnicodeAttribute(null=True)


class Erad_AccountingLogsIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'Erad_AccountingLogs_Global-gsi-account_id__instance'
        read_capacity_units = 1
        write_capacity_units = 1
        projection = AllProjection()
    Account_ID = UnicodeAttribute(hash_key=True)
    Instance = NumberAttribute(range_key=True)


# Accounting Logs table
class Erad_AccountingLogs(Model):
    class Meta:
        table_name = "Erad_AccountingLogs_Global"
        name = "AccountingLogs"
        read_capacity_units=3
        write_capacity_units=2
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    AccountInstance_Index = Erad_AccountingLogsIndex()

    # Account ID
    Account_ID = UnicodeAttribute(hash_key=True)

    # Raw logs
    Raw_Log = UnicodeAttribute(null=False, range_key=True)
    Log_Content = UnicodeAttribute(null=True)

    # Instance: Individual Logs are located by their Instance number.
    #  - Contains 2 values: Number of UTC seconds since the epoch and a 4 digit random identifier.
    #  or 10^-4 decimilliseconds
    #  - Use the erad.util.instance_unpack, and erad.util.instance_pack functions to read/write

    # Allowing null because there are old records that does have this field
    Instance = NumberAttribute(null=True)

    # Group_ID
    Group_ID = UnicodeAttribute(null=False)

    # Group_Name
    Group_Name = UnicodeAttribute(null=True)

    # Endpoint Ip Address
    EndpointIPAddress = UnicodeAttribute(null=True)

    # Endpoint Port
    EndpointPort = UnicodeAttribute(null=True)


# All of the above dynamo Models are not JSON serializable natively.
#  Use model_to_pod() to convert to a 'Plain Old Data' representation
#  which is ready for JSON/XML/CSV serialization.
def model_to_pod( model, str_day_tz=None ):
    ret = {}
    for k,v in inspect.getmembers(type(model), lambda a:not(inspect.isroutine(a))):
        if type(v) in [BinarySetAttribute, NumberSetAttribute]:
            ret[k] = []
            for val in getattr( model, k ):
                ret[k].append( val )
        elif type(v) is UnicodeSetAttribute:
            ret[k] = []
            for val in getattr( model, k ):
                ret[k].append( None if val is None else val )
        elif type(v) is UnicodeAttribute:
            val = getattr( model, k )
            ret[k] = None if val is None else val
        elif issubclass(type(v), UTCDateTimeAttribute):
            val = getattr( model, k )
            if val is None:
                ret[k] = None
            elif str_day_tz is None:
                ret[k] = util.utc_isoformat( val )
            else:
                # if str_day_tz is passed, the caller wants day formatted datetimes in a timezone
                ret[k] = util.to_human_date_str( util.utc_to_local(val, str_day_tz) )
        elif issubclass(type(v), Attribute):
            ret[k] = getattr( model, k )
    return ret


# A log in DynamoDB
class AccountGroupIdIndex2(GlobalSecondaryIndex):
    class Meta:
        index_name = 'AccountGroup_ID-Instance-index-2'
        read_capacity_units = 3
        write_capacity_units = 2
        projection = AllProjection()

    AccountGroup_ID = UnicodeAttribute(null=False, hash_key=True)
    Instance = NumberAttribute(null=False, range_key=True)





class Erad_Radius_Log(Model):
    class Meta:
        table_name = "Erad_RadiusLog_Global"
        name = "Radius_Log"
        read_capacity_units=3
        write_capacity_units=2
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    # Group_ID: Audits are partitioned into the related Group_ID
    Group_ID = UnicodeAttribute(hash_key=True)

    # Instance: Individual Audits are located by their Instance number.
    #  - Contains 2 values: Number of UTC seconds since the epoch and a 4 digit random identifier.
    #  or 10^-4 decimilliseconds
    #  - Use the erad.util.instance_unpack, and erad.util.instance_pack functions to read/write
    Instance = NumberAttribute(range_key=True)

    # Group name for the group ID
    Group_Name = UnicodeAttribute(null=True)

    # AccountGroup_ID
    AccountGroupId_index = AccountGroupIdIndex2()
    AccountGroup_ID = UnicodeAttribute(null=True)

    # Packet Type from log file
    PacketType = UnicodeAttribute(null=True)

    # Username: A specific Username, or a Wildcarded Username,
    #  or a Mac Address.
    Username = UnicodeAttribute(null=True)

    # Parent_ID: An optional parent id to group logs to specific supplicant parent
    Parent_ID = UnicodeAttribute(null=True)

    # CalledStationId: The Mac Address (and possibly SSID) of this device.
    CalledStationId = UnicodeAttribute(null=True)

    # CallingStationId: The Mac Address (and possibly SSID) of client.
    CallingStationId = UnicodeAttribute(null=True)

    # Event Timestamp from log file
    EventTimestamp = UnicodeAttribute(null=True)

    # NAS Identifier from log file
    NASIdentifier = UnicodeAttribute(null=True)

    # NAS IP Address from log file
    NASIpAddress = UnicodeAttribute(null=True)

    # Access (Accept or Reject)
    Access = UnicodeAttribute(null=True)

    # True when the request is proxied to a different server
    IsProxied = LegacyBooleanAttribute(null=True)

    # Endpoint Ip Address
    EndpointIPAddress = UnicodeAttribute(null=True)

    # Endpoint Port
    EndpointPort = UnicodeAttribute(null=True)

    # EndpointGroupName: Stores information about endpoint related group name.
    EndpointGroupName = UnicodeAttribute(null=True)

    # EndpointGroupID: Stores information about endpoint related group id.
    EndpointGroupID = UnicodeAttribute(null=True)

    # CustomJsonData: Stores information Parent_ID and DeviceType on auth request.
    CustomJsonData = UnicodeAttribute(null=True)


# This function specifies which of the above fields are allowed for
#  export of logs.
#  Instance is omitted.
def supplicant_log_export_fields():
    return [
        "Group_ID",
        "PacketType",
        "Username",
        "CalledStationId",
        "CallingStationId",
        "EventTimestamp",
        "NASIdentifier",
        "NASIpAddress",
        "Access"
    ]


class SupplicantCertificateIndex(GlobalSecondaryIndex):
    class Meta:
        index_name = 'Username-Group_ID-index'
        read_capacity_units = 3
        write_capacity_units = 2
        projection = AllProjection()

    Username = UnicodeAttribute(hash_key=True)
    Group_ID = UnicodeAttribute(range_key=True)


class Erad_SupplicantCertificate(Model):
    class Meta:
        table_name = "Erad_SupplicantCertificate_Global"
        name = "SupplicantCertificate"
        read_capacity_units = 3
        write_capacity_units = 2
        region = util.erad_cfg().database.region
        host = util.erad_cfg().database.host

    # Random string generated for certificate, used as hash key
    ID = UnicodeAttribute(hash_key=True)

    # Username: A specific Username, or a Wildcarded Username,
    #  or a Mac Address.
    Username = UnicodeAttribute(null=False)

    # Group_ID
    Group_ID = UnicodeAttribute(null=False)

    # Format
    Format = UnicodeAttribute(null=False)

    # Certificate
    Certificate = UnicodeAttribute(null=False)

    # Timestamp
    Timestamp = UTCDateTimeAttribute(null=True)

    supplicant_certificate_index = SupplicantCertificateIndex()

    # Checksum: A one-way hash of Account_ID, Group_ID, Username, and Password from Erad_Supplicant
    Checksum = UnicodeAttribute(null=False)


