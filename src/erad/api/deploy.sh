#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
#
# Deploy script which is intended to run from the machine intended to be
# deployed.
#
################################################################################

set -e
set -x

export ERAD_API_INSTALL=true

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

SOURCE=/usr/local/src
TARGET=/usr/local/webservices
INSTANCE=$(curl http://169.254.169.254/latest/meta-data/instance-id)
PUB_IP=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
DEPLOY_REGION=$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone | sed 's/[a-z]$//')

parse_global_config() {
    for file in boto.cfg erad.cfg; do
        cat /etc/$file \
            | sed "s:\##AWS_ACCESS_KEY_ID##:$AWS_ACCESS_KEY_ID:g" \
            | sed "s:\##AWS_SECRET_ACCESS_KEY##:$AWS_SECRET_ACCESS_KEY:g" \
            | sed "s:\##AWS_DEFAULT_REGION##:$AWS_DEFAULT_REGION:g" \
            | sed "s:\##SYSTEM_KEY##:$SYSTEM_KEY:g" \
            | sed "s:\##BILLING_KEY##:$BILLING_KEY:g" \
            | sed "s:\##SITEWIDE_SALT##:$SITEWIDE_SALT:g" \
            | sed "s:\##DEPLOY_REGION##:$DEPLOY_REGION:g" \
            > /etc/$file.deploy

        cp /etc/$file.deploy /etc/$file
    done
}

# set scripts executable
chmod +x $SOURCE/api_unit_testing.sh

# update and install required packages
yum -y update
amazon-linux-extras enable -y epel redis4.0
yum -y install epel-release
yum -y install \
  httpd \
  httpd-devel \
  python3 \
  python3-devel \
  gcc \
  java \
  git \
  redis \
  jemalloc \
  tar \
  wget \
  unzip \
  openssl

systemctl start redis

# install python modules
pip3 install --upgrade pip
pip3 install \
    'awscli<1.16.264' \
    'pynamodb<4.0' \
    'pyyaml>=3.10,<=5.1' \
    'botocore<1.13.0' \
    'boto3==1.9.253' \
    boto \
    'celery<5.0' \
    'Flask' \
    'flask-caching>=1.10' \
    flask-compress \
    lxml \
    mako \
    mod_wsgi \
    ordereddict \
    pycrypto \
    python-dateutil \
    pytz \
    redis\
    requests \
    validate_email

# Installing fork of signxml with changes that fix this issue https://github.com/XML-Security/signxml/issues/43
pip3 install https://bitbucket.org/teamfortytwo/signxml/get/master.zip

# Loaing mod_wsgi that support python3, was installed on previous step
echo 'LoadModule wsgi_module /usr/local/lib64/python3.7/site-packages/mod_wsgi/server/mod_wsgi-py37.cpython-37m-x86_64-linux-gnu.so' >> /etc/httpd/conf.modules.d/00-base.conf

# change owner to apache
chown -R ec2-user:ec2-user $SOURCE

# deploy wsgi file
mkdir -p $TARGET/erad
cp $SOURCE/erad/api/api.wsgi $TARGET/api.wsgi
cp $SOURCE/erad/api/statuscheck.wsgi $TARGET/statuscheck.wsgi

# deploy configuration for apache
cp $SOURCE/erad/api/erad.conf /etc/httpd/conf.d

# copy swagger-ui to the apache root
curl https://codeload.github.com/swagger-api/swagger-ui/tar.gz/v2.2.3 \
    | tar xzf - -C /var/www/html/ \
    && cp -r /var/www/html/swagger-ui-2.2.3/dist/* /var/www/html \
    && rm -rf /var/www/html/swagger-ui-2.2.3/ \
    && sed -i 's/http:\/\/petstore.swagger.io\/v2\/swagger.json/erad-docs.yaml/g' \
        /var/www/html/index.html

# copy erad docs
aws s3 cp s3://teamfortytwo.deploy/docs/erad-docs.yaml /var/www/html/erad-docs.yaml

# set the python path
export PATH=$PATH:$TARGET
export PYTHONPATH=$PATH:/usr/local/sbin

# copy files to the proper location
cp $SOURCE/create_database.py $TARGET
cp $SOURCE/spawn_api_server.py $TARGET
cp -r $SOURCE/erad/* $TARGET/erad
chown -R ec2-user:ec2-user $TARGET

# Verify if decrypt passphrase was provided.
if [ ! -z "$T42_PASSPHRASE" ]; then

  # decrypt production config file
  source $TARGET/erad/api/config.prod.secure $T42_PASSPHRASE

  # copy production configs
  cp $TARGET/erad/api/boto.cfg.prod /etc/boto.cfg
  cp $TARGET/erad/api/erad.cfg.prod /etc/erad.cfg
  chmod 644 /etc/erad.cfg
  parse_global_config

  # decrypt and install certificates
  pushd $TARGET/erad/api/certs
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output enterpriseauth.key --decrypt enterpriseauth.key.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output calix-ev-cert.key --decrypt calix-ev-cert.key.asc
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output prod-calix-ev-cert.key --decrypt prod-calix-ev-cert.key.asc
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output ca.key --decrypt ca.prod.key.secure
  gpg --batch --yes --passphrase $T42_PASSPHRASE --output ca.pass --decrypt ca.prod.pass.secure
  cat ca.prod.pem > ca.pem
  sed -i "s/non3y4db/$(cat ca.pass)/g" ../interface_admin.py

  # build profile signing certificates
  cat enterpriseauth.ca > signer-ios.ca
  cat enterpriseauth.crt > signer-ios.pem
  cat enterpriseauth.key >> signer-ios.pem
  cat enterpriseauth.crt > signer-windows.pem
  cat enterpriseauth.ca >> signer-windows.pem
  cat enterpriseauth.key > signer-windows.key

  cat calix-ev-cert.ca > signer-ios-calix.ca
  cat calix-ev-cert.pem > signer-ios-calix.pem
  cat calix-ev-cert.key >> signer-ios-calix.pem
  cat calix-ev-cert.pem > signer-windows-calix.pem
  cat calix-ev-cert.ca >> signer-windows-calix.pem
  cat calix-ev-cert.key > signer-windows-calix.key

  cat prod-calix-ev-cert.ca > signer-ios-calix-prod.ca
  cat prod-calix-ev-cert.pem > signer-ios-calix-prod.pem
  cat prod-calix-ev-cert.key >> signer-ios-calix-prod.pem
  cat prod-calix-ev-cert.pem > signer-windows-calix-prod.pem
  cat prod-calix-ev-cert.ca >> signer-windows-calix-prod.pem
  cat prod-calix-ev-cert.key > signer-windows-calix-prod.key
  popd

  # set the deployment region for configuring the database connection, record in version file
  sed -i "s/##location##/$DEPLOY_REGION/g" $TARGET/erad/api/static/version.html

else
  source $TARGET/erad/api/config.dev

  # otherwise, install development .cfg files
  cp $SOURCE/enable_streams.py $TARGET
  cp $TARGET/erad/api/boto.cfg.dev /etc/boto.cfg
  cp $TARGET/erad/api/erad.cfg.dev /etc/erad.cfg
  chmod 644 /etc/erad.cfg
  parse_global_config

  # run the web service in the background (on the dev port of 5000)
  python3 $TARGET/spawn_api_server.py 2> $TARGET/webservice_error.log > $TARGET/webservice.log &

  # install local dynamo db
  source $DIR/../db/deploy.sh install_local_dynamodb $TARGET

  pip3 install nose

  cp $SOURCE/api_unit_testing.sh /usr/bin
fi

pushd $TARGET/erad/api
celery worker -A tasks.celery \
    --loglevel=info --beat \
    --logfile="$TARGET/%n.log" --uid ec2-user &

# enable and start Apache
chkconfig httpd on
systemctl start httpd

# add alarm for cpu-surplus-credits-charged metric
export AWS_ACCESS_KEY_ID=AKIAIDR4RVSXLXAIAUYQ
export AWS_SECRET_ACCESS_KEY=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
aws cloudwatch put-metric-alarm \
    --alarm-name cpu-surplus-credits-charged-$INSTANCE-api \
    --alarm-description "CPUSurplusCreditsCharged " \
    --metric-name CPUSurplusCreditsCharged --namespace AWS/EC2 \
    --statistic Average --period 300 --threshold 0 \
    --comparison-operator GreaterThanThreshold \
    --dimensions Name=InstanceId,Value=$INSTANCE \
    --evaluation-periods 1 --alarm-actions arn:aws:sns:us-west-2:439838887486:trouble \
    --unit Count --region us-west-2

## Add disk and swap monitoring if this is a production server
if [ ! -z "$T42_PASSPHRASE" ]; then
  pushd $SOURCE
  yum -y install \
          perl-Switch \
          perl-DateTime \
          perl-Sys-Syslog \
          perl-LWP-Protocol-https \
          perl-Digest-SHA.x86_64

  curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
  unzip CloudWatchMonitoringScripts-1.2.2.zip && \
  rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
  pushd aws-scripts-mon

  echo "AWSAccessKeyId=$AWS_ACCESS_KEY_ID
AWSSecretKey=$AWS_SECRET_ACCESS_KEY
" > awscreds.conf
  crontab -l > crontab.conf || true
  echo "* * * * * $SOURCE/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail --swap-util --swap-used --disk-space-util --disk-path=/ --from-cron" >> crontab.conf
  crontab crontab.conf
  popd

  # add alarms for disk and swap
  aws cloudwatch put-metric-alarm \
      --alarm-name disk-utilization-$INSTANCE-erad-api \
      --alarm-description "Disk usage is high" \
      --metric-name DiskSpaceUtilization \
      --namespace System/Linux  --statistic Average --period 300 --threshold 90 \
      --comparison-operator GreaterThanThreshold \
      --dimensions \
        Name=Filesystem,Value=/dev/xvda1 \
        Name=MountPath,Value=/ \
        Name=InstanceId,Value=$INSTANCE \
      --evaluation-periods 1 \
      --alarm-actions arn:aws:sns:us-west-2:439838887486:trouble \
      --unit Percent --region us-west-2
  aws cloudwatch put-metric-alarm \
      --alarm-name swap-utilization-$INSTANCE-erad-api \
      --alarm-description "Swap usage is high" \
      --metric-name SwapUtilization \
      --namespace System/Linux --statistic Average  --period 300 --threshold 20 \
      --comparison-operator GreaterThanThreshold \
      --dimensions \
        Name=InstanceId,Value=$INSTANCE \
      --evaluation-periods 1 \
      --alarm-actions arn:aws:sns:us-west-2:439838887486:trouble \
      --unit Percent --region us-west-2
fi

exit $?

