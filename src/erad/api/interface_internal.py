#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad.exceptions import *
from .erad_model import *
from .interface_util import *

class EradInterfaceInternal(EradInterfaceUtil):

    def schema_status( self ):
        statuses = {}
        for table in erad_table_list():
            m = table.Meta
            statuses[m.table_name] = { "Exists": table.exists() }
        return statuses

    def schema_retrieve( self ):
        schema = {}
        for table in erad_table_list():
            m = table.Meta
            cols = {}
            for k,v in inspect.getmembers(table, lambda a:not(inspect.isroutine(a))):
                if issubclass(type(v), Attribute):
                    cols[k] =  type(v).__name__
            schema[m.table_name] = {
                        "Exists": table.exists(),
                        "Read Capacity": m.read_capacity_units,
                        "Write Capacity": m.write_capacity_units,
                        "Columns": cols
                    }
        return schema

    def schema_describe( self ):
        schema = {}
        for table in erad_table_list():
            m = table.Meta
            if table.exists():
                schema[m.table_name] = table.describe_table()
            else:
                schema[m.table_name] = None
        return schema

    def schema_delete_all( self ):
        schema = {}
        for table in erad_table_list():
            m = table.Meta
            if table.exists():
                schema[m.table_name] = table.delete_table()
            else:
                schema[m.table_name] = None
        return schema

    def dump_database( self ):
        ret = {}
        for table in erad_table_list():
            name = table.Meta.table_name
            try:
                ret[name] = []
                for obj in table.scan():
                    ret[name].append( model_to_pod( obj ) )
            except:
                ret[name] = None
        return ret
































