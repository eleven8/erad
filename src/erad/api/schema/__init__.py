#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad.api import erad_model
from datetime import datetime

# early exit if this smells anything like production
def is_safe_environment():
    tables = erad_model.erad_table_list()
    for table in tables:
        if "amazonaws.com" in table.Meta.host:
            return False
    return True

def insert_data():
    table = erad_model.Erad_Endpoint
    item1 = table('52.26.34.201', Port="1812", Group_ID="elevenos::Global", ClusterId="uswest-dev-01", Secret="4905DVCA3B", Region="us-west-1")
    item1.save()
    item2 = table('52.26.34.201', Port="20000", Group_ID="fooaccount::Global", ClusterId="uswest-dev-02", Secret="secret20000", Region="us-west-1")
    item2.save()
    item3 = table('52.26.34.201', Port="20002", Group_ID="baraccount::Global", ClusterId="uswest-dev-03", Secret="secret20002", Region="us-west-1")
    item3.save()
    # group_id for std_group3
    item4 = table('52.26.34.201', Port="20004", Group_ID="xptoaccount::AB-CD-1002", ClusterId="uswest-dev-04", Secret="secret20004", Region="us-west-1")
    item4.save()
    item5 = table('52.26.34.201', Port="20006", Group_ID="xptoaccount::AB-CD-1002", ClusterId="uswest-dev-05", Secret="secret20006", Region="us-west-1")
    item5.save()
    # group_id for std_group2
    item6 = table('52.26.34.201', Port="20008", Group_ID="xptoaccount::FF-1925", ClusterId="uswest-dev-06", Secret="secret20008", Region="us-west-1")
    item6.save()
    # group_id to test the "saving" of endpoints
    item7 = table('52.26.34.124', Port="20006", Group_ID="::free", ClusterId="uswest-dev-01", Secret="4905DVCA3B", Region="us-west-1")
    item7.save()
    item8 = table('52.26.34.124', Port="20008", Group_ID="::free", ClusterId="uswest-dev-01", Secret="4905DVCA3B", Region="us-west-1")
    item8.save()
    item9 = table('52.26.34.124', Port="20010", Group_ID="::free", ClusterId="uswest-dev-01", Secret="4905DVCA3B", Region="us-east-1")
    item9.save()
    item10 = table('52.26.34.124', Port="20012", Group_ID="xptoaccount::AA-BCD", ClusterId="uswest-dev-01", Secret="4905DVCA3B", Region="us-west-2")
    item10.save()
    item11 = table('33.44.55.66', Port="3334", Group_ID="::dirty", ClusterId="uswest-dev-02", Secret="4905DVCA3B", Region="us-west-2")
    item11.save()
    item12 = table('52.26.34.255', Port="20020", Group_ID="::free", ClusterId="useast-dev-03", Secret="secret20020", Region="us-west-1")
    item12.save()
    # Not allocated group, to test test_search_supplicant_in_not_allocated_group()
    item12 = table('52.26.34.254', Port="20021", Group_ID="::free", ClusterId="useast-dev-03", Secret="secret20020", Region="us-west-5")
    item12.save()
    # New ednpoint to test public account creation
    item13 = table('52.26.34.124', Port="20014", Group_ID="::free", ClusterId="uswest-dev-01", Secret="4905DVCA3B", Region="us-east-")
    item13.save()
    # save free radius ports
    insert_radius_ports("52.26.34.201", "uswest-dev-01", "us-west-2", 20030)
    insert_radius_ports("34.192.196.106", "useast-dev-01", "us-east-2", 20030)


    table('52.26.34.201', Port="20014", Group_ID="fooaccount::bar", ClusterId="uswest-dev-02", Secret="secret20002", Region="us-west-1").save()

    # table('52.26.34.201', Port='2222', Group_ID='::dirty', ClusterId='useast-dev-01', Secret='secret-lb-format', Region='us-east-1').save()
    instances = {'auth': {
        'instance-id': '52.26.34.201',
        },
                 'acct': {
                     'instance-id-acct': '52.26.34.201',
                 }
                }
    instances_str = str(instances)
    erad_model.Erad_Cluster('useast-dev-01', AuthNum=1, AcctNum=1, Instances=instances_str).save()
    erad_model.Erad_Cluster('uswest-dev-01', AuthNum=1, AcctNum=1, Instances=instances_str).save()
    erad_model.Erad_Cluster('uswest-dev-02', AuthNum=2, AcctNum=4, Instances=instances_str).save()

    # for the new lb format
    table('22.33.44.0', Port='2222', Group_ID='xptoaccount::AA-1591', ClusterId='useast-dev-02', Secret='secret-lb-format', Region='us-east-1').save()
    instances = {'auth': {
        'auth-instance-id-1': '22.33.44.1',
        },
                 'acct': {
                     'acct-instance-id-1': '22.33.44.3',
                     'acct-instance-id-2': '22.33.44.4',
                     'acct-instance-id-3': '22.33.44.5',
                     'acct-instance-id-4': '22.33.44.6'
                 }
                }
    instances_str = str(instances)
    erad_model.Erad_Cluster('useast-dev-02', AuthNum=2, AcctNum=4, Instances=instances_str).save()

    group = erad_model.Erad_Group2(Account_ID="elevenos", ID="foo", Name="foo", TimeZone="America/Los_Angeles")
    group.save()
    # for frmod testing
    group = erad_model.Erad_Group2(
                        Account_ID="elevenos",
                        ID="fooremoteattr",
                        Name="fooremoteattr",
                        TimeZone="America/Los_Angeles",
                        OverrideVlan=True,
                        OverrideConnectionSpeed=True,
                        MaxDownloadSpeedBits=9,
                        MaxUploadSpeedBits=9
                        )
    group.save()
    # --------
    group = erad_model.Erad_Group2(Account_ID="fooaccount", ID="bar", Name="bar", TimeZone="America/Los_Angeles")
    group.save()

    # adding Global group
    group = erad_model.Erad_Group2(Account_ID="eradtests", ID="ERAD Monitoring", Name="Global", TimeZone="America/Los_Angeles")
    group.save()
    group = erad_model.Erad_Group2(Account_ID="eradtests", ID="erad-tests", Name="Global", TimeZone="America/Los_Angeles")
    group.save()
    group = erad_model.Erad_Group2(Account_ID="elevenos", ID="Global", Name="Global", TimeZone="America/Los_Angeles")
    group.save()
    group = erad_model.Erad_Group2(Account_ID="fooaccount", ID="Global", Name="Global", TimeZone="America/Los_Angeles")
    group.save()
    group = erad_model.Erad_Group2(Account_ID="baraccount", ID="Global", Name="Global", TimeZone="America/Los_Angeles")
    group.save()
    group = erad_model.Erad_Group2(Account_ID="elevenos", ID="AB-CD-1002", Name="AB-CD-1002", TimeZone="America/Los_Angeles")
    group.save()
    group = erad_model.Erad_Group2(Account_ID="xptoaccount", ID="AB-CD-1002", Name="AB-CD-1002", TimeZone="America/Los_Angeles")
    group.save()
    group = erad_model.Erad_Group2(Account_ID="xptoaccount", ID="FF-1925", Name="FF-1925", TimeZone="America/Los_Angeles")
    group.save()

    authenticator = erad_model.Erad_Authenticator2('11:22:33:44:55:66', Group_ID="erad-tests", Account_ID='eradtests')
    authenticator.save()
    authenticator = erad_model.Erad_Authenticator2('11:22:33:44:55:66', Group_ID="foo", Account_ID='elevenos')
    authenticator.save()
    authenticator = erad_model.Erad_Authenticator2('66:55:44:33:22:11', Group_ID="bar", Account_ID='fooaccount')
    authenticator.save()
    authenticator = erad_model.Erad_Authenticator2('AA:55:44:33:22:11', Group_ID="Global", Account_ID='fooaccount')
    authenticator.save()
    authenticator = erad_model.Erad_Authenticator2('11:22:33:44:55:66', Group_ID="Global", Account_ID='baraccount')
    authenticator.save()
    authenticator = erad_model.Erad_Authenticator2('BB:55:44:33:22:11', Group_ID="Global", Account_ID='fooaccount', RadiusAttribute='nas-identifier')
    authenticator.save()
    authenticator = erad_model.Erad_Authenticator2('testidentifier', Group_ID="bar", Account_ID='fooaccount', RadiusAttribute='nas-identifier')
    authenticator.save()
    authenticator = erad_model.Erad_Authenticator2('testidentifier', Group_ID="xpto", Account_ID='xptoaccount', RadiusAttribute='nas-identifier')
    authenticator.save()

    # for frmod test
    authenticator = erad_model.Erad_Authenticator2('foo', Group_ID="foo", Account_ID='elevenos', RadiusAttribute='nas-identifier')
    authenticator.save()
    authenticator = erad_model.Erad_Authenticator2('11:22:33:44:55:67', Group_ID="fooremoteattr", Account_ID='elevenos')
    authenticator.save()
    authenticator = erad_model.Erad_Authenticator2('fooremoteattr', Group_ID="fooremoteattr", Account_ID='elevenos', RadiusAttribute='nas-identifier')
    authenticator.save()
    authenticator = erad_model.Erad_Authenticator2('11:22:33:44:55:66', Group_ID="foo", Account_ID='elevenos')
    authenticator.save()

    # authenticator to test radiusd with radclient
    # called from scripts proxy_loader and radiusd_conf_loader
    authenticator = erad_model.Erad_Authenticator2('BA:BB:1E:BA:BB:1E', Group_ID="erad-tests", Account_ID='eradtests', RadiusAttribute='called-station-id')
    authenticator.save()


    supplicant = erad_model.Erad_Supplicant('eradtests::erad-tests', 'babble', Password="3000", UseRemote=0, UseWildcard=0)
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('eradtests::erad-tests', 'testing', Password="testing", UseRemote=0, UseWildcard=0)
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('elevenos::foo', 'testing', Password="testing", UseRemote=0, UseWildcard=0)
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('fooaccount::bar', 'testing', Password="testing", UseRemote=0, UseWildcard=0)
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('baraccount::bar', 'testing', Password="testing", UseRemote=0, UseWildcard=0)
    supplicant.save()

    # for frmod test
    supplicant = erad_model.Erad_Supplicant('elevenos::foo', 'testing', Password="testing", UseRemote=0, UseWildcard=0, Vlan=999)
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('elevenos::foo', 'testing_remote', Password="testing", UseRemote=1, UseWildcard=0, Vlan=888)
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('elevenos::foo', 'testing@enterpriseauth.com', Password="testing", UseRemote=0, UseWildcard=0, Vlan=333)
    supplicant.save()
    # this supplicant account used to test attributes accepted from home server
    # script erad/src/erad/frmod/remove_proxy.py hardcode vlan attribute into
    # freeradius server home instance
    supplicant = erad_model.Erad_Supplicant('elevenos::fooremoteattr',
                                            'testing_remote_attr',
                                            Password="testing",
                                            UseRemote=1,
                                            UseWildcard=0,
                                            Vlan=787,
                                            )
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('elevenos::fooremoteattr',
                                            'AA:AA:AA:AA:AA:AA',
                                            Password="testing",
                                            UseRemote=1,
                                            UseWildcard=0,
                                            Vlan=444,
                                            )
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('elevenos::foo', 'testing_remote_full_eap', Password="testing_full_eap", UseRemote=1, UseWildcard=0, Vlan=333)
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('elevenos::foo', 'malicious_outer_identitny', Password="malicious_pass", UseRemote=0, UseWildcard=0, Vlan=123)
    supplicant.save()

    supplicant = erad_model.Erad_Supplicant(
                    'elevenos::foo',
                    'expired_testing',
                    Password="expired",
                    UseRemote=0,
                    UseWildcard=0,
                    Vlan=123,
                    ExpirationDate = datetime.utcfromtimestamp(0))
    supplicant.save()

    supplicant = erad_model.Erad_Supplicant('elevenos::foo', 'AA:BB:CC:DD:EE:FF', Password="AA:BB:CC:DD:EE:FF", UseRemote=0, UseWildcard=0, Vlan=999)
    supplicant.save()
    # this supplicant will be authorized through proxy
    supplicant = erad_model.Erad_Supplicant('elevenos::foo', 'AA:BB:CC:DD:EE:99', Password="AA:BB:CC:DD:EE:99", UseRemote=1, UseWildcard=0, Vlan=888)
    supplicant.save()


    # Global supplicants
    supplicant = erad_model.Erad_Supplicant('elevenos::Global', 'globaltest', Password="globaltest", UseRemote=0, UseWildcard=0)
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('fooaccount::Global', 'globaltest', Password="globaltest", UseRemote=0, UseWildcard=0)
    supplicant.save()
    supplicant = erad_model.Erad_Supplicant('baraccount::Global', 'globaltest', Password="globaltest", UseRemote=0, UseWildcard=0)
    supplicant.save()

    # authenticator to test radiusd with radclient
    # called from scripts proxy_loader and radiusd_conf_loader
    supplicant = erad_model.Erad_Supplicant('eradtests::erad-tests', 'SR-980-83/erad-monitor', Password="nt5ewa9s", UseRemote=0, UseWildcard=0)
    supplicant.save()


    # Supplicant with certificates
    supplicant = erad_model.Erad_SupplicantCertificate(
            '0V1XNP9SF5ZLW3DEJ4M2',
            Username='Fred_Durst69',
            Group_ID='elevenos::Global',
            Format='something',
            Certificate='cert-data',
            Checksum='fCaAZEIXh2nBKK3WzNqLyBhQCeFc_xyfIlNfZLA_Ba5OTfvyq9bBClnvnfLhbFYr'
           )
    supplicant.save()
    # -----

    apikey = erad_model.Erad_ApiKey('i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF', Account_ID='elevenos', Active=1, ApiKeyName='ElevenWireless')
    apikey.save()

    apikey1 = erad_model.Erad_ApiKey('disabled_inegration_key', Account_ID='elevenos', Active=0, ApiKeyName='ElevenWireless-disabled')
    apikey1.save()


    # password for this record: Eleven
    accountOwner = erad_model.Erad_AccountOwner('owner@elevenos.com',
                                                Password='8242c0e86adfeeded50eb887e2765c12649a1581adfe7ec13aa69ee971b3310e',
                                                Account_ID='elevenos',
                                                Salt='7303c866')
    accountOwner.save()

     # password for this record: Eleven
    accountOwner = erad_model.Erad_AccountOwner('fooaccount@elevenos.com',
                                                Password='6685180cb83fe6c650e021fd7e68a632af08800360fdb845c1401a09cced1e88',
                                                Account_ID='fooaccount',
                                                Salt='83f58ebb')
    accountOwner.save()

     # password for this record: Eleven
    accountOwner = erad_model.Erad_AccountOwner('baraccount@elevenos.com',
                                                Password='a8d4455940a88542c7787aeb9b3765110bec767fc69929f19cbdd417a911e7bc',
                                                Account_ID='baraccount',
                                                Salt='722bdf11')

    accountOwner.save()

    return True

def insert_radius_ports(ipaddress, cluster, region, startport):
    i = 0
    port = startport
    while i < 100 :
        table = erad_model.Erad_Endpoint
        item = table(ipaddress, Port=str(port), Group_ID="::free", ClusterId=cluster, Secret="4905DVCA3B", Region=region)
        item.save()
        port = port + 2
        i = i + 1

    for port in range(40000, 40100, 2):
        table = erad_model.Erad_Endpoint
        item = table(ipaddress, Port=str(port), Group_ID="::premium", ClusterId=cluster, Secret="4905DVCA3B", Region=region)
        item.save()

    return True

def create_logs(group, num=30):
    for i in range(num):
        date = datetime.utcnow()
        timestamp = int(date.timestamp() * (10000))
        print(i, timestamp)
        erad_model.Erad_Radius_Log.save(
            erad_model.Erad_Radius_Log(
                Group_ID=group,
                Instance=timestamp,
                Group_Name="Group Admin",
                AccountGroup_ID=f"elevenos::{group}",
                PacketType="Access-Request",
                Username=f"supplicant_{i}",
                CalledStationId="00:11:22:33:44:55",
                EventTimestamp=date.strftime("%b %d %Y %H:%M:%S UTC"),
                NASIpAddress="52.35.24.232",
                Access="Access-Accept",
            )
        )

def create_bubble_logs(group, num=30):
    for i in range(num):
        date = datetime.utcnow()
        timestamp = int(date.timestamp() * (10000))
        print(i, timestamp)
        erad_model.Erad_Radius_Log.save(
            erad_model.Erad_Radius_Log(
                Group_ID=group,
                Instance=timestamp,
                Group_Name="Group Admin",
                AccountGroup_ID=f"elevenos::{group}",
                PacketType="Access-Request",
                Username=f"babble_{i}",
                CalledStationId="ba:bb:1e:ba:bb:1e",
                EventTimestamp=date.strftime("%b %d %Y %H:%M:%S UTC"),
                NASIpAddress="52.35.24.232",
                Access="Access-Accept",
            )
        )

def create_database():
    if not is_safe_environment():
        print("This smells like production, refusing to create database!")
        return
    try:
        print("Beginning creation of Erad database.")

        tables = erad_model.erad_table_list()

        for table in tables:
            m = table.Meta
            if table.exists():
                print("Updating Table {0}. ({1} read / {2} write)".format(
                                                    m.table_name,
                                                    m.read_capacity_units,
                                                    m.write_capacity_units
                                                    ))
            else:
                print("Creating Table {0}. ({1} read / {2} write)".format(
                                                    m.table_name,
                                                    m.read_capacity_units,
                                                    m.write_capacity_units
                                                    ))
            table.create_table(wait=True)

        insert_data()
        print("Erad Database created successfully.")

    except Exception as e:
        print("Erad Database creation failed: ")
        print(str(e))

def remove_database():
    if not is_safe_environment():
        print("This smells like production, refusing to remove database!")
        return
    try:
        print("Beginning removal of Erad database.")

        tables = erad_model.erad_table_list()
        for table in tables:
            if table.exists():
                print("Deleting table " + table.Meta.name + " in " + table.Meta.region)
                table.delete_table()

        print("Erad Database removed.")

    except Exception as e:
        print("Erad Database removal failed: ")
        print(str(e))
