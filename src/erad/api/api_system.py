#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from erad.api import *
from .interface_system import EradInterfaceSystem

#---------- System Services ----------

# http://localhost:5000/erad/system/supplicant/find
@app.route('/erad/system/supplicant/find', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def system_supplicant_find():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceSystem().supplicant_find( request.json ) )


# http://localhost:5000/erad/system/supplicant/log/save
@app.route('/erad/system/supplicant/log/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def system_supplicant_log_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceSystem().supplicant_log_save( request.json ) )

# http://localhost:5000/erad/system/supplicant/acct/log/save
@app.route('/erad/system/supplicant/acct/log/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def system_supplicant_acct_log_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceSystem().supplicant_acct_log_save( request.json ) )

# http://localhost:5000/erad/system/server/config/load
@app.route('/erad/system/server/config/load', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def system_server_config_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceSystem().server_config_load( request.json ) )

# http://localhost:5000/erad/system/account/save
@app.route('/erad/system/account/save', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def system_account_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceSystem().account_save( request.json ) )

@app.route('/erad/system/account/certs/list', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def system_account_certs_list():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceSystem().certs_list( request.json ) )

# http://localhost:5000/erad/system/radius/finish
@app.route('/erad/system/radius/finish', methods=['HEAD', 'POST', 'OPTIONS'])
def system_radius_finish():
    if request.method != 'POST':
        return ok_response()
    return ok_response(EradInterfaceSystem().radius_finish(request.json))

# http://localhost:5000/erad/system/radius/save
@app.route('/erad/system/radius/save', methods=['HEAD', 'POST', 'OPTIONS'])
def system_radius_save():
    if request.method != 'POST':
        return ok_response()
    return ok_response(EradInterfaceSystem().radius_save(request.json))

# http://localhost:5000/erad/system/radius/load
@app.route('/erad/system/radius/load', methods=['HEAD', 'POST', 'OPTIONS'])
def system_radius_load():
    if request.method != 'POST':
        return ok_response()
    return ok_response(EradInterfaceSystem().radius_load(request.json))

# http://localhost:5000/erad/system/radius/region
@app.route('/erad/system/radius/region', methods=['HEAD', 'POST', 'OPTIONS'])
def system_radius_region():
    if request.method != 'POST':
        return ok_response()
    return ok_response(EradInterfaceSystem().radius_region(request.json))

# http://localhost:5000/erad/system/supplicant/find/certificate
@app.route('/erad/system/supplicant/find/certificate', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def system_supplicant_find_certificate():
    if request.method != 'POST':
        return ok_response()
    return ok_response( EradInterfaceSystem().supplicant_find_certificate( request.json ) )
