import celery
import boto.ec2.cloudwatch
import redis


@celery.task
def send_call_duration():
    calls = list()
    min_duration = 0
    max_duration = 0
    avg_duration = 0

    redis_client = redis.StrictRedis()

    # Count number of calls in last minute
    num_calls = redis_client.llen("api-call-duration")

    # Pop from Redis all calls in last minute
    for call in range(num_calls):
        calls.append(float(redis_client.lpop("api-call-duration")))

    if num_calls != 0:
        min_duration = float(min(calls))
        max_duration = float(max(calls))
        avg_duration = float(sum(calls)) / num_calls

    print("Num :: %s" % str(num_calls))
    print("Min :: %s" % str(min_duration))
    print("Max :: %s" % str(max_duration))
    print("Avg :: %s" % str(avg_duration))

    # Connection to CloudWatch
    boto_connection = boto.ec2.cloudwatch.connect_to_region(
        'us-east-1',
        aws_access_key_id='AKIAIDR4RVSXLXAIAUYQ',
        aws_secret_access_key='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'
    )

    # Send number of requests to API
    boto_connection.put_metric_data(
        namespace='Web-Services',
        name='flask-api-call-count',
        value=float(num_calls),
        unit='Count'
    )

    # Send the min request time duration
    boto_connection.put_metric_data(
        namespace='Web-Services',
        name='flask-api-call-duration-min',
        value=min_duration,
        unit='Milliseconds'
    )

    # Send the max request time duration
    boto_connection.put_metric_data(
        namespace='Web-Services',
        name='flask-api-call-duration-max',
        value=max_duration,
        unit='Milliseconds'
    )

    # Send the average request time duration
    boto_connection.put_metric_data(
        namespace='Web-Services',
        name='flask-api-call-duration-avg',
        value=avg_duration,
        unit='Milliseconds'
    )
