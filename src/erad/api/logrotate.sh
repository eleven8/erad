#!/usr/bin/env bash

# if any error happen abort script to prevent deletion of log file
set -e

logfile="/var/log/httpd/logrotate.log"
logbucket="%LogsBucket%"
hostname=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)

date >> $logfile

# suppress using /etc/boto.cfg
export BOTO_CONFIG=

for filepath in $(ls /var/log/httpd/*.gz); do
    filename=${filepath##*/}

    dateext=${filename%.gz}
    dateext=${dateext##*-}
    y=$(date -d @${dateext} +%Y); m=$(date -d @${dateext} +%m); d=$(date -d @${dateext} +%d); h=$(date -d @${dateext} +%H)

    aws s3 cp "${filepath}" \
        s3://$logbucket/logs/erad/apache/$y/$m/$d/$h/${hostname}_${filename} \
        --acl bucket-owner-full-control \
        --content-encoding gzip \
        >> $logfile 2>&1
    rm -f "${filepath}"
done

