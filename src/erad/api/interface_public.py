#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import random
import re, os
from erad import util
from erad.api.interface_admin import EradInterfaceAdmin
from erad.exceptions import *
from .erad_model import *
from .interface_util import *
from .interface_system import *
import time
from time import mktime
import csv, io, string
import base64
import datetime
import requests
from lxml import etree
from signxml import XMLSigner
import boto
from boto.s3.key import Key
from boto.s3.connection import S3Connection
from boto.s3.connection import OrdinaryCallingFormat
from os import path
from hmac import compare_digest
from uuid import uuid4

from subprocess import Popen, PIPE, STDOUT

import logging

from .auditor import Auditor

logger = logging.getLogger(__name__)


class EradInterfacePublic(EradInterfaceUtil):

    __certsdir = 'certs'
    __dirname = path.dirname(path.realpath(__file__))

    # signer_ios_pem = path.join(__dirname, __certsdir, 'signer-ios.pem')
    # signer_ios_ca = path.join(__dirname, __certsdir, 'signer-ios.ca')
    #
    # signer_windows_key = path.join(__dirname, __certsdir, 'signer-windows.key')
    # signer_windows_crt = path.join(__dirname, __certsdir, 'signer-windows.pem')
    #
    # signer_ios_calix_pem = path.join(__dirname, __certsdir, 'signer-ios-calix.pem')
    # signer_ios_calix_ca = path.join(__dirname, __certsdir, 'signer-ios-calix.ca')
    #
    # signer_windows_calix_key = path.join(__dirname, __certsdir, 'signer-windows-calix.key')
    # signer_windows_calix_crt = path.join(__dirname, __certsdir, 'signer-windows-calix.crt')

    s3_gc_max_age = util.erad_cfg().s3_gc_max_age

    def get_sing_cert_path(self, account, device_type):
        accounts = {
            'calix': '-calix-prod',
            'calix-staging': '-calix'
        }

        supported_device_types = {'ios', 'windows'}

        if device_type not in supported_device_types:
            raise EradConfigurationException('Device type is not supported for signing')

        # if account does not have custom sing cert using default - enterpriseauth.com
        file_suffix = accounts.get(account, '')

        cert_filename = f'signer-{device_type}{file_suffix}.pem'
        key_filename = f'signer-{device_type}{file_suffix}.key'
        ca_filename = f'signer-{device_type}{file_suffix}.ca'

        full_path_cert = path.join(self.__dirname, self.__certsdir, cert_filename)
        full_path_key = path.join(self.__dirname, self.__certsdir, key_filename)
        full_path_ca = path.join(self.__dirname, self.__certsdir, ca_filename)

        return full_path_cert, full_path_key, full_path_ca


    def status_check( self ):
        statuses = {}
        table_item = Erad_Group2.get('elevenos','SR-980-83')
        statuses["Name"] =  table_item.Name
        return statuses

    def account_login ( self, fields ):
        erad_cfg = util.erad_cfg()
        account_fields = self.expect_dict( fields, Erad_AccountOwner.Meta.name )

        email = self.expect_unicode_value_nonempty(account_fields, "Email")
        password = self.expect_unicode_value_nonempty(account_fields, "Password")

        try:
            saved_account = Erad_AccountOwner(email)
            saved_account.Password = "fake_pass"
            saved_account.Account_ID = "fake_id"
            saved_account.Salt = "fake_salt"
            saved_account.refresh(consistent_read=True)
        except Exception as err:
            raise EradAccessException( "Login failed." )
        else:
            pass_hash = EradInterfaceSystem().pass_hash(password,
                                                        saved_account.Salt,
                                                        erad_cfg.crypto_params.sitewide_salt_secret_key)
            if compare_digest(saved_account.Password, pass_hash) is not True:
                raise EradAccessException("Login failed.")

        # pull all the groups for this session
        groups = []
        for group_item in Erad_Group2.query(saved_account.Account_ID):
            groups.append( group_item.ID )

        session_model = Erad_Session()
        session_model.AccountOwner = 1
        session_model.Account_ID = saved_account.Account_ID
        session_model.Group_ID_List = groups
        session_model.LogoutUrl = "https://admin.enterpriseauth.com/#/login"
        session_model.Identifier = saved_account.Email
        session_model.LastUsed = util.utcnow()

        random_id = ''
        while True:
            random_id = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(32))
            try:
                item = Erad_Session.get(random_id)
            except:
                break
        session_model.ID = random_id
        session_model.save()

        # delete older matching sessions
        self.delete_older_sessions(session_model)

        return {'Session_ID': random_id}

    # Account save
    def account_save(self, fields, request_metadata=None):
        erad_cfg = util.erad_cfg()
        account_model = Erad_AccountOwner()

        account_fields = self.expect_dict(fields, account_model.Meta.name)

        self.set_unicode_value_nonempty(account_model, account_fields, "Email")
        self.set_unicode_value_nonempty(account_model, account_fields, "Password")
        self.set_optional_unicode_value(account_model, account_fields, "Account_ID")

        if not util.is_valid_email(account_fields['Email']):
            raise EradValidationException('The email {0} is not valid.'.format(account_fields['Email']))

        try:
            if Erad_AccountOwner.get(account_fields['Email']):
                raise EradValidationException('The email already exists.')

        except DoesNotExist:
            logger.debug('Account for {0} does not exists.'.format(account_fields['Email']))

        if self.is_password_blacklisted(account_model.Password):
            raise EradBlacklistedPasswordException(
                "That password was found in a public list of previously compromised passwords. Please choose a different password."
            )

        # if no Account ID was specified, generate a random one.
        if account_model.Account_ID is None:
            account_model.Account_ID = util.generate_random_string(size=10).lower()

        try:
            if Erad_AccountOwner.account_index.count(account_model.Account_ID) > 0:
                raise EradValidationException('The account ID already exists.')

        except DoesNotExist:
            logger.debug('Account for {0} does not exists.'.format(account_fields['Email']))

        salt = util.get_salt()
        account_model.Password = self.pass_hash(account_model.Password,
                                                salt,
                                                erad_cfg.crypto_params.sitewide_salt_secret_key)
        account_model.Salt = salt

        # save to Erad_Group2
        group = Erad_Group2(account_model.Account_ID, ID="Global", Name="Global", TimeZone="America/Los Angeles")

        output = dict()
        endpoint = Erad_Endpoint()

        try:
            # Retrieve list of free endpoints and pick one.
            endpoints = [e for e in Erad_Endpoint().group_id_index.query("::free")]
            logger.debug(endpoints)
            endpoint = random.sample(endpoints, 1).pop()

            # Assign the new group to endpoint.
            endpoint.Group_ID = "{0}::{1}".format(
                account_model.Account_ID,
                group.ID
            )

            endpoint.save()

            output.update(
                {
                    'Account': {
                        'Endpoint': {
                            'IpAddress': endpoint.IpAddress,
                            'Port': endpoint.Port,
                            'Group_ID': endpoint.Group_ID
                        }
                    }
                }
            )

        except ValueError:
            raise EradValidationException("No more endpoints available")

        except Exception as e:
            raise EradConfigurationException(
                'Could not assign group {0} to endpoint {1}:{2}: {3}'.format(
                    endpoint.Group_ID,
                    endpoint.IpAddress,
                    endpoint.Port,
                    e.message
                )
            )

        account_model.save()

        group.save()

        output['Account'].update(
            {
                'Email': account_model.Email,
                'Account_ID': account_model.Account_ID
            }
        )

        audit_message = {
            "UserAgent": 'tests' if request_metadata is None else request_metadata.get('HTTP_USER_AGENT'),
            "SourceIp": '127.0.0.1' if request_metadata is None else request_metadata.get('REMOTE_ADDR'),
            "TargetHost": 'localhost' if request_metadata is None else request_metadata.get('HTTP_HOST'),
            "EradResponsePayload": output
        }

        Auditor().record(
            "Account Created: " + json.dumps(audit_message),
            group.ID,
            account_model.Email,
            account_model.Account_ID
        )

        return output

    #
    # HS20 - Profile Generator
    #
    ProfilesFolder = '/tmp/profiles/'

    def hs20_profile_setup ( self ):
        if not os.path.exists(os.path.dirname(self.ProfilesFolder)):
            os.makedirs(os.path.dirname(self.ProfilesFolder))

    def _garbage_collector_s3(self):
        """Deletes old profiles from the s3 tempfileserv bucket."""
        s3 = boto.s3.connect_to_region('us-west-2', aws_access_key_id='AKIAII77OKUOZC6TPKLQ',
                                                    aws_secret_access_key='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',
                                                    calling_format=boto.s3.connection.OrdinaryCallingFormat())
        bucket = s3.get_bucket('tempfileserv', validate=False)

        for key in bucket:
            utc_modified = datetime.datetime.strptime(key.last_modified, "%Y-%m-%dT%H:%M:%S.%fZ")
            utc_now = datetime.datetime.utcnow()

            age = utc_now - utc_modified
            if age.total_seconds() > self.s3_gc_max_age:
                bucket.delete_key(key)

    # garbage collector deletes old profiles
    def garbage_collector(self):
        """Calls methods to delete old profiles locally and from the s3 bucket"""
        self._garbage_collector_local()
        try:
            self._garbage_collector_s3()
        except Exception as e:
            msg = 'Check the aws credentials and bucket name'
            raise EradConfigurationException(msg)

    def _garbage_collector_local ( self ):
        """Deletes old profiles locally"""
        self.hs20_profile_setup()
        dir_to_search = self.ProfilesFolder
        for dirpath, dirnames, filenames in os.walk(dir_to_search):
            for file in filenames:
                curpath = os.path.join(dirpath, file)
                file_modified = datetime.datetime.fromtimestamp(os.path.getmtime(curpath))
                if datetime.datetime.now() - file_modified > datetime.timedelta(minutes=2):
                    os.remove(curpath)

    def profile_download ( self, fields ):
        self.hs20_profile_setup()
        # run gargabe collector
        self.garbage_collector()

        filename = self.expect_parameter( fields, 'file' )

        template = ""
        # downloading the profile from S3
        s3 = boto.s3.connect_to_region('us-west-2', aws_access_key_id="AKIAII77OKUOZC6TPKLQ", aws_secret_access_key="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", calling_format=boto.s3.connection.OrdinaryCallingFormat())
        bucket = s3.get_bucket('tempfileserv', validate=False)

        key = bucket.get_key(filename)
        # Check if key exist in s3
        if not key:
            raise EradAccessException("The requested file does not exist.")
        key.get_contents_to_filename(self.ProfilesFolder+filename)

        try:
            with open(self.ProfilesFolder+filename, 'rb') as file_:
                template = file_.read()
        except Exception as e:
            raise EradAccessException( "File not found." )

        si = io.BytesIO()
        si.write(template)
        return si.getvalue()

# Deprecated, Samsung now uses the standard Android profiles
    # returns the profle
#     def samsung_profile_create ( self, fields ):
#         self.hs20_profile_setup()
#         username = self.expect_parameter( fields, 'username' )
#         password = self.expect_parameter( fields, 'password' )
#         domain = self.expect_optional_parameter( fields, 'domain', "enterpriseauth.com" )
#
#         template = """
# cred={
#     username=\""""+username+"""\"
#     password=\""""+password+"""\"
#     realm=\""""+domain+"""\"
#     domain=\""""+domain+"""\"
#     eap=TTLS
#     phase2="auth=MSCHAPv2"
# }
#         """
#
#         filename = str(int(time.time()))+''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))+'.conf'
#         with open(self.ProfilesFolder+filename, 'w') as file_:
#             file_.write(template)
#
#         metadata = {
#             "domain": domain
#         }
#         # Save metadata to supplicant
#         self.store_metadata(fields, username, **metadata)
#
#         # uploading the profile to S3
#         s3 = boto.s3.connect_to_region('us-west-2', aws_access_key_id="AKIAII77OKUOZC6TPKLQ", aws_secret_access_key="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", calling_format=boto.s3.connection.OrdinaryCallingFormat())
#         key = s3.get_bucket('tempfileserv', validate=False).new_key(filename)
#         key.set_contents_from_filename(self.ProfilesFolder+filename)
#         key.set_acl('public-read')
#
#         return {'Url': 'https://api.enterpriseauth.com/erad/public/profile/download?file='+filename}

    def store_metadata(self, fields, username, **metadata):
        Group_ID = self.expect_parameter(fields, 'Group_ID')
        Account_ID = self.expect_parameter(fields, 'Account_ID')

        global_sup = Erad_Supplicant()
        global_sup.Group_ID = Group_ID
        global_sup.Account_ID = Account_ID
        global_sup.Username = username
        global_sup.refresh(consistent_read=True)

        data = global_sup.CustomJsonData

        if data is None or data == "":
            data = {}
        else:
            try:
                data = json.loads(data)
            except Exception as err:
                logger.exception(err)
                data = {}

        data.update(metadata)
        global_sup.CustomJsonData = json.dumps(data)
        global_sup.save()

    def get_android_rcois(self, rcoi_list):
        if isinstance(rcoi_list, list) and len(rcoi_list) > 0:
            filtered_list = list(filter(lambda rcoi: rcoi, rcoi_list))
            return ",".join(filtered_list) if len(filtered_list) > 0 else False
        return False

    def android_profile_create ( self, fields ):
        self.hs20_profile_setup()

        cacert = self.expect_parameter(fields, 'cacert')

        username = self.expect_parameter(fields, 'username')
        password = self.expect_parameter(fields, 'password')
        is_android_gte_11 = self.expect_optional_parameter_not_none(fields, 'is_android_gte_11', False)
        domain = self.expect_optional_parameter_not_none(fields, 'domain', "enterpriseauth.com")

        trusted_AAA_server_name = self.expect_optional_parameter_not_none(fields, 'trusted_AAA_server_name', "enterpriseauth.com")
        nairealm = self.expect_optional_parameter_not_none(fields, 'nairealm', domain)
        rcoi = self.expect_optional_parameter_not_none(fields, 'rcoi', None)
        rcoi = self.get_android_rcois(rcoi)
        display_name = self.expect_optional_parameter_not_none(fields, 'display_name', domain)
        friendly_name = self.expect_optional_parameter_not_none(fields, 'friendly_name', domain)
        account = self.expect_optional_parameter_not_none(fields, 'account', None)
        rcoi_node = """<Node>
          <NodeName>RoamingConsortiumOI</NodeName>
          <Value>"""+ rcoi + """</Value>
        </Node>""" if rcoi else ""

        xml_data = """
<MgmtTree xmlns="syncml:dmddf1.2">
  <VerDTD>1.2</VerDTD>
  <Node>
    <NodeName>PerProviderSubscription</NodeName>
    <RTProperties>
      <Type>
        <DDFName>urn:wfa:mo:hotspot2dot0-perprovidersubscription:1.0</DDFName>
      </Type>
    </RTProperties>
    <Node>
      <NodeName>X1</NodeName>
      <Node>
        <NodeName>HomeSP</NodeName>
        <Node>
          <NodeName>FQDN</NodeName>
          <Value>"""+nairealm+"""</Value>
        </Node>
        <Node>
          <NodeName>FriendlyName</NodeName>
          <Value>"""+friendly_name+"""</Value>
        </Node> 
        """ + rcoi_node + """
      </Node>
      <Node>
        <NodeName>Credential</NodeName>
        <Node>
          <NodeName>UsernamePassword</NodeName>
          <Node>
            <NodeName>EAPMethod</NodeName>
            <Node>
              <NodeName>InnerMethod</NodeName>
              <Value>MS-CHAP-V2</Value>
            </Node>
            <Node>
              <NodeName>EAPType</NodeName>
              <Value>21</Value>
            </Node>
          </Node>
          <Node>
            <NodeName>MachineManaged</NodeName>
            <Value>true</Value>
          </Node>
          <Node>
            <NodeName>Username</NodeName>
            <Value>"""+username+"""</Value>
          </Node>
          <Node>
            <NodeName>Password</NodeName>
            <Value>"""+base64.b64encode(password.encode()).decode()+"""</Value>
          </Node>
        </Node>
        <Node>
          <NodeName>Realm</NodeName>
          <Value>"""+nairealm+"""</Value>
        </Node>
        <Node>
          <NodeName>CreationDate</NodeName>
          <Value>2015-11-24T10:56:17Z</Value>
        </Node>
      </Node>
      <Node>
        <NodeName>Extension</NodeName>
        <Node>
            <NodeName>Android</NodeName>
            <Node>
                <NodeName>AAAServerTrustedNames</NodeName>
                <Node>
                    <NodeName>FQDN</NodeName>
                    <Value>"""+trusted_AAA_server_name+"""</Value>
                </Node>
            </Node>
        </Node>
      </Node>
    </Node>
  </Node>
</MgmtTree>
        """
        template_lt_11 = """Content-Type: multipart/mixed; boundary={boundary}
Content-Transfer-Encoding: base64

--{boundary}
Content-Type: application/x-passpoint-profile
Content-Transfer-Encoding: base64

%s--{boundary}
Content-Type: application/x-x509-ca-cert
Content-Transfer-Encoding: base64

%s
--{boundary}--
"""

        template_gte_11 = """Content-Type: multipart/mixed; boundary={boundary}
Content-Transfer-Encoding: base64

--{boundary}
Content-Type: application/x-passpoint-profile
Content-Transfer-Encoding: base64

%s
--{boundary}--
"""

        if is_android_gte_11 is True:
            template = template_gte_11 % (base64.b64encode(xml_data.encode()).decode())
        else:
            template = template_lt_11 % (base64.b64encode(xml_data.encode()).decode(), cacert)

        template = base64.encodebytes(template.encode()).decode()
        filename = str(int(time.time()))+''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))+'.config'
        with open(self.ProfilesFolder+filename, 'w') as file_:
            file_.write(template)

        # Create metadata
        metadata = {
            "rcoi": rcoi,
            "trusted_AAA_server_name": trusted_AAA_server_name,
            "nairealm": nairealm,
            "friendly_name": friendly_name,
            "is_android_gte_11": is_android_gte_11
        }

        # Save metadata to supplicant
        self.store_metadata(fields, username, **metadata)

        # uploading the profile to S3
        s3 = boto.s3.connect_to_region(
            'us-west-2',
            aws_access_key_id="AKIAII77OKUOZC6TPKLQ",
            aws_secret_access_key="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",
            calling_format=boto.s3.connection.OrdinaryCallingFormat()
        )

        key = s3.get_bucket('tempfileserv', validate=False).new_key(filename)
        key.set_contents_from_filename(self.ProfilesFolder+filename)
        key.set_acl('public-read')

        return {'Url': 'https://api.enterpriseauth.com/erad/public/profile/download?vendor=android&file='+filename}

    def upload_file_to_s3(self, filename, key):
        s3 = boto.s3.connect_to_region(
            'us-west-2',
            aws_access_key_id="AKIAII77OKUOZC6TPKLQ",
            aws_secret_access_key="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",
            calling_format=boto.s3.connection.OrdinaryCallingFormat(),
        )

        key = s3.get_bucket('tempfileserv', validate=False).new_key(key)
        key.set_contents_from_filename(filename)
        key.set_acl('public-read')

    def android_profile_create_tls(self, fields):
        self.hs20_profile_setup()
        cacert = self.expect_parameter(fields, 'cacert')
        domain = self.expect_optional_parameter_not_none(fields, 'domain', "enterpriseauth.com")
        sha256_fingerprint = self.expect_parameter(fields, 'Sha256-fingerprint')
        clientCert = self.expect_parameter(fields, 'clientCert')
        username = self.expect_parameter(fields, "username")

        sha256_fingerprint = sha256_fingerprint.replace(':', '')
        utc_timestamp = datetime.datetime.utcnow().isoformat().rsplit('.')[0] + 'Z'

        is_android_gte_11 = self.expect_optional_parameter_not_none(fields, 'is_android_gte_11', False)

        trusted_AAA_server_name = self.expect_optional_parameter_not_none(fields, 'trusted_AAA_server_name',
                                                                 "enterpriseauth.com")
        nairealm = self.expect_optional_parameter_not_none(fields, 'nairealm', domain)
        rcoi = self.expect_optional_parameter_not_none(fields, 'rcoi', None)
        rcoi = self.get_android_rcois(rcoi)
        display_name = self.expect_optional_parameter_not_none(fields, 'display_name', domain)
        friendly_name = self.expect_optional_parameter_not_none(fields, 'friendly_name', domain)
        self.expect_optional_parameter_not_none(fields, 'account', None)
        rcoi_node = """<Node>
          <NodeName>RoamingConsortiumOI</NodeName>
          <Value>"""+ rcoi + """</Value>
        </Node>""" if rcoi else ""

        xml_data = """
<MgmtTree xmlns="syncml:dmddf1.2">
  <VerDTD>1.2</VerDTD>
  <Node>
    <NodeName>PerProviderSubscription</NodeName>
    <RTProperties>
      <Type>
        <DDFName>urn:wfa:mo:hotspot2dot0-perprovidersubscription:1.0</DDFName>
      </Type>
    </RTProperties>
    <Node>
      <NodeName>X1</NodeName>
      <Node>
        <NodeName>HomeSP</NodeName>
        <Node>
          <NodeName>FQDN</NodeName>
          <Value>"""+nairealm+"""</Value>
        </Node>
        <Node>
          <NodeName>FriendlyName</NodeName>
          <Value>"""+friendly_name+"""</Value>
        </Node>
        """ + rcoi_node + """
      </Node>
      <Node>
        <NodeName>Credential</NodeName>
        <Node>
          <NodeName>DigitalCertificate</NodeName>
          <Node>
            <NodeName>CertificateType</NodeName>
            <Value>x509v3</Value>
          </Node>
          <Node>
            <NodeName>CertSHA256Fingerprint</NodeName>
            <Value>""" + sha256_fingerprint + """</Value>
          </Node>
        </Node>
        <Node>
          <NodeName>Realm</NodeName>
          <Value>"""+nairealm+"""</Value>
        </Node>
        <Node>
          <NodeName>CreationDate</NodeName>
          <Value>""" + utc_timestamp + """</Value>
        </Node>
      </Node>
      <Node>
        <NodeName>Extension</NodeName>
        <Node>
            <NodeName>Android</NodeName>
            <Node>
                <NodeName>AAAServerTrustedNames</NodeName>
                <Node>
                    <NodeName>FQDN</NodeName>
                    <Value>"""+trusted_AAA_server_name+"""</Value>
                </Node>
            </Node>
        </Node>
      </Node>
    </Node>
  </Node>
</MgmtTree>
        """

        template_lt_11 = """Content-Type: multipart/mixed; boundary={boundary}
Content-Transfer-Encoding: base64

--{boundary}
Content-Type: application/x-passpoint-profile
Content-Transfer-Encoding: base64

%s
--{boundary}
Content-Type: application/x-x509-ca-cert
Content-Transfer-Encoding: base64

%s
--{boundary}
Content-Type: application/x-pkcs12
Content-Transfer-Encoding: base64

%s
--{boundary}--
"""
        template_gte_11 = """Content-Type: multipart/mixed; boundary={boundary}
Content-Transfer-Encoding: base64

--{boundary}
Content-Type: application/x-passpoint-profile
Content-Transfer-Encoding: base64

%s
--{boundary}
Content-Type: application/x-pkcs12
Content-Transfer-Encoding: base64

%s
--{boundary}--
"""
        if is_android_gte_11 is True:
            template = template_gte_11 % (base64.b64encode(xml_data.encode()).decode(), clientCert)
        else:
            template = template_lt_11 % (base64.b64encode(xml_data.encode()).decode(), cacert, clientCert)

        template = base64.encodebytes(template.encode()).decode()

        filename = str(int(time.time()))+''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))+'.config'
        with open(self.ProfilesFolder+filename, 'w') as file_:
            file_.write(template)

        metadata = {
            "rcoi": rcoi,
            "trusted_AAA_server_name": trusted_AAA_server_name,
            "nairealm": nairealm,
            "friendly_name": friendly_name,
            "is_android_gte_11": is_android_gte_11
        }

        # Save metadata to supplicant
        self.store_metadata(fields, username, **metadata)

        # uploading the profile to S3
        s3 = boto.s3.connect_to_region('us-west-2', aws_access_key_id="AKIAII77OKUOZC6TPKLQ", aws_secret_access_key="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", calling_format=boto.s3.connection.OrdinaryCallingFormat())
        key = s3.get_bucket('tempfileserv', validate=False).new_key(filename)
        key.set_contents_from_filename(self.ProfilesFolder+filename)
        key.set_acl('public-read')

        return {'Url': 'https://api.enterpriseauth.com/erad/public/profile/download?vendor=android&file='+filename}

    def get_apple_rcois(self, rcoi_list):
        item = """<string>{rcoi}</string>"""
        
        if not isinstance(rcoi_list, list):
            return False
        filtered_list = list(filter(lambda rcoi: rcoi, rcoi_list))
        if len(filtered_list) == 0:
            return False

        rcois = ""
        for i in range(len(filtered_list)):
            rcois += item.format(rcoi=filtered_list[i])
        return rcois

    def apple_profile_create(self, fields):
        self.hs20_profile_setup()

        cacert = self.expect_optional_parameter_not_none(fields, 'cacert', None)

        # define default value
        srv_cert_cn_or_cn_wildcard = None
        if cacert is None:
            # and expect not optional param if cacert parameter not provided
            # to throw exceptions if both parameters 'cacert' and 'srv_cert_cn_or_cn_wildcard'
            # are not presented in fields
            srv_cert_cn_or_cn_wildcard = self.expect_parameter(fields, 'srv_cert_cn_or_cn_wildcard')

        domain = self.expect_optional_parameter_not_none(fields, 'domain', 'enterpriseauth.com')
        username = self.expect_parameter(fields, 'username')
        password = self.expect_parameter(fields, 'password')
        profileName = self.expect_parameter(fields, 'profileName')
        expDate = self.expect_optional_parameter_not_none(fields, 'expDate', False)
        isPasspoint = self.expect_optional_parameter_not_none(fields, 'isPasspoint', False)
        nairealm = self.expect_optional_parameter_not_none(fields, 'nairealm', 'enterpriseauth.com')
        rcoi = self.expect_optional_parameter_not_none(fields, 'rcoi', None)
        rcoi = self.get_apple_rcois(rcoi)
        display_name = self.expect_optional_parameter_not_none(fields, 'display_name', None)
        friendly_name = self.expect_optional_parameter_not_none(fields, 'friendly_name', None)
        account = self.expect_optional_parameter_not_none(fields, 'account', None)

        # PayloadIdentifier - String A reverse-DNS style identifier (com.example.myprofile, for example)
        # that identifies the profile. This string is used to determine whether a new profile should replace
        # an existing one or should be added.
        base_payload_id = '.'.join(nairealm.split('.')[::-1])
        rcoi_node = """<key>RoamingConsortiumOIs</key>
            <array>
                {rcoi}
            </array>""" if rcoi else ""

        templates = {
'passpoint' : """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>PayloadContent</key>
    <array>
        <dict>
            <key>AutoJoin</key>
            <true/>
            <key>DisplayedOperatorName</key>
            <string>{friendly_name}</string>
            <key>DomainName</key>
            <string>{nai_realm}</string>
            <key>EAPClientConfiguration</key>
            <dict>
                <key>AcceptEAPTypes</key>
                <array>
                    <integer>21</integer>
                </array>
                <key>TLSTrustedServerNames</key>
                <array>
                    <string>{tls_trusted_server_names}</string>
                </array>
                <key>OuterIdentity</key>
                <string>anonymous@{anon_identity_domain}</string>
                <key>TTLSInnerAuthentication</key>
                <string>MSCHAPv2</string>
                <key>UserName</key>
                <string>{username}</string>
                <key>UserPassword</key>
                <string>{password}</string>
            </dict>
            <key>EncryptionType</key>
            <string>WPA2</string>
            <key>IsHotspot</key>
            <true/>
            <key>NAIRealmNames</key>
            <array>
                <string>{nai_realm}</string>
            </array>
            """ + rcoi_node + """
            <key>PayloadDescription</key>
            <string>Configures Wi-Fi settings</string>
            <key>PayloadDisplayName</key>
            <string>Wi-Fi</string>
            <key>PayloadIdentifier</key>
            <string>{wifi_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.wifi.managed</string>
            <key>PayloadUUID</key>
            <string>{wifi_payload_uuid}</string>
            <key>PayloadVersion</key>
            <real>1</real>
            <key>ProxyType</key>
            <string>None</string>
            <key>ServiceProviderRoamingEnabled</key>
            <true/>
        </dict>
    </array>
    <key>PayloadDescription</key>
    <string>Description</string>
    <key>PayloadDisplayName</key>
    <string>{toplevel_payload_display_name}</string>
    <key>PayloadIdentifier</key>
    <string>{toplevel_payload_id}</string>
    <key>PayloadOrganization</key>
    <string>{payload_org}</string>
    <key>PayloadRemovalDisallowed</key>
    <false/>
    <key>PayloadType</key>
    <string>Configuration</string>
    <key>PayloadUUID</key>
    <string>{toplevel_payload_uuid}</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
    DATETAG
</dict>
</plist>""",


'not passpoint' : """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>PayloadContent</key>
    <array>
        <dict>
            <key>AutoJoin</key>
            <true/>
            <key>DisplayedOperatorName</key>
            <string>{friendly_name}</string>
            <key>EAPClientConfiguration</key>
            <dict>
                <key>AcceptEAPTypes</key>
                <array>
                    <integer>21</integer>
                </array>
                <key>TLSTrustedServerNames</key>
                <array>
                    <string>{tls_trusted_server_names}</string>
                </array>
                <key>TTLSInnerAuthentication</key>
                <string>MSCHAPv2</string>
                <key>UserName</key>
                <string>{username}</string>
                <key>UserPassword</key>
                <string>{password}</string>
            </dict>
            <key>EncryptionType</key>
            <string>WPA2</string>
            <key>IsHotspot</key>
            <true/>
            <key>NAIRealmNames</key>
            <array>
                <string>{nai_realm}</string>
            </array>
            <key>PayloadDescription</key>
            <string>Configures Wi-Fi settings</string>
            <key>PayloadDisplayName</key>
            <string>Wi-Fi</string>
            <key>PayloadIdentifier</key>
            <string>{wifi_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.wifi.managed</string>
            <key>PayloadUUID</key>
            <string>{wifi_payload_uuid}</string>
            <key>PayloadVersion</key>
            <real>1</real>
            <key>ProxyType</key>
            <string>None</string>
            <key>SSID_STR</key>
            <string>{ssid}</string>
            <key>ServiceProviderRoamingEnabled</key>
            <true/>
            <key>_UsingHotspot20</key>
            <false/>
        </dict>
    </array>
    <key>PayloadDescription</key>
    <string>Description</string>
    <key>PayloadDisplayName</key>
    <string>{toplevel_payload_display_name}</string>
    <key>PayloadIdentifier</key>
    <string>{toplevel_payload_id}</string>
    <key>PayloadOrganization</key>
    <string>{payload_org}</string>
    <key>PayloadRemovalDisallowed</key>
    <false/>
    <key>PayloadType</key>
    <string>Configuration</string>
    <key>PayloadUUID</key>
    <string>{toplevel_payload_uuid}</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
    DATETAG
</dict>
</plist>""",


'passpoint_deprecated' : """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>PayloadContent</key>
    <array>
        <dict>
            <key>AutoJoin</key>
            <true/>
            <key>DisplayedOperatorName</key>
            <string>{friendly_name}</string>
            <key>DomainName</key>
            <string>{nai_realm}</string>
            <key>EAPClientConfiguration</key>
            <dict>
                <key>AcceptEAPTypes</key>
                <array>
                    <integer>21</integer>
                </array>
                <key>OuterIdentity</key>
                <string>anonymous@{anon_identity_domain}</string>
                <key>PayloadCertificateAnchorUUID</key>
                <array>
                    <string>E149E8A8-A303-4ECB-BACA-223DD6E7546D</string>
                    <string>64133612-FFF6-4ABD-B64A-11F8B6CCE478</string>
                </array>
                <key>TLSTrustedServerNames</key>
                <array/>
                <key>TTLSInnerAuthentication</key>
                <string>MSCHAPv2</string>
                <key>UserName</key>
                <string>{username}</string>
                <key>UserPassword</key>
                <string>{password}</string>
            </dict>
            <key>EncryptionType</key>
            <string>WPA2</string>
            <key>IsHotspot</key>
            <true/>
            <key>NAIRealmNames</key>
            <array>
                <string>{nai_realm}</string>
            </array>
            """ + rcoi_node + """
            <key>PayloadDescription</key>
            <string>Configures Wi-Fi settings</string>
            <key>PayloadDisplayName</key>
            <string>Wi-Fi</string>
            <key>PayloadIdentifier</key>
            <string>{wifi_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.wifi.managed</string>
            <key>PayloadUUID</key>
            <string>{wifi_payload_uuid}</string>
            <key>PayloadVersion</key>
            <real>1</real>
            <key>ProxyType</key>
            <string>None</string>
            <key>ServiceProviderRoamingEnabled</key>
            <true/>
        </dict>
        <dict>
            <key>PayloadCertificateFileName</key>
            <string>ca.der</string>
            <key>PayloadContent</key>
            <data>
            {cacert}
            </data>
            <key>PayloadDescription</key>
            <string>Configures certificate settings.</string>
            <key>PayloadDisplayName</key>
            <string>Enterprise Authentication CA</string>
            <key>PayloadIdentifier</key>
            <string>com.apple.security.root.64133612-FFF6-4ABD-B64A-11F8B6CCE478</string>
            <key>PayloadType</key>
            <string>com.apple.security.root</string>
            <key>PayloadUUID</key>
            <string>64133612-FFF6-4ABD-B64A-11F8B6CCE478</string>
            <key>PayloadVersion</key>
            <integer>1</integer>
        </dict>
    </array>
    <key>PayloadDescription</key>
    <string>Description</string>
    <key>PayloadDisplayName</key>
    <string>{toplevel_payload_display_name}</string>
    <key>PayloadIdentifier</key>
    <string>{toplevel_payload_id}</string>
    <key>PayloadOrganization</key>
    <string>{payload_org}</string>
    <key>PayloadRemovalDisallowed</key>
    <false/>
    <key>PayloadType</key>
    <string>Configuration</string>
    <key>PayloadUUID</key>
    <string>{toplevel_payload_uuid}</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
    DATETAG
</dict>
</plist>""",


'not passpoint_deprecated' : """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>PayloadContent</key>
    <array>
        <dict>
            <key>PayloadCertificateFileName</key>
            <string>server.der</string>
            <key>PayloadContent</key>
            <data>
            {cacert}
            </data>
            <key>PayloadDescription</key>
            <string>Configures certificate settings.</string>
            <key>PayloadDisplayName</key>
            <string>{cert_payload_display_name}</string>
            <key>PayloadIdentifier</key>
            <string>{server_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.security.pkcs1</string>
            <key>PayloadUUID</key>
            <string>98072F43-ECB2-4113-A6C8-CF4F2A740292</string>
            <key>PayloadVersion</key>
            <integer>1</integer>
        </dict>
        <dict>
            <key>AutoJoin</key>
            <true/>
            <key>DisplayedOperatorName</key>
            <string>{friendly_name}</string>
            <key>EAPClientConfiguration</key>
            <dict>
                <key>AcceptEAPTypes</key>
                <array>
                    <integer>21</integer>
                </array>
                <key>PayloadCertificateAnchorUUID</key>
                <array>
                    <string>98072F43-ECB2-4113-A6C8-CF4F2A740292</string>
                </array>
                <key>TTLSInnerAuthentication</key>
                <string>MSCHAPv2</string>
                <key>UserName</key>
                <string>{username}</string>
                <key>UserPassword</key>
                <string>{password}</string>
            </dict>
            <key>EncryptionType</key>
            <string>WPA</string>
            <key>IsHotspot</key>
            <true/>
            <key>NAIRealmNames</key>
            <array>
                <string>{nai_realm}</string>
            </array>
            <key>PayloadDescription</key>
            <string>Configures Wi-Fi settings</string>
            <key>PayloadDisplayName</key>
            <string>Wi-Fi</string>
            <key>PayloadIdentifier</key>
            <string>{wifi_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.wifi.managed</string>
            <key>PayloadUUID</key>
            <string>{wifi_payload_uuid}</string>
            <key>PayloadVersion</key>
            <real>1</real>
            <key>ProxyType</key>
            <string>None</string>
            <key>SSID_STR</key>
            <string>{ssid}</string>
            <key>ServiceProviderRoamingEnabled</key>
            <true/>
            <key>_UsingHotspot20</key>
            <false/>
        </dict>
    </array>
    <key>PayloadDescription</key>
    <string>Description</string>
    <key>PayloadDisplayName</key>
    <string>{toplevel_payload_display_name}</string>
    <key>PayloadIdentifier</key>
    <string>{toplevel_payload_id}</string>
    <key>PayloadOrganization</key>
    <string>{payload_org}</string>
    <key>PayloadRemovalDisallowed</key>
    <false/>
    <key>PayloadType</key>
    <string>Configuration</string>
    <key>PayloadUUID</key>
    <string>{toplevel_payload_uuid}</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
    DATETAG
</dict>
</plist>"""
        }

        

        variables = {
            'anon_identity_domain': nairealm or domain,
            'cacert': cacert,
            'cert_payload_display_name':  friendly_name or domain,
            'nai_realm': nairealm or domain,
            'rcoi': rcoi,
            'password': password,
            'payload_org': friendly_name or profileName,
            'ssid': None,
            'tls_trusted_server_names': srv_cert_cn_or_cn_wildcard,
            'toplevel_payload_display_name': friendly_name or domain ,
            'toplevel_payload_id': base_payload_id,
            'toplevel_payload_uuid': uuid4(),
            'username': username,
            'wifi_payload_id': base_payload_id + '.wifi',
            'client_cert_payload_id': base_payload_id + '.client.p12',
            'server_payload_id': base_payload_id + '.server.der',
            'wifi_payload_uuid': uuid4(),
            'display_name': display_name or domain,
            'friendly_name': friendly_name or domain,
        }


        payload_uuid = uuid4()
        wifi_payload_uuid = uuid4()
        payload_identifier = profileName # determine if profile will be replaced or added as new
        if isPasspoint:
            if srv_cert_cn_or_cn_wildcard is not None:
                template = templates['passpoint'].format(**variables)
            elif cacert is not None:
                template =  templates['passpoint_deprecated'].format(**variables)
        else:
            variables['ssid'] = self.expect_parameter( fields, 'ssid' )
            if srv_cert_cn_or_cn_wildcard is not None:
                template = templates['not passpoint'].format(**variables)
            elif cacert is not None:
                template = templates['not passpoint_deprecated'].format(**variables)

        # check if expDate is set - if so then add removal date
        if expDate:
            datetag = "<key>RemovalDate</key><date>%s</date>" % datetime.datetime.strptime(expDate, '%Y%m%d').strftime("%Y-%m-%dT%H:%M:%SZ")
            template = template.replace('DATETAG', datetag)
        else:
            template = template.replace('DATETAG', '')

        filename = str(int(time.time()))+''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))+'.mobileconfig'
        with open(self.ProfilesFolder+filename, 'w') as file_:
            file_.write(template)
        # signing the profile

        full_path_cert, full_path_key, full_path_ca = self.get_sing_cert_path(account, 'ios')

        proc = Popen(['/usr/bin/openssl',
                                'smime',
                                '-sign',
                                '-in', os.path.join(self.ProfilesFolder, filename),
                                '-out', os.path.join(self.ProfilesFolder, "signed_"+filename),
                                '-signer', full_path_cert,
                                '-certfile', full_path_ca,
                                '-outform', 'der',
                                '-nodetach',
                            ],
                            stdout=PIPE,
                            stderr=STDOUT)
        (stdoutdata, stderrdata) = proc.communicate()
        if proc.returncode != 0:
            logger.error('Can not sign apple profile:\n {0}'.format(stdoutdata))
            raise EradGeneralException('Can not sign apple profile')

        metadata = {
            "domain": domain,
            "rcoi": rcoi,
            "display_name": display_name,
            "nairealm": nairealm,
            "friendly_name": friendly_name,
            "profileName": profileName,
            "isPasspoint": isPasspoint
        }

        # Save metadata to supplicant
        self.store_metadata(fields, username, **metadata)

        # uploading the profile to S3
        s3 = boto.s3.connect_to_region('us-west-2', aws_access_key_id="AKIAII77OKUOZC6TPKLQ", aws_secret_access_key="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", calling_format=boto.s3.connection.OrdinaryCallingFormat())
        key = s3.get_bucket('tempfileserv', validate=False).new_key("signed_"+filename)
        key.set_contents_from_filename(self.ProfilesFolder+"signed_"+filename)
        key.set_acl('public-read')


        return {'Url': 'https://api.enterpriseauth.com/erad/public/profile/download?vendor=apple&file='+"signed_"+filename}

    def apple_profile_create_tls(self, fields):
        self.hs20_profile_setup()

        cacert = self.expect_optional_parameter_not_none(fields, 'cacert', None)
        username = self.expect_parameter(fields, "username")


        # define default value
        srv_cert_cn_or_cn_wildcard = None
        if cacert is None:
            # and expect not optional param if cacert parameter not provided
            # to throw exceptions if both parameters 'cacert' and 'srv_cert_cn_or_cn_wildcard'
            # are not presented in fields
            srv_cert_cn_or_cn_wildcard = self.expect_parameter(fields, 'srv_cert_cn_or_cn_wildcard')

        clientCert = self.expect_parameter(fields, 'clientCert')
        account = self.expect_optional_parameter_not_none(fields, 'account', None)
        domain = self.expect_optional_parameter_not_none(fields, 'domain', 'enterpriseauth.com')
        profileName = self.expect_parameter(fields, 'profileName')
        expDate = self.expect_optional_parameter_not_none(fields, 'expDate', False)
        is_not_passpoint = self.expect_optional_parameter_not_none(fields, 'is_not_passpoint', False)
        nairealm = self.expect_optional_parameter_not_none(fields, 'nairealm', 'enterpriseauth.com')
        rcoi = self.expect_optional_parameter_not_none(fields, 'rcoi', None)
        rcoi = self.get_apple_rcois(rcoi)
        display_name = self.expect_optional_parameter_not_none(fields, 'display_name', None)
        friendly_name = self.expect_optional_parameter_not_none(fields, 'friendly_name', None)
        rcoi_node = """<key>RoamingConsortiumOIs</key>
            <array>
                {rcoi}
            </array>""" if rcoi else ""
        # PayloadIdentifier - String A reverse-DNS style identifier (com.example.myprofile, for example)
        # that identifies the profile. This string is used to determine whether a new profile should replace
        # an existing one or should be added.
        base_payload_id = '.'.join(nairealm.split('.')[::-1])





        templates = {
'default': """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>PayloadContent</key>
    <array>
        <dict>
            <key>AutoJoin</key>
            <true/>
            <key>DisplayedOperatorName</key>
            <string>{friendly_name}</string>
            <key>DomainName</key>
            <string>{nai_realm}</string>
            <key>EAPClientConfiguration</key>
            <dict>
                <key>AcceptEAPTypes</key>
                <array>
                    <integer>13</integer>
                </array>
                <key>OuterIdentity</key>
                <string>anonymous@{anon_identity_domain}</string>
                <key>TLSTrustedServerNames</key>
                <array>
                    <string>{tls_trusted_server_names}</string>
                </array>
            </dict>
            <key>EncryptionType</key>
            <string>WPA2</string>
            <key>IsHotspot</key>
            <true/>
            <key>PayloadCertificateUUID</key>
            <string>5D26FE45-A8B1-4F03-9D12-12E712BA0CBE</string>
            <key>NAIRealmNames</key>
            <array>
                <string>{nai_realm}</string>
            </array>
            """ + rcoi_node + """
            <key>PayloadDescription</key>
            <string>Configures Wi-Fi settings</string>
            <key>PayloadDisplayName</key>
            <string>Wi-Fi</string>
            <key>PayloadIdentifier</key>
            <string>{wifi_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.wifi.managed</string>
            <key>PayloadUUID</key>
            <string>{wifi_payload_uuid}</string>
            <key>PayloadVersion</key>
            <real>1</real>
            <key>ProxyType</key>
            <string>None</string>
            <key>ServiceProviderRoamingEnabled</key>
            <true/>
        </dict>
        <dict>
            <key>Password</key>
            <string>WYPCMS6T</string>
            <key>PayloadCertificateFileName</key>
            <string>client.p12</string>
            <key>PayloadContent</key>
            <data>
            {client_cert}
            </data>
            <key>PayloadDescription</key>
            <string>Configures certificate settings.</string>
            <key>PayloadDisplayName</key>
            <string>client.p12</string>
            <key>PayloadIdentifier</key>
            <string>{client_cert_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.security.pkcs12</string>
            <key>PayloadUUID</key>
            <string>5D26FE45-A8B1-4F03-9D12-12E712BA0CBE</string>
            <key>PayloadVersion</key>
            <integer>1</integer>
        </dict>
    </array>
    <key>PayloadDescription</key>
    <string>Description</string>
    <key>PayloadDisplayName</key>
    <string>{toplevel_payload_display_name}</string>
    <key>PayloadIdentifier</key>
    <string>{toplevel_payload_id}</string>
    <key>PayloadOrganization</key>
    <string>{payload_org}</string>
    <key>PayloadRemovalDisallowed</key>
    <false/>
    <key>PayloadType</key>
    <string>Configuration</string>
    <key>PayloadUUID</key>
    <string>{toplevel_payload_uuid}</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
    DATETAG
</dict>
</plist> """,


'not passpoint': """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>PayloadContent</key>
    <array>
        <dict>
            <key>AutoJoin</key>
            <true/>
            <key>DisplayedOperatorName</key>
            <string>{friendly_name}</string>
            <key>DomainName</key>
            <string>{nai_realm}</string>
            <key>EAPClientConfiguration</key>
            <dict>
                <key>AcceptEAPTypes</key>
                <array>
                    <integer>13</integer>
                </array>
                <key>OuterIdentity</key>
                <string>anonymous@{anon_identity_domain}</string>
                <key>TLSTrustedServerNames</key>
                <array>
                    <string>{tls_trusted_server_names}</string>
                </array>
            </dict>
            <key>EncryptionType</key>
            <string>WPA2</string>
            <key>IsHotspot</key>
            <true/>
            <key>PayloadCertificateUUID</key>
            <string>5D26FE45-A8B1-4F03-9D12-12E712BA0CBE</string>
            <key>NAIRealmNames</key>
            <array>
                <string>{nai_realm}</string>
            </array>
            <key>PayloadDescription</key>
            <string>Configures Wi-Fi settings</string>
            <key>PayloadDisplayName</key>
            <string>Wi-Fi</string>
            <key>PayloadIdentifier</key>
            <string>{wifi_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.wifi.managed</string>
            <key>PayloadUUID</key>
            <string>E3A08CD3-D1A2-47F3-A71D-F673964996E5</string>
            <key>PayloadVersion</key>
            <real>1</real>
            <key>ProxyType</key>
            <string>None</string>
            <key>SSID_STR</key>
            <string>{ssid}</string>
            <key>ServiceProviderRoamingEnabled</key>
            <true/>
            <key>_UsingHotspot20</key>
            <false/>
        </dict>
        <dict>
            <key>Password</key>
            <string>WYPCMS6T</string>
            <key>PayloadCertificateFileName</key>
            <string>client.p12</string>
            <key>PayloadContent</key>
            <data>
            {client_cert}
            </data>
            <key>PayloadDescription</key>
            <string>Configures certificate settings.</string>
            <key>PayloadDisplayName</key>
            <string>client.p12</string>
            <key>PayloadIdentifier</key>
            <string>{client_cert_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.security.pkcs12</string>
            <key>PayloadUUID</key>
            <string>5D26FE45-A8B1-4F03-9D12-12E712BA0CBE</string>
            <key>PayloadVersion</key>
            <integer>1</integer>
        </dict>
    </array>
    <key>PayloadDescription</key>
    <string>Description</string>
    <key>PayloadDisplayName</key>
    <string>{toplevel_payload_display_name}</string>
    <key>PayloadIdentifier</key>
    <string>{toplevel_payload_id}</string>
    <key>PayloadOrganization</key>
    <string>{payload_org}</string>
    <key>PayloadRemovalDisallowed</key>
    <false/>
    <key>PayloadType</key>
    <string>Configuration</string>
    <key>PayloadUUID</key>
    <string>{toplevel_payload_uuid}</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
    DATETAG
</dict>
</plist> """,


'depreacated' : """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>PayloadContent</key>
    <array>
        <dict>
            <key>AutoJoin</key>
            <true/>
            <key>DisplayedOperatorName</key>
            <string>{friendly_name}</string>
            <key>DomainName</key>
            <string>{nai_realm}</string>
            <key>EAPClientConfiguration</key>
            <dict>
                <key>AcceptEAPTypes</key>
                <array>
                    <integer>13</integer>
                </array>
                <key>OuterIdentity</key>
                <string>anonymous@{anon_identity_domain}</string>
                <key>PayloadCertificateAnchorUUID</key>
                <array>
                    <string>E149E8A8-A303-4ECB-BACA-223DD6E7546D</string>
                    <string>64133612-FFF6-4ABD-B64A-11F8B6CCE478</string>
                    <string>AEE4EA4C-2FF4-4401-A292-E04CBD9F6055</string>
                </array>
                <key>TLSTrustedServerNames</key>
                <array/>
            </dict>
            <key>EncryptionType</key>
            <string>WPA2</string>
            <key>IsHotspot</key>
            <true/>
            <key>PayloadCertificateUUID</key>
            <string>5D26FE45-A8B1-4F03-9D12-12E712BA0CBE</string>
            <key>NAIRealmNames</key>
            <array>
                <string>{nai_realm}</string>
            </array>
            """ + rcoi_node + """
            <key>PayloadDescription</key>
            <string>Configures Wi-Fi settings</string>
            <key>PayloadDisplayName</key>
            <string>Wi-Fi</string>
            <key>PayloadIdentifier</key>
            <string>{wifi_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.wifi.managed</string>
            <key>PayloadUUID</key>
            <string>{wifi_payload_uuid}</string>
            <key>PayloadVersion</key>
            <real>1</real>
            <key>ProxyType</key>
            <string>None</string>
            <key>ServiceProviderRoamingEnabled</key>
            <true/>
        </dict>
        <dict>
            <key>Password</key>
            <string>WYPCMS6T</string>
            <key>PayloadCertificateFileName</key>
            <string>client.p12</string>
            <key>PayloadContent</key>
            <data>
            {client_cert}
            </data>
            <key>PayloadDescription</key>
            <string>Configures certificate settings.</string>
            <key>PayloadDisplayName</key>
            <string>client.p12</string>
            <key>PayloadIdentifier</key>
            <string>{client_cert_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.security.pkcs12</string>
            <key>PayloadUUID</key>
            <string>5D26FE45-A8B1-4F03-9D12-12E712BA0CBE</string>
            <key>PayloadVersion</key>
            <integer>1</integer>
        </dict>
        <dict>
            <key>PayloadCertificateFileName</key>
            <string>ca.der</string>
            <key>PayloadContent</key>
            <data>
            {cacert}
            </data>
            <key>PayloadDescription</key>
            <string>Configures certificate settings.</string>
            <key>PayloadDisplayName</key>
            <string>Enterprise Authentication CA</string>
            <key>PayloadIdentifier</key>
            <string>com.apple.security.root.64133612-FFF6-4ABD-B64A-11F8B6CCE478</string>
            <key>PayloadType</key>
            <string>com.apple.security.root</string>
            <key>PayloadUUID</key>
            <string>64133612-FFF6-4ABD-B64A-11F8B6CCE478</string>
            <key>PayloadVersion</key>
            <integer>1</integer>
        </dict>
    </array>
    <key>PayloadDescription</key>
    <string>Description</string>
    <key>PayloadDisplayName</key>
    <string>{toplevel_payload_display_name}</string>
    <key>PayloadIdentifier</key>
    <string>{toplevel_payload_id}</string>
    <key>PayloadOrganization</key>
    <string>{payload_org}</string>
    <key>PayloadRemovalDisallowed</key>
    <false/>
    <key>PayloadType</key>
    <string>Configuration</string>
    <key>PayloadUUID</key>
    <string>{toplevel_payload_uuid}</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
    DATETAG
</dict>
</plist>""",


'depreacated not passpoint' : """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>PayloadContent</key>
    <array>
        <dict>
            <key>AutoJoin</key>
            <true/>
            <key>DisplayedOperatorName</key>
            <string>{friendly_name}</string>
            <key>DomainName</key>
            <string>{nai_realm}</string>
            <key>EAPClientConfiguration</key>
            <dict>
                <key>AcceptEAPTypes</key>
                <array>
                    <integer>13</integer>
                </array>
                <key>OuterIdentity</key>
                <string>anonymous@{anon_identity_domain}</string>
                <key>PayloadCertificateAnchorUUID</key>
                <array>
                    <string>E149E8A8-A303-4ECB-BACA-223DD6E7546D</string>
                    <string>64133612-FFF6-4ABD-B64A-11F8B6CCE478</string>
                    <string>AEE4EA4C-2FF4-4401-A292-E04CBD9F6055</string>
                </array>
                <key>TLSTrustedServerNames</key>
                <array/>
            </dict>
            <key>EncryptionType</key>
            <string>WPA2</string>
            <key>IsHotspot</key>
            <true/>
            <key>PayloadCertificateUUID</key>
            <string>5D26FE45-A8B1-4F03-9D12-12E712BA0CBE</string>
            <key>NAIRealmNames</key>
            <array>
                <string>{nai_realm}</string>
            </array>
            <key>PayloadDescription</key>
            <string>Configures Wi-Fi settings</string>
            <key>PayloadDisplayName</key>
            <string>Wi-Fi</string>
            <key>PayloadIdentifier</key>
            <string>{wifi_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.wifi.managed</string>
            <key>PayloadUUID</key>
            <string>{wifi_payload_uuid}</string>
            <key>PayloadVersion</key>
            <real>1</real>
            <key>ProxyType</key>
            <string>None</string>
            <key>SSID_STR</key>
            <string>{ssid}</string>
            <key>ServiceProviderRoamingEnabled</key>
            <true/>
            <key>_UsingHotspot20</key>
            <false/>
        </dict>
        <dict>
            <key>Password</key>
            <string>WYPCMS6T</string>
            <key>PayloadCertificateFileName</key>
            <string>client.p12</string>
            <key>PayloadContent</key>
            <data>
            {client_cert}
            </data>
            <key>PayloadDescription</key>
            <string>Configures certificate settings.</string>
            <key>PayloadDisplayName</key>
            <string>client.p12</string>
            <key>PayloadIdentifier</key>
            <string>{client_cert_payload_id}</string>
            <key>PayloadType</key>
            <string>com.apple.security.pkcs12</string>
            <key>PayloadUUID</key>
            <string>5D26FE45-A8B1-4F03-9D12-12E712BA0CBE</string>
            <key>PayloadVersion</key>
            <integer>1</integer>
        </dict>
        <dict>
            <key>PayloadCertificateFileName</key>
            <string>ca.der</string>
            <key>PayloadContent</key>
            <data>
            {cacert}
            </data>
            <key>PayloadDescription</key>
            <string>Configures certificate settings.</string>
            <key>PayloadDisplayName</key>
            <string>Enterprise Authentication CA</string>
            <key>PayloadIdentifier</key>
            <string>com.apple.security.root.64133612-FFF6-4ABD-B64A-11F8B6CCE478</string>
            <key>PayloadType</key>
            <string>com.apple.security.root</string>
            <key>PayloadUUID</key>
            <string>64133612-FFF6-4ABD-B64A-11F8B6CCE478</string>
            <key>PayloadVersion</key>
            <integer>1</integer>
        </dict>
    </array>
    <key>PayloadDescription</key>
    <string>Description</string>
    <key>PayloadDisplayName</key>
    <string>{toplevel_payload_display_name}</string>
    <key>PayloadIdentifier</key>
    <string>{toplevel_payload_id}</string>
    <key>PayloadOrganization</key>
    <string>{payload_org}</string>
    <key>PayloadRemovalDisallowed</key>
    <false/>
    <key>PayloadType</key>
    <string>Configuration</string>
    <key>PayloadUUID</key>
    <string>{toplevel_payload_uuid}</string>
    <key>PayloadVersion</key>
    <integer>1</integer>
    DATETAG
</dict>
</plist>"""
}

        variables = {
            'anon_identity_domain': nairealm or domain,
            'cacert': cacert,
            'cert_payload_display_name':  friendly_name or domain,
            'client_cert': clientCert,
            'nai_realm': nairealm or domain,
            'rcoi': rcoi,
            'payload_org': friendly_name or profileName,
            'ssid': None,
            'tls_trusted_server_names': srv_cert_cn_or_cn_wildcard,
            'toplevel_payload_display_name': friendly_name or domain,
            'toplevel_payload_id': base_payload_id,
            'toplevel_payload_uuid': uuid4(),
            'wifi_payload_id': base_payload_id + '.wifi',
            'client_cert_payload_id': base_payload_id + '.client.p12',
            'wifi_payload_uuid': uuid4(),
            'display_name': display_name or domain,
            'friendly_name': friendly_name or domain,

        }

        if srv_cert_cn_or_cn_wildcard is not None:
            if is_not_passpoint is True:
                variables['ssid'] = self.expect_parameter(fields, 'ssid')
                template =  templates['not passpoint'].format(**variables)
            else:
                template =  templates['default'].format(**variables)
        elif cacert is not None:
            if is_not_passpoint is False:
                template =  templates['depreacated'].format(**variables)
            else:
                variables['ssid'] = self.expect_parameter(fields, 'ssid')
                template =  templates['depreacated not passpoint'].format(**variables)

        full_path_cert, full_path_key, full_path_ca  = self.get_sing_cert_path(account, 'ios')

        # check if expDate is set - if so then add removal date
        if expDate:
            datetag = "<key>RemovalDate</key><date>%s</date>" % datetime.datetime.strptime(expDate, '%Y%m%d').strftime("%Y-%m-%dT%H:%M:%SZ")
            template = template.replace('DATETAG', datetag)
        else:
            template = template.replace('DATETAG', '')

        filename = str(int(time.time())) + ''.join(
            random.choice(string.ascii_lowercase + string.digits) for _ in range(10)) + '.mobileconfig'
        with open(self.ProfilesFolder + filename, 'w') as file_:
            file_.write(template)
        # signing the profile
        proc = Popen(['/usr/bin/openssl',
                                'smime',
                                '-sign',
                                '-in', os.path.join(self.ProfilesFolder, filename),
                                '-out', os.path.join(self.ProfilesFolder, "signed_" + filename),
                                '-signer', full_path_cert,
                                '-certfile', full_path_ca,
                                '-outform', 'der',
                                '-nodetach',
                            ],
                            stdout=PIPE,
                            stderr=STDOUT)
        (stdoutdata, stderrdata) = proc.communicate()
        if proc.returncode != 0:
            logger.error('Can not sign apple tls profile:\n {0}'.format(stdoutdata))
            raise EradGeneralException('Can not sign apple tls profile')

        metadata = {
            "domain": domain,
            "rcoi": rcoi,
            "display_name": display_name,
            "nairealm": nairealm,
            "friendly_name": friendly_name,
            "profileName": profileName,
            "is_not_passpoint": is_not_passpoint
        }

        # Save metadata to supplicant
        self.store_metadata(fields, username, **metadata)

        # uploading the profile to S3
        s3 = boto.s3.connect_to_region('us-west-2', aws_access_key_id="AKIAII77OKUOZC6TPKLQ",
                                       aws_secret_access_key="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",
                                       calling_format=boto.s3.connection.OrdinaryCallingFormat())
        key = s3.get_bucket('tempfileserv', validate=False).new_key("signed_" + filename)
        key.set_contents_from_filename(self.ProfilesFolder + "signed_" + filename)
        key.set_acl('public-read')

        return {'Url': 'https://api.enterpriseauth.com/erad/public/profile/download?vendor=apple&file=' + "signed_" + filename}

    def get_windows_rcois(self, rcoi_list):
        if not isinstance(rcoi_list, list):
            return ""
        filtered_list = list(filter(lambda rcoi: rcoi, rcoi_list))
        if len(filtered_list) == 0:
            return ""
        else:
            item = """<OUI>{rcoi}</OUI>"""
            rcois =''.join([item.format(rcoi=rcoi) for rcoi in rcoi_list])
            return '''<RoamingConsortium>
        {rcois}
    </RoamingConsortium>'''.format(rcois=rcois)

    def windows_profile_create(self, fields):
        self.hs20_profile_setup()
        ssid = self.expect_parameter(fields, 'ssid')
        profileName = self.expect_parameter(fields, 'profileName')
        username = self.expect_parameter(fields, 'username')
        password = self.expect_parameter(fields, 'password')

        # ERAD function expect_optional_parameter working in unexpected way, if value is passed and even if value is
        # None then it will be used instead of default value in expect_optional_parameter(..., devault_value),
        # Manually casting deafault values to ovoid

        is_passpoint = self.expect_optional_parameter_not_none(fields, 'isPasspoint', False)
        return_file_content = self.expect_optional_parameter_not_none(fields, 'return_file_content', True)
        nairealm = self.expect_optional_parameter_not_none(fields, 'nairealm', "enterpriseauth.com")
        domain = self.expect_optional_parameter_not_none(fields, 'domain', "enterpriseauth.com")
        rcoi = self.expect_optional_parameter_not_none(fields, 'rcoi', None)
        display_name = self.expect_optional_parameter_not_none(fields, 'display_name', domain)
        friendly_name = self.expect_optional_parameter_not_none(fields, 'friendly_name', domain)

        account = self.expect_optional_parameter_not_none(fields, "account", None)

        # one or semicolon diveded cn from server certificate
        srv_cert_cns = self.expect_optional_parameter_not_none(fields, 'srv_cert_cns',
                                                      'enterpriseauth.com;www.enterpriseauth.com')
        root_ca_hash = self.expect_optional_parameter_not_none(fields, 'root_ca_hash',
                                                      '91 c6 d6 ee 3e 8a c8 63 84 e5 48 c2 99 29 5c 75 6c 81 7b 81')

        if is_passpoint is False:
            passpoint_block = ''
        elif is_passpoint is True:
            roaming_block = self.get_windows_rcois(rcoi)


            passpoint_block = '''
<Hotspot2>
    <DomainName>{nairealm}</DomainName>
    <NAIRealm>
        <name>{nairealm}</name>
    </NAIRealm>
    {rcoi}
</Hotspot2>
'''.format(nairealm=nairealm, rcoi=roaming_block)


        profile_template = """<?xml version="1.0"?>
<CarrierProvisioning xmlns="http://www.microsoft.com/networking/CarrierControl/v1" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><Global><CarrierId>{{6aae5a9b-66ac-3d2d-a6f0-a311cd90c2b8}}</CarrierId><SubscriberId>1234567890</SubscriberId></Global><WLANProfiles><WLANProfile xmlns="http://www.microsoft.com/networking/CarrierControl/WLAN/v1">   <name>%s</name>
    <SSIDConfig>
        <SSID>
            <name>%s</name>
        </SSID>
    </SSIDConfig>
    %s
    <MSM>
      <security>
        <authEncryption>
          <authentication>WPA2</authentication>
          <encryption>AES</encryption>
          <useOneX>true</useOneX>
        </authEncryption>
        <OneX xmlns="http://www.microsoft.com/networking/OneX/v1">
          <cacheUserData>true</cacheUserData>
          <authMode>user</authMode>
          <EAPConfig>
            <EapHostConfig xmlns="http://www.microsoft.com/provisioning/EapHostConfig">
              <EapMethod>
                <Type xmlns="http://www.microsoft.com/provisioning/EapCommon">21</Type>
                <VendorId xmlns="http://www.microsoft.com/provisioning/EapCommon">0</VendorId>
                <VendorType xmlns="http://www.microsoft.com/provisioning/EapCommon">0</VendorType>
                <AuthorId xmlns="http://www.microsoft.com/provisioning/EapCommon">311</AuthorId>
              </EapMethod>
              <Config xmlns="http://www.microsoft.com/provisioning/EapHostConfig">
                <EapTtls xmlns="http://www.microsoft.com/provisioning/EapTtlsConnectionPropertiesV1">
                  <ServerValidation>
                    <TrustedRootCAHash>{root_ca_hash}</TrustedRootCAHash>
                    <ServerNames>{srv_cert_cns}</ServerNames>
                    <DisablePrompt>false</DisablePrompt>
                  </ServerValidation>
                  <Phase2Authentication>
                    <MSCHAPv2Authentication>
                      <UseWinlogonCredentials>false</UseWinlogonCredentials>
                    </MSCHAPv2Authentication>
                  </Phase2Authentication>
                  <Phase1Identity>
                    <IdentityPrivacy>true</IdentityPrivacy>
                    <AnonymousIdentity>anonymous@enterpriseauth.com</AnonymousIdentity>
                  </Phase1Identity>
                </EapTtls>
              </Config>
            </EapHostConfig>
          </EAPConfig>
        </OneX>
        <EapHostUserCredentials xmlns="http://www.microsoft.com/provisioning/EapHostUserCredentials" xmlns:baseEap="http://www.microsoft.com/provisioning/BaseEapMethodUserCredentials" xmlns:eapCommon="http://www.microsoft.com/provisioning/EapCommon">
          <EapMethod>
            <eapCommon:Type>21</eapCommon:Type>
            <eapCommon:AuthorId>311</eapCommon:AuthorId>
          </EapMethod>
          <Credentials>
            <EapTtls xmlns="http://www.microsoft.com/provisioning/EapTtlsUserPropertiesV1">
              <Username>%s</Username>
              <Password>%s</Password>
            </EapTtls>
          </Credentials>
        </EapHostUserCredentials>
      </security>
    </MSM>
</WLANProfile></WLANProfiles></CarrierProvisioning>
        """

        template = profile_template.format(
            srv_cert_cns=srv_cert_cns,
            root_ca_hash=root_ca_hash,
        )


        template = template % (friendly_name, ssid, passpoint_block, username, password)

        full_path_cert, full_path_key, full_path_ca  = self.get_sing_cert_path(account, 'windows')

        with open(full_path_cert, 'rb') as r:
            cert = r.read()

        with open(full_path_key, 'rb') as r:
            key = r.read()

        root = etree.fromstring(template)
        signed_root = XMLSigner(
                digest_algorithm='sha1',
                signature_algorithm='rsa-sha1',
                c14n_algorithm='http://www.w3.org/TR/2001/REC-xml-c14n-20010315',
                include_c14n_transform=False
            ).sign(root,
                key=key,
                passphrase=None,
                cert=cert)

        encodedRoot = etree.tostring(signed_root).decode()

        if return_file_content:
            return {'Url': encodedRoot}

        current_time = int(time.time())
        random_suffix = ''.join(random.sample(string.digits, 10))
        key = f'{current_time}{random_suffix}.xml'
        filename = f'{self.ProfilesFolder}{key}'

        with open(filename, 'w') as writer:
            writer.write(encodedRoot)

        metadata = {
            "domain": domain,
            "rcoi": rcoi,
            "display_name": display_name,
            "nairealm": nairealm,
            "friendly_name": friendly_name,
            "profileName": profileName,
            "is_passpoint": is_passpoint
        }

        # Save metadata to supplicant
        self.store_metadata(fields, username, **metadata)

        self.upload_file_to_s3(filename, key)

        url = f'https://api.enterpriseauth.com/erad/public/profile/download?vendor=windows&file={key}'
        return {'Url': url}

    def check_password(self, fields):
        password = self.expect_unicode_value(fields, 'password')
        pwned = False
        try:
            pwned = self.is_password_blacklisted(password)
        except Exception as err:
            logger.info(err)
            # If service that check password is unavailable return False to not block user operation
            pwned = False

        return {'pwned': pwned}
