#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

# ----- exceptions thrown by api -----

# The caller doesnt have access
class EradAccessException(Exception):
    pass

# The caller called us wrong
class EradMalformedException(Exception):
    pass

# user supplied data was invalid
class EradValidationException(Exception):
    pass

# Something is configured incorrectly
class EradConfigurationException(Exception):
    pass

# Access is expired
class EradExpiredException(Exception):
    pass

# something required was missing
class EradRequiredFieldException(EradMalformedException):
    pass

# something was passed, but the wrong type
class EradWrongTypeException(EradMalformedException):
    pass

# something passed is not allowed
class EradRestrictedFieldException(EradMalformedException):
    pass

class EradInvalidFileException(EradMalformedException):
    pass

class EradBlacklistedPasswordException(EradValidationException):
    pass

class EradValidationExceptionRadius(Exception):
    pass

# custom exception for expired session
class EradSessionExpiredException(Exception):
    def __init__(self, message, LogoutUrl):
        Exception.__init__(self)
        self.message = message
        self.LogoutUrl = LogoutUrl

    def to_dict(self):
        ret = dict(())
        ret['Error'] = self.message
        ret['RedirectURL'] = self.LogoutUrl
        return ret

# ----- exceptions thrown by frmod -----

class EradNotFound(Exception):
   pass

class EradServiceError(Exception):
   pass


class EradGeneralException(Exception):
    def __init__(self, message):
        super(EradGeneralException, self).__init__()
        self.message = message
