import json
import logging
import boto3

from erad import util

logger = logging.getLogger(__name__)

customers_with_external_subscriber_info = {
    "calix": {},
    "calix-staging": {},
    "spectrum": {},
    "spectrum-staging": {},
}


def get_client_info(provider, account_id, subscriber, group, realm):
    if account_id not in customers_with_external_subscriber_info:
        return

    region, client_arn = util.load_client_arns(group.ClientInfoArn)

    if region is None or client_arn is None:
        return

    if subscriber:
        requestData = {
            "account": account_id,
            "provider": provider,
            "subscriber": subscriber,
            "realm": realm
        }

        try:
            client = boto3.client('lambda', region_name=region)

            response = client.invoke(
                FunctionName=client_arn,
                Payload=json.dumps(requestData).encode('utf-8')
            )
            payload = response.get('Payload').read()

            return {"ClientInfo": json.loads(payload)}
        except Exception as e:
            logger.exception(e)

    return
