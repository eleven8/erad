import boto.sns
from boto.sts import STSConnection

from erad import util

import logging

logger = logging.getLogger(__name__)

ACCT_SNS_TOPIC = 'acct_sns_topic'


def send_log_sns(message, session_name):

    try:
        role_arn = util.erad_cfg().role_arn
        role_profile = util.erad_cfg().role_profile
        topic = getattr(util.erad_cfg(), ACCT_SNS_TOPIC)

        if topic is None:
            raise Exception(f"SNS topic `{topic}` not found in config.")

        connection = STSConnection(profile_name=role_profile)
        assumed_role_object = connection.assume_role(
            role_arn=role_arn,
            role_session_name=session_name
        )

        role_data = {
            'access_key': getattr(assumed_role_object.credentials, 'access_key'),
            'secret_key': getattr(assumed_role_object.credentials, 'secret_key'),
            'session_token': getattr(assumed_role_object.credentials, 'session_token')
        }

        conn = boto.sns.SNSConnection(
            aws_access_key_id=role_data["access_key"],
            aws_secret_access_key=role_data["secret_key"],
            security_token=role_data["session_token"]
        )

        conn.publish(topic=topic, message=message)

    except Exception as exc:
        logger.exception(exc)
