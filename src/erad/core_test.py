#!/usr/bin/python3
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import unittest


class CoreTest(unittest.TestCase):
    std_integration_key = "i_u6DMJLRFCHczrG8bJugCmfZZ3UcmJDtF"
    std_integration_key_disabled = "EradDisabledKey"

    def assert_is_superset(self, innerset, superset):
        # for arrays, they must contain the same elements
        if isinstance(innerset, list) or isinstance(innerset, tuple):
            self.assertEqual(len(innerset), len(superset),
                             "Expected array of length {0} but got array of length {1}.".format(len(innerset),
                                                                                                len(superset)))
            for i in range(0, len(innerset)):
                self.assert_is_superset(innerset[i], superset[i])
        # for dicts, the superset must contain the items in the innerset (but may have more)
        elif isinstance(innerset, dict):
            for k in innerset:
                self.assertTrue(k in superset, "Expected key '{0}' does not exist.".format(k))
                self.assert_is_superset(innerset[k], superset[k])
        # otherwise, values must be equivilent
        else:
            self.assertEqual(superset, innerset)
