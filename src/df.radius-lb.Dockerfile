FROM amazonlinux:2 as builder

RUN \
	yum -y upgrade && \
	yum -y install \
		git-core \
		gcc \
		libtalloc-devel \
		openssl \
		openssl-devel \
		make


ARG source=https://github.com/FreeRADIUS/freeradius-server.git
ARG release=v3.0.x

RUN cd /root &&\
	git clone --depth 1 --single-branch --branch ${release} ${source} && \
	cd freeradius-server && \
	./configure && \
	make && \
	make install

#
FROM amazonlinux:2

RUN \
	yum -y upgrade && \
	yum -y install \
		shadow-utils \
		libtalloc \
		libatomic

ARG RADIUSD_USER_UID=1000

RUN \
	useradd -M -u $RADIUSD_USER_UID radiusd && \
	usermod -L radiusd && \
	yum -y remove shadow-utils && \
	yum clean all && \
	mkdir /var/log/radius && \
	chmod -R 777 /var/log/radius

WORKDIR /usr
COPY --from=builder /usr/local /usr/local

RUN \
	rm -rf /usr/local/etc/raddb \
	chown -R radiusd:radiusd /usr/local/var/log/radius/

# Freeradius drop permission on start,
# and it can't open stdout if it will be started ad radiusd user
#USER radiusd
ENTRYPOINT ["/usr/local/sbin/radiusd"]
CMD ["-fl stdout"]






