#!/bin/bash

set -e

if [ $# -ne 6 ]; then
  echo "Usage: $0 <domain_cert_arn> <erad_api_domain> <s3_bucket_to_host_static_files> <route53_zone_id> <deploy_url> <stack-name>"
  echo "Example: $0 arn:aws:acm:us-east-1:306836384016:certificate/f9252fcf-f423-4478-8234-49923189e18f https://api.enterpriseauth.com admin.enterpriseauth.com ZVMUXA2RIR4UY admin.enterpriseauth.com erad-static-production"
  exit 1
fi

export AWS_PAGER=""

CERTIFICATE_FOR_DOMAIN_ARN=$1
S3BUCKETNAME=$3
HOSTED_ZONE_ID=$4
DEPLOY_URL=$5
STACK_NAME=$6

cat << EOF > .env.production
ERAD_API_PATH=$2
EOF

echo
echo "Building for production"
echo

npm install
rm -rf build && npm run build

echo
echo "Syncing build files to S3 bucket"
echo

aws s3 sync build s3://${S3BUCKETNAME} \
  --acl public-read \
  --delete \
  | tee sync.log

echo
echo "Deploying"
echo

aws cloudformation deploy  \
  --no-fail-on-empty-changeset \
  --capabilities CAPABILITY_IAM \
  --stack-name $STACK_NAME \
  --template stack.yaml \
  --parameter-overrides \
      Bucket="$S3BUCKETNAME" \
      Certificate="$CERTIFICATE_FOR_DOMAIN_ARN" \
      HostedZoneId="$HOSTED_ZONE_ID" \
      AliasDomain="$DEPLOY_URL"

echo
echo "Waiting for invalidation to complete"
echo

cloudfront_id=$(aws cloudformation describe-stacks \
  --stack-name $STACK_NAME \
  --output text \
  --query 'Stacks[0]
    .Outputs[?OutputKey == `CloudFrontID`]
    .OutputValue')

invalidation_id=$(aws cloudfront create-invalidation \
  --distribution-id "$cloudfront_id" \
  --query Invalidation.Id --output text \
  --paths '/*')

aws cloudfront wait invalidation-completed \
  --distribution-id "$cloudfront_id" \
  --id "$invalidation_id" && echo DONE || echo FAIL
