## Setup

Generate environment variables and make changes accordingly

```bash
bash create_local_config.sh
```

Install dependencies

```bash
nvm use # node 16.15.1
npm install && npm prepare
```

Recommended to run `npm run format` before committing any files

## Developing

To start a development server:

```bash
npm run dev
```

## Deployment to AWS CloudFront and S3

Make sure you have node 16.15.1 and install required dependencies:

```bash
nvm use
npm install && npm prepare
```

Make sure you have configured correct AWS credentials:

```bash
aws configure
```

Make sure you have created S3 bucket and configured it for static website:

```bash
aws s3 mb s3://<bucket_name>
aws s3 website s3://<bucket_name> --index-document index.html
```

Run deploy script as below:

```bash
bash deploy.sh <domain_cert_arn> <erad_api_domain> <s3_bucket_to_host_static_files> <route53_zone_id> <deploy_url>

# Example: bash deploy.sh arn:aws:acm:us-east-1:306836384016:certificate/f9252fcf-f423-4478-8234-49923189e18f https://api.enterpriseauth.com admin.enterpriseauth.com ZVMUXA2RIR4UY admin.enterpriseauth.com
```
