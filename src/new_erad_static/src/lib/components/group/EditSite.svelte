<script lang="ts">
  import IMask from 'imask';
  import { createEventDispatcher, getContext, onDestroy, onMount } from 'svelte';
  import { authenticators, endpoints, isEndpointGlobal, selectedGroup } from '$lib/stores/groups';
  import { isIP, isNotEmpty, isPort } from '$lib/utils/validation';
  import {
    fetchGroupAuthenticators,
    fetchGroupEndpoints,
    fetchSelectedGroup,
    updateGroup
  } from '$lib/services/groups';
  import { showSpinner } from '$lib/stores/spinner';
  import type { EditGroupPayload, RemoteAuthOptions } from '$lib/types/groups';
  import Alert from '../Alert.svelte';
  import TimezonePicker from '../TimezonePicker.svelte';
  import InputMask from '../InputMask.svelte';
  import PortProvision from './PortProvision.svelte';
  import Devices from './Devices.svelte';
  import { userSession } from '$lib/stores/auth';

  const dispatch = createEventDispatcher();
  const { getModalInstance } = getContext('edit-site');

  let submitted: boolean = false;
  let message: string = '';

  $: isSelectedGroupGlobal = $selectedGroup.ID.toLowerCase() === 'global';
  $: formEditable = $userSession?.AccountOwner === 1;
  $: siteNameEditable = formEditable && !isSelectedGroupGlobal;

  let site: EditGroupPayload = {
    ID: '',
    MaxDownloadSpeedBits: 0,
    MaxUploadSpeedBits: 0,
    Name: '',
    OverrideConnectionSpeed: false,
    OverrideVlan: false,
    RemoteServer: '',
    RemoteServerUrl: '',
    SharedSecret: '',
    TimeZone: ''
  };

  let remoteAuthOptions: RemoteAuthOptions[] = ['ElevenOS', 'Marriott', 'None', 'Other'];
  let remoteAuth: RemoteAuthOptions;
  let remoteServerIp: string = '';
  let remoteServerPort: string = '';

  $: validation = {
    siteIDValid: isNotEmpty(site.ID),
    siteNameValid: isNotEmpty(site.Name),
    timezoneValid: isNotEmpty(site.TimeZone),
    remoteServerIpValid:
      remoteAuth !== 'Other' ||
      (remoteAuth === 'Other' && isNotEmpty(remoteServerIp) && isIP(remoteServerIp)),
    remoteServerPortValid:
      remoteAuth !== 'Other' ||
      (remoteAuth === 'Other' && isNotEmpty(remoteServerPort) && isPort(remoteServerPort)),
    sharedSecretValid:
      remoteAuth !== 'Other' || (remoteAuth === 'Other' && isNotEmpty(site.SharedSecret))
  };

  $: showOverrides = remoteAuth !== 'None';
  $: showRemoteInputs = remoteAuth === 'Other';

  $: {
    if (remoteServerIp && remoteServerPort) {
      site.RemoteServerUrl = `${remoteServerIp}:${remoteServerPort}`;
    }
  }

  $: parsedMaxDownloadSpeed =
    site.MaxDownloadSpeedBits === 0
      ? 'Not Set'
      : site.MaxDownloadSpeedBits >= 1000000
      ? `${Math.round((site.MaxDownloadSpeedBits / 1000000) * 1e2) / 1e2} Mbps`
      : `${Math.round((site.MaxDownloadSpeedBits / 1000) * 1e2) / 1e2} Kbps`;

  $: parsedMaxUploadSpeed =
    site.MaxUploadSpeedBits === 0
      ? 'Not Set'
      : site.MaxUploadSpeedBits >= 1000000
      ? `${Math.round((site.MaxUploadSpeedBits / 1000000) * 1e2) / 1e2} Mbps`
      : `${Math.round((site.MaxUploadSpeedBits / 1000) * 1e2) / 1e2} Kbps`;

  onMount(async () => {
    // Fetch latest data for site
    $showSpinner = true;
    await fetchSelectedGroup($userSession!.ID, $selectedGroup.ID);
    await Promise.all([
      fetchGroupEndpoints($userSession!.ID, $selectedGroup.ID),
      fetchGroupAuthenticators($userSession!.ID, $selectedGroup.ID)
    ]);

    // prepare initial value
    site.ID = $selectedGroup.ID;
    site.MaxDownloadSpeedBits = $selectedGroup.MaxDownloadSpeedBits || 0;
    site.MaxUploadSpeedBits = $selectedGroup.MaxUploadSpeedBits || 0;
    site.Name = $selectedGroup.Name;
    site.OverrideVlan = $selectedGroup.OverrideVlan;
    site.OverrideConnectionSpeed = $selectedGroup.OverrideConnectionSpeed;
    site.TimeZone = $selectedGroup.TimeZone;

    if (
      $selectedGroup.RemoteServer === undefined &&
      $selectedGroup.RemoteServerUrl &&
      $selectedGroup.RemoteServerUrl !== ''
    ) {
      let url_parts = $selectedGroup.RemoteServerUrl.split(':');
      remoteServerIp = url_parts[0];
      remoteServerPort = url_parts[1];
      site.SharedSecret = $selectedGroup.SharedSecret;
      remoteAuth = 'Other';
    } else if ($selectedGroup.RemoteServer === undefined && !site.RemoteServerUrl) {
      remoteAuth = 'None';
    } else if ($selectedGroup.RemoteServer) {
      remoteAuth = $selectedGroup.RemoteServer as RemoteAuthOptions;
      site.RemoteServer = $selectedGroup.RemoteServer;
    }

    $showSpinner = false;
  });

  onDestroy(() => {
    endpoints.set([]);
    authenticators.set([]);
    isEndpointGlobal.set(false);
  });

  export const submitEditSite = async () => {
    message = '';
    submitted = true;

    // validation
    const valid = Object.values(validation).every(Boolean);
    if (!valid) {
      message = 'Please correct all the errors marked in red';
      const modalInstance: HTMLDivElement = getModalInstance();
      modalInstance.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
      return;
    }

    $showSpinner = true;

    // prepare payload for update
    if (remoteAuth === 'None') {
      site.OverrideVlan = false;
      site.OverrideConnectionSpeed = false;
      site.RemoteServer = '';
      site.RemoteServerUrl = '';
      site.SharedSecret = '';
    } else if (remoteAuth === 'Other') {
      site.RemoteServer = '';
    } else {
      site.RemoteServer = remoteAuth;
      site.RemoteServerUrl = '';
      site.SharedSecret = '';
    }

    const { Error, Group: updatedGroup } = await updateGroup($userSession!.ID, site);

    if (Error) {
      message = Error;
    } else {
      message = '';
      await fetchSelectedGroup($userSession!.ID, updatedGroup.ID);
      dispatch('done');
    }

    $showSpinner = false;
  };
</script>

<Alert type="danger" {message} on:click={() => (message = '')} />
<div class="text-gray-800">
  <div class="form-group">
    <div class="label">Site Name</div>
    <div class="form-item">
      <input
        class:border-red-500={!validation.siteNameValid && submitted}
        type="text"
        bind:value={site.Name}
        disabled={!siteNameEditable}
      />
      {#if !validation.siteNameValid && submitted}
        <span class="mt-1 ml-1 text-xs font-medium tracking-wide text-red-500">
          Site name is required
        </span>
      {/if}
    </div>
  </div>

  <div class="form-group">
    <div class="label">Site ID</div>
    <div class="form-item">
      <input
        class:border-red-500={!validation.siteIDValid && submitted}
        type="text"
        bind:value={site.ID}
        disabled
      />
      {#if !validation.siteIDValid && submitted}
        <span class="mt-1 ml-1 text-xs font-medium tracking-wide text-red-500">
          Site ID is required
        </span>
      {/if}
    </div>
  </div>

  <div class="form-group">
    <div class="label">Timezone</div>
    <div class="form-item">
      <TimezonePicker
        bind:value={site.TimeZone}
        showInvalidBorder={!validation.timezoneValid && submitted}
        disabled={!formEditable}
      />
      {#if !validation.timezoneValid && submitted}
        <span class="mt-1 ml-1 text-xs font-medium tracking-wide text-red-500">
          Site timezone is required
        </span>
      {/if}
    </div>
  </div>

  <div class="form-group">
    <div class="label">Remote Authentication</div>
    <div class="form-item">
      <div class="relative flex w-full items-center">
        <select
          bind:value={remoteAuth}
          class="h-[34px] w-full cursor-pointer appearance-none rounded border border-gray-300 py-1.5 px-3 leading-tight text-gray-700 focus:outline-none focus:ring-1 focus:ring-blue-200"
        >
          {#each remoteAuthOptions as remoteAuthOption (remoteAuthOption)}
            <option value={remoteAuthOption}>{remoteAuthOption}</option>
          {/each}
        </select>
        <svg
          class="pointer-events-none absolute right-2 h-4 w-4"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
          stroke-width="2"
        >
          <path stroke-linecap="round" stroke-linejoin="round" d="m19 9-7 7-7-7" />
        </svg>
      </div>
    </div>
  </div>

  {#if showRemoteInputs}
    <div class="form-group">
      <div class="label">Remote Server IP</div>
      <div class="form-item">
        <InputMask
          showInvalidBorder={!validation.remoteServerIpValid && submitted}
          imask={{
            mask: 'IP{.}`IP{.}`IP{.}`IP',
            blocks: {
              IP: {
                mask: Number,
                scale: 0,
                min: 0,
                max: 255
              }
            }
          }}
          bind:value={remoteServerIp}
        />
        {#if !validation.remoteServerIpValid && submitted}
          <span class="mt-1 ml-1 text-xs font-medium tracking-wide text-red-500">
            A valid Remote Server IP is required
          </span>
        {/if}
      </div>
    </div>

    <div class="form-group">
      <div class="label">Remote Server Port</div>
      <div class="form-item">
        <InputMask
          showInvalidBorder={!validation.remoteServerPortValid && submitted}
          imask={{
            mask: IMask.MaskedRange,
            from: 1,
            to: 65535
          }}
          bind:value={remoteServerPort}
        />
        {#if !validation.remoteServerPortValid && submitted}
          <span class="mt-1 ml-1 text-xs font-medium tracking-wide text-red-500">
            A valid Remote Server Port (1-65535) is required
          </span>
        {/if}
      </div>
    </div>

    <div class="form-group">
      <div class="label">Shared Secret</div>
      <div class="form-item">
        <input
          class:border-red-500={!validation.sharedSecretValid && submitted}
          type="text"
          bind:value={site.SharedSecret}
        />
        {#if !validation.sharedSecretValid && submitted}
          <span class="mt-1 ml-1 text-xs font-medium tracking-wide text-red-500">
            Shared secret is required
          </span>
        {/if}
      </div>
    </div>
  {/if}

  {#if showOverrides}
    <div class="form-group items-start">
      <div class="label">Use Remote Server Settings</div>
      <div class="form-item">
        <div class="flex w-full flex-col gap-1">
          <label class="flex items-center font-bold">
            <input class="mr-2 accent-primary" type="checkbox" bind:checked={site.OverrideVlan} />
            VLAN
          </label>
          <label class="flex items-center font-bold">
            <input
              class="mr-2 accent-primary"
              type="checkbox"
              bind:checked={site.OverrideConnectionSpeed}
            />
            Upload and Download Speed
          </label>
          <p>Please allow 5 minutes for changes to take effect.</p>
        </div>
      </div>
    </div>
  {/if}

  <hr class="my-5" />

  <div class="mb-2 text-sm">Default Site Settings</div>
  <div class="form-group">
    <div class="label mb-2 w-full">Max Download Speed</div>
    <div class="form-item !w-full">
      <div class="flex w-full items-center gap-4">
        <span class="shrink-0">Not Set</span>
        <div class="range relative flex-grow">
          <input
            type="range"
            bind:value={site.MaxDownloadSpeedBits}
            min="0"
            max="20000000"
            step="5000"
          />
        </div>
        <span class="shrink-0">20 Mbps</span>
        <span class="w-24 shrink-0 rounded border border-gray-300 bg-white py-1.5 text-center">
          {parsedMaxDownloadSpeed}
        </span>
      </div>
    </div>
  </div>

  <div class="form-group">
    <div class="label mb-2 w-full">Max Upload Speed</div>
    <div class="form-item !w-full">
      <div class="flex w-full items-center gap-4">
        <span class="shrink-0">Not Set</span>
        <div class="range relative flex-grow">
          <input
            type="range"
            bind:value={site.MaxUploadSpeedBits}
            min="0"
            max="20000000"
            step="5000"
          />
        </div>
        <span class="shrink-0">20 Mbps</span>
        <span class="w-24 shrink-0 rounded border border-gray-300 bg-white py-1.5 text-center">
          {parsedMaxUploadSpeed}
        </span>
      </div>
    </div>
  </div>

  <hr class="my-5" />
  <Devices />
  <hr class="my-5" />
  <PortProvision />
</div>
