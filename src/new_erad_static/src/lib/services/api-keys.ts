import { apikeys } from '$lib/stores/api-keys';
import type { APIKey } from '$lib/types/api-key';
import { api } from '$lib/utils/api';

export const fetchAPIKeyList = async (sessionId: string) => {
  const data = await api('POST', '/erad/admin/apikey/list', { Session: { ID: sessionId } });
  if (data.ApiKeys && data.ApiKeys.length > 0) {
    apikeys.set(data.ApiKeys);
  }
  return data;
};

export const fetchAPIKey = async (sessionId: string, apikeyname: string) => {
  return await api('POST', '/erad/admin/apikey/load', {
    Session: { ID: sessionId },
    ApiKey: { ApiKeyName: apikeyname }
  });
};

export const addUpdateAPIKey = async (sessionId: string, apikey: APIKey) => {
  const data = await api('POST', '/erad/admin/apikey/save', {
    Session: { ID: sessionId },
    ApiKey: {
      DisplayName: apikey.DisplayName,
      Active: apikey.Active,
      ...(apikey.ApiKeyName && { ApiKeyName: apikey.ApiKeyName })
    }
  });

  if (!data.Error) {
    await fetchAPIKeyList(sessionId);
  }

  return data;
};

export const deleteAPIKey = async (sessionId: string, apikeyname: string) => {
  const data = await api('POST', '/erad/admin/apikey/delete', {
    Session: { ID: sessionId },
    ApiKey: { ApiKeyName: apikeyname }
  });

  if (!data.Error) {
    await fetchAPIKeyList(sessionId);
  }

  return data;
};
