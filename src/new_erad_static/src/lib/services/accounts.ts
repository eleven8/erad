import { accounts } from '$lib/stores/accounts';
import { api } from '$lib/utils/api';

export const fetchAccountList = async (sessionId: string) => {
  const data = await api('POST', '/erad/admin/account/list', { Session: { ID: sessionId } });

  if (data.Accounts && data.Accounts.length > 0) {
    accounts.set(data.Accounts);
  }

  return data;
};

export const addUpdateAccount = async (
  sessionId: string,
  payload: { email: string; password: string }
) => {
  const data = await api('POST', '/erad/admin/account/save', {
    Session: { ID: sessionId },
    AccountOwner: { Email: payload.email, Password: payload.password }
  });

  if (!data.Error) {
    await fetchAccountList(sessionId);
  }

  return data;
};

export const deleteAccount = async (sessionId: string, email: string) => {
  const data = await api('POST', '/erad/admin/account/delete', {
    Session: { ID: sessionId },
    AccountOwner: { Email: email }
  });

  if (!data.Error) {
    await fetchAccountList(sessionId);
  }

  return data;
};
