import { audits, radius_logs, supplicants } from '$lib/stores/users';
import { api } from '$lib/utils/api';

export const fetchSupplicantList = async (sessionId: string, groupId: string) => {
  const data = await api('POST', '/erad/admin/supplicant/list', {
    Session: { ID: sessionId },
    Group: { ID: groupId }
  });

  if (data.Supplicant_List) {
    const _supplicants = data.Supplicant_List.map((supplicant: any) => ({
      ...supplicant,
      id: Symbol()
    }));
    supplicants.set(_supplicants);
  }

  return data;
};

export const addSupplicant = async (sessionId: string, supplicant: any, isMac = false) => {
  const url = isMac ? '/erad/admin/supplicant/mac/save' : '/erad/admin/supplicant/save';
  const data = await api('POST', url, { Session: { ID: sessionId }, Supplicant: supplicant });

  if (data.Supplicant) {
    supplicants.update((currentSupplicants) => {
      return [...currentSupplicants, { ...data.Supplicant, id: Symbol() }];
    });
  }

  return data;
};

export const removeSupplicant = async (
  sessionId: string,
  supplicant: { Username: string; Group_ID: string }
) => {
  const data = await api('POST', '/erad/admin/supplicant/delete', {
    Session: { ID: sessionId },
    Supplicant: supplicant
  });

  if (!data.Error) {
    supplicants.update((currentSupplicants) => {
      return currentSupplicants.filter(
        (currentSupplicant) => currentSupplicant.Username !== supplicant.Username
      );
    });
  }

  return data;
};

export const importSupplicants = async (sessionId: string, groupId: string, file: File) => {
  const formdata = new FormData();
  formdata.append('file', file);

  const data = await api(
    'POST',
    `/erad/admin/supplicant/import/csv?Session_ID=${sessionId}&Group_ID=${groupId}`,
    formdata
  );

  if (!data.Error) {
    await fetchSupplicantList(sessionId, groupId);
  }

  return data;
};

export const fetchChangeHistory = async (
  sessionId: string,
  options: { groupId: string; start: string; end: string }
) => {
  const data = await api('POST', '/erad/admin/audit/search', {
    Session: { ID: sessionId },
    Group: { ID: options.groupId },
    AfterDate: options.start,
    BeforeDate: options.end
  });

  if (!data.Error) {
    const _audits = data.Audit_List.map((audit: any) => ({
      ...audit,
      id: Symbol()
    }));
    audits.set(_audits);
  }

  return data;
};

export const fetchRadiusLog = async (
  sessionId: string,
  options: {
    groupId: string;
    includeStray: boolean;
    includeBubble: boolean;
    interval: [number, number];
  }
) => {
  const data = await api('POST', '/erad/admin/supplicant/log/group/list', {
    Session: { ID: sessionId },
    Radius_Log: {
      Group_ID: options.groupId,
      Include_Stray: options.includeStray,
      Include_Bubble: options.includeBubble,
      Instance_Interval: options.interval
    }
  });

  if (!data.Error) {
    const _logs = data.map((log: any) => ({
      ...log,
      id: Symbol()
    }));
    radius_logs.set(_logs);
  } else {
    radius_logs.set([]);
  }

  return data;
};
