import {
  authenticators,
  endpoints,
  groups,
  isEndpointGlobal,
  selectedGroup
} from '$lib/stores/groups';
import type {
  AddGroupPayload,
  EditGroupPayload,
  Group,
  GroupAuthenticator,
  GroupEndpoint
} from '$lib/types/groups';
import { api } from '$lib/utils/api';

export const fetchGroupList = async (sessionId: string) => {
  const data = await api('POST', '/erad/admin/group/list', { Session: { ID: sessionId } });
  if (data.Group_List) {
    groups.set(data.Group_List);
  }
  return data;
};

export const fetchSelectedGroup = async (sessionId: string, groupId: string) => {
  const data = await api('POST', '/erad/admin/group_unique_devices/load', {
    Session: { ID: sessionId },
    Group: { ID: groupId }
  });

  if (data.Group) {
    selectedGroup.set(data.Group);
  }
  return data;
};

export const addGroup = async (sessionId: string, group: AddGroupPayload) => {
  const data = await api('POST', '/erad/admin/group/save', {
    Session: { ID: sessionId },
    Group: group
  });
  if (data.Group) {
    groups.update((currentGroups) => {
      return [...currentGroups, data.Group];
    });
  }

  return data;
};

export const updateGroup = async (sessionId: string, group: EditGroupPayload) => {
  const data = await api('POST', '/erad/admin/group/save', {
    Session: { ID: sessionId },
    Group: group
  });
  if (data.Group) {
    groups.update((currentGroups) => {
      let copiedGroups = [...currentGroups];
      let index = copiedGroups.findIndex((_group) => _group.ID === group.ID);
      if (index !== -1) {
        copiedGroups[index] = data.Group;
      }
      return copiedGroups;
    });
  }

  return data;
};

export const deleteGroup = async (sessionId: string, group: Group) => {
  const data = await api('POST', '/erad/admin/group/delete', {
    Session: { ID: sessionId },
    Group: group
  });

  if (!data.Error) {
    groups.update((currentGroups) => {
      return currentGroups.filter((currentGroup) => currentGroup.ID !== group.ID);
    });
  }

  return data;
};

export const fetchGroupEndpoints = async (sessionId: string, groupId: string) => {
  const data = await api('POST', '/erad/admin/endpoint/load', {
    Session: { ID: sessionId },
    Group_ID: groupId
  });

  if (!data.Error) {
    if (Array.isArray(data.Endpoint)) {
      endpoints.set(data.Endpoint);
    } else {
      endpoints.set([data.Endpoint]);
    }
    isEndpointGlobal.set(data.Global);
  }

  return data;
};

export const provisionGroupEndpoint = async (
  sessionId: string,
  groupId: string,
  region: string
) => {
  const data = await api('POST', '/erad/admin/endpoint/save', {
    Session: { ID: sessionId },
    Group_ID: groupId,
    Region: region
  });

  if (!data.Error) {
    await fetchGroupEndpoints(sessionId, groupId);
  }

  return data;
};

export const deleteGroupEndpoint = async (
  sessionId: string,
  groupId: string,
  endpoint: GroupEndpoint
) => {
  const data = await api('POST', '/erad/admin/endpoint/delete', {
    Session: { ID: sessionId },
    Group_ID: groupId,
    Port: endpoint.Port
  });

  if (!data.Error) {
    await fetchGroupEndpoints(sessionId, groupId);
  }

  return data;
};

export const fetchGroupAuthenticators = async (sessionId: string, groupId: string) => {
  const data = await api('POST', '/erad/admin/group/authenticator/list', {
    Session: { ID: sessionId },
    Group_ID: groupId
  });

  if (!data.Error) {
    const list: GroupAuthenticator[] = data.Auth_List.map((authenticator: any) => {
      const key: string = Object.keys(authenticator)[0];
      const value: any = Object.values(authenticator)[0];
      return { key, value, editing: false };
    });
    authenticators.set(list);
  }

  return data;
};

export const saveGroupAuthenticator = async (
  sessionId: string,
  groupId: string,
  authenticator: GroupAuthenticator,
  fetchAuthenticators: boolean = true
) => {
  const data = await api('POST', '/erad/admin/authenticator/save', {
    Session: { ID: sessionId },
    Authenticator: {
      ID: authenticator.value,
      RadiusAttribute: authenticator.key,
      Group_ID: groupId
    }
  });

  if (!data.Error && fetchAuthenticators) {
    await fetchGroupAuthenticators(sessionId, groupId);
  }

  return data;
};

export const deleteGroupAuthenticator = async (
  sessionId: string,
  groupId: string,
  authenticator: GroupAuthenticator
) => {
  const data = await api('POST', '/erad/admin/authenticator/delete', {
    Session: { ID: sessionId },
    Authenticator: {
      ID: authenticator.value,
      RadiusAttribute: authenticator.key,
      Group_ID: groupId
    }
  });

  if (!data.Error) {
    await fetchGroupAuthenticators(sessionId, groupId);
  }

  return data;
};
