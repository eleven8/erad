export function isNotEmpty(value: string) {
  return value !== '';
}

export function isEmail(value: string) {
  return new RegExp("^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$").test(value);
}

export function isIP(value: string) {
  return new RegExp(
    '^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$'
  ).test(value);
}

export function isPort(value: string) {
  return new RegExp(
    '^([1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$'
  ).test(value);
}

export function isMacAddress(value: string) {
  return new RegExp('^([0-9A-Fa-f]{2}[:]){5}([0-9A-Fa-f]{2})$').test(value);
}
