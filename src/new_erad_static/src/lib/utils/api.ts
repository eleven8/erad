import { browser } from '$app/env';
import { logout } from './auth';
import { variables } from './variables';
type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH';

const base = variables.apiPath;

export const api = async (
  method: HttpMethod,
  path: string,
  data?: { [key: string]: any } | FormData,
  headers: HeadersInit = { 'Content-Type': 'application/json' }
) => {
  const noData = method === 'GET' || method === 'DELETE';
  const url = path.charAt(0) === '/' ? `${base}${path}` : `${base}/${path}`;
  let isFormData = false;
  if (browser) {
    isFormData = data instanceof FormData;
  }

  return fetch(url, {
    method: method,
    ...(isFormData ? null : { headers: headers }),
    ...(!noData ? { body: isFormData ? (data as FormData) : JSON.stringify(data) } : null)
  })
    .then(async (res) => {
      const json = await res.json();
      if (json.Error && json.Error.includes('Access Denied')) {
        logout();
      }
      return json;
    })
    .catch(() => {
      // no network connection so we send here a general error message
      return {
        status: 502,
        Error: 'Oops! Something is wrong. Please try later.'
      };
    });
};
