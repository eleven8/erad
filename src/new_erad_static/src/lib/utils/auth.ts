import { goto } from '$app/navigation';
import { userSession } from '$lib/stores/auth';
import { api } from './api';

export const authenticate = async (sessionId: string) => {
  const data = await api('POST', '/erad/admin/session/load', {
    Session: {
      ID: sessionId
    }
  });

  if (data.Session) {
    userSession.set(data.Session);
  }

  return data;
};

export const logout = async () => {
  userSession.set(null);
  goto('/login', { replaceState: true });
};
