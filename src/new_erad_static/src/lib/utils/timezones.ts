import type { TimeZone } from '$lib/types/timezone';

export const timezones: TimeZone[] = [
  {
    country: 'Afghanistan',
    children: [
      {
        id: 'Asia/Kabul',
        name: 'Asia/Kabul',
        offset: 'UTC+04:30',
        nOffset: 270
      }
    ],
    firstNOffset: 270
  },
  {
    country: 'Aland Islands',
    children: [
      {
        id: 'Europe/Mariehamn',
        name: 'Europe/Mariehamn',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Albania',
    children: [
      {
        id: 'Europe/Tirane',
        name: 'Europe/Tirane',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Algeria',
    children: [
      {
        id: 'Africa/Algiers',
        name: 'Africa/Algiers',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'American Samoa',
    children: [
      {
        id: 'Pacific/Pago_Pago',
        name: 'Pacific/Pago Pago',
        offset: 'UTC-11:00',
        nOffset: -660
      }
    ],
    firstNOffset: -660
  },
  {
    country: 'Andorra',
    children: [
      {
        id: 'Europe/Andorra',
        name: 'Europe/Andorra',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Angola',
    children: [
      {
        id: 'Africa/Luanda',
        name: 'Africa/Luanda',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Anguilla',
    children: [
      {
        id: 'America/Anguilla',
        name: 'America/Anguilla',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Antarctica',
    children: [
      {
        id: 'Antarctica/Casey',
        name: 'Antarctica/Casey',
        offset: 'UTC+11:00',
        nOffset: 660
      },
      {
        id: 'Antarctica/Davis',
        name: 'Antarctica/Davis',
        offset: 'UTC+07:00',
        nOffset: 420
      },
      {
        id: 'Antarctica/DumontDUrville',
        name: 'Antarctica/DumontDUrville',
        offset: 'UTC+10:00',
        nOffset: 600
      },
      {
        id: 'Antarctica/Mawson',
        name: 'Antarctica/Mawson',
        offset: 'UTC+05:00',
        nOffset: 300
      },
      {
        id: 'Antarctica/McMurdo',
        name: 'Antarctica/McMurdo',
        offset: 'UTC+12:00',
        nOffset: 720
      },
      {
        id: 'Antarctica/Palmer',
        name: 'Antarctica/Palmer',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'Antarctica/Rothera',
        name: 'Antarctica/Rothera',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'Antarctica/Syowa',
        name: 'Antarctica/Syowa',
        offset: 'UTC+03:00',
        nOffset: 180
      },
      {
        id: 'Antarctica/Troll',
        name: 'Antarctica/Troll',
        offset: 'UTC+02:00',
        nOffset: 120
      },
      {
        id: 'Antarctica/Vostok',
        name: 'Antarctica/Vostok',
        offset: 'UTC+06:00',
        nOffset: 360
      }
    ],
    firstNOffset: 660
  },
  {
    country: 'Antigua And Barbuda',
    children: [
      {
        id: 'America/Antigua',
        name: 'America/Antigua',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Argentina',
    children: [
      {
        id: 'America/Argentina/Buenos_Aires',
        name: 'America/Argentina/Buenos Aires',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/Catamarca',
        name: 'America/Argentina/Catamarca',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/Cordoba',
        name: 'America/Argentina/Cordoba',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/Jujuy',
        name: 'America/Argentina/Jujuy',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/La_Rioja',
        name: 'America/Argentina/La Rioja',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/Mendoza',
        name: 'America/Argentina/Mendoza',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/Rio_Gallegos',
        name: 'America/Argentina/Rio Gallegos',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/Salta',
        name: 'America/Argentina/Salta',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/San_Juan',
        name: 'America/Argentina/San Juan',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/San_Luis',
        name: 'America/Argentina/San Luis',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/Tucuman',
        name: 'America/Argentina/Tucuman',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Argentina/Ushuaia',
        name: 'America/Argentina/Ushuaia',
        offset: 'UTC-03:00',
        nOffset: -180
      }
    ],
    firstNOffset: -180
  },
  {
    country: 'Armenia',
    children: [
      {
        id: 'Asia/Yerevan',
        name: 'Asia/Yerevan',
        offset: 'UTC+04:00',
        nOffset: 240
      }
    ],
    firstNOffset: 240
  },
  {
    country: 'Aruba',
    children: [
      {
        id: 'America/Aruba',
        name: 'America/Aruba',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Australia',
    children: [
      {
        id: 'Antarctica/Macquarie',
        name: 'Antarctica/Macquarie',
        offset: 'UTC+10:00',
        nOffset: 600
      },
      {
        id: 'Australia/Adelaide',
        name: 'Australia/Adelaide',
        offset: 'UTC+09:30',
        nOffset: 570
      },
      {
        id: 'Australia/Brisbane',
        name: 'Australia/Brisbane',
        offset: 'UTC+10:00',
        nOffset: 600
      },
      {
        id: 'Australia/Broken_Hill',
        name: 'Australia/Broken Hill',
        offset: 'UTC+09:30',
        nOffset: 570
      },
      {
        id: 'Australia/Currie',
        name: 'Australia/Currie',
        offset: 'UTC+10:00',
        nOffset: 600
      },
      {
        id: 'Australia/Darwin',
        name: 'Australia/Darwin',
        offset: 'UTC+09:30',
        nOffset: 570
      },
      {
        id: 'Australia/Eucla',
        name: 'Australia/Eucla',
        offset: 'UTC+08:45',
        nOffset: 525
      },
      {
        id: 'Australia/Hobart',
        name: 'Australia/Hobart',
        offset: 'UTC+10:00',
        nOffset: 600
      },
      {
        id: 'Australia/Lindeman',
        name: 'Australia/Lindeman',
        offset: 'UTC+10:00',
        nOffset: 600
      },
      {
        id: 'Australia/Lord_Howe',
        name: 'Australia/Lord Howe',
        offset: 'UTC+10:30',
        nOffset: 630
      },
      {
        id: 'Australia/Melbourne',
        name: 'Australia/Melbourne',
        offset: 'UTC+10:00',
        nOffset: 600
      },
      {
        id: 'Australia/Perth',
        name: 'Australia/Perth',
        offset: 'UTC+08:00',
        nOffset: 480
      },
      {
        id: 'Australia/Sydney',
        name: 'Australia/Sydney',
        offset: 'UTC+10:00',
        nOffset: 600
      }
    ],
    firstNOffset: 600
  },
  {
    country: 'Austria',
    children: [
      {
        id: 'Europe/Vienna',
        name: 'Europe/Vienna',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Azerbaijan',
    children: [
      {
        id: 'Asia/Baku',
        name: 'Asia/Baku',
        offset: 'UTC+04:00',
        nOffset: 240
      }
    ],
    firstNOffset: 240
  },
  {
    country: 'Bahamas',
    children: [
      {
        id: 'America/Nassau',
        name: 'America/Nassau',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Bahrain',
    children: [
      {
        id: 'Asia/Bahrain',
        name: 'Asia/Bahrain',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Bangladesh',
    children: [
      {
        id: 'Asia/Dhaka',
        name: 'Asia/Dhaka',
        offset: 'UTC+06:00',
        nOffset: 360
      }
    ],
    firstNOffset: 360
  },
  {
    country: 'Barbados',
    children: [
      {
        id: 'America/Barbados',
        name: 'America/Barbados',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Belarus',
    children: [
      {
        id: 'Europe/Minsk',
        name: 'Europe/Minsk',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Belgium',
    children: [
      {
        id: 'Europe/Brussels',
        name: 'Europe/Brussels',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Belize',
    children: [
      {
        id: 'America/Belize',
        name: 'America/Belize',
        offset: 'UTC-06:00',
        nOffset: -360
      }
    ],
    firstNOffset: -360
  },
  {
    country: 'Benin',
    children: [
      {
        id: 'Africa/Porto-Novo',
        name: 'Africa/Porto-Novo',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Bermuda',
    children: [
      {
        id: 'Atlantic/Bermuda',
        name: 'Atlantic/Bermuda',
        offset: 'UTC-03:00',
        nOffset: -180
      }
    ],
    firstNOffset: -180
  },
  {
    country: 'Bhutan',
    children: [
      {
        id: 'Asia/Thimphu',
        name: 'Asia/Thimphu',
        offset: 'UTC+06:00',
        nOffset: 360
      }
    ],
    firstNOffset: 360
  },
  {
    country: 'Bolivia',
    children: [
      {
        id: 'America/La_Paz',
        name: 'America/La Paz',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Bonaire',
    children: [
      {
        id: 'America/Kralendijk',
        name: 'America/Kralendijk',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Bosnia And Herzegovina',
    children: [
      {
        id: 'Europe/Sarajevo',
        name: 'Europe/Sarajevo',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Botswana',
    children: [
      {
        id: 'Africa/Gaborone',
        name: 'Africa/Gaborone',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Brazil',
    children: [
      {
        id: 'America/Araguaina',
        name: 'America/Araguaina',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Bahia',
        name: 'America/Bahia',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Belem',
        name: 'America/Belem',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Boa_Vista',
        name: 'America/Boa Vista',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Campo_Grande',
        name: 'America/Campo Grande',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Cuiaba',
        name: 'America/Cuiaba',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Eirunepe',
        name: 'America/Eirunepe',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Fortaleza',
        name: 'America/Fortaleza',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Maceio',
        name: 'America/Maceio',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Manaus',
        name: 'America/Manaus',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Noronha',
        name: 'America/Noronha',
        offset: 'UTC-02:00',
        nOffset: -120
      },
      {
        id: 'America/Porto_Velho',
        name: 'America/Porto Velho',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Recife',
        name: 'America/Recife',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Rio_Branco',
        name: 'America/Rio Branco',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Santarem',
        name: 'America/Santarem',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Sao_Paulo',
        name: 'America/Sao Paulo',
        offset: 'UTC-03:00',
        nOffset: -180
      }
    ],
    firstNOffset: -180
  },
  {
    country: 'British Indian Ocean Territory',
    children: [
      {
        id: 'Indian/Chagos',
        name: 'Indian/Chagos',
        offset: 'UTC+06:00',
        nOffset: 360
      }
    ],
    firstNOffset: 360
  },
  {
    country: 'Brunei Darussalam',
    children: [
      {
        id: 'Asia/Brunei',
        name: 'Asia/Brunei',
        offset: 'UTC+08:00',
        nOffset: 480
      }
    ],
    firstNOffset: 480
  },
  {
    country: 'Bulgaria',
    children: [
      {
        id: 'Europe/Sofia',
        name: 'Europe/Sofia',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Burkina Faso',
    children: [
      {
        id: 'Africa/Ouagadougou',
        name: 'Africa/Ouagadougou',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Burundi',
    children: [
      {
        id: 'Africa/Bujumbura',
        name: 'Africa/Bujumbura',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Cambodia',
    children: [
      {
        id: 'Asia/Phnom_Penh',
        name: 'Asia/Phnom Penh',
        offset: 'UTC+07:00',
        nOffset: 420
      }
    ],
    firstNOffset: 420
  },
  {
    country: 'Cameroon',
    children: [
      {
        id: 'Africa/Douala',
        name: 'Africa/Douala',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Canada',
    children: [
      {
        id: 'America/Atikokan',
        name: 'America/Atikokan',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Blanc-Sablon',
        name: 'America/Blanc-Sablon',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Cambridge_Bay',
        name: 'America/Cambridge Bay',
        offset: 'UTC-06:00',
        nOffset: -360
      },
      {
        id: 'America/Creston',
        name: 'America/Creston',
        offset: 'UTC-07:00',
        nOffset: -420
      },
      {
        id: 'America/Dawson',
        name: 'America/Dawson',
        offset: 'UTC-07:00',
        nOffset: -420
      },
      {
        id: 'America/Dawson_Creek',
        name: 'America/Dawson Creek',
        offset: 'UTC-07:00',
        nOffset: -420
      },
      {
        id: 'America/Edmonton',
        name: 'America/Edmonton',
        offset: 'UTC-06:00',
        nOffset: -360
      },
      {
        id: 'America/Glace_Bay',
        name: 'America/Glace Bay',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Goose_Bay',
        name: 'America/Goose Bay',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Halifax',
        name: 'America/Halifax',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Inuvik',
        name: 'America/Inuvik',
        offset: 'UTC-06:00',
        nOffset: -360
      },
      {
        id: 'America/Iqaluit',
        name: 'America/Iqaluit',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Moncton',
        name: 'America/Moncton',
        offset: 'UTC-03:00',
        nOffset: -180
      },
      {
        id: 'America/Nipigon',
        name: 'America/Nipigon',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Pangnirtung',
        name: 'America/Pangnirtung',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Rainy_River',
        name: 'America/Rainy River',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Rankin_Inlet',
        name: 'America/Rankin Inlet',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Regina',
        name: 'America/Regina',
        offset: 'UTC-06:00',
        nOffset: -360
      },
      {
        id: 'America/Resolute',
        name: 'America/Resolute',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/St_Johns',
        name: 'America/St Johns',
        offset: 'UTC-02:30',
        nOffset: -150
      },
      {
        id: 'America/Swift_Current',
        name: 'America/Swift Current',
        offset: 'UTC-06:00',
        nOffset: -360
      },
      {
        id: 'America/Thunder_Bay',
        name: 'America/Thunder Bay',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Toronto',
        name: 'America/Toronto',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Vancouver',
        name: 'America/Vancouver',
        offset: 'UTC-07:00',
        nOffset: -420
      },
      {
        id: 'America/Whitehorse',
        name: 'America/Whitehorse',
        offset: 'UTC-07:00',
        nOffset: -420
      },
      {
        id: 'America/Winnipeg',
        name: 'America/Winnipeg',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Yellowknife',
        name: 'America/Yellowknife',
        offset: 'UTC-06:00',
        nOffset: -360
      }
    ],
    firstNOffset: -300
  },
  {
    country: 'Cape Verde',
    children: [
      {
        id: 'Atlantic/Cape_Verde',
        name: 'Atlantic/Cape Verde',
        offset: 'UTC-01:00',
        nOffset: -60
      }
    ],
    firstNOffset: -60
  },
  {
    country: 'Cayman Islands',
    children: [
      {
        id: 'America/Cayman',
        name: 'America/Cayman',
        offset: 'UTC-05:00',
        nOffset: -300
      }
    ],
    firstNOffset: -300
  },
  {
    country: 'Central African Republic',
    children: [
      {
        id: 'Africa/Bangui',
        name: 'Africa/Bangui',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Chad',
    children: [
      {
        id: 'Africa/Ndjamena',
        name: 'Africa/Ndjamena',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Chile',
    children: [
      {
        id: 'America/Santiago',
        name: 'America/Santiago',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'Pacific/Easter',
        name: 'Pacific/Easter',
        offset: 'UTC-06:00',
        nOffset: -360
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'China',
    children: [
      {
        id: 'Asia/Chongqing',
        name: 'Asia/Chongqing',
        offset: 'UTC+08:00',
        nOffset: 480
      },
      {
        id: 'Asia/Harbin',
        name: 'Asia/Harbin',
        offset: 'UTC+08:00',
        nOffset: 480
      },
      {
        id: 'Asia/Kashgar',
        name: 'Asia/Kashgar',
        offset: 'UTC+06:00',
        nOffset: 360
      },
      {
        id: 'Asia/Shanghai',
        name: 'Asia/Shanghai',
        offset: 'UTC+08:00',
        nOffset: 480
      },
      {
        id: 'Asia/Urumqi',
        name: 'Asia/Urumqi',
        offset: 'UTC+06:00',
        nOffset: 360
      }
    ],
    firstNOffset: 480
  },
  {
    country: 'Christmas Island',
    children: [
      {
        id: 'Indian/Christmas',
        name: 'Indian/Christmas',
        offset: 'UTC+07:00',
        nOffset: 420
      }
    ],
    firstNOffset: 420
  },
  {
    country: 'Cocos (Keeling) Islands',
    children: [
      {
        id: 'Indian/Cocos',
        name: 'Indian/Cocos',
        offset: 'UTC+06:30',
        nOffset: 390
      }
    ],
    firstNOffset: 390
  },
  {
    country: 'Colombia',
    children: [
      {
        id: 'America/Bogota',
        name: 'America/Bogota',
        offset: 'UTC-05:00',
        nOffset: -300
      }
    ],
    firstNOffset: -300
  },
  {
    country: 'Comoros',
    children: [
      {
        id: 'Indian/Comoro',
        name: 'Indian/Comoro',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Congo',
    children: [
      {
        id: 'Africa/Brazzaville',
        name: 'Africa/Brazzaville',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Congo (Democratic Republic)',
    children: [
      {
        id: 'Africa/Kinshasa',
        name: 'Africa/Kinshasa',
        offset: 'UTC+01:00',
        nOffset: 60
      },
      {
        id: 'Africa/Lubumbashi',
        name: 'Africa/Lubumbashi',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Cook Islands',
    children: [
      {
        id: 'Pacific/Rarotonga',
        name: 'Pacific/Rarotonga',
        offset: 'UTC-10:00',
        nOffset: -600
      }
    ],
    firstNOffset: -600
  },
  {
    country: 'Costa Rica',
    children: [
      {
        id: 'America/Costa_Rica',
        name: 'America/Costa Rica',
        offset: 'UTC-06:00',
        nOffset: -360
      }
    ],
    firstNOffset: -360
  },
  {
    country: "Cote D'Ivoire",
    children: [
      {
        id: 'Africa/Abidjan',
        name: 'Africa/Abidjan',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Croatia',
    children: [
      {
        id: 'Europe/Zagreb',
        name: 'Europe/Zagreb',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Cuba',
    children: [
      {
        id: 'America/Havana',
        name: 'America/Havana',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Curacao',
    children: [
      {
        id: 'America/Curacao',
        name: 'America/Curacao',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Cyprus',
    children: [
      {
        id: 'Asia/Nicosia',
        name: 'Asia/Nicosia',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Czech Republic',
    children: [
      {
        id: 'Europe/Prague',
        name: 'Europe/Prague',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Denmark',
    children: [
      {
        id: 'Europe/Copenhagen',
        name: 'Europe/Copenhagen',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Djibouti',
    children: [
      {
        id: 'Africa/Djibouti',
        name: 'Africa/Djibouti',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Dominica',
    children: [
      {
        id: 'America/Dominica',
        name: 'America/Dominica',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Dominican Republic',
    children: [
      {
        id: 'America/Santo_Domingo',
        name: 'America/Santo Domingo',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Ecuador',
    children: [
      {
        id: 'America/Guayaquil',
        name: 'America/Guayaquil',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'Pacific/Galapagos',
        name: 'Pacific/Galapagos',
        offset: 'UTC-06:00',
        nOffset: -360
      }
    ],
    firstNOffset: -300
  },
  {
    country: 'Egypt',
    children: [
      {
        id: 'Africa/Cairo',
        name: 'Africa/Cairo',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'El Salvador',
    children: [
      {
        id: 'America/El_Salvador',
        name: 'America/El Salvador',
        offset: 'UTC-06:00',
        nOffset: -360
      }
    ],
    firstNOffset: -360
  },
  {
    country: 'Equatorial Guinea',
    children: [
      {
        id: 'Africa/Malabo',
        name: 'Africa/Malabo',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Eritrea',
    children: [
      {
        id: 'Africa/Asmara',
        name: 'Africa/Asmara',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Estonia',
    children: [
      {
        id: 'Europe/Tallinn',
        name: 'Europe/Tallinn',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Ethiopia',
    children: [
      {
        id: 'Africa/Addis_Ababa',
        name: 'Africa/Addis Ababa',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Falkland Islands (Malvinas)',
    children: [
      {
        id: 'Atlantic/Stanley',
        name: 'Atlantic/Stanley',
        offset: 'UTC-03:00',
        nOffset: -180
      }
    ],
    firstNOffset: -180
  },
  {
    country: 'Faroe Islands',
    children: [
      {
        id: 'Atlantic/Faroe',
        name: 'Atlantic/Faroe',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Fiji',
    children: [
      {
        id: 'Pacific/Fiji',
        name: 'Pacific/Fiji',
        offset: 'UTC+12:00',
        nOffset: 720
      }
    ],
    firstNOffset: 720
  },
  {
    country: 'Finland',
    children: [
      {
        id: 'Europe/Helsinki',
        name: 'Europe/Helsinki',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'France',
    children: [
      {
        id: 'Europe/Paris',
        name: 'Europe/Paris',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'French Guiana',
    children: [
      {
        id: 'America/Cayenne',
        name: 'America/Cayenne',
        offset: 'UTC-03:00',
        nOffset: -180
      }
    ],
    firstNOffset: -180
  },
  {
    country: 'French Polynesia',
    children: [
      {
        id: 'Pacific/Gambier',
        name: 'Pacific/Gambier',
        offset: 'UTC-09:00',
        nOffset: -540
      },
      {
        id: 'Pacific/Marquesas',
        name: 'Pacific/Marquesas',
        offset: 'UTC-09:30',
        nOffset: -570
      },
      {
        id: 'Pacific/Tahiti',
        name: 'Pacific/Tahiti',
        offset: 'UTC-10:00',
        nOffset: -600
      }
    ],
    firstNOffset: -540
  },
  {
    country: 'French Southern Territories',
    children: [
      {
        id: 'Indian/Kerguelen',
        name: 'Indian/Kerguelen',
        offset: 'UTC+05:00',
        nOffset: 300
      }
    ],
    firstNOffset: 300
  },
  {
    country: 'Gabon',
    children: [
      {
        id: 'Africa/Libreville',
        name: 'Africa/Libreville',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Gambia',
    children: [
      {
        id: 'Africa/Banjul',
        name: 'Africa/Banjul',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Georgia',
    children: [
      {
        id: 'Asia/Tbilisi',
        name: 'Asia/Tbilisi',
        offset: 'UTC+04:00',
        nOffset: 240
      }
    ],
    firstNOffset: 240
  },
  {
    country: 'Germany',
    children: [
      {
        id: 'Europe/Berlin',
        name: 'Europe/Berlin',
        offset: 'UTC+02:00',
        nOffset: 120
      },
      {
        id: 'Europe/Busingen',
        name: 'Europe/Busingen',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Ghana',
    children: [
      {
        id: 'Africa/Accra',
        name: 'Africa/Accra',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Gibraltar',
    children: [
      {
        id: 'Europe/Gibraltar',
        name: 'Europe/Gibraltar',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Greece',
    children: [
      {
        id: 'Europe/Athens',
        name: 'Europe/Athens',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Greenland',
    children: [
      {
        id: 'America/Danmarkshavn',
        name: 'America/Danmarkshavn',
        offset: 'UTC+00:00',
        nOffset: 0
      },
      {
        id: 'America/Godthab',
        name: 'America/Godthab',
        offset: 'UTC-02:00',
        nOffset: -120
      },
      {
        id: 'America/Scoresbysund',
        name: 'America/Scoresbysund',
        offset: 'UTC+00:00',
        nOffset: 0
      },
      {
        id: 'America/Thule',
        name: 'America/Thule',
        offset: 'UTC-03:00',
        nOffset: -180
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Grenada',
    children: [
      {
        id: 'America/Grenada',
        name: 'America/Grenada',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Guadeloupe',
    children: [
      {
        id: 'America/Guadeloupe',
        name: 'America/Guadeloupe',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Guam',
    children: [
      {
        id: 'Pacific/Guam',
        name: 'Pacific/Guam',
        offset: 'UTC+10:00',
        nOffset: 600
      }
    ],
    firstNOffset: 600
  },
  {
    country: 'Guatemala',
    children: [
      {
        id: 'America/Guatemala',
        name: 'America/Guatemala',
        offset: 'UTC-06:00',
        nOffset: -360
      }
    ],
    firstNOffset: -360
  },
  {
    country: 'Guernsey',
    children: [
      {
        id: 'Europe/Guernsey',
        name: 'Europe/Guernsey',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Guinea',
    children: [
      {
        id: 'Africa/Conakry',
        name: 'Africa/Conakry',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Guinea-Bissau',
    children: [
      {
        id: 'Africa/Bissau',
        name: 'Africa/Bissau',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Guyana',
    children: [
      {
        id: 'America/Guyana',
        name: 'America/Guyana',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Haiti',
    children: [
      {
        id: 'America/Port-au-Prince',
        name: 'America/Port-au-Prince',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Holy See (Vatican City State)',
    children: [
      {
        id: 'Europe/Vatican',
        name: 'Europe/Vatican',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Honduras',
    children: [
      {
        id: 'America/Tegucigalpa',
        name: 'America/Tegucigalpa',
        offset: 'UTC-06:00',
        nOffset: -360
      }
    ],
    firstNOffset: -360
  },
  {
    country: 'Hong Kong',
    children: [
      {
        id: 'Asia/Hong_Kong',
        name: 'Asia/Hong Kong',
        offset: 'UTC+08:00',
        nOffset: 480
      }
    ],
    firstNOffset: 480
  },
  {
    country: 'Hungary',
    children: [
      {
        id: 'Europe/Budapest',
        name: 'Europe/Budapest',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Iceland',
    children: [
      {
        id: 'Atlantic/Reykjavik',
        name: 'Atlantic/Reykjavik',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'India',
    children: [
      {
        id: 'Asia/Kolkata',
        name: 'Asia/Kolkata',
        offset: 'UTC+05:30',
        nOffset: 330
      }
    ],
    firstNOffset: 330
  },
  {
    country: 'Indonesia',
    children: [
      {
        id: 'Asia/Jakarta',
        name: 'Asia/Jakarta',
        offset: 'UTC+07:00',
        nOffset: 420
      },
      {
        id: 'Asia/Jayapura',
        name: 'Asia/Jayapura',
        offset: 'UTC+09:00',
        nOffset: 540
      },
      {
        id: 'Asia/Makassar',
        name: 'Asia/Makassar',
        offset: 'UTC+08:00',
        nOffset: 480
      },
      {
        id: 'Asia/Pontianak',
        name: 'Asia/Pontianak',
        offset: 'UTC+07:00',
        nOffset: 420
      }
    ],
    firstNOffset: 420
  },
  {
    country: 'Iran (Islamic Republic Of)',
    children: [
      {
        id: 'Asia/Tehran',
        name: 'Asia/Tehran',
        offset: 'UTC+04:30',
        nOffset: 270
      }
    ],
    firstNOffset: 270
  },
  {
    country: 'Iraq',
    children: [
      {
        id: 'Asia/Baghdad',
        name: 'Asia/Baghdad',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Ireland',
    children: [
      {
        id: 'Europe/Dublin',
        name: 'Europe/Dublin',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Isle Of Man',
    children: [
      {
        id: 'Europe/Isle_of_Man',
        name: 'Europe/Isle of Man',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Israel',
    children: [
      {
        id: 'Asia/Jerusalem',
        name: 'Asia/Jerusalem',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Italy',
    children: [
      {
        id: 'Europe/Rome',
        name: 'Europe/Rome',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Jamaica',
    children: [
      {
        id: 'America/Jamaica',
        name: 'America/Jamaica',
        offset: 'UTC-05:00',
        nOffset: -300
      }
    ],
    firstNOffset: -300
  },
  {
    country: 'Japan',
    children: [
      {
        id: 'Asia/Tokyo',
        name: 'Asia/Tokyo',
        offset: 'UTC+09:00',
        nOffset: 540
      }
    ],
    firstNOffset: 540
  },
  {
    country: 'Jersey',
    children: [
      {
        id: 'Europe/Jersey',
        name: 'Europe/Jersey',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Jordan',
    children: [
      {
        id: 'Asia/Amman',
        name: 'Asia/Amman',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Kazakhstan',
    children: [
      {
        id: 'Asia/Almaty',
        name: 'Asia/Almaty',
        offset: 'UTC+06:00',
        nOffset: 360
      },
      {
        id: 'Asia/Aqtau',
        name: 'Asia/Aqtau',
        offset: 'UTC+05:00',
        nOffset: 300
      },
      {
        id: 'Asia/Aqtobe',
        name: 'Asia/Aqtobe',
        offset: 'UTC+05:00',
        nOffset: 300
      },
      {
        id: 'Asia/Oral',
        name: 'Asia/Oral',
        offset: 'UTC+05:00',
        nOffset: 300
      },
      {
        id: 'Asia/Qyzylorda',
        name: 'Asia/Qyzylorda',
        offset: 'UTC+05:00',
        nOffset: 300
      }
    ],
    firstNOffset: 360
  },
  {
    country: 'Kenya',
    children: [
      {
        id: 'Africa/Nairobi',
        name: 'Africa/Nairobi',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Kiribati',
    children: [
      {
        id: 'Pacific/Enderbury',
        name: 'Pacific/Enderbury',
        offset: 'UTC+13:00',
        nOffset: 780
      },
      {
        id: 'Pacific/Kiritimati',
        name: 'Pacific/Kiritimati',
        offset: 'UTC+14:00',
        nOffset: 840
      },
      {
        id: 'Pacific/Tarawa',
        name: 'Pacific/Tarawa',
        offset: 'UTC+12:00',
        nOffset: 720
      }
    ],
    firstNOffset: 780
  },
  {
    country: 'Korea',
    children: [
      {
        id: 'Asia/Seoul',
        name: 'Asia/Seoul',
        offset: 'UTC+09:00',
        nOffset: 540
      }
    ],
    firstNOffset: 540
  },
  {
    country: 'Kuwait',
    children: [
      {
        id: 'Asia/Kuwait',
        name: 'Asia/Kuwait',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Kyrgyzstan',
    children: [
      {
        id: 'Asia/Bishkek',
        name: 'Asia/Bishkek',
        offset: 'UTC+06:00',
        nOffset: 360
      }
    ],
    firstNOffset: 360
  },
  {
    country: "Lao People's Democratic Republic",
    children: [
      {
        id: 'Asia/Vientiane',
        name: 'Asia/Vientiane',
        offset: 'UTC+07:00',
        nOffset: 420
      }
    ],
    firstNOffset: 420
  },
  {
    country: 'Latvia',
    children: [
      {
        id: 'Europe/Riga',
        name: 'Europe/Riga',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Lebanon',
    children: [
      {
        id: 'Asia/Beirut',
        name: 'Asia/Beirut',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Lesotho',
    children: [
      {
        id: 'Africa/Maseru',
        name: 'Africa/Maseru',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Liberia',
    children: [
      {
        id: 'Africa/Monrovia',
        name: 'Africa/Monrovia',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Libyan Arab Jamahiriya',
    children: [
      {
        id: 'Africa/Tripoli',
        name: 'Africa/Tripoli',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Liechtenstein',
    children: [
      {
        id: 'Europe/Vaduz',
        name: 'Europe/Vaduz',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Lithuania',
    children: [
      {
        id: 'Europe/Vilnius',
        name: 'Europe/Vilnius',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Luxembourg',
    children: [
      {
        id: 'Europe/Luxembourg',
        name: 'Europe/Luxembourg',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Macao',
    children: [
      {
        id: 'Asia/Macau',
        name: 'Asia/Macau',
        offset: 'UTC+08:00',
        nOffset: 480
      }
    ],
    firstNOffset: 480
  },
  {
    country: 'Macedonia',
    children: [
      {
        id: 'Europe/Skopje',
        name: 'Europe/Skopje',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Madagascar',
    children: [
      {
        id: 'Indian/Antananarivo',
        name: 'Indian/Antananarivo',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Malawi',
    children: [
      {
        id: 'Africa/Blantyre',
        name: 'Africa/Blantyre',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Malaysia',
    children: [
      {
        id: 'Asia/Kuala_Lumpur',
        name: 'Asia/Kuala Lumpur',
        offset: 'UTC+08:00',
        nOffset: 480
      },
      {
        id: 'Asia/Kuching',
        name: 'Asia/Kuching',
        offset: 'UTC+08:00',
        nOffset: 480
      }
    ],
    firstNOffset: 480
  },
  {
    country: 'Maldives',
    children: [
      {
        id: 'Indian/Maldives',
        name: 'Indian/Maldives',
        offset: 'UTC+05:00',
        nOffset: 300
      }
    ],
    firstNOffset: 300
  },
  {
    country: 'Mali',
    children: [
      {
        id: 'Africa/Bamako',
        name: 'Africa/Bamako',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Malta',
    children: [
      {
        id: 'Europe/Malta',
        name: 'Europe/Malta',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Marshall Islands',
    children: [
      {
        id: 'Pacific/Kwajalein',
        name: 'Pacific/Kwajalein',
        offset: 'UTC+12:00',
        nOffset: 720
      },
      {
        id: 'Pacific/Majuro',
        name: 'Pacific/Majuro',
        offset: 'UTC+12:00',
        nOffset: 720
      }
    ],
    firstNOffset: 720
  },
  {
    country: 'Martinique',
    children: [
      {
        id: 'America/Martinique',
        name: 'America/Martinique',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Mauritania',
    children: [
      {
        id: 'Africa/Nouakchott',
        name: 'Africa/Nouakchott',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Mauritius',
    children: [
      {
        id: 'Indian/Mauritius',
        name: 'Indian/Mauritius',
        offset: 'UTC+04:00',
        nOffset: 240
      }
    ],
    firstNOffset: 240
  },
  {
    country: 'Mayotte',
    children: [
      {
        id: 'Indian/Mayotte',
        name: 'Indian/Mayotte',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Mexico',
    children: [
      {
        id: 'America/Bahia_Banderas',
        name: 'America/Bahia Banderas',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Cancun',
        name: 'America/Cancun',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Chihuahua',
        name: 'America/Chihuahua',
        offset: 'UTC-06:00',
        nOffset: -360
      },
      {
        id: 'America/Hermosillo',
        name: 'America/Hermosillo',
        offset: 'UTC-07:00',
        nOffset: -420
      },
      {
        id: 'America/Matamoros',
        name: 'America/Matamoros',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Mazatlan',
        name: 'America/Mazatlan',
        offset: 'UTC-06:00',
        nOffset: -360
      },
      {
        id: 'America/Merida',
        name: 'America/Merida',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Mexico_City',
        name: 'America/Mexico City',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Monterrey',
        name: 'America/Monterrey',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Ojinaga',
        name: 'America/Ojinaga',
        offset: 'UTC-06:00',
        nOffset: -360
      },
      {
        id: 'America/Santa_Isabel',
        name: 'America/Santa Isabel',
        offset: 'UTC-07:00',
        nOffset: -420
      },
      {
        id: 'America/Tijuana',
        name: 'America/Tijuana',
        offset: 'UTC-07:00',
        nOffset: -420
      }
    ],
    firstNOffset: -300
  },
  {
    country: 'Micronesia (Federated States Of)',
    children: [
      {
        id: 'Pacific/Chuuk',
        name: 'Pacific/Chuuk',
        offset: 'UTC+10:00',
        nOffset: 600
      },
      {
        id: 'Pacific/Kosrae',
        name: 'Pacific/Kosrae',
        offset: 'UTC+11:00',
        nOffset: 660
      },
      {
        id: 'Pacific/Pohnpei',
        name: 'Pacific/Pohnpei',
        offset: 'UTC+11:00',
        nOffset: 660
      }
    ],
    firstNOffset: 600
  },
  {
    country: 'Moldova',
    children: [
      {
        id: 'Europe/Chisinau',
        name: 'Europe/Chisinau',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Monaco',
    children: [
      {
        id: 'Europe/Monaco',
        name: 'Europe/Monaco',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Mongolia',
    children: [
      {
        id: 'Asia/Choibalsan',
        name: 'Asia/Choibalsan',
        offset: 'UTC+08:00',
        nOffset: 480
      },
      {
        id: 'Asia/Hovd',
        name: 'Asia/Hovd',
        offset: 'UTC+07:00',
        nOffset: 420
      },
      {
        id: 'Asia/Ulaanbaatar',
        name: 'Asia/Ulaanbaatar',
        offset: 'UTC+08:00',
        nOffset: 480
      }
    ],
    firstNOffset: 480
  },
  {
    country: 'Montenegro',
    children: [
      {
        id: 'Europe/Podgorica',
        name: 'Europe/Podgorica',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Montserrat',
    children: [
      {
        id: 'America/Montserrat',
        name: 'America/Montserrat',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Morocco',
    children: [
      {
        id: 'Africa/Casablanca',
        name: 'Africa/Casablanca',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Mozambique',
    children: [
      {
        id: 'Africa/Maputo',
        name: 'Africa/Maputo',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Myanmar',
    children: [
      {
        id: 'Asia/Rangoon',
        name: 'Asia/Rangoon',
        offset: 'UTC+06:30',
        nOffset: 390
      }
    ],
    firstNOffset: 390
  },
  {
    country: 'Namibia',
    children: [
      {
        id: 'Africa/Windhoek',
        name: 'Africa/Windhoek',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Nauru',
    children: [
      {
        id: 'Pacific/Nauru',
        name: 'Pacific/Nauru',
        offset: 'UTC+12:00',
        nOffset: 720
      }
    ],
    firstNOffset: 720
  },
  {
    country: 'Nepal',
    children: [
      {
        id: 'Asia/Kathmandu',
        name: 'Asia/Kathmandu',
        offset: 'UTC+05:45',
        nOffset: 345
      }
    ],
    firstNOffset: 345
  },
  {
    country: 'Netherlands',
    children: [
      {
        id: 'Europe/Amsterdam',
        name: 'Europe/Amsterdam',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'New Caledonia',
    children: [
      {
        id: 'Pacific/Noumea',
        name: 'Pacific/Noumea',
        offset: 'UTC+11:00',
        nOffset: 660
      }
    ],
    firstNOffset: 660
  },
  {
    country: 'New Zealand',
    children: [
      {
        id: 'Pacific/Auckland',
        name: 'Pacific/Auckland',
        offset: 'UTC+12:00',
        nOffset: 720
      },
      {
        id: 'Pacific/Chatham',
        name: 'Pacific/Chatham',
        offset: 'UTC+12:45',
        nOffset: 765
      }
    ],
    firstNOffset: 720
  },
  {
    country: 'Nicaragua',
    children: [
      {
        id: 'America/Managua',
        name: 'America/Managua',
        offset: 'UTC-06:00',
        nOffset: -360
      }
    ],
    firstNOffset: -360
  },
  {
    country: 'Niger',
    children: [
      {
        id: 'Africa/Niamey',
        name: 'Africa/Niamey',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Nigeria',
    children: [
      {
        id: 'Africa/Lagos',
        name: 'Africa/Lagos',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Niue',
    children: [
      {
        id: 'Pacific/Niue',
        name: 'Pacific/Niue',
        offset: 'UTC-11:00',
        nOffset: -660
      }
    ],
    firstNOffset: -660
  },
  {
    country: 'Norfolk Island',
    children: [
      {
        id: 'Pacific/Norfolk',
        name: 'Pacific/Norfolk',
        offset: 'UTC+11:00',
        nOffset: 660
      }
    ],
    firstNOffset: 660
  },
  {
    country: 'North Korea',
    children: [
      {
        id: 'Asia/Pyongyang',
        name: 'Asia/Pyongyang',
        offset: 'UTC+09:00',
        nOffset: 540
      }
    ],
    firstNOffset: 540
  },
  {
    country: 'Northern Mariana Islands',
    children: [
      {
        id: 'Pacific/Saipan',
        name: 'Pacific/Saipan',
        offset: 'UTC+10:00',
        nOffset: 600
      }
    ],
    firstNOffset: 600
  },
  {
    country: 'Norway',
    children: [
      {
        id: 'Europe/Oslo',
        name: 'Europe/Oslo',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Oman',
    children: [
      {
        id: 'Asia/Muscat',
        name: 'Asia/Muscat',
        offset: 'UTC+04:00',
        nOffset: 240
      }
    ],
    firstNOffset: 240
  },
  {
    country: 'Pakistan',
    children: [
      {
        id: 'Asia/Karachi',
        name: 'Asia/Karachi',
        offset: 'UTC+05:00',
        nOffset: 300
      }
    ],
    firstNOffset: 300
  },
  {
    country: 'Palau',
    children: [
      {
        id: 'Pacific/Palau',
        name: 'Pacific/Palau',
        offset: 'UTC+09:00',
        nOffset: 540
      }
    ],
    firstNOffset: 540
  },
  {
    country: 'Palestinian Territory (Occupied)',
    children: [
      {
        id: 'Asia/Gaza',
        name: 'Asia/Gaza',
        offset: 'UTC+03:00',
        nOffset: 180
      },
      {
        id: 'Asia/Hebron',
        name: 'Asia/Hebron',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Panama',
    children: [
      {
        id: 'America/Panama',
        name: 'America/Panama',
        offset: 'UTC-05:00',
        nOffset: -300
      }
    ],
    firstNOffset: -300
  },
  {
    country: 'Papua New Guinea',
    children: [
      {
        id: 'Pacific/Port_Moresby',
        name: 'Pacific/Port Moresby',
        offset: 'UTC+10:00',
        nOffset: 600
      }
    ],
    firstNOffset: 600
  },
  {
    country: 'Paraguay',
    children: [
      {
        id: 'America/Asuncion',
        name: 'America/Asuncion',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Peru',
    children: [
      {
        id: 'America/Lima',
        name: 'America/Lima',
        offset: 'UTC-05:00',
        nOffset: -300
      }
    ],
    firstNOffset: -300
  },
  {
    country: 'Philippines',
    children: [
      {
        id: 'Asia/Manila',
        name: 'Asia/Manila',
        offset: 'UTC+08:00',
        nOffset: 480
      }
    ],
    firstNOffset: 480
  },
  {
    country: 'Pitcairn',
    children: [
      {
        id: 'Pacific/Pitcairn',
        name: 'Pacific/Pitcairn',
        offset: 'UTC-08:00',
        nOffset: -480
      }
    ],
    firstNOffset: -480
  },
  {
    country: 'Poland',
    children: [
      {
        id: 'Europe/Warsaw',
        name: 'Europe/Warsaw',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Portugal',
    children: [
      {
        id: 'Atlantic/Azores',
        name: 'Atlantic/Azores',
        offset: 'UTC+00:00',
        nOffset: 0
      },
      {
        id: 'Atlantic/Madeira',
        name: 'Atlantic/Madeira',
        offset: 'UTC+01:00',
        nOffset: 60
      },
      {
        id: 'Europe/Lisbon',
        name: 'Europe/Lisbon',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Puerto Rico',
    children: [
      {
        id: 'America/Puerto_Rico',
        name: 'America/Puerto Rico',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Qatar',
    children: [
      {
        id: 'Asia/Qatar',
        name: 'Asia/Qatar',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Reunion',
    children: [
      {
        id: 'Indian/Reunion',
        name: 'Indian/Reunion',
        offset: 'UTC+04:00',
        nOffset: 240
      }
    ],
    firstNOffset: 240
  },
  {
    country: 'Romania',
    children: [
      {
        id: 'Europe/Bucharest',
        name: 'Europe/Bucharest',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Russian Federation',
    children: [
      {
        id: 'Asia/Anadyr',
        name: 'Asia/Anadyr',
        offset: 'UTC+12:00',
        nOffset: 720
      },
      {
        id: 'Asia/Irkutsk',
        name: 'Asia/Irkutsk',
        offset: 'UTC+08:00',
        nOffset: 480
      },
      {
        id: 'Asia/Kamchatka',
        name: 'Asia/Kamchatka',
        offset: 'UTC+12:00',
        nOffset: 720
      },
      {
        id: 'Asia/Khandyga',
        name: 'Asia/Khandyga',
        offset: 'UTC+09:00',
        nOffset: 540
      },
      {
        id: 'Asia/Krasnoyarsk',
        name: 'Asia/Krasnoyarsk',
        offset: 'UTC+07:00',
        nOffset: 420
      },
      {
        id: 'Asia/Magadan',
        name: 'Asia/Magadan',
        offset: 'UTC+11:00',
        nOffset: 660
      },
      {
        id: 'Asia/Novokuznetsk',
        name: 'Asia/Novokuznetsk',
        offset: 'UTC+07:00',
        nOffset: 420
      },
      {
        id: 'Asia/Novosibirsk',
        name: 'Asia/Novosibirsk',
        offset: 'UTC+07:00',
        nOffset: 420
      },
      {
        id: 'Asia/Omsk',
        name: 'Asia/Omsk',
        offset: 'UTC+06:00',
        nOffset: 360
      },
      {
        id: 'Asia/Sakhalin',
        name: 'Asia/Sakhalin',
        offset: 'UTC+11:00',
        nOffset: 660
      },
      {
        id: 'Asia/Ust-Nera',
        name: 'Asia/Ust-Nera',
        offset: 'UTC+10:00',
        nOffset: 600
      },
      {
        id: 'Asia/Vladivostok',
        name: 'Asia/Vladivostok',
        offset: 'UTC+10:00',
        nOffset: 600
      },
      {
        id: 'Asia/Yakutsk',
        name: 'Asia/Yakutsk',
        offset: 'UTC+09:00',
        nOffset: 540
      },
      {
        id: 'Asia/Yekaterinburg',
        name: 'Asia/Yekaterinburg',
        offset: 'UTC+05:00',
        nOffset: 300
      },
      {
        id: 'Europe/Kaliningrad',
        name: 'Europe/Kaliningrad',
        offset: 'UTC+02:00',
        nOffset: 120
      },
      {
        id: 'Europe/Moscow',
        name: 'Europe/Moscow',
        offset: 'UTC+03:00',
        nOffset: 180
      },
      {
        id: 'Europe/Samara',
        name: 'Europe/Samara',
        offset: 'UTC+04:00',
        nOffset: 240
      },
      {
        id: 'Europe/Simferopol',
        name: 'Europe/Simferopol',
        offset: 'UTC+03:00',
        nOffset: 180
      },
      {
        id: 'Europe/Volgograd',
        name: 'Europe/Volgograd',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 720
  },
  {
    country: 'Rwanda',
    children: [
      {
        id: 'Africa/Kigali',
        name: 'Africa/Kigali',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Saint Barthelemy',
    children: [
      {
        id: 'America/St_Barthelemy',
        name: 'America/St Barthelemy',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Saint Helena',
    children: [
      {
        id: 'Atlantic/St_Helena',
        name: 'Atlantic/St Helena',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Saint Kitts And Nevis',
    children: [
      {
        id: 'America/St_Kitts',
        name: 'America/St Kitts',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Saint Lucia',
    children: [
      {
        id: 'America/St_Lucia',
        name: 'America/St Lucia',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Saint Martin',
    children: [
      {
        id: 'America/Marigot',
        name: 'America/Marigot',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Saint Pierre And Miquelon',
    children: [
      {
        id: 'America/Miquelon',
        name: 'America/Miquelon',
        offset: 'UTC-02:00',
        nOffset: -120
      }
    ],
    firstNOffset: -120
  },
  {
    country: 'Saint Vincent And Grenadines',
    children: [
      {
        id: 'America/St_Vincent',
        name: 'America/St Vincent',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Samoa',
    children: [
      {
        id: 'Pacific/Apia',
        name: 'Pacific/Apia',
        offset: 'UTC+13:00',
        nOffset: 780
      }
    ],
    firstNOffset: 780
  },
  {
    country: 'San Marino',
    children: [
      {
        id: 'Europe/San_Marino',
        name: 'Europe/San Marino',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Sao Tome And Principe',
    children: [
      {
        id: 'Africa/Sao_Tome',
        name: 'Africa/Sao Tome',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Saudi Arabia',
    children: [
      {
        id: 'Asia/Riyadh',
        name: 'Asia/Riyadh',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Senegal',
    children: [
      {
        id: 'Africa/Dakar',
        name: 'Africa/Dakar',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Serbia',
    children: [
      {
        id: 'Europe/Belgrade',
        name: 'Europe/Belgrade',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Seychelles',
    children: [
      {
        id: 'Indian/Mahe',
        name: 'Indian/Mahe',
        offset: 'UTC+04:00',
        nOffset: 240
      }
    ],
    firstNOffset: 240
  },
  {
    country: 'Sierra Leone',
    children: [
      {
        id: 'Africa/Freetown',
        name: 'Africa/Freetown',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Singapore',
    children: [
      {
        id: 'Asia/Singapore',
        name: 'Asia/Singapore',
        offset: 'UTC+08:00',
        nOffset: 480
      }
    ],
    firstNOffset: 480
  },
  {
    country: 'Sint Maarten',
    children: [
      {
        id: 'America/Lower_Princes',
        name: 'America/Lower Princes',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Slovakia',
    children: [
      {
        id: 'Europe/Bratislava',
        name: 'Europe/Bratislava',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Slovenia',
    children: [
      {
        id: 'Europe/Ljubljana',
        name: 'Europe/Ljubljana',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Solomon Islands',
    children: [
      {
        id: 'Pacific/Guadalcanal',
        name: 'Pacific/Guadalcanal',
        offset: 'UTC+11:00',
        nOffset: 660
      }
    ],
    firstNOffset: 660
  },
  {
    country: 'Somalia',
    children: [
      {
        id: 'Africa/Mogadishu',
        name: 'Africa/Mogadishu',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'South Africa',
    children: [
      {
        id: 'Africa/Johannesburg',
        name: 'Africa/Johannesburg',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'South Georgia And Sandwich Isl.',
    children: [
      {
        id: 'Atlantic/South_Georgia',
        name: 'Atlantic/South Georgia',
        offset: 'UTC-02:00',
        nOffset: -120
      }
    ],
    firstNOffset: -120
  },
  {
    country: 'South Sudan',
    children: [
      {
        id: 'Africa/Juba',
        name: 'Africa/Juba',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Spain',
    children: [
      {
        id: 'Africa/Ceuta',
        name: 'Africa/Ceuta',
        offset: 'UTC+02:00',
        nOffset: 120
      },
      {
        id: 'Atlantic/Canary',
        name: 'Atlantic/Canary',
        offset: 'UTC+01:00',
        nOffset: 60
      },
      {
        id: 'Europe/Madrid',
        name: 'Europe/Madrid',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Sri Lanka',
    children: [
      {
        id: 'Asia/Colombo',
        name: 'Asia/Colombo',
        offset: 'UTC+05:30',
        nOffset: 330
      }
    ],
    firstNOffset: 330
  },
  {
    country: 'Sudan',
    children: [
      {
        id: 'Africa/Khartoum',
        name: 'Africa/Khartoum',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Suriname',
    children: [
      {
        id: 'America/Paramaribo',
        name: 'America/Paramaribo',
        offset: 'UTC-03:00',
        nOffset: -180
      }
    ],
    firstNOffset: -180
  },
  {
    country: 'Svalbard And Jan Mayen',
    children: [
      {
        id: 'Arctic/Longyearbyen',
        name: 'Arctic/Longyearbyen',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Swaziland',
    children: [
      {
        id: 'Africa/Mbabane',
        name: 'Africa/Mbabane',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Sweden',
    children: [
      {
        id: 'Europe/Stockholm',
        name: 'Europe/Stockholm',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Switzerland',
    children: [
      {
        id: 'Europe/Zurich',
        name: 'Europe/Zurich',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Syrian Arab Republic',
    children: [
      {
        id: 'Asia/Damascus',
        name: 'Asia/Damascus',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Taiwan',
    children: [
      {
        id: 'Asia/Taipei',
        name: 'Asia/Taipei',
        offset: 'UTC+08:00',
        nOffset: 480
      }
    ],
    firstNOffset: 480
  },
  {
    country: 'Tajikistan',
    children: [
      {
        id: 'Asia/Dushanbe',
        name: 'Asia/Dushanbe',
        offset: 'UTC+05:00',
        nOffset: 300
      }
    ],
    firstNOffset: 300
  },
  {
    country: 'Tanzania',
    children: [
      {
        id: 'Africa/Dar_es_Salaam',
        name: 'Africa/Dar es Salaam',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Thailand',
    children: [
      {
        id: 'Asia/Bangkok',
        name: 'Asia/Bangkok',
        offset: 'UTC+07:00',
        nOffset: 420
      }
    ],
    firstNOffset: 420
  },
  {
    country: 'Timor-Leste',
    children: [
      {
        id: 'Asia/Dili',
        name: 'Asia/Dili',
        offset: 'UTC+09:00',
        nOffset: 540
      }
    ],
    firstNOffset: 540
  },
  {
    country: 'Togo',
    children: [
      {
        id: 'Africa/Lome',
        name: 'Africa/Lome',
        offset: 'UTC+00:00',
        nOffset: 0
      }
    ],
    firstNOffset: 0
  },
  {
    country: 'Tokelau',
    children: [
      {
        id: 'Pacific/Fakaofo',
        name: 'Pacific/Fakaofo',
        offset: 'UTC+13:00',
        nOffset: 780
      }
    ],
    firstNOffset: 780
  },
  {
    country: 'Tonga',
    children: [
      {
        id: 'Pacific/Tongatapu',
        name: 'Pacific/Tongatapu',
        offset: 'UTC+13:00',
        nOffset: 780
      }
    ],
    firstNOffset: 780
  },
  {
    country: 'Trinidad And Tobago',
    children: [
      {
        id: 'America/Port_of_Spain',
        name: 'America/Port of Spain',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Tunisia',
    children: [
      {
        id: 'Africa/Tunis',
        name: 'Africa/Tunis',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Turkey',
    children: [
      {
        id: 'Europe/Istanbul',
        name: 'Europe/Istanbul',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Turkmenistan',
    children: [
      {
        id: 'Asia/Ashgabat',
        name: 'Asia/Ashgabat',
        offset: 'UTC+05:00',
        nOffset: 300
      }
    ],
    firstNOffset: 300
  },
  {
    country: 'Turks And Caicos Islands',
    children: [
      {
        id: 'America/Grand_Turk',
        name: 'America/Grand Turk',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Tuvalu',
    children: [
      {
        id: 'Pacific/Funafuti',
        name: 'Pacific/Funafuti',
        offset: 'UTC+12:00',
        nOffset: 720
      }
    ],
    firstNOffset: 720
  },
  {
    country: 'Uganda',
    children: [
      {
        id: 'Africa/Kampala',
        name: 'Africa/Kampala',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Ukraine',
    children: [
      {
        id: 'Europe/Kiev',
        name: 'Europe/Kiev',
        offset: 'UTC+03:00',
        nOffset: 180
      },
      {
        id: 'Europe/Uzhgorod',
        name: 'Europe/Uzhgorod',
        offset: 'UTC+03:00',
        nOffset: 180
      },
      {
        id: 'Europe/Zaporozhye',
        name: 'Europe/Zaporozhye',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'United Arab Emirates',
    children: [
      {
        id: 'Asia/Dubai',
        name: 'Asia/Dubai',
        offset: 'UTC+04:00',
        nOffset: 240
      }
    ],
    firstNOffset: 240
  },
  {
    country: 'United Kingdom',
    children: [
      {
        id: 'Europe/London',
        name: 'Europe/London',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'United States',
    children: [
      {
        id: 'America/Adak',
        name: 'America/Adak',
        offset: 'UTC-09:00',
        nOffset: -540
      },
      {
        id: 'America/Anchorage',
        name: 'America/Anchorage',
        offset: 'UTC-08:00',
        nOffset: -480
      },
      {
        id: 'America/Boise',
        name: 'America/Boise',
        offset: 'UTC-06:00',
        nOffset: -360
      },
      {
        id: 'America/Chicago',
        name: 'America/Chicago',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Denver',
        name: 'America/Denver',
        offset: 'UTC-06:00',
        nOffset: -360
      },
      {
        id: 'America/Detroit',
        name: 'America/Detroit',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Indiana/Indianapolis',
        name: 'America/Indiana/Indianapolis',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Indiana/Knox',
        name: 'America/Indiana/Knox',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Indiana/Marengo',
        name: 'America/Indiana/Marengo',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Indiana/Petersburg',
        name: 'America/Indiana/Petersburg',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Indiana/Tell_City',
        name: 'America/Indiana/Tell City',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Indiana/Vevay',
        name: 'America/Indiana/Vevay',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Indiana/Vincennes',
        name: 'America/Indiana/Vincennes',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Indiana/Winamac',
        name: 'America/Indiana/Winamac',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Juneau',
        name: 'America/Juneau',
        offset: 'UTC-08:00',
        nOffset: -480
      },
      {
        id: 'America/Kentucky/Louisville',
        name: 'America/Kentucky/Louisville',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Kentucky/Monticello',
        name: 'America/Kentucky/Monticello',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Los_Angeles',
        name: 'America/Los Angeles',
        offset: 'UTC-07:00',
        nOffset: -420
      },
      {
        id: 'America/Menominee',
        name: 'America/Menominee',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Metlakatla',
        name: 'America/Metlakatla',
        offset: 'UTC-08:00',
        nOffset: -480
      },
      {
        id: 'America/New_York',
        name: 'America/New York',
        offset: 'UTC-04:00',
        nOffset: -240
      },
      {
        id: 'America/Nome',
        name: 'America/Nome',
        offset: 'UTC-08:00',
        nOffset: -480
      },
      {
        id: 'America/North_Dakota/Beulah',
        name: 'America/North Dakota/Beulah',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/North_Dakota/Center',
        name: 'America/North Dakota/Center',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/North_Dakota/New_Salem',
        name: 'America/North Dakota/New Salem',
        offset: 'UTC-05:00',
        nOffset: -300
      },
      {
        id: 'America/Phoenix',
        name: 'America/Phoenix',
        offset: 'UTC-07:00',
        nOffset: -420
      },
      {
        id: 'America/Sitka',
        name: 'America/Sitka',
        offset: 'UTC-08:00',
        nOffset: -480
      },
      {
        id: 'America/Yakutat',
        name: 'America/Yakutat',
        offset: 'UTC-08:00',
        nOffset: -480
      },
      {
        id: 'Pacific/Honolulu',
        name: 'Pacific/Honolulu',
        offset: 'UTC-10:00',
        nOffset: -600
      }
    ],
    firstNOffset: -540
  },
  {
    country: 'United States Outlying Islands',
    children: [
      {
        id: 'Pacific/Johnston',
        name: 'Pacific/Johnston',
        offset: 'UTC-10:00',
        nOffset: -600
      },
      {
        id: 'Pacific/Midway',
        name: 'Pacific/Midway',
        offset: 'UTC-11:00',
        nOffset: -660
      },
      {
        id: 'Pacific/Wake',
        name: 'Pacific/Wake',
        offset: 'UTC+12:00',
        nOffset: 720
      }
    ],
    firstNOffset: -600
  },
  {
    country: 'Uruguay',
    children: [
      {
        id: 'America/Montevideo',
        name: 'America/Montevideo',
        offset: 'UTC-03:00',
        nOffset: -180
      }
    ],
    firstNOffset: -180
  },
  {
    country: 'Uzbekistan',
    children: [
      {
        id: 'Asia/Samarkand',
        name: 'Asia/Samarkand',
        offset: 'UTC+05:00',
        nOffset: 300
      },
      {
        id: 'Asia/Tashkent',
        name: 'Asia/Tashkent',
        offset: 'UTC+05:00',
        nOffset: 300
      }
    ],
    firstNOffset: 300
  },
  {
    country: 'Vanuatu',
    children: [
      {
        id: 'Pacific/Efate',
        name: 'Pacific/Efate',
        offset: 'UTC+11:00',
        nOffset: 660
      }
    ],
    firstNOffset: 660
  },
  {
    country: 'Venezuela',
    children: [
      {
        id: 'America/Caracas',
        name: 'America/Caracas',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Viet Nam',
    children: [
      {
        id: 'Asia/Ho_Chi_Minh',
        name: 'Asia/Ho Chi Minh',
        offset: 'UTC+07:00',
        nOffset: 420
      }
    ],
    firstNOffset: 420
  },
  {
    country: 'Virgin Islands (British)',
    children: [
      {
        id: 'America/Tortola',
        name: 'America/Tortola',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Virgin Islands (U.S.)',
    children: [
      {
        id: 'America/St_Thomas',
        name: 'America/St Thomas',
        offset: 'UTC-04:00',
        nOffset: -240
      }
    ],
    firstNOffset: -240
  },
  {
    country: 'Wallis And Futuna',
    children: [
      {
        id: 'Pacific/Wallis',
        name: 'Pacific/Wallis',
        offset: 'UTC+12:00',
        nOffset: 720
      }
    ],
    firstNOffset: 720
  },
  {
    country: 'Western Sahara',
    children: [
      {
        id: 'Africa/El_Aaiun',
        name: 'Africa/El Aaiun',
        offset: 'UTC+01:00',
        nOffset: 60
      }
    ],
    firstNOffset: 60
  },
  {
    country: 'Yemen',
    children: [
      {
        id: 'Asia/Aden',
        name: 'Asia/Aden',
        offset: 'UTC+03:00',
        nOffset: 180
      }
    ],
    firstNOffset: 180
  },
  {
    country: 'Zambia',
    children: [
      {
        id: 'Africa/Lusaka',
        name: 'Africa/Lusaka',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  },
  {
    country: 'Zimbabwe',
    children: [
      {
        id: 'Africa/Harare',
        name: 'Africa/Harare',
        offset: 'UTC+02:00',
        nOffset: 120
      }
    ],
    firstNOffset: 120
  }
];
