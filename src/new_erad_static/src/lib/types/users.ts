export interface Supplicant {
  id: symbol;
  Account_ID: string;
  Description: string;
  DeviceName: string;
  ExpirationDate: string;
  Group_ID: string;
  Location: string;
  MaxDownloadSpeedBits: number;
  MaxUploadSpeedBits: number;
  Password: string;
  UseRemote: boolean;
  Username: string;
  Vlan: string;
}

export interface Audit {
  id: symbol;
  AccountGroup_ID: string;
  Date: string;
  Group_ID: string;
  Identifier: string;
  Instance: number;
  Message: string;
}

export interface RadiusLogs {
  id: symbol;
  Access: string;
  Username: string;
  Group_ID: string;
  CalledStationId: string;
  CallingStationId: string;
  EventTimestamp: string;
  NASIdentifier: string;
  NASIpAddress: string;
  IsProxied: boolean;
  Instance: number;
}
