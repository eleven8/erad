export interface EradSession {
  AccountOwner: number;
  Account_ID: string;
  Group_ID_List: string[];
  ID: string;
  Identifier: string;
}
