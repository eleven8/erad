export type SortDirection = 'none' | 'ascending' | 'descending';

export type TableKey = string;

export type TableValue = any;

export interface TableHeader {
  key: TableKey;
  value: TableValue;
  display?: (item: any) => TableValue;
  sort?: false | ((a: TableValue, b: TableValue) => number);
}

export interface TableRow {
  id: any;
  [key: string]: TableValue;
}

export interface TableCell {
  key: TableKey;
  value: TableValue;
  display?: (item: any) => TableValue;
}
