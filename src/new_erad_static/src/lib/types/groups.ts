export type RemoteAuthOptions = 'ElevenOS' | 'Marriott' | 'None' | 'Other';

export interface Group {
  Account_ID: string;
  ClientInfoArn?: string;
  Domain: string;
  ID: string;
  MaxDownloadSpeedBits: number;
  MaxUploadSpeedBits: number;
  Name: string;
  OverrideConnectionSpeed: boolean;
  OverrideVlan: boolean;
  RemoteServer?: string;
  RemoteServerUrl: string;
  SharedSecret: string;
  TimeZone: string;
  UniqueDevices?: number;
  highCapacityProvisioned: boolean;
}

export interface AddGroupPayload {
  ID: string;
  Name: string;
  TimeZone: string;
  RemoteServerUrl: string;
  SharedSecret: string;
}

export interface EditGroupPayload {
  ID: string;
  MaxDownloadSpeedBits: number;
  MaxUploadSpeedBits: number;
  Name: string;
  OverrideConnectionSpeed: boolean;
  OverrideVlan: boolean;
  RemoteServer: string;
  RemoteServerUrl: string;
  SharedSecret: string;
  TimeZone: string;
}

export interface GroupEndpoint {
  IpAddress: string;
  Port: string;
  Region: string;
  Secret: string;
}

export interface GroupAuthenticator {
  key: 'called-station-id' | 'nas-identifier';
  value: string;
  editing?: boolean;
}
