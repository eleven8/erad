export interface APIKeyList {
  DisplayName: string;
  ApiKeyName: string;
}

export interface APIKey {
  Active: 0 | 1;
  DisplayName: string;
  ApiKeyName?: string;
  ApiKey_ID?: string;
}
