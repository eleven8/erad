export interface TimeZone {
  country: string;
  children: Zone[];
  firstNOffset: number;
}

export interface Zone {
  id: string;
  name: string;
  offset: string;
  nOffset: number;
}
