import type { Group, GroupAuthenticator, GroupEndpoint } from '$lib/types/groups';
import { writable, type Writable } from 'svelte/store';

export const selectedGroup: Writable<Group> = writable({} as Group);
export const groups: Writable<Group[]> = writable([]);
export const endpoints: Writable<GroupEndpoint[]> = writable([]);
export const isEndpointGlobal: Writable<boolean> = writable(false);
export const authenticators: Writable<GroupAuthenticator[]> = writable([]);
