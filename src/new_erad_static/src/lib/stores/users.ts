import type { Audit, RadiusLogs, Supplicant } from '$lib/types/users';
import { writable, type Writable } from 'svelte/store';

export const supplicants: Writable<Supplicant[]> = writable([]);
export const audits: Writable<Audit[]> = writable([]);
export const radius_logs: Writable<RadiusLogs[]> = writable([]);
