import { writable, type Writable } from 'svelte/store';

export const accounts: Writable<string[]> = writable([]);
