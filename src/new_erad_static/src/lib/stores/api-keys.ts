import type { APIKeyList } from '$lib/types/api-key';
import { writable, type Writable } from 'svelte/store';

export const apikeys: Writable<APIKeyList[]> = writable([]);
