import { browser } from '$app/env';
import type { EradSession } from '$lib/types/session';
import { writable, type Writable } from 'svelte/store';

export const userSession: Writable<EradSession | null> = writable(
  browser ? JSON.parse(localStorage.getItem('userSession') || 'null') : null
);
userSession.subscribe((value) => {
  if (value) {
    browser && localStorage.setItem('userSession', JSON.stringify(value));
  } else {
    browser && localStorage.removeItem('userSession');
  }
});
