/// <reference types="@sveltejs/kit" />

// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces

declare global {
  declare namespace App {
    // interface Locals {}
    // interface Session {}
    // interface Platform {}
    // interface Stuff {}
  }

  interface ImportMetaEnv {
    ERAD_API_PATH: string;
  }
}
