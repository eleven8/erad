declare namespace svelte.JSX {
  interface HTMLAttributes<T> {
    oncomplete?: (event: any) => any;
    onaccept?: (event: any) => any;
  }
}
