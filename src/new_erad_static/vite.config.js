import { sveltekit } from '@sveltejs/kit/vite';

/** @type {import('vite').UserConfig} */
const config = {
  envPrefix: 'ERAD_',
  mode: process.env.NODE_ENV,
  plugins: [sveltekit()],
  ssr: {
    noExternal: ['chart.js']
  }
};

export default config;
