#!/bin/bash

cat << EOF > .env.development
ERAD_API_PATH=http://52.26.34.201:5000
EOF

cat << EOF > .env.production
ERAD_API_PATH=https://api.enterpriseauth.com
EOF
