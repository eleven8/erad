/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        primary: '#00629b'
      },
      fontFamily: {
        body: ['"Helvetica Neue"', 'Helvetica', 'Arial', 'sans-serif']
      }
    }
  },
  plugins: []
};
