#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
##
## This packing script produce a tar.gz file with the proper files for
## deployment and pushes them to s3 if it not a dev version.
##
################################################################################
set -x
export AWS_ACCESS_KEY_ID=AKIAIK5KT3VEGJFHTKOA   # Jenkins
# AWS_SECRET_ACCESS_KEY injected by Jenkins
export AWS_DEFAULT_REGION=us-west-2

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

VERSION=$1
if [ -z "$VERSION" ]
then
    VERSION="DEV"
fi

# pack the deployment files
rm -f erad_static_*.tar.gz

pushd $DIR/../../
echo "$VERSION" > version


# Note that archive also has files all-in-one.sh and all-in-one.fail.sh.
# These files make sense only on AIO stack.
# When UI is being deployed not in AIO stack, archive with erad_static is not used.

tar -cvzf "$DIR/erad_static_$VERSION.tar.gz" \
	--exclude="src/erad_static/node_modules" \
	src/erad_static \
	src/tools/dashboard/UI \
	deploy/erad_static_deploy_development.sh \
	deploy/all-in-one.sh \
	deploy/all-in-one.fail.sh
popd

# copy deployment archive to S3
if [ "$VERSION" != "DEV" ]
then
   aws s3 cp "$DIR/erad_static_$VERSION.tar.gz" s3://teamfortytwo.deploy/erad_static/erad_static_$VERSION.tar.gz
   aws s3 cp "$DIR/erad_static_$VERSION.tar.gz" s3://teamfortytwo.deploy/erad_static.dev.tar.gz
fi
