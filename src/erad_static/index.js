'use strict';

import './app/styles/main.css';
import './app/styles/dropdown.css';
import './app/styles/timezone-selector.css';

import './bower_components/bootstrap/dist/css/bootstrap.min.css'
import './bower_components/ng-table/dist/ng-table.min.css'
import './bower_components/font-awesome/css/font-awesome.css'
import './bower_components/angular-ui-bootstrap-datetimepicker/datetimepicker.css'
import './bower_components/angularjs-slider/dist/rzslider.css'

window.moment = require('moment');
window.moment = require('imports-loader?define=>false!' +
	'moment-timezone');

window.Spinner = require('spin.js');

window.$ = window.jQuery = require('jquery');
require('imports-loader?jQuery=>window.jQuery!' +
	'./bower_components/bootstrap/js/transition.js');
require('imports-loader?jQuery=>window.jQuery!' +
	'./bower_components/bootstrap/js/collapse.js');
require('imports-loader?jQuery=>window.jQuery!' +
	'./bower_components/bootstrap/js/tooltip.js');



require('imports-loader?this=>window,jQuery=>window.jQuery!' +
	'chosen-js');


window.angular = require('exports-loader?window.angular!imports-loader?jQuery=>window.jQuery!' +
	'angular');
require('imports-loader?angular=>window.angular!' +
	'angular-bootstrap');
require('imports-loader?define=>false,angular=>window.angular!' +
	'ng-table');
require('imports-loader?define=>false,angular=>window.angular!' +
	'ui.router');

window.Chart = require('imports-loader?this=>window!' +
	'chart.js');


require('imports-loader?define=>false,angular=>window.angular,Chart=>window.Chart!' +
	'angular-chart');

require('imports-loader?define=>false,angular=>window.angular!' +
	'angularjs-slider');

require('imports-loader?angular=>window.angular!' +
	'angular-ui-bootstrap-datetimepicker');




import './app/scripts/wua.js';



window.app_entry = require('exports-loader?services,directives,eradApp,eradControllers!' +
	'./app/scripts/app.js');

require('imports-loader?angular=>window.angular!' +
	'./app/scripts/config.js');

require('imports-loader?services=>window.app_entry["services"]!' +
	'./app/scripts/services.js');
require('imports-loader?eradApp=>window.app_entry["eradApp"],directives=>window.app_entry["directives"]!' +
	'./app/scripts/directives.js');
require('imports-loader?eradApp=>window.app_entry["eradApp"]!' +
	'./app/scripts/filters.js');

// general controllers

require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/header.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/aaa_log.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/login.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/panelHeader.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/users.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/groups.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/changeHistory.js');

require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/modalErrors.js');

// group controllers
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/groupModals/add.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/groupModals/delete.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/groupModals/edit.js');

require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/groupModals/confirmDeleteEndpointModal.js');

// user controllers
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/userModals/import.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/userModals/delete.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/userModals/addEditWizard.js');

// admin controllers

require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/admin/admin.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/admin/addModal.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/admin/deleteModal.js');
require('imports-loader?angular=>window.angular!' +
	'./app/scripts/controllers/admin/editModal.js');

