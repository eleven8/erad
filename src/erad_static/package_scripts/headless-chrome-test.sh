#!/bin/bash
set -x

# Current default test method in Jenkins is PhantomJS, it does not depends on headless Chromium.
# To minimize Jenkins workspace for UI tests and do not install headless Chromium
# these dev dependencies moved here from package.json.
# TODO: if default method to run UI test will be switched from PhantomJS to headless Chromium
#  move these dev dependencies back to `package.json`
npm install mocha-headless-chrome http-server@13.0.2

./node_modules/.bin/http-server -s -a 127.0.0.1 -p 37591 . &
pid=$!
# $@ - is needed to pass `-a no-sandbox` option because chrome inside docker does not support it
./node_modules/.bin/mocha-headless-chrome $@ -f http://127.0.0.1:37591/test/ -r xunit > xunit.xml
test_result=$?
kill -15 $pid
exit $test_result
