#!/bin/bash

# set -x
set -e

prod_distributionID="E1AQ5D7WO3B73D"

declare -A TARGET_S3_BUCKET
TARGET_S3_BUCKET['production']='admin.enterpriseauth.com'
TARGET_S3_BUCKET['staging']='staging.enterpriseauth.com'
TARGET_S3_BUCKET['development']='dev.enterpriseauth.com'

# build/bundle JavaScript/html files
npm run bundle

target=${1:-"development"}
target_bucket_id=${2:-${TARGET_S3_BUCKET[$target]}}
target_distribution_id=${3:-$prod_distributionID}

do_invalidation=$false

if [[ -z $target_bucket_id  ]]; then
	echo 'Target S3 bucket can not be empty, chose the right target or specify S3 buket id'
	echo 'deploy-static-files.sh <target> <s3_bucket_id> <cloudfront_distribution_id>'
	exit 1
fi

if [[ "$target" == 'production' ]]; then
	do_invalidation='true'
fi

# copy to target bucket
aws s3 cp ./dist/ "s3://${target_bucket_id}/" --recursive --acl public-read

# invalidate CloudFront cache
if [[ "$do_invalidation" == 'true' ]]; then

	aws cloudfront create-invalidation \
        --distribution-id ${target_distribution_id} \
        --paths \
        	/index.html
fi

