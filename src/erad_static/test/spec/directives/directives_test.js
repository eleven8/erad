'use strict';

/* New tests need to be added to  spec runner at ../index.html */

var expect = chai.expect;

describe('highCapacityRequired', function () {
    var scope, compile, validHTML, AdminService;

    validHTML = `<div>
        <input
                 ng-model="user.Username"
                 high-capacity-required>
          <p ng-if="view.usernameError">{{ view.usernameError }}</p>
        </div>`;

    beforeEach(module('directives'));
    beforeEach(module('services'));


    beforeEach(inject(function($injector){
        scope = $injector.get('$rootScope').$new();
        compile = $injector.get('$compile');
        AdminService = $injector.get('AdminService');
        scope.view = {};
    }));

    function create() {
        var elem, compiledElem;
        elem = angular.element(validHTML);
        compiledElem = compile(elem)(scope);
        scope.$digest();

        return compiledElem;
    }

    it('will display warning if device name is aalert and high capacity port required', function (done) {
        scope.user = {
            Username: 'aalert'
        };
        scope.AppModel = {
            highCapacityProvisioned: false
        };

        var el = create();
        expect(el.find('p').length).equals(1);
        expect(el.find('p').text()).equals('Staff alerting devices require a high-capacity port, which this site is not configured for. Please contact support.');
        done();
    });

    it('will display warning if device name contains aalert and high capacity port required', function (done) {
        scope.user = {
            Username: 'qaalertq'
        };
        scope.AppModel = {
            highCapacityProvisioned: false
        };

        var el = create();
        expect(el.find('p').length).equals(1);
        expect(el.find('p').text()).equals('Staff alerting devices require a high-capacity port, which this site is not configured for. Please contact support.');
        done();
    });

    it('will not display warning if device name is aalert and high capacity port is not required', function (done) {
        scope.user = {
            Username: 'aalert'
        };
        scope.AppModel = {
            highCapacityProvisioned: true
        };

        var el = create();
        expect(el.find('p').length).equals(0);
        done();
    });

    it('will not display warning if device name is not aalert', function (done) {
        scope.user = {
            Username: 'notaalert'
        };
        scope.AppModel = {
            highCapacityProvisioned: true
        };

        var el = create();
        expect(el.find('p').length).equals(0);
        done();
    });
});

describe('test weak passwords:', function () {

    var scope, compile, validHTML, $httpBackend, AdminService;

    validHTML = '<div><input ng-model="user.Password" \
            name="password" \
            type="text"  \
            no-weak-passwords></input>\
            <p ng-if="view.passwordError">error</p></div>';

    beforeEach(module('directives'));
    beforeEach(module('services'));


    beforeEach(inject(function($injector){
        scope = $injector.get('$rootScope').$new();
        compile = $injector.get('$compile');
        $httpBackend = $injector.get('$httpBackend');
        AdminService = $injector.get('AdminService');
    }));

    function create() {
        var elem, compiledElem;
        elem = angular.element(validHTML);
        compiledElem = compile(elem)(scope);
        scope.$digest();

        return compiledElem;
    }

    it('weak password triggering error', function (done) {

        $httpBackend.expectPOST("https://testing.example.org/erad/public/check_password").respond(200,{'pwned': true});

        scope.user = {
            Password: '123'
        };
        scope.view = {
            passwordError: false
        };

        var el = create();
        expect(el.find('p').length).equals(0)
        scope.$digest();
        $httpBackend.flush();
        expect(el.find('p').length).equals(1)
        done();
    });
    it('not weak passwordr', function (done) {

        $httpBackend.expectPOST("https://testing.example.org/erad/public/check_password").respond(200,{'pwned': false});

        scope.user = {
            Password: '123'
        };
        scope.view = {
            passwordError: false
        };

        var el = create();
        expect(el.find('p').length).equals(0)
        scope.$digest();
        $httpBackend.flush();
        expect(el.find('p').length).equals(0)
        done();
    });

});

