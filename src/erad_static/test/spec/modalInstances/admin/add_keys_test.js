'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('AdminAddModalCtrl - Add API keys branch', function() {

    var AdminAddModalCtrl;
    var SessionErrorService, AdminService, AdminAPIKeyService;
    var $httpBackend, $controller, $scope, $rootScope, $q;
    var modalSpy, errorSpy;
    var modalData;
    var mockModalInstance;

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
        $provide.service('modalData', function () {
            return {
                type: 'keys'
            };
        });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));

    beforeEach(inject(function ($injector) {
        SessionErrorService = $injector.get('SessionErrorService');
        AdminService        = $injector.get('AdminService');
        AdminAPIKeyService  = $injector.get('AdminAPIKeyService');

        $httpBackend        = $injector.get('$httpBackend');
        $controller         = $injector.get('$controller');
        $rootScope          = $injector.get('$rootScope');
        $q                  = $injector.get('$q');
        $scope              = $rootScope.$new();
        modalData           = $injector.get('modalData');

        mockModalInstance = {
            close: function (result) {
            },
            dismiss: function (msg) {
            }
        };

        AdminAddModalCtrl = $controller('AdminAddModalCtrl', {
            '$scope': $scope,
            '$modalInstance': mockModalInstance,
            'SessionErrorService': SessionErrorService,
            'AdminService': AdminService,
            'modalData': modalData,
            'AdminAPIKeyService': AdminAPIKeyService
        });

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    it('will set the modal view values correctly', function (done) {

        expect($scope.view.title).to.equal('Add API Key');
        expect($scope.view.action).to.equal('ADD');
        expect($scope.view.type).to.equal('keys');
        expect($scope.view.message).to.equal('Please supply some details');
        expect($scope.view.active).to.equal(1);

        done();
    });

    describe('submitForm()', function () {
        it('will close the modal with the correct data', function (done) {
            var modalSpy = sinon.stub(mockModalInstance, 'close');

            var formData = {
                $valid: true,
                newAPIKeyName: {
                    $viewValue: 'myKey12345'
                },
                newAPIKeyStatus: {
                    $viewValue: false
                }
            };

            $scope.submitForm(formData);

            expect(modalSpy.args[0][0]).to.deep.equal({
                action: 'ADD',
                data: {
                    DisplayName: 'myKey12345',
                    Active: 0
                },
                type: 'keys'
            });

            expect($scope.submitted).to.equal(false);

            modalSpy.restore();

            done();
        });

        it('will set the submitted variable if form is invalid', function (done) {

            var modalSpy = sinon.stub(mockModalInstance, 'close');

            var formData = {
                $valid: false
            };

            // initial state
            expect($scope.submitted).to.equal(false);

            $scope.submitForm(formData);

            expect(modalSpy.called).to.equal(false);

            // after submitof invalid form
            expect($scope.submitted).to.equal(true);

            modalSpy.restore();

            done();
        });
    });

    describe('cancelAdd()', function () {

        it('will close the modal', function (done) {
            var modalSpy = sinon.stub(mockModalInstance, 'dismiss');

            $scope.cancelAdd();

            expect(modalSpy.called).to.be.ok;
            expect(modalSpy.calledWith('close')).to.be.ok;

            modalSpy.restore();

            done();
        });

    });

});
