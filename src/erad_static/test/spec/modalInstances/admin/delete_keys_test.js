'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('AdminDeleteModalCtrl', function() {

    var AdminDeleteModalCtrl;
    var SessionErrorService, AdminService, AdminAPIKeyService;
    var $httpBackend, $controller, $scope, $rootScope, $q;
    var modalSpy, errorSpy;
    var modalData;
    var apiCall;
    var mockModalInstance;

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
        $provide.service('modalData', function () {
            return {
                type: 'keys',
                value: 'myKey12345'
            };
        });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));

    beforeEach(inject(function ($injector) {
        SessionErrorService = $injector.get('SessionErrorService');
        AdminService        = $injector.get('AdminService');
        AdminAPIKeyService  = $injector.get('AdminAPIKeyService');

        $httpBackend        = $injector.get('$httpBackend');
        $controller         = $injector.get('$controller');
        $rootScope          = $injector.get('$rootScope');
        $q                  = $injector.get('$q');
        $scope              = $rootScope.$new();
        modalData           = $injector.get('modalData');

        mockModalInstance = {
            close: function (result) {
            },
            dismiss: function (msg) {
            }
        };

        AdminDeleteModalCtrl = $controller('AdminDeleteModalCtrl', {
            '$scope': $scope,
            '$modalInstance': mockModalInstance,
            'SessionErrorService': SessionErrorService,
            'AdminService': AdminService,
            'modalData': modalData,
            'AdminAPIKeyService': AdminAPIKeyService
        });

    }));

    beforeEach(function () {
        apiCall = $httpBackend
            .expect('POST', 'https://testing.example.org/erad/admin/apikey/load')
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('API keys branch, successful call to endpoint', function () {

        beforeEach(function () {
            apiCall
                .respond({ Error: null, ApiKey: { ApiKeyName: 'key1', Active: 0 }});
            $httpBackend.flush();
        });

        it('will set the modal view values correctly', function (done) {

            expect($scope.view.title).to.equal('Delete API Key');
            expect($scope.view.action).to.equal('DELETE');
            expect($scope.view.type).to.equal('keys');
            expect($scope.view.message).to.equal('Warning. This action is not recoverable.');
            expect($scope.view.data.ApiKeyName).to.equal('key1');
            expect($scope.view.data.Active).to.equal(0);
            expect($scope.view.data.isActive).to.equal('No');

            done();
        });

        describe('remove()', function () {
            it('will close the modal with the correct data', function (done) {

                var reply = {
                    type: 'keys',
                    data: 'myKey12345'
                };

                modalSpy = sinon.stub(mockModalInstance, 'close');

                $scope.remove(reply.type, reply.data);

                expect(modalSpy.called).to.be.ok;

                modalSpy.restore();

                done();
            });
        });

        describe('close()', function () {

            it('will close the modal', function (done) {

                modalSpy = sinon.stub(mockModalInstance, 'dismiss');

                $scope.cancel();

                expect(modalSpy.called).to.be.ok;
                expect(modalSpy.calledWith('close')).to.be.ok;

                modalSpy.restore();

                done();
            });

        });
    });

    describe('API keys branch, call to endpoint fails', function () {

        it('will call an error', function (done) {

            apiCall.respond({ Error:  'failed to fetch keys' });

            var errorServiceSpy = sinon.stub(SessionErrorService, 'handleError');

            $httpBackend.flush();

            expect(errorServiceSpy.called).to.be.ok;
            expect(errorServiceSpy.args[0][0]).to.equal('failed to fetch keys');

            errorServiceSpy.restore();

            done();
        });

    });

});
