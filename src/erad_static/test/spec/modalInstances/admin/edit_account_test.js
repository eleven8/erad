'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('AdminEditModalCtrl - Edit Account branch', function() {

    var AdminEditModalCtrl;
    var SessionErrorService, AdminService, AdminAPIKeyService;
    var $httpBackend, $controller, $scope, $rootScope, $q;
    var modalSpy, errorSpy;
    var modalData;
    var mockModalInstance;
    var apiCall;

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
        $provide.service('modalData', function () {
            return {
                type: 'account'
            };
        });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));

    beforeEach(inject(function ($injector) {
        SessionErrorService = $injector.get('SessionErrorService');
        AdminService        = $injector.get('AdminService');
        AdminAPIKeyService  = $injector.get('AdminAPIKeyService');

        $httpBackend        = $injector.get('$httpBackend');
        $controller         = $injector.get('$controller');
        $rootScope          = $injector.get('$rootScope');
        $q                  = $injector.get('$q');
        $scope              = $rootScope.$new();
        modalData           = $injector.get('modalData');

        mockModalInstance = {
            close: function (result) {
            },
            dismiss: function (msg) {
            }
        };

        AdminEditModalCtrl = $controller('AdminEditModalCtrl', {
            '$scope': $scope,
            '$modalInstance': mockModalInstance,
            'SessionErrorService': SessionErrorService,
            'AdminService': AdminService,
            'modalData': modalData,
            'AdminAPIKeyService': AdminAPIKeyService
        });

    }));

    beforeEach(function () {
        apiCall = $httpBackend
            .expect('POST', 'https://testing.example.org/erad/admin/account/load')
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('After successful call to endpoint', function () {

        beforeEach(function () {
            apiCall
                .respond({ Error: null, AccountOwner: { Email: 'key1' }});
            $httpBackend.flush();
        });

        it('will set the modal view values correctly', function (done) {

            expect($scope.view.title).to.equal('Edit Account');
            expect($scope.view.action).to.equal('EDIT');
            expect($scope.view.type).to.equal('account');
            expect($scope.view.message).to.equal('Editing');

            done();
        });

        describe('submitForm()', function () {
            it('will close the modal with the correct data', function (done) {
                var modalSpy = sinon.stub(mockModalInstance, 'close');

                var formData = {
                    $valid: true,
                    accountEmail: {
                        $viewValue: 'my@email.com'
                    },
                    accountPassword: {
                        $viewValue: 'newPassword'
                    }
                };

                $scope.submitForm(formData);

                expect(modalSpy.args[0][0]).to.deep.equal({
                    action: 'EDIT',
                    data: {
                        Email: 'my@email.com',
                        Password: 'newPassword'
                    },
                    type: 'account'
                });

                expect($scope.submitted).to.equal(false);

                modalSpy.restore();

                done();
            });

            it('will set the submitted variable if form is invalid', function (done) {

                var modalSpy = sinon.stub(mockModalInstance, 'close');

                var formData = {
                    $valid: false
                };

                // initial state
                expect($scope.submitted).to.equal(false);

                $scope.submitForm(formData);

                expect(modalSpy.called).to.equal(false);

                // after submitof invalid form
                expect($scope.submitted).to.equal(true);

                modalSpy.restore();

                done();
            });
        });

        describe('close()', function () {

            it('will close the modal', function (done) {
                var modalSpy = sinon.stub(mockModalInstance, 'dismiss');

                $scope.close();

                expect(modalSpy.called).to.be.ok;
                expect(modalSpy.calledWith('close')).to.be.ok;

                modalSpy.restore();

                done();
            });

        });
    });

    describe('failed call to endpoint', function () {
        it('will call an error', function (done) {

            apiCall
                .respond({ Error: 'call failed' });

            errorSpy = sinon.stub(SessionErrorService, 'handleError');

            $httpBackend.flush();

            expect(errorSpy.called).to.be.ok;

            errorSpy.restore();

            done();
        });
    });

});
