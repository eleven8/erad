'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('AdminDeleteModalCtrl', function() {

    var AdminDeleteModalCtrl;
    var SessionErrorService, AdminService, AdminAPIKeyService;
    var $httpBackend, $controller, $scope, $rootScope, $q;
    var modalSpy, errorSpy;
    var apiCall;
    var mockModalInstance;

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
        $provide.service('modalData', function () {
            return {
                type: 'account',
                value: 'e@mail.com'
            };
        });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));

    beforeEach(inject(function ($injector) {
        SessionErrorService = $injector.get('SessionErrorService');
        AdminService        = $injector.get('AdminService');
        AdminAPIKeyService  = $injector.get('AdminAPIKeyService');

        $httpBackend        = $injector.get('$httpBackend');
        $controller         = $injector.get('$controller');
        $rootScope          = $injector.get('$rootScope');
        $q                  = $injector.get('$q');
        $scope              = $rootScope.$new();

        mockModalInstance = {
            close: function (result) {},
            dismiss: function (msg) {}
        };

        AdminDeleteModalCtrl = $controller('AdminDeleteModalCtrl', {
            '$scope': $scope,
            '$modalInstance': mockModalInstance,
            'SessionErrorService': SessionErrorService,
            'AdminService': AdminService,
            'AdminAPIKeyService': AdminAPIKeyService
        });

    }));

    beforeEach(function () {
        apiCall = $httpBackend
            .expect('POST', 'https://testing.example.org/erad/admin/account/load')
    });

    afterEach(function () {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('Account branch, successful call to endpoint', function () {

        beforeEach(function () {
            apiCall
                .respond({ Error: null, AccountOwner: { Email: 'key1' }});
            $httpBackend.flush();
        });

        it('will set the modal view values correctly', function (done) {

            expect($scope.view.title).to.equal('Delete Account');
            expect($scope.view.action).to.equal('DELETE');
            expect($scope.view.type).to.equal('account');
            expect($scope.view.message).to.equal('Warning. This action is not recoverable.');
            expect($scope.view.data.Email).to.equal('key1');

            done();
        });

        describe('remove()', function () {
            it('will close the modal with the correct data', function (done) {

                var reply = {
                    Email: 'e@mail.com'
                };

                modalSpy = sinon.stub(mockModalInstance, 'close');

                $scope.remove('account', reply);

                expect(modalSpy.called).to.be.ok;
                expect(modalSpy.args[0][0]).to.deep.equal({
                    data: 'e@mail.com',
                    type: 'account'
                });

                modalSpy.restore();

                done();
            });
        });

    });

    describe('Account branch, call to endpoint fails', function () {

        it('will call an error', function (done) {

            apiCall.respond({ Error:  'failed to fetch account' });

            errorSpy = sinon.stub(SessionErrorService, 'handleError');

            $httpBackend.flush();

            expect(errorSpy.called).to.be.ok;
            expect(errorSpy.args[0][0]).to.equal('failed to fetch account');

            errorSpy.restore();

            done();
        });

    });

});
