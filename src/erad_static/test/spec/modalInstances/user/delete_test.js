'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('DeleteUserCtrl', function() {

    var DeleteUserCtrl;
    var $httpBackend, $controller, $scope, $rootScope, $q;
    var modalSpy, errorSpy, userRemoveSpy, userListSpy;
    var UserService, SessionErrorService;
    var mockModalInstance;
    var user;

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
      $provide.service('user', function () {
        return { User: 'user', Username: 'username' };
      });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));
    beforeEach(inject(function ($injector) {
        UserService     = $injector.get('UserService');
        SessionErrorService = $injector.get('SessionErrorService');

        $httpBackend    = $injector.get('$httpBackend');
        $controller     = $injector.get('$controller');
        $rootScope      = $injector.get('$rootScope');
        $q              = $injector.get('$q');
        $scope          = $rootScope.$new();

        mockModalInstance = {
            close: function (result) {
            },
            dismiss: function (msg) {
            }
        };

        DeleteUserCtrl = $controller('DeleteUserCtrl', {
            '$scope': $scope,
            'SessionErrorService': SessionErrorService,
            'UserService': UserService,
            '$modalInstance': mockModalInstance
        });

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('controller setup', function () {

        it('will name the modal correctly', function (done) {

            expect($scope.user).to.deep.equal({ User: 'user', Username: 'username' });
            expect($scope.view.Title).to.equal('Delete Device');

            done();
        });
    });

    describe('close()', function () {

        it('will close the modal and not call the delete endpoint', function (done) {

            modalSpy = sinon.stub(mockModalInstance, 'dismiss');
            userRemoveSpy = sinon.stub(UserService, 'remove');

            $scope.close();

            expect(modalSpy.args[0][0]).to.equal('close');
            expect(userRemoveSpy.called).to.be.false;

            modalSpy.restore();
            userRemoveSpy.restore();

            done();

        });
    });

     describe('deleteUser()', function () {

        it('will call an error if delete fails', function (done) {

            userRemoveSpy = sinon.stub(UserService, 'remove').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'error' });
                return deferred.promise;
            });

            errorSpy = sinon.stub(SessionErrorService, 'handleError');

            modalSpy = sinon.stub(mockModalInstance, 'dismiss');

            $scope.deleteUser('wrongUsername');

            $scope.$apply();

            expect(modalSpy.args[0][0]).to.equal('close');
            expect(errorSpy.args[0][0]).to.deep.equal({ Error: 'error' });
            expect(userRemoveSpy.called).to.ok;

            userRemoveSpy.restore();
            errorSpy.restore();
            modalSpy.restore();

            done();
        });

        it('will call the list users endpoint if delete is successful', function (done) {

            userRemoveSpy = sinon.stub(UserService, 'remove').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            userListSpy = sinon.stub(UserService, 'list').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({});
                return deferred.promise;
            });

            errorSpy = sinon.stub(SessionErrorService, 'handleError');

            modalSpy = sinon.stub(mockModalInstance, 'dismiss');

            $scope.deleteUser('goodUsername');

            $scope.$apply();

            expect(modalSpy.args[0][0]).to.equal('close');
            expect(errorSpy.called).to.be.false;
            expect(userRemoveSpy.called).to.ok;

            userRemoveSpy.restore();
            userListSpy.restore();
            errorSpy.restore();
            modalSpy.restore();

            done();
        });
    });
});
