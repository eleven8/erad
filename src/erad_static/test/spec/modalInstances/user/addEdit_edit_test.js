'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('User modal - Update', function() {

    var UserWizardCtrl, Ctrl;
    var $httpBackend, $controller, $scope, $rootScope, $q;
    var modalSpy, errorSpy, userSaveSpy, userSaveMacSpy, userListSpy, endpointListSpy;
    var UserService, GroupService, SessionErrorService, AppModel, Utils, DateService;
    var mockModalInstance;
    var user, userlist;

    // saveOk_generator function to mock response with needed data
    var saveOk_generator = function(response_data){
        return function() {
            var deferred = $q.defer();
            deferred.resolve(response_data);
            return deferred.promise;
        };
    };

    var saveOk = saveOk_generator({
                        'Error' : null,
                        'Supplicant' : {
                                'Username': 'username'
                            }
                        });

    var endpoint_info = saveOk_generator(
        {"Endpoint": [
            {
                "IpAddress": "52.26.34.201",
                "Port": "20004",
                "Region": "us-west-1",
                "Secret": "secret20004"
            },
            {
                "IpAddress": "52.26.34.201",
                "Port": "20006",
                "Region": "us-west-1",
                "Secret": "secret20006"
            }
        ],
            "Global": false,
            'Error' : null
        }
    );


    var saveFail = function saveFail () {
        var deferred = $q.defer();
        deferred.resolve({ Error: 'error' });
        return deferred.promise;
    };

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
        $provide.service('user', function () {
            return { User: 'user', Username: 'username', UseRemote: false };
        });
        $provide.service('userlist', function () {
            return ['username'];
        });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers', 'directives'));
    beforeEach(inject(function ($injector) {
        UserService     = $injector.get('UserService');
        GroupService     = $injector.get('GroupService');
        SessionErrorService = $injector.get('SessionErrorService');
        AppModel        = $injector.get('AppModel');
        Utils           = $injector.get('Utils');
        DateService     = $injector.get('DateService');

        $httpBackend    = $injector.get('$httpBackend');
        $controller     = $injector.get('$controller');
        $rootScope      = $injector.get('$rootScope');
        $q              = $injector.get('$q');
        $scope          = $rootScope.$new();
        user            = $injector.get('user');

        mockModalInstance = {
            close: function (result) {
            },
            dismiss: function (msg) {
            }
        };

        $scope.userForm = {
            $setPristine: function (val) {},
            $valid: true,
            Expiration: {
                Enabled: false
            }
        };
        $scope.view = {};

        $scope.ModalMode = 'update';
        AppModel.timezone = 'America/Chicago';
        AppModel.selectedGroup = 'test';

        errorSpy = sinon.stub(SessionErrorService, 'handleError');

        userListSpy = sinon.stub(UserService, 'list').callsFake(saveOk);
        endpointListSpy = sinon.stub(GroupService, 'listEndpoints').callsFake(endpoint_info);

        modalSpy = sinon.stub(mockModalInstance, 'dismiss');
        UserWizardCtrl = $controller('UserWizardCtrl', {
            '$scope': $scope,
            'SessionErrorService': SessionErrorService,
            'UserService': UserService,
            'GroupService': GroupService,
            'AppModel': AppModel,
            'Utils': Utils,
            'user': user,
            'DateService': DateService,
            '$modalInstance': mockModalInstance
        });

        // $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/endpoint/load')
        //     .respond(200, { Error: null, Endpoint: { IpAddress:'52.26.34.201', Port:'1812', Region:'us-west', Secret:'4905DVCA3B' }, Global: true });
        // $httpBackend.flush();
    }));

    afterEach(function () {
        userListSpy.restore();
        errorSpy.restore();
        modalSpy.restore();
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    it('will setup the scope variables correctly', function (done) {

        expect($scope.user).to.deep.equal({ User: 'user', Username: 'username', UseRemote: false });
        expect($scope.view).to.not.be.undefined;
        expect($scope.view.Title).to.equal('Edit Device');
        expect($scope.view.Description).to.equal('Edit a device (supplicant) using the form below:');

        done();
    });

    describe('updateUser()', function () {

        it('will error if the update user form isn\'t valid', function (done) {

            $scope.userForm.$valid = false;

            // call with invalid form
            $scope.createUser($scope.userForm, $scope.user);

            expect($scope.submitted).to.be.true;

            done();
        });

        it('will not call UserService.save() unless a user object is passed', function (done) {

            var userSaveSpy = sinon.stub(UserService, 'save');

            $scope.userForm.$valid = true;

            // call createUser() without a user object
            $scope.createUser($scope.userForm);

            expect($scope.submitted).to.be.true;
            expect(userSaveSpy.called).to.be.false;

            userSaveSpy.restore();

            done();
        });

        it('will call an error if update UserService.save() fails', function (done) {

            $scope.userAuth = true; // testing not MAC auth

            var userSaveSpy = sinon.stub(UserService, 'save').callsFake(saveFail);

            $scope.userForm.$valid = true;

            $scope.updateUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(errorSpy.called).to.be.ok;

            userSaveSpy.restore();

            done();
        });

        it('inspecting that upload/download speed sets correctly', function (done) {

            $scope.userAuth = true;
            var userSaveSpy = sinon.stub(UserService, 'save').callsFake(saveOk);

            $scope.sliderUl.value = 1;
            $scope.sliderDl.value = 2;

            $scope.updateUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(userSaveSpy.called).to.be.ok;
            expect(userSaveSpy.args[0][0].MaxUploadSpeedBits).equals(1);
            expect(userSaveSpy.args[0][0].MaxDownloadSpeedBits).equals(2);
            expect(userListSpy.called).to.be.ok;
            expect(modalSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;

            userSaveSpy.restore();
            $scope.sliderUl.value = 0;
            $scope.sliderDl.value = 0;
            done();
        });

        it('will call UserService.save() when userAuth is true', function (done) {

            $scope.userAuth = true; // testing user auth, not MAC
            var userSaveSpy = sinon.stub(UserService, 'save').callsFake(saveOk);

            $scope.updateUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(userSaveSpy.called).to.be.ok;
            expect(userListSpy.called).to.be.ok;
            expect(modalSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;

            userSaveSpy.restore();

            done();
        });

        it('will call UserService.saveMac() when userAuth is false', function (done) {

            $scope.userAuth = false; // testing MAC auth this time

            var updated_user_mac = "11:22:33:^";

            var saveOk_MAC = saveOk_generator({
                        'Error' : null,
                        'Supplicant' : {
                                'Username': updated_user_mac
                            }
                        });

            var userSaveSpy = sinon.stub(UserService, 'saveMac').callsFake(saveOk_MAC);
            var userDeleteSpy = sinon.stub(UserService, 'remove').callsFake(saveOk_MAC);
            var otherSpy = sinon.stub(Utils, 'filterMac').callsFake(function () {
                return updated_user_mac;
            });

            $scope.updateUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(userSaveSpy.called).to.be.ok;
            expect(userListSpy.called).to.be.ok;
            expect(modalSpy.called).to.be.ok;
            expect(userDeleteSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;
            expect(otherSpy.called).to.be.true;

            userSaveSpy.restore();
            userDeleteSpy.restore();

            done();
        });

        it('will call UserService.remove() when the username has changed', function (done) {

            $scope.userAuth = true; // testing MAC auth this time

            var saveOk = saveOk_generator({
                        'Error' : null,
                        'Supplicant' : {
                                'Username': 'changed username'
                            }
                        });

            var userSaveSpy = sinon.stub(UserService, 'save').callsFake(saveOk);
            var userDeleteSpy = sinon.stub(UserService, 'remove').callsFake(saveOk);

            $scope.user.Username = 'changed username';
            $scope.updateUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(userSaveSpy.called).to.be.ok;
            expect(userDeleteSpy.called).to.be.ok;
            expect(modalSpy.called).to.be.ok;
            expect(userListSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;

            userSaveSpy.restore();
            userDeleteSpy.restore();

            done();
        });

        it('will not call UserService.remove() when the username is the same', function (done) {

            $scope.userAuth = true; // testing MAC auth this time

            var userSaveSpy = sinon.stub(UserService, 'save').callsFake(saveOk);
            var userDeleteSpy = sinon.stub(UserService, 'remove').callsFake(saveOk);

            // same as before
            $scope.user.Username = 'username';

            $scope.updateUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(userSaveSpy.called).to.be.ok;
            expect(userDeleteSpy.called).to.be.false;
            expect(modalSpy.called).to.be.ok;
            expect(userListSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;

            userSaveSpy.restore();
            userDeleteSpy.restore();

            done();
        });

        it('will not call UserService.remove() when the old and new username different in lower/upper cases', function (done) {

            $scope.userAuth = true;

            var userSaveSpy = sinon.stub(UserService, 'save').callsFake(saveOk);
            var userDeleteSpy = sinon.stub(UserService, 'remove').callsFake(saveOk);

            // same as before but case is differnet
            $scope.user.Username = 'UsErNaMe';

            $scope.updateUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(userSaveSpy.called).to.be.ok;
            expect(userDeleteSpy.called).to.be.false;
            expect(modalSpy.called).to.be.ok;
            expect(userListSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;

            userSaveSpy.restore();
            userDeleteSpy.restore();

            done();
        });

    });

});
