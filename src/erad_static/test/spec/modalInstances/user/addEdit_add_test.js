'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('User modal - Create', function() {

    var UserWizardCtrl;
    var $httpBackend, $controller, $scope, $rootScope, $q;
    var modalSpy, errorSpy, userSaveSpy, userSaveMacSpy, userListSpy, endpointListSpy;
    var UserService, GroupService, SessionErrorService, AppModel, Utils, DateService;
    var mockModalInstance;
    var user;

    var saveOk = function saveOk () {
        var deferred = $q.defer();
        deferred.resolve({ Error: null });
        return deferred.promise;
    };
    var saveFail = function saveFail () {
        var deferred = $q.defer();
        deferred.resolve({ Error: 'error' });
        return deferred.promise;
    };

    var saveOk_generator = function(response_data){
        return function() {
            var deferred = $q.defer();
            deferred.resolve(response_data);
            return deferred.promise;
        };
    };

    var endpoint_info = saveOk_generator(
        {"Endpoint": [
            {
                "IpAddress": "52.26.34.201",
                "Port": "20004",
                "Region": "us-west-1",
                "Secret": "secret20004"
            },
            {
                "IpAddress": "52.26.34.201",
                "Port": "20006",
                "Region": "us-west-1",
                "Secret": "secret20006"
            }
        ],
            "Global": false,
            'Error' : null
        }
    );

    var emptyUserTemplate = {
        'Username': '',
        'Vlan': '',
        'UseRemote': false,
        'Password': null,
        'MacAddress': '',
        'ExpirationDate' : '',
        'Location' : '',
        'Description' : '',
        'DeviceName' : '',
        'Mac': []
    };

    // function expectResponseWithEndpointPort(port=1812) {
    //     $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/endpoint/load')
    //         .respond(200, { Error: null, Endpoint: { IpAddress:'52.26.34.201', Port:port, Region:'us-west', Secret:'4905DVCA3B' }, Global: true });
    // }

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
        $provide.service('user', function () {
            // no user passed in by the calling controller
            // as this modal deals with adding new user
            return null;
        });
        $provide.service('userlist', function () {
            return [];
        });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers', 'directives'));
    beforeEach(inject(function ($injector) {
        UserService     = $injector.get('UserService');
        GroupService     = $injector.get('GroupService');
        SessionErrorService = $injector.get('SessionErrorService');
        AppModel        = $injector.get('AppModel');
        Utils           = $injector.get('Utils');
        DateService     = $injector.get('DateService');

        $httpBackend    = $injector.get('$httpBackend');
        $controller     = $injector.get('$controller');
        $rootScope      = $injector.get('$rootScope');
        $q              = $injector.get('$q');
        $scope          = $rootScope.$new();
        user            = $injector.get('user');

        mockModalInstance = {
            close: function (result) {
            },
            dismiss: function (msg) {
            }
        };

        $scope.userForm = {
            $setPristine: function (val) {},
            $valid: true,
            Expiration: {
                Enabled: false
            },
        };
        $scope.view = {};

        $scope.ModalMode = 'create';
        AppModel.timezone = 'America/Chicago';
        AppModel.selectedGroup = 'test';

        errorSpy = sinon.stub(SessionErrorService, 'handleError');

        userListSpy = sinon.stub(UserService, 'list').callsFake(saveOk);
        endpointListSpy = sinon.stub(GroupService, 'listEndpoints').callsFake(endpoint_info);

        modalSpy = sinon.stub(mockModalInstance, 'dismiss');

        UserWizardCtrl = $controller('UserWizardCtrl', {
            '$scope': $scope,
            'user': emptyUserTemplate,
            'SessionErrorService': SessionErrorService,
            'UserService': UserService,
            'GroupService': GroupService,
            'AppModel': AppModel,
            'Utils': Utils,
            'DateService': DateService,
            '$modalInstance': mockModalInstance
        });
    }));

    afterEach(function () {
        userListSpy.restore();
        errorSpy.restore();
        modalSpy.restore();
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    it('will setup the scope variables correctly', function (done) {
        // expectResponseWithEndpointPort();
        // $httpBackend.flush();

        expect($scope.Expiration.Enabled).to.not.be.ok;
        expect($scope.Expiration.Date).to.not.be.ok;
        //expect($scope.Expiration.TzShortname).to.not.be.ok;
        expect($scope.Expiration).to.be.ok;

        expect($scope.dateOptions).to.be.ok;
        expect($scope.dateOptions.startingDay).to.be.ok;
        expect($scope.dateOptions.showWeeks).to.be.false;

        expect($scope.user).to.deep.equal(emptyUserTemplate);
        expect($scope.view).to.not.be.undefined;
        expect($scope.view.Title).to.equal('Add New Device');
        expect($scope.view.Description).to.equal('Add a device (supplicant) to this group using the form below:');

        done();
    });

    describe('createUser()', function () {
        beforeEach(function() {
            // expectResponseWithEndpointPort();
            // $httpBackend.flush();
        });

        it('will error if the create user form isn\'t valid', function (done) {

            $scope.userForm.$valid = false;

            // call with invalid form
            $scope.createUser($scope.userForm, $scope.user);

            expect($scope.submitted).to.be.true;

            done();
        });

        it('will not call create user unless a user object is passed', function (done) {

            $scope.userForm.$valid = true;

            // call createUser() without a user object
            $scope.createUser($scope.userForm);

            expect($scope.submitted).to.be.true;

            done();
        });

        it('will call an error if save fails', function (done) {

            $scope.userAuth = true; // testing not MAC auth

            var userSaveSpy = sinon.stub(UserService, 'save').callsFake(saveFail);

            $scope.userForm.$valid = true;

            $scope.createUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(errorSpy.called).to.be.ok;

            userSaveSpy.restore();

            done();
        });

        it('inspecting that upload/download speed sets correctly', function (done) {

            $scope.userAuth = true;

            var userSaveSpy = sinon.stub(UserService, 'save').callsFake(saveOk);
            $scope.sliderUl.value = 1;
            $scope.sliderDl.value = 2;

            $scope.createUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(userSaveSpy.called).to.be.ok;
            expect(userSaveSpy.args[0][0].MaxUploadSpeedBits).equals(1);
            expect(userSaveSpy.args[0][0].MaxDownloadSpeedBits).equals(2);
            expect(userListSpy.called).to.be.ok;
            expect(modalSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;

            userSaveSpy.restore();
            $scope.sliderUl.value = 0;
            $scope.sliderDl.value = 0;

            done();
        });

        it('will call save user when userAuth is true', function (done) {

            $scope.userAuth = true; // testing not MAC auth

            var userSaveSpy = sinon.stub(UserService, 'save').callsFake(saveOk);

            $scope.createUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(userSaveSpy.called).to.be.ok;
            expect(userListSpy.called).to.be.ok;
            expect(modalSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;

            userSaveSpy.restore();

            done();
        });

        it('will call saveMac when userAuth is false', function (done) {

            $scope.userAuth = false; // testing MAC auth this time

            var userSaveSpy = sinon.stub(UserService, 'saveMac').callsFake(saveOk);
            var otherSpy = sinon.stub(Utils, 'filterMac').callsFake(function () {
                return "11:22:33:^";
            });

            $scope.createUser($scope.userForm, $scope.user);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect(userSaveSpy.called).to.be.ok;
            expect(userListSpy.called).to.be.ok;
            expect(modalSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;
            expect(otherSpy.called).to.be.true;

            userSaveSpy.restore();
            otherSpy.restore();
            done();
        });

    });

    describe('close()', function () {
        beforeEach(function() {
            // expectResponseWithEndpointPort();
            // $httpBackend.flush();
        });

        it('will close the modal and not call the delete endpoint', function (done) {

            $scope.close();

            expect(modalSpy.calledWith('close')).to.be.ok;

            done();
        });
    });
});
