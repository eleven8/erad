'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('UserImportCtrl', function() {

    var UserImportCtrl;
    var $modalInstance, $httpBackend, $controller, $scope, $rootScope, $q, $http;
    var modalSpy, errorSpy, userRemoveSpy, userListSpy;
    var UserService, SessionErrorService, AppModel;
    var mockModalInstance;
    var url;

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));
    beforeEach(inject(function ($injector) {
        UserService     = $injector.get('UserService');
        SessionErrorService = $injector.get('SessionErrorService');
        AppModel        = $injector.get('AppModel');

        $httpBackend    = $injector.get('$httpBackend');
        $controller     = $injector.get('$controller');
        $rootScope      = $injector.get('$rootScope');
        $q              = $injector.get('$q');
        $http           = $injector.get('$http');
        $scope          = $rootScope.$new();

        AppModel.accessToken = 'token';
        AppModel.selectedGroup = 'group';

        mockModalInstance = {
            close: function (result) {
            },
            dismiss: function (msg) {
            }
        };

        UserImportCtrl = $controller('UserImportCtrl', {
            '$scope': $scope,
            '$http': $http,
            'SessionErrorService': SessionErrorService,
            'AppModel': AppModel,
            'UserService': UserService,
            '$modalInstance': mockModalInstance
        });

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('controller setup', function () {

        it('will set the scope variables correctly', function (done) {

            expect($scope.AppModel).to.equal(AppModel);
            expect($scope.view.Title).to.equal('Import Users');
            expect($scope.myFile).to.be.null;

            done();
        });
    });

    describe('close()', function () {

        it('will close the modal and not call the delete endpoint', function (done) {

            modalSpy = sinon.stub(mockModalInstance, 'dismiss');
            userRemoveSpy = sinon.stub(UserService, 'remove');

            $scope.close();

            expect(modalSpy.args[0][0]).to.equal('close');
            expect(userRemoveSpy.called).to.be.false;

            modalSpy.restore();
            userRemoveSpy.restore();

            done();

        });
    });

    describe('uploadFile()', function () {

        it('will close the modal & call the list users endpoint if import is successful', function (done) {

            var httpSpy = sinon.stub(UserService, 'importUsers').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            userListSpy = sinon.stub(UserService, 'list');

            errorSpy = sinon.stub(SessionErrorService, 'handleError');

            modalSpy = sinon.stub(mockModalInstance, 'dismiss');

            $scope.uploadFile({});

            $scope.$apply();

            expect(httpSpy.called).to.be.ok;
            expect(modalSpy.calledWith('close')).to.be.ok;
            expect(userListSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;

            httpSpy.restore();
            modalSpy.restore();
            userListSpy.restore();
            errorSpy.restore();

            done();
        });

        it('will close the modal & call an error if import fails', function (done) {

            var httpSpy = sinon.stub(UserService, 'importUsers').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'error' });
                return deferred.promise;
            });

            userListSpy = sinon.stub(UserService, 'list');

            errorSpy = sinon.stub(SessionErrorService, 'handleError');

            modalSpy = sinon.stub(mockModalInstance, 'dismiss');

            $scope.uploadFile({});

            $scope.$apply();

            expect(httpSpy.called).to.be.ok;
            expect(modalSpy.calledWith('close')).to.be.ok;
            expect(userListSpy.called).to.be.false;
            expect(errorSpy.called).to.be.ok;

            httpSpy.restore();
            modalSpy.restore();
            userListSpy.restore();
            errorSpy.restore();

            done();
        });
    });

});
