'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('DeleteGroupModalCtrl', function() {

    var DeleteGroupModalCtrl;
    var $httpBackend, $controller, $scope, $rootScope, $q;
    var modalSpy, errorSpy, groupSaveSpy, groupListSpy;

    var mockModalInstance, groupToDelete;

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
      $provide.service('groupToDelete', function () {
        return { title: 'Delete Site', data: { ID: 'site', Name: 'site', }, indx: 1 };
      });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));

    beforeEach(inject(function ($injector) {

        $httpBackend    = $injector.get('$httpBackend');
        $controller     = $injector.get('$controller');
        $rootScope      = $injector.get('$rootScope');
        $q              = $injector.get('$q');
        groupToDelete   = $injector.get('groupToDelete');
        $scope          = $rootScope.$new();

        mockModalInstance = {
            close: function (result) {
            },
            dismiss: function (msg) {
            }
        };

        DeleteGroupModalCtrl = $controller('DeleteGroupModalCtrl', {
            '$scope': $scope,
            'groupToDelete': groupToDelete,
            '$modalInstance': mockModalInstance
        });

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('controller setup', function () {

        it('will name the modal correctly', function (done) {

            expect($scope.name).to.equal('site');
            expect($scope.title).to.equal('Delete Site');

            done();
        });
    });

    describe('close()', function () {

        it('will close the modal', function (done) {

            modalSpy = sinon.stub(mockModalInstance, 'close');

            $scope.close();

            expect(modalSpy.called).to.be.ok;
            expect(modalSpy.calledWith('close')).to.be.ok;

            modalSpy.restore();

            done();
        });

    });

    describe('removeGroup()', function () {

        it('will close the modal correctly', function (done) {

            modalSpy = sinon.stub(mockModalInstance, 'close');

            $scope.removeGroup();

            expect(modalSpy.called).to.be.ok;
            expect(modalSpy.calledWith('Ok')).to.be.ok;

            modalSpy.restore();

            done();

        });
    });
});
