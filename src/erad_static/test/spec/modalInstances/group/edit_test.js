'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('editGroupCtrl', function() {

    var editGroupCtrl;
    var GroupService, AppModel, SessionErrorService, AuthenticatorService, Utils;
    var $httpBackend, $controller, $scope, $rootScope, $q, $filter;

    var modalSpy, errorSpy, groupSaveSpy, groupListSpy;
    var authenticatorDeleteSpy, authenticatorListSpy, authenticatorSaveSpy;
    var endpointListSpy, endpointAddSpy, endpointDeleteSpy;

    var defaultAuthType;
    var defaultRegion;
    var mockModalInstance;
    var ngTableParams;
    var getAuthenticatorsAPICall;

    var items = [{
            ref: 0,
            title: 'ElevenOS',
            RemoteServerIp: '',
            RemoteServerPort: '',
            RemoteServerUrl: '',
            SharedSecret: ''
         },{
            ref: 1,
            title: 'Marriott',
            RemoteServerIp : '',
            RemoteServerPort: '',
            RemoteServerUrl: '',
            SharedSecret: ''
         },{
            ref: 2,
            title: 'None',
            RemoteServerIp : '',
            RemoteServerPort: '',
            RemoteServerUrl: '',
            SharedSecret: ''
         },{
            ref: 3,
            title: 'Other',
            RemoteServerIp: '',
            RemoteServerPort: '',
            RemoteServerUrl: '',
            SharedSecret: ''
         }];

    var fakeGroup = {
        'Session': {
          'ID': 'token'
        },
        'Group': {
          'Name': 'name',
          'ID': 'groupId',
          'RemoteServerUrl': 'RemoteServerUrl',
          'SharedSecret': 'SharedSecret',
          'TimeZone': 'America/Chicago',
          'OverrideVlan': true,
          'OverrideConnectionSpeed': true
        }
    };
    var originalData = [{ key: 'called-station-id', val: '11:22:22:11:22:22' }];

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
        var myWindow = {
            // mock the window.location object
            location: {
            'href': '',
            'search': '',
            'hash': ''
            },
            document: window.document
        };

        // provide myWindow as the $window
        $provide.value('$window', myWindow);

        $provide.service('data', function () {
            return [];
            //return [{ key: 'called-station-id', val: '11:22:11:11:22:11' }];
        });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));
    beforeEach(inject(function ($injector) {
        GroupService    = $injector.get('GroupService');
        AppModel        = $injector.get('AppModel');
        SessionErrorService = $injector.get('SessionErrorService');
        AuthenticatorService = $injector.get('AuthenticatorService');
        Utils           = $injector.get('Utils');
        ngTableParams   = $injector.get('ngTableParams');

        $filter         = $injector.get('$filter');
        $httpBackend    = $injector.get('$httpBackend');
        $controller     = $injector.get('$controller');
        $rootScope      = $injector.get('$rootScope');
        $q              = $injector.get('$q');
        $scope          = $rootScope.$new();


        mockModalInstance = {
            close: function (result) {
            },
            dismiss: function (msg) {
            }
        };

        AppModel.accountId = 'ID';
        AppModel.selectedGroup = 'Group_ID';
        AppModel.timezone = 'America/Chicago';
        AppModel.accessToken = 'token';
        AppModel.tzShortname = 'CST';
        //$scope.originalData = [];

        editGroupCtrl = $controller('editGroupCtrl', {
            'GroupService': GroupService,
            'AppModel': AppModel,
            'SessionErrorService': SessionErrorService,
            'AuthenticatorService': AuthenticatorService,
            //'Utils': Utils,
            'ngTableParams': ngTableParams,
            '$scope': $scope,
            '$filter': $filter,
            '$modalInstance': mockModalInstance
        });

        $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/endpoint/load')
            .respond(200, { Error: null, Endpoint: { IpAddress:'52.26.34.201', Port:'1812', Region:'us-west', Secret:'4905DVCA3B' }, Global: true });

        //authenticatorListSpy = sinon.stub(GroupService, 'listAuthenticators');
        $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/group/authenticator/list')
            .respond(200, { Error: null, Auth_List: [{ 'called-station-id': '11:22:22:11:22:22'}] });
        $httpBackend.flush();
    }));

    afterEach(function () {
        //modalSpy.restore();
        //errorSpy.restore();
        //groupSaveSpy.restore();
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('setup', function () {

        it('will provide data via the route resolve', function (done) {

            var pageloadData = [{ key: 'called-station-id', val: '11:22:22:11:22:22' }];

            expect($scope.data).to.deep.equal(pageloadData);

            done();
        });

        it('will provide data via the route resolve', function (done) {

            defaultAuthType = { title: 'called-station-id', placeholder: 'FF:FF:FF:FF:FF:FF' };

            defaultRegion = { title: "us-west", value: "us-west-2" };

            expect($scope.authType).to.deep.equal(defaultAuthType);

            expect($scope.region).to.deep.equal(defaultRegion);

            done();
        });

    });

    describe('closeModal()', function () {
        it('will close the modal correctly', function (done) {

            modalSpy = sinon.stub(mockModalInstance, 'dismiss');

            $scope.close();

            expect(modalSpy.called).to.be.ok;
            expect(modalSpy.args[0][0]).to.equal('close');

            done();
        });
    });

    // 'save' button
    describe('updateGroup()', function () {

        it('will send an error if group save fails', function (done) {

            modalSpy = sinon.stub(mockModalInstance, 'dismiss');

            groupListSpy = sinon.stub(GroupService, 'list').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ });
                return deferred.promise;
            });

            groupSaveSpy = sinon.stub(GroupService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'error' });
                return deferred.promise;
            });

            errorSpy = sinon.stub(SessionErrorService, 'handleError');

            $scope.group = fakeGroup;

            var fakeForm = {
                $valid: true
            };

            $scope.updateGroup(fakeForm);

            $scope.$apply();

            expect($scope.submitted).to.be.false;
            expect($scope.group.timezone).to.equal('America/Chicago');
            expect(modalSpy.args[0][0]).to.equal('close');

            groupListSpy.restore();
            groupSaveSpy.restore();
            errorSpy.restore();
            modalSpy.restore();

            done();
        });

        it('will send an error if group save completes', function (done) {

            groupSaveSpy = sinon.stub(GroupService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            groupListSpy = sinon.stub(GroupService, 'list').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({});
                return deferred.promise;
            });

            errorSpy = sinon.stub(SessionErrorService, 'handleError');
            modalSpy = sinon.stub(mockModalInstance, 'dismiss');

            $scope.group = fakeGroup;

            var fakeForm = {
                $valid: true
            };

            $scope.updateGroup(fakeForm);

            $scope.$apply();

            expect($scope.group.timezone).to.equal('America/Chicago');
            expect(modalSpy.args[0][0]).to.equal('close');
            expect($scope.submitted).to.be.false;

            expect(groupSaveSpy.called).to.be.ok;
            expect(groupListSpy.called).to.be.ok;
            expect(errorSpy.called).to.equal(false);

            groupSaveSpy.restore();
            groupListSpy.restore();
            errorSpy.restore();
            modalSpy.restore();

            done();
        });

        it('will not work when a form object isn\'t passed', function (done) {

            errorSpy = sinon.stub(SessionErrorService, 'handleError');
            modalSpy = sinon.stub(mockModalInstance, 'dismiss');
            groupSaveSpy = sinon.stub(GroupService, 'save');

            $scope.updateGroup();

            expect($scope.submitted).to.be.ok;
            expect(modalSpy.called).to.be.false;
            expect(groupSaveSpy.called).to.be.false;

            errorSpy.restore();
            modalSpy.restore();
            groupSaveSpy.restore();

            done();
        });
    });

    // close error box button handler
    describe('dismissAlert()', function () {
        it('will work', function (done) {

            $scope.xhrError = 'error';

            $scope.dismissAlert();

            expect($scope.xhrError).to.equal('');

            done();
        });
    });

    // remove authenticator 'save' button
    describe('delAuthenticators()', function () {
        it('will display an error if delete fails', function (done) {

            authenticatorDeleteSpy = sinon.stub(AuthenticatorService, 'deleteItem').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'fail' });
                return deferred.promise;
            });

            var row = {};

            $scope.delAuthenticators(row);

            $scope.$apply();

            expect($scope.xhrError).to.deep.equal({ type: 'danger', msg: 'Delete failed' });

            done();

            authenticatorDeleteSpy.restore();

        });

        it('will delete ok', function (done) {

            authenticatorDeleteSpy = sinon.stub(AuthenticatorService, 'deleteItem').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            var xhrErrorSpy = sinon.stub($scope, 'dismissAlert');

            var tableSpy = sinon.stub($scope.tableParams, 'reload').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null, Data: [{}] });
                return deferred.promise;
            });

            var validRow = { key: 'called-station-id', val: '11:22:22:11:22:22' };
            var invalidRow = { key: 'nas-identifier', val: 'some string' };

            expect($scope.data).to.be.ok;
            expect($scope.data).to.be.an('array');

            // initial data
            expect($scope.data.length).to.equal(1);

            // try to delete an authenticator that isn't in the data array
            $scope.delAuthenticators(invalidRow);

            $scope.$apply();

            // array will be untouched
            expect($scope.data.length).to.equal(1);

            // try again with valid authenticator
            $scope.delAuthenticators(validRow);

            $scope.$apply();

            // array will be reduced by one in length
            expect($scope.data.length).to.equal(0);

            expect($scope.xhrError).to.equal('');
            expect(xhrErrorSpy.called).to.be.ok;
            expect(tableSpy.called).to.be.ok;

            done();

            authenticatorDeleteSpy.restore();
            tableSpy.restore();
        });
    });

    // add new authenticator 'save' button
    describe('updateAuthenticators()', function () {
        it('when delete fails', function (done) {

            authenticatorSaveSpy = sinon.stub(AuthenticatorService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'Invalid Mac' });
                return deferred.promise;
            });

            authenticatorDeleteSpy = sinon.stub(AuthenticatorService, 'deleteItem').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'error' });
                return deferred.promise;
            });

            var xhrError = {
               msg: 'Please enter a valid MAC address.',
               type: 'danger'
            };

            var row = {
                val: 'val',
                key: 'nas-identifier'
            };

            var rowForm = {};
            var index = 1;

            var rowSpy = sinon.stub($scope, 'resetRow').returns(row);

            // no error before failed save
            expect($scope.xhrError).to.deep.equal('');

            $scope.updateAuthenticator(row, rowForm, index);

            $scope.$apply();

            expect(authenticatorSaveSpy.called).to.be.ok;
            expect($scope.xhrError).to.deep.equal(xhrError);

            authenticatorDeleteSpy.restore();
            authenticatorSaveSpy.restore();
            rowSpy.restore();

            done();
        });

        it('when delete is successful', function (done) {
            authenticatorSaveSpy = sinon.stub(AuthenticatorService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            authenticatorDeleteSpy = sinon.stub(AuthenticatorService, 'deleteItem').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            var tableSpy = sinon.stub($scope.tableParams, 'reload').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null, Data: [{}] });
                return deferred.promise;
            });

            var row = {
                val: 'val',
                key: 'nas-identifier'
            };

            var rowSpy = sinon.stub($scope, 'resetRow').returns(row);

            var rowForm = {};
            var index = 1;

            // no error before failed save
            expect($scope.xhrError).to.deep.equal('');

            $scope.updateAuthenticator(row, rowForm, index);

            $scope.$apply();

            expect(authenticatorSaveSpy.called).to.be.ok;
            expect($scope.xhrError).to.equal('');
            expect(tableSpy.called).to.be.ok;

            authenticatorDeleteSpy.restore();
            authenticatorSaveSpy.restore();
            tableSpy.restore();
            //rowSpy.restore();

            done();
        });
    });

    // callback from dropdown directive
    describe('authCallback()', function () {
        it('will work', function (done) {

            // starts empty
            expect($scope.newVal).to.deep.equal({});
            expect($scope.authType).to.deep.equal(defaultAuthType);

            $scope.authCallback('nas-identifier');

            expect($scope.authType.title).to.deep.equal('nas-identifier');
            expect($scope.newVal.key).to.deep.equal('nas-identifier');

            $scope.authCallback('called-station-id');

            expect($scope.authType.title).to.deep.equal('called-station-id');
            expect($scope.newVal.key).to.deep.equal('called-station-id');

            $scope.authCallback('nas-identifier');

            expect($scope.authType.title).to.deep.equal('nas-identifier');
            expect($scope.newVal.key).to.deep.equal('nas-identifier');

            done();
        });
    });

    // callback from region dropdown
    describe('regionCallback()', function () {
        it('will work', function (done) {

            $scope.regionCallback('us-west');

            expect($scope.region.title).to.deep.equal('us-west');

            done();
        });
    });

    // another callback from dropdown directive
    describe('selectItemCallback()', function () {
        it('will set the remote authentication item from the dropdown', function (done) {

            var itemsSpy = sinon.stub(GroupService, 'remoteAuthenticationOptions')
                .returns(items);

            // default to not showing extra fields
            expect($scope.view.showInputs).to.be.false;
            expect($scope.view.showOverrides).to.be.false;
            expect($scope.item).to.not.be.undefined;

            // select other
            $scope.selectItemCallback('Other');

            // item is set correctly
            expect($scope.item.title).to.deep.equal('Other');
            // extra form inputs are shown
            expect($scope.view.showInputs).to.be.ok;
            expect($scope.view.showOverrides).to.be.ok;


            // set overrides in `checked/true` state
            // selectItemCallback('None') should set them in false stated
            $scope.view.OverrideVlan = true;
            $scope.view.OverrideConnectionSpeed = true;

            // select 'none' on the dropdown
            $scope.selectItemCallback('None');

            expect($scope.item.title).to.deep.equal('None');
            // extra inputs are now hidden again
            expect($scope.view.showInputs).to.be.false;
            expect($scope.view.showOverrides).to.be.false;
            expect($scope.view.OverrideVlan).to.be.false;
            expect($scope.view.OverrideConnectionSpeed).to.be.false;

            itemsSpy.restore();

            $scope.selectItemCallback('Marriott');

            expect($scope.item.title).to.deep.equal('Marriott');
            expect($scope.item.RemoteServer).to.deep.equal('Marriott');
            expect($scope.item.RemoteServerUrl).to.deep.equal('');
            // extra inputs are now hidden again
            expect($scope.view.showInputs).to.be.false;
            expect($scope.view.showOverrides).to.be.ok;

            itemsSpy.restore();

            $scope.selectItemCallback('ElevenOS');

            expect($scope.item.title).to.deep.equal('ElevenOS');
            expect($scope.item.RemoteServer).to.deep.equal('ElevenOS');
            expect($scope.item.RemoteServerUrl).to.deep.equal('');
            // extra inputs are now hidden again
            expect($scope.view.showInputs).to.be.false;
            expect($scope.view.showOverrides).to.be.ok;

            itemsSpy.restore();

            done();
        });
    });

    // resets row of authenticators table when editing
    describe('clearRow()', function () {
        it('will clear the input field for adding a new authenticator', function (done) {

            $scope.newVal.val = 'somevalue that\'s not uet saved';
            var fakeForm = {
                $setPristine: function () {}
            };
            var formSpy = sinon.stub(fakeForm, '$setPristine');

            $scope.clearRow(fakeForm);

            expect($scope.newVal.val).to.equal('');
            expect(formSpy.called).to.be.ok;

            formSpy.restore();

            done();
        });
    });

    // save button for adding new authenticator
    describe('addAuthenticator()', function () {
        it('will call a error if save fails', function (done) {

            authenticatorSaveSpy = sinon.stub(AuthenticatorService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'Invalid Mac' });
                return deferred.promise;
            });

            var fakeForm = {
                $setPristine: function () {},
                $invalid: false
            };

            var formValues = {
                key: 'nas-identifier',
                val: 'some string'
            };
            var err = {
                msg: 'Please enter a valid MAC address.',
                type: 'danger'
            };

            // check no error before function call
            expect($scope.xhrError).to.equal('');

            // function clear error before save API call
            $scope.xhrError = {
                msg: 'fake error',
                type: 'none'
            };

            $scope.addAuthenticator(fakeForm, formValues);

            $scope.$apply();

            expect(authenticatorSaveSpy.called).to.be.ok;
            expect($scope.xhrError).to.deep.equal(err);

            authenticatorSaveSpy.restore();

            done();
        });

        it('will display an error if form is invalid when submited', function (done) {

            authenticatorSaveSpy = sinon.stub(AuthenticatorService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'Invalid Mac' });
                return deferred.promise;
            });

            var fakeForm = {
                $setPristine: function () {},
                $invalid: true
            };

            var formValues = {
                key: 'nas-identifier',
                val: 'some string'
            };

            var err = {
                msg: 'Called-station-id must be valid MAC Address',
                type: 'danger'
            };

            // check no error before function call
            expect($scope.xhrError).to.equal('');

            // call function with invalid form
            $scope.addAuthenticator(fakeForm, formValues);

            $scope.$apply();

            expect(authenticatorSaveSpy.called).to.be.false;
            expect($scope.xhrError).to.deep.equal(err);

            authenticatorSaveSpy.restore();

            done();
        });

        it('will call a error if save fails', function (done) {

            authenticatorSaveSpy = sinon.stub(AuthenticatorService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({
                    Error: null,
                    Authenticator: {
                        ID: 'woop',
                        RadiusAttribute: 'woop'
                    }
                });
                return deferred.promise;
            });

            var tableSpy = sinon.stub($scope.tableParams, 'reload').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null, Data: [{}] });
                return deferred.promise;
            });

            var fakeForm = {
                $setPristine: function () {},
                $invalid: false
            };

            var formValues = {
                key: 'nas-identifier',
                val: 'some string'
            };
            var err = {
                msg: 'Failed to add authenticator.',
                type: 'danger'
            };
            var formSpy = sinon.stub(fakeForm, '$setPristine');

            // check no error before function call
            expect($scope.xhrError).to.equal('');

            // function clear error before save API call
            $scope.xhrError = {
                msg: 'fake error',
                type: 'none'
            };

            expect($scope.data.length).to.equal(1);

            $scope.addAuthenticator(fakeForm, formValues);

            $scope.$apply();

            expect(authenticatorSaveSpy.called).to.be.ok;
            expect($scope.xhrError).to.equal('');
            expect($scope.data).to.be.an('array');
            expect($scope.data.length).to.equal(2);
            expect($scope.originalData).to.deep.equal($scope.data);
            expect($scope.newVal).to.deep.equal({});
            expect(formSpy.called).to.be.ok;
            expect(tableSpy.called).to.be.ok;

            authenticatorSaveSpy.restore();
            tableSpy.restore();

            done();
        });

    });

    // provision button for adding new endpoint
    describe('addEndpoint()', function () {
        it('will call a error if save fails', function (done) {

            endpointAddSpy = sinon.stub(GroupService, 'provisionEndpoint').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'No ports left.' });
                return deferred.promise;
            });

            var err = {
                msg: 'No ports left.',
                type: 'danger'
            };

            // check no error before function call
            expect($scope.xhrEndpointError).to.equal('');

            // function clear error before save API call
            $scope.xhrEndpointError = {
                msg: 'fake error',
                type: 'none'
            };

            // passing default regions as selected for $scope.region
            $scope.addEndpoint();

            $scope.$apply();

            expect(endpointAddSpy.called).to.be.ok;
            expect($scope.xhrEndpointError).to.deep.equal(err);

            endpointAddSpy.restore();

            done();
        });


        it('will send newly saved endpoint', function (done) {

            endpointAddSpy = sinon.stub(GroupService, 'provisionEndpoint').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({
                    Error: null,
                    Endpoint: {
                        IpAddress:'52.26.34.124',
                        Port:'20006',
                        Region:'us-west',
                        Secret:'4905DVCA3B'
                    }
                });
                return deferred.promise;
            });

            endpointListSpy = sinon.stub(GroupService, 'listEndpoints').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({
                    Error: null,
                    Endpoint: {
                        IpAddress:'52.26.34.124',
                        Port:'20006',
                        Region:'us-west',
                        Secret:'4905DVCA3B'
                    },
                    Global: false
                });
                return deferred.promise;
            });

            // endpoint results
            var endpoints = [{
                        IpAddress:'52.26.34.124',
                        Port:'20006',
                        Region:'us-west',
                        Secret:'4905DVCA3B'
                    }];

            var err = {
                msg: 'Failed to provision endpoint.',
                type: 'danger'
            };

            // check no error before function call
            expect($scope.xhrEndpointError).to.equal('');

            // check for endpoint data
            expect($scope.endpointsData.length).to.equal(1);

            // passing default regions as selected for $scope.region
            $scope.addEndpoint();

            $scope.$apply();

            expect(endpointAddSpy.called).to.be.ok;
            expect(endpointListSpy.called).to.be.ok;
            expect($scope.xhrEndpointError).to.equal('');
            expect($scope.endpointsData).to.be.an('array');
            expect($scope.endpointsData).to.deep.equal(endpoints);
            expect($scope.endpointsData.length).to.equal(1);

            endpointAddSpy.restore();
            endpointListSpy.restore();

            done();
        });

    });

    // remove provision endpoint button
    describe('delEndpoint()', function () {
        it('will display an error if delete fails', function (done) {

            endpointDeleteSpy = sinon.stub(GroupService, 'deleteEndpoint').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'fail' });
                return deferred.promise;
            });

            var item = {};

            $scope.delEndpoint(item);

            $scope.$apply();

            expect($scope.xhrEndpointError).to.deep.equal({ type: 'danger', msg: 'Delete failed' });

            done();

            endpointDeleteSpy.restore();

        });

        it('will delete ok', function (done) {

            endpointDeleteSpy = sinon.stub(GroupService, 'deleteEndpoint').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            endpointListSpy = sinon.stub(GroupService, 'listEndpoints').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({
                    Error: null,
                    Endpoint: {
                        IpAddress:'52.26.34.124',
                        Port:'1812',
                        Region:'us-west',
                        Secret:'4905DVCA3B'
                    },
                    Global: false
                });
                return deferred.promise;
            });

            var xhrErrorSpy = sinon.stub($scope, 'dismissAlert');

            var item = {
                IpAddress:'52.26.34.124',
                Port:'20006',
                Region:'us-west',
                Secret:'4905DVCA3B'
            };

            expect($scope.endpointsData).to.be.ok;
            expect($scope.endpointsData).to.be.an('array');

            // initial data
            expect($scope.endpointsData.length).to.equal(1);

            // try to delete an authenticator that isn't in the data array
            $scope.delEndpoint(item);

            $scope.$apply();

            expect($scope.xhrEndpointError).to.equal('');

            done();

            endpointDeleteSpy.restore();
            endpointListSpy.restore();
        });
    });

    // restore row to original state, before save
    xdescribe('cancelEditAuthenticators()', function () {
        it('will restore the original value to the form', function (done) {

            var row = {
                val: 'tets',
                key: 'nas-identifier'
            };

            var rowForm = {
                $setPristine: function() {}
            };
            var spy = sinon.stub($scope, 'resetRow');
            //var formSpy = sinon.stub(rowForm, '$setPristine');

            var result = $scope.resetRow([{ key: 'called-station-id', val: '11:22:22:11:22:22' }], row, 1);

            expect(result).to.equal(1);

            //expect(formSpy.called).to.be.ok;
            spy.restore();

            done();
        });
    });

});
