'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('addGroupCtrl', function() {

    var addGroupCtrl;
    var GroupService;
    var $httpBackend, $controller, $scope, $rootScope, $q;
    var modalSpy, errorSpy, groupSaveSpy, groupListSpy;

    var mockModalInstance;

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));
    beforeEach(inject(function ($injector) {
        GroupService    = $injector.get('GroupService');

        $httpBackend    = $injector.get('$httpBackend');
        $controller     = $injector.get('$controller');
        $rootScope      = $injector.get('$rootScope');
        $q              = $injector.get('$q');
        $scope          = $rootScope.$new();

        mockModalInstance = {
            close: function (result) {
            },
            dismiss: function (msg) {
            }
        };

        addGroupCtrl = $controller('addGroupCtrl', {
            'GroupService': GroupService,
            '$scope': $scope,
            '$modalInstance': mockModalInstance
        });

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('controller setup', function () {
        it('will name the view correctly', function (done) {
            expect($scope.view.Title).to.equal('Add New Site')
            done();
        });
    });

    describe('close()', function () {
        it('will close the modal', function (done) {

            groupSaveSpy = sinon.stub(GroupService, 'save');

            modalSpy = sinon.stub(mockModalInstance, 'dismiss');

            $scope.close();

            expect(groupSaveSpy.called).to.be.false;
            expect(modalSpy.called).to.be.ok;
            expect(modalSpy.calledWith('close')).to.be.ok;
            expect($scope.submitted).to.be.undefined;

            groupSaveSpy.restore();
            modalSpy.restore();

            done();

        });
    });

    describe('addGroup()', function () {

        it('will not call GroupService.save() if the form is invalid', function (done) {

            groupSaveSpy = sinon.stub(GroupService, 'save');
            modalSpy = sinon.stub(mockModalInstance, 'close');

            var form = {};
            form.$valid = false;

            $scope.addGroup(form);

            expect(groupSaveSpy.called).to.be.false;
            expect(modalSpy.called).to.be.false;
            expect($scope.submitted).to.be.ok;

            groupSaveSpy.restore();
            modalSpy.restore();

            done();

        });

        it('will call GroupService.save() if the form is valid', function (done) {

            var groupSavePromise = sinon.stub(GroupService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            modalSpy = sinon.stub(mockModalInstance, 'close');

            var form = {};
            form.$valid = true;

            $scope.addGroup(form);

            $scope.$apply();

            expect(groupSavePromise.called).to.be.ok;
            expect(modalSpy.called).to.be.ok;
            expect(modalSpy.calledWith({ Error: null })).to.be.ok;
            expect($scope.submitted).to.be.false;

            groupSavePromise.restore();
            modalSpy.restore();

            done();

        });
    });
});
