'use strict';

/* New tests need to be added to  spec runner at ../index.html */

var expect = chai.expect;

describe('AdminService', function() {
   var AdminService, $window, $httpBackend, $rootScope, AppModel, $q;

   // // before each test, spin up the module and inject the service
   beforeEach(angular.mock.module('services'));

   beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $httpBackend = $injector.get('$httpBackend');
      $window = $injector.get('$window');
      $q = $injector.get('$q');
      AppModel = $injector.get('AppModel');
      AppModel.timezone = 'America/Los_Angeles';
      AdminService = $injector.get('AdminService');
   }));

   afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
   });

   describe('.loadEndpoints()', function() {

      it('should load the admin endpoints', function () {

         expect(1).to.equal(1);

      });

   });
});
