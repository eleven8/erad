'use strict';

/* New tests need to be added to  spec runner at ../index.html */

var expect = chai.expect;

describe('DateService', function() {
   var DateService, $window, $rootScope, AppModel;

   var LA = 'America/Los_Angeles';
   var Chicago = 'America/Chicago';
   var CEurope = 'Europe/Budapest';
   // uses UTC +5.5 timezone
   var Kolkata = 'Asia/Kolkata';

   // // before each test, spin up the module and inject the service
   beforeEach(angular.mock.module('services'));

   beforeEach(inject(function ($injector) {
      $rootScope = $injector.get('$rootScope');
      $window = $injector.get('$window');
      AppModel = $injector.get('AppModel');
      AppModel.timezone = 'America/Los_Angeles';
      DateService = $injector.get('DateService');
   }));

   describe('.calcDate()', function() {

      it('should accept a moment date object', function () {
         var momentDate = DateService.calcDate($window.moment());
         expect(momentDate.length).equals(2);
      });

      it('should accept a regular date object', function () {
         var regularDate = DateService.calcDate(new Date());
         expect(regularDate.length).equals(2);
      });

      it('should return an empty array if nothing is passed', function () {
         var nothingPassed = DateService.calcDate();
         expect(Array.isArray(nothingPassed)).equals(true);
         expect(nothingPassed.length).equals(0);
      });

      it('should return an array of two numbers when a moment object is passed', function () {
         var momentInput = DateService.calcDate($window.moment());
         expect(typeof(momentInput[0])).equals('number');
         expect(typeof(momentInput[1])).equals('number');
      });
   });

   describe('.addGroupUTCOffset()', function() {

      it('should return a UTC string offset by the group UTC offset', function () {
         var dateInput = '2015-12-30T22:00:50Z';

         var expectedPSTDateOutput = '2015-12-30T14:00:50Z';
         var expectedCETDateOutput = '2015-12-30T23:00:50Z';
         var expectedCSTDateOutput = '2015-12-30T16:00:50Z';
         var expectedISTDateOutput = '2015-12-31T03:30:50Z';

         var PST_Result = DateService.addGroupUTCOffset(dateInput, LA);
         var CET_Result = DateService.addGroupUTCOffset(dateInput, CEurope);
         var CST_Result = DateService.addGroupUTCOffset(dateInput, Chicago);
         var IST_Result = DateService.addGroupUTCOffset(dateInput, Kolkata);

         expect(PST_Result).equals(expectedPSTDateOutput);
         expect(CET_Result).equals(expectedCETDateOutput);
         expect(CST_Result).equals(expectedCSTDateOutput);
         expect(IST_Result).equals(expectedISTDateOutput);

      });

   });

   describe('.removeGroupUTCOffset()', function() {

      // currently dependent on browser timezone
      xit('should return a UTC string offset by the group UTC offset ', function () {

         var pstBrowser  = 'Mon Jan 04 2016 04:31:42 GMT-0800 (PST)';
         var cstBrowser  = 'Mon Jan 04 2016 06:31:42 GMT-0600 (CST)';
         var cetBrowser  = 'Mon Jan 04 2016 13:31:42 GMT+0100 (CET)';
         var istBrowser  = 'Mon Jan 04 2016 18:01:42 GMT+0530 (IST)';

         // PST group
         var PST_PST_Result = DateService.removeGroupUTCOffset(pstBrowser, LA);
         var CST_PST_Result = DateService.removeGroupUTCOffset(cstBrowser, LA);
         var CET_PST_Result = DateService.removeGroupUTCOffset(cetBrowser, LA);
         var ITC_PST_Result = DateService.removeGroupUTCOffset(istBrowser, LA);

         // PST group
         var expectedPSTOutput = '2016-01-04T12:31:42+00:00';

         // PST group
         expect(PST_PST_Result).equals(expectedPSTOutput);
         expect(CST_PST_Result).equals(expectedPSTOutput);
         expect(CET_PST_Result).equals(expectedPSTOutput);
         expect(ITC_PST_Result).equals(expectedPSTOutput);

         // CST group
         var PST_CST_Result = DateService.removeGroupUTCOffset(pstBrowser, Chicago);
         var CST_CST_Result = DateService.removeGroupUTCOffset(cstBrowser, Chicago);
         var CET_CST_Result = DateService.removeGroupUTCOffset(cetBrowser, Chicago);
         var ITC_CST_Result = DateService.removeGroupUTCOffset(istBrowser, Chicago);

         // CST group
         var expectedCSTOutput = '2016-01-04T10:31:42+00:00';

         // CST group
         expect(PST_CST_Result).equals(expectedCSTOutput);
         expect(CST_CST_Result).equals(expectedCSTOutput);
         expect(CET_CST_Result).equals(expectedCSTOutput);
         expect(ITC_CST_Result).equals(expectedCSTOutput);

         // CET group
         var PST_CET_Result = DateService.removeGroupUTCOffset(pstBrowser, CEurope);
         var CST_CET_Result = DateService.removeGroupUTCOffset(cstBrowser, CEurope);
         var CET_CET_Result = DateService.removeGroupUTCOffset(cetBrowser, CEurope);
         var ITC_CET_Result = DateService.removeGroupUTCOffset(istBrowser, CEurope);

         // CET group
         var expectedCETOutput = '2016-01-04T03:31:42+00:00';

         // CET group
         expect(PST_CET_Result).equals(expectedCETOutput);
         expect(CST_CET_Result).equals(expectedCETOutput);
         expect(CET_CET_Result).equals(expectedCETOutput);
         expect(ITC_CET_Result).equals(expectedCETOutput);

         // IST group
         var PST_IST_Result = DateService.removeGroupUTCOffset(pstBrowser, Kolkata);
         var CST_IST_Result = DateService.removeGroupUTCOffset(cstBrowser, Kolkata);
         var CET_IST_Result = DateService.removeGroupUTCOffset(cetBrowser, Kolkata);
         var ITC_IST_Result = DateService.removeGroupUTCOffset(istBrowser, Kolkata);

         // IST group
         var expectedPSTISTOutput = '2016-01-03T23:01:42+00:00';

         // IST group
         expect(PST_IST_Result).equals(expectedPSTISTOutput);
         expect(CST_IST_Result).equals(expectedPSTISTOutput);
         expect(CET_IST_Result).equals(expectedPSTISTOutput);
         expect(ITC_IST_Result).equals(expectedPSTISTOutput);
      });

   });

});
