'use strict';

/* New tests need to be added to  spec runner at ../index.html */

var expect = chai.expect;

describe('GroupService', function() {

   var GroupService, $httpBackend, $rootScope, AppModel, $window, Utils;

   beforeEach(angular.mock.module('services', function($provide) {

      var myWindow = {
         // mock the window.location object
         location: {
            'href': '',
            'search': '',
            'hash': ''
         },
         moment: window.moment,
         // keep the window.document object (for cookies)
         document: window.document
      };

      // provide myWindow as the $window
      $provide.value('$window', myWindow);
   }));

   beforeEach(inject(function($injector) {
      GroupService = $injector.get('GroupService');
      AppModel = $injector.get('AppModel');
      Utils = $injector.get('Utils');
      $window = $injector.get('$window');
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');

      AppModel.accessToken = 'cheeseToken';
      AppModel.selectedGroup = 'groupId';
   }));

   afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
   });

   describe('.list()', function() {
      it('should set AppModel.groupList with its return data', function() {
         $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/group/list')
            .respond({
               'Group_List': [{
                  'TimeZone': 'America/Los  Angeles',
                  'RemoteServerUrl': 'RSU',
                  'SharedSecret': 'SS',
                  'ID': 'SG',
                  'Name': 'SGN',
                  'OverrideVlan': true,
                  'OverrideConnectionSpeed': true,
                  'MaxDownloadSpeedBits': 3850000,
                  'MaxUploadSpeedBits': 3900000
               }]
            });

         var dateSpy = sinon.stub(Utils, 'getTzCode').returns('PDT');

         GroupService.list();
         $httpBackend.flush();

         expect(AppModel.groupList).deep.equals([{
            'TimeZone': 'America/Los_Angeles',
            'RemoteServerUrl': 'RSU',
            'SharedSecret': 'SS',
            'ID': 'SG',
            'Name': 'SGN',
            'tzShortname': 'PDT',
            'OverrideVlan': true,
            'OverrideConnectionSpeed': true,
            'MaxDownloadSpeedBits': 3850000,
            'MaxUploadSpeedBits': 3900000
         }]);

         dateSpy.restore();

      });

   });

   describe('.load()', function() {
      it('should set the appropriate values', function() {
         $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/group/load')
            .respond({
               'Group': {
                  'TimeZone': 'America/Los Angeles',
                  'RemoteServerUrl': 'RSU',
                  'SharedSecret': 'SS',
                  'ID': 'SG',
                  'Name': 'SGN',
                  'OverrideVlan': true,
                  'OverrideConnectionSpeed': true,
                  'MaxDownloadSpeedBits': 3850000,
                  'MaxUploadSpeedBits': 3900000
               }
            });
         var tzSpy = sinon.stub(Utils, 'cleanTimeZone').returns('America/Los_Angeles');
         var dateSpy = sinon.stub(Utils, 'getTzCode').returns('PDT');

         GroupService.load();
         $httpBackend.flush();

         expect(AppModel.remoteServerUrl).equals('RSU');
         expect(AppModel.sharedSecret).equals('SS');
         expect(AppModel.selectedGroup).equals('SG');
         expect(AppModel.selectedGroupName).equals('SGN');
         expect(AppModel.tzShortname).equals('PDT');
         expect(AppModel.timezone).equal('America/Los_Angeles');
         expect(AppModel.OverrideVlan).equal(true);
         expect(AppModel.OverrideConnectionSpeed).equal(true);
         expect(AppModel.maxDownload).equal(3850000);
         expect(AppModel.maxUpload).equal(3900000);


         tzSpy.restore();
         dateSpy.restore();
      });
   });

   describe('.save()', function() {
      it('should set the appropriate values after saving', function() {

         var response = {
            'Group': {
               'RemoteServerUrl': 'RSU',
               'SharedSecret': 'SS',
               'ID': 'SG',
               'Name': 'SGN',
               'TimeZone': 'America/Los Angeles',
               'OverrideVlan': true,
               'OverrideConnectionSpeed': true
            }
         };

         var setGroupSpy = sinon.spy(AppModel, 'selectGroup');

         $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/group/save')
            .respond(response);

         GroupService.save({ 'RemoteServerUrl': 'RSU', 'SharedSecret': 'SS', 'ID': 'SG', 'Name': 'SGN', 'TimeZone': 'America/Los Angeles' });

         $httpBackend.flush();

         expect(setGroupSpy.called).to.equal(true);
         expect(setGroupSpy.calledWith(response.Group));

         setGroupSpy.restore();
      });
   });

});
