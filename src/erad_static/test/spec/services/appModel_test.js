'use strict';

/* New tests need to be added to  spec runner at ../index.html */

var expect = chai.expect;

// Example testing a service
describe('AppModel', function () {
   var SessionService, $httpBackend, $rootScope, AppModel, $window, httpObj, Utils, $q;

   // before each test, spin up the module and inject the service
   beforeEach(angular.mock.module('services', function ($provide) {

      var myWindow = {
         // mock the window.location object
         location: {
            'href': '',
            'search': '',
            'hash': ''
         },
         moment: moment,
         // keep the window.document object (for cookies)
         document: window.document,
         close : window.close
      };

      // provide myWindow as the $window
      $provide.value('$window', myWindow);
   }));

   beforeEach(inject(function ($injector) {
      SessionService = $injector.get('SessionService');
      AppModel = $injector.get('AppModel');
      Utils = $injector.get('Utils');
      $window = $injector.get('$window');
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');
      $q = $injector.get('$q');

      // when posting to fake 'error' url, respond with an error code
      // jshint ignore: start
      $httpBackend.when('POST', 'error400').respond(function(method, url, data, headers) {
         return [400, { Error: '400 error' }, 'headers', 'TestStatusPhrase'];
      });

      $httpBackend.when('POST', 'error500').respond(function(method, url, data, headers) {
         return [500, '500 error', {}, 'TestPhrase'];
      });

      $httpBackend.when('POST', 'success').respond(function(method, url, data, headers) {
         return [200, '200 success', {}, 'TestPhrase'];
      });
      // jshint ignore: end

   }));

   // set up the base httpObj for each test
   beforeEach(function() {
      httpObj = {
         method: 'POST',
         path: '',
         data: { 'Session': { 'ID': 'testID' } },
         sh: function (data) { return data; },
         eh: function (data) { return data; },
         offlineData: {}
      };

   });

   afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
   });

   describe('.callService()', function () {

      it('should make the remote request with the Content-Type specified', function () {

         var myHttpObj = {
            method: 'POST',
            path: 'headerTestEndpoint',
            headers: { 'Content-Type': 'borked' },
            data: { 'Session': { 'ID': 'testID' } },
            sh: function (data) { return data; },
            eh: function (data) { return data; },
            offlineData: {}
         };

         $httpBackend.expectPOST('headerTestEndpoint', undefined, function (headers) {
            return headers['Content-Type'] !== 'application/json';
         }).respond(200, '');

         AppModel.callService(myHttpObj);

         $httpBackend.flush();

      });

      describe('.eh()', function () {

         it('should call the eh function with the correct args', function () {

            var errorSpy = sinon.stub();
            var successSpy = sinon.stub();

            httpObj.path = 'error400';
            httpObj.eh = errorSpy;
            httpObj.sh = successSpy;

            AppModel.callService(httpObj);
            $httpBackend.flush();

            expect(errorSpy.called).to.equal(true);
            expect(errorSpy.args[0][0]).to.deep.equal({ Error: '400 error' });
            expect(successSpy.called).to.be.false;

         });

         it('should process data using the eh function correctly', function () {

            var errorSpy = sinon.stub();
            errorSpy.withArgs({ Error: '400 error' }).returns({ NewError: 'FAIL' });
            var successSpy = sinon.stub();

            httpObj.path = 'error400';
            httpObj.eh = errorSpy;
            httpObj.sh = successSpy;

            var promise = AppModel.callService(httpObj);
            $httpBackend.flush();

            expect(errorSpy.called).to.equal(true);
            expect(errorSpy.args[0][0]).to.deep.equal({ Error: '400 error' });

            expect(successSpy.called).to.be.false;

            expect(promise.$$state.value).deep.equals({ NewError: 'FAIL' });

         });

         it('should return an error object when we get a 500 status code', function() {

            httpObj.path = 'error500';
            httpObj.eh = null;

            var promise = AppModel.callService(httpObj);

            $httpBackend.flush();

            expect(promise.$$state.value).deep.equals({ 'Error': 'Server Error' });

         });

         it('should return the data processed with the "eh" function when API returns non-200.', function () {

            httpObj.path = 'error500';
            httpObj.eh = function(data) {
               var obj = { 'ehObj': data };
               return obj;
            };

            var promise = AppModel.callService(httpObj);

            $httpBackend.flush();

            expect(promise.$$state.value).deep.equals({ 'ehObj': '500 error' });
         });
      });

      it('should return the data processed with the "sh" function when successful.', function () {

         var failSpy = sinon.stub();

         httpObj.path = 'success';
         httpObj.sh = function(data) {
            var obj = { 'shObj': data };
            return obj;
         };
         httpObj.eh = failSpy;

         var promise = AppModel.callService(httpObj);

         $httpBackend.flush();

         expect(promise.$$state.value).deep.equals({ 'shObj': '200 success' });
         expect(failSpy.called).to.be.false;

      });

      it('should return offline data when in offline mode.', function () {

         $window.location.search = '?offline=1';
         AppModel.offline = 1;

         // setting the path to 'offline' and not defining it with $httpBackend would cause this
         // test to fail if it doesn't catch in the offline check.
         httpObj.path = 'offline';
         httpObj.sh = function (data) {
            var obj = { 'offlineObj': data };
            return obj;
         };
         httpObj.offlineData = { 'offline': 'data' };

         var promise = AppModel.callService(httpObj);

         expect(promise.$$state.value).deep.equals({ 'offlineObj': { 'offline': 'data' } });
      });

      it('should make the remote request with the default Content-Type', function () {

         var myHttpObj = {
            method: 'POST',
            path: 'headerTestEndpoint',
            data: { 'Session': { 'ID': 'testID' } },
            sh: function (data) { return data; },
            eh: function (data) { return data; },
            offlineData: {}
         };

         $httpBackend.expectPOST('headerTestEndpoint', undefined, function (headers) {
            return headers['Content-Type'] === 'application/json';
         }).respond(200, '');

         AppModel.callService(myHttpObj);

         $httpBackend.flush();

      });

      it('should work correctly if passed Content-Type is application/json', function () {

         var spy = sinon.stub(AppModel, 'callService');

         var myHttpObj = {
            method: 'POST',
            path: 'headerTestEndpoint',
            headers: 'application/json',
            data: { 'Session': { 'ID': 'testID' } },
            sh: function (data) { return data; },
            eh: function (data) { return data; },
            offlineData: {}
         };

         AppModel.callService(myHttpObj);

         expect(spy.called).to.equal(true);
         expect(spy.args[0][0].headers).to.equal('application/json');

         spy.restore();
      });

   });

   describe('.logout()', function() {
      it('should go to the logout url if defined', function() {

         AppModel.logoutUrl = 'http://google.com';

         AppModel.logout();

         expect($window.location.href).equals('http://google.com');
         expect(AppModel.accessToken).equals(null);
         expect(AppModel.isLoggedIn).equals(false);
      });

      /*it('should close the window if logout url is not defined', function () {

         $httpBackend.when('GET', 'templates/modalNotLoggedIn.html').respond();

         AppModel.logout();

         expect($window.location.href).to.not.equal('http://google.com');

         $httpBackend.flush();

         console.log($httpBackend);
      });*/
   });

   describe('.selectGroup()', function() {
      it('should convert any timezones to Olsen format that contain a space' , function() {
         var input = {
            'RemoteServerUrl': 'RSU',
            'SharedSecret': 'SS',
            'ID': 'SG',
            'Name': 'SGN',
            'TimeZone': 'America/Los         Angeles'
         };

         AppModel.logsList = [{
            EventTimestamp: 'Jan 12 2016 10:03:17 UTC'
         }];

         var getShortcodeSpy = sinon.stub(Utils, 'getTzCode').returns('PST');
         var addLocalTimeToLogsSpy = sinon.stub(AppModel, 'addLocalTimeToLogs');
         var eventSpy = sinon.stub($rootScope, '$broadcast');

         AppModel.selectGroup(input);
         expect(AppModel.timezone).to.equal('America/Los_Angeles');

         getShortcodeSpy.restore();
         addLocalTimeToLogsSpy.restore();
         eventSpy.restore();
      });

      it('apply the group values passed to the AppModel', function() {
         var input = {
            'RemoteServerUrl': 'RSU',
            'SharedSecret': 'SS',
            'ID': 'SG',
            'Name': 'SGN',
            'TimeZone': 'America/Los_Angeles'
         };

         AppModel.logsList = [{
            EventTimestamp: 'Jan 12 2016 10:03:17 UTC'
         }];

         var getShortcodeSpy = sinon.stub(Utils, 'getTzCode').returns('PST');
         var addLocalTimeToLogsSpy = sinon.stub(AppModel, 'addLocalTimeToLogs');
         var eventSpy = sinon.stub($rootScope, '$broadcast');

         AppModel.selectGroup(input);

         expect(AppModel.remoteServerUrl).to.equal('RSU');
         expect(AppModel.sharedSecret).to.equal('SS');
         expect(AppModel.selectedGroup).to.equal('SG');
         expect(AppModel.selectedGroupName).to.equal('SGN');
         expect(AppModel.timezone).to.equal('America/Los_Angeles');
         expect(AppModel.tzShortname).to.equal('PST');

         // method is called
         expect(addLocalTimeToLogsSpy.called).to.equal(true);
         expect(addLocalTimeToLogsSpy.args[0][0]).to.deep.equal([{ EventTimestamp: 'Jan 12 2016 10:03:17 UTC' }]);

         // event emitted
         expect(eventSpy.called).to.equal(true);
         expect(eventSpy.args[0][0]).to.equal('updateGroup');

         getShortcodeSpy.restore();
         addLocalTimeToLogsSpy.restore();
         eventSpy.restore();
      });
   });

   describe('.addLocalTimeToLogs()', function() {
      it('should set the logslist array if passed an array of logs', function() {

         AppModel.addLocalTimeToLogs([{
            EventTimestamp: 'Jan 12 2016 10:03:17 UTC'
         }]);

         expect(AppModel.logsList).to.be.an('array');
         expect(AppModel.logsList[0].LocalTime).to.not.be.undefined; // jshint ignore: line
         expect(AppModel.logsList).to.have.length(1);

      });

      it('should set the logsList to an empty array if no params are sent', function() {

         AppModel.addLocalTimeToLogs();

         expect(AppModel.logsList).to.be.an('array');
         expect(AppModel.logsList).to.deep.equal([]);

      });

   });

});
