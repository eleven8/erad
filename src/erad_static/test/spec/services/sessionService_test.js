'use strict';

/* New tests need to be added to  spec runner at ../index.html */

var expect = chai.expect;

// Example $httpBackend for mocking web service responses
describe('SessionService', function() {
   var SessionService, $httpBackend, $rootScope, AppModel;

   // before each test, spin up the module and inject the service
   beforeEach(angular.mock.module('services'));

   beforeEach(inject(function ($injector) {
      SessionService = $injector.get('SessionService');
      AppModel = $injector.get('AppModel');
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');

      AppModel.accessToken = 'cheeseToken';

      $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/session/load').respond({ Session: { LogoutUrl: 'cheese.com' } });
   }));

   afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
   });

   describe('.load()', function() {
      it('should save the logout url', function () {

         SessionService.load();
         $httpBackend.flush();
         expect(AppModel.logoutUrl).equals('http://cheese.com');

      });
   });
});
