'use strict';

/* New tests need to be added to  spec runner at ../index.html */

var expect = chai.expect;

describe('Utils', function() {

   var Utils, $log, $location, $window;
   var LA = 'America/Los_Angeles';
   var CEurope = 'Europe/Budapest';
   // uses UTC +5.5 timezone
   var badTz = 'America/Los        Angeles';

   // before each test, spin up the module and inject the service
   beforeEach(angular.mock.module('services', function ($provide) {

      var myWindow = {
         // mock the window.location object
         location: {
            'href': '',
            'search': '',
            'hash': ''
         },
         // keep the window.document object (for cookies)
         document: window.document
      };

      // provide myWindow as the $window
      $provide.value('$window', myWindow);
   }));

   beforeEach(inject(function ($injector) {
      $log = $injector.get('$log');
      $location = $injector.get('$location');
      $window = $injector.get('$window');
      Utils = $injector.get('Utils');
      Utils.deleteAllCookies();
   }));

   describe('.deleteAllCookies()', function() {
      it('should delete all cookies', function() {
         Utils.setCookie('c1', 'c1');
         Utils.setCookie('c2', 'c2');
         Utils.setCookie('c3', 'c3');
         expect(document.cookie).to.not.equal('');

         Utils.deleteAllCookies();
         expect(document.cookie).equals('');
      });
   });

   describe('.setCookie()', function() {
      it('should set a cookie', function() {
         Utils.setCookie('testCookie', 'testValue');
         expect(document.cookie).equals('testCookie=testValue');
      });
   });

   describe('.getCookie()', function() {
      it('should get a cookie', function () {
         Utils.setCookie('testCookie', 'testValue');
         var cookie = Utils.getCookie('testCookie');
         expect(cookie).equals('testValue');
      });
   });

   describe('.deleteCookie()', function() {
      it('should delete a cookie', function() {
         Utils.deleteCookie('testCookie');
         expect(document.cookie).equals('');
      });
   });

   describe('.getValidUrl()', function() {
      it('should correct a partial url', function() {
         expect(Utils.getValidUrl('www.google.com')).equals('http://www.google.com');
      });

      it('should ignore a full url', function() {
         expect(Utils.getValidUrl('https://www.google.com')).equals('https://www.google.com');
      });
   });

   describe('.logError()', function () {
      it('should log an error for a given object', function() {
         var errObj = { 'Test': 'Message', 'Error': { 'Message': 'Test Error Message', 'Code': 999 } };
         Utils.logError(errObj);
         expect($log.warn.logs[0][0]).to.equal('Error:');
         expect($log.warn.logs[1][0]).to.equal(errObj);
      });

      it('should log an error for a given string', function() {
         var errMsg = 'Test error message';
         Utils.logError(errMsg);
         expect($log.warn.logs[0][0]).to.equal('Error: Test error message');
      });

      it('should log unexpected error if there is no input', function() {
         Utils.logError();
         expect($log.warn.logs[0][0]).to.equal('Unknown Error Occurred');
      });
   });

   describe('.goTo()', function() {
      it('should set the hash', function() {
         Utils.goTo('id');
         expect($location.hash()).equals('id');
      });
   });

   describe('.getQueryVariable()', function () {

      it('should get the query value', function () {
         $window.location.search = '?api_domain=http://testing.example.org';
         expect(Utils.getQueryVariable('api_domain')).equals('http://testing.example.org');
      });
   });

   describe('.cleanTimeZone()', function () {

      it('should return a timezone in Olsen format or null, if no timezone passed', function () {
         var goodTzInput = 'America/Los_Angeles';
         var badTzInput = 'America/Los     Angeles';
         var missingTzInput = null;

         var goodInputResult = Utils.cleanTimeZone(goodTzInput);
         var badInputResult = Utils.cleanTimeZone(badTzInput);
         var missingInputResult = Utils.cleanTimeZone(missingTzInput);

         expect(goodInputResult).equals('America/Los_Angeles');
         expect(badInputResult).equals('America/Los_Angeles');
         expect(missingInputResult).equals(null);
      });
   });

   describe('.getTzCode()', function() {
      it('should handle timezones with multiple spaces correctly', function () {
         var noDST = 1452250028489;
         var DST   = 1467802028489;

         var malformednoDSTInput     = Utils.getTzCode(badTz, noDST);
         expect(malformednoDSTInput).equals('PST');

         var malformedDSTInput     = Utils.getTzCode(badTz, DST);
         expect(malformedDSTInput).equals('PDT');

      });

      it('should return a timezone shortcode according to DST', function () {
         // Fri Jan 08 2016 02:47:08 GMT-0800 (PST)
         // Fri Jan 08 2016 11:47:08 GMT+0100 (CET)
         var noDST = 1452250028489;

         // Wed Jul 06 2016 03:47:08 GMT-0700 (PDT)
         // Wed Jul 06 2016 12:47:08 GMT+0200 (CEST)
         var DST   = 1467802028489;

         // Simple LA check
         var LA_Standard        = Utils.getTzCode(LA, noDST);
         var LA_Daylight_Saving = Utils.getTzCode(LA, DST);

         expect(LA_Standard).equals('PST');
         expect(LA_Daylight_Saving).equals('PDT');

         // Simple Budapest check
         var Central_Europe_Standard = Utils.getTzCode(CEurope, noDST);
         var Central_European_Summer = Utils.getTzCode(CEurope, DST);

         expect(Central_Europe_Standard).equals('CET');
         expect(Central_European_Summer).equals('CEST');

         // Budapest CET -> CEST checks
         // Sun Mar 27 2016 01:00:41 GMT+0100 (CET)
         var justBeforeDST = 1459036841652;

         // Sun Mar 27 2016 03:00:41 GMT+0200 (CEST)
         var justAfterDST  = 1459040441652;

         // the time between the two point in time above
         // 'should' be 2 hours, but we've shifted
         // to DST so this is actually 1 hour
         var diff = justAfterDST - justBeforeDST;

         // last check for the sake of readability
         var oneHourNotTwo = 1000 * 60 * 60;

         var Central_Europe_Edge = Utils.getTzCode(CEurope, justBeforeDST);
         var Central_European_Over = Utils.getTzCode(CEurope, justAfterDST);

         expect(Central_Europe_Edge).equals('CET');
         expect(Central_European_Over).equals('CEST');
         expect(diff).equals(3600000); // one hour
         expect(diff).equals(oneHourNotTwo);

      });

   });

   describe('.filterMac()', function () {

      it('should append a caret to short MAC address', function () {

         var mac1 = ['11'];
         var mac2 = ['11', '22', '33', '44', '55', '66'];
         var mac3 = ['11', '22', '33'];

         expect(Utils.filterMac(mac1)).equals('11:^');
         expect(Utils.filterMac(mac2)).equals('11:22:33:44:55:66');
         expect(Utils.filterMac(mac3)).equals('11:22:33:^');

      });

      it('should handle array of numbers as input', function () {

         var mac1 = [11];
         var mac2 = [11, 22, 33, 44, 55, 66];
         var mac3 = [11, 22, 33 ];

         expect(Utils.filterMac(mac1)).equals('11:^');
         expect(Utils.filterMac(mac2)).equals('11:22:33:44:55:66');
         expect(Utils.filterMac(mac3)).equals('11:22:33:^');
      });

      it('should handle array input with missing values correctly', function () {

         var missingValue1 = [11, 22, 33, '', 55, 66 ];
         var missingValue2 = [11, 22, 33, '', '', 66 ];
         var missingValue3 = [11, 22, 33, 44, '', 66 ];
         var missingValue4 = [11, 22, 33, '', 55, '' ];
         var missingValue5 = ['ee', 22, 33, , 55, '' ];
         var missingValue6 = ['ff', 22, 33, undefined, 55, '' ];

         expect(Utils.filterMac(missingValue1)).equals('11:22:33:^');
         expect(Utils.filterMac(missingValue2)).equals('11:22:33:^');
         expect(Utils.filterMac(missingValue3)).equals('11:22:33:44:^');
         expect(Utils.filterMac(missingValue4)).equals('11:22:33:^');
         expect(Utils.filterMac(missingValue5)).equals('ee:22:33:^');
         expect(Utils.filterMac(missingValue6)).equals('ff:22:33:^');
      });
   });

});
