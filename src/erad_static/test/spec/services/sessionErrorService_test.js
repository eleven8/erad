'use strict';

/* New tests need to be added to  spec runner at ../index.html */

var expect = chai.expect;

describe('SessionErrorService', function() {

   var SessionErrorService, AppModel;
   var $modal, modalInstance, $window, $timeout;
   var modalSpy;

   var fakeModal = {
      result: {
         then: function (confirmCallback, cancelCallback) {
             this.confirmCallBack = confirmCallback;
             this.cancelCallback = cancelCallback;
             return this;
         },
         catch: function (cancelCallback) {
             this.cancelCallback = cancelCallback;
             return this;
         },
         finally: function (finallyCallback) {
             this.finallyCallback = finallyCallback;
             return this;
         }
      },
      close: function (item) {
         this.result.confirmCallBack(item);
      },
      dismiss: function (item) {
         this.result.cancelCallback(item);
      },
      finally: function () {
         this.result.finallyCallback();
      }
   };

   // before each test, spin up the module and inject the service
   beforeEach(angular.mock.module('services', function ($provide) {

      var myWindow = {
         // mock the window.location object
         location: {
            'href': '',
            'search': '',
            'hash': ''
         }
      };

      // provide myWindow as the $window
      $provide.value('$window', myWindow);
   }));

   beforeEach(function () {

      inject(function($injector) {

         AppModel = $injector.get('AppModel');
         SessionErrorService = $injector.get('SessionErrorService');
         $modal = $injector.get('$modal');
         $window = $injector.get('$window');
         $timeout = $injector.get('$timeout');
         modalInstance = fakeModal;
      });

      modalSpy = sinon.stub($modal, 'open').returns(fakeModal);

   });

   afterEach(function() {
      modalSpy.restore();
   });

   describe('.log()', function() {

      it('should call $modal.open when the method is called', function() {
         SessionErrorService.log({ type: 'someError' });
         expect(modalSpy).to.have.been.called; // jshint ignore: line
      });

  });

});
