'use strict';

/* New tests need to be added to  spec runner at ../index.html */

var expect = chai.expect;

describe('UserService', function() {

   var UserService, $httpBackend, $rootScope, AppModel, $window;

   beforeEach(angular.mock.module('services', function($provide) {

      var myWindow = {
         // mock the window.location object
         location: {
            'href': '',
            'search': '',
            'hash': ''
         },
         // keep the window.document object (for cookies)
         document: window.document
      };

      // provide myWindow as the $window
      $provide.value('$window', myWindow);
   }));

   beforeEach(inject(function($injector) {
      UserService = $injector.get('UserService');
      AppModel = $injector.get('AppModel');
      $window = $injector.get('$window');
      $httpBackend = $injector.get('$httpBackend');
      $rootScope = $injector.get('$rootScope');

      AppModel.accessToken = 'cheeseToken';
      AppModel.selectedGroup = 'myGroup';

   }));

   afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
   });

   describe('.list()', function() {
      it('should set the usersList', function() {
         $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/supplicant/list').respond({'Supplicant_List':[{}]});

         UserService.list();
         $httpBackend.flush();

         expect(AppModel.usersList).deep.equals([{}]);
      });
   });

   describe('.load()', function() {
      it('should return data', function() {
         $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/supplicant/load').respond({'some_data':'load values'});
         var promise = UserService.load();

         $httpBackend.flush();
         expect(promise.$$state.value).deep.equals({ 'some_data': 'load values' });
      });
   });

   describe('.save()', function() {
      it('should require a user object and return data', function () {
         var user = {
            Username: '',
            Vlan: '',
            UseRemote: '',
            Password: ''
         };

         $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/supplicant/save').respond({ 'some_data': 'save values' });
         var promise = UserService.save(user);

         $httpBackend.flush();
         expect(promise.$$state.value).deep.equals({ 'some_data': 'save values' });
      });
   });

   describe('.remove()', function() {
      it('should return data', function () {
         $httpBackend.expect('POST', 'https://testing.example.org/erad/admin/supplicant/delete').respond({ 'some_data': 'delete values' });
         var promise = UserService.remove();

         $httpBackend.flush();
         expect(promise.$$state.value).deep.equals({ 'some_data': 'delete values' });
      });
   });

   describe('.importUsers()', function() {
      it('should import OK', function () {

         var AppModelSpy = sinon.stub(AppModel, 'callService');

         UserService.importUsers({});

         // checking the headers are not set to application/json
         // to get the correct headers for the form submit
         // $http will set the Accept headers, etc.
         expect(AppModelSpy.args[0][0].headers).to.deep.equal({ 'Content-Type': undefined});
         expect(AppModelSpy.args[0][0].data).to.deep.equal({});

         AppModelSpy.restore();

      });
   });
});
