'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('AdminCtrl', function() {

    var adminController;
    var AppModel, AdminService, AdminAPIKeyService, SessionErrorService;
    var $httpBackend, $controller, $window, $state, $q, $scope, $rootScope, $modal;
    var modalSpy, errorServiceSpy;

    var fakeModal = {
        result: {
            then: function (confirmCallback, cancelCallback) {
                this.confirmCallBack = confirmCallback;
                this.cancelCallback = cancelCallback;
                return this;
            },
            catch: function (cancelCallback) {
                this.cancelCallback = cancelCallback;
                return this;
            },
            finally: function (finallyCallback) {
                this.finallyCallback = finallyCallback;
                return this;
            }
        },
        close: function (item) {
            this.result.confirmCallBack(item);
        },
        dismiss: function (item) {
            this.result.cancelCallback(item);
        },
        finally: function () {
            this.result.finallyCallback();
        }
    };
    beforeEach(angular.mock.module('eradControllers', function ($provide) {

      var myWindow = {
            // mock the window.location object
        location: {
            'href': '',
            'search': '',
            'hash': ''
        },
        // keep the window.document object (for cookies)
        document: window.document,
        close : window.close,
        open: window.open
      };

        // provide myWindow as the $window
        $provide.value('$window', myWindow);
        $provide.service('checkStatus', function () {
            return true;
        });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));

    beforeEach(inject(function ($injector) {
        AppModel        = $injector.get('AppModel');
        AdminService    = $injector.get('AdminService');
        AdminAPIKeyService = $injector.get('AdminAPIKeyService');
        SessionErrorService = $injector.get('SessionErrorService');
        $controller     = $injector.get('$controller');
        $window         = $injector.get('$window');
        $state          = $injector.get('$state');
        $rootScope      = $injector.get('$rootScope');
        $scope          = $rootScope.$new();
        $q              = $injector.get('$q');
        $httpBackend    = $injector.get('$httpBackend');
        $modal          = $injector.get('$modal');

        AppModel.logoutUrl = 'http://google.com';
        AppModel.isAccountOwner = 1;

        adminController = $controller('AdminCtrl', {
            'AppModel': AppModel,
            'AdminService': AdminService,
            'SessionErrorService': SessionErrorService,
            'AdminAPIKeyService': AdminAPIKeyService,
            '$state': $state,
            '$window': $window,
            '$q': $q,
            '$modal': $modal,
            'modalInstance': fakeModal,
            '$scope': $scope
        });

        adminController.loginPage = false;
        modalSpy = sinon.stub($modal, 'open').returns(fakeModal);
        errorServiceSpy = sinon.stub(SessionErrorService, 'handleError');
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
        //adminController.keys = [];
    });

    beforeEach(function (done) {

        $httpBackend
            .expect('POST', 'https://testing.example.org/erad/admin/account/list')
            .respond({ Error: null, Accounts: ['admin']});

        $httpBackend
            .expect('POST', 'https://testing.example.org/erad/admin/apikey/list')
            .respond({ Error: null, ApiKeyNames: ['key1'], ApiKeys: [{ApiKeyName: 'MyKey1', DisplayName: 'key1'}] });

        $httpBackend.flush();

        expect(adminController.accounts).to.deep.equal(['admin']);
        expect(adminController.keys).to.deep.equal([{ApiKeyName: 'MyKey1', DisplayName: 'key1'}]);

        done();
    });

    describe('openAddModal()', function () {

        it('will call $modal.open', function (done) {

            var modalOpenParam = {
                type: 'keys'
            };

            adminController.openAddModal(modalOpenParam);

            expect(modalSpy.called).to.be.ok;

            modalSpy.restore();

            done();
        });

        it('will call the correct endpoint based on the action passed', function (done) {

            // first test the modal returns a type so we can decide which endpoint to hit

            var modalOpenParam = {
                type: 'keys'
            };

            var replyFromModal = {
                type: 'keys',
                data:  {
                    DisplayName: 'key2',
                    Active: 1
                },
                action: 'ADD'
            };

            // define the correct function that is called based on the type ie, 'keys'
            var endpointSpy = sinon.stub(adminController, 'callSaveApiKeysEndpoint');
            var wrongEndpointSpy = sinon.stub(adminController, 'callSaveAccountEndpoint');

            adminController.openAddModal(modalOpenParam);

            adminController.modalInstance.close(replyFromModal);

            expect(modalSpy.called).to.be.ok;
            expect(endpointSpy.called).to.be.ok;
            expect(wrongEndpointSpy.called).to.be.false;

            // what's passed to the service is just what we get back from the modal
            expect(endpointSpy.args[0][0]).to.deep.equal(replyFromModal);

            modalSpy.restore();
            endpointSpy.restore();
            wrongEndpointSpy.restore();

            done();
        });

    });

    describe('openEditModal()', function () {
        it('will call $modal.open', function (done) {

            var modalData = {
                type: 'keys'
            };

            adminController.openEditModal('site', modalData);

            expect(modalSpy.called).to.be.ok;

            modalSpy.restore();

            done();
        });

        it('will call the appropriate endpoint based on the action passed', function (done) {

            // as the previous test in add(), but this time test the other function is
            // called. This time we need to pass a 2nd param to the function too, as
            // we're editing

            var modalOpenParam = {
                type: 'account'
            };

            var replyFromModal = {
                type: 'account',
                data: {
                   Email: 'me@you.co',
                   Password: 'reallygoodpassword'
                },
                action: 'EDIT'
            };

            // opposite to previous test
            var endpointSpy      = sinon.stub(adminController, 'callSaveAccountEndpoint');
            var wrongEndpointSpy = sinon.stub(adminController, 'callSaveApiKeysEndpoint');

            adminController.openEditModal('site', modalOpenParam);

            adminController.modalInstance.close(replyFromModal);

            expect(modalSpy.called).to.be.ok;
            expect(endpointSpy.called).to.be.ok;
            expect(wrongEndpointSpy.called).to.be.false;
            expect(endpointSpy.args[0].length).to.equal(1);

            modalSpy.restore();
            endpointSpy.restore();
            wrongEndpointSpy.restore();

            done();
        });

    });

    describe('openRemoveModal()', function () {
        it('will call $modal.open', function (done) {
            var modalData = {
                type: 'keys'
            };

            adminController.openAddModal(modalData);

            expect(modalSpy.called).to.be.ok;

            modalSpy.restore();

            done();
        });

        it('will call the endpoint with the correct data from the modal', function (done) {

            var toDelete = {
                type: 'account',
                data: 'you@me.com'
            };

            var endpointSpy      = sinon.stub(adminController, 'callDeleteEndpoint');

            adminController.openRemoveModal(toDelete.data, toDelete.type);

            adminController.modalInstance.close(toDelete);

            expect(modalSpy.called).to.be.ok;
            expect(endpointSpy.called).to.be.ok;

            expect(endpointSpy.args[0][0]).to.be.an('object');
            expect(endpointSpy.args[0][0]).to.deep.equal({
                type: toDelete.type,
                value: toDelete.data
            });

            modalSpy.restore();
            endpointSpy.restore();

            done();
        });
    });

    describe('callSaveApiKeysEndpoint()', function () {

        it('will call the API Key endpoint for when "adding" OK', function (done) {

            var serviceSpy = sinon.stub(AdminAPIKeyService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null, ApiKey: { ApiKeyName: "MyKey2", DisplayName: "key2", Active: 1 } });
                return deferred.promise;
            });

            var replyFromModal = {
                type: 'keys',
                data:  {
                    DisplayName: 'key2',
                    Active: 1
                },
                action: 'ADD'
            };

            adminController.callSaveApiKeysEndpoint(replyFromModal);

            $scope.$apply();

            // check whatever data object we close the modal with is
            // passed to AdminAPIKeyService.save
            expect(serviceSpy.args[0][0]).to.deep.equal({ DisplayName: 'key2', Active: 1 });

            // check the keys array is updated properly
            // But as we create the array in the beforeEach above
            // it already === [1] and we add the result to this
            expect(adminController.keys).to.deep.equal([{ApiKeyName: 'MyKey1', DisplayName: 'key1'}, {ApiKeyName: 'MyKey2', DisplayName: 'key2'}]);
            expect(errorServiceSpy.called).to.be.false;

            modalSpy.restore();
            serviceSpy.restore();
            errorServiceSpy.restore();

            done();
        });

        it('will call the API Key endpoint for when "editing" OK', function (done) {

            var serviceSpy = sinon.stub(AdminAPIKeyService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null, ApiKey: { ApiKeyName: "MyKey1", DisplayName: "key1", Active: 0} });
                return deferred.promise;
            });

            var replyFromModal = {
                type: 'keys',
                data:  {
                    DisplayName: 'key1',
                    ApiKeyName: 'MyKey1',
                    Active: 0
                },
                action: 'EDIT'
            };

            adminController.keys = [{ DisplayName: 'key1', ApiKeyName: 'MyKey1' }];
            adminController.callSaveApiKeysEndpoint(replyFromModal);

            $scope.$apply();

            // check whatever data object we close the modal with is
            // passed to AdminAPIKeyService.save
            expect(serviceSpy.args[0][0]).to.deep.equal({ DisplayName: 'key1', ApiKeyName: 'MyKey1', Active: 0 });

            // result should be that the array is the same as before
            // as we're editing
            expect(adminController.keys).to.deep.equal([{ DisplayName: 'key1', ApiKeyName: 'MyKey1' }]);
            expect(errorServiceSpy.called).to.be.false;

            modalSpy.restore();
            serviceSpy.restore();
            errorServiceSpy.restore();

            done();
        });

        it('will call an error if the save() fails', function (done) {

            var serviceSpy = sinon.stub(AdminAPIKeyService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'bad request' });
                return deferred.promise;
            });

            var replyFromModal = {
                type: 'keys',
                data:  {
                    ApiKeyName: 'key2',
                    Active: 1
                },
                action: 'ADD'
            };

            adminController.callSaveApiKeysEndpoint(replyFromModal);

            $scope.$apply();

            expect(serviceSpy.called).to.be.ok;
            expect(errorServiceSpy.args[0][0]).to.deep.equal({ Error: 'bad request' });
            // still the single item from top beforeEach
            expect(adminController.keys).to.deep.equal([{ DisplayName: 'key1', ApiKeyName: 'MyKey1' }]);

            modalSpy.restore();
            serviceSpy.restore();
            errorServiceSpy.restore();

            done();
        });

        it('will call an error if the action is not recognised', function (done) {

            var serviceSpy = sinon.stub(AdminAPIKeyService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            var replyFromModal = {
                type: 'keys',
                data:  {
                    ApiKeyName: 'key2',
                    Active: 1
                },
                action: 'UNKNOWN_ACTION'
            };

            adminController.callSaveApiKeysEndpoint(replyFromModal);

            $scope.$apply();

            expect(serviceSpy.called).to.be.ok;
            expect(errorServiceSpy.args[0][0]).to.deep.equal({ Error: 'Action not recognised' });
            // still just the single item from top beforeEach
            expect(adminController.keys).to.deep.equal([{ DisplayName: 'key1', ApiKeyName: 'MyKey1' }]);

            modalSpy.restore();
            serviceSpy.restore();
            errorServiceSpy.restore();

            done();
        });

    });

    describe('callSaveAccountEndpoint()', function () {
        it('will add new accounts ok', function (done) {
            var adminServiceSpy = sinon.stub(AdminService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            var replyFromModal = {
                data: {
                    Email: 'me@yahoo.co',
                    Password: 'somethingeasy'
                },
                type: 'account',
                action: 'ADD'
            };

            adminController.callSaveAccountEndpoint(replyFromModal);

            $scope.$apply();

            expect(adminServiceSpy.called).to.be.ok;
            expect(adminServiceSpy.args[0][0]).to.equal(replyFromModal.data);

            // expect the email to be added to the account array
            expect(adminController.accounts).to.deep.equal(['admin', 'me@yahoo.co']);
            expect(errorServiceSpy.called).to.be.false;

            adminServiceSpy.restore();
            errorServiceSpy.restore();

            done();

        });

        it('will edit accounts ok', function (done) {
            var adminSaveSpy = sinon.stub(AdminService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            var adminDeleteSpy = sinon.stub(AdminService, 'deleteAccount').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            var replyFromModal = {
                data: {
                    Email: 'admin',
                    Password: 'somethingdifferent'
                },
                type: 'account',
                action: 'EDIT'
            };

            adminController.callSaveAccountEndpoint(replyFromModal);

            $scope.$apply();

            expect(adminSaveSpy.called).to.be.ok;
            expect(adminSaveSpy.args[0][0]).to.equal(replyFromModal.data);

            expect(adminDeleteSpy.called).to.be.false;

            // expect the email to be added to the account array
            expect(adminController.accounts).to.deep.equal(['admin']);
            expect(errorServiceSpy.called).to.be.false;

            adminSaveSpy.restore();
            adminDeleteSpy.restore();
            errorServiceSpy.restore();

            done();

        });

        it('will call an error if the save() API call fails', function (done) {
            var adminSaveSpy = sinon.stub(AdminService, 'save').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'oh noes' });
                return deferred.promise;
            });

            var adminDeleteSpy = sinon.stub(AdminService, 'deleteAccount').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            var replyFromModal = {
                data: {
                    Email: 'me@yahoo.co',
                    Password: 'somethingeasy'
                },
                type: 'account',
                action: 'EDIT'
            };

            adminController.callSaveAccountEndpoint(replyFromModal);

            $scope.$apply();

            expect(adminSaveSpy.called).to.be.ok;
            expect(adminSaveSpy.args[0][0]).to.equal(replyFromModal.data);

            expect(errorServiceSpy.called).to.be.true;
            expect(adminDeleteSpy.called).to.be.false;

            // expect the email to be added to the account array
            expect(adminController.accounts).to.deep.equal(['admin']);

            adminSaveSpy.restore();
            adminDeleteSpy.restore();
            errorServiceSpy.restore();

            done();

        });
    });

    describe('callDeleteApiKeysEndpoint()', function () {
        it('will call the correct endpoint and delete ok', function (done) {

            var apiKeyDeleteSpy = sinon.stub(AdminAPIKeyService, 'deleteKey').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            var accountDeleteSpy = sinon.stub(AdminService, 'deleteAccount').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            var replyFromModal = {
                type: 'account',
                value: 'admin2'
            };

            // push to the array, so we can check it's state is right
            adminController.accounts.push('admin2');
            adminController.accounts.push('admin3');

            adminController.callDeleteEndpoint(replyFromModal);

            $scope.$apply();

            expect(accountDeleteSpy.called).to.be.ok;
            expect(apiKeyDeleteSpy.called).to.be.false;

            expect(errorServiceSpy.called).to.be.false;
            expect(adminController.accounts).to.deep.equal(['admin', 'admin3']);

            apiKeyDeleteSpy.restore();
            accountDeleteSpy.restore();
            errorServiceSpy.restore();

            done();

        });

        it('will call an error if delete fails', function (done) {

            var apiKeyDeleteSpy = sinon.stub(AdminAPIKeyService, 'deleteKey').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'delete failed' });
                return deferred.promise;
            });

            var replyFromModal = {
                type: 'keys',
                value: 'key54321'
            };

            adminController.callDeleteEndpoint(replyFromModal);

            $scope.$apply();

            expect(apiKeyDeleteSpy.called).to.be.ok;

            expect(errorServiceSpy.called).to.be.ok;
            expect(errorServiceSpy.args[0][0]).to.deep.equal({ Error: 'delete failed' });

            apiKeyDeleteSpy.restore();
            errorServiceSpy.restore();

            done();

        });
    });
});
