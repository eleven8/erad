'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('UsersCtrl', function() {

    var UsersController;
    var AppModel, GroupService, UserService, SessionErrorService;
    var $httpBackend, $controller, $state, $scope, $rootScope, $modal, $filter, ngTableParams, $q, $window;
    var fakeModal = {
        result: {
            then: function (confirmCallback, cancelCallback) {
                this.confirmCallBack = confirmCallback;
                this.cancelCallback = cancelCallback;
                return this;
            },
            catch: function (cancelCallback) {
                this.cancelCallback = cancelCallback;
                return this;
            },
            finally: function (finallyCallback) {
                this.finallyCallback = finallyCallback;
                return this;
            }
        },
        close: function (item) {
            this.result.confirmCallBack(item);
        },
        dismiss: function (item) {
            this.result.cancelCallback(item);
        },
        finally: function () {
            this.result.finallyCallback();
        }
    };
    var modalSpy, userServiceSpy, errorSpy;

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
        $provide.service('getUsers', function () {
            return [];
        });

        var myWindow = {
            // mock the window.location object
            location: {
                'href': '',
                'search': '',
                'hash': ''
            },
            // keep the window.document object (for cookies)
            document: window.document,
            close : window.close,
            open: window.open
        };

        // provide myWindow as the $window
        $provide.value('$window', myWindow);
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));
    beforeEach(inject(function ($injector) {
        AppModel        = $injector.get('AppModel');
        UserService     = $injector.get('UserService');
        $httpBackend    = $injector.get('$httpBackend');
        $controller     = $injector.get('$controller');
        $window         = $injector.get('$window');
        $state          = $injector.get('$state');
        $modal          = $injector.get('$modal');
        $filter         = $injector.get('$filter');
        $rootScope      = $injector.get('$rootScope');
        $scope          = $rootScope.$new();
        $q              = $injector.get('$q');

        SessionErrorService = $injector.get('SessionErrorService');

        ngTableParams   = $injector.get('ngTableParams');

        AppModel.isAccountOwner = 1;
        AppModel.accessToken = 'token';
        AppModel.selectedGroup = 'theGroup';
        AppModel.apiDomain = 'https://testing.example.org';

        UsersController = $controller('UsersCtrl', {
            'AppModel': AppModel,
            'GroupService': GroupService,
            'UserService': UserService,
            '$scope': $scope,
            '$state': $state,
            '$modal': $modal,
            '$filter': $filter,
            '$q': $q,
            '$modalInstance': fakeModal
        });

        modalSpy        = sinon.stub($modal, 'open').returns(fakeModal);
        errorSpy        = sinon.stub(SessionErrorService, 'handleError');

        $httpBackend
            .expect('POST', 'https://testing.example.org/erad/admin/supplicant/list')
            .respond(200, {"Error": null });
        $httpBackend.flush();

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('show admin only fields', function () {

        it('will be true if isAccountOwner is 1', function (done) {

            expect(UsersController.formEditable).to.be.ok;

            done();
        });

    });

    describe('createUser()', function () {
        it('will launch the createUser modal', function (done) {

            UsersController.createUser();

            expect(modalSpy).to.be.ok;
            expect($scope.ModalMode).to.equal('create');

            done();
        });
    });

    describe('deleteUser()', function () {
        it('will launch the deleteUser modal', function (done) {

            var user = {};

            UsersController.deleteUser(user);

            expect(modalSpy.called).to.be.ok;
            expect($scope.ModalMode).to.be.equal('delete');

            done();
        });
    });

    describe('updateUser()', function () {

        it('will launch the updateUser modal window', function (done) {

            var user = {
                Description: 'desc',
                DeviceName: 'device',
                ExpirationDate: null,
                Group_ID: 'elevenos::Global',
                Location: null,
                Password: 'onely',
                UseRemote: false,
                UseWildcard: false,
                Username: 'none',
                Vlan: null
            };

            userServiceSpy = sinon.stub(UserService, 'load').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null, Supplicant: user });
                return deferred.promise;
            });

            UsersController.updateUser({ Username: 'OK!' });

            $scope.$apply();

            expect(userServiceSpy.called).to.be.ok;

            expect(modalSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;
            expect($scope.ModalMode).to.be.equal('update');

            userServiceSpy.restore();

            done();
        });

        it('will send an error if UserService.load() fails', function (done) {

            userServiceSpy = sinon.stub(UserService, 'load').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'No user!' });
                return deferred.promise;
            });

            UsersController.updateUser({ Username: 'OK!' });

            $scope.$apply();

            expect(userServiceSpy.called).to.be.ok;
            expect($scope.ModalMode).to.equal('update');

            expect(modalSpy.called).to.be.false;
            expect(errorSpy.called).to.be.true;

            userServiceSpy.restore();

            done();
        });

    });

    describe('importUsers()', function () {
        it('will open the import users modal', function (done) {

            UsersController.importUsers();

            expect($scope.ModalMode).to.equal('import');
            expect(modalSpy.called).to.be.true;
            expect(errorSpy.called).to.be.false;

            done();
        });

    });

    describe('exportUsers()', function () {
        it('will launch the help window', function (done) {

            var url = AppModel.apiDomain + '/erad/admin/supplicant/export/csv?Session_ID=token&Group_ID=theGroup';

            expect($window.location.href).to.equal('');

            UsersController.exportUsers();

            expect(UsersController.exportUrl).to.equal(url);
            expect($window.location.href).to.equal(url);

            done();
        });
    });

});
