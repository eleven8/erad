'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('HeaderCtrl', function() {

    var headerController;
    var AppModel;
    var $httpBackend, $controller, $window, $state;
    beforeEach(angular.mock.module('eradControllers', function ($provide) {

      var myWindow = {
            // mock the window.location object
        location: {
            'href': '',
            'search': '',
            'hash': ''
        },
        // keep the window.document object (for cookies)
        document: window.document,
        close : window.close,
        open: window.open
      };

      // provide myWindow as the $window
      $provide.value('$window', myWindow);
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));

    beforeEach(inject(function ($injector) {
        AppModel        = $injector.get('AppModel');
        $controller     = $injector.get('$controller');
        $window         = $injector.get('$window');
        $state          = $injector.get('$state');
        $httpBackend    = $injector.get('$httpBackend');

        AppModel.logoutUrl = 'http://google.com';

        headerController = $controller('HeaderCtrl', {
            'AppModel': AppModel,
            '$state': $state,
            '$window': $window
        });

        headerController.loginPage = false;

    }));

    it('will signal if current view is the login page', function (done) {

        if ($state.is('login')) {
            expect(headerController.loginPage).to.be.ok;
        } else {
            expect(headerController.loginPage).to.be.false;
        }

        done();
    });

    describe('launchHelp()', function () {

        it('will launch the help window', function (done) {

            var spy = sinon.stub( $window, 'open');

            headerController.launchHelp();

            expect(spy.calledOnce).to.be.ok;
            expect(spy.args[0][0]).to.equal('http://help.enterpriseauth.com');

            spy.restore();
            done();
        });

    });

    describe('logout()', function () {

        // this is just a wrapper around the service method
        it('will log the user out', function (done) {

            var spy = sinon.stub( headerController.AppModel, 'logout');

            headerController.logout();

            expect(spy.calledOnce).to.be.ok;

            // also tested in the AppModel service tests
            expect(headerController.AppModel.isLoggedIn).to.be.null;
            expect(headerController.AppModel.accessToken).to.be.null;

            spy.restore();

            done();

        });
    });

});
