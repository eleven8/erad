'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('AaaLogCtrl', function() {

    var aaaLogController;
    var AppModel, GroupService, SessionErrorService, AaaLogService, DateService;
    var ngTableParams;
    var $rootScope, $q, $state, $filter, $modal, $controller, $httpBackend, $scope;
    var stateSpy, dateSpy, tableReloadSpy, errorSpy, convertedTimeSpy;

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
        $provide.service('getAaaLogs', function () {
            return [];
        });
        var myWindow = {
            // mock the window.location object
            location: {
                'href': '',
                'search': '',
                'hash': ''
            },
            moment: window.moment,
            // keep the window.document object (for cookies)
            document: window.document,
            Math: {
                sign: function () {},
                abs: function () {}
            }
        };
        // provide myWindow as the $window
        $provide.value('$window', myWindow);
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));
    beforeEach(inject(function ($injector) {
        AppModel        = $injector.get('AppModel');
        AppModel.timezone = 'America/Chicago';
        AppModel.accessToken = 'cheeseToken';

        GroupService    = $injector.get('GroupService');
        SessionErrorService = $injector.get('SessionErrorService');
        AaaLogService   = $injector.get('AaaLogService');
        DateService     = $injector.get('DateService');

        ngTableParams   = $injector.get('ngTableParams');

        $rootScope      = $injector.get('$rootScope');
        $state          = $injector.get('$state');
        $filter         = $injector.get('$filter');
        $modal          = $injector.get('$modal');
        $q              = $injector.get('$q');
        $controller     = $injector.get('$controller');
        $httpBackend    = $injector.get('$httpBackend');

        $scope = $rootScope.$new();

        aaaLogController = $controller('AaaLogCtrl', {
            'AppModel': AppModel,
            'GroupService': GroupService,
            'SessionErrorService': SessionErrorService,
            'AaaLogService': AaaLogService,
            'DateService': DateService,

            'ngTableParams': ngTableParams,

            '$scope': $scope,
            '$state': $state,
            '$filter': $filter,
            '$modal': $modal
        });

        stateSpy    = sinon.stub($state, 'go');
        dateSpy     = sinon.stub(DateService, 'calcDate');
        errorSpy    = sinon.stub(SessionErrorService, 'log');
        tableReloadSpy = sinon.stub(aaaLogController.tableParams, 'reload').callsFake(function () {
            return $q.when();
        });

        $httpBackend
            .expect('POST', 'https://testing.example.org/erad/admin/supplicant/log/group/list')
            .respond(200, {"Error": null, "Radius_Log": [] });
        $httpBackend.flush();

    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    it('should set up some page defaults', function (done) {

        expect(aaaLogController.timezoneTooltipText).to.not.be.undefined;
        expect(aaaLogController.includeStrayRequests).to.be.false;
        expect(aaaLogController.includeBubbleLogs).to.be.false;
        expect(aaaLogController.showRefreshButton).to.be.true;
        expect(aaaLogController.calendarIsOpened).to.be.false;

        done();

    });

    // toggles calendar popup visibility
    describe('openCalendar()', function () {
        it('should toggle the view of the calendar', function (done) {

            aaaLogController.open();
            expect(aaaLogController.calendarIsOpened).to.be.ok;

            aaaLogController.open();
            expect(aaaLogController.calendarIsOpened).to.be.false;

            done();
        });

    });

    // refresh table data button
    describe('refreshDataTable', function () {
        it('should refresh the table', function (done) {

            aaaLogController.includeStrayRequests = true;
            // 'Mon Feb 01 2016 15:13:07 GMT+0100 (CET)';
            var convertedDate = [1454313600000, 1454399999999];
            aaaLogController.selectedDate = convertedDate;

            dateSpy.returns(convertedDate);

            var aaaLogSpy = sinon.stub(AaaLogService, 'list').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null, Radius_Log: [{ Group_ID: 'one' }, {Group_ID: 'two' }] });
                return deferred.promise;
            });

            $scope.$apply();

            aaaLogController.refreshDataTable();

            expect(errorSpy.called).to.be.false;
            expect(dateSpy.called).to.be.ok;
            expect(aaaLogSpy.called).to.be.ok;
            expect(tableReloadSpy.called).to.equal(true);

            aaaLogSpy.restore();
            dateSpy.restore();

            done();

        });

        it('should call an error when no logs are found', function (done) {

            aaaLogController.includeStrayRequests = true;
            aaaLogController.selectedDate = 'Mon Feb 01 2016 15:13:07 GMT+0100 (CET)';

            dateSpy.returns(true);

            var aaaLogSpy = sinon.stub(AaaLogService, 'list').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'Major Error!!' });
                return deferred.promise;
            });

            aaaLogController.refreshDataTable();

            $scope.$apply();

            expect(tableReloadSpy.called).to.equal(true);
            expect(aaaLogSpy.called).to.equal(true);
            expect(errorSpy.called).to.equal(true);

            aaaLogSpy.restore();
            dateSpy.restore();

            done();

        });
    });

    describe('setInitialDate', function () {

        it('should set the date', function (done) {

            convertedTimeSpy = sinon.stub(DateService, 'getConvertedTime').returns({});

            aaaLogController.setInitialDate();

            expect(convertedTimeSpy.called).to.be.ok;
            expect(aaaLogController.someDate).to.not.be.undefined;

            convertedTimeSpy.restore();

            done();
        });
    });

    describe('goBack()', function () {

        it('should call $state.go', function (done) {

            var goBackSpy = sinon.stub(aaaLogController, 'goBack');

            aaaLogController.goBack();

            expect(goBackSpy.called).to.be.ok;
            expect(stateSpy.calledWith('users'));

            goBackSpy.restore();
            stateSpy.restore();

            done();
        });
    });

});
