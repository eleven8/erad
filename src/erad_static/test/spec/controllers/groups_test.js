'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('GroupsCtrl', function() {

    var GroupsCtrl;
    var AppModel, GroupService, UserService, SessionErrorService, AuthService;
    var $filter, $modal, $httpBackend, $controller, $state, $scope, $rootScope, $q;

    var modalSpy, errorSpy, groupServiceSpy, groupListSpy, authSpy;
    var site, index;

    var fakeModal = {
        result: {
            then: function (confirmCallback, cancelCallback) {
                this.confirmCallBack = confirmCallback;
                this.cancelCallback = cancelCallback;
                return this;
            },
            catch: function (cancelCallback) {
                this.cancelCallback = cancelCallback;
                return this;
            },
            finally: function (finallyCallback) {
                this.finallyCallback = finallyCallback;
                return this;
            }
        },
        close: function (item) {
            this.result.confirmCallBack(item);
        },
        dismiss: function (item) {
            this.result.cancelCallback(item);
        },
        finally: function () {
            this.result.finallyCallback();
        }
    };
    beforeEach(angular.mock.module('eradControllers', function ($provide) {
      $provide.service('getGroups', function () {
        //return { title: 'Delete Site', data: { ID: 'site', Name: 'site', }, indx: 1 };
        return [];
      });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));
    beforeEach(inject(function ($injector) {
        AppModel        = $injector.get('AppModel');
        UserService     = $injector.get('UserService');
        GroupService    = $injector.get('GroupService');
        SessionErrorService = $injector.get('SessionErrorService');
        AuthService     = $injector.get('AuthService');
        $httpBackend    = $injector.get('$httpBackend');
        $controller     = $injector.get('$controller');

        $state          = $injector.get('$state');
        $modal          = $injector.get('$modal');
        $filter         = $injector.get('$filter');
        $rootScope      = $injector.get('$rootScope');
        $scope          = $rootScope.$new();
        $q              = $injector.get('$q');

        AppModel.accessToken = 'cheeseToken';
        AppModel.isAccountOwner = 1;
        AppModel.groupList = ['one', 'two', 'three'];
        AppModel.isLoggedIn = true;

        GroupsCtrl = $controller('GroupsCtrl', {
            'AppModel': AppModel,
            'GroupService': GroupService,
            'UserService': UserService,
            'SessionErrorService': SessionErrorService,
            'AuthService': AuthService,

            '$scope': $scope,
            '$state': $state,
            '$modal': $modal,
            '$filter': $filter,
            '$q': $q,
            'modalInstance': fakeModal
        });

        site = { ID: 'site', Name: 'site', };
        index = 1;

        GroupsCtrl.toDelete = { title: 'Delete Site', data: site, indx: index };

        modalSpy        = sinon.stub($modal, 'open').returns(fakeModal);
        errorSpy        = sinon.stub(SessionErrorService, 'handleError');
        authSpy         = sinon.stub(AuthService, 'isAuthed').callsFake(function () {
            return $q.when(true);
        });
        groupListSpy = sinon.stub(GroupService, 'list').callsFake(function () {
            var deferred = $q.defer();
            deferred.resolve({ Error: null, Group_List: [{ ID: 'site' }] });
            return deferred.promise;
        });

        $httpBackend
            .expect('POST', 'https://testing.example.org/erad/admin/group/list')
            .respond(200, {"Error": null, "Group_List": [] });
        $httpBackend.flush();

        $scope.$digest();

    }));

    afterEach(function () {
        errorSpy.restore();
        modalSpy.restore();
        authSpy.restore();
        groupListSpy.restore();
    });

   describe('removeGroup()', function () {

        it('will not call the SessionErrorService if save is ok', function (done) {

            groupServiceSpy = sinon.stub(GroupService, 'remove').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null });
                return deferred.promise;
            });

            GroupsCtrl.removeSite(site, index);

            GroupsCtrl.modalInstance.close('Ok');

            $scope.$apply();

            expect(modalSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;
            expect(groupServiceSpy.called).to.be.ok;

            groupServiceSpy.restore();

            done();

        });

        it('will call the SessionErrorService if delete failed', function (done) {

            groupServiceSpy = sinon.stub(GroupService, 'remove').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'fail' });
                return deferred.promise;
            });

            GroupsCtrl.removeSite(site, index);

            GroupsCtrl.modalInstance.close('Ok');

            $scope.$apply();

            expect(modalSpy.called).to.be.ok;
            expect(errorSpy.called).to.equal(true);
            //expect(errorSpy.args[0][0]).to.equal(1);

            groupServiceSpy.restore();

            done();
        });

        xit('will remove the deleted group from the groups array if delete is successfull', function () {});
    });
});
