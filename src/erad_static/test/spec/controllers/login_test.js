'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('LoginCtrl', function() {

    var loginController;
    var AppModel, SessionService, AuthService;
    var $state,$log, $modal, $rootScope, $controller, $httpBackend, $q;
    var modalSpy, authServiceSpy;

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));

    beforeEach(inject(function ($injector) {
        AppModel        = $injector.get('AppModel');
        SessionService  = $injector.get('SessionService');
        AuthService     = $injector.get('AuthService');
        $q              = $injector.get('$q');
        $state          = $injector.get('$state');
        $log            = $injector.get('$log');
        $modal          = $injector.get('$modal');
        $rootScope      = $injector.get('$rootScope');
        $controller     = $injector.get('$controller');
        $httpBackend    = $injector.get('$httpBackend');

        modalSpy        = sinon.stub($modal, 'open');
        authServiceSpy  = sinon.stub(AuthService, 'getAuthed');

        loginController = $controller('LoginCtrl', {
            'AppModel': AppModel,
            'SessionService': SessionService,
            'AuthService': AuthService,
            '$state': $state,
            '$log': $log,
            '$modal': $modal,
            '$scope': $rootScope,
            '$q': $q
        });

        // set to true as we the form doesn't allow submits
        // unless it's valid
        loginController.loginForm = {
            $valid: true
        };

        // default values
        loginController.error       = null;
        loginController.showError   = false;

        AppModel.isLoggedIn = null;
        AppModel.notLoggedIn = null;
        AppModel.accessToken = null;
        AppModel.isSecureUrl = true;
        AppModel.logoutUrl = null;
        AppModel.groupIDList = [];
        AppModel.remoteServerUrl = '';
        AppModel.sharedSecret = '';
        AppModel.OverrideVlan = false;
        AppModel.OverrideConnectionSpeed = false;
        AppModel.selectedGroup = null;
        AppModel.selectedGroupName = null;
        AppModel.timezone = null;
        AppModel.isAccountOwner = 0;
        AppModel.accountId = null;

    }));

    afterEach(function () {});

    describe('login()', function() {

        it('should check login function calls an error correctly', function (done) {

            var data = {};
            var deferred = $q.defer();
            var promise = deferred.promise;

            expect(AppModel.accessToken).to.be.null;

            loginController.login(data);

            $httpBackend
                .expect('POST', 'https://testing.example.org/erad/public/account/login')
                .respond(403, { Error: 'Login Failed.' });

            authServiceSpy.returns(promise);

            deferred.resolve(true);

            $httpBackend
                .flush();

            expect(loginController.error).to.equal('Login Failed.');
            expect(loginController.showError).to.be.ok;

            $httpBackend.verifyNoOutstandingRequest();
            $httpBackend.verifyNoOutstandingExpectation();

            authServiceSpy.restore();
            done();
        });

        it('should check login flow runs ok', function (done) {

            var data = {};
            var deferred = $q.defer();
            var promise = deferred.promise;

            expect(AppModel.accessToken).to.be.null;
            expect(AppModel.isLoggedIn).to.be.null;

            loginController.login(data);

            //order of expected request is essential
            $httpBackend
                .expect('POST', 'https://testing.example.org/erad/public/account/login')
                .respond(200, {
                    Session_ID: 'someFakeToken'
                });

            authServiceSpy.returns(promise);

            deferred.resolve(true);

            $httpBackend
                .flush();

            // no error message when login is good
            expect(loginController.error).to.be.null;
            expect(loginController.showError).to.be.false;

            expect(authServiceSpy.called).to.be.ok;

            $httpBackend.verifyNoOutstandingRequest();
            $httpBackend.verifyNoOutstandingExpectation();

            authServiceSpy.restore();
            done();
        });
    });

    describe('toggleError()', function () {

        it('should check showError function toggles correctly', function (done) {

            // reset to initial state
            loginController.error       = null;
            loginController.showError   = false;

            loginController.toggleError();
            expect(loginController.showError).to.be.ok;

            loginController.toggleError();
            expect(loginController.showError).to.be.false;

            loginController.toggleError();
            expect(loginController.showError).to.be.ok;

            done();
        });
    });
});
