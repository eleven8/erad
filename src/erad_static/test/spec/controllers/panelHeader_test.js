'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('PanelHeaderCtrl', function() {

    var PanelHeaderCtrl;
    var AppModel, GroupService, UserService, SessionErrorService;
    var $filter, $modal, $httpBackend, $controller, $state, $scope, $rootScope, $q;
    var modalSpy, errorSpy, groupServiceSpy, groupListSpy;

    var fakeModal = {
        result: {
            then: function (confirmCallback, cancelCallback) {
                this.confirmCallBack = confirmCallback;
                this.cancelCallback = cancelCallback;
                return this;
            },
            catch: function (cancelCallback) {
                this.cancelCallback = cancelCallback;
                return this;
            },
            finally: function (finallyCallback) {
                this.finallyCallback = finallyCallback;
                return this;
            }
        },
        close: function (item) {
            this.result.confirmCallBack(item);
        },
        dismiss: function (item) {
            this.result.cancelCallback(item);
        },
        finally: function () {
            this.result.finallyCallback();
        }
    };
    beforeEach(angular.mock.module('eradControllers', function ($provide) {
      $provide.service('data', function () {
        return [];
      });
    }));

    beforeEach(angular.mock.module('eradConfig', 'services', 'eradControllers'));
    beforeEach(inject(function ($injector) {
        AppModel        = $injector.get('AppModel');
        UserService     = $injector.get('UserService');
        GroupService    = $injector.get('GroupService');
        SessionErrorService = $injector.get('SessionErrorService');

        $httpBackend    = $injector.get('$httpBackend');
        $controller     = $injector.get('$controller');

        $state          = $injector.get('$state');
        $modal          = $injector.get('$modal');
        $filter         = $injector.get('$filter');
        $q              = $injector.get('$q');
        $rootScope      = $injector.get('$rootScope');
        $scope          = $rootScope.$new();

        AppModel.usersList = [];

        PanelHeaderCtrl = $controller('PanelHeaderCtrl', {
            'AppModel': AppModel,
            'GroupService': GroupService,
            'UserService': UserService,
            'SessionErrorService': SessionErrorService,
            '$scope': $scope,
            '$state': $state,
            '$modal': $modal,
            '$filter': $filter,
            'modalInstance': fakeModal,
            '$q': $q
        });

        modalSpy        = sinon.stub($modal, 'open').returns(fakeModal);
        errorSpy        = sinon.stub(SessionErrorService, 'handleError');

    }));

    afterEach(function () {
        errorSpy.restore();
        modalSpy.restore();
        groupListSpy.restore();
        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('addGroup()', function () {

        it('will call the SessionErrorService if save fails', function (done) {

            groupListSpy = sinon.stub(GroupService, 'list');

            PanelHeaderCtrl.addGroup();

            PanelHeaderCtrl.modalInstance.close({ Error: 'error' });

            expect(errorSpy.called).to.be.ok;
            expect(groupListSpy.called).to.be.false;
            done();

        });

        it('will not call the SessionErrorService if save is ok', function (done) {

            groupListSpy = sinon.stub(GroupService, 'list').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null, Group_List: ['one']});
                return deferred.promise;
            });

            PanelHeaderCtrl.addGroup();

            $scope.$apply();

            PanelHeaderCtrl.modalInstance.close({ Error: null, Group: 'error' });

            expect(errorSpy.called).to.be.false;
            expect(groupListSpy.called).to.be.ok;
            done();

        });
    });

    describe('updateGroup() success', function () {
        it('will launch the updateGroup modal window when there are groups', function (done) {

            var group = {
                RemoteServerUrl: 'url',
                SharedSecret: 'secret',
                ID: 'id',
                Name: 'name',
                TimeZone: 'America/Chicage'
            };

            groupServiceSpy = sinon.stub(GroupService, 'load').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null, Group: group });
                return deferred.promise;
            });

            PanelHeaderCtrl.updateGroup();

            $scope.$apply();

            expect(groupServiceSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.false;
            expect(modalSpy.called).to.be.ok;

            groupServiceSpy.restore();

            done();
        });

    });

    describe('updateGroup() fail', function () {
        it('will call the SessionErrorService when there are no groups', function (done) {

            groupServiceSpy = sinon.stub(GroupService, 'load').callsFake(function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: 'error' });
                return deferred.promise;
            });

            PanelHeaderCtrl.updateGroup();

            $scope.$apply();

            expect(groupServiceSpy.called).to.be.ok;
            expect(errorSpy.called).to.be.ok;
            expect(errorSpy.args[0][0]).to.deep.equal({ Error: 'error' });
            expect(modalSpy.called).to.be.false;

            groupServiceSpy.restore();

            done();
        });

    });

});
