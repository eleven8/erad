'use strict';
/*jshint expr: true*/

/* New tests need to be added to  spec runner at ../index.html */
var expect = chai.expect;

describe('ChangeHistoryCtrl', function() {

    var ChangeHistoryCtrl;
    var AppModel, SessionErrorService, ChangeHistoryService, DateService, AuthService, Utils;
    var ngTableParams;
    var $rootScope, $q, $state, $filter, $controller, $httpBackend, $scope, $window;
    var stateSpy, dateSpy, tableReloadSpy, errorSpy;

    beforeEach(angular.mock.module('eradControllers', function ($provide) {
      $provide.service('getChangeHistoryLogs', function () {
        return [];
      });
    }));

    beforeEach(angular.mock.module('eradConfig','services', 'eradControllers'));
    beforeEach(inject(function ($injector) {
        AppModel        = $injector.get('AppModel');
        AppModel.timezone = 'America/Los_Angeles';
        AppModel.accessToken = 'cheeseToken';
        AppModel.logsList = [];

        AuthService     = $injector.get('AuthService');
        SessionErrorService = $injector.get('SessionErrorService');
        ChangeHistoryService = $injector.get('ChangeHistoryService');
        DateService     = $injector.get('DateService');
        Utils           = $injector.get('Utils');

        ngTableParams   = $injector.get('ngTableParams');

        $rootScope      = $injector.get('$rootScope');
        $state          = $injector.get('$state');
        $filter         = $injector.get('$filter');
        $q              = $injector.get('$q');
        $controller     = $injector.get('$controller');
        $httpBackend    = $injector.get('$httpBackend');
        $window         = $injector.get('$window');

        $scope = $rootScope.$new();

        ChangeHistoryCtrl = $controller('ChangeHistoryCtrl', {
            'AppModel': AppModel,
            'SessionErrorService': SessionErrorService,
            'ChangeHistoryService': ChangeHistoryService,
            'DateService': DateService,
            'AuthService': AuthService,
            'Utils': Utils,
            'ngTableParams': ngTableParams,

            '$window': $window,
            '$scope': $scope,
            '$state': $state,
            '$filter': $filter
        });

        ChangeHistoryCtrl.opened = {
            start: false,
              end: false
        };

        stateSpy    = sinon.stub($state, 'go');
        dateSpy     = sinon.stub(DateService, 'calcDate').returns([1454313600000, 1454399999999]);
        errorSpy    = sinon.stub(SessionErrorService, 'log');

        $httpBackend
            .expect('POST', 'https://testing.example.org/erad/admin/audit/search')
            .respond(200, {"Error": null, "Group_List": [] });
        $httpBackend.flush();
    }));

    afterEach(function () {
        stateSpy.restore();
        dateSpy.restore();
        errorSpy.restore();
        tableReloadSpy.restore();

        $httpBackend.verifyNoOutstandingRequest();
        $httpBackend.verifyNoOutstandingExpectation();
    });

    describe('Startup', function () {

        it('should set some values on start', function (done) {

            tableReloadSpy = sinon.stub(ChangeHistoryCtrl.tableParams, 'reload').callsFake(function () {
                return $q.when();
            });

            $scope.$digest();

            // AppModel
            expect(ChangeHistoryCtrl.AppModel).is.not.undefined;

            // check we still get an array when we have an empty reply from the server
            expect(ChangeHistoryCtrl.logdata).to.be.an('array');

            // check the calendars start closed
            expect(ChangeHistoryCtrl.opened.start).to.be.false;
            expect(ChangeHistoryCtrl.opened.end).to.be.false;

            // check the calendar has a start and end date
            expect(ChangeHistoryCtrl.date).is.ok;
            expect(ChangeHistoryCtrl.date.startDate).is.ok;
            expect(ChangeHistoryCtrl.date.endDate).is.ok;

            // table
            expect(tableReloadSpy.called).is.not.undefined;

            done();
        });

    });

    describe('open()', function () {

        it('should toggle the opened/closed status of the calendar passed to the function', function (done) {

            //open start calendar
            ChangeHistoryCtrl.open('start');
            expect(ChangeHistoryCtrl.opened.start).to.be.ok;
            expect(ChangeHistoryCtrl.opened.end).to.be.false;

            // close start calendar
            ChangeHistoryCtrl.open('start');
            expect(ChangeHistoryCtrl.opened.start).to.be.false;
            expect(ChangeHistoryCtrl.opened.end).to.be.false;

            // open end calendar
            ChangeHistoryCtrl.open('end');
            expect(ChangeHistoryCtrl.opened.end).to.be.ok;
            expect(ChangeHistoryCtrl.opened.start).to.be.false;

            // close end calendar
            ChangeHistoryCtrl.open('end');
            expect(ChangeHistoryCtrl.opened.end).to.be.false;
            expect(ChangeHistoryCtrl.opened.start).to.be.false;

            done();

        });
    });

    // refresh table data button
    xdescribe('refreshDataTable', function () {
        it('should refresh the table', function (done) {

            var initSpy =   sinon.stub(ChangeHistoryService, 'list', function () {
                var deferred = $q.defer();
                deferred.resolve({ Error: null, Audit_List: [{ Group_ID: 'one' }] });
                return deferred.promise;
            });

            ChangeHistoryCtrl.refreshDataTable();

            $scope.$apply();

            expect(initSpy.called).to.be.ok;
            expect(ChangeHistoryCtrl.logdata).to.deep.equal([{ Group_ID: 'one' }]);

            initSpy.restore();

            done();

        });
    });

});
