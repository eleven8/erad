const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;



module.exports = {
	target: "web",
	mode: 'production',
	// devtool: 'cheap-eval-source-map',
	resolve: {
		alias: {
			'spin.js': path.resolve(__dirname, 'bower_components/spin.js/spin.js'),
			'moment': path.resolve(__dirname, 'bower_components/moment/moment.js'),
			'moment-timezone': path.resolve(__dirname, 'bower_components/moment-timezone/builds/moment-timezone-with-data-2010-2020.js'),

			'jquery': path.resolve(__dirname, 'bower_components/jquery/dist/jquery.js'),

			'angular': path.resolve(__dirname, 'bower_components/angular/angular.js'),
			'ng-table': path.resolve(__dirname, 'bower_components/ng-table/dist/ng-table.js'),
			'ui.router': path.resolve(__dirname, 'bower_components/angular-ui-router/release/angular-ui-router.js'),

			'angular-bootstrap': path.resolve(__dirname, 'bower_components/angular-bootstrap/ui-bootstrap-tpls.js'),

			'chosen-js': path.resolve(__dirname, 'bower_components/chosen/chosen.jquery.js'),

			'chart.js': path.resolve(__dirname, 'bower_components/Chart.js/dist/Chart.min.js'),
			'angular-chart': path.resolve(__dirname, 'bower_components/angular-chart.js/dist/angular-chart.js'),
			'angularjs-slider': path.resolve(__dirname, 'bower_components/angularjs-slider/dist/rzslider.js'),
			'angular-ui-bootstrap-datetimepicker': path.resolve(__dirname, 'bower_components/angular-ui-bootstrap-datetimepicker/datetimepicker.js')
		}
	},
	entry: {
		app: './index.js',
	},

	plugins: [
		// new BundleAnalyzerPlugin(),
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			favicon: 'app/favicon.ico',
			template: 'webpack_base_templates/index.html',
			// inject: 'body'
		}),

	],
	output: {
		hashDigest: 'hex',
		hashDigestLength: 8,
		hashFunction: 'sha256',
		filename: '[hash].[name].bundle.js',
		path: path.resolve(__dirname, 'dist')
	}
	,optimization: {
		minimize: true
		// ,minimizer: [
		// 	new TerserPlugin({
		// 		terserOptions: {
		// 			parallel: true,
		// 			module: false,
		// 			keep_fnames: true,
		// 			keep_classnames: true,
		// 		}
		// 	})
		// ]
		,splitChunks: {
			cacheGroups: {
				vendors: {
					chunks: 'all',
					test: /(bower_components)|(node_modules)/,
					reuseExistingChunk: true,
				},
			}
		}
	}
	,devServer: {
		compress: true,
		port: 9000
	}
	,module: {
		rules: [
			{
				test: /app\/scripts/,
				use: [
					{
						loader: 'angularjs-template-loader',
						options: {
							relativeTo: path.resolve(__dirname, 'app/'),
						}
					}
				]
			}
			,{
				test: /\.html$/i,
				use: {
					loader: 'html-loader',
					options: {
						root: path.resolve(__dirname, 'app/')
					}
				}
			}
			,{
				test: /\.css/i,
				use: [
					'style-loader',
					'css-loader',
					{
						loader: 'postcss-loader',
						options: {
							ident: 'postcss',
							plugins: [
								require('autoprefixer')({overrideBrowserslist: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1']}),
								require('cssnano')(),
							]
						}
					},
				]
			}
			,{
				test: /\.(ico|png|jpg|svg|gif|woff|woff2|eot|ttf|otf)/i,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[sha256:hash:hex:8].[name].[ext]',
						}
					}
				]
			}
		]
	}
};
