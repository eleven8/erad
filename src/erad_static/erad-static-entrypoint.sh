#!/bin/sh

set -e
set -x

MY_IP=$(awk 'END{print $1}' /etc/hosts)

cd /usr/local/src/erad_static
sed -i 's/{%ERAD_SERVER_NAME%}//g' app/scripts/services.js
npm install
bower install
set +e
npm run build
set -e
cd /usr/local/
sed -i "s/52.26.34.201/localhost/g" src/tools/dashboard/UI/config.json
sed -i "s/dev.enterpriseauth.com/localhost/g" src/tools/dashboard/UI/config.json
rm -rf /erad-static-volume/*

cp -r /usr/local/src/erad_static/dist/* /erad-static-volume/
cp -r /usr/local/src/tools/dashboard/UI/ /erad-static-volume/start
