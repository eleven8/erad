#!/bin/sh

set -e
set -x

MY_IP=$(awk 'END{print $1}' /etc/hosts)

cd /usr/local/src/erad_static
sed -i 's/{%ERAD_SERVER_NAME%}/https:\/\/testing.example.org/g' app/scripts/services.js
npm install verbose
bower install
npm run test -- -a no-sandbox
