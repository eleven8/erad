'use strict';

// Commands that execute `aws cli` used credentials from standard places like `~/.aws/credentials` or env variables

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  var serveStatic = require('serve-static');


  // Configurable paths
  var config = {
    base: './',
    app: 'app',
    dist: 'dist',
    test: 'test'
  };

  // Load the grunt-mocha
  grunt.loadNpmTasks('grunt-mocha');

  // designated to execute aws cli
  grunt.loadNpmTasks('grunt-exec');

  grunt.loadNpmTasks('grunt-postcss');

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: config,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: []
      },
      js: {
        files: ['<%= config.app %>/scripts/**/*.js'],
        tasks: ['jshint'],
        options: {
          livereload: true
        }
      },
      jstest: {
        files: ['test/spec/**/*.js'],
        tasks: ['test:watch']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      styles: {
        files: ['<%= config.app %>/styles/{,*/}*.css'],
        tasks: ['newer:copy:styles', 'postcss']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= config.app %>/**/*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= config.app %>/images/{,*/}*'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        open: true,
        livereload: 35729,
        // Change this to '0.0.0.0' to access the server from outside
        hostname: 'localhost'
      },
      livereload: {
        options: {
          middleware: function(connect) {
            return [
              serveStatic('.tmp'),
              connect().use('/bower_components', serveStatic('./bower_components')),
              serveStatic(config.app)

            ];
          }
        }
      },
      test: {
        options: {
          open: false,
          port: 9001,
          hostname: 'localhost',
          middleware: function(connect) {
            return [
              serveStatic('.tmp'),
              connect().use('/bower_components', serveStatic('./bower_components')),
              serveStatic(config.base)
            ];
          }
        }
      },
      dist: {
        options: {
          base: '<%= config.dist %>',
          livereload: false
        }
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js',
        '<%= config.app %>/scripts/{,*/}*.js',
        '!<%= config.app %>/scripts/vendor/*',
        'test/spec/{,*/}*.js'
      ]
    },

    // Mocha testing framework configuration options
    mocha: {
      all: {
        src: ['test/index.html'],
        options: {
          run: true,
          reporter: 'XUnit',
          // urls: ['http://<%= connect.test.options.hostname %>:<%= connect.test.options.port %>/test/index.html'],
          logErrors: true,
          log: true,
          growlOnFail: false,
          growlOnSuccess: false
        },
        dest: './test/output/xunit.xml'
      }
    },

    // Add vendor prefixed styles
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({overrideBrowserslist: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1']})
        ]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Automatically inject Bower components into the HTML file
    wiredep: {
      app: {
        ignorePath: /^\/|\.\.\//,
        src: ['<%= config.app %>/index.html'],
        exclude: ['bower_components/bootstrap/dist/css/bootstrap.min.css'],
      },
    },

    // Renames files for browser caching purposes
    rev: {
      dist: {
        files: {
          src: [
            '<%= config.dist %>/scripts/{,*/}*.js',
            '<%= config.dist %>/styles/{,*/}*.css',
            '<%= config.dist %>/images/{,*/}*.*',
            '<%= config.dist %>/styles/fonts/{,*/}*.*',
            '<%= config.dist %>/*.{ico,png}'
          ]
        }
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      options: {
        dest: '<%= config.dist %>'
      },
      html: '<%= config.app %>/index.html'
    },

    // Performs rewrites based on rev and the useminPrepare configuration
    usemin: {
      options: {
        assetsDirs: [
          '<%= config.dist %>',
          //'<%= config.dist %>/images',
          '<%= config.dist %>/styles'
        ]
      },
      html: ['<%= config.dist %>/{,*/}*.html'],
      css: ['<%= config.dist %>/styles/{,*/}*.css']
    },

    // The following *-min tasks produce minified files in the dist folder

    ngtemplates:  {
      eradApp:        {
        cwd: 'app',
        src:      ['templates/**/*.html', 'partials/**/*.html'],
        dest:     '<%= config.dist %>/scripts/main.js',
        options:  {
          htmlmin:  { collapseWhitespace: true, collapseBooleanAttributes: true },
          usemin: 'scripts/main.js' // <~~ This came from the <!-- build:js --> block
        }
      }
    },

    uglify: {
      options: {
        mangle: false
      },
       dist: {
         files: {
           '<%= config.dist %>/scripts/scripts.js': [
             '<%= config.dist %>/scripts/scripts.js'
           ]
         }
       }
     },
     concat: {
       dist: {}
     },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.dist %>',
          src: [
            '*.{ico,png,txt}',
            'images/{,*/}*.webp',
            'index.html',
            'styles/chosen-sprite.png',
            'styles/fonts/{,*/}*.*'
          ]
        }, {
          src: 'node_modules/apache-server-configs/dist/.htaccess',
          dest: '<%= config.dist %>/.htaccess'
         }, {
          src: '<%= config.app %>/styles/css.wua',
          dest: '<%= config.dist %>/wua.css'
        }, {
          expand: true,
          dot: true,
          cwd: '.',
          src: 'bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*',
          dest: '<%= config.dist %>'
        }, {
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          src: 'fonts/*',
          dest: '<%= config.dist %>'
        }]
      },
      styles: {
        expand: true,
        dot: true,
        cwd: '<%= config.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      }
    },

    // Run some tasks in parallel to speed up build process
    concurrent: {
      server: [
        'copy:styles'
      ],
      test: [
        'copy:styles'
      ],
      dist: [
        'copy:styles'
      ]
    },

    // Read the file
    aws: grunt.file.readJSON('aws.json'),

    exec: {
      cloudfront_invalidation: {
        cmd: function(cf_distribution_id){
          var invalidation_batch = {
            'CallerReference': Date.now().toString(),
            'Paths': {
              'Quantity': 1,
              'Items': [ '/index.html' ]
            }
          };

          var args = [
            'aws',
            'cloudfront',
            'create-invalidation',
            '--distribution-id', cf_distribution_id,
            '--invalidation-batch ', ['\'', JSON.stringify(invalidation_batch), '\''].join('')
          ];
          console.log(args.join(' '));
          return args.join(' ');
        }
      },
      copy_to_s3: {
        cmd: function (bucket_name) {
          var args = [
            'aws',
            's3',
            'cp',
            './dist/',
            ['s3://', bucket_name, '/'].join(''),
            '--recursive',
            '--acl public-read'
          ];
          console.log(args.join(' '))
          return args.join(' ');
        }
      }

    }
  });
  grunt.registerTask('serve', 'start the server and preview your app, --allow-remote for remote access', function (target) {
    if (grunt.option('allow-remote')) {
      grunt.config.set('connect.options.hostname', '0.0.0.0');
    }
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      //'wiredep',
      'concurrent:server',
      'postcss',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('server', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run([target ? ('serve:' + target) : 'serve']);
  });

  grunt.registerTask('test', function (target) {
    if (target !== 'watch') {
      grunt.task.run([
        'clean:server',
        'concurrent:test',
        'postcss'
      ]);
    }

    grunt.task.run([
      'mocha'
    ]);

  });

  grunt.registerTask('build', [
    'clean:dist',
    //'wiredep',
    'useminPrepare',
    'ngtemplates',
    'concurrent:dist',
    'postcss',
    'concat',
    'cssmin',
    'uglify',
    'copy:dist',
    'rev',
    'usemin'
  ]);

  grunt.registerTask('development', function(s3_bukcet_name){
    var s3_bukcet_name = s3_bukcet_name || grunt.config.get('aws').dev_bucket;

    grunt.task.run([
      'build',
      'exec:copy_to_s3:' + s3_bukcet_name
    ]);
  });

  grunt.registerTask('staging', function(s3_bukcet_name){
    var s3_bukcet_name = s3_bukcet_name || grunt.config.get('aws').staging_bucket;

    grunt.task.run([
      'build',
      'exec:copy_to_s3:' + s3_bukcet_name
    ]);
  });

  grunt.registerTask('production', function(s3_bukcet_name, cf_distribution_id){
    var s3_bukcet_name = s3_bukcet_name || grunt.config.get('aws').bucket;
    var cf_distribution_id = cf_distribution_id || grunt.config.get('aws').distributionID;

    grunt.task.run([
      'build',
      'exec:copy_to_s3:' + s3_bukcet_name,
      'exec:cloudfront_invalidation:' + cf_distribution_id
    ]);
  });

  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);
};
