'use strict';

// Main App Declaration

// create a module named services that gets injected into the eradApp
// this way, we can unit test the services without needing to do the eradApp.run
var services        = angular.module('services', ['ui.bootstrap', 'eradConfig']); // jshint ignore: line
var eradControllers = angular.module('eradControllers', ['ui.bootstrap', 'ngTable', 'eradConfig', 'ui.router', 'ui.bootstrap.datetimepicker', 'services', 'chart.js', 'rzSlider']); // jshint ignore: line
var directives      = angular.module('directives', []);
var eradApp         = angular.module('eradApp', ['services', 'eradConfig', 'eradControllers', 'ui.router', 'directives']);

/**
 * App Run
 *
 *  This does the one time setup tasks used to kickstart the application.
 *  A bypass flag( undefined by default ) is declared on startup that is
 *  set to true after the account is loaded.
 *
 *  Testing URL: http://localhost:9000/#session=y2xmdawdawd15
 *  You must open it in new tab.
 */

var bypass;

eradApp.run(initEradApp);
  initEradApp.$inject = ['AuthService', '$rootScope', '$urlRouter', '$state', '$log', '$modal', '$timeout', 'Utils', 'AppModel', 'SessionErrorService', '$window', '$templateCache'];

  function initEradApp(AuthService, $rootScope, $urlRouter, $state, $log, $modal, $timeout, Utils, AppModel, SessionErrorService, $window, $templateCache) {

    $templateCache.put('panel-header.html', require('../templates/panel-header.html'));

   if( $window.location.hash && $window.location.hash.indexOf('session=') !== -1){
        var token = $window.location.hash.split('=')[1];
        AppModel.accessToken = token;
        Utils.setCookie('EradToken', token, 30);
   }

  if (Utils.getCookie('EradToken')) {
    AppModel.accessToken = Utils.getCookie('EradToken');
  }

  // Interrupt state change to check if we have valid token/session
  $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {

    if (toState.name === 'login') {
      if (toParams && toParams.allow_insecure === 'true') {
        return;
      } else if (AppModel.isSecureUrl) {
        return;
      } else {
        location.href = 'https:' + location.href.substring(location.protocol.length);
      }
    }

    if (toState.name === 'admin') {
      if (AppModel.isAccountOwner !== 1) {
        event.preventDefault();
      }
    }

    if (bypass) return; // Do nothing if Account has already loaded up

    // Halt state change from even starting
    event.preventDefault();

    // Check cookie for token, if exists save in app model
    if ( AppModel.accessToken && AppModel.isLoggedIn ) {
      $log.info('Token Found AND logged in');

      bypass = true;
      return $state.go('users', {}, { reload: true });

    } else if ( AppModel.accessToken && !AppModel.isLoggedIn ) {
      $log.info('Token Found');

      bypass = true;

      AuthService.getAuthed().then(function () {
        if (AppModel.selectedGroup) {
          return $state.go('users', {}, { reload: true });
        } else {
          $log.info('No groups found... ');
        }
      });

    } else {

      // No token found, just redirect to default non-authenticated page

      AppModel.isLoggedIn = false;
      bypass = true;
      return $state.go('login');

    }

  });

}

eradApp.config(
  ['$stateProvider', '$urlRouterProvider',

  function ($stateProvider, $urlRouterProvider) {

  // For any unmatched url, redirect to /groups
  // set the default admin tab
//  $urlRouterProvider.when('/admin', '/admin/keys');
  $urlRouterProvider.otherwise('/groups');

  $stateProvider
  .state('groups', {
    url: '/groups',
    templateUrl: 'partials/groups.html',
    controller: 'GroupsCtrl as vm'
  })
  .state('login', {
    url: '/login?allow_insecure',
    templateUrl: 'partials/login.html',
    controller: 'LoginCtrl as vm'
  })
  .state('users', {
    url: '/users',
    templateUrl: 'partials/users.html',
    controller: 'UsersCtrl as vm'
  })
  .state('aaa_log', {
    url: '/aaa_log',
    templateUrl: 'partials/aaa_log.html',
    controller: 'AaaLogCtrl as vm',
  })
  .state('change_history', {
    url: '/change_history',
    templateUrl: 'partials/change_history.html',
    controller: 'ChangeHistoryCtrl as vm'
  })
  .state('admin', {
    url: '/admin',
    templateUrl: 'partials/admin/admin.html',
    controller: 'AdminCtrl as vm',
  })
  .state('start', {
    url: '/start',
    templateUrl: 'partials/start.html',
  });
}]);
