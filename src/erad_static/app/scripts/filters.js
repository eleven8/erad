/**
 * Filter for handling validation in correct order
 */

'use strict';

eradApp.filter('firstValidKey', function() {
   return function (object) {
      var keyArray = [];
      for (var i in object)
        object[i] && keyArray.push(i); // jshint ignore: line
      return keyArray[0];
   };
});
