/**
 * Login Controller
 *
 */

(function() {
    'use strict';

    angular.module('eradControllers').controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$state', '$log', 'SessionService', 'AppModel', 'AuthService' ];

    function LoginCtrl ($state, $log, SessionService, AppModel, AuthService) {

        var ctrl         = this;

        /////////////////////////////////////////////
        // variables exposed to the view
        /////////////////////////////////////////////

        ctrl.error       = null;
        ctrl.showError   = false;

        /////////////////////////////////////////////
        // functions exposed to the view
        /////////////////////////////////////////////

        ctrl.login       = login;
        ctrl.toggleError = toggleError;

        /////////////////////////////////////////////
        // functions
        /////////////////////////////////////////////

        function login (userCredentials) {
            ctrl.showError = false;
            SessionService
                .login(userCredentials, ctrl.loginForm)
                .then(checkLoginStatus)
                .catch(function (err) {
                    ctrl.error = err.Error || 'Error logging in';
                    ctrl.toggleError();
                });
        }

        function checkLoginStatus (data) {
            AuthService
                .getAuthed()
                .then(function setGroupAndRedirect () {
                    if (AppModel.selectedGroup) {
                      return $state.go('users', {}, { reload: true });
                    } else {
                      $log.info('No groups found... ');
                    }
                });
        }

        function toggleError () {
            ctrl.showError = ctrl.showError ? false : true;
        }

    }
})();
