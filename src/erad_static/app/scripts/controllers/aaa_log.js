/**
 * AAA Log Controller
 *
 **/

(function () {

  'use strict';

  angular.module('eradControllers').controller('AaaLogCtrl', AaaLogCtrl);

  AaaLogCtrl.$inject = ['$scope', '$state', '$filter', 'ngTableParams', 'AppModel', 'SessionErrorService', 'DateService', 'AaaLogService', '$window', '$rootScope'];

  function AaaLogCtrl ($scope, $state, $filter, ngTableParams, AppModel, SessionErrorService, DateService, AaaLogService, $window, $rootScope) {

    var ctrl = this;
    // data is loaded in async mode,
    AaaLogService.list({}).then(function(data) {
      getLogsCallback(data);
      updateChartData(data);
    });

    /////////////////////////////////////////////
    // variables exposed to the view
    /////////////////////////////////////////////

    ctrl.AppModel = AppModel;

    // toggles visibility of stray requests in table
    ctrl.includeStrayRequests = false;

    // toggles visibility of bubble logs in table
    ctrl.includeBubbleLogs = false;

    // set to true as we default to today's date inthe calendar
    ctrl.showRefreshButton = true;

    // fetches timezone adjusted date/time
    ctrl.TodaysDate = DateService.getConvertedTime();
    ctrl.selectedDay = '';

    ctrl.view = {
      'Title':'',
      'Message' : ''
    };

    //set tooltip text
    ctrl.timezoneTooltipText = setTooltipText();

    // toggle opened calendar var
    ctrl.calendarIsOpened = false;

    // datepicker options
    ctrl.dateOptions = {
      'showWeeks': false
    };

    // ctrl.data needs to be an array
    if (ctrl.AppModel.logsList === undefined) {
      ctrl.data = [];
      ctrl.AppModel.logsList = [];
    }

    /////////////////////////////////////////////
    // functions exposed to the view
    /////////////////////////////////////////////

    ctrl.goBack = goBack;

    // exports logs as csv
    ctrl.exportAaaLogs = exportAaaLogs;

    // toggles calendar popup visibility
    ctrl.open = openCalendar;

    // refresh table data button
    ctrl.refreshDataTable = refreshDataTable;

    // set initial date to today
    ctrl.setInitialDate = setInitialDate;

    // call initial date on page load
    ctrl.setInitialDate();

    // call when chart toggle
    ctrl.changeBtnClass = changeBtnClass;

    // controller data for aaa log chart
    ctrl.daterange = [];
    $scope.access_accept_reqs = [];
    $scope.access_reject_reqs = [];
    ctrl.labels = ['00:00','01:00','02:00','03:00','04:00','05:00','06:00','07:00','08:00','09:00','10:00','11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00'];
    ctrl.series = ['Access-Accept', 'Acces-Reject'];
    ctrl.chartdata = [
      $scope.access_accept_reqs,
      $scope.access_reject_reqs
    ];
    ctrl.chart_colors = [
      '#5cb85c', // green
      '#d9534f' // red
    ];
    ctrl.showchart = false;
    ctrl.btnClass = 'fa fa-minus-square';

    /////////////////////////////////////////////
    //  functions
    /////////////////////////////////////////////

    // function to change toggle button class
    function changeBtnClass (item) {
      var getClass = $(item.target).attr('class');
      if(getClass != 'btn btn-default btn-xs')
        ctrl.btnClass = 'fa fa-minus-square';
      else
        ctrl.btnClass = 'fa fa-plus-square';
    }

    function setTooltipText () {
      var tzZoneObject = $window.moment.tz.zone(ctrl.AppModel.timezone);
      var tzOffset = tzZoneObject.parse(new Date().getTime());
      // var offsetDirection = $window.Math.sign(tzOffset) === 1 ? '-' : '+';
      var offsetDirection = tzOffset > 0 ? '-' : '+';
      var offsetHours = $window.Math.abs(tzOffset/60);
      var result = tzZoneObject.name + ' (UTC ' + offsetDirection + offsetHours + ' Hours)';
      return result;
    }

    // wrapped to clear logs
    function goBack () {
      // calling the function with no args sets to an empty array
      ctrl.AppModel.addLocalTimeToLogs();
      $state.go('users');
    }

    // called when a new date is chosen
    function onDateChange (newVal, oldVal) {
      var chosenNewDay = DateService.calcDate(newVal);


      // hack not undestand why DateService.calcDate in tests reutrn sometimes undefined so hacking it
      ctrl.daterange = chosenNewDay || [];
      if (oldVal === newVal) {
        return;
      } else {
        var base_opts = {
          start: chosenNewDay[0] * 10,
          end: chosenNewDay[1] * 10,
        }

        var opts = JSON.parse(JSON.stringify(base_opts));

        opts.include_bubble = ctrl.includeBubbleLogs;
        opts.include_stray = ctrl.includeStrayRequests;


        ctrl.data = [];
        ctrl.AppModel.logsList = [];
        ctrl.tableParams.reload();

        getLogData(opts);
      }

      var day1 = moment(newVal).format('ddd MMM DD YYYY');
      var day2 = moment(ctrl.TodaysDate).format('ddd MMM DD YYYY');

      // check to see if we are navigating to today
      if (day1 === day2) {
        // used to toggle refresh button
        ctrl.showRefreshButton = true;
      } else {
        ctrl.showRefreshButton = false;
      }
    }

    function updateChartData(chartlist){
      $scope.access_accept_reqs = []
      $scope.access_reject_reqs = []

      getChartData(ctrl.daterange, chartlist)

      ctrl.chartdata = [
        $scope.access_accept_reqs,
        $scope.access_reject_reqs
      ];

      ctrl.showchart = true;
    }

    // Get Chart Data from Logs
    function getChartData(timestamps, logs){
      // Set Initial time gap to 0 and increase it by 1 hours
      var gap = 0;
      for(var i = 0; i<24; i++){

        // Assign 0 for initial values for each hour
        $scope.access_accept_reqs[i] = 0;
        $scope.access_reject_reqs[i] = 0;
        // Go through 2 hours of hap
        var start_date = moment.unix(timestamps[0]/1000).add(gap, 'hours').valueOf();
        gap = gap + 1;
        var end_date = moment.unix(timestamps[0]/1000).add(gap, 'hours').valueOf();
        for(var j=0; j<logs.length; j++){
          var inst = logs[j].Instance.toString().slice(0, -1);
          // Check for Request Count
          if(inst >= start_date && inst < end_date){
            if(logs[j].Access == 'Access-Accept')
              $scope.access_accept_reqs[i]++;
            else
              $scope.access_reject_reqs[i]++;
          }
        }
      }

    }

    // ngTable data fetch function, called on table refresh
    function getTableData ($defer, params) {

      // Actual Data after filter has been performed
      // ctrl.filter - input string from view that provide search query
      var filteredData = $filter('filter')(ctrl.data, ctrl.filter);

      //if filter exists, apply it to filteredData
      var orderedData = params.sorting() ?
        $filter('orderBy')(filteredData, params.orderBy()) :
        filteredData;

      params.total(orderedData.length); // set total for recalc pagination
      ctrl.itemCount = orderedData.length;

      if(params.total() < (params.page() -1) * params.count()){
        params.page(1);
      }

      $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

    }

    function exportAaaLogs (logDate, includeStrayRequests, includeBubbleLogs) {

      var selectedDay = DateService.calcDate(ctrl.someDate);

      var domain = ctrl.AppModel.apiDomain +
        '/erad/admin/supplicant/log/export/csv?Session_ID=' + ctrl.AppModel.accessToken +
        '&Group_ID=' + ctrl.AppModel.selectedGroup +
        '&Include_Stray=' + includeStrayRequests +
        '&Include_Bubble=' + includeBubbleLogs +
        '&Instance_From=' + (selectedDay[0]*10) +
        '&Instance_To=' + (selectedDay[1]*10);

      window.location.href = domain;
    }

    function openCalendar () {
      ctrl.calendarIsOpened = ctrl.calendarIsOpened ? false : true;
    }

    function getLogData(opts){
      AaaLogService.list(opts).then(function (data) {
        getLogsCallback(data);
        updateChartData(data);
      })
    }

    function fetchBubbleLogs(value, prev) {
      if (value !== prev){
        getLogData({
          include_bubble: value,
          include_stray: ctrl.includeStrayRequests,
          start: ctrl.daterange[0]*10,
          end: ctrl.daterange[1]*10
        })
      }
    }

    function fetchStrayLogs(value, prev) {
      ctrl.data = ctrl.AppModel.logsList
      ctrl.tableParams.reload()

      if (value !== prev) {
        getLogData({
          include_stray: value,
          include_bubble: ctrl.includeBubbleLogs,
          start: ctrl.daterange[0]*10,
          end: ctrl.daterange[1]*10
        })
      }
    }

    function getLogsCallback (data){
      if (data.Error) {
        // open modal
        SessionErrorService.log({ type: 'noAaaLogs' });
        ctrl.data = [];
        ctrl.AppModel.logsList = [];
        ctrl.tableParams.reload();
      } else {
        ctrl.AppModel.addLocalTimeToLogs(data);
        ctrl.data = ctrl.AppModel.logsList
        ctrl.tableParams.reload()
      }
    }

    // set initial calendar date
    function setInitialDate () {
      ctrl.someDate = DateService.getConvertedTime();
    }

    function refreshDataTable () {

      ctrl.selectedDay = DateService.calcDate(ctrl.someDate);

      var base_opts = {
        start: ctrl.selectedDay[0] * 10,
        end: ctrl.selectedDay[1] * 10,
      }

      var opts = JSON.parse(JSON.stringify(base_opts));
      opts.include_bubble = ctrl.includeBubbleLogs;
      opts.include_stray = ctrl.includeStrayRequests;

      ctrl.data = [];
      ctrl.AppModel.logsList = [];
      ctrl.tableParams.reload();

      getLogData(opts)
    }

    ctrl.tableParams = new ngTableParams({ // jshint ignore: line
      page: 1,            // show first page
      count: 10,          // count per page
      sorting: {
        EventTimestamp: 'desc'     // initial sorting
      }
    }, {
      total: ctrl.data ? ctrl.data.length : 0,
      getData: getTableData,
      $scope: $scope
    });

    /////////////////////////////////////////////
    // watches
    /////////////////////////////////////////////

    // just call the function on change to filter the table data
    $scope.$watch('vm.includeStrayRequests', fetchStrayLogs);

    // call function on change to fetch bubble data
    $scope.$watch('vm.includeBubbleLogs', fetchBubbleLogs)

    // Search filter event ( $ means everything )
    $scope.$watch('vm.filter.$', function () {
      ctrl.tableParams.reload();
    });

    // handles change of date in calendar picker
    $scope.$watch('vm.someDate', onDateChange, { objectEquality: true });

    // update group (ie, add, save or update)
    $rootScope.$on('updateGroup', function () {
      ctrl.timezoneTooltipText = setTooltipText();
    });



  }

})();
