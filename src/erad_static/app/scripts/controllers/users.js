/**
 * Users Controller
 *
 */

(function () {

	'use strict';

	angular.module('eradControllers').controller('UsersCtrl', UsersCtrl);

	UsersCtrl.$inject = [ '$scope', '$state', '$filter', '$modal', 'ngTableParams', 'AppModel', 'UserService', 'ERAD_CONFIG', 'SessionErrorService', '$window'];

	function UsersCtrl ($scope, $state, $filter, $modal, ngTableParams, AppModel, UserService, ERAD_CONFIG, SessionErrorService, $window) {

		var ctrl = this;
		// data is loaded in async mode,
		UserService.list().then(function() {
			ctrl.data = ctrl.AppModel.usersList;
		});

		/////////////////////////////////////////////
		// variables exposed to the view
		/////////////////////////////////////////////

		ctrl.AppModel = AppModel;

    	ctrl.data = ctrl.AppModel.usersList;

		ctrl.tableParams = new ngTableParams({ // jshint ignore: line
			page: 1,            // show first page
			count: 10,          // count per page
			sorting: {
				name: 'asc'     // initial sorting
			}
		}, {
			total: ctrl.data ? ctrl.data.length : 0,
			getData: getTableData,
			$scope: $scope
		});

		ctrl.formEditable = ctrl.AppModel.isAccountOwner ? true : false;

		ctrl.ACCESS_CHANGE_HISTORY_LOG = ERAD_CONFIG.ACCESS_CHANGE_HISTORY_LOG;

		if (typeof(ctrl.ACCESS_CHANGE_HISTORY_LOG.indexOf) !== 'undefined' && ctrl.ACCESS_CHANGE_HISTORY_LOG.indexOf(ctrl.AppModel.selectedGroup) !== -1) {
			ctrl.viewHistoryToggle = true;
		} else {
			ctrl.viewHistoryToggle = true; // switch back to false!!!
		}

		$scope.view = {
			'Title': '',
			'Message' : ''
		};

		// var to hold url to call to export users
		ctrl.exportUrl = '';

		/////////////////////////////////////////////
		// functions exposed to the view
		/////////////////////////////////////////////

		ctrl.createUser = createUser;
		ctrl.deleteUser = deleteUser;
		ctrl.updateUser = updateUser;
		ctrl.importUsers = importUsers;
		ctrl.exportUsers = exportUsers;

		/////////////////////////////////////////////
		//  functions
		/////////////////////////////////////////////

		function createUser () {

			$scope.ModalMode = 'create';

			var modalInstance = $modal.open({ // jshint ignore: line
				 backdrop: 'static',
				 windowClass: 'add-device-modal',
				 templateUrl: 'templates/userModals/addEditUser.html',
				 controller: 'UserWizardCtrl',
				 scope: $scope,
				 resolve: {
					user: function () {
						return undefined;
					},
					userlist: function () {
						return ctrl.data.map(function (supplicant) {
							return supplicant.Username;
						});
					}
				}
			});
		}

		function deleteUser (user) {

			$scope.ModalMode = 'delete';

			var modalInstance = $modal.open({ // jshint ignore: line
				 backdrop: 'static',
				 windowClass: 'delete-user-modal',
				 templateUrl: 'templates/userModals/deleteUser.html',
				 controller: 'DeleteUserCtrl',
				 scope: $scope,
				 resolve: {
					user: function () {
						return user;
					}
				}
			});
		}

		function updateUser (user) {

			$scope.ModalMode = 'update';

			UserService.load(user.Username).then(function updateUserOrSendError (obj){
				if ( !obj.Error ) {
					$scope.user = obj.Supplicant;
					$('.loading').hide();
					var modalInstance = $modal.open({ // jshint ignore: line
						 backdrop: 'static',
						 windowClass: 'update-site-modal',
						 templateUrl: 'templates/userModals/addEditUser.html',
						 controller: 'UserWizardCtrl',
						 scope: $scope,
						 resolve: {
							user: function () {
								return $scope.user;
							},
							userlist: function () {
								return ctrl.data.map(function (supplicant) {
									return supplicant.Username;
								});
							}
						}
					});
				} else {
					SessionErrorService.handleError(obj);
				}
			 });
		}

		function importUsers () {

			$scope.ModalMode = 'import';

			var modalInstance = $modal.open({ // jshint ignore: line
				 backdrop: 'static',
				 windowClass: 'import-users-modal',
				 templateUrl: 'templates/userModals/importUser.html',
				 controller: 'UserImportCtrl',
				 scope: $scope
			});
		}

		function exportUsers () {
			ctrl.exportUrl = ctrl.AppModel.apiDomain + '/erad/admin/supplicant/export/csv?Session_ID=' + ctrl.AppModel.accessToken + '&Group_ID=' + ctrl.AppModel.selectedGroup;
			$window.location.href = ctrl.exportUrl;
		}

		function getTableData ($defer, params) {

			// Actual Data after filter has been performed
			var filteredData = $filter('filter')(ctrl.data, ctrl.filter);

			// if filter exists, apply it to filteredData
			var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

			params.total(orderedData.length); // set total for recalc pagination
			ctrl.itemCount = orderedData.length;

			if(params.total() < (params.page() -1) * params.count()){
				params.page(1);
			}

			$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

		}

		/////////////////////////////////////////////
		// watches
		/////////////////////////////////////////////

		$scope.$watch('vm.AppModel.usersList', function (newVal, oldVal) {
			if (angular.equals(newVal, oldVal)) {
				return;
			} else {
				ctrl.data = newVal;
				ctrl.tableParams.reload();
			}
		});

		$scope.$watch('vm.AppModel.selectedGroup', function (newVal, oldVal) {
			if (oldVal !== newVal) {
				UserService.list();
			}
		});

		// Search filter event ( $ means everything )
		$scope.$watch('vm.filter.$', function reloadTable () {
			ctrl.tableParams.reload();
		});
	}

})();
