/**
 * Panel header Controller
 *
 */

(function () {

	'use strict';

	angular.module('eradControllers').controller('PanelHeaderCtrl', PanelHeaderCtrl);

	PanelHeaderCtrl.$inject = ['$state', '$scope', '$modal', 'AppModel', 'GroupService', 'SessionErrorService'];

	function PanelHeaderCtrl ($state, $scope, $modal, AppModel, GroupService, SessionErrorService) {

		var ctrl 			= this;

		/////////////////////////////////////////////
		// variables exposed to the view
		/////////////////////////////////////////////

		ctrl.AppModel 		= AppModel;

		/////////////////////////////////////////////
		// functions exposed to the view
		/////////////////////////////////////////////

		ctrl.addGroup 		= addGroup;
		ctrl.updateGroup 	= updateGroup;

		/////////////////////////////////////////////
		// functions
		/////////////////////////////////////////////

		function openEditGroupModalOrSendError (obj) {
			if ( !obj.Error ) {

				$('.loading').hide();

				var modalInstance = $modal.open({ // jshint ignore: line
					backdrop: 'static',
					windowClass: 'update-site-modal',
					templateUrl: 'templates/groupModals/editSite.html',
					controller: 'editGroupCtrl',
					resolve: {
						data: function () {
							return [];
						}
					}
				});

			} else {
				// only called when no group is found,
				// which shouldn't normally occur
				// unless network error, etc
				SessionErrorService.handleError(obj);
			}
		}

		function updateGroup () {
			GroupService.load().then(openEditGroupModalOrSendError);
		}

		function addGroup () {

			ctrl.modalInstance = $modal.open({ // jshint ignore: line
				 backdrop: 'static',
				 windowClass: 'add-site-modal',
				 templateUrl: 'templates/groupModals/addSite.html',
				 controller: 'addGroupCtrl'
			});

			// result of GroupService.save()
			ctrl.modalInstance.result.then(function (res) {
				if ( res.Error ) {
					SessionErrorService.handleError(res);
				} else {
					GroupService.list();
					$('.loading').hide();
				}
			});
		}

	}

})();
