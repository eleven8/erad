'use strict';

angular.module('eradControllers').controller(
	'ConfirmDeleteEndpointCtrl',
	['$scope', '$modalInstance', 'endpoint', function ($scope, $modalInstance, endpoint) {

	var endpoint_repr = [
		endpoint['IpAddress'],
		':',
		endpoint['Port'],
		', ',
		endpoint['Region']
	].join('');


	/////////////////////////////////////////////
	// variables exposed to the view
	/////////////////////////////////////////////

	$scope.view = {
		Title: 'Confirm endpoint delete action',
		'Endpoint': endpoint_repr
	};

	/////////////////////////////////////////////
	// functions exposed to the view
	/////////////////////////////////////////////

	$scope.confirm_delete_action = function() {
		$modalInstance.close(endpoint)
	};

	$scope.close = function () {
		$modalInstance.dismiss('canceled');
	};
}]);
