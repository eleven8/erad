'use strict';

/**
 * Edit Group Controller
 *
 *
 */
angular.module('eradControllers').controller('editGroupCtrl', editGroupCtrl);

editGroupCtrl.$inject = ['$scope', '$modal', '$modalInstance', '$filter', '$rootScope', '$timeout', 'AppModel', 'GroupService', 'SessionErrorService', 'ngTableParams', 'AuthenticatorService', 'data'];

function editGroupCtrl ($scope, $modal, $modalInstance, $filter, $rootScope, $timeout, AppModel, GroupService, SessionErrorService, ngTableParams, AuthenticatorService, data) {

   /////////////////////////////////////////////
   // variables exposed to the view
   /////////////////////////////////////////////

   var sliderOptions = {
      id: 'ul',
      floor: 0,
      ceil: 20000000,
      noSwitching: true,
      step: 50000,
      showSelectionBar: true,
      getSelectionBarColor: function(value) {
         if (value <= 1000000)
             return 'red';
         if (value <= 4999999)
             return 'orange';
         if (value <= 7999999)
             return 'yellow';
         return '#2AE02A';
      },
      translate: function (val) {
         if (val == 0)
            return 'Not set'
         if (val >= 1000000)
            return val/1000000 + ' Mbps'
         return val/1000 + ' Kbps';
      },
   };

   $scope.data = [];

   $scope.AppModel = AppModel;
   $scope.timezone = $scope.AppModel.timezone;
   // toggle editable fields
   $scope.formEditable = $scope.AppModel.isAccountOwner ? true : false;

   $scope.originalData = [];
   $scope.newVal = {};

   $scope.view = {
      'Title': 'Edit Site Configuration',
      'showInputs': false,
      'showOverrides': false
   };

   // local error on add/edit/delete device table
   $scope.xhrError = '';
   $scope.xhrEndpointError = '';

   // options for add/edit/delete device table
   $scope.authTypes = [
      { title: 'called-station-id', placeholder: 'FF:FF:FF:FF:FF:FF'    },
      { title: 'nas-identifier',    placeholder: 'Please enter a value' }
   ];

   // set initial view of dropdown
   $scope.authType = $scope.authTypes[0];

   // set initial load to global endpoint
   $scope.global_endpoint = true;

   //  regions for endpoints
   $scope.regions = [
      { title: "us-west", value: "us-west-2" },
      { title: "us-east", value: "us-east-2" }
   ];

   // method to extract only the region
   $scope.extract_region = extractRegionName;

   $scope.parseInt = parseInt;
   // disable port provision on Global site
   if($scope.AppModel.selectedGroup !== ('Global' || 'global')){
      $scope.formPortProvisionEditable = true
   }
   else{
      $scope.formPortProvisionEditable = false;
   }
   // set initial view of endpoint dropdown
   $scope.region = $scope.regions[0];
   // initialize an array to load endpoint data
   $scope.endpointsData = [];

   $scope.getEndpoints = getEndpoints;

   $scope.getEndpoints();

   // remove endpoint button
   $scope.delEndpoint = delEndpoint;
   $scope.confirmDelEndpoint = confirmDelEndpoint;

   // add endpoint button
   $scope.addEndpoint = addEndpoint;

   // fetch remote auth types from service
   $scope.items = GroupService.remoteAuthenticationOptions();

   // set initial group
   $scope.group = {
      'ID': $scope.AppModel.selectedGroup,
      'Name': $scope.AppModel.selectedGroupName
   };

   // New group object
   $scope.newGroup = {};

   // set cases when site name is editable
   $scope.siteNameEditable = $scope.formEditable && $scope.group.ID !== ('Global' || 'global');

   var remoteServer = $scope.AppModel.remoteServerObject();

   // set initial item to the one stored on server

   $scope.item = setRemoteAuthenticator($scope.AppModel.RemoteServer, $scope.AppModel.remoteServerUrl);
   $scope.originalData = [];

   // Get authenticators from server
   // move this to a resolve on $modal.open ?
   $scope.getAuthenticators = getAuthenticators;

   $scope.getAuthenticators();

   $scope.tableParams = new ngTableParams({ // jshint ignore:line
      page: 1,            // show first page
      count: 100000,          // count per page
      sorting: {
         key: 'desc'     // initial sorting
      }
   }, {
      // we have to set the counts to an empty
      // array and the count (above) to something higher
      // to stop the pagination appearing as there's
      // no option to switch it off. This also creates
      // an empty element we have to hide with CSS too
      // unfortunately.
      counts: [],
      total: $scope.data ? $scope.data.length : 0,
      getData: getTableData,
      $scope: $scope
   });

   // $scope.sliderDl;
   // $scope.sliderUl;

   // function setSlider() {

   $scope.sliderDl = {
      value: AppModel.maxDownload,
      options: sliderOptions
   };

   $scope.sliderUl = {
      value: AppModel.maxUpload,
      options: sliderOptions,
   };

   // we need to force a re-render of the sliders to get them to start in the correct position
   $timeout(setSlider);

   /////////////////////////////////////////////
   // functions exposed to the view
   /////////////////////////////////////////////

   // Close functionality
   $scope.close = closeModal;

   // 'save' button
   $scope.updateGroup = updateGroup;

   // restore row to original state, before save
   $scope.cancelEditAuthenticators = cancelEditAuthenticators;

   // close error box button handler
   $scope.dismissAlert = dismissAlert;

   // remove authenticator 'save' button
   $scope.delAuthenticators = delAuthenticators;

   // add new authenticator 'save' button
   $scope.updateAuthenticator = updateAuthenticator;

   // callback from dropdown directive
   $scope.authCallback = authCallback;

   $scope.regionCallback = regionCallback;

   // another callback from dropdown directive
   $scope.selectItemCallback = selectItemCallback;

   // resets row of authenticators table when editing
   $scope.clearRow = clearRow;

   // save button for adding new authenticator
   $scope.addAuthenticator = addAuthenticator;

   /////////////////////////////////////////////
   //  functions
   /////////////////////////////////////////////

   function setSlider () {
      $rootScope.$broadcast('rzSliderForceRender');
   }

   function extractRegionName(region){
      var ret = region.split("-");
      return ret[0]+"-"+ret[1];
   }

   function getAuthenticators () {

      GroupService.listAuthenticators($scope.AppModel.selectedGroup)
         .then(function displayAuthenticators (reply) {
            $scope.data = reply.Auth_List.map(function mapAuthenticators (item) {
               var key = Object.keys(item)[0];
               return {
                  key: key,
                  val: item[key]
               };
            });
            $scope.originalData = angular.copy($scope.data);
            $scope.tableParams.reload();
         });
   }

   function getEndpoints (callback_delete) {
      GroupService.listEndpoints($scope.AppModel.selectedGroup)
         .then(function displayAuthenticators (reply) {
            if(Array.isArray(reply.Endpoint))
               $scope.endpointsData = reply.Endpoint;
            else
               $scope.endpointsData = [reply.Endpoint];

            if(callback_delete){
               // enable global indicator
               if(reply.Global)
                  $scope.global_endpoint = true;
               else
                  $scope.global_endpoint = false;
               // show success message on reverting back to global
               if(reply.Global && callback_delete){
                  $scope.xhrEndpointError = {
                     msg: 'This site is reverted back to the global port.',
                     type: 'success'
                  };
               }
            }else{
               // enable global indicator
               if(reply.Global)
                  $scope.global_endpoint = true;
               else
                  $scope.global_endpoint = false;
            }

         });
   }


   function delEndpoint (item) {
      GroupService.deleteEndpoint(item).then(function (data) {
         if (!data.Error) {
            // Call to load endpoints again
            $scope.getEndpoints(true);
         } else {
            $scope.xhrEndpointError = {
               msg: 'Delete failed',
               type: 'danger'
            };
         }
      });
   }

   function confirmDelEndpoint(endpoint) {
      var modalInstance = $modal.open({ // jshint ignore: line
            backdrop: 'static',
            windowClass: 'confirm-delete-endpoint-modal',
            templateUrl: 'templates/groupModals/confirm.delete.endpoint.html',
            controller: 'ConfirmDeleteEndpointCtrl',
            scope: $scope,
            resolve: {
               'endpoint': function () { return endpoint; }
            }
      });

      modalInstance.result.then(function (endpoint) {
         $scope.delEndpoint(endpoint);
      }, function (reason) {});
   }


   function addEndpoint () {
      // check for region information
      var region_data = $scope.region;
      GroupService.provisionEndpoint(region_data).then(function (data) {
         if (!data.Error) {
            // Call to load endpoints again
            $scope.getEndpoints();
         } else {
            $scope.xhrEndpointError = {
               msg: data.Error,
               type: 'danger'
            };
         }
      });
   }

   function setRemoteAuthenticator (remote_server, remote_server_url) {

      $scope.view.OverrideVlan = $scope.AppModel.OverrideVlan;
      $scope.view.OverrideConnectionSpeed = $scope.AppModel.OverrideConnectionSpeed;

      if (remote_server === undefined && remote_server_url.trim() != '') {
         // $scope.view.RemoteServerIp = url;
         var url_parts = $scope.AppModel.remoteServerUrl.split(':');
         $scope.view.RemoteServerIp = url_parts[0];
         $scope.view.RemoteServerPort = url_parts[1];
         $scope.view.SharedSecret = $scope.AppModel.sharedSecret;
         $scope.view.showInputs = true;
         $scope.view.showOverrides = true;
         return $scope.items['Other']
      } else if (remote_server === undefined && remote_server_url.trim() === '') {
         $scope.view.showOverrides = false;
         $scope.view.OverrideVlan = false;
         $scope.view.OverrideConnectionSpeed = false;
         return $scope.items['None']
      } else if (remote_server != '') {
         $scope.view.showOverrides = true;
         return $scope.items[remote_server];
      }

   }

   function getTableData ($defer, params) {
      // Actual Data after filter has been performed
      var filteredData = $filter('filter')($scope.data, $scope.filter);

      //if filter exists, apply it to filteredData
      var orderedData = params.sorting() ?
         $filter('orderBy')(filteredData, params.orderBy()) :
         filteredData;

      params.total(orderedData.length); // set total for recalc pagination
      $scope.itemCount = orderedData.length || 0;

      if (params.total() < (params.page() -1) * params.count()){
         params.page(1);
      }
      $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
   }

   function closeModal () {
      $modalInstance.dismiss('close');
   }

   function showGroupListsCallback (obj) {
      if ( obj.Error ) {
         SessionErrorService.handleError(obj);
      } else {
         GroupService.list();
         $('.loading').hide();
      }
   }

   function updateGroup (groupForm) {
      if (groupForm && groupForm.$valid) {
         $scope.submitted = false;
         $scope.group.timezone = $scope.timezone;
         $scope.group.MaxDownloadSpeedBits = $scope.sliderDl.value;
         $scope.group.MaxUploadSpeedBits = $scope.sliderUl.value;
         $modalInstance.dismiss('close');
         GroupService.save($scope.group).then(showGroupListsCallback);
      } else {
         $scope.submitted = true;
         // This is a hack, setDirty doesn't seem to work
         $('.ng-pristine:visible').removeClass('ng-pristine').addClass('ng-dirty');
      }
   }

   function resetRow (data, row, indx) {
      if (data[indx].val !== row.val) {
         return data[indx];
      } else {
         return {};
      }
   }

   $scope.resetRow = resetRow;

   function cancelEditAuthenticators (row, rowForm, indx) {
      row.isEditing = false;
      rowForm.$setPristine();

      var originalRow = $scope.resetRow($scope.originalData, row, indx);

      angular.extend(row, originalRow);
   }

   function dismissAlert () {
      $scope.xhrError = '';
      $scope.xhrEndpointError = '';
   }

   function delAuthenticators (row) {
      AuthenticatorService.deleteItem(row).then(function (data) {
         if (!data.Error) {
            for (var i in $scope.data) {
               if ($scope.data[i].key === row.key && $scope.data[i].val === row.val) {
                  $scope.data.splice(i, 1);
               }
            }
            $scope.dismissAlert();
            $scope.tableParams.reload().then(function(data) {
               if (data.length === 0 && $scope.tableParams.total() > 0) {
                  $scope.tableParams.page($scope.tableParams.page() - 1);
                  $scope.tableParams.reload();
               }
            });
         } else {
            $scope.xhrError = {
               msg: 'Delete failed',
               type: 'danger'
            };
         }
      });
   }

   function updateAuthenticator (row, rowForm, indx) {
      var originalRow = $scope.resetRow($scope.originalData, row, indx);

      AuthenticatorService.save(row).then(function saveAuthenticatorCallback (data) {

         if (data.Error && data.Error != null) {
            if(data.Error === 'Invalid Mac'){
               $scope.xhrError = {
                  msg: 'Please enter a valid MAC address.',
                  type: 'danger'
               };
            } else if(data.Error === 'DUPLICATE_RECORD'){
               $scope.xhrError = {
                  msg: 'A device with that ID already exists at another site.',
                  type: 'danger'
               };
            }else{
               $scope.xhrError = {
                  msg: data.Error,
                  type: 'danger'
               };
            }
            angular.extend(row, originalRow);
         } else {
            $scope.dismissAlert();
            AuthenticatorService.deleteItem(originalRow).then(function delAuthenticatorsCallback () {
               for (var i in $scope.data) {
                  if ($scope.data[i].key === row.key && $scope.data[i].val === row.val) {
                     $scope.data.splice(i, 1);
                  }
               }
               //angular.copy($scope.data[indx], originalRow);
               $scope.tableParams.reload();
            });
         }
      });
   }

   function selectItemCallback (itemType) {
      $scope.item = $scope.items[itemType];

      $scope.view.showInputs = false;
      if ($scope.item['title'] === 'Other') {
               $scope.view.showInputs = true;
      }

      if ($scope.item['title'] === 'None'){
         $scope.view.OverrideVlan = false;
         $scope.view.OverrideConnectionSpeed = false;
         $scope.view.showOverrides = false;
      } else{
         $scope.view.showOverrides = true;
      }
   }

   function authCallback (authenticatorType) {
      for (var i in $scope.authTypes) {
         if ($scope.authTypes[i].title === authenticatorType) {
            $scope.authType = $scope.authTypes[i];
         }
      }
      $scope.newVal.key = authenticatorType;
   }

   function regionCallback (region) {
      for (var i in $scope.regions) {
         if ($scope.regions[i].title === region) {
            $scope.region = $scope.regions[i];
         }
      }
   }

   function clearRow (authenticatorForm) {
      $scope.newVal.val = '';
      authenticatorForm.$setPristine();
   }

   function addAuthenticator (authForm, values) {

      if (!values.key) {
         values.key = $scope.authType.title;
      }

      if (authForm.$invalid) {
         $scope.xhrError = {
            msg: 'Called-station-id must be valid MAC Address',
            type: 'danger'
         };
      } else {
         if ($scope.xhrError) {
            $scope.xhrError = '';
         }
         AuthenticatorService.save(values).then(function (data) {
            if (data.Error && data.Error != null) {
               if(data.Error === 'Invalid Mac'){
                  $scope.xhrError = {
                     msg: 'Please enter a valid MAC address.',
                     type: 'danger'
                  };
               } else if(data.Error === 'DUPLICATE_RECORD'){
                  $scope.xhrError = {
                     msg: 'A device with that ID already exists at another site.',
                     type: 'danger'
                  };
               }else{
                  $scope.xhrError = {
                     msg: data.Error,
                     type: 'danger'
                  };
               }
            } else {
               var newAuthenticator = {
                  key: data.Authenticator.RadiusAttribute,
                  val: data.Authenticator.ID
               };
               $scope.data.push(newAuthenticator);
               $scope.originalData = angular.copy($scope.data);
               $scope.newVal = {};
               authForm.$setPristine();
               $scope.tableParams.reload();
            }
         });
      }
   }

   /////////////////////////////////////////////
   // watches
   /////////////////////////////////////////////
   $scope.$watch('item', function (newVal) {
      angular.extend($scope.group, newVal);
   });

   $scope.$watch('view.SharedSecret', function (newVal, oldVal) {
      if (newVal !== oldVal) {
         $scope.group.SharedSecret = newVal;
         $scope.group.RemoteServerUrl = $scope.view.RemoteServerIp + ':' + $scope.view.RemoteServerPort;
      }
   });

   $scope.$watch('view.RemoteServerIp', function (newVal, oldVal) {
      if (newVal && newVal !== oldVal) {
         $scope.group.SharedSecret = $scope.view.SharedSecret;
         $scope.group.RemoteServerUrl = $scope.view.RemoteServerIp + ':' + $scope.view.RemoteServerPort;
      }
   });

   $scope.$watch('view.RemoteServerPort', function (newVal, oldV) {
      if (newVal && newVal !== oldV) {
         $scope.group.SharedSecret = $scope.view.SharedSecret;
         $scope.group.RemoteServerUrl = $scope.view.RemoteServerIp + ':' + $scope.view.RemoteServerPort;
      }
   });
   $scope.$watch('view.OverrideVlan', function (newVal, oldV) {
         $scope.group.OverrideVlan = $scope.view.OverrideVlan;
   });
   $scope.$watch('view.OverrideConnectionSpeed', function (newVal, oldV) {
         $scope.group.OverrideConnectionSpeed = $scope.view.OverrideConnectionSpeed;
   });
}
