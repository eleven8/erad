'use strict';

/**
 * Add Group Controller
 *
 *
 */
angular.module('eradControllers').controller('addGroupCtrl', addGroupCtrl);
addGroupCtrl.$inject = ['$scope', '$modalInstance', 'GroupService'];

function addGroupCtrl ($scope, $modalInstance, GroupService) {

   /////////////////////////////////////////////
   // $scope variables
   /////////////////////////////////////////////

   $scope.view = {
      'Title': 'Add New Site'
   };

   /////////////////////////////////////////////
   // functions exposed to the view
   /////////////////////////////////////////////

   $scope.close      = closeModal;
   $scope.addGroup   = addGroup;

   /////////////////////////////////////////////
   // functions
   /////////////////////////////////////////////

   function returnSaveResult (obj){
      // send the result back to the calling controller
      $modalInstance.close(obj);
   }

   function addGroup (groupAddForm) {
      if (groupAddForm.$valid) {
         $scope.submitted = false;
         GroupService.save($scope.newGroup).then(returnSaveResult);
      } else {
         $scope.submitted = true;
         // This is a hack, setDirty doesn't seem to work
         $('.ng-pristine:visible').removeClass('ng-pristine').addClass('ng-dirty');
      }
   }

   function closeModal () {
      $modalInstance.dismiss('close');
   }

}
