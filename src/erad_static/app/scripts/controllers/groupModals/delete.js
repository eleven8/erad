'use strict';

/**
 * Delete Group Modal Controller
 *
 *
 */

angular.module('eradControllers').controller('DeleteGroupModalCtrl', DeleteGroupModalCtrl);
DeleteGroupModalCtrl.$inject = ['$scope', '$modalInstance', 'groupToDelete'];

function DeleteGroupModalCtrl ($scope, $modalInstance, groupToDelete) {

	/////////////////////////////////////////////
	// $scope variables
	/////////////////////////////////////////////

	$scope.name = groupToDelete.data.Name;
	$scope.title = groupToDelete.title;

	/////////////////////////////////////////////
	// functions exposed to the view
	/////////////////////////////////////////////

	$scope.removeGroup   = removeGroup;
	$scope.close         = closeModal;

	/////////////////////////////////////////////
	// functions
	/////////////////////////////////////////////

	function removeGroup () {
		$modalInstance.close('Ok');
	}

	function closeModal () {
		$modalInstance.close('close');
	}

}
