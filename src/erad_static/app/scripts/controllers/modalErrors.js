/**
 * Error modal Controller
 *
 */

(function () {

	'use strict';

	angular.module('eradControllers').controller('ModalCtrl', ModalCtrl);

	ModalCtrl.$inject = ['message', '$scope'];

	function ModalCtrl (message, $scope) {

	    $scope.type = message.type;

	    $scope.message = {
	    	body: message.body,
	    	title: message.title
	    };

	}

})();
