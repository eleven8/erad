/**
 * Header Controller
 *
 */

(function () {

	'use strict';

	angular.module('eradControllers').controller('HeaderCtrl', HeaderCtrl);

	HeaderCtrl.$inject = ['$state', '$window', 'AppModel'];

	function HeaderCtrl ($state, $window, AppModel) {

		var ctrl 			= this;

		/////////////////////////////////////////////
		// variables exposed to the view
		/////////////////////////////////////////////

	    ctrl.AppModel 		= AppModel;
	    ctrl.loginPage 		= false;

	    if ($state.is('login')) {
	    	ctrl.loginPage 	= true;
	    }

		/////////////////////////////////////////////
		// functions exposed to the view
		/////////////////////////////////////////////

	    ctrl.launchHelp 	= launchHelp;
	    ctrl.logout 		= logout;

		/////////////////////////////////////////////
		// functions
		/////////////////////////////////////////////

	    function logout () {
			return ctrl.AppModel.logout();
	    }

	    function launchHelp () {
			$window.open('http://help.enterpriseauth.com', 'HelpWindow', 'width=900, height=800');
	    }
	}

})();
