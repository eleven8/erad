/**
 * Groups Controller
 *
 */

(function () {

	'use strict';

	angular.module('eradControllers').controller('GroupsCtrl', GroupsCtrl);

	GroupsCtrl.$inject = ['$scope', '$modal', '$state', 'AppModel', 'GroupService', 'SessionErrorService'];

	function GroupsCtrl ($scope, $modal, $state, AppModel, GroupService, SessionErrorService) {

		var ctrl 		  = this;
		// data is loaded in async mode,
		GroupService.list().then(function(){
			ctrl.groups       = ctrl.AppModel.groupList;
		});

	    /////////////////////////////////////////////
	    // variables exposed to the view
	    /////////////////////////////////////////////

		ctrl.AppModel 	  = AppModel;
		ctrl.fullyLoaded  = true;
		ctrl.groups       = ctrl.AppModel.groupList;
		ctrl.formEditable = ctrl.AppModel.isAccountOwner ? true : false;

	    /////////////////////////////////////////////
	    // functions exposed to the view
	    /////////////////////////////////////////////

		ctrl.selectSite   = selectSite;
		ctrl.removeSite   = removeSite;

	    /////////////////////////////////////////////
	    // functions
	    /////////////////////////////////////////////

		function selectSite (site) {
			AppModel.selectGroup(site);
			GroupService.load_unique_devices();
			$state.go('users');
		}

		function removeSite (site, indx) {

			ctrl.toDelete = { title: 'Delete Site', data: site, indx: indx };

			ctrl.modalInstance = $modal.open({
				backdrop: 'static',
				windowClass: 'delete-group-modal',
				templateUrl: 'templates/groupModals/deleteSite.html',
				controller: 'DeleteGroupModalCtrl',
				resolve: {
					groupToDelete: function() {
						return ctrl.toDelete;
					}
				}
			});

			ctrl.modalInstance.result.then(function (modalReply) {
				if (modalReply === 'Ok') {
					GroupService.remove(ctrl.toDelete.data).then(function removeGroup (obj) {
						if ( obj.Error ) {
							SessionErrorService.handleError(modalReply);
						} else {
							ctrl.groups.splice(ctrl.toDelete.indx, 1);
							$('.loading').hide();
						}
					});
				}
			});

		}

	}
})();
