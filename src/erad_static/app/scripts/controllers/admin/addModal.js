/**
 * Admin.add modal Controller
 *
 *
 */

(function () {

   'use strict';

   angular.module('eradControllers').controller('AdminAddModalCtrl', AdminAddModalCtrl);

   AdminAddModalCtrl.$inject = ['$scope', '$modalInstance', 'SessionErrorService', 'modalData'];

   function AdminAddModalCtrl ($scope, $modalInstance, SessionErrorService, modalData) {

      var MODAL_ACTION = 'ADD';
      var MODAL_TYPE = modalData.type;

      var MODAL_TITLE = MODAL_TYPE === 'keys'
         ? 'Add API Key'
         : 'Add Account';

      var defaults = {
         title: MODAL_TITLE,
         action: MODAL_ACTION,
         type: MODAL_TYPE,
         passwordError: null,
         message: 'Please supply some details' // not currently used
      };

      // set the default status of keys to inactive
      if (MODAL_TYPE === 'keys') {
         defaults.active = 1;
      }

      var defaultReply = {
         type: MODAL_TYPE,
         action: MODAL_ACTION
      };

      /////////////////////////////////////////////
      // variables exposed to the view
      /////////////////////////////////////////////

      $scope.submitted = false;

      // merge in to view object to pass to the modal template
      $scope.view = angular.copy(defaults);

      /////////////////////////////////////////////
      // functions exposed to the view
      /////////////////////////////////////////////

      $scope.cancelAdd   = cancelAdd;
      $scope.submitForm  = submitForm;

      /////////////////////////////////////////////
      // functions
      /////////////////////////////////////////////

      function cancelAdd () {
         $modalInstance.dismiss('close');
      }

      function submitForm (addForm) {

         if (addForm.$valid) {
            $scope.submitted = false;

            var formData = {};

            if (MODAL_TYPE === 'account') {

               formData.data = {
                  Email: addForm.newAccountEmail.$viewValue,
                  Password: addForm.newAccountPassword.$viewValue // || null ?
               };

            } else if (MODAL_TYPE === 'keys') {

               formData.data = {
                  DisplayName: addForm.newAPIKeyName.$viewValue,
                  Active: addForm.newAPIKeyStatus.$viewValue === true ? 1 : 0
               };

            } else {
               // call local error on form?
            }

            var reply = angular.extend(defaultReply, formData);

            $modalInstance.close(reply);

         } else {
            $scope.submitted = true;
         }
      }
   }

})();
