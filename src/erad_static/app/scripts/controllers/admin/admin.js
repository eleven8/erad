/**
 * Admin Controller
 *
 */

(function () {

	'use strict';

	angular.module('eradControllers').controller('AdminCtrl', AdminCtrl);

	AdminCtrl.$inject = ['$state', '$window', '$modal', 'AdminService', 'AdminAPIKeyService', 'SessionErrorService'];

	function AdminCtrl ($state, $window, $modal, AdminService, AdminAPIKeyService, SessionErrorService) {

		var ctrl 				= this;

		/////////////////////////////////////////////
		// variables exposed to the view
		/////////////////////////////////////////////

		/////////////////////////////////////////////
		// functions exposed to the view
		/////////////////////////////////////////////

		ctrl.fetchAccounts 		= fetchAccounts;
		ctrl.fetchAPIKeys 		= fetchAPIKeys;

	    ctrl.openAddModal		= openAddModal;
	    ctrl.openEditModal 		= openEditModal;
	    ctrl.openRemoveModal 	= openRemoveModal;

		ctrl.callSaveApiKeysEndpoint = callSaveApiKeysEndpoint;
		ctrl.callSaveAccountEndpoint = callSaveAccountEndpoint;

		ctrl.callDeleteEndpoint = callDeleteEndpoint;

		/////////////////////////////////////////////
		// fetch data on page load
		/////////////////////////////////////////////

		ctrl.fetchAccounts();
		ctrl.fetchAPIKeys();

		/////////////////////////////////////////////
		// functions
		/////////////////////////////////////////////

		function fetchAPIKeys () {
			AdminAPIKeyService.list().then(function (res) {
				ctrl.keys = res.ApiKeys;
			});
		}

		function fetchAccounts () {
			AdminService.list().then(function (res) {
				ctrl.accounts = res.Accounts;
			});
		}

		// click handler
		function openAddModal (modalType) {

			ctrl.modalInstance = $modal.open({
				backdrop: 'static',
				templateUrl: 'templates/adminModals/admin.add.html',
				controller: 'AdminAddModalCtrl',
				resolve: {
					modalData: function() {
						return {
							type: modalType
						};
					}
				}
			});

			ctrl.modalInstance.result.then(function (modalReply) {
				if (modalReply.type === 'account') {
					ctrl.callSaveAccountEndpoint(modalReply);
				} else if (modalReply.type === 'keys') {
					ctrl.callSaveApiKeysEndpoint(modalReply);
				}
			});
		}

		// click handler
		function openEditModal (itemToEdit, action) {

			ctrl.modalInstance = $modal.open({
				backdrop: 'static',
				templateUrl: 'templates/adminModals/admin.edit.html',
				controller: 'AdminEditModalCtrl',
				resolve: {
					modalData: function() {
						return {
							value: itemToEdit,
							type: action
						};
					}
				}
			});

			ctrl.modalInstance.result.then(function (modalReply) {
				if (modalReply.type === 'account') {
					ctrl.callSaveAccountEndpoint(modalReply);
				} else if (modalReply.type === 'keys') {
					ctrl.callSaveApiKeysEndpoint(modalReply);
				}
			});
		}

		// click handler
		function openRemoveModal (itemToRemove, endpoint) {

			ctrl.modalInstance = $modal.open({
				backdrop: 'static',
				templateUrl: 'templates/adminModals/admin.delete.html',
				controller: 'AdminDeleteModalCtrl',
				resolve: {
					modalData: function() {
						return {
							value: itemToRemove,
							type: endpoint
						};
					}
				}
			});

			ctrl.modalInstance.result.then(function (modalReply) {
				ctrl.callDeleteEndpoint({ type: modalReply.type, value: modalReply.data });
			});
		}

		// XHR wrapper
		function callDeleteEndpoint (keyToDelete) {

			if (keyToDelete.type === 'keys') {
				AdminAPIKeyService.deleteKey(keyToDelete.value).then(function (res) {
					if (res.Error) {
						// hande error
						SessionErrorService.handleError(res);
					} else {
						// remove from view
						ctrl.keys = ctrl.keys.filter(function (key) {
							return keyToDelete.value !== key.ApiKeyName;
						});
					}
				});
			} else if (keyToDelete.type === 'account') {
				AdminService.deleteAccount(keyToDelete.value).then(function (res) {
					if (res.Error) {
						// hande error
						SessionErrorService.handleError(res);
					} else {
						// remove from view
						ctrl.accounts = ctrl.accounts.filter(function (account) {
							return keyToDelete.value !== account;
						});
					}
				});

			}
		}

		// XHR wrapper
		function callSaveApiKeysEndpoint (modalReply) {
			AdminAPIKeyService.save(modalReply.data).then(function (res) {
				if (res.Error) {
					// handle error
					SessionErrorService.handleError(res);
				} else {
					switch (modalReply.action) {
						case 'ADD':
							ctrl.keys = ctrl.keys.concat({
								"ApiKeyName": res.ApiKey.ApiKeyName,
								"DisplayName": res.ApiKey.DisplayName
							});
							break;
						case 'EDIT':
							if (!ctrl || !ctrl.keys || !res || !res.ApiKey) {
								break;
							}
				var keysLength = ctrl.keys.length;
				for (var index = 0; index < keysLength; index++) {
				if (ctrl.keys[index].ApiKeyName == res.ApiKey.ApiKeyName) {
					ctrl.keys[index].DisplayName = res.ApiKey.DisplayName;
								break;
					}
				}
							break;
						default:
							SessionErrorService.handleError({ Error: 'Action not recognised' });
							break;
					}
				}
			});
		}

		// XHR wrapper
		function callSaveAccountEndpoint (newAccountData) {

			AdminService.save(newAccountData.data).then(function (res) {
				if (res.Error) {
					// handle error
					SessionErrorService.handleError(res);
				} else {
					switch (newAccountData.action) {
						case 'EDIT':
							// email value is not editable, password hidden nothing to update
							break;

						case 'ADD':
							ctrl.accounts = ctrl.accounts.concat(newAccountData.data.Email);
							break;
					}
				}
			});
		}

	}

})();
