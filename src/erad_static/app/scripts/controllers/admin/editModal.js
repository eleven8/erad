/**
 * User Wizard Controller
 *
 *
 */

(function () {

   'use strict';

   angular.module('eradControllers').controller('AdminEditModalCtrl', AdminEditModalCtrl);

   AdminEditModalCtrl.$inject = ['$scope', '$modalInstance', 'AppModel', 'Utils', 'UserService', '$filter', 'SessionErrorService', 'DateService', 'AdminService', 'AdminAPIKeyService', 'modalData'];

   function AdminEditModalCtrl ($scope, $modalInstance, AppModel, Utils, UserService, $filter, SessionErrorService, DateService, AdminService, AdminAPIKeyService, modalData) {

      var MODAL_ACTION = 'EDIT';
      var MODAL_TYPE = modalData.type;
      var MODAL_TITLE = MODAL_TYPE === 'keys'
         ? 'Edit API Key'
         : 'Edit Account';

      var defaultReply = {
         type: MODAL_TYPE,
         action: MODAL_ACTION
      };

      var defaults = {
         title: MODAL_TITLE,
         action: MODAL_ACTION,
         type: MODAL_TYPE,
         passwordError: null,
         message: 'Editing'
      };

      // set the default status to hidden
      if (MODAL_TYPE === 'keys') {
         defaults.showApiKey = false;
      }

      /////////////////////////////////////////////
      // variables exposed to the view
      /////////////////////////////////////////////

      $scope.submitted = false;

      if (MODAL_TYPE === 'keys') {
         AdminAPIKeyService.load(modalData.value).then(function (res) {
            if (res.Error) {
               // handle error
               SessionErrorService.handleError(res.Error);
            }

            var reply = {
               data: res.ApiKey
            };

            // merge in to view object to pass to the modal template
            $scope.view = angular.extend(defaults, reply);

         }).catch(closeModal);

      } else if (MODAL_TYPE === 'account') {

         AdminService.load(modalData.value).then(function (res) {

            if (res.Error) {
               // handle error
               SessionErrorService.handleError(res.Error);
            }

            var reply = {
               form: res.AccountOwner
            };

            // merge in to view object to pass to the modal template
            $scope.view = angular.extend(defaults, reply);

         }).catch(closeModal);
      }

      /////////////////////////////////////////////
      // functions exposed to the view
      /////////////////////////////////////////////

      $scope.close         = closeModal;
      $scope.toggleAPIKey  = toggleAPIKey;
      $scope.submitForm    = submitForm;

      /////////////////////////////////////////////
      // functions
      /////////////////////////////////////////////

      function toggleAPIKey () {
         $scope.view.showApiKey = true;
      }

      function closeModal () {
         $modalInstance.dismiss('close');
      }

      function submitForm (newForm) {
         if (newForm.$valid) {

            $scope.submitted = false;

            var formData = {};

            if (MODAL_TYPE === 'account') {

               formData.data = {
                  Email: newForm.accountEmail.$viewValue,
                  Password: newForm.accountPassword.$viewValue
               };

            } else if (MODAL_TYPE === 'keys') {

               var activate;

               if (newForm.apiKeyActive.$viewValue === true) {
                  activate = 1;
               } else if (newForm.apiKeyActive.$viewValue === false) {
                  activate = 0;
               } else {
                  activate = undefined;
               }

               formData.data = {
                  DisplayName: newForm.apiKeyName.$viewValue,
                  ApiKeyName: newForm.apiKey.$viewValue,
                  Active: activate
               };

            }

            var reply = angular.extend(defaultReply, formData);

            $modalInstance.close(reply);

         } else {
            $scope.submitted = true;
         }
      }
   }

})();
