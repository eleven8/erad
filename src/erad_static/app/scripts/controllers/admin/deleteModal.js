/**
 * User Wizard Controller
 *
 *
 */

(function () {

   'use strict';

   angular.module('eradControllers').controller('AdminDeleteModalCtrl', AdminDeleteModalCtrl);

   AdminDeleteModalCtrl.$inject = ['$scope', '$modalInstance', 'SessionErrorService', 'AdminService', 'AdminAPIKeyService', 'modalData'];

   function AdminDeleteModalCtrl ($scope, $modalInstance, SessionErrorService, AdminService, AdminAPIKeyService, modalData) {

      var MODAL_ACTION = 'DELETE';
      var MODAL_TYPE = modalData.type;
      var MODAL_TITLE = MODAL_TYPE === 'keys'
         ? 'Delete API Key'
         : 'Delete Account';

      var defaults = {
         title: MODAL_TITLE,
         action: MODAL_ACTION,
         type: MODAL_TYPE,
         message: 'Warning. This action is not recoverable.'
      };

      /////////////////////////////////////////////
      // variables exposed to the view
      /////////////////////////////////////////////


      /////////////////////////////////////////////
      // On page load
      /////////////////////////////////////////////

      if (MODAL_TYPE === 'keys') {
         AdminAPIKeyService.load(modalData.value).then(function (res) {

            if (res.Error) {
               // handle error
               return SessionErrorService.handleError(res.Error);
            }

            var reply = {
               data: res.ApiKey
            };

            // merge in to object to pass to the modal template
            $scope.view = angular.extend(defaults, reply);

            $scope.view.data.isActive = $scope.view.data.Active ? 'Yes' : 'No';

         }).catch(cancelDelete);

      } else if (MODAL_TYPE === 'account') {
         AdminService.load(modalData.value).then(function (res) {

            if (res.Error) {
               // handle error
               return SessionErrorService.handleError(res.Error);
            }

            var reply = {
               data: res.AccountOwner
            };

            // merge in to object to pass to the modal template
            $scope.view = angular.extend(defaults, reply);

         }).catch(cancelDelete);

      }

      /////////////////////////////////////////////
      // functions exposed to the view
      /////////////////////////////////////////////

      $scope.cancel      = cancelDelete;
      $scope.remove      = remove;

      /////////////////////////////////////////////
      // functions
      /////////////////////////////////////////////

      function remove (type, dataToRemove) {
         var reply = {
            type: type
         };

         if (type === 'keys') {
            reply.data = dataToRemove.ApiKeyName;
         } else if (type === 'account') {
            reply.data = dataToRemove.Email;
         }

         // pass back to calling controller the object to delete
         $modalInstance.close(reply);
      }

      function cancelDelete () {
         // nothing passed back to calling controller
         $modalInstance.dismiss('close');
      }

   }

})();
