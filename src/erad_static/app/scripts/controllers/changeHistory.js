/**
 * Change History Controller
 *
 */

(function () {

	'use strict';

	angular.module('eradControllers').controller('ChangeHistoryCtrl', ChangeHistoryCtrl);

	ChangeHistoryCtrl.$inject = ['$scope', '$state', '$filter', 'ngTableParams', 'AppModel', 'SessionErrorService', 'DateService', 'ChangeHistoryService', '$window'];

	function ChangeHistoryCtrl ($scope, $state, $filter, ngTableParams, AppModel, SessionErrorService, DateService, ChangeHistoryService, $window) {

		var ctrl = this;
		// data is loaded in async mode,
		ChangeHistoryService.list().then(function() {
			ctrl.logdata = ctrl.AppModel.changeHistory;
		});

	    /////////////////////////////////////////////
	    // variables exposed to the view
	    /////////////////////////////////////////////

		ctrl.AppModel = AppModel;
		ctrl.logdata = ctrl.AppModel.changeHistory;

		// needs data to be an array
		if (ctrl.logdata === undefined) {
			ctrl.logdata = [];
		}
		// datepicker options
		ctrl.dateOptions = {
			'show-weeks': false
		};

		// default to start with both datepickers closed
		ctrl.opened = {
			start: false,
			end: false
		};

		// exactly the same as on aaa_log, except the scope that's
		// passed in, eg, $scope.logdata
		ctrl.tableParams = new ngTableParams({ // jshint ignore:line
			page: 1,            // show first page
			count: 10,          // count per page
			sorting: {
				Date: 'desc'     // initial sorting
			}
		}, {
			total: ctrl.logdata ? ctrl.logdata.length : 0,
			getData: getTableData,
			$scope: $scope
		});

	    /////////////////////////////////////////////
	    // functions exposed to the view
	    /////////////////////////////////////////////

		// open datepicker
		ctrl.open = openCalendar;

		ctrl.refreshDataTable = refreshDataTable;

		ctrl.setInitialDate = setInitialDate;

		ctrl.setInitialDate();

	    /////////////////////////////////////////////
	    // functions
	    /////////////////////////////////////////////

		function openCalendar (calendar) {
			if (ctrl.opened[calendar] === false) {
				ctrl.opened[calendar] = true;
			} else {
				ctrl.opened[calendar] = false;
			}
		}

		// set date's to start first date and end of second date
		// via a call to DateService.calcDate(), then remove a month
		// for the default timespan
		// refresh data button
		function setInitialDate () {
			var d = $window.moment();
		  	var dayRange = DateService.calcDate(d);

			ctrl.date = {
				startDate: $window.moment(dayRange[0]).subtract(1, 'month').toISOString(),
				endDate: $window.moment(dayRange[1]).toISOString()
			};
		}

		function refreshDataTable () {
			ChangeHistoryService.list(ctrl.date).then(getLogsCallback);
		}

		function getLogsCallback (data){
			if (data.Error) {
				// open modal on error
				SessionErrorService.log({ type: 'changeHistoryError' });
				ctrl.logdata = [];
			} else {
				ctrl.logdata = data.Audit_List;
			}
			// always reload table
			ctrl.tableParams.reload();
		}

		function getTableData ($defer, params) {

			// Actual Data after filter has been performed
			var filteredData = $filter('filter')(ctrl.logdata, ctrl.filter);

			//if filter exists, apply it to filteredData
			var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

			params.total(orderedData.length); // set total for recalc pagination
			ctrl.itemCount = orderedData.length;

			if(params.total() < (params.page() -1) * params.count()){
				params.page(1);
			}

			$defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
		}

	    /////////////////////////////////////////////
	    // watches
	    /////////////////////////////////////////////

		// Search box filter ( $ means everything )
		$scope.$watch('vm.filter.$', function () {
			ctrl.tableParams.reload();
		});
	}

})();
