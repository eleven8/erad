/**
 * User Wizard Controller
 *
 *
 */

(function () {

   'use strict';

   angular.module('eradControllers').controller('UserWizardCtrl', UserWizardCtrl);
   UserWizardCtrl.$inject = ['$scope', '$modalInstance', '$rootScope', '$timeout', 'AppModel', 'Utils', 'UserService', 'GroupService', 'SessionErrorService', 'DateService', 'user', 'userlist'];

   function UserWizardCtrl ($scope, $modalInstance, $rootScope, $timeout, AppModel, Utils, UserService, GroupService, SessionErrorService, DateService, user, userlist) {

      var DATEPICKER_OUTPUT_FORMAT = 'ddd MMM DD YYYY HH:mm:ss Z';

      var emptyUser = {
         'Username': '',
         'Vlan': '',
         'UseRemote': false,
         'Password': null,
         'MacAddress': '',
         'ExpirationDate' : '',
         'Location' : '',
         'Description' : '',
         'DeviceName' : '',
         'Mac': [],
         'MaxUploadSpeedBits': 0,
         'MaxDownloadSpeedBits': 0
      };

      var sliderOptions = {
         id: 'ul',
         floor: 0,
         ceil: 20000000,
         noSwitching: true,
         step: 50000,
         showSelectionBar: true,
         getSelectionBarColor: function(value) {
            if (value <= 1000000)
                return 'red';
            if (value <= 4999999)
                return 'orange';
            if (value <= 7999999)
                return 'yellow';
            return '#2AE02A';
         },
         translate: function (val) {
            if (val == 0)
               return 'Not set'
            if (val >= 1000000)
               return val/1000000 + ' Mbps'
            return val/1000 + ' Kbps';
         },
      };



      /////////////////////////////////////////////
      // variables exposed to the view
      /////////////////////////////////////////////

      $scope.AppModel            = AppModel;
      $scope.userAuth            = true;
      $scope.oldUsername;
      $scope.oldMacAddress;
      $scope.userlist            = userlist;
      $scope.showOptionalFields  = false;
      $scope.MaxDate             = moment().add(3, 'years').endOf('day');
      $scope.TodaysDate          = moment().startOf('day');

      $scope.submitted           = false;

      // we set them depending on the mode of the modal (ie, $scope.'ModalMode')
      $scope.view = {
         Title: null,
         passwordError: null,
         Description: null
      };

      // defaults to off
      $scope.Expiration = {
         'Enabled' : false,
         'Date' : null,
         'TzShortname' : null
      };

      // picker widget options
      $scope.dateOptions = {
         startingDay: 1,
         showWeeks: false
      };

      // initial error value for empty string
      $scope.validationError = '';

      // we need to force a re-render of the sliders to get them to start in the correct position
      $timeout(setSlider);

      // if we have a user passed in by the $modal resolve in users.js
      if (user) {
         $scope.user = user;
         // replace oldUsername and other ref to passed in user to
         // the copied user
         $scope.originalUser = angular.copy(user);
         $scope.oldUsername = angular.copy(user.Username);

      } else {
         // copy the empty user template
         $scope.user = angular.copy(emptyUser);
      }

      $scope.sliderDl = {
         value: $scope.user.MaxDownloadSpeedBits || 0,
         options: sliderOptions
      };

      $scope.sliderUl = {
         value: $scope.user.MaxUploadSpeedBits || 0,
         options: sliderOptions,
      };

      // Set view properties
      if( $scope.ModalMode === 'create' ) {

         $scope.view.Title = 'Add New Device';
         $scope.view.Description = 'Add a device (supplicant) to this group using the form below:';

      } else if ( $scope.ModalMode === 'update' ) {

         $scope.view.Title = 'Edit Device';
         $scope.view.Description = 'Edit a device (supplicant) using the form below:';

         $scope.userAuth = !Utils.checkIfMacAuth( user.Username, user.Password );

         if( !$scope.userAuth ) {
            $scope.user.MacAddress = user.Username;
            $scope.user.Mac = user.Username.split(':').filter(function (val) {
              if (val === '^' || val == '*') {
                return
              }
              return val;
            });
            // stash values as we may need to restore?
            $scope.oldUsername = user.Username;
            $scope.oldPassword = user.Password;
            // remove from scope
            $scope.user.Username = '';
            $scope.user.Password = '';
         }
      }

      // at this point, we should always have a user on the scope
      if ($scope.user && $scope.user.ExpirationDate ) {
         var momentExpirationDate = moment($scope.user.ExpirationDate);
         $scope.Expiration.Enabled = true;
         // check the data is 'regular time' or DST
         var storedExpDate = momentExpirationDate.valueOf();
         $scope.Expiration.TzShortname = Utils.getTzCode(AppModel.timezone, storedExpDate);
         $scope.Expiration.Date = DateService.fakeUTCAsLocalTime($scope.user.ExpirationDate, $scope.AppModel.timezone);
      } else {
         var now = new Date().getTime();
         $scope.Expiration.TzShortname = Utils.getTzCode(AppModel.timezone, now);
      }

      /////////////////////////////////////////////
      // functions exposed to the view
      /////////////////////////////////////////////

      $scope.close = closeModal; // Close functionality
      $scope.createUser = createUser; // Create User
      $scope.updateUser = updateUser; // Update User
      // close error box button handler
      $scope.dismissAlert = dismissAlert;
      // paste function
      $scope.handlePaste = handlePaste;

      /////////////////////////////////////////////
      // functions
      /////////////////////////////////////////////

      function handlePaste($event){
         var paste = $event.originalEvent.clipboardData.getData('text/plain');
         if(Utils.validateMac(paste)){
            // replace the Mac input list with the text pasted
            $scope.user.Mac = paste.split(/[:-]+/).filter(function (val) {
              return val;
            });
         }
      }

      function dismissAlert () {
         $scope.validationError = '';
      }

      function closeModal () {
         $modalInstance.dismiss('close');
      }

      function createUser (userForm, newUser) {
         // deviceForm must be an Angular object
         if ( (userForm && userForm.$valid) && newUser ) {
            $scope.submitted = false;
            newUser.MaxUploadSpeedBits = $scope.sliderUl.value;
            newUser.MaxDownloadSpeedBits = $scope.sliderDl.value;

            if( $scope.Expiration.Enabled && $scope.Expiration.Date ) {
               newUser.ExpirationDate = DateService.removeGroupUTCOffset($scope.Expiration.Date, $scope.AppModel.timezone);
            } else {
               newUser.ExpirationDate = null;
            }

            if( $scope.userAuth === true) {
               UserService.save(newUser).then(errorOrListUsers);
            } else {
               newUser.MacAddress = Utils.filterMac(newUser.Mac)
               delete newUser.Mac;
               UserService.saveMac(newUser).then(errorOrListUsers);
            }

         } else {
            $scope.submitted = true;
            // This is a hack, setDirty doesn't seem to work
            $('.ng-pristine:visible').removeClass('ng-pristine').addClass('ng-dirty');
            // show error on Mac Auth
            if( $scope.userAuth != true) {
               // if mac address error set input focus to first error element
               document.getElementsByName(userForm.$error[Object.keys(userForm.$error)[0]][0].$name)[0].focus();
               $scope.validationError = {
                  msg: 'Provided MAC address does not meet the minimum requirements.',
                  type: 'danger'
               };
            }
         }
      }

      function updateUser (userForm, updatedUser) {
         // deviceForm must be an Angular object
         if ((userForm && userForm.$valid) && updatedUser) {

            $scope.submitted = false;
            $modalInstance.dismiss('close');

            updatedUser.MaxUploadSpeedBits = $scope.sliderUl.value;
            updatedUser.MaxDownloadSpeedBits = $scope.sliderDl.value;


            if( $scope.Expiration.Enabled && $scope.Expiration.Date ) {
               updatedUser.ExpirationDate = DateService.removeGroupUTCOffset($scope.Expiration.Date, $scope.AppModel.timezone);
            }  else {
               updatedUser.ExpirationDate = null;
            }

            // We call different web services for adding user-auth User vs mac-auth User
            if( $scope.userAuth === true) {
               UserService.save(updatedUser).then(function (obj){
                  if ( !obj.Error ) {
                     if( obj.Supplicant.Username !== $scope.oldUsername ){
                        //Delete old username
                        UserService.remove($scope.oldUsername).then(errorOrListUsers);
                     } else {
                        UserService.list();
                        $('.loading').hide();
                     }
                  } else {
                     SessionErrorService.handleError(obj);
                  }
               });
            } else {
               updatedUser.MacAddress = Utils.filterMac(updatedUser.Mac)
               delete updatedUser.Mac;
               UserService.saveMac(updatedUser).then(function (obj){
                  if ( !obj.Error ) {
                    if( obj.Supplicant.Username !== $scope.oldUsername ){
                        // Delete old username
                        UserService.remove($scope.oldUsername).then(errorOrListUsers);
                     } else {
                        UserService.list();
                        $('.loading').hide();
                     }
                  } else {
                     SessionErrorService.handleError(obj);
                  }
               });
            }
         } else {
            $scope.submitted = true;
            // This is a hack, setDirty doesn't seem to work
            $('.ng-pristine:visible').removeClass('ng-pristine').addClass('ng-dirty');
            // show error on Mac Auth
            if( $scope.userAuth != true) {
               // if mac address error set input focus to first error element
               document.getElementsByName(userForm.$error[Object.keys(userForm.$error)[0]][0].$name)[0].focus();
               $scope.validationError = {
                  msg: 'Provided MAC address does not meet the minimum requirements.',
                  type: 'danger'
               };
            }
         }
      }

      function compareUserPass (user, pass) {
         if (user === pass) {
            return true;
         } else {
            return false;
         }
      }

      function errorOrListUsers (obj){

         $modalInstance.dismiss('close');

         if ( !obj.Error ) {
            UserService.list();
            $('.loading').hide();
         } else {
            SessionErrorService.handleError(obj);
         }
      }

      function setSlider () {
         $rootScope.$broadcast('rzSliderForceRender');
      }

      /////////////////////////////////////////////
      // scope watches
      /////////////////////////////////////////////

      $scope.$watch('userAuth', function () {
         // onchange of view of the form, clear errors
         $scope.userForm.$setPristine(true);
         $scope.submitted = false;
      });

      $scope.$watch('user.UseRemote', function (newVal, oldVal) {

         // onchange of view of the form, clear errors
         $scope.userForm.$setPristine(true);
         $scope.submitted = false;

         // set back to user auth if mac auth is selected and enabled remote authentication
         if($scope.user.UseRemote === true &&  $scope.userAuth === false){
            $scope.userAuth = true;
         }

         if ($scope.userAuth === true && $scope.user.UseRemote === true) {
            if (oldVal === true && newVal === false) {
                  $scope.user.Password = '';
            } else {
               if (compareUserPass($scope.user.Username, $scope.user.Password)) {
                  $scope.user.Password = $scope.user.Username;
               }
            }
         }
      });

      $scope.$watch('Expiration.Enabled', function (v) {
         if( v ) {
            if ($scope.user.ExpirationDate) {
               // convert server exp date format suitable to display
               // in the timepicker, which uses UTC. For this to be
               // displayed correctly it needs to be offset by the
               // group timezone, so for LA, -8 hrs.
               $scope.Expiration.Date = DateService.fakeUTCAsLocalTime($scope.user.ExpirationDate, $scope.AppModel.timezone);
            } else {
               $scope.Expiration.Date = moment().utc().toDate();
            }
         } else {
            $scope.Expiration.Date = null;
         }
      });

      $scope.$watch('Expiration.Date', function (v) {
         if( v ) {
            // parse datepicker date as moment
            var now = moment(v, DATEPICKER_OUTPUT_FORMAT).valueOf();
            // Set to correct abbreviation
            $scope.Expiration.TzShortname = Utils.getTzCode(AppModel.timezone, now);
         }
      });

   }

})();
