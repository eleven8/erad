/**
 * User Wizard Controller
 *
 *
 */

(function () {

	'use strict';

	angular.module('eradControllers').controller('UserImportCtrl', UserImportCtrl);

	UserImportCtrl.$inject = ['$scope', '$modalInstance', '$http', 'AppModel', 'UserService', 'SessionErrorService'];

	function UserImportCtrl ($scope, $modalInstance, $http, AppModel, UserService, SessionErrorService) {

		/////////////////////////////////////////////
		// variables exposed to the view
		/////////////////////////////////////////////

		$scope.AppModel = AppModel;
		$scope.view = {
			Title: 'Import Users'
		};

		$scope.myFile = null;

		/////////////////////////////////////////////
		// functions exposed to the view
		/////////////////////////////////////////////

		// Close functionality
		$scope.close = closeModal;

		//upload button
		$scope.uploadFile = uploadFile;

		/////////////////////////////////////////////
		// functions
		/////////////////////////////////////////////

		function closeModal () {
			$modalInstance.dismiss('close');
		}

		function uploadFile (file) {

			var fd = new FormData();
			fd.append('file', file);

			UserService.importUsers(fd).then(function (reply) {
				$modalInstance.dismiss('close');
				if (reply.Error) {
					SessionErrorService.handleError(reply);
				} else {
					UserService.list();
				}
			});
		}

	}

})();
