'use strict';

/**
 * Delete User Controller
 *
 *
 */
angular.module('eradControllers').controller('DeleteUserCtrl', DeleteUserCtrl);
DeleteUserCtrl.$inject = ['$scope', '$modalInstance', 'UserService', 'SessionErrorService', 'user'];

function DeleteUserCtrl ($scope, $modalInstance, UserService, SessionErrorService, user) {

	/////////////////////////////////////////////
	// variables exposed to the view
	/////////////////////////////////////////////

	$scope.view = {
		Title: 'Delete Device'
	};

	$scope.user = user;

	/////////////////////////////////////////////
	// functions exposed to the view
	/////////////////////////////////////////////

	// Delete User
	$scope.deleteUser = deleteUser;

	// Close modal
	$scope.close = closeModal;

	/////////////////////////////////////////////
	// functions
	/////////////////////////////////////////////

	function closeModal () {
		$modalInstance.dismiss('close');
	}

	function deleteUser (username) {

		UserService.remove(username).then(function (obj){
			$modalInstance.dismiss('close');

			if ( !obj.Error ) {
				UserService.list();
				$('.loading').hide();
			} else {
				SessionErrorService.handleError(obj);
			}
		});
	}


}
