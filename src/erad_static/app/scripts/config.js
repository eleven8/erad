'use strict';

var config_module = angular.module('eradConfig', []);
var config_data = {
    'ERAD_CONFIG': {
        'ACCESS_CHANGE_HISTORY_LOG': [ 'ET-662-57', 'SR-980-83', 'FB-816-64', 'GX-287-83', 'IV-535-95' ]
    }
};
angular.forEach(config_data,function(key,value) {
    config_module.constant(value,key);
});
