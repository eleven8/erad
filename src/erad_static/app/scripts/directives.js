'use strict';

eradApp.directive('connectionInfo', [ 'AdminService', function (AdminService) {
  return {
    restrict: 'E',
    scope: {
      connection: '&'
    },
    link: function (scope) {
      scope.retryLoad = function () {

        AdminService.loadEndpoints().then(function (data) {
          if (data.Error) {
            scope.connection = false;
          }
          scope.connection = data.Endpoint;
        });
      };

      scope.retryLoad();

    },
    templateUrl: 'templates/_connectionInfo.html'
  };
}]);

eradApp.directive('timezoneSelector', ['DateService', function (DateService) {

  return {
    restrict: 'E',
    replace: true,
    template: '<select style="min-width:300px;"></select>',
    scope: {
      ngModel: '='
    },
    link: function ($scope, elem, attrs) {
      var data = [];
      var timezones = DateService.getTimezones();

      // Group the timezones by their country code
      var timezonesGroupedByCC = {};
      var countryCodes = DateService.zoneToCC();
      var abreviatedCountryCodes = DateService.CCToCountryName();

      angular.forEach(timezones, function (timezone) {
        if (countryCodes.hasOwnProperty(timezone.id)) {
          var CC = countryCodes[timezone.id];
          timezonesGroupedByCC[CC] = !timezonesGroupedByCC[CC] ? [] : timezonesGroupedByCC[CC];
          timezonesGroupedByCC[CC].push(timezone);
        }
      });

      // Add the grouped countries to the data array with their country name as the group option
      angular.forEach(timezonesGroupedByCC, function (zonesByCountry, CC) {
        var zonesForCountry = {
          text: abreviatedCountryCodes[CC] + ': ',
          children: zonesByCountry,
          firstNOffset: zonesByCountry[0].nOffset
        };

        data.push(zonesForCountry);
      });

      data = data.sort(function sorted(a, b) {
        if (a.text < b.text) {
          return -1;
        }
        if (a.text > b.text) {
          return 1;
        }
        return 0;
      });

      // Construct a select box with the timezones grouped by country
      angular.forEach(data, function (group) {
        var $optgroup = $('<optgroup label="' + group.text + '">');
        group.children.forEach(function (option) {
          if (attrs.displayUtc === 'true' && !option.name.includes('(UTC')) {
            option.name = option.name + ' (' + option.offset + ')';
          }

          $optgroup.append('<option value="' + option.id + '">' +
            option.name + '</option>');
        });
        elem.append($optgroup);
      });

      // Initialise the chosen box
      elem.chosen({
        width: attrs.width || '300px',
        include_group_label_in_selected: true,
        search_contains: true,
        no_results_text: 'No results, try searching for the name of your country or nearest major city.',
        placeholder_text_single: 'Choose a timezone'
      });

      // Update the box if ngModel changes
      $scope.$watch('ngModel', function () {
        elem.val($scope.ngModel);
        elem.trigger('chosen:updated');
      });
    }
  };
}]);

eradApp.directive('dropdown', function() {
  return {
    restrict: 'E',
    require: '^ngModel',
    scope: {
      ngModel: '=', // selection
      isDisabled: '=',
      items: '=',
      callback: '=callback'
    },
    link: function(scope, element) {

      element.on('click', function(event) {
        event.preventDefault();
      });

      scope.selectItem = function(item) {
        scope.ngModel = item;
        if (scope.callback) {
          scope.callback(item.title);
        }
      };
    },
    templateUrl: 'templates/dropdown.html'
  };
});

eradApp.directive('spinner', function() {
    return {
        restrict: 'A',

        link: function(scope, element) {

            var opts = {
                lines: 13, // The number of lines to draw
                length: 20, // The length of each line
                width: 10, // The line thickness
                radius: 30, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#000', // #rgb or #rrggbb or array of colors
                speed: 1, // Rounds per second
                trail: 60, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: '50%', // Top position relative to parent in px
                left: '50%' // Left position relative to parent in px
            };

            var spinner = new Spinner(opts).spin();
            var elem = $(element);
            elem.append( spinner.el );
        }
    };
});

/**
 * Directive for IP Address Validation
 */
eradApp.directive('validIp', [
   function() {

      var link = function($scope, $element, $attrs, ctrl) {

         var validate = function(viewValue) {

            if(!viewValue || !viewValue.length){
               // It's valid because we have nothing to compare against
               ctrl.$setValidity('validIp', true);
            }
            else if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(viewValue)) {

               ctrl.$setValidity('validIp', true);
            }
            else {
               ctrl.$setValidity('validIp', false);
            }
            return viewValue;
         };

         ctrl.$parsers.unshift(validate);
         ctrl.$formatters.push(validate);

         $attrs.$observe('validIp', function(){
            return validate(ctrl.$viewValue);
         });
      };
      return {
         require: 'ngModel',
         link: link
      };
   }
]);

/**
 * Directive for MAC Address Validation
 */
eradApp.directive('validMac', [
   function() {

      var link = function($scope, $element, $attrs, ctrl) {

         var validate = function(viewValue) {

            if(!viewValue || !viewValue.length){
               // It's valid because we have nothing to compare against
               ctrl.$setValidity('validMac', true);
            }
            else if (/^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/.test(viewValue)) {
               ctrl.$setValidity('validMac', true);
            }
            else {
               ctrl.$setValidity('validMac', false);
            }
            return viewValue;
         };

         ctrl.$parsers.unshift(validate);
         ctrl.$formatters.push(validate);

         $attrs.$observe('validMac', function(){
            return validate(ctrl.$viewValue);
         });
      };
      return {
         require: 'ngModel',
         link: link
      };
   }
]);

// as above, but just checks for two hex characters
eradApp.directive('validHex', [
   function() {

      var link = function($scope, $element, $attrs, ctrl) {

         var validate = function(viewValue) {

            if(!viewValue || !viewValue.length){
               // It's valid because we have nothing to compare against
               ctrl.$setValidity('validHex', true);
            }
            else if (/^([0-9A-Fa-f]{2})$/.test(viewValue)) {
              ctrl.$setValidity('validHex', true);
            }
            else {
               ctrl.$setValidity('validHex', false);
            }
            return viewValue;
         };

         ctrl.$parsers.unshift(validate);
         ctrl.$formatters.push(validate);

         $attrs.$observe('validHex', function(){
            return validate(ctrl.$viewValue);
         });
      };
      return {
         require: 'ngModel',
         link: link
      };
   }
]);

eradApp.directive('validWildcard', [
   function() {

      var link = function($scope, $element, $attrs, ctrl) {
         var validate = function(viewValue) {
            if(!viewValue || !viewValue.length){
               // It's valid because we have nothing to compare against
               ctrl.$setValidity('validWildcard', true);
            }
            else if (/^([0-9A-Fa-f]{2}|[\^\*])$/.test(viewValue)) {
              ctrl.$setValidity('validWildcard', true);
            }
            else {
               ctrl.$setValidity('validWildcard', false);
            }
            return viewValue;
         };

         ctrl.$parsers.unshift(validate);
         ctrl.$formatters.push(validate);

         $attrs.$observe('validWildcard', function(){
            return validate(ctrl.$viewValue);
         });
      };
      return {
         require: 'ngModel',
         link: link
      };
   }
]);

eradApp.directive('validUser', [
   function() {

      var link = function($scope, $element, $attrs, ctrl) {

        var originalVal = $scope.user.Username;
        var validate = function(viewValue) {

            if(!viewValue || !viewValue.length){
               // It's valid because we have nothing to compare against
               ctrl.$setValidity('validUser', true);

            // checks the form has been interacted with and the username
            // already exists
            } else if ($scope.userlist.indexOf(viewValue) !== -1 && ctrl.$dirty) {

               if (ctrl.$viewValue === originalVal) {
                // this covers cases when users click save on
                // a form they have interacted with, but restored
                // to the original value
                ctrl.$setValidity('validUser', true);
               } else {
                // otherwise, they have changed it to an existing username
                // ie, one that's not 'itself'
                ctrl.$setValidity('validUser', false);
               }

            } else {
               ctrl.$setValidity('validUser', true);
            }
            return viewValue;
         };

         ctrl.$parsers.unshift(validate);
         ctrl.$formatters.push(validate);

         $attrs.$observe('validUser', function(){
            return validate(ctrl.$viewValue);
         });
      };
      return {
         require: 'ngModel',
         link: link
      };
   }
]);

directives.directive('highCapacityRequired', [ function() {
    var USERNAME_TO_VALIDATE = 'aalert';
    var WARNING_TEXT = 'Staff alerting devices require a high-capacity port, which this site is not configured for. Please contact support.';

    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            function check_port(value) {
                if (value !== undefined && value.indexOf(USERNAME_TO_VALIDATE) !== -1 && scope.AppModel.highCapacityProvisioned === false) {
                     scope.view.usernameError = WARNING_TEXT;
                }
            }

            scope.$watch('user.Username', function (username) {
                if (username === undefined){
                    return;
                }
                check_port(username);
            });

            check_port(scope.user.Username);
        }
    };
}]);


/*
 * Does xhr to check password is not in blacklist
 */
directives.directive('noWeakPasswords', ['AdminService', '$q', function (AdminService, $q) {
  return {
     restrict: 'A',
     require: 'ngModel',
     link: function ($scope, element, attributes, ctrl) {
      ctrl.$asyncValidators.passwordCheck = function (modelVal) {
        if (ctrl.$isEmpty(modelVal)) {
          return $q.when();
        }

        var def = $q.defer();
        AdminService
          .checkPasswordBlacklist(modelVal)
          .then(function (reply) {
            if (attributes.allowWeakSubmit === 'true') {
              $scope.view.passwordError = reply.Error;
              def.resolve();
            } else if (reply.Error === null) {
              def.resolve();
            } else {
              $scope.view.passwordError = reply.Error;
              def.reject();
            }
         });

        return def.promise;
      };
    }
  };
}]);

/**
 * Directive for only digit entry
 */
eradApp.directive('onlyDigits', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;
            ngModel.$parsers.unshift(function (inputValue) {
                var digits = inputValue.split('').filter(function (s) { return (!isNaN(s) && s !== ' '); }).join('');
                ngModel.$viewValue = digits;
                ngModel.$render();
                return digits;
            });
        }
    };
});


/**
 * Directive to change 0 to '-'
 */
eradApp.directive('zeroToDash', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) return;
            ngModel.$parsers.unshift(function (inputValue) {
                var digits = inputValue > 0 ? inputValue : '-';
                ngModel.$viewValue = digits;
                ngModel.$render();
                return digits;
            });
        }
    };
});


/**
 * Directive to give access to file object from <input type='file'>
 * in view controllers
 */
eradApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

eradApp.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});

eradApp.directive("moveNextOnMaxlength", function() {
    return {
        restrict: "A",
        link: function($scope, element) {
            element.on("input", function(e) {
                if(element.val().length == element.attr("maxlength")) {
                    var $nextElement = element.next();
                    if($nextElement.length) {
                        $nextElement[0].focus();
                    }
                }
            });
        }
    }
});
