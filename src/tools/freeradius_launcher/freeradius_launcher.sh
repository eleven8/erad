#!/bin/bash

RADIUSD_PATH='/usr/local/sbin/radiusd'
export PYTHONPATH=$PATH:/usr/local/sbin

if [ -n "$T42_PASSPHRASE" ]; then
    INSTANCE_HOSTNAME=$(curl http://169.254.169.254/latest/meta-data/local-hostname)
    MESSAGE="radiusd was restarted on ${INSTANCE_HOSTNAME}"
    SNS_ARN='arn:aws:sns:us-east-1:439838887486:trouble'
    REGION='us-east-1'

    PUB="aws sns publish --profile sns --region ${REGION} \
        --topic-arn ${SNS_ARN} --message \"${MESSAGE}\""
fi

while true; do
    $RADIUSD_PATH -f
    [ -n "$PUB" ] && $PUB
done
