#!/usr/bin/env python

import sys, getopt
from time import sleep
import inspect
from optparse import OptionParser
from pynamodb.connection import Connection
from pynamodb.models import Model
from pynamodb.exceptions import DoesNotExist
from pynamodb.indexes import *
from pynamodb.attributes import (
        Attribute, 
        NumberAttribute, 
        UnicodeAttribute, 
        UTCDateTimeAttribute,
        UnicodeSetAttribute, 
        NumberSetAttribute, 
        BinarySetAttribute
        #BinaryAttribute,
    )

class CheckTables():

    HostUrl = None
    Region = None

    def main( self ):  

        print "Connecting to DynamoDB:"
        print "Host: "+self.HostUrl
        print "Region: "+self.Region
        print " "

        conn = Connection(host=self.HostUrl, region=self.Region)
        tables = conn.list_tables()


        for table in tables['TableNames']:
            table_threshold = 100
            if 'Erad_RadiusLog_Global' in table:
                table_threshold = 400
            print "Checking table: "+table
            description = conn.describe_table(table)
            if description['ProvisionedThroughput']['ReadCapacityUnits'] > table_threshold:
                raise ValueError("Read Capacity for "+table)

            if description['ProvisionedThroughput']['WriteCapacityUnits'] > table_threshold:
                raise ValueError("Write Capacity for "+table)

            if 'GlobalSecondaryIndexes' in description:
                for index in description['GlobalSecondaryIndexes']:
                    print "Checking index "+index['IndexName']+" in table "+table
                    if index['ProvisionedThroughput']['ReadCapacityUnits'] > table_threshold:
                        raise ValueError("Read Capacity for index in table: "+table)
                    if index['ProvisionedThroughput']['WriteCapacityUnits'] > table_threshold:
                        raise ValueError("Write Capacity for index in table: "+table)

if __name__ == "__main__":
    parser = OptionParser(usage="Usage: CheckTables.py [options]")
    parser.add_option("-H", "--HostUrl", dest="HostUrl",
                      help="HostUrl to be used for testing", metavar="HostUrl")
    parser.add_option("-R", "--Region", dest="Region",
                      help="Region to be used for testing", metavar="Region")    
    (options, args) = parser.parse_args()
    CheckTables.Region = "us-west-2" if not options.Region else options.Region
    CheckTables.HostUrl = "http://localhost:8000" if not options.HostUrl else options.HostUrl    

    sys.argv = args
    CheckTables().main()