## EC2 Security Group Incoming IP Updater

This scripts creates a daemon on client machine to update the target security group incoming IP.

First, you need to install dependencies using:

`pip install -r requirements.txt`

After, you just need to run script with its arguments.

Run `python run.py --help` to see usage options.
