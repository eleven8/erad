import boto3
import time
import requests
import argparse
import logging
from daemonize import Daemonize
from botocore.exceptions import ClientError


# Parse script command line arguments
def parse_args():
    parser = argparse.ArgumentParser(
        description='AWS EC2 Security Group IP Updater',
        add_help=True
    )

    parser.add_argument(
        '-i',
        '--aws-access-key-id',
        help='Set access key id for target IAM',
        dest='aws_access_key_id',
        default=None,
        action='store',
        required=True

    )

    parser.add_argument(
        '-s',
        '--aws-secret-access-key',
        help='Set secret access key for target IAM',
        dest='aws_secret_access_key',
        default=None,
        action='store',
        required=True
    )

    parser.add_argument(
        '-r',
        '--aws-default-region',
        help='Set AWS target region',
        dest='aws_default_region',
        action='store',
        default='us-west-2',
        choices=[
            "us-east-1",
            "us-west-2",
            "us-west-1",
            "eu-west-1",
            "eu-central-1",
            "ap-southeast-1",
            "ap-southeast-2",
            "ap-northeast-1",
            "ap-southeast-2",
            "sa-east-1",
            "cn-north-1"
        ],
        required=True
    )

    parser.add_argument(
        '-g',
        '--group-name',
        help='Set the target group to update ',
        dest='group_name',
        action='store',
        default=None,
        required=True
    )

    opts = parser.parse_args()

    # Convert instance type to dict
    opts = opts.__dict__

    return opts


# Get external IP
def get_ip():
    ip = requests.get("http://ipecho.net/plain")

    logging.getLogger(__name__).info(ip)

    if ip.status_code == 200:
        return ip.content

    return False


def update_group(args):
    security_groups = boto3.client(
        service_name='ec2',
        aws_access_key_id=args['aws_access_key_id'],
        aws_secret_access_key=args['aws_secret_access_key'],
        region_name=args['aws_default_region']
    ).describe_security_groups(
        GroupNames=[
            args['group_name'],
        ]
    )

    gid = None

    for sg in security_groups['SecurityGroups']:
        gid = sg['GroupId']

    sg = boto3.resource(
        service_name='ec2',
        aws_access_key_id=args['aws_access_key_id'],
        aws_secret_access_key=args['aws_secret_access_key'],
        region_name=args['aws_default_region']
    ).SecurityGroup(gid)

    try:
        sg.revoke_ingress(
            IpPermissions=sg.ip_permissions
        )

    except ClientError as e:
        if e.response['Error']['Code'] == 'MissingParameter':
            pass

    finally:
        sg.authorize_ingress(
            IpProtocol='-1',
            CidrIp="%s/32" % get_ip()
        )

        logging.getLogger(__name__).info(sg.ip_permissions)


def run():
    while True:
        update_group(args)

        # Polling each 15min
        time.sleep(60*15)


if __name__ == '__main__':
    args = parse_args()

    pid = "/tmp/ip_updater.pid"

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logger.propagate = False

    fh = logging.FileHandler("/tmp/ip_updater.log", "w")
    fh.setLevel(logging.DEBUG)
    logger.addHandler(fh)
    keep_fds = [fh.stream.fileno()]

    daemon = Daemonize(
        app="ip_updater",
        pid=pid,
        action=run,
        keep_fds=keep_fds
    )
    daemon.start()
