#!/bin/bash

# Install apache
yum update -y
yum install -y httpd24 php56 mysql55-server php56-mysqlnd

# install python modules
pip install --upgrade pip python-dateutil
# download Mkdocs
pip install mkdocs

# Fix path
export PATH=$PATH:/usr/local/bin

# Build Site
mkdocs build --clean
# Copy site to www folder or s3 bucket
cp -r /usr/local/src/site/* /var/www/html
# Start apache
service httpd start