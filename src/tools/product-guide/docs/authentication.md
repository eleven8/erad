## Request Signature Verification

#### Signing the payload with a signature

- Required: a payload, timestamp, ApiKey, ApiKeyName, API Url (i.e.: erad/api/supplicant/load)
- Example:

```json
"ApiKey": "i_qwertt123512354325"
"ApiKeyName": "ElevenWireless"
"APU Url": "erad/api/supplicant/load"
"timestamp": "1454015468"
"payload":
  {       
    "Supplicant": { 
      "Username": "testing",
      "Group_ID":"foo"
    }    
  }
```

----------

1) **Optional**: Copy payload to temporary variable and then use it in further steps.  

2) Add ApiKeyName, a timestamp and an API Url to a payload. The timestamp must be UTC and within 10 minutes of our server time. For API Url, use `"erad/api"` instead of `"erad/admin"` or `"erad/integration"`.  

```json
{       
  "Supplicant": { 
    "Username": "testing",
    "Group_ID":"foo"
  },
  "ApiKeyName": "ElevenWireless",
  "Timestamp": "1454015468",
  "Url": "erad/api/supplicant/load"
}
```

3) Sort payload by the key (asc) - **Important**: we have 2 levels of data in a payload and both have to be sorted  

```json
{       
  "ApiKeyName": "ElevenWireless",
  "Supplicant": { 
    "Group_ID":"foo",
    "Username": "testing"
  },
  "Timestamp": "1454015468",
  "Url": "erad/api/supplicant/load"
}
```
4) Construct a canonical string made of a payload - use the pattern: `key+value+key+subkey+subvalue` etc. 
   **Example:**
   `ApiKeyNameElevenWirelessSupplicantGroup_IDfooUsernametestingTimestamp1454015468Urlerad/api/supplicant/load`

5) Base64 the canonical string.

6)  Hash base64 encoded string using hmac with sha256 encryption (digest method). Use your ApiKey as the HMAC 
    cryptographic key.

7) Add the hash to the original payload as a `"Signature": "---hash---"`   
   **Important:** Sent payload doesn't have to be sorted.

```json
{       
  "Supplicant": { 
    "Username": "testing",
    "Group_ID":"foo"
  },
  "ApiKeyName": "ElevenWireless",
  "Timestamp": "1454015468",
  "Url": "erad/api/supplicant/load",
  "Signature": "659510f7d15dedc5d2c68d83f5a002ea8c0470be2f40231f0992f288ff448930"
}
```

8) Send the payload.

