Manual deployment.

To deploy:
   - Zip the files in this folder into "dashboard.tar.gz" and place that file at s3://teamfortytwo.deploy/
   - Launch the /aws/standard-stack.json configuration through CloudFormation.
      - Specify "dashboard.tar.gz" for the DeployFile parameter.
