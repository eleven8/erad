var testPageLoad = function (pgUrl) {
	$("#pageLoadStatus").css('color', "orange");
	$("#pageLoadStatus").html("In Progress");
	$.ajax({
	  type: "POST",
	  url: "http://localhost:3000/test",
	  // contentType: "application/json",
	  data: {"url": pgUrl},
	  success: function(data) {
	  	console.log(data);
	  	$("#pageLoadStatus").css('color', "green");
	  	$("#pageLoadStatus").html(data);
	  },	
	  error: function (xhr, ajaxOptions, thrownError) {
      	
      }	  
	});
};