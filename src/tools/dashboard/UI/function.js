var IntegrationKey;
var HostUrl;
var macsToBeSaved = [];
var configData;
var consoleUrl;
var passphraseUrl;
var passphrase;
var groupList;
//variable to check if group lists are declared on config file. default to false
var groupListStatus = false;

$(document).ready(function(){
	//Function to submit form on enter key press
	$('.input').keypress(function (e) {
	  if (e.which == 13) {
	    // $('form#login').submit();
	    e.preventDefault();
	    submitForm();
	    return false;
	  }
	});
	//Setting Default log out url to current url
	$("#txtLogoutUrl").val($(location).attr('href'));
	//load session id at page load
	$("#txtSessionId").val(generateSessionId());

	//handle refresh button click event
	$( "#btnRefresh" ).click(function () {
		//add refresh function here
		$("#txtSessionId").val(generateSessionId());
	});
	//handle clear button event
	$("#btnClear").click(function(){
		$("#txtSessionId").val(generateSessionId());
		loadServers();
		$("#txtId").val("");
		$("#txtIdentifier").val("");
		$("#selectGroupId option:selected").removeAttr("selected");
		$("#txtLogoutUrl").val($(location).attr('href'));
		$("#txtResponse").val("");
		$("#statusCode").html("");
		$("#txtPassphrase").val("");
	});

	//load servers to the dropdown list
	loadServers();

	//handle server change
	$("#serverSelect").change(function(){
		serverChanged();
	});

	//handle form submit
	$("#btnSubmit").click(function() {
		submitForm();

	});
	//handle mac validate
	$("#btnMacSubmit").click(function() {
		if($("#txtMacs").val() == ""){
			displayError("MAC Address cannot be blank");
			$('#txtMacs').focus();
		}else{
		$("#txtMacResponse").val("");
			var mac = $("#txtMacs").val();
			var macGroupId = $("#txtMacGroupId").val();
			validateMac(IntegrationKey, macGroupId, mac);
		}

	});

	//handle hint click
	$("#btnHint").click(function() {
		window.open(passphraseUrl, "_blank");
	});

	//pageLoadTest
	$("#btnPageLoad").click(function() {
		var encodedSessionId= encodeURIComponent($("#txtSessionId").val());
		testPageLoad(consoleUrl + "#session=" + encodedSessionId);
	});
});

//error display function
var displayError = function (message) {
	toastr.options = {"positionClass": "toast-bottom-right"};
	toastr.error(message);
};

//warning display function
var displayWarning = function (message) {
	toastr.options = {"positionClass": "toast-bottom-right"};
	toastr.warning(message);
};

//return generated sessionId
var generateSessionId = function() {
	return rand() + rand();
};

//generates random string
var rand = function() {
	return Math.random().toString(36).substr(2);
};

//load server list
var loadServers = function() {
	try {
		$.getJSON("config.json", function(data){
		//set global variables
		configData = data;
		//populate servers
		$("#serverSelect").empty();
		$.each(data.servers, function(key, value) {
			$("#serverSelect").append(new Option(value.name, key));
		});
		serverChanged();
		// IntegrationKey = data.servers
	});
	} catch (ex) {
		toastr.error(".config file error");
		// alert(".config file error");
	}
};

//server change action
var serverChanged = function() {
	var index = $("#serverSelect").val();
	IntegrationKey = configData.servers[index].IntegrationKey;
	HostUrl = configData.servers[index].HostUrl;
	consoleUrl = configData.servers[index].ConsoleUrl;
	passphraseUrl = configData.servers[index].PassphraseHelpUrl;
	passphrase = configData.servers[index].Passphrase;
	groupList =  configData.servers[index].GroupIdList;
	//Enable and Disable Textbox based on Group id on config file
	if(typeof groupList === "undefined" || groupList.length === 0){
		$("#selectDiv").hide();
		$("#txtDiv").show();
		// Group lists haven't declared on config file
		groupListStatus = false;
	}else{
		//Group Lists have declared on Config File
		groupListStatus = true;
		$("#txtDiv").hide();
		$("#selectDiv").show();
		$("#selectDiv").find('option').remove().end();
		var option = '';
		//Generate Option
		for (var i=0;i<groupList.length;i++){
		   option += '<option value="'+ groupList[i] + '">' + groupList[i] + '</option>';
		}
		$('#selectGroupId').append(option);
	}
};

//makes request
var makeRequest = function(IntegrationKey, HostUrl, data) {
	$.ajax({
	  type: "POST",
	  url: HostUrl + "erad/integration/session/save",
	  contentType: "application/json",
	  data: JSON.stringify(data),
	  success: function(data) {

		$("#statusCode").html("200 OK");
		$("#statusCode").addClass("label label-success");
		$("#txtResponse").val($("#txtResponse").val() + "\n" + JSON.stringify(data, null, 2));
	  	if(data.Error == null) {
			//redirect user
			var encodedSessionId= encodeURIComponent($("#txtSessionId").val());
			if($("#radios-0").is(":checked")) {
				//Redirect on success
				$(location).attr('href', consoleUrl + "#session=" + encodedSessionId);
			}
			if($("#radios-1").is(":checked")) {
				//open the test window here
				$('#testWindowModal').modal('show');
			}
		}
	  },
	  error: function (xhr, ajaxOptions, thrownError) {
        $("#statusCode").html(xhr.status + " " + xhr.statusText);
		if(xhr.status == "400") {
			$("#statusCode").addClass("label label-danger");
		}
		$("#txtResponse").val($("#txtResponse").val() + "\n" + JSON.stringify(xhr.responseText, null, 2));
      },
	  dataType: "json"
	});
};

//validate group ID
var validateGroupId = function(IntegrationKey, id, timezone) {
	var data = {
		"IntegrationKey": IntegrationKey,
		"Group": {"ID": id}
	};

	return $.ajax({
	  type: "POST",
	  url: HostUrl + "erad/integration/group/load",
	  contentType: "application/json",
	  data: JSON.stringify(data),
	  success: function(data) {
		if(data.Error != null) {
			$("#txtResponse").val($("#txtResponse").val() + "\n ### Group ID: " + id + " does not exist. Saving ID..");
			saveGroupId(IntegrationKey, id, timezone);
		}
	  },
	  error: function (xhr, ajaxOptions, thrownError) {

      },
	  dataType: "json"
	});
};

//save group ID
var saveGroupId = function(IntegrationKey, id, timezone) {
	var data = {
		"IntegrationKey": IntegrationKey,
		"Group": {"ID": id, "Name": id, "RemoteServerUrl": "", "SharedSecret": "", "TimeZone": timezone}
	};

	return $.ajax({
	  type: "POST",
	  url: HostUrl + "erad/integration/group/save",
	  contentType: "application/json",
	  data: JSON.stringify(data),
	  success: function(data) {
		if(data.Error == null){
			$("#txtResponse").val($("#txtResponse").val() + "\n ### Group ID: " + id + " saved");
		} else {
			$("#txtResponse").val($("#txtResponse").val() + "\n ### Group ID: " + id + " Error occurred while saving : " + data.Error);
		}
	  },
	  error: function (xhr, ajaxOptions, thrownError) {

      },
	  dataType: "json"
	});
};

//validate mac
var validateMac = function(IntegrationKey, id, mac) {
	//Checking for Passpharse validation before executing the code
	if(passphrase != $("#txtPassphrase").val()) {
		displayError("Invalid Passphrase");
		$('#txtPassphrase').focus();
	} else {

		var data = {
			"IntegrationKey": IntegrationKey,
			"Authenticator": {"ID": mac}
		};

		$.ajax({
		  type: "POST",
		  url: HostUrl + "erad/integration/authenticator/load",
		  contentType: "application/json",
		  data: JSON.stringify(data),
		  success: function(data) {
			if(data.Error == null){
				if(data.Authenticator.Group_ID == id) {
					$("#txtMacResponse").val($("#txtMacResponse").val() + "\n ### found match for MAC: " + mac + " with Group ID " + id);
				} else {
					$("#txtMacResponse").val($("#txtMacResponse").val() + "\n ### no match found for MAC: " + mac + " with Group ID " + id);
					saveNewMac(IntegrationKey, mac, id);
				}
			} else {
				$("#txtMacResponse").val($("#txtMacResponse").val() + "\n ### MAC: " + mac + " " + data.Error);
				if(data.Error == "This item does not exist in the table.") {
					//saving new mac
					saveNewMac(IntegrationKey, mac, id);
				}
			}
		  },
		  error: function (xhr, ajaxOptions, thrownError) {
	        console.log(xhr);
	      },
		  dataType: "json"
		});
	}
};

//save mac
var saveNewMac = function(IntegrationKey, mac, id) {
	var data = {
		"IntegrationKey": IntegrationKey,
		"Authenticator": {"ID": mac, "CalledStationId": mac, "Group_ID": id}
	};

	$.ajax({
	  type: "POST",
	  url: HostUrl + "erad/integration/authenticator/save",
	  contentType: "application/json",
	  data: JSON.stringify(data),
	  success: function(data) {
		if(data.Error == null){
			$("#txtMacResponse").val($("#txtMacResponse").val() + "\n ### saved new MAC " + mac + " with the first Group ID specified " + id);
		} else {
			$("#txtMacResponse").val($("#txtMacResponse").val() + "\n ### error saving new MAC " + mac + " with the first Group ID specified " + id + " " + data.Error);
		}
	  },
	  error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr);
      },
	  dataType: "json"
	});
};

//Form Submit Code
var submitForm = function(){
	if(passphrase != $("#txtPassphrase").val()) {
		displayError("Invalid Passphrase");
		$('#txtPassphrase').focus();
	} else {
		$("#txtResponse").val("");
		// Getting Values from text boxes
		var server = $("#serverSelect").val();
		var sessionId = $("#txtSessionId").val();
		//Generate Group Ids Array according to the config file
		if(groupListStatus === false){
			var groupIds = $("#txtId").val().replace(" ", "").split(",");
		} else{
			//If Group Ids have already defined
			var groupIds = [];
			$('#selectGroupId :selected').each(function(i, selected){
			  groupIds[i] = $(selected).text();
			});
		}
		var identifier = $("#txtIdentifier").val();
		var timezone = $("#txtTimeZone").val();
		var logoutUrl = $("#txtLogoutUrl").val();

		if($(groupListStatus === false && "#txtId").val() == ""){
			displayError("Group ID / IDs cannot be blank");
			$('#txtId').focus();
		} else{
			//validate and save group Ids
			var promises=[];
			groupIds.forEach(function(element) {
				var request= validateGroupId(IntegrationKey, element, timezone);
				promises.push(request);
			}, this);

			//Wait to finsih all the ajax calls using a promise
			$.when.apply(null, promises).done(function(){
				if(identifier == ""){
					identifier = "Console Launcher";
					displayWarning("Using Default Identifier");
				}

				if(identifier != "" && logoutUrl != "") {
					var requestData = {
						"IntegrationKey": IntegrationKey,
						"Session": {"ID":sessionId, "Group_ID_List":groupIds, "Identifier":identifier, "LogoutUrl":logoutUrl}
					};
					makeRequest(IntegrationKey, HostUrl, requestData);
				}else{
					displayError("You need to input Identifier and Logout URL");
					$('#txtIdentifier').focus();
				}
			});
		}
	}
}
