#!/bin/bash
#
# monitoraccess	Monitor Access Team 42
# chkconfig: 345 70 30
# description: monitoraccess
# processname: python
# Source function library.
. /etc/init.d/functions


USERID=`id -u`
status() {
	PIDs=`ps aux |grep python |grep '/usr/bin/python manage.py' |grep -v grep|grep -Eo '^[a-zA-Z]+\s+[0-9]+'|grep -Eo '[0-9]+$' `
	if [ $? -eq 0 ];then
		echo "$PIDs"
		return 0
	else
		echo ''
		return 1 
	fi
}
start() {
	if [ "$USERID" != "0" ];then
		echo "Please run this script as root"
		exit 1
	fi
	STATUS=$(status)
	if [ "$STATUS" != "" ];then
		echo "Server is already running, or there's another Django running, please start the server manually using cd /opt/monitoraccess && nohup python manage.py runserver 0.0.0.0:8080&"
		return 1
	fi
	PYTHON='sudo -u apache /usr/bin/python'
	pushd  /opt/monitoraccess
	mkdir /var/log/monitoraccess 2>/dev/null
	gzip -c /var/log/monitoraccess/nohup.log > "/var/log/monitoraccess/`date`.gz" 2>/dev/null
	echo 'Starting the server...'
	nohup $PYTHON manage.py runserver 0.0.0.0:8080 > /var/log/monitoraccess/nohup.log &
	popd
	pushd  /opt/log_stream
	nohup $PYTHON index.py &
	popd
        return $?
}
stop() {
	if [ "$USERID" != "0" ];then
		echo "Please run this script as root"
		exit 1
	fi
	STATUS=$(status)
	if [ "$STATUS" != "" ];then
		kill $STATUS && echo 'Server stopped.'
	fi
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
	STATUS=$(status)
	if [ "$STATUS" != "" ];then
		echo 'Server is running...'
	else 
		echo 'Server is stopped.'
	fi
        ;;
    restart)
        stop
        start
        ;;
    *)
        echo "Usage: monitoraccess {start|stop|status|restart}"
        exit 1
        ;;
esac
exit $?
