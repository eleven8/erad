(function (){
    // parse the date / time from api
    var parseDate = d3.time.format.utc("%Y-%m-%dT%H:%M:%S").parse;
    // size definition
    sizes = {
        SMALL: {
            WIDTH : 300,
            HEIGHT : 150,
            MARGINS : {
                top: 8,
                right: 8,
                bottom: 8,
                left: 40
            },
            TICKFORMAT: "%H:%M"
        },
        MEDIUM: {
            WIDTH : 600,
            HEIGHT : 300,
            MARGINS : {
                top: 8,
                right: 8,
                bottom: 8,
                left: 50
            },
            TICKFORMAT: "%H:%M:%S"
        },
        LARGE: {
            WIDTH : 900,
            HEIGHT : 400,
            MARGINS : {
                top: 8,
                right: 8,
                bottom: 8,
                left: 50
            },
            TICKFORMAT: "%b %d %H:%M:%S%p"
        }
    }; // size definition ends
    // the first metric (eference metric) is used to define the scales for the graph
    metrics = [
    {metric_str: 'access-request', color: 'purple'}, 
    {metric_str: 'access-accept', color: 'red'}, 
    {metric_str: 'access-reject', color: 'blue'}
    ]; 

    setInterval(function() {update_graph(metrics, parseDate);}, 3000);

    // get parsed available data, scales and min/max values for the graph
    var analyze_data = function(metrics, size, json_data) {
        all_max_times = [];
        all_max_counts = [];
        all_min_times = [];
        // get the first data as a reference
        ref_data = json_data.result[metrics[0].metric_str];
        available_data = [];
        for (var i=0; i<metrics.length;i++) {
            metric_str = metrics[i].metric_str;
            data = json_data.result[metric_str]; // get data for current metric_str
            if ( !data || data.length == 0 ) // data is not defined or is empty
                continue
            // if here then data is available for the current metric_str
            // parse time attributes
            data.forEach(function(d) {
                d.time = parseDate(d.time);
                d.count = d.count;
            });
            // sort data according to time attribute
            data.sort(function sortByDateAscending(a, b) {
                return a.time - b.time;
            });
            // add the processed data to available_data list
            available_data.push({
                data: data,
                color: metrics[i].color,
                metric_str: metric_str
            });
            // add min/max values of this data
            all_max_times.push(d3.max(data, function(d) { return d.time; }));
            all_min_times.push(d3.min(data, function(d) { return d.time; }));
            all_max_counts.push(d3.max(data, function(d) { return d.count; }));
        }
        // calculate min/max values of all metrics
        min = d3.max(all_min_times);
        max = d3.min(all_max_times);

        // if reference data is available override calculated min/max 
        if (ref_data && ref_data.length > 0) {
            max = d3.max(ref_data, function(d) { return d.time; })
            min = d3.min(ref_data, function(d) { return d.time; })
        }
        // trim data according to the min/max values (ready to return)
        available_data.forEach(function(data){
            trimmed_data = [];
            data.data.forEach(function(d) {
                if (d.time >= min && d.time <= max) {
                    trimmed_data.push(d);
                }
            });
            data.data = trimmed_data;
        });
        result = {};
        // x and y d3 scale functions
        result.xScale = d3.time.scale().range([size.MARGINS.left, size.WIDTH - size.MARGINS.right]).domain([min, max]);
        result.yScale = d3.scale.linear().range([size.HEIGHT - size.MARGINS.top, size.MARGINS.bottom]).domain([0, d3.max(all_max_counts)]);
        result.data_min = min;
        result.data_max = max;
        result.available_data = available_data;
        return result;
    } // end of analyze_data 

    var draw_axis = function( vis, scales ){

        xScale = scales.xScale;
        yScale = scales.yScale;
        size = scales.size;

        // create xAxis according to xScale
        xAxis = d3.svg.axis()
        .scale(xScale)
        .ticks(5)
        .tickFormat(d3.time.format(size.TICKFORMAT));

        // create yAxis according to yScale
        yAxis = d3.svg.axis()
        .scale(yScale)
        .orient("left");

        // draw the x axis
        vis.append("svg:g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + (size.HEIGHT - size.MARGINS.bottom) + ")")
        .call(xAxis);

        // draw the y axis
        vis.append("svg:g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + (size.MARGINS.left) + ",0)")
        .call(yAxis);

        // set the width/height of the graph
        vis.attr('width', size.WIDTH+size.MARGINS.left+size.MARGINS.right)
        .attr('height', size.HEIGHT+size.MARGINS.bottom+size.MARGINS.top);

        // draw the x Grid lines
        vis.append("g")
            .attr("class", "grid")
            .attr("transform", "translate(0," + size.HEIGHT + ")")
            .call(
                d3.svg.axis()
                .scale(xScale)
                .orient("bottom")
                .ticks(5)
                .tickSize(-size.HEIGHT, 0, 0)
                .tickFormat("")
            )

        // draw the y Grid lines
        vis.append("g")            
            .attr("class", "grid")
            .attr("transform", "translate(" + size.MARGINS.left + ", 0)")
            .call(
                d3.svg.axis()
                .scale(yScale)
                .orient("left")
                .tickSize(-size.WIDTH + size.MARGINS.left, 0, 0)
                .tickFormat("")
            )
    } // end of draw_axis
    // Update graph function (the main function called by setInterval)
    var update_graph = function(metrics, parseDate){

        // get size definition from the window hash
        size = sizes.MEDIUM;
        hash = sizes[window.location.hash.toUpperCase().replace('#', '')]
        if (typeof(hash) !== 'undefined') {
            size = hash;
        }
        
        // Get data from api
        d3.json("/api", function(json_data) {

            // if any data exists in this reply remove previous graph 
            if ( json_data.result.length != 0 ) {
                d3.selectAll("svg > *").remove();
            }
            
            // analyze json_data 
            result = analyze_data(metrics, size, json_data);
            xScale = result.xScale;
            yScale = result.yScale;
            min = result.min;
            max = result.max;
            available_data = result.available_data;

            // select the visualization place-holder 
            vis = d3.select("#visualisation");

            // draw x and y axis
            draw_axis( vis, {
                xScale: xScale,
                yScale: yScale,
                size: size
            });
            
            // draw available data
            available_data.forEach(function(data){
                color = data.color;
                metric_str = data.metric_str;
                data = data.data;
                
                // line generator
                lineGen = d3.svg.area()
                .x(function(d) {
                    return xScale(d.time);
                })
                .y(function(d) {
                    return yScale(d.count);
                });

                // generate lines
                vis.append('svg:path')
                .attr('d', lineGen(data))
                .attr('stroke', color)
                .attr('stroke-width', 2)
                .attr("data-legend", metric_str)
                .attr('fill', 'none');
            });
            
            // add legend to the graph (detects data-legend automatically)
            legend = d3.select("#visualisation").append("g")
            .attr("class","legend")
            .attr("transform","translate(50,30)")
            .style("font-size","12px")
            .call(d3.legend)
        }); // end of d3 json call
    }; // end of update_graph
})();
