(function (){
	// show a news ticker to display new log entries every 3 seconds

	var socket = io.connect('http://' + document.domain + ':' + 5000);
	socket.on('log_stream', function(records) {
		// add each record to the top of the ticker list
		if (records.length) {
			records.forEach(function(record){
				var iconClass;
				var remoteAuthIconClass;
				var listClass;
				if(record.IsProxied.N === "1") {
					remoteAuthIconClass = 'active';
				}else{
					remoteAuthIconClass = 'inactive';
				}
				if (record.Access.S === 'Access-Accept') { 
					iconClass = 'fa-check';
					listClass = 'text-success';
				} else if (record.Access.S === 'Access-Reject') {
					iconClass = 'fa-times';
					listClass = 'text-danger';
				} else {
					iconClass = '';
					listClass = '';
				}
				var username = (record.Username) ? record.Username.S : '';
				var groupName = (record.Group_Name) ? record.Group_Name.S : '';
				if ( $('#ticker li').length < 1000 ) { // limit the record number to 1000
					var liWithIcon = '<li class="'+ listClass +'"><span class="fa-lg success-indicator-icon">' +
						'<i class="fa fa-wifi ' + remoteAuthIconClass + '"></i><i class="fa ' + iconClass + '"></i></span>';                    
					$('#ticker').prepend(liWithIcon + groupName + ' | ' + _.escape(username) + '</li>');
				} else {
					var first = $('#ticker li:last').insertBefore('#ticker li:first');
					first.attr('class', listClass);
					first.find('i:eq(0)').attr('class', 'fa fa-wifi ' + remoteAuthIconClass);
					first.find('i:eq(1)').attr('class', 'fa ' + iconClass);
					first.css('opacity', '1.0');
					first.find('span')[0].nextSibling.textContent=groupName + ' | ' + _.escape(username);
				}
			});

			$('#ticker li:eq(' + (records.length - 1) + ')').nextAll().each(function(){
				$(this).animate({opacity: 0.5}, 50); // change opacity of all previous entries
			});

		}
		// remove extra entries
	});
})();
