(function () {

	var img = document.querySelector('#monitor-graph-img');

	function formatHash() {
		if(window.location.hash.toUpperCase().replace('#', '') == ""){
			return "MEDIUM";
		}else{
			return window.location.hash.toUpperCase().replace('#', '');
		}	
	}

	function calculateWidth (sizeObject) {
		return sizeObject.WIDTH + sizeObject.MARGINS.left + sizeObject.MARGINS.right;
	}

	function calculateNewGraphWidth () {
		
		var hash = formatHash();
		var size = sizes[hash];

		img.width = calculateWidth(size);

	}

	calculateNewGraphWidth();

	window.onhashchange = calculateNewGraphWidth;
})();
