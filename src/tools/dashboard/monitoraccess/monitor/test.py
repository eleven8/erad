from threading import Thread
from time import sleep
from monitor import Monitor
import sys
m = Monitor()
t1=Thread(target=m.run)
t1.daemon = True
t1.start()
try:
	while True:
		print 'sleeping for 5 sec'
		sleep(5)
		for stat in m.get_stat():
			print stat
		print 'sleeping for 5 sec'
		sleep(5)
		for stat in m.get_stat():
			print stat
		print 'sleeping for 185 sec'
		sleep(3*60+5)
		for stat in m.get_stat():
			print stat
		print 'sleeping for 5 sec'
		sleep(5)
except (KeyboardInterrupt, SystemExit):
	print 'requesting exit for the monitoring thread'
	m.exit();
	sys.exit()
