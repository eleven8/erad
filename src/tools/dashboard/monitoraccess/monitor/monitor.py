from time import sleep, mktime
from traceback import format_exc as stacktrace
import boto.ec2.cloudwatch
from datetime import datetime, timedelta

# a daemon looks for new metrics added to cloudwatch every 3 seconds
# saves the metrics in a list provided to the get_stat function
# clears all data in this list if get_stat wasn't called in timeout seconds
class Monitor():
    def __init__(self, *args):
        """ Monitor() constructor accepts any number of strings each representing
        a metric to monitor. example: Monitor('access-request', 'access-accept', ...)"""
        # get metrics from args
        self.metrics = args
        self.stat = {}
        self.previous = {}
        self.previous_time = {}
        # set last_get to now at the start
        self.last_get = self.get_second(datetime.utcnow())
        # initialize the metrics list, and the previous values
        for metric_str in self.metrics:
            self.stat[metric_str] = []
            self.previous[metric_str] = 0
            self.previous_time[metric_str] = self.last_get
        # stats older than stat_interval are deleted
        self.stat_interval = 3*60
        # if not get_stat was called after timeout start over
        self.timeout = 3*60
        # exit request to exit from the monitor thread properly
        self.exit_request = False
        # try to connect to the cloudwatch
        try:
            self.conn = boto.ec2.cloudwatch.connect_to_region('us-east-1')
        except:
            self.conn = None
            print stacktrace()
    def add_metric_init(self, response, now, metric_str):
        """ adds initial values for the metrics list
            add average of the sum to all 3 seconds interval from
            time_base to now.
        """
        now_minutes = self.get_minute(now)
        for stat in response:
            # time of the response
            time_base = stat['Timestamp']
            # we're in a new minute
            if now_minutes != time_base:
                seconds = 60
            else:
                self.previous[metric_str] = stat['Sum']
                # how many seconds passed from time_based
                seconds = (now - time_base).seconds
            # add the average value for 3 seconds interval
            for i in range(seconds/3):
                self.stat[metric_str].append({'time': time_base, 'count': round(stat['Sum']/float(seconds), 2)})
                time_base += timedelta(seconds=3)
        # sort all stats for this metric
        self.stat[metric_str].sort(key=lambda item:item['time'])
    def init(self, now):
        """ initialize empty metrics """
        try:
            # detect if there's any empty metric
            empty_metric = False
            for metric_str in self.stat:
                if self.stat[metric_str] == []:
                    empty_metric = True
            if empty_metric:
                # get the empty metrics in last 3 minutes
                start = now - timedelta(minutes=2)
                end = now + timedelta(minutes=1)
                if self.conn == None:
                    try:
                        self.conn = boto.ec2.cloudwatch.connect_to_region('us-east-1')
                    except:
                        print stacktrace()
                        return
                for metric_str in self.stat:
                    if self.stat[metric_str] == []:
                        response = self.conn.get_metric_statistics(period=60, start_time=start, end_time=end, metric_name=metric_str, namespace='RADIUS', statistics='Sum')
                        self.add_metric_init(response, now, metric_str)
        except:
            print stacktrace()
    def get_minute(self, time):
        """ set seconds and microseconds to 0 """
        return time - timedelta(seconds=time.second, microseconds=time.microsecond)
    def get_second(self, time):
        """ set microseconds to 0 """
        return time - timedelta(microseconds=time.microsecond)
    def get_previous(self, time, metric_str):
        """ get last metric added of this metric kind """
        if self.get_minute(time) == self.get_minute(self.previous_time[metric_str]):
            return self.previous[metric_str]
        else:
            return 0
    def delete_old_stat(self, now, metric_str):
        """ delete old stats according to the stat_interval """
        min_time = now - timedelta(seconds=self.stat_interval)
        to_delete = []
        for i in range(len(self.stat[metric_str])):
            if self.stat[metric_str][i]['time'] < min_time:
                to_delete.append(i)
        to_delete.sort(reverse=True)
        for i in to_delete:
            del(self.stat[metric_str][i])
    def add_stat(self, stat, now, metric_str):
        """ add new stat to the metric """
        #print 'new add:', stat
        self.delete_old_stat(now, metric_str)
        self.stat[metric_str].append(stat)
    def stop(self):
        """ reset the stats after stop """
        for metric_str in self.stat:
            self.stat[metric_str] = []
            self.previous[metric_str] = 0
    def add_metric(self, response, now, metric_str):
        """ add metrics from response to the previously added
        metrics """
        previous = self.get_previous(now, metric_str)
        for stat in response:
            try:
                # check if we're in a new minute
                if self.get_minute(now) != self.get_minute(self.previous_time[metric_str]):
                    self.previous[metric_str] = stat['Sum']
                    self.previous_time[metric_str] = now
                    # if 0 seconds passed this minute set metrics count to sum
                    if now.second == 0:
                        count = stat['Sum']
                    # else calculate the average
                    else:
                        count = round(stat['Sum']/float(now.second), 2)
                # else calculate average count according to the elapsed time from previous added metric
                else:
                    count = round((stat['Sum']-previous)/float((now - self.previous_time[metric_str]).seconds), 2)
                # if the calculated count is positive add it to the stats
                if count > 0:
                    self.add_stat({'time': now, 'count': count}, now, metric_str)
                    self.previous[metric_str] = stat['Sum']
                    self.previous_time[metric_str] = now
            except:
                print stacktrace()
                continue
    def run(self):
        """ a daemon looks for new metrics added to cloudwatch every 3 seconds
        saves the metrics in a list provided to the get_stat function
        clears all data in this list if get_stat wasn't called in timeout seconds """
        try:
            # while safe exit is not requested run these tasks
            while not self.exit_request:
                # stop on timeout
                if (self.get_second(datetime.utcnow()) - self.last_get).seconds >= self.timeout:
                    self.stop()
                    print 'TIMEOUT'
                    sleep(3) # check every 3 seconds
                    continue
                # get new metrics for this minute and add them to stats
                now = self.get_second(datetime.utcnow())
                self.init(now) # try to fill empty stats
                start = now
                end = now + timedelta(minutes=1)
                for metric_str in self.stat:
                    response = self.conn.get_metric_statistics(period=60, start_time=start, end_time=end, metric_name=metric_str, namespace='RADIUS', statistics='Sum')
                    self.add_metric(response, now, metric_str)
                sleep(3) # check every 3 seconds
        except:
            print stacktrace()
    def get_stat(self):
        """ get stats provided by the monitor thread """
        self.last_get = self.get_second(datetime.utcnow())
        return self.stat
    def exit(self):
        """ request safe exit from the monitor thread """
        print 'exiting...'
        self.exit_request = True
