from django.conf.urls import url
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html', content_type='text/html')),
    url(r'^index.html$', TemplateView.as_view(template_name='index.html', content_type='text/html')),
    url(r'^d3.min.js$', TemplateView.as_view(template_name='d3.min.js', content_type='text/javascript')),
    url(r'^jquery.min.js$', TemplateView.as_view(template_name='jquery.min.js', content_type='text/javascript')),
    url(r'^display.js$', TemplateView.as_view(template_name='display.js', content_type='text/javascript')),
    url(r'^display.css$', TemplateView.as_view(template_name='display.css', content_type='text/css')),
    url(r'^api', views.api, name='api'),
    url(r'^(?!(?:index.html|d3.min.js|jquery.min.js|display.js|display.css|api).*).*', views.not_found, name='not_found'),
]
