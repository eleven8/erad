import boto3
import json
import threading
from flask import Flask, render_template
from flask_socketio import SocketIO

app = Flask(__name__)
socketio = SocketIO(app)
started = False

client = boto3.client(
    'dynamodbstreams',
    region_name='us-west-2',
    aws_access_key_id='AKIAJ33NF7FNXRLQ3LLA',
    aws_secret_access_key='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',
    )

stream_arn = ''
for stream in client.list_streams(TableName='Erad_RadiusLog_Global')['Streams']:
    # Find the first enabled stream and get its stream_arn
    if client.describe_stream(StreamArn=stream['StreamArn'])['StreamDescription']['StreamStatus'] == 'ENABLED':
        stream_arn = stream['StreamArn']
        break

data = {}
keep = 10
def read_from_shard_iterators( shard_iterator ):
    threadId = threading.currentThread().ident
    if threadId not in data:
        data[threadId] = [[] for i in range(keep)]
    try: # catching expired shard iterators
        records = client.get_records(ShardIterator=shard_iterator)
        index = 0
        while 'NextShardIterator' in records and records['NextShardIterator']:
            shard_iterator = records['NextShardIterator']
            records = filter(lambda l: l['eventName'] == 'INSERT', records['Records'])
            if len(records) > 0:
                data[threadId][index] = [r['dynamodb']['NewImage'] for r in records]
                index = (index+1) % keep
            records = client.get_records(ShardIterator=shard_iterator)
            socketio.sleep(1)
        records = filter(lambda l: l['eventName'] == 'INSERT', records['Records'])
        if len(records) > 0:
            data[threadId][index] = [r['dynamodb']['NewImage'] for r in records]
    except:
        pass
    socketio.sleep(60)
    del(data[threadId])

def sortShards(shards):
    result = []
    order = []
    for s in shards:
        if s['ParentShardId'] in order:
            i = order.index(s['ParentShardId'])
            result = result[0:i+1] + [s] + result[i+1:]
            order = order[0:i+1] + [s['ShardId']] + order[i+1:]
        else:
            result.append(s)
            order.append(s['ShardId'])
    return result

seenShards = []
def logStream():
    shards = []
    stream = client.describe_stream(StreamArn=stream_arn)
    while 'LastEvaluatedShardId' in stream['StreamDescription']:
        shards += stream['StreamDescription']['Shards']
        stream = client.describe_stream(StreamArn=stream_arn, ExclusiveStartShardId=stream['StreamDescription']['LastEvaluatedShardId'])
    shards += stream['StreamDescription']['Shards']
    oshards = sortShards(shards)
    shard_iterators = []
    for shard in oshards:
        if 'EndingSequenceNumber' not in shard['SequenceNumberRange']:
            if shard['ShardId'] not in seenShards:
                seenShards.append(shard['ShardId'])
                shard_iterators.append(
                        client.get_shard_iterator(StreamArn=stream_arn,
                                                ShardId=shard['ShardId'],
                                                ShardIteratorType='LATEST',
                                                )['ShardIterator']
                    )
    if len(shard_iterators) > 0:
        for si in shard_iterators:
            t = threading.Thread(target=read_from_shard_iterators, args=(si,))
            t.setDaemon(True)
            t.start()

def readData():
    memoSize = 1000
    seen = [0 for i in range(memoSize)]
    index = 0
    while True:
        result = []
        for i in data:
            for j in data[i]:
                result += j
        output = []
        for i in sorted(result, key=lambda x: x['Instance']):
            if i['Instance'] not in seen:
                seen[index] = i['Instance']
                output.append(i)
                index = (index + 1) % memoSize
        if len(output) > 0:
            socketio.emit('log_stream', output)
        socketio.sleep(1)

def updateShards():
    while True:
        logStream()
        socketio.sleep(120)

@socketio.on('connect')
def test_connect():
    global started
    if not started:
        started = True
        socketio.start_background_task(readData)
        socketio.start_background_task(updateShards)

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=5000)
