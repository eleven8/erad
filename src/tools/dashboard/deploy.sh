#!/bin/bash
# install pip django and boto if not already installed
yum install -y python27-pip gcc
pip-2.7 install django boto boto3 flask flask_socketio eventlet
# copy monitoraccess files to /opt
cp -r /usr/local/src/monitoraccess /opt
cp -r /usr/local/src/log_stream /opt
# add monitoraccess.sh as a service
cp /usr/local/src/monitoraccess.sh /etc/init.d/monitoraccess
chkconfig --add monitoraccess

echo "installing apache"
yum install -y httpd24 php56 mysql55-server php56-mysqlnd
# copy add /api proxy config to apache
cp /usr/local/src/monitor.conf /etc/httpd/conf.d

echo "restarting apache"
service httpd restart

# restart the monitoraccess service
service monitoraccess restart

# link the static dashboard files to apache root (to serve by apache rather than django)
echo "linking the monitor static files to apache root"
ln -s /opt/monitoraccess/monitor/templates/* /var/www/html/

# copy UI (console launcher) to apache root
echo "copying UI to apache root"
cp -r UI /var/www/html

# attempt to decrypt console launcher config file
echo "attempting to decrypt config file"
yum -y install gpg
gpg --batch --yes --passphrase $T42_PASSPHRASE --output /var/www/html/UI/config.json --decrypt /var/www/html/UI/config.json.secure

echo "done"

