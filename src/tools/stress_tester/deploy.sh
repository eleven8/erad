#!/bin/bash

set -e
set -x


WORK_HOME="/usr/local/src"
RADCLIENT="http://s3-us-west-2.amazonaws.com/teamfortytwo.deploy/public/freeradius-server-3.0.7.tar.gz"

# install epel
amazon-linux-extras install epel -y

# Install deps
echo installing required packages....

yum update -y -q
yum install --enablerepo=epel -y -q nodejs npm git libtalloc

cd $WORK_HOME
# Get radius client
wget $RADCLIENT

# Unpack
tar -C / -xzf freeradius-server-3.0.7.tar.gz usr/local/bin/radclient usr/local/lib/libfreeradius-radius.so usr/local/share/freeradius/*

cd $WORK_HOME/app
# or... run node
npm install -q -s

node app.js
