#!/bin/bash

REPO=$(pwd)
TARBALL=app.tar.gz
BUILD=/tmp/app
APP=$HOME/stress_tester

if [ -d $BUILD ]; then
    echo Removing build directory
    rm -fr $BUILD
fi

cp -R $APP $BUILD
echo Copying node app to build dir

if [ -e /tmp/$TARBALL ]; then
    echo Found old tarball deleting    
    rm /tmp/$TARBALL 
fi

if [ -e $BUILD/.gitignore ]; then
    echo Found gitignore, deleting 
    rm $BUILD/.gitignore 
fi

if [ -e $BUILD/alex_dev_tools.sh ]; then
    echo Found alex_dev_tools.sh, deleting 
    rm $BUILD/alex_dev_tools.sh 
fi

if [ -d $BUILD/.git ]; then
    echo Removing git directory 
    rm -fr $BUILD/.git
fi

if [ -d $BUILD/app/node_modules ]; then
    echo Removing node_modules before building new tarball
    rm -fr $BUILD/app/node_modules
fi

cd $BUILD/app
echo changing in to build directory

tar czf /tmp/$TARBALL ../.
echo Making tarball in /tmp/$TARBALL

rm -fr $BUILD
echo Removing build directory
