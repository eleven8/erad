'use strict';

var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    jscs = require('gulp-jscs'),
    jasmine = require('gulp-jasmine'),
    stylish = require('gulp-jscs-stylish'),
    plumber = require('gulp-plumber');

gulp.task('test', function () {
    return gulp.src('./tests/*_test.js')
        .pipe(jasmine({
            verbose: true
        }));
});

gulp.task('lint', function () {
    return gulp.src('./*.js')
        .pipe(plumber())
        .pipe(jshint())
        .pipe(jscs({
            verbose: true
        }))
        .pipe(stylish.combineWithHintResults())   // combine with jshint results
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('watch', function () {
    gulp.watch('./*.js', ['lint']);
    gulp.watch(['./*.js', './tests/*.js'], ['lint', 'test']);
});

gulp.task('default', ['lint', 'test', 'watch']);
