var cli = require('cli').enable('status');

var cliOptions = cli.parse({
  // comes with a built in  --debug flag
  // comes with a built in  --help flag
  tries: ['s', 'Tries, ie seconds to run for', 'number' ],
  count: ['c', 'Number of packets', 'number', 1 ],
  retries: ['r', 'Number of retries', 'number', 1 ]
});

// refers to radclient -x switch for long replies
cliOptions.debug = true;

module.exports = cliOptions;
