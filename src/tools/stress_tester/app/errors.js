'use strict';

var cli = require('cli');

module.exports = function callErrorHandler (error) {
  cli.error(new Error(error));
};
