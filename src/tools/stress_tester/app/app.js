'use strict';

var cli = require('cli').enable('status');
var singleExec = require('./singleExec');
var options = require('./options');
var CommandString = require('./Command');
var createStationId = require('./writeInputFile');
var cliOptions = require('./cliOptions');
var _ = require('lodash');
var utils = require('./utils');
var scheduler = require('./scheduler');

// packets received counter
var counter = 0;

// array to collect 'no reply' replies that come in multiple 
// duplicates so we can filter easily
var errorArr = [];

// init the scheduler with an array of servers, 
// defined in the main options file
var queue = scheduler.init(options.SERVERS);

// set the required env for radclient
process.env.LD_LIBRARY_PATH = '/usr/local/lib';

// not currently in use
options.startTime = Date.now();

// main clock
var secondsElapsed = 0;

// simple calculation secondds * number of packets per second
var expected = function () {
    return secondsElapsed * (cliOptions.count);
};

function parseReplies (err, data) {
    // needs better error handling 
    if (err) {
        cli.error('Main ' + err);
        return;
    }

    // formats output and clicks on counter
    function addOnData () {
	// otherwise it starts at 0, which doesn't really work
        data['reply number'] = counter + 1; 
        cli.ok(utils.formatJsonData(JSON.stringify(data))); 
        counter++;
    }

    // this is for the error replies from radclient when it can't cope
    if (data.msg === 'No reply') {
	// push all errors in to arrary so we can de-dupe...
        errorArr.push(data);
	// via the nice lodash uniq function
        _.uniq(errorArr).forEach(addOnData);
    } else {
        addOnData(); 
    }

    cli.info('Total packets received: ' + counter + 
		' in ' + secondsElapsed + 
		' seconds. ' + expected() + 
		' expected, ' + (expected() - counter) + ' missing');
}

// the call to st up the scheduler 
function callRemote () {

    // currently the function just takes the desired number of packets
    // each second, as defined by the -c switch
    // We only call this once for each second irrespective of however many 
    // servers are defined as *presumeably* each server manages its own cache
    // and we don't need a different ID across servers. This may be an 
    // incorrect assumption, however.
    createStationId(cliOptions.count);
   
    // get the sorted array of servers to hit from scheduler
   
    // call run on scheduler with the array as arg1
    // scheduler.run simply ticks over the counters and returns the
    // callback so we can probably run this in a loop if we want, 
    // ie to do multiple calls per second (of the main loop);
    // Currently this is only a single run per second.  
    var nextServer = scheduler.filterByDiff(scheduler.processQueue(queue));
    scheduler.run(nextServer, function schedulerCallback(servers) {
	    // get a cli string to pass to radclient
	    var shellCmd = new CommandString(options, cliOptions, servers[0].IP);
		     
	    cli.debug('Scheduler string: ' + JSON.stringify(servers[0]));
	    cli.debug('Calling radclient with: ' + shellCmd.cmd);
	    
	    // call the function that handles the child process, handing replies to the callback
	    // above, 'parseReplies' 
	    singleExec(Number(cliOptions.count), shellCmd.cmd, parseReplies);
    });
    // tick the clock outside the above async call
    secondsElapsed++;
}

cli.info('Calling remote ' + cliOptions.count + ' times a second');
cli.info('Stop script with control-c ');

var repeater = setInterval(function () {
    // keep going if no '-s NUM' flag s passed on the cli or untill we hit our 
    // number limit of runs
    if (!cliOptions.tries || secondsElapsed < cliOptions.tries) {
        callRemote();
    } else {
        clearInterval(repeater);
    }
}, 1000);

