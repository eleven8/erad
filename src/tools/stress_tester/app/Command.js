'use strict';

function CommandString (opts, cliOpts, server) {
  
  // cli options from node app.js etc
  var retries = cliOpts.retries ? cliOpts.retries : 1;
  var debugFlag = cliOpts.debug ? '-x' : '';
  //var packetCount = cliOpts.count ? cliOpts.count : 1;

  // final params for radclient
  var callString = ' -D ' + opts.DICT + ' ' + server + ' ' + opts.UID + ' ' + opts.PASS;
 
  // the actual command string we return 
  this.cmd = opts.RADCLIENT + 
    ' ' + debugFlag + 
    ' -r ' + retries + 
    ' -t 15' +
    ' -f input_vars.txt' +
    // we no longer use this option when 
    // reading config from the input file
    // " -c " + packetCount + 
    ' -c 1' + 
    '' + callString;
	
  return this.cmd;
}

module.exports = CommandString;
