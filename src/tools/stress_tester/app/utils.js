'use strict';

var utils = {
	formatJsonData: function (data) {
		// for now just strip curly braces from the json object
		return data.replace(/{|}/g, '');
	}, 

	simplePluralised: function (opts, word) {
		// obviously limited pluralisation
		return opts.count >1 ? (word + 's') : word;
	}
};

module.exports = utils;
