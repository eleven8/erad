'use strict';

var cli = require('cli');
var shell = require('shelljs');
var parser = require('./parser');

var dataReqs = 0;

function doSingleRequest (requests, cmd, callback) {

  function mapReplies (err, data) {
        
    // currently no error handling in parser...
    if (err) {
      cli.error('Error returning from parser ' + JSON.stringify(err[0]));
      return callback(err[0], null);
    }
      
    callback(null, data);
  }

  var startTime = Date.now();
  var child = shell.exec(cmd, { async: true, silent: true });

  child.stdout.on('data', function(data) {
    var repl = String(data.toString());
    // remove requests param all around as it's not being used
    parser(repl, requests, startTime, mapReplies);
  });

  child.stderr.on('data', function (data) {
    if (data.indexOf('(0)') !== -1 || data.indexOf('(1)') !== -1) {
      // swallow error from radclient as we're already 
      // capturing the packet above but show it on --debug flag
      cli.debug(data); 
    }
  });

  // close event fires on each 
  child.on('close', function () {
    cli.debug('Data events: ' + dataReqs);
  });

  child.on('exit', function (code, signal) {
    if (code !== 0) {
      cli.debug(new Error('process exited with code ' + code));
      cli.debug(new Error('process exited with signal ' + signal));
    }
  });
}

module.exports = doSingleRequest;
