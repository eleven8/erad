'use strict';

var packetCount = 0; 

function sorted (a, b) {
   if (a.diff > b.diff) {
     return -1;
   }
   if (a.diff < b.diff) {
     return 1;
   }
   return 0;
}

function init(serverArray) {
  var localArr = [];
 
  serverArray.map(function (server) {
    server.count = 0;
    server.diff = server.weight;
    localArr.push(server);
  });
  
  return localArr; 
}

function filterByDiff(serverArray) {
  var queue = serverArray.sort(sorted);
  return queue; 
}

function processQueue( serverList ) {
  var mappedList = []; 
  
  serverList.map(function (server) {
    var fudgeFactor = (server.count > 0) ? (server.count/packetCount) * 100 : 0;
    server.percentage = fudgeFactor;  
    server.diff = parseInt( ( server.weight - fudgeFactor), 10);
    mappedList.push(server);
  });

  return mappedList;
}

function run(server, callback) {
  server[0].count++; 
  packetCount++;
  return callback(server);
}

var app = {
  init: init,
  filterByDiff: filterByDiff,
  processQueue:processQueue,
  sorted: sorted,
  run:run 
};

module.exports = app;

