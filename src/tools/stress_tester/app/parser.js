'use strict';

var cli = require('cli').enable('status');

function parseReply (data, numberOfRequests, startTime, callback) {

  // split string reply on newline
  var lines = data.split(/\r?\n/);
  lines.map(function (line) {
    var result = {};
    var received;
	  
    // remove tab chars 
    line = line.replace(/(\t)/g, '');

    // Parse valid replies
    if (line.slice(0,8) === 'Received') {
        cli.debug('Raw response: ' + line);
	var splitOnSpace = line.split(' ');
        result.msg = 'Recieved ' + splitOnSpace[1];
        result.id = parseInt(splitOnSpace[3], 10); 
        received = Date.now();
        result.responseTime = received - startTime + 'ms';
    // parse no replies from radclient
    } else if (line.slice(0,12) === '(0) No reply') {
        //result.msg = lines[0]; 
        result.msg = 'No reply';
        result.id = parseInt(line.split(' ')[7], 10);
        received = Date.now();
        result.responseTime = received - startTime + 'ms';
    } else {
        return;
    }

    callback(null, result);
  
  });

}

module.exports = parseReply;
