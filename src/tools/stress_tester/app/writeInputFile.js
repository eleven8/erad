'use strict';

var options = require('./options');
var fs = require('fs');

var output = [];
var lastVal = 0;

function createStationId (num) {
    // this should really be part of the Command class 
    var str = String(num);

    while (str.length < 12) {
        str = '0' + str;
    }
    var retStr = str.replace(/(..)(..)(..)(..)(..)(..)/, '$1-$2-$3-$4-$5-$6');

    // vars we write to config file 
    var INPUT_VARS = 'User-Name=' + options.INPUT_USERNAME + 
      ',User-Password=' + options.INPUT_PASSWORD + 
      ',Calling-Station-Id=' + retStr +
      ',Called-Station-Id=' + options.CALLED_STATION_ID;
    
    return INPUT_VARS;
}

function main (multi) {
  
  if (multi) {
    // use a simple loop 
    for (var i = 0; i < multi; i++) {
        // but calculate starting value/offset here
        output.push(createStationId(lastVal + i));
    } 
  }

  // increment to last used ID 
  lastVal += multi;

  // sync should be fine here
  fs.writeFileSync(__dirname + '/input_vars.txt', output.join('\n\n'));     

  // reset array
  output = [];
}

module.exports = main;
