var options = require('../options.js');

describe("App options file", function() {
	it("Will be present", function() {
	    expect(options).toBeDefined();
	});

	describe("INPUT_USERNAME key", function() {
	    it('Will have a value for the username to pass to radclient', function () {
		expect(options.INPUT_USERNAME).toBeDefined();
	    });

	    it('Will have a type of "string"', function () {
		expect(typeof options.INPUT_USERNAME).toBe('string');
	    });
	});

	describe("INPUT_PASSWORD key", function() {
	    it('Will have a value for the password to pass to radclient', function () {
		expect(options.INPUT_PASSWORD).toBeDefined();
	    });
	    it('Will have a type of "string"', function () {
		expect(typeof options.INPUT_PASSWORD).toBe('string');
	    });
	});

	describe("CALLED_STATION_ID key", function() {
	    it('Will have a value for the called-station-id to pass to radclient', function () {
		expect(options.CALLED_STATION_ID).toBeDefined();
	    });
	    it('Will have a type of "string"', function () {
		expect(typeof options.CALLED_STATION_ID).toBe('string');
	    });
	    it('Will be composed of 12 digits based on six two digit pairs joined with a "-"', function () {
		expect(options.CALLED_STATION_ID.split('-').join('').length).toEqual(12);
	    });

	});

	describe("SERVER key", function() {
	    it('Will have a value for the server or servers to pass to radclient', function () {
		expect(options.SERVERS).toBeDefined();
	    });
	    it('Will have a type of Array', function () {
		expect(typeof options.SERVERS).not.toBe('string');
		expect(Array.isArray(options.SERVERS)).toBe(true);
		expect(options.SERVERS.length).not.toBeLessThan(1);
		expect(options.SERVERS[0]).toBeDefined();
	    });
	    it('Object will have an IP key', function () {
		expect(options.SERVERS[0].IP).not.toBe(null);
		expect(typeof options.SERVERS[0].IP).toBe('string');
	    });
	    it('Object will have a "weight" key of type "number"', function () {
		expect(options.SERVERS[0].weight).not.toBe(null);
		expect(typeof options.SERVERS[0].weight).toBe('number');
	    });
	    it('Object will have a "weight" key between 0 - 100', function () {
		expect(options.SERVERS[0].weight).toBeLessThan(100);
		expect(options.SERVERS[0].weight).toBeGreaterThan(0);
	    });
	    it('Weightings of all servers will equal 100', function () {
		var totalWeighting = 0;
		options.SERVERS.forEach(function (item, count) {
			totalWeighting += item.weight;
		});
		expect(totalWeighting).toEqual(100);
	    });

	});

	describe("UID key", function() {
	    it('Will have a value for the UID to pass to radclient', function () {
		expect(options.UID).toBeDefined();
	    });
	    it('Will have a type of "string"', function () {
		expect(typeof options.UID).toBe('string');
	    });
	});

	describe("PASS key", function() {
	    it('Will have a value for the password to pass to radclient', function () {
		expect(options.PASS).toBeDefined();
	    });
	    it('Will have a type of "string"', function () {
		expect(typeof options.PASS).toBe('string');
	    });
	});

	describe("RADCLIET key", function() {
	    it('Will have string that represents the binary location of radclient', function () {
		expect(options.RADCLIENT).toBeDefined();
	    });
	    it('Will have a type of "string"', function () {
		expect(typeof options.RADCLIENT).toBe('string');
	    });
	});

	describe("DICT key", function() {
	    it('Will have a avlue for the dictionary location directory to pass to radclient', function () {
		expect(options.DICT).toBeDefined();
	    });
	});
});
