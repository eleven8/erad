README
======

Manual Run
----------

* Install dependencies [nodejs, npm, git, libtalloc] via yum, system package manager or build_deps.sh script.

* Install radclient (see build_deps.sh for how-to or just run script).

``` bash
	cd app
	npm install
```

* Run script via

``` bash
	node app.js
```

* Script accepts -c switch that determines number of packets sent via radclient, defaults to 1 per second.

* Terminate script via control-c

Build Tarball
-------------

- Run ./pack_tarball.sh to make tarball app.tar.gz in /tmp 