import boto3
import parser
from concurrent.futures import ThreadPoolExecutor, wait
import time
import argparse

client = boto3.client('dynamodb')

def get_data(source_table_name, ExclusiveStartKey=None):
    if ExclusiveStartKey:
        data = client.scan(TableName=source_table_name, ExclusiveStartKey=ExclusiveStartKey)
    else:
        data = client.scan(TableName=source_table_name)
    return data


def put_data(target_table_name, data):
    put_requests = []
    for item in data:
        put_requests.append({'PutRequest': {'Item': item}})
    respose = client.batch_write_item(RequestItems={target_table_name: put_requests})
    return respose


def put_data_concurrently(target_table_name, data, pool):
    begin = 0
    while begin <= len(data):
        future = pool.submit(put_data, target_table_name, data[begin:begin+25])
        wait([future])
        if future.done():
            # print(future.result())
            while future.result().get('UnprocessedItems'):
                future = pool.submit(put_data, target_table_name, future.result().get('UnprocessedItems'))
            begin += 25


def main():
    start_time = time.time()

    parser = argparse.ArgumentParser(description="Dynamodb table to table copy")
    parser.add_argument('-s', '--sourceTable', help='Source dynamodb table')
    parser.add_argument('-t', '--targetTable', help='Target dynamodb table')
    args = parser.parse_args()

    pool = ThreadPoolExecutor(5)
    data = get_data(source_table_name=args.sourceTable)
    put_data_concurrently(args.targetTable, data['Items'], pool)
    while data.get('LastEvaluatedKey'):
        data = get_data(source_table_name=args.sourceTable, ExclusiveStartKey=data.get('LastEvaluatedKey'))
        put_data_concurrently(args.targetTable, data['Items'], pool)
    print("Database copied.")
    print("Time elapsed: {:.2f}".format(time.time()-start_time))



if __name__ == '__main__':
    main()

# put_data('Erad_Endpoint_Global', data=get_data(table_name='Erad_Endpoint')['Items'])