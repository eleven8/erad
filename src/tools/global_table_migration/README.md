## Dynamodb table to table copy

To run the script you need Python 3. 
Create an environment with virtualenv (`virtualenv -p python3 venv`) and activate.
Install dependencies with `pip install -r requirements.txt`.


### Steps

1. Create a global table with identical schema of the source table. Increase **Read Capacity** of the source table and increase **Write Capacity** of the target table.


2. Create an ec2 instance. ( I used an Ubuntu Server 16.04 LTS - ami-965e6bf3, t3.micro instance)
3. Create a file `~/.aws/credentials` with aws credentials in it. 

```
    [default]
    aws_access_key_id = <aws access key>
    aws_secret_access_key = <AWS secret access key>
    region = <region, i.e. us-east-2>
```
    
4. Update the machine: `sudo apt update`. 
5. Install pip: `sudo apt install python-pip`.
6. Install virtualenv: `sudo pip install virtualenv`.
7. Clone the erad repo: `hg clone https://<username>@bitbucket.org/teamfortytwo/erad -r global-table-migration`.
8. Change directory to `erad/src/tools/global_table_migration`.
9. Create a virtualenv: `virtualenv -p python3 venv` and activate `source venv/bin/activate`.
10. Install all the dependencies: `pip install -r requirements.txt`.
11. Final step is to run the migration script. 
	`python table_to_table_copy.py -s <source table> -t <target global table>`
    For example:
	`python table_to_table_copy.py -s Erad_Endpoint -t Erad_Endpoint_Global`

#### Usage

```
usage: table_to_table_copy.py [-h] [-s SOURCETABLE] [-t TARGETTABLE]

Dynamodb table to table copy

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCETABLE, --sourceTable SOURCETABLE
                        Source dynamodb table
  -t TARGETTABLE, --targetTable TARGETTABLE
                        Target dynamodb table
```
