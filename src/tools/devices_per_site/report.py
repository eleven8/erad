from erad.api.erad_model import Erad_Group2
from erad.api.interface_admin import EradInterfaceAdmin
from datetime import timedelta
import argparse
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def get_groups_index(rate_limit):
    groups_index = set()

    for group in Erad_Group2.scan(rate_limit=rate_limit):
        groups_index.add((group.Account_ID, group.ID))

    return sorted(groups_index)


def one_group(account_id, group_id, rate_limit):
    """ Return result only for one group

    :param account_id:
    :param group_id:
    :param rate_limit:
    :return:
    """
    logger.info(f'{(account_id, group_id)}')
    admin = EradInterfaceAdmin()
    group = (account_id, group_id)
    get_group_stats(admin, group, rate_limit)


def starts_with_group(account_id, group_id, rate_limit):
    """Sort list of groups and skipp all groups prior (account_id, group_id).
    Return data for not skipped groups


    :param account_id:
    :param group_id:
    :param rate_limit:
    :return:
    """
    logger.info(f'{(account_id, group_id)}')

    admin = EradInterfaceAdmin()

    skipped = False
    for group in get_groups_index(rate_limit):
        # skipping service site which is used only for tests
        if group == ('eradtests', 'erad-tests'):
            continue
        # skipping processed sites
        if group != (account_id, group_id) and skipped is False:
            continue

        skipped = True

        get_group_stats(admin, group, rate_limit)


def iterate_all(rate_limit):
    """Scan all groups except ('eradtests', 'erad-tests')

    :param rate_limit:
    :return:
    """

    admin = EradInterfaceAdmin()

    for group in get_groups_index(rate_limit):
        # skipping service site which is used only for tests
        if group == ('eradtests', 'erad-tests'):
            continue

        get_group_stats(admin, group, rate_limit)


def get_group_stats(admin, group, rate_limit):
    logger.debug(group)

    unique_devices= admin.get_unique_devices_count(*group, timedelta(days=30), rate_limit)
    print(f'{"::".join(group):<50}, {unique_devices}')


def main():
    parser = argparse.ArgumentParser(
        description='Get number of unique devices (Calling-Stationg-ID) per site for last 30 days')
    parser.add_argument(
        'rate_limit',
        metavar='rate-limit',
        type=int,
        help='AWS DynamoDB Read Capacity Units limit',
        default=20
    )
    parser.add_argument(
        '--starts-with',
        metavar='group_id',
        type=str,
        help='Specify group id for with report should start working, example `elevenos::test`')

    parser.add_argument(
        '--only',
        metavar='group_id',
        type=str,
        help='Specify one group id that will be processed, example `elevenos::test`')

    args = parser.parse_args()

    logger.info(f'{args.rate_limit}')

    if args.only is not None:
        account_id, group_id = args.only.split('::')
        one_group(account_id, group_id, args.rate_limit)

    elif args.starts_with is not None:
        account_id, group_id = args.starts_with.split('::')
        starts_with_group(account_id, group_id, args.rate_limit)

    else:
        iterate_all(args.rate_limit)


if __name__ == '__main__':
    main()

