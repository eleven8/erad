exports.timeout = function timeout (msg) {
    console.log(msg);
};

exports.login = function login () {
    casper.fill('#loginForm', {
        'txtUserName': config['username'],
        'txtPassword': config['password'],
        'txtRealm': config['realm'],
        '__EVENTTARGET': 'ImageButton1',
        '__EVENTARGUMENT': '',
        '__VIEWSTATE': '/wEPDwULLTExMjYyNjIzMzcPZBYCAgEPFgIeBmFjdGlvbgVcaHR0cHM6Ly9zZWN1cmUuZWxldmVud2lyZWxlc3MuY29tL0VsZXZlbk9TL2FkbWluL2xvZ2luL2xvZ2luLmFzcHg/UmV0dXJuVXJsPS9FbGV2ZW5PUy9BZG1pbi9kZNBu88mEn4VtHMC85BC8GHblGOMX'
    }, true);
};
