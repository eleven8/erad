var fs = require('fs');
var config = JSON.parse(fs.read('./config.json'));

var myUtils = require('../_utils');

casper.test.begin('Test #1 - Set Remote-authentication', 5, function suite(test) {

   casper.start(config['url'], myUtils.login);

   casper.waitForUrl(config['startPage'], function () {

        casper.wait(10000);

        casper.thenClick('.edit-property');

        casper.wait(5000, function () {
            test.assertExists('.modal-title', 'Edit modal opened OK');
            test.assertEval(function() {
                return __utils__.findOne('.modal-title').textContent === 'Edit Site Configuration';
            }, 'Modal title is correct');
        });

        casper.thenClick('.dropdown-subclass a');
                
        casper.waitForSelector('.dropdown-menu', function () {
            this.click('.dropdown-menu li:first-of-type');
            test.assertEval(function() {
                return __utils__.findOne('.dropdown-subclass a span').textContent.trim() === 'ElevenOS';
            }, 'Remote-authentication value changed to "ElevenOS"');
        });

        casper.then(function () {
            this.clickLabel('Update', 'a');            
        });

        casper.wait(5000);

        casper.thenClick('.edit-property');

        casper.wait(5000, function () {
            test.assertExists('.modal-title', 'Modal opens again OK after update');
            test.assertEval(function() {
                return __utils__.findOne('.dropdown-subclass a span').textContent.trim() === 'ElevenOS';                
            }, '"ElevenOS" is still the selected value');
        });

        // close modal?
        casper.thenClick('.modal-footer a:first-of-type');           

    }, myUtils.timeout, 15000);

    casper.run(function end () {
        test.done();
    });
});

casper.test.begin('Test #2 - Reset Remote-authentication', 6, function suite(test) {

   casper.start(config['url'], myUtils.login);

   casper.waitForUrl(config['startPage'], function () {

        casper.wait(5000);

        casper.thenClick('.edit-property');

        casper.wait(5000, function () {
            test.assertExists('.modal-title', 'Modal opens again OK');
            test.assertEval(function() {
                return __utils__.findOne('.modal-title').textContent === 'Edit Site Configuration';
            }, 'Modal title is correct');
            test.assertEval(function() {
                return __utils__.findOne('.dropdown-subclass a span').textContent.trim() === 'ElevenOS';
            }, '"ElevenOS" is still the selected value');
        });

        casper.thenClick('.dropdown-subclass a');

        casper.waitForSelector('.dropdown-menu', function () {
            this.click('.dropdown-menu li:nth-of-type(3)');
            test.assertEval(function() {
                return __utils__.findOne('.dropdown-subclass a span').textContent.trim() === 'None';
            }, 'Remote-authentication value changed to "None"');
        });

        casper.then(function () {
            this.clickLabel('Update', 'a');            
        });

        casper.wait(5000);

        casper.thenClick('.edit-property');

        casper.wait(5000, function () {
            test.assertExists('.modal-title', 'Modal opens again OK after update to "None"');
            test.assertEval(function() {
                return __utils__.findOne('.dropdown-subclass span').textContent === 'None';                
            }, '"None" is still the selected value');
        });

    }, myUtils.timeout, 15000);

    casper.run(function end () {
        test.done();
    });
});

