var fs = require('fs');
var config = JSON.parse(fs.read('./config.json'));

var myUtils = require('../_utils');

// Test #1 - Verify that a user can login and logout of the system.
// * Log in
// * Verify that ERAD is loaded 
// * Try to read the Group_ID (DM-415-09) from the page.
// * Click the logout link
// * Verify the browser returns to secure.elevenwireless.com.
casper.test.begin('Test #1 - ERAD loads ok', 3, function suite(test) {

    casper.start(config['url'], myUtils.login);

    casper.waitForUrl('https://admin.enterpriseauth.com/#/groups', function () {
        
        casper.wait(15000, function () {
            test.assertExists('.group-id', 'Edit site link exists');
            test.assertEval(function() {
                return __utils__.findOne('.group-id').textContent === 'DM-415-09';
            }, 'Selected group is correct');
        });
           
        casper.wait(5000, function () { 
            casper.evaluate(function (){
               $('#logout').click();
            });
        });

        casper.wait(5000, function () {
            var testedUrl = casper.getCurrentUrl().split('?')[0].toLowerCase();

            test.assertEqual(config['url'].toLowerCase(), testedUrl, 'URL is correct');
        });

    }, myUtils.timeout, 15000);

    casper.run(function() {
        test.done();
    });
});

casper.test.begin('Test #2 - Add Device', 4, function suite(test) {

    casper.start(config['url'], myUtils.login);

    casper.waitForUrl('https://admin.enterpriseauth.com/#/groups', function () {

        casper.wait(15000, function () {

            // just to check there are not any 'lingering' devices from failed run
            test.assertDoesntExist('tbody tr', 'No devices in table');
            
            casper.click('#btnAddDevice');

            casper.waitForSelector('.modal-footer', function () {
                casper.fill('form[name="userForm"]', {
                    'username': config['testUsername'],
                    'password': config['testPassword']
                }, false);

                casper.wait(5000, function () {
                    test.assertExists('#btnCreateUser', 'Create button exists');
                    casper.click('a#btnCreateUser');
                    casper.wait(15000, function () {
                        test.assertExists('tbody tr', 'Device added to table ok');
                        test.assertSelectorHasText('tbody tr:nth-child(1) td[data-title-text="Username/MAC"]', config['testUsername'].toLowerCase(), 'Device has correct username');
                    });
                });
            });
        });

    }, myUtils.timeout, 15000);
    
    casper.run(function() {
        test.done();
    });
});

casper.test.begin('Test #3 - Remove Device', 1, function suite(test) {

    casper.start(config['url'], myUtils.login);

    casper.waitForUrl('https://admin.enterpriseauth.com/#/groups', function () {

        casper.wait(15000, function () {
            casper.click('tbody tr:nth-child(1) td[data-title-text="Actions"] a:nth-child(2)');
            casper.waitForSelector(".modal-footer", function () {
                casper.click('a#btnDeleteUser');
                casper.wait(5000, function () {
                    test.assertDoesntExist('tbody tr:nth-child(1)', 'Device deleted OK');
                });
            });
        });

    }, myUtils.timeout, 15000);
    
    casper.run(function() {
        test.done();
    });
});