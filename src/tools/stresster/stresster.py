#!/usr/bin/python

import apscheduler.events
from apscheduler.schedulers.background import BackgroundScheduler
import logging
import optparse
import os
import subprocess
import sys
import time
import threading


# Triggers a thread for each request
def trigger(threads, params):
    global ticks

    ticks += 1

    # print "[" + str(time.clock()) + "] Cycle #" + str(ticks)

    for i in range(threads):
        t = threading.Thread(target=radius, args=(i, params))
        t.start()


# Call the bash script that calls the radclient
def radius(id_thread, credentials):
    global threads_output

    if credentials is None:
        cred = {
            'server':       '52.26.34.201',     # bash var $1
            'secret':       'ElevenRules',       # bash var $2
            'username':     'testing',           # bash var $3
            'password':     'testing',           # bash var $4
            'stationId':    '11-22-33-44-55-66'  # bash var $5
        }
    else:
        cred = {
            'server':       credentials[0],  # bash var $1
            'secret':       credentials[1],  # bash var $2
            'username':     credentials[2],  # bash var $3
            'password':     credentials[3],  # bash var $4
            'stationId':    credentials[4]   # bash var $5
        }

    p1 = subprocess.Popen(
        [
            "echo",
            "User-Name=" + cred['username'] + ",User-Password=" + cred['password'] + ",Called-Station-Id=" + cred['stationId']
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    p2 = subprocess.Popen(
        [
            "/usr/local/bin/radclient",
            "-x",
            "-r",
            "1",
            "-D",
            "/usr/local/share/freeradius/",
            cred['server'],
            "auth",
            cred['secret']
        ],
        stdin=p1.stdout,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )

    stdout, stderr = p2.communicate()

    if not stderr:
        access = 'Accept'
        fail = 0
    else:
        access = 'Reject'
        fail = 1

    # Add to list for count number of fails
    threads_output.append(fail)

    # print "\t[" + str(time.clock()) + "] Thread #" + str(id_thread) + " :: " + access


# Parse script command line arguments
def parse_args(argv):
    parser = optparse.OptionParser(
        description='Stress tester script to RADIUS auth server',
        usage='Usage: %prog [option] [value]'
    )

    parser.add_option(
        '-d',
        '--duration',
        help='Set stress script duration (in minutes) [default: 0 - infinite]',
        dest='duration',
        type='int',
        default=0,
        action='store'

    )

    parser.add_option(
        '-r',
        '--requests',
        help='Set number of requests per second [default: 1 req/s]',
        dest='requests',
        type='int',
        default=1,
        action='store'
    )

    parser.add_option(
        '-c',
        help='Set the RADIUS credentials',
        dest='config',
        action='store',
        metavar='<SERVER> <SECRET> <USER> <PASSWORD> <STATION-ID>',
        nargs=5
    )

    (opts, args) = parser.parse_args()

    # Convert instance type to dict
    opts = opts.__dict__

    return opts


def results():
    global requests
    global ticks

    print "\r\n"

    # Wait a little to get threads done
    print '\rWaiting for threads...\n'
    time.sleep(2)

    total_requests = requests * ticks

    # Calc Accept and Reject Rate
    accept_counter = threads_output.count(0)
    reject_counter = threads_output.count(1)

    accept_rate = (accept_counter * 100) / total_requests
    reject_rate = (reject_counter * 100) / total_requests

    print "\rTotal requests: " + str(total_requests)
    print "\r\tAccept: " + str(accept_rate) + '% (' + str(accept_counter) + ')'
    print "\r\tReject: " + str(reject_rate) + '% (' + str(reject_counter) + ')'


# Event Handler
def shutdown_handler(event):
    results()


# Main
if __name__ == '__main__':
    logging.basicConfig()

    # Get script arguments
    options = parse_args(sys.argv[1:])

    config = ''

    # Verify if user set RADIUS credentials
    if 'config' in options.keys() is not None:
        config = options['config']

    requests = options['requests']       # Requests per second
    duration = options['duration'] * 60  # Convert 'duration' to seconds

    ticks = 0                            # Current step duration
    threads_output = []                  # Store the return value from threads

    scheduler = BackgroundScheduler()

    # Dispatch a job event each second
    scheduler.add_job(
        trigger,
        'interval',
        seconds=1,
        args=[requests, config]
    )

    scheduler.add_listener(shutdown_handler, apscheduler.events.EVENT_SCHEDULER_SHUTDOWN)

    scheduler.start()

    print('\r\nPress Ctrl+{0} to stop, show results and exit'.format('Break' if os.name == 'nt' else 'C'))
    print "\r\n[" + str(requests) + " req/s]"

    try:

        while True:
            if duration > 0 and duration == ticks:
                scheduler.remove_all_jobs()
                sys.exit()

    except (KeyboardInterrupt, SystemExit):
        # Wait 'till the current job is done
        scheduler.shutdown()

    sys.exit(0)
