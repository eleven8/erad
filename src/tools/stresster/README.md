Usage: stresster.py [option] [value]

Stress tester script to RADIUS auth server

Options:
  -h, --help            show this help message and exit
  -d DURATION, --duration=DURATION
                        Set stress script duration (in minutes) [default: 0 -
                        infinite]
  -r REQUESTS, --requests=REQUESTS
                        Set number of requests per second [default: 1 req/s]
  -c <SERVER> <SECRET> <USER> <PASSWORD> <STATION-ID>
                        Set the RADIUS credentials
