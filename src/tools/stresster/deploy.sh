#!/bin/bash

set -e


# install epel
amazon-linux-extras install epel -y

echo "Installing dependencies..."
yum --enablerepo=epel -y install libtalloc python-pip
pip install APScheduler
wget http://s3-us-west-2.amazonaws.com/teamfortytwo.deploy/public/freeradius-server-3.0.7.tar.gz
tar -C / -xzvf freeradius-server-3.0.7.tar.gz usr/local/bin/radclient usr/local/lib/libfreeradius-radius.so usr/local/share/freeradius/*

echo "Moving files to /opt/stresster..."
mkdir /opt/stresster
mv /usr/local/src/stresster.py /opt/stresster

echo "Configuring permissions..."
chmod 755 /opt/stresster/stresster.py
chown -R root:root /opt/stresster


echo "Changing current folder..."
cd /opt/stresster

echo "Cleaning files..."
rm -rf /usr/local/src/deploy.sh

echo "Done!"
