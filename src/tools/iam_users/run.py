from iamusers import iamusers

if __name__ == '__main__':
    iamusers.setup()

    iamusers.main()

    exit()
