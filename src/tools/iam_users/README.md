## Script usage


### Installation
To install script, you just need to move all files to target folder
and install its dependencies using the following command:

<pre>pip install -r requirements.txt</pre>


### Configuration
First, you need to set the following environment variables:

export AWS_ACCESS_KEY_ID=<fake>
export AWS_SECRET_ACCESS_KEY=<fake>
export AWS_DEFAULT_REGION=<fake>

# S3 bucket to store iam files
export AWS_BUCKET=<fake>

# local directory in which tool will sotre iam files, on Jenskins it should be equal swomething like ``$WORKSPACE/iam``
export REPO_PATH=$(pwd)/iam


#### Logging

You can edit the log configuration using logging.yaml file.


### Run

To run script, you just need to use the following command:

<pre>python run.py</pre>
