import os

import iamusers.log as log
import iamusers.util as util
from iamusers.bucket import AWSBucketUpload
from iamusers.role import Role
from iamusers.user import User

logger = log.get_logger(__name__)


# Warming up...
def setup():
    log.setup_logging()


# Where the magic begin.
def main():
    directory_path = os.getenv(
        'REPO_PATH',  # ENV var name
        '/usr/local/src/erad/aws/iam/'  # Default value
    )
    bucket = os.getenv('AWS_BUCKET')
    if not bucket:
        raise ValueError('Required AWS_BUCKET environment variable to be set')

    logger.info('Getting user data...')

    users = [
        User(user)
        for user in util.list_iam_users()
    ]
    users += [Role(role) for role in util.list_iam_roles()]

    logger.info('Removing old files...')
    util.clear_directory(directory_path)

    logger.info('Saving user data...')
    for user in users:
        kind, name = (
                type(user) is User
                and ('user', user.username)
                or ('role', user.rolename)
        )
        filename = 'iam.%s.%s.txt' % (kind, name)

        util.write_to_file(os.path.join(directory_path, filename), user.to_json())

    logger.info('Syncing with aws bucket...')

    AWSBucketUpload(bucket).sync_directory_to_bucket(directory_path)

    logger.info('User settings updated successfully.')
