import json

import boto3


# Class to serve as adapter for boto3 IAM.User()
class User(object):

    def __init__(self, username):

        self.username = username

    def to_json(self):
        return json.dumps(self.data)

    @property
    def data(self):

        try:
            user = boto3.resource('iam').User(self.username)

        except Exception:
            raise Exception("Could not find user %s." % self.username)

        data = {
            'username': self.username,
            'access_keys': User.get_access_keys(user),
            'attached_policies': User.get_attached_policies(user),
            'groups': User.get_groups(user),
            'inline_policies': User.get_inline_policies(user),
            'mfa_devices': User.get_mfa_devices(user),
            'signing_certificates': User.get_signing_certificates(user)
        }

        return data

    @staticmethod
    def get_attached_policies(resource):

        attached_policies = [
            {
                'policy_name': policy.policy_name,
                'policy_id': policy.policy_id,
                'attachment_count': policy.attachment_count,
                'create_date': str(policy.create_date),
                'update_date': str(policy.update_date),
                'default_version_id': policy.default_version_id,
                'document': policy.default_version.document,
                'description': policy.description,
                'path': policy.path

            }
            for policy in resource.attached_policies.all()
        ]

        return attached_policies

    @staticmethod
    def get_access_keys(resource):

        access_keys = [
            {
                'id': key.id,
                'status': key.status
            }
            for key in resource.access_keys.all()
        ]

        return access_keys

    @staticmethod
    def get_groups(resource):

        groups = [
            {
                'group_id': group.group_id,
                'group_name': group.group_name,
                'path': group.path,
                'attached_policies': User.get_attached_policies(group),
                'inline_policies': User.get_inline_policies(group)
            }
            for group in resource.groups.all()
        ]

        return groups

    @staticmethod
    def get_inline_policies(resource):

        inline_policies = [
            {
                'policy_name': policy.policy_name,
                'policy_document': policy.policy_document
            }
            for policy in resource.policies.all()
        ]

        return inline_policies

    @staticmethod
    def get_mfa_devices(resource):
        mfa_devices = [
            {
                'serial_number': mfa_device.serial_number,
                'enable_date': str(mfa_device.enable_date)
            }
            for mfa_device in resource.mfa_devices.all()
        ]

        return mfa_devices

    @staticmethod
    def get_signing_certificates(resource):

        signing_certificates = [
            {
                'certificate_body': certificate.certificate_body,
                'certificate_id': certificate.certificate_id,
                'status': certificate.status,
                'upload_date': str(certificate.upload_date)
            }
            for certificate in resource.signing_certificates.all()
        ]

        return signing_certificates
