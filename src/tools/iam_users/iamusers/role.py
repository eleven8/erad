import json

import boto3


# Class to serve as adapter for boto3 IAM.Role()
class Role(object):

    def __init__(self, rolename):

        self.rolename = rolename

    def to_json(self):
        return json.dumps(self.data)

    @property
    def data(self):

        try:
            role = boto3.resource('iam').Role(self.rolename)

        except Exception:
            raise Exception("Could not find role %s." % self.rolename)

        data = {
            'rolename': self.rolename,
            'attached_policies': Role.get_attached_policies(role),
            'inline_policies': Role.get_inline_policies(role),
            'instance_profiles': Role.get_instance_profiles(role),
        }

        return data

    @staticmethod
    def get_attached_policies(resource):

        attached_policies = [
            {
                'policy_name': policy.policy_name,
                'policy_id': policy.policy_id,
                'attachment_count': policy.attachment_count,
                'create_date': str(policy.create_date),
                'update_date': str(policy.update_date),
                'default_version_id': policy.default_version_id,
                'document': policy.default_version.document,
                'description': policy.description,
                'path': policy.path

            }
            for policy in resource.attached_policies.all()
        ]

        return attached_policies

    @staticmethod
    def get_inline_policies(resource):

        inline_policies = [
            {
                'policy_name': policy.policy_name,
                'policy_document': policy.policy_document
            }
            for policy in resource.policies.all()
        ]

        return inline_policies

    @staticmethod
    def get_instance_profiles(resource):
        fields = [
            'arn',
            'create_date',
            'instance_profile_id',
            'instance_profile_name',
            'path',
            'roles_attribute',
        ]
        instance_profiles = []
        for policy in resource.instance_profiles.all():
            elem = {}
            for field in fields:
                elem[field] = str(getattr(policy, field, ''))
            instance_profiles.append(elem)

        return instance_profiles
