import os

import boto3

import iamusers.log as log

logger = log.get_logger(__name__)


class AWSBucketUpload:
    def __init__(self, bucket):
        self.client = boto3.client('s3')
        self.bucket = bucket

    def sync_directory_to_bucket(self, path):
        self._upload_files(path)
        self._remove_deleted_files(path)

    def _upload_files(self, path):
        for filename in os.listdir(path):
            logger.info('Uploading %s file...' % filename)
            self.client.upload_file(os.path.join(path, filename), self.bucket, filename)

    def _remove_deleted_files(self, path):
        on_bucket = {name['Key'] for name in self.client.list_objects_v2(Bucket=self.bucket)['Contents']}
        on_disk = set(os.listdir(path))
        to_delete = on_bucket - on_disk
        if to_delete:
            logger.info('Deleting files: [%s]...' % ', '.join(to_delete))
            self.client.delete_objects(
                Bucket=self.bucket,
                Delete={
                    'Objects': [{'Key': name} for name in to_delete],
                }
            )
        else:
            logger.info('No files to delete')
