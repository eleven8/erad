import errno
import glob
import os

import boto3

import iamusers.log as log

logger = log.get_logger(__name__)


# Get username from all available IAM users.
def list_iam_users():
    users_list = boto3.client('iam').list_users()

    return [
        user['UserName']
        for user in users_list['Users']
    ]


# Get rolename from all available IAM roles.
def list_iam_roles():
    roles_list = boto3.client('iam').list_roles()

    return [
        role['RoleName']
        for role in roles_list['Roles']
    ]


def clear_directory(path):
    files = glob.glob(f"{path}/*")
    for f in files:
        os.remove(f)


# Write some data to target file.
def write_to_file(filename, data):
    # If folder doesn't exists...
    if not os.path.exists(os.path.dirname(filename)):
        try:
            # ... try to create it.
            os.makedirs(os.path.dirname(filename))

        # Raise exception to unknown errors.
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise Exception('File creation failed.')

    # Write to file
    with open(filename, 'w+') as f:
        logger.info('Writing %s file...' % filename)
        f.write(data)
