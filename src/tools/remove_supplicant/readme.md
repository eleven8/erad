## How this tool works

0. Use python3, and install requirements `pip install -r ./requirements.txt`
1. Set env variable `AWS_PRODUCTION_ENV=True`, if it is not set program will use localhost DynamoDB endpoint
2. Program will use AWS credentials from standard places like ``~/.aws/credentials` or evn variables
3. List in file regular expression one per line that will be used to skip supplicants
4. By default program only create 2 files `list_to_process` and `excluded` and will not change any data in DB. Program only require to specify group id. Use this command to do it

```bash
python remove_supplicant.py --group-id 'foo_account_id::bar_group_id'
```

5. Check created files to make sure it contains expected list of supplicants

6. Run program with additional option `--action remove`, to remove supplicants. Example:

```bash
python remove_supplicant.py --group-id 'foo_account_id::bar_group_id' --action remove
```

Use `python remove_supplicant.py -h` to see all available options

```
usage: remove_supplicant.py [-h] --group-id <group-id:str>
                            [--action {print,remove}]
                            [--path-to-exclude-file <path-to-exclude-file:str>]

Tool to remove supplicants

optional arguments:
  -h, --help            show this help message and exit
  --action {print,remove}
                        Action that will be applied executed (default: print)
  --path-to-exclude-file <path-to-exclude-file:str>
                        Path to file that contains regexp to exclude
                        supplicants from processing (default: ./exclude-
                        patterns.txt)

required params:
  --group-id <group-id:str>
                        Full group id - foo_account_id::bar_group_id (default:
                        None)

```
