import setuptools

setuptools.setup(
	name="remove_supplicant",
	version="0.1",
	author="",
	author_email="",
	description="Utility to ERAD supplicants",
	url="",
	install_requires=[
		'boto3'
	],
	py_modules=['remove_supplicant'],
	classifiers=(
		"Programming Language :: Python :: 3",
		"Operating System :: Tested only on Linux",
	),
)
