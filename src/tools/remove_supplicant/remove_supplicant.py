import argparse
import logging
import re
from collections import namedtuple
from os import environ
from os import getcwd
from os import path

import boto3

LOGGER_FORMAT = '%(asctime)-15s:%(name)s : line# %(lineno)s : %(message)s'
numeric_level = getattr(logging, environ.get('LOGLEVEL', 'INFO'), None)

logging.basicConfig(
	level=numeric_level,
	format=LOGGER_FORMAT
)

logger = logging.getLogger(__name__)

Supplicant = namedtuple('Supplicant', ['group_id', 'supplicant_username'])


class SupplicantRemover:
	def __init__(self, input_args):
		self.radius_payload = 'User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e'
		if environ.get('AWS_PRODUCTION_ENV') == 'True':
			self.base_config = {
				'region_name': environ.get('AWS_DEFAULT_REGION', 'us-west-2'),
			}
		else:
			self.base_config = {
				'endpoint_url': 'http://localhost:8000',
				'region_name': environ.get('AWS_DEFAULT_REGION', 'us-west-2'),
				'aws_access_key_id': environ.get('AWS_ACCESS_KEY_ID', 'localhost-aws-access-key'),
				'aws_secret_access_key': environ.get('AWS_SECRET_ACCESS_KEY', 'localhost-aws-secret-key'),
			}

		self.supplicant_table_name = 'Erad_Supplicant_Global'

		self.cwd = getcwd()
		self.group_id = input_args.group_id
		self.path_to_exclude_file = input_args.path_to_exclude_file

		self.dyndb_connection = None
		self.dynamodb_client = None
		self.exclude_patterns = set()

		self.init_aws_connections()

	def init_aws_connections(self):
		connection_config = self.base_config.copy()
		self.dyndb_connection = boto3.resource('dynamodb', **connection_config)
		self.dynamodb_client = boto3.client('dynamodb', **connection_config)

	def load_patterns(self):
		filepath = path.realpath(self.path_to_exclude_file)

		with open(filepath, 'r') as pattern_file:
			for line in pattern_file:
				self.exclude_patterns.add(re.compile(line.strip()))

	def filter_supplicants_list(self, supplicants_list):
		self.load_patterns()
		excluded = []
		list_to_process = []
		for supplicant in supplicants_list:
			has_match = False

			for pattern in self.exclude_patterns:
				if pattern.search(supplicant.supplicant_username) is not None:
					has_match = True
					break

			if has_match is True:
				excluded.append(supplicant)
			else:
				list_to_process.append(supplicant)

		return list_to_process, excluded

	def save_list(self, filename, list_to_save):
		filepath = path.join(self.cwd, filename)
		with open(filepath, 'w') as outfile:
			for item in list_to_save:
				outfile.write(repr(item) + '\n')

	def del_supplicants(self, list_to_delete):
		table = self.dyndb_connection.Table(self.supplicant_table_name)

		with table.batch_writer() as batch:
			for i, supplicant in enumerate(list_to_delete, start=1):
				batch.delete_item(Key={'Group_ID': supplicant.group_id, 'Username': supplicant.supplicant_username})
				logger.info(f'Deleted {supplicant}')

	def load_supplicants(self):
		paginator = self.dynamodb_client.get_paginator('query')
		response_iterator = paginator.paginate(
			TableName=self.supplicant_table_name,
			KeyConditionExpression='Group_ID = :x',
			ExpressionAttributeValues={
				':x': {'S': self.group_id},
			}
		)
		supplicants_list = []

		for response in response_iterator:
			items = response['Items']

			for item in items:
				supplicants_list.append(
					Supplicant(group_id=item['Group_ID']['S'], supplicant_username=item['Username']['S'])
				)
		return supplicants_list

	def process(self, input_args):
		supplicants_list = self.load_supplicants()
		list_to_process, excluded = self.filter_supplicants_list(supplicants_list)

		if input_args.action == 'print':
			self.save_list('list_to_process', list_to_process)
			self.save_list('excluded', excluded)
		elif input_args.action == 'remove':
			self.del_supplicants(list_to_process)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description='Tool to remove supplicants',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter
	)
	required_params = parser.add_argument_group('required params')

	required_params.add_argument(
		'--group-id',
		metavar='<group-id:str>',
		type=str,
		help='Full group id - foo_account_id::bar_group_id',
		required=True
		)
	parser.add_argument(
		'--action',
		choices=['print', 'remove'],
		help='Action that will be applied executed',
		default='print'
		)
	parser.add_argument(
		'--path-to-exclude-file',
		metavar='<path-to-exclude-file:str>',
		type=str,
		help='Path to file that contains regexp to exclude supplicants from processing',
		default='./exclude-patterns.txt'
		)

	input_args = parser.parse_args()

	endpoint_tester = SupplicantRemover(input_args)
	endpoint_tester.process(input_args)
