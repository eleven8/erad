import re
from os import environ
from unittest import mock

import boto3
import pytest

from remove_supplicant import Supplicant
from remove_supplicant import SupplicantRemover

base_config = {
	'endpoint_url': None if environ.get('AWS_PRODUCTION_ENV') == 'True' else 'http://localhost:8000',
	'region_name': environ.get('AWS_DEFAULT_REGION', 'us-west-2'),
	'aws_access_key_id': environ.get('AWS_ACCESS_KEY_ID', 'localhost-aws-access-key'),
	'aws_secret_access_key': environ.get('AWS_SECRET_ACCESS_KEY', 'localhost-aws-secret-key'),
}

SUPPLICANT_TABLE = 'Erad_Supplicant_Global'


class InputArgs:
	def __init__(self):
		self.group_id = ''
		self.action = 'print'
		self.path_to_exclude_file = './exclude'


def get_dyndb_connections(region: str = None):
	connection_config = base_config.copy()
	if region is not None:
		connection_config['region_name'] = region
	dyndb_connection = boto3.resource('dynamodb', **connection_config)
	dynamodb_client = boto3.client('dynamodb', **connection_config)
	return dyndb_connection, dynamodb_client


@pytest.fixture(scope='module')
def create_test_supplicants():
	dynamodb, dynamodb_client = get_dyndb_connections()
	try:
		dynamodb_client.delete_table(
			TableName=SUPPLICANT_TABLE
		)
	except Exception as err:
		print(err)

	dynamodb_client.create_table(
		AttributeDefinitions=[
			{
				'AttributeName': 'Group_ID',
				'AttributeType': 'S'
			},
			{
				'AttributeName': 'Username',
				'AttributeType': 'S'
			},
		],
		TableName=SUPPLICANT_TABLE,
		KeySchema=[
			{
				'AttributeName': 'Group_ID',
				'KeyType': 'HASH'
			},
			{
				'AttributeName': 'Username',
				'KeyType': 'RANGE'
			},
		],
		ProvisionedThroughput={
			'ReadCapacityUnits': 1,
			'WriteCapacityUnits': 1
		}
	)
	table = dynamodb.Table(SUPPLICANT_TABLE)

	test_items = [
		{
			'Group_ID': 'aa::bb',
			'Username': 'userA'
		},
		{
			'Group_ID': 'aa::bb',
			'Username': 'userB'
		},
		{
			'Group_ID': 'aa::bc',
			'Username': 'userC'
		},

	]

	for item in test_items:
		table.put_item(Item=item)


@pytest.mark.usefixtures('create_test_supplicants')
def test_load_supplicants():
	ia = InputArgs()
	ia.group_id = 'aa::bb'

	supp_remover = SupplicantRemover(ia)
	supp_list = supp_remover.load_supplicants()

	expected = {
		Supplicant(group_id='aa::bb', supplicant_username='userA'),
		Supplicant(group_id='aa::bb', supplicant_username='userB'),
	}
	assert set(supp_list) == expected

	ia.group_id = 'aa::bc'

	supp_remover = SupplicantRemover(ia)
	supp_list = supp_remover.load_supplicants()

	expected = {
		Supplicant(group_id='aa::bc', supplicant_username='userC'),
	}
	assert set(supp_list) == expected


def load_patterns_mock_se(self, *args):
	patterns = [
		'A',
		'BB',
		'^c',
	]
	self.exclude_patterns = set(map(re.compile, patterns))


@mock.patch('remove_supplicant.SupplicantRemover.load_patterns', side_effect=load_patterns_mock_se, autospec=True)
@pytest.mark.usefixtures('create_test_supplicants')
def test_filter_supplicants_list(_mock):
	ia = InputArgs()
	ia.group_id = 'aa::bb'

	supp_remover = SupplicantRemover(ia)

	supplicants_list = {
		Supplicant(group_id='aa::bb', supplicant_username='userB'),
		Supplicant(group_id='aa::bb', supplicant_username='userA'),
		Supplicant(group_id='aa::bb', supplicant_username='c'),
		Supplicant(group_id='aa::bb', supplicant_username='888***BBB'),
	}

	list_to_process, excluded = supp_remover.filter_supplicants_list(supplicants_list)

	expected_list_to_process = {
		Supplicant(group_id='aa::bb', supplicant_username='userB'),
	}
	expected_excluded = {
		Supplicant(group_id='aa::bb', supplicant_username='userA'),
		Supplicant(group_id='aa::bb', supplicant_username='c'),
		Supplicant(group_id='aa::bb', supplicant_username='888***BBB'),
	}

	assert set(list_to_process) == expected_list_to_process
	assert set(excluded) == expected_excluded

