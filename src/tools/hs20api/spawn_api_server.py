from api import app

#---------------------- Debug Mode -----------------------
# This next statement only runs if this script is run manually from the CLI
# When enabled as a wsgi module within Apache, this will not execute.
if __name__ == '__main__':
    app.run(debug = True, host='0.0.0.0', port=9090)