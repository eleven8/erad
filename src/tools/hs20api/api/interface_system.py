#!/usr/bin/python
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import random
import re, os
import util
from .exceptions import *
from interface_util import *
import csv, StringIO, string
import json
import base64
import time, datetime

class HS20InterfaceSystem(HS20InterfaceUtil):

	ProfilesFolder = 'profiles/'

	# garbage collector deletes old profiles
	def garbage_collector ( self ):
		dir_to_search = os.path.curdir+'/'+self.ProfilesFolder
		for dirpath, dirnames, filenames in os.walk(dir_to_search):
			for file in filenames:
				curpath = os.path.join(dirpath, file)
				file_modified = datetime.datetime.fromtimestamp(os.path.getmtime(curpath))
				if datetime.datetime.now() - file_modified > datetime.timedelta(minutes=2):
					os.remove(curpath)

    # returns the profle
	def samsung_profile_create ( self, fields ):		
		username = self.expect_parameter( fields, 'username' )
		password = self.expect_parameter( fields, 'password' )
		vendor = self.expect_parameter( fields, 'vendor' )
		domain = self.expect_parameter( fields, 'domain' )
		consortium = self.expect_parameter( fields, 'consortium' )

		template = """
cred={
	username=\""""+username+"""\"
	password=\""""+password+"""\"	
	realm=\""""+domain+"""\"	
	domain=\""""+domain+"""\"
	roaming_consortium="""+consortium+"""
	eap=TTLS
	phase2="auth=MSCHAPv2"
}
		"""			

		filename = str(int(time.time()))+''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))+'.conf'
		with open(self.ProfilesFolder+filename, 'w') as file_:
			file_.write(template)
			
		return {'Url': '/system/profile/download?file='+filename}

	def profile_download ( self, fields ):
		# run gargabe collector
		self.garbage_collector()

		filename = self.expect_parameter( fields, 'file' )

		template = ""

		try:
			with open(self.ProfilesFolder+filename, 'r') as file_:
				template = file_.read()
		except:
			raise HS20AccessException( "File not found." )

		si = StringIO.StringIO()
		si.write(template)
		return si.getvalue()        

	def android_profile_create ( self, fields ):
		cacert = self.expect_parameter( fields, 'cacert' )
		username = self.expect_parameter( fields, 'username' )
		password = self.expect_parameter( fields, 'password' )
		domain = self.expect_parameter( fields, 'profileName' )		
		vendor = self.expect_parameter( fields, 'vendor' )
		# cafile = base64.b64decode(cacert)		

		xml_data = """
<MgmtTree xmlns="syncml:dmddf1.2">
  <VerDTD>1.2</VerDTD>
  <Node>
    <NodeName>PerProviderSubscription</NodeName>
    <RTProperties>
      <Type>
        <DDFName>urn:wfa:mo:hotspot2dot0-perprovidersubscription:1.0</DDFName>
      </Type>
    </RTProperties>
    <Node>
      <NodeName>X1</NodeName>
      <Node>
        <NodeName>HomeSP</NodeName>
        <Node>
          <NodeName>FQDN</NodeName>
          <Value>"""+domain+"""</Value>
        </Node>
        <Node>
          <NodeName>FriendlyName</NodeName>
          <Value>WiFi Powered by ElevenWireless</Value>
        </Node>
      </Node>
      <Node>
        <NodeName>Credential</NodeName>
        <Node>
          <NodeName>UsernamePassword</NodeName>
          <Node>
            <NodeName>EAPMethod</NodeName>
            <Node>
              <NodeName>InnerMethod</NodeName>
              <Value>MS-CHAP-V2</Value>
            </Node>
            <Node>
              <NodeName>EAPType</NodeName>
              <Value>21</Value>
            </Node>
          </Node>
          <Node>
            <NodeName>MachineManaged</NodeName>
            <Value>true</Value>
          </Node>
          <Node>
            <NodeName>Username</NodeName>
            <Value>"""+username+"""</Value>
          </Node>
          <Node>
            <NodeName>Password</NodeName>
            <Value>"""+password+"""</Value>
          </Node>
        </Node>
        <Node>
          <NodeName>Realm</NodeName>
          <Value>"""+domain+"""</Value>
        </Node>
        <Node>
          <NodeName>CreationDate</NodeName>
          <Value>2015-11-24T10:56:17Z</Value>
        </Node>
      </Node>
    </Node>
  </Node>
</MgmtTree>
		"""
		
		template = """
Content-Type: multipart/mixed; boundary={boundary}
Content-Transfer-Encoding: base64

--{boundary}
Content-Type: application/x-passpoint-profile
Content-Transfer-Encoding: base64

%s
--{boundary}
Content-Type: application/x-x509-ca-cert
Content-Transfer-Encoding: base64

%s
--{boundary}--
		""" % (base64.encodestring(xml_data), cacert)
		template = base64.encodestring(template)
		filename = str(int(time.time()))+''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))+'.config'
		with open(self.ProfilesFolder+filename, 'w') as file_:
			file_.write(template)
		return {'Url': '/system/profile/download?vendor=android&file='+filename}


	def apple_profile_create ( self, fields ):
		cacert = self.expect_parameter( fields, 'cacert' )
		username = self.expect_parameter( fields, 'username' )
		password = self.expect_parameter( fields, 'password' )
		ssid = self.expect_parameter( fields, 'ssid' )
		profileName = self.expect_parameter( fields, 'profileName' )
		expDate = self.expect_parameter( fields, 'expDate' )
		vendor = self.expect_parameter( fields, 'vendor' )
		cafile = base64.b64decode(cacert)		

		template = """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>ConsentText</key>
	<dict>
		<key>default</key>
		<string>COnsent Message that will be displayed during profile installation...</string>
	</dict>
	<key>PayloadContent</key>
	<array>
		<dict>
			<key>PayloadCertificateFileName</key>
			<string>server.der</string>
			<key>PayloadContent</key>
			<data>
			%s
			</data>
			<key>PayloadDescription</key>
			<string>Configures certificate settings.</string>
			<key>PayloadDisplayName</key>
			<string>osu-server.osu.example.com</string>
			<key>PayloadIdentifier</key>
			<string>com.apple.security.pkcs1.98072F43-ECB2-4113-A6C8-CF4F2A740292</string>
			<key>PayloadType</key>
			<string>com.apple.security.pkcs1</string>
			<key>PayloadUUID</key>
			<string>98072F43-ECB2-4113-A6C8-CF4F2A740292</string>
			<key>PayloadVersion</key>
			<integer>1</integer>
		</dict>
		<dict>
			<key>AutoJoin</key>
			<true/>
			<key>DisplayedOperatorName</key>
			<string>%s</string>
			<key>EAPClientConfiguration</key>
			<dict>
				<key>AcceptEAPTypes</key>
				<array>
					<integer>21</integer>
				</array>
				<key>PayloadCertificateAnchorUUID</key>
				<array>
					<string>98072F43-ECB2-4113-A6C8-CF4F2A740292</string>
				</array>
				<key>TTLSInnerAuthentication</key>
				<string>MSCHAPv2</string>
				<key>UserName</key>
				<string>%s</string>
				<key>UserPassword</key>
				<string>%s</string>
			</dict>
			<key>EncryptionType</key>
			<string>WPA</string>
			<key>IsHotspot</key>
			<true/>
			<key>NAIRealmNames</key>
			<array>
				<string>enterpriseauth.com</string>
			</array>
			<key>PayloadDescription</key>
			<string>Configures Wi-Fi settings</string>
			<key>PayloadDisplayName</key>
			<string>Wi-Fi</string>
			<key>PayloadIdentifier</key>
			<string>com.apple.wifi.managed.BB4CD673-997E-4A15-B694-B7FF9D8B8B78</string>
			<key>PayloadType</key>
			<string>com.apple.wifi.managed</string>
			<key>PayloadUUID</key>
			<string>F505C454-29A5-4F91-8F34-3EC26927D298</string>
			<key>PayloadVersion</key>
			<real>1</real>
			<key>ProxyType</key>
			<string>None</string>
			<key>RoamingConsortiumOIs</key>
			<array>
				<string>2233445566</string>
			</array>
			<key>SSID_STR</key>
			<string>%s</string>
			<key>ServiceProviderRoamingEnabled</key>
			<true/>
			<key>_UsingHotspot20</key>
			<false/>
		</dict>
	</array>
	<key>PayloadDescription</key>
	<string>Description</string>
	<key>PayloadDisplayName</key>
	<string>%s</string>
	<key>PayloadIdentifier</key>
	<string>elevenwirelessidentificator</string>
	<key>PayloadOrganization</key>
	<string>%s</string>
	<key>PayloadRemovalDisallowed</key>
	<false/>
	<key>PayloadType</key>
	<string>Configuration</string>
	<key>PayloadUUID</key>
	<string>6D9EC1F4-0BAE-403C-8550-BD04EB19EE3F</string>
	<key>PayloadVersion</key>
	<integer>1</integer>
</dict>
</plist>

		""" % (cacert, profileName, username, password, ssid, profileName, profileName)
		filename = str(int(time.time()))+''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))+'.mobileconfig'
		with open(self.ProfilesFolder+filename, 'w') as file_:
			file_.write(template)
		# signing the profile
		runCommand = "/usr/bin/openssl smime -sign -in "+self.ProfilesFolder+filename+" -out "+self.ProfilesFolder+"signed_"+filename+" -signer apple/serverwp.pem -certfile apple/server.crt -outform der -nodetach"		
		os.system(runCommand)

		return {'Url': '/system/profile/download?vendor=apple&file='+"signed_"+filename}


