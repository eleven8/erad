#!/usr/bin/python
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from datetime import datetime, timedelta
import dateutil.parser as parser
import pytz
import re
import boto
import json
from boto.s3.key import Key
import exceptions

erad_configuration = None

def erad_cfg():
    global erad_configuration

    if not erad_configuration is None:
        return erad_configuration

    # specify defaults
    cfg = {
        "system_key": ["r_7X385PDgqPSB2cMvpmPZ6GHqXh5Cd8ZF"],
        "api": {
            "host": "http://0.0.0.0:5000",
            "allow_internal": True
        },
        "database" : {
            "region" : "us-west-2",
            "host" : "http://localhost:8000"
        },
        "proxy_conf": {
            "can_write" : False,
            "can_read" : False,
            "bucket": None,
            "filename": "proxy.conf"
        },
        "audit": {
            "admin" : {
                "group": True,
                "supplicant": True
            }, 
            "integration" : {
                "session": True
            }
        }
    }
    # try override defaults with machine config
    try:
        cfg.update( json.load(open("/etc/erad.cfg")) or {} )
    except:
        pass
    # convert dict to object so it can be referenced 
    # as cfg.region instead of cfg["region"]
    erad_configuration = deep_convert_dict_to_object(cfg)
    return erad_configuration

def deep_convert_dict_to_object( d ):
    for k in d:
        if type(d[k]) is dict:
            d[k] = deep_convert_dict_to_object( d[k] )
    return type("", (), d)()

def from_isoformat( str ):
    # 2015-04-09T06:23:57+00:00
    return parser.parse(str)

def utc_isoformat( utc ):
    # 2015-04-08T00:00:00Z
    return utc.strftime("%Y-%m-%dT%H:%M:%SZ")

def day_format( date ):
    return date.strftime("%Y-%m-%d")

def local_to_utc( dt, str_tz ):
    tz = pytz.timezone(str_tz)
    try:
        # this can raise exception if time is ambiguous
        aware = tz.normalize(tz.localize(dt))
    except:
        # choose standard time
        aware = tz.normalize(tz.localize(dt, is_dst=False))
    return aware.astimezone(pytz.utc)

def utc_to_local( utc, str_tz ):
    tz = pytz.timezone(str_tz)
    utc = utc.replace(tzinfo=pytz.utc)
    return utc.astimezone(tz)

def utcnow():
    return datetime.utcnow()

def now_isoformat():
    return utc_isoformat( datetime.utcnow() )

def now_ptzformat():
    return utc_to_local( datetime.utcnow(), 'US/Pacific' ).strftime("%Y-%m-%d %I:%M %p")

def today( str_tz ):
    return utc_to_local( datetime.utcnow(), str_tz )

def today_in_day_format( str_tz ):
    return day_format( today( str_tz ) )

# timedelta function to add support for python 2.6
def timedelta_total_seconds(timedelta):
    return (
        timedelta.microseconds + 0.0 +
        (timedelta.seconds + timedelta.days * 24 * 3600) * 10 ** 6) / 10 ** 6

def datetime_to_milliseconds_since_epoch( dt ):
    epoch = datetime.utcfromtimestamp(0).replace(tzinfo=pytz.utc)
    delta = dt - epoch
    return long(timedelta_total_seconds(delta) * 1000.0)

def session_time_is_expired( time ):
    if time is None:
        return False
    threshold = (utcnow() - timedelta(hours=1))
    return time.replace(tzinfo=pytz.utc) < threshold.replace(tzinfo=pytz.utc)

def parse_human_date_str( s ):
    return datetime.strptime( s, "%m/%d/%Y" )

def to_human_date_str( dt ):
    return dt.strftime( "%m/%d/%Y" )

def parse_ipaddr( proxy ):
    return proxy.rsplit(":",1)[0]
    
def parse_port( proxy ):
    try:
        return int(proxy.rsplit(":",1)[1])
    except:
        # default port
        return 1812

def parse_shared_secret( shared_secret ):
    return shared_secret or "4905DVCA3B"

def construct_proxy_id( RemoteServerUrl ):
    return parse_ipaddr(RemoteServerUrl).replace(".","_") + "__" + str(parse_port(RemoteServerUrl))

def is_valid_proxy( proxy ):
    return ('52.11.20.214' not in (proxy or "")) and re.search( "\.", proxy or "" )

def is_valid_ipaddr( ipaddr ):
    return re.search( "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", ipaddr or "" )    

# ensures 6 sets of hex numbers separated by : or -
def is_valid_mac( mac ):
    return re.match("[0-9a-f]{2}([-:])[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", mac.lower())

def sanitize_proxy( proxy ):
    try:
        if is_valid_proxy( proxy ):
            ip = parse_ipaddr( proxy )
            port = parse_port( proxy )
            if is_valid_ipaddr( ip ):
                return ip + ":" + str(port)
    except:
        pass
    return None

def uncompress_mac( mac ):
    chars = list( mac )
    for i in range(2,15,3):
        chars.insert(i,':')
    return ''.join(chars)

# checks mac validity and converts to uppercase and colons
def sanitize_mac( calledId ):
    if calledId is None:
        return calledId
    calledId = calledId.strip()

    # uncompress
    if len(calledId) == 12:
        calledId = uncompress_mac(calledId)

    ssidName = ""

    # if we have an extra colon, its a wifi SSID name and we need to strip it out.
    if calledId.count(":") == 1 or calledId.count(":") == 6:
        calledId, ssidName = calledId.rsplit(':', 1)
        ssidName = ssidName.strip()

    calledId = calledId.upper().replace("-",":").strip()

    if not is_valid_mac(calledId):
        raise exceptions.HS20ValidationException('Invalid Mac')

    return calledId
    
def read_string_from_s3( bucket_name, key ):
    connection = boto.connect_s3()
    try:
        bucket = connection.get_bucket(bucket_name)
        s3file = Key(bucket)
        s3file.key = key
        return s3file.get_contents_as_string()
    finally:
        connection.close()
    
def write_string_to_s3( bucket_name, key, contents ):
    connection = boto.connect_s3()
    try:
        bucket = connection.get_bucket(bucket_name)
        s3file = Key(bucket)
        s3file.key = key
        return s3file.set_contents_from_string( contents )
    finally:
        connection.close()
    
def read_proxy_conf_to_s3():
    pc = erad_cfg().proxy_conf
    return read_string_from_s3( pc.bucket, pc.filename )

def write_proxy_conf_to_s3( contents ):
    pc = erad_cfg().proxy_conf
    return write_string_to_s3( pc.bucket, pc.filename, contents )






