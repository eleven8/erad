#!/usr/bin/python
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
## 
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of 
## Eleven Wireless Inc.
##
################################################################################

import traceback
from datetime import timedelta
from .exceptions import *


from flask import Flask, request, make_response, abort, jsonify, current_app
from flask_compress import Compress

from functools import update_wrapper

# turn off annoying DEBUG logs
import logging
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logging.getLogger('pynamodb.connection.base').setLevel(logging.CRITICAL)


#---------------------- Initialization -----------------------
# this should go in a config or be part of debug mode. 
# it enables stack traces in errors.
show_tracebacks = True

# create the Flask app object, override default configurations
app = Flask(__name__)

app.config.update( 
    # pass through unicode when serializing, do not convert it to ASCII
    JSON_AS_ASCII = False,
    # always pass exceptions to our server_error() function (even in Debug mode)
    PROPAGATE_EXCEPTIONS = False
    )
# turn on the Compress module which does automatic gzipping for us
Compress(app)


#---------------------- CORS Decorator -----------------------
# see http://en.wikipedia.org/wiki/Cross-origin_resource_sharing
def crossdomain(origin='*', methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS'
            h['Access-Control-Max-Age'] = str(max_age)
            h['Access-Control-Allow-Headers'] = 'origin, x-requested-with, content-type'
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator


#---------------------- Utility Methods -----------------------
def ok_response( data=None ):
    ret = { "Error": None }
    if not data is None:
        ret.update( data )
    return make_response(jsonify(ret), 200)

def string_input( name ):
    if not request.json:
        abort(400)
    if not name in request.json:
        abort(400)
    return str(request.json[name]);


#---------------------- Error Handlers -----------------------

# if user does not pass us JSON, they end up here
@app.errorhandler(400)
@crossdomain()
def error_malformed(e):
    return make_response(jsonify({"Error": "Malformed request"}), 400)

# if user accesses nonexistent path, they end up here
@app.errorhandler(404)
@crossdomain()
def error_not_found(Error):
    return make_response(jsonify({"Error": 'Not found'}), 404)

# if user uses GET instead of POST (for example), they end up here
@app.errorhandler(405)
@crossdomain()
def error_method_not_allowed(Error):
    return make_response(jsonify({"Error": 'Method Not Allowed'}), 405)

# all uncaught exceptions end up here.
@app.errorhandler(500)
@crossdomain()
def error_exception(e):

    # build return object
    ret = {"Error": (str(e) or "Exception") }

    # handle HS20 exceptions which may need a different http code
    etype = type(e)
    if issubclass(etype, HS20MalformedException):
        httpcode = 400
    elif issubclass(etype, HS20AccessException):
        httpcode = 403
    elif issubclass(etype, HS20ConfigurationException):
        httpcode = 200
    elif issubclass(etype, HS20ValidationException):
        httpcode = 200
    elif issubclass(etype, HS20ExpiredException):
        httpcode = 200
    else:
        httpcode = 500
        # only in the case of an unexpected Error might we show a stack trace
        if show_tracebacks:
            ret['stack'] = traceback.format_exc()

    # return json Error to client
    return make_response(jsonify(ret), httpcode)

#---------------------- Import API Routes -----------------------
# TO_BE_CHECKED
import profile 
import api_system

__all__ = [ "api_system" ]






























