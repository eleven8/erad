#!/usr/bin/python
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

# ----- exceptions thrown by api -----

# The caller doesnt have access
class HS20AccessException(Exception):
    pass
    
# The caller called us wrong
class HS20MalformedException(Exception):
    pass

# user supplied data was invalid
class HS20ValidationException(Exception):
    pass

# Something is configured incorrectly
class HS20ConfigurationException(Exception):
    pass

# Access is expired
class HS20ExpiredException(Exception):
    pass

# something required was missing
class HS20RequiredFieldException(HS20MalformedException):
    pass

# something was passed, but the wrong type
class HS20WrongTypeException(HS20MalformedException):
    pass

# something passed is not allowed
class HS20RestrictedFieldException(HS20MalformedException):
    pass

class HS20InvalidFileException(HS20MalformedException):
    pass

# ----- exceptions thrown by frmod -----

class HS20NotFound(Exception):
   pass

class HS20ServiceError(Exception):
   pass