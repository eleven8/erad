#!/usr/bin/python
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

from api import *
from interface_system import HS20InterfaceSystem

#---------- System Services ----------

# this message is used for alerting, changing it will cause a "code red" alert
@app.route('/')
def system_index():
    return ok_response({ 'status': "HS20 ProfileCreator Webservice is up and running." })

# http://localhost:9090/system/apple/profile/create
@app.route('/system/samsung/profile/create', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def system_samsung_profile_create():
    if request.method != 'POST':
        return ok_response()
    return ok_response( HS20InterfaceSystem().samsung_profile_create( request.json ) )

# http://localhost:9090/system/profile/create
@app.route('/system/profile/download', methods=['HEAD', 'GET', 'OPTIONS'])
@crossdomain()
def system_profile_download():
    if request.method != 'GET':
        return ok_response()
    # args is used here instead of json because this is a GET request
    response = make_response( HS20InterfaceSystem().profile_download( request.args ), 200 )

    filename = "cred.conf"
    if "vendor" in request.args:
        if request.args['vendor'] == 'android':
            filename = 'passpoint.config'
        if request.args['vendor'] == 'apple':
            filename = 'ElevenWireless.mobileconfig'

    # the response should be downloaded
    response.headers["Content-Disposition"] = "attachment; filename="+filename
    response.headers["Content-type"] = "text/plain"
    return response

# http://localhost:9090/system/apple/profile/create
#@app.route('/system/apple/profile/create', methods=['HEAD', 'POST', 'OPTIONS'])
#@crossdomain()
#def system_apple_profile_create():
#    if request.method != 'POST':
#        return ok_response()
#    return ok_response( HS20InterfaceSystem().apple_profile_create( request.json ) )

# http://localhost:9090/system/marshmallow/profile/create
@app.route('/system/android/profile/create', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def system_android_profile_create():
    if request.method != 'POST':
        return ok_response()
    return ok_response( HS20InterfaceSystem().android_profile_create( request.json ) )

# http://localhost:9090/system/apple/profile/create
@app.route('/system/apple/profile/create', methods=['HEAD', 'POST', 'OPTIONS'])
@crossdomain()
def system_apple_profile_create():
    if request.method != 'POST':
        return ok_response()
    return ok_response( HS20InterfaceSystem().apple_profile_create( request.json ) )

