#!/usr/bin/python
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################

import decimal
import pytz
import util
from .exceptions import *


class HS20InterfaceUtil:

    #TODO: Autodetect parameter type and call a generic 'set_input_value'

    def expect_parameter( self, fields, field_name ):
        if (fields is None) or (not field_name in fields):
            raise HS20RequiredFieldException( "Required parameter '{0}' is missing.".format( field_name ) )
        return fields[field_name]

    def expect_unicode_value( self, fields, field_name ):
        val = self.expect_parameter( fields, field_name )
        if val is None:
            return val
        if not type(val) is unicode:
            raise HS20WrongTypeException( "Parameter '{0}' is incorrect type (expected unicode string).".format( field_name ) )
        return val if val else None

    def expect_unicode_value_nonempty( self, fields, field_name ):
        val = self.expect_parameter( fields, field_name )
        if not type(val) is unicode:
            raise HS20WrongTypeException( "Parameter '{0}' is incorrect type (expected unicode string).".format( field_name ) )
        if not val:
            raise HS20WrongTypeException( "Parameter '{0}' cannot be empty string.".format( field_name ) )
        return val

    def expect_dict( self, fields, field_name ):
        val = self.expect_parameter( fields, field_name )
        if not isinstance(val, dict):
            raise HS20WrongTypeException( "Parameter '{0}' is incorrect type (expected an object).".format( field_name ) )
        return val

    def expect_date_value_from_tz( self, fields, field_name, str_tz ):
        val = self.expect_unicode_value( fields, field_name ) or ''
        try:
            val = util.parse_human_date_str( val.strip() )
        except:
            raise HS20WrongTypeException( "Parameter '{0}' is incorrect type (expected date string in format MM/DD/YYYY).".format( field_name ) )
        return util.local_to_utc( val, str_tz )

    def expect_datetime_value_utc( self, fields, field_name ):
        val = self.expect_unicode_value( fields, field_name ) or ''
        try:
            return util.from_isoformat( val.strip() ).replace(tzinfo=pytz.utc)
        except:
            raise HS20WrongTypeException( "Parameter '{0}' is incorrect type (expected datetime string in ISO format).".format( field_name ) )

    def set_unicode_value( self, model, fields, field_name ):
        setattr(model, field_name, self.expect_unicode_value(fields, field_name) )

    def set_unicode_value_nonempty( self, model, fields, field_name ):
        setattr(model, field_name, self.expect_unicode_value_nonempty(fields, field_name) )

    def set_optional_unicode_value( self, model, fields, field_name ):
        if (not fields is None) and (field_name in fields):
            setattr(model, field_name, self.expect_unicode_value(fields, field_name) )

    def set_integer_value( self, model, fields, field_name ):
        val = self.expect_parameter( fields, field_name ) or 0
        if type(val) is unicode:
            val = int(val)
        if not type(val) in [int, long]:
            raise HS20WrongTypeException( "Parameter '{0}' is incorrect type (expected number).".format( field_name ) )
        setattr(model, field_name, val )

    def set_optional_integer_value( self, model, fields, field_name, default_value ):
        if (not fields is None) and (field_name in fields):
            val = self.expect_parameter( fields, field_name ) or 0
            if type(val) is unicode:
                val = int(val)
            if not type(val) in [int, long]:
                raise HS20WrongTypeException( "Parameter '{0}' is incorrect type (expected number).".format( field_name ) )
            setattr(model, field_name, val )
        else:
            setattr(model, field_name, default_value )

    def set_boolean_value( self, model, fields, field_name, default_value ):
        val = self.expect_parameter( fields, field_name )
        if type(val) is unicode:
            if val.lower() == "true":
                val = True
            elif val.lower() == "false":
                val = False
        if not type(val) is bool:
            if not val:
                val = default_value
            else:
                raise HS20WrongTypeException( "Parameter '{0}' is incorrect type (expected bool).".format( field_name ) )
        setattr(model, field_name, val )

    def set_optional_boolean_value( self, model, fields, field_name, default_value ):        
        if (not fields is None) and (field_name in fields):
            val = val = self.expect_parameter( fields, field_name )
            if type(val) is unicode:
                if val.lower() == "true":
                    val = True
                elif val.lower() == "false":
                    val = False
            if not type(val) is bool:
                if not val:
                    val = default_value
                else:
                    raise HS20WrongTypeException( "Parameter '{0}' is incorrect type (expected bool).".format( field_name ) )
            setattr(model, field_name, val )
        else:
            setattr(model, field_name, default_value )

    def set_unicode_array( self, model, fields, field_name ):
        val = self.expect_parameter( fields, field_name )
        if not isinstance( val, list ):
            raise HS20WrongTypeException( "Parameter '{0}' is incorrect type (expected array of unicode strings).".format( field_name ) )
        setattr(model, field_name, val )

    def set_optional_date_value_from_tz( self, model, fields, field_name, str_tz ):
        if (not fields is None) and (field_name in fields) and (len(str(fields[field_name]).strip()) > 0):
            setattr(model, field_name, self.expect_date_value_from_tz(fields, field_name, str_tz) )

    def set_optional_datetime_value_utc( self, model, fields, field_name ):
        if (not fields is None) and (field_name in fields) and (fields[field_name] is not None) and (len(str(fields[field_name]).strip()) > 0):
            setattr(model, field_name, self.expect_datetime_value_utc(fields, field_name) )

    def standard_output_object( self, model ):
        ret = {}
        ret[model.Meta.name] = model_to_pod( model )
        return ret

    def standard_output_object_tz( self, model, str_tz ):
        ret = {}
        ret[model.Meta.name] = model_to_pod( model, str_tz )
        return ret

