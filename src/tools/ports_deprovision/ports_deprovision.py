from erad.api.erad_model import Erad_Group2, Erad_Endpoint
from erad.api.interface_admin import EradInterfaceAdmin
from erad import util
import time
import argparse
import logging
import os
import re


logger = logging.getLogger(__name__)
logger.setLevel(
    os.environ.get('LOGLEVEL', 'DEBUG').upper()
)


def get_groups_index(account_id: str, rate_limit: int) -> list:
    groups_index = set()

    for group in Erad_Group2.scan(Erad_Group2.Account_ID==account_id, rate_limit=rate_limit):
        groups_index.add((group.Account_ID, group.ID))

    return sorted(groups_index)


def get_groups(account_id: str, group_id: str, rate_limit: int, force: bool = False) -> list:
    """Scan all groups except ('eradtests', 'erad-tests')
       If group_id is set - all groups before it will be skipped
       return: list of groups data
    """
    logger.debug(f'{(account_id, group_id)}')

    groups = []
    skip = True
    for account, group in get_groups_index(account_id, rate_limit):
        # by default process only groups which names match XX-NNN-NN  pattern
        if not force and not re.match('[A-Z]{2}-\d{3}-\d{2}', group):
            continue
        # skipping service site which is used only for tests
        if (account, group) == ('eradtests', 'erad-tests'):
            continue
        # skipping processed sites
        if group_id and skip:
            if (account, group) != (account_id, group_id):
                continue
            skip = False

        groups.append((account, group))

    return groups


def iterate_groups(groups: list, rate_limit: int, delete=False) -> bool:
    updated = False

    for group in groups:
        endpoints = [e for e in
            Erad_Endpoint().group_id_index.query(
                '%s::%s' % group,
                rate_limit=rate_limit
            )
        ]
        if not endpoints: continue

        for e in endpoints:
            old_group_id = e.Group_ID
            if delete:
                e.Group_ID = '::dirty'
                e.Secret = util.generate_random_string(size=10)
                e.save()
                updated = True

            # csv like format
            print(f'{old_group_id},{e.IpAddress},{e.Port},{e.Group_ID}')

    return updated


def main():
    parser = argparse.ArgumentParser(
        description='Deprovision ERAD sites for given account'
    )
    parser.add_argument(
        '-a', '--account-id',
        required=True,
        metavar='account_id',
        type=str,
        help='ERAD Account ID',
    )
    parser.add_argument(
        '-r', '--rate-limit',
        metavar='rate-limit',
        type=int,
        help='AWS DynamoDB Read Capacity Units limit',
        default=20,
    )
    parser.add_argument(
        '-s', '--start-with',
        metavar='group_id',
        type=str,
        help='Specify group id for with report should start working, example: `foo`'
    )
    parser.add_argument(
        '-o', '--only',
        nargs='+',
        metavar='group_id',
        type=str,
        help='Specify group id that will be processed, example: `foo` `bar` `baz`'
    )
    parser.add_argument(
        '-d', '--delete',
        action='store_true',
        help='Delete found endpoints',
    )
    parser.add_argument(
        '-f', '--force',
        action='store_true',
        help='Force get all groups instead of XX-NNN-NN',
    )

    args = parser.parse_args()

    logger.debug(args)
    print(args.delete and 'DELETE' or 'DRY RUN', 'MODE')

    groups = []
    if args.only is not None:
        groups = [(args.account_id, group) for group in args.only]
    else:
        groups = [
            group for group in
            get_groups(args.account_id, args.start_with, args.rate_limit, args.force)
        ]

    updated = iterate_groups(groups, args.rate_limit, args.delete)

    if updated:
        logger.info('Updating radiusconf timestamp')
        EradInterfaceAdmin().update_radiusconf_timestamp(timestamp=int(time.time()))

    logger.info('done')


if __name__ == '__main__':
    main()

