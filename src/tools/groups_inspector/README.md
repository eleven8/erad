## Script usage

### Installation
To install script, you just need to move all files to target folder
and install its dependencies using the following command:

<pre>pip install -r requirements.txt</pre>


### Configuration
First, you need to set the following environment variables:

#### AWS credentials

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION

#### Script variables

- DB_HOST: Host of target DynamoDB - default is 'http://localhost:8000'
- DB_REGION: Region of DynamoDB - default is 'us-west-2'

#### Logging

You can edit the log configuration using logging.yaml file.


### Running

1. To dump a just empty groups:

<pre>python run.py --empty /path/to/output.csv</pre>

2. To dump a just non-empty groups:

<pre>python run.py --non-empty /path/to/output.csv</pre>

3. To dump both empty and non-empty groups to their default output files:

<pre>python run.py</pre>

4. To dump both empty and non-empty groups informing the output files:

<pre>python run.py --empty /path/to/empty.csv --non-empty /path/to/non-empty</pre>

5. To inform the target account ID - if not provided, the default value is 'elevenos':

<pre>python run.py --account-id elevenos</pre>