import helpers
import logging
import setup


logger = logging.getLogger(__name__)


def main():
    setup.setup()

    args = helpers.get_arguments()

    groups = helpers.get_groups(args.account_id)

    logger.debug(groups)
    logger.debug("Account ID :: {0}".format(args.account_id))

    for group in groups:
        if helpers.is_group_empty(group['AccountGroupId']):
            if args.empty:
                helpers.write_to_csv(args.empty, group)

        else:
            if args.non_empty:
                helpers.write_to_csv(args.non_empty, group)
