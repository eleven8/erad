import argparse
import unicodecsv as csv
import logging
import models


logger = logging.getLogger(__name__)


def create_csv_file(filename):
    with open(filename, 'w+b') as csv_file:

        field_names = ["GroupId", "GroupName", "AccountGroupId"]

        writer = csv.DictWriter(csv_file, fieldnames=field_names)

        writer.writeheader()


def write_to_csv(filename, row):
    with open(filename, 'ab') as csv_file:

        field_names = ["GroupId", "GroupName", "AccountGroupId"]

        writer = csv.DictWriter(csv_file, fieldnames=field_names)

        writer.writerow(row)


def get_groups(account_id):

    groups = list()

    for group in models.Erad_Group2.query(account_id):
        logger.debug(
            "{0} - {1}::{2}".format(
                group.Name,
                group.Account_ID,
                group.ID
            )
        )

        group = {
            "GroupId": group.ID,
            "GroupName": group.Name,
            "AccountGroupId": "{0}::{1}".format(group.Account_ID, group.ID)
        }

        groups.append(group)

    return groups


def count_supplicants(group_id):
    supplicants = models.Erad_Supplicant.count(group_id)

    return supplicants


def is_group_empty(group_id):

    supplicants = count_supplicants(group_id)

    if not supplicants:

        logger.info(
            "Group {0} is EMPTY ({1} supplicants assigned).".format(
                group_id,
                supplicants
            )
        )

        return True
    else:

        logger.info(
            "Group {0} is NOT EMPTY ({1} supplicants assigned).".format(
                group_id,
                supplicants
            )
        )

        return False


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--account-id',
        nargs='?',
        default='elevenos',
        help='This is the target account to get groups.'
    )

    parser.add_argument(
        '--empty',
        nargs='?',
        default=False,
        help='Dump all empty groups to file.'
    )

    parser.add_argument(
        '--non-empty',
        nargs='?',
        default=False,
        help='Dump all non-empty groups to file.'
    )
    args = parser.parse_args()

    # If none of them were provided, both files should be created on default path.
    if not args.empty and not args.non_empty:
        args.empty = 'empty.csv'
        args.non_empty = 'non-empty.csv'

    logger.debug(args)

    return args
