import helpers
import logging
import logging.config
import os
import yaml


def setup_logging(
    default_path='logging.yaml',
    default_level=logging.INFO,
    env_key='LOG_CFG'
):
    path = default_path
    value = os.getenv(env_key, None)

    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


def setup():
    setup_logging()

    args = helpers.get_arguments()

    if args.empty:
        helpers.create_csv_file(args.empty)

    if args.non_empty:
        helpers.create_csv_file(args.non_empty)
