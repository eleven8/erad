from erad.api.erad_model import Erad_Endpoint
from erad.api.erad_model import Erad_Group2
import argparse
import boto
import time
from erad import util


def get_groups_index():
	groups_index = set()

	for group in Erad_Group2.scan():
		groups_index.add('::'.join([group.Account_ID, group.ID]))

	return groups_index


def get_endpoitns():
	for endpoint in Erad_Endpoint.scan(
			filter_condition=(Erad_Endpoint.Group_ID !='::dirty') & (Erad_Endpoint.Group_ID !='::free')):
		yield endpoint


def find_orphanded_endpoint_id():
	groups_index = get_groups_index()
	for endpoint in get_endpoitns():
		if endpoint.Group_ID not in groups_index:
			yield (endpoint.IpAddress, endpoint.Port), endpoint.Group_ID


def free_orphaned_ports():
	for (ip, port), group_id in find_orphanded_endpoint_id():
		endpoint = Erad_Endpoint.get(ip, port)
		assert group_id == endpoint.Group_ID
		print('Freeing: {0}'.format(repr(endpoint)))
		endpoint.Group_ID = '::dirty'
		endpoint.Secret = util.generate_random_string(size=10)
		endpoint.save()


def update_radiusconf_timestamp(s3_bucket_name):
	timestamp = str(int(time.time()))

	# force using credentials from boto config file
	s3_connection = boto.s3.connect_to_region(
		'us-west-2',
		calling_format=boto.s3.connection.OrdinaryCallingFormat()
		)

	bucket = s3_connection.get_bucket(s3_bucket_name, validate=False)
	key = bucket.get_key('radiusconf')

	if key is None:
		key = bucket.new_key('radiusconf')
	key.set_contents_from_string(timestamp)
	s3_connection.close()


def main():
	parser = argparse.ArgumentParser(description='Find or/and delete orphaned ERAD Endpoints (ports)')
	parser.add_argument(
		'command',
		type=str,
		choices=['show', 'remove', 'update_cfg'],
		help='Choose what to do')

	parser.add_argument(
		'--s3-bucket-name',
		type=str,
		help='S3 bucket name where timestamp file located')

	args = parser.parse_args()

	if args.command == 'show':
		count = 0
		for count, item in enumerate(find_orphanded_endpoint_id(), start=1):
			print(item)
		print('Total amount: {0}'.format(count))
		return
	if args.command == 'remove':
		free_orphaned_ports()
		return

	if args.command == 'update_cfg':
		s3_bucket_name = args.s3_bucket_name
		if s3_bucket_name is None:
			print('--s3-bucket-name should be specified')
			return

		update_radiusconf_timestamp(s3_bucket_name)
		return


if __name__ == '__main__':
	main()
