from erad.api.erad_model import Erad_Endpoint
from erad.api.erad_model import Erad_Group2
from main import find_orphanded_endpoint_id
from main import free_orphaned_ports
from pynamodb.exceptions import DoesNotExist
import unittest


def is_safe_environment():

	if "amazonaws.com" in Erad_Endpoint.Meta.host:
		return False
	return True

def clear_data():
	if not is_safe_environment():
		print "This smells like production, refusing to remove database!"
		return

	if Erad_Group2.exists():
		Erad_Group2.delete_table()
		Erad_Group2.create_table(wait=True)
	if Erad_Group2.exists():
		Erad_Endpoint.delete_table()
		Erad_Endpoint.create_table(wait=True)



class SearchOrphaneddEndpoins(unittest.TestCase):
	def setUp(self):
		clear_data()

		Erad_Group2(Account_ID='A1', ID='g1').save()

		Erad_Endpoint(
			IpAddress='1.1.1.1',
			Port='1',
			Group_ID='A1::g1',
			ClusterId='c1',
			Secret='whatever',
			Region='us-west-2').save()

		# should be freed and marked as ::dirty
		Erad_Endpoint(
			IpAddress='1.1.1.1',
			Port='10',
			Group_ID='A1::g2',
			ClusterId='c1',
			Secret='whatever',
			Region='us-west-2').save()

		# should be freed and marked as ::dirty
		Erad_Endpoint(
			IpAddress='1.1.1.1',
			Port='11',
			Group_ID='A2::g1',
			ClusterId='c1',
			Secret='whatever',
			Region='us-west-2').save()

		Erad_Endpoint(
			IpAddress='1.1.1.2',
			Port='2',
			Group_ID='::free',
			ClusterId='c1',
			Secret='whatever',
			Region='us-west-2').save()

		Erad_Endpoint(
			IpAddress='1.1.1.3',
			Port='3',
			Group_ID='::dirty',
			ClusterId='c1',
			Secret='whatever',
			Region='us-west-2').save()

	def tearDown(self):
		group_ids = [
			('A1','g1'),
		]
		for (acc_id, group_id) in group_ids:
			Erad_Group2.get(acc_id, group_id).delete()

		endpoint_ids = [
			('1.1.1.1', '1'),
			('1.1.1.1', '10'),
			('1.1.1.1', '11'),
			('1.1.1.2', '2'),
			('1.1.1.3', '3'),
		]

		for (ip, port) in endpoint_ids:
			Erad_Endpoint(ip, port).delete()


	def test_find_orphanded_endpoint_id(self):
		result = [x for x in find_orphanded_endpoint_id()]
		expected = [
			((u'1.1.1.1', u'10'), u'A1::g2'),
			((u'1.1.1.1', u'11'), u'A2::g1')
		]
		self.assertEqual(expected, result)

	def test_free_orphaned_ports(self):
		free_orphaned_ports()

		endpoint_ids = [
			(('1.1.1.1', '1'),  'A1::g1'),
			(('1.1.1.1', '10'), '::dirty'),
			(('1.1.1.1', '11'), '::dirty'),
			(('1.1.1.2', '2'),  '::free'),
			(('1.1.1.3', '3'),  '::dirty'),
		]

		for (ip, port), group_id in endpoint_ids:
			endpoint = Erad_Endpoint.get(ip, port)
			self.assertEqual(group_id, endpoint.Group_ID)



