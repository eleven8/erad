0. Use python 2.7 (same as ERAD API) and go to `src/tools/search_orphaned_ports` directory
1. Set PYTHONPATH so script will know where to find `erad` module
2. Use this command to print orphaned endpoints

```bash
python main.py show
```
3. Use this command to "free" orphaned endpoints

```bash
python main.py remove
```

4. Use this command to "update" radius config timestamp

```bash
python main.py update_cfg --s3-bucket-name <s3-bucket-name>
```

To run tests use this command

```bash
python -m unittest test
```

