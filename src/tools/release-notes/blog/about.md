---
layout: page
title: About
permalink: /about/
---

This web site is used to publish release notes and other notices about the Enterprise Authentication system hosted at enterpriseauth.com.

Every release is versioned using semantic versioning. Version numbers follow the format of MAJOR.MINOR.PATCH.MAINTENANCE.

The MAJOR version is incremented when we make API changes that are not backwards-compatible, significant UI changes, or release a highly-anticipated feature.

The MINOR version is incremented when we make any other changes that are backwards-compatible and affect users, except bug fixes.

The PATCH version is incremented when we make backwards-compatible bug fixes.

The MAINTENANCE version is incremented when we make internal changes that do not affect users. Some examples include system improvements, changes in code organization, and bug fixes.

This site provides an RSS feed to subscribe to all release notes or a specific category of note such as major releases only.

Also, we occasionally post notices, which are informational notes unrelated to a release. They may provide information about an upcoming release, maintenance work, an outage recap, or changes to other related software.
