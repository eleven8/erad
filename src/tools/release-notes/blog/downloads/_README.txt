Instead of bloating the source code repository with media files, upload them directly to s3://notes.enterpriseauth.com/download

Remember to make the files public after uploading.
