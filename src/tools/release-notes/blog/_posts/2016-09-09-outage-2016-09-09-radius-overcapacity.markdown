---
layout: post
title:  "Outage: 2016-09-09 Radius overcapacity"
date:   2016-09-09 00:00:00 +0000
categories: Notice
---
#### Root Cause:
1. Overcapacity caused our RADIUS servers to run out of operational memory. We received 100x more accounting requests per second than usual.

#### Mitigation:
1. Redeployed RADIUS servers.
1. We are considering splitting authentication and accounting servers to avoid this type of outage in the future.

#### Statistics:
Outage Duration: 4 minutes

Previous Uptime (2016): 99.989%

New Uptime (2016): 99.989%

Days Since Last Outage: 28

Outage Start: 11:17 PDT

Outage Stop: 11:21 PDT

