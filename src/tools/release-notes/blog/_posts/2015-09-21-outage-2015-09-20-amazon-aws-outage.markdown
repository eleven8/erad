---
layout: post
title:  "Outage: 2015-09-20 Amazon AWS Outage"
date:   2015-09-21 20:02:00 +0000
categories: Notice
---
#### Root Cause:
1. Amazon experienced a widespread outage to critical systems including EC2 instances, DynamoDB, and CloudWatch.
1. This was a massive outage, effecting not only Amazon's own shopping cart, FireTV, and Echo but also other major sites such as Netflix, Reddit, Airbnb, iMDB, and many others.
1. The details of why this happened have yet to be released. During the event, Amazon was claiming the effects were being felt in the US-EAST-1 region but our DynamoDB database in the US-WEST-2 region was impacted.

#### Mitigation:
1. There was nothing we could do except wait for Amazon to remedy the situation. Once they did, all of our systems came back online.

#### Statistics:
Outage Duration: 80 minutes

Previous Uptime (2015): 100.000%

New Uptime (2015): 99.985%

Days Since Last Outage: n/a (first)

Outage Start: 06:50 PDT

Outage Stop: 08:10 PDT
