---
layout: post
title:  "3.8.0.0"
date:   2016-02-18 23:59:00 +0000
categories: Minor
---
* Added new web methods for interacting with ApiKeys.
  * /erad/admin/apikey/list
  * /erad/admin/apikey/load
  * /erad/admin/apikey/save
  * /erad/admin/apikey/delete
