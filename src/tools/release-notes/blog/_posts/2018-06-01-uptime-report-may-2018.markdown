---
layout: post
title:  "Uptime Report: May 2018"
date:   2018-06-01 00:00:00 +0000
categories: Notice
---
**Monthly**

| Year | Month | Outages | Uptime | Downtime |
| :--- | :--- | ---: | ---: | :--- |
| 2018 | May | 1 | 99.982% | 0 hrs, 8 mins |
| 2018 | April | 0 | 100.00% | 0 hrs, 0 mins |
| 2018 | March | 0 | 100.00% | 0 hrs, 0 mins |
| 2018 | February | 0 | 100.00% | 0 hrs, 0 mins |
| 2018 | January | 1 | 99.973% | 0 hrs, 12 mins |

<br>

**Yearly**

| Year | Outages | Uptime | Downtime |
| :--- | ---: | ---: | :--- |
| 2018 | 2 | 99.996% | 0 hrs, 20 mins |
| 2017 | 3 | 99.998% | 0 hrs, 12 mins |
| 2016 | 8 | 99.989% | 1 hrs, 0 mins |
| 2015 | 2 | 99.98% | 1 hrs, 29 mins |
