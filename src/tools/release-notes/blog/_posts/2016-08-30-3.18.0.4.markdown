---
layout: post
title:  "3.18.0.4"
date:   2016-08-30 18:06:00 +0000
categories: Maintenance
---
* Further preparation work for offering individual RADIUS ports per site. Now successful authorization is possible only on assigned ports.