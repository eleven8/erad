---
layout: post
title:  "Outage: 2016-06-16 API Server Offline"
date:   2016-06-16 00:00:00 +0000
categories: Notice
---
#### Root Cause:
1. One of the API servers went offline. The load balancer did not recognize that the server was down and continued sending traffic to it.

#### Mitigation:
1. Manually removed the server from the load balancer. This resolved the outage.
1. We are already in the process of moving to a better load balancer. During this outage, we verified that the new load balancer correctly detected the offline server and removed it from the pool. We need to finish the task of switching systems over to use the new load balancer.

#### Statistics:
Outage Duration: 7 minutes

Previous Uptime (2016): 99.9998%

New Uptime (2016): 99.998%

Days Since Last Outage: 22

Outage Start: 10:29 PDT

Outage Stop: 10:36 PDT
