---
layout: post
title:  "Outage: 2016-07-29 Radius overcapacity"
date:   2016-07-29 00:00:00 +0000
categories: Notice
---
#### Root Cause:
1. Overcapacity caused our RADIUS servers to run out of operational memory.

#### Mitigation:
1. Enabled swap on all RADIUS servers.


#### Statistics:
Outage Duration: 9 minutes

Previous Uptime (2016): 99.997%

New Uptime (2016): 99.996%

Days Since Last Outage: 2

Outage Start: 16:43 PDT, 16:58 PDT

Outage Stop: 16:46 PDT, 17:04 PDT

