---
layout: post
title:  "Uptime Report: August 2016"
date:   2016-09-01 00:00:00 +0000
categories: Notice
---
**Monthly**

| Year | Month | Outages | Uptime | Downtime |
| :--- | :--- | ---: | ---: | :--- |
| 2016 | August | 2 | 99.957% | 0 hrs, 19 mins |
| 2016 | July | 3 | 99.935% | 0 hrs, 29 mins |
| 2016 | June | 1 | 99.984% | 0 hrs, 7 mins |
| 2016 | May | 1 | 99.998% | 0 hrs, 1 mins |
| 2016 | April | 0 | 100.00% | 0 hrs, 0 mins |
| 2016 | March | 0 | 100.00% | 0 hrs, 0 mins |
| 2016 | February | 0 | 100.00% | 0 hrs, 0 mins |
| 2016 | January | 0 | 100.00% | 0 hrs, 0 mins |

<br>

**Yearly**

| Year | Outages | Uptime | Downtime |
| :--- | ---: | ---: | :--- |
| 2016 | 7 | 99.989% | 0 hrs, 56 mins |
| 2015 | 2 | 99.98% | 1 hrs, 29 mins |
