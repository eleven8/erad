---
layout: post
title:  "Uptime Report: May 2017"
date:   2017-06-01 00:00:00 +0000
categories: Notice
---
**Monthly**

| Year | Month | Outages | Uptime | Downtime |
| :--- | :--- | ---: | ---: | :--- |
| 2017 | May | 1 | 99.998% | 0 hrs, 1 mins |
| 2017 | April | 0 | 100.00% | 0 hrs, 0 mins |
| 2017 | March | 0 | 100.00% | 0 hrs, 0 mins |
| 2017 | February | 1 | 99.989% | 0 hrs, 5 mins |
| 2017 | January | 0 | 100.00% | 0 hrs, 0 mins |

<br>

**Yearly**

| Year | Outages | Uptime | Downtime |
| :--- | ---: | ---: | :--- |
| 2017 | 2 | 99.999% | 0 hrs, 6 mins |
| 2016 | 8 | 99.989% | 1 hrs, 0 mins |
| 2015 | 2 | 99.98% | 1 hrs, 29 mins |
