---
layout: post
title:  "Outage: 2016-05-26 Load Balancer Mistake"
date:   2016-05-27 00:00:00 +0000
categories: Notice
---
#### Root Cause:
1. The load balancer marked all RADIUS servers as offline although they were not.

#### Mitigation:
1. Deployed a new load balancer. This resolved the outage.
1. We will update the load balancer offline detection code to never remove the last RADIUS server so that a false alert like this won't cause a real outage.

#### Statistics:
Outage Duration: 1 minute

Previous Uptime (2016): 100.000%

New Uptime (2016): 99.9998%

Days Since Last Outage: 244

Outage Start: 23:03 PDT

Outage Stop: 23:04 PDT
