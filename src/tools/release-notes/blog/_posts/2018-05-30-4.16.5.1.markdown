---
layout: post
title:  "4.16.5.1"
date:   2018-05-30 14:15:00 +0000
categories: Maintenance
---
* Deploying a watcher for the RADIUS process to avoid outages similar to the one experienced on 2018-05-22. The watcher will restart the RADIUS process if it terminates unexpectedly.
