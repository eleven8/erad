---
layout: post
title:  "Uptime Report: December 2017"
date:   2018-01-01 00:00:00 +0000
categories: Notice
---
**Monthly**

| Year | Month | Outages | Uptime | Downtime |
| :--- | :--- | ---: | ---: | :--- |
| 2017 | December | 0 | 100.00% | 0 hrs, 0 mins |
| 2017 | November | 0 | 100.00% | 0 hrs, 0 mins |
| 2017 | October | 0 | 100.00% | 0 hrs, 0 mins |
| 2017 | September | 1 | 99.987% | 0 hrs, 6 mins |
| 2017 | August | 0 | 100.00% | 0 hrs, 0 mins |
| 2017 | July | 0 | 100.00% | 0 hrs, 0 mins |
| 2017 | June | 0 | 100.00% | 0 hrs, 0 mins |
| 2017 | May | 1 | 99.998% | 0 hrs, 1 mins |
| 2017 | April | 0 | 100.00% | 0 hrs, 0 mins |
| 2017 | March | 0 | 100.00% | 0 hrs, 0 mins |
| 2017 | February | 1 | 99.989% | 0 hrs, 5 mins |
| 2017 | January | 0 | 100.00% | 0 hrs, 0 mins |

<br>

**Yearly**

| Year | Outages | Uptime | Downtime |
| :--- | ---: | ---: | :--- |
| 2017 | 3 | 99.998% | 0 hrs, 12 mins |
| 2016 | 8 | 99.989% | 1 hrs, 0 mins |
| 2015 | 2 | 99.98% | 1 hrs, 29 mins |
