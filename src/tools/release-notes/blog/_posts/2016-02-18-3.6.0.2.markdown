---
layout: post
title:  "3.6.0.2"
date:   2016-02-18 16:28:00 +0000
categories: Maintenance
---
* Changes to the automated load balancer failover system.