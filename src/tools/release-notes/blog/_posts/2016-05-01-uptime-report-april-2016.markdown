---
layout: post
title:  "Uptime Report: April 2016"
date:   2016-05-01 00:00:00 +0000
categories: Notice
---
**Monthly**

| Year | Month | Outages | Uptime | Downtime |
| :--- | :--- | ---: | ---: | :--- |
| 2016 | April | 0 | 100.00% | 0 hrs, 0 mins |
| 2016 | March | 0 | 100.00% | 0 hrs, 0 mins |
| 2016 | February | 0 | 100.00% | 0 hrs, 0 mins |
| 2016 | January | 0 | 100.00% | 0 hrs, 0 mins |

<br>

**Yearly**

| Year | Outages | Uptime | Downtime |
| :--- | ---: | ---: | :--- |
| 2016 | 0 | 100.00% | 0 hrs, 0 mins |
| 2015 | 2 | 99.98% | 1 hrs, 29 mins |
