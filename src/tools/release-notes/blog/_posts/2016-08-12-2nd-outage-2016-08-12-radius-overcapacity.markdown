---
layout: post
title:  "Outage: 2016-08-12 Radius overcapacity"
date:   2016-08-12 00:00:01 +0000
categories: Notice
---
#### Root Cause:
1. Overcapacity caused our RADIUS servers to run out of operational memory. We received 100x more accounting requests per second than usual.

#### Mitigation:
1. Deployed additional RADIUS servers.
1. We had enabled an option allowing for accounting packet throttling but it was not sufficient.


#### Statistics:
Outage Duration: 7 minutes

Previous Uptime (2016): 99.991%

New Uptime (2016): 99.989%

Days Since Last Outage: 0

Outage Start: 17:11 PDT

Outage Stop: 17:18 PDT

