---
layout: post
title:  "2.1.0.0"
date:   2016-01-04 00:00:00 +0000
categories: Minor
---
* System-wide performance improvement (speed) of about 80%.
