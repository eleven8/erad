---
layout: post
title:  "Outage: 2015-09-26 Load Balancer Failure"
date:   2015-09-28 21:40:00 +0000
categories: Notice
---
#### Root Cause:
1. Our load balancer became unreachable.
1. We knowingly architected the system so that the load balancer is a single point of failure. If stability of Amazon servers is a concern, we can take additional measures to ensure a faster recovery.

#### Mitigation:
1. Deployed a new load balancer. This resolved the outage.
1. We are considering whether we need to have a warm failover load balancer up and ready. This might trim 3-5 minutes off of recovery time.
1. We are considering whether we need to have an automated failover process to detect this situation and failover to a standby load balancer. This might reduce outages to under a minute but has the risk of false positives.

#### Statistics:
Outage Duration: 9 minutes

Previous Uptime (2015): 99.985%

New Uptime (2015): 99.983%

Days Since Last Outage: 7

Outage Start: 19:05 PDT

Outage Stop: 19:14 PDT
