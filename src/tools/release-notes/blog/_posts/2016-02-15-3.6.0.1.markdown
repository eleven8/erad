---
layout: post
title:  "3.6.0.1"
date:   2016-02-15 22:28:00 +0000
categories: Maintenance
---
* A standby load balancer is now deployed as part of the standard stack.
* Deployed an automated failover system that monitors the primary load balancer and, if connectivity is lost, will automatically failover to the standby.