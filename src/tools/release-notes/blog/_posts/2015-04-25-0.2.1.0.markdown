---
layout: post
title:  "0.2.1.0"
date:   2015-04-25 00:00:00 +0000
categories: Patch
---
* Fixed some minor bugs in the API.
