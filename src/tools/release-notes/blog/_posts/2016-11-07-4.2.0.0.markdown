---
layout: post
title:  "4.2.0.0"
date:   2016-11-07 21:45:00 +0000
categories: Minor
---
* The system now allows provisioning a unique, random, RADIUS port and secret key on a per-site basis. When a site has a port provisioned, it is not required to setup called-station-ids or nas-identifiers as all requests would be assigned to this site. The account owner can provision a port through the UI on the 'Edit Site Configuration' modal as shown on the screenshot below. Deprovisioning of a port is possible by clicking the trash icon.

![](/downloads/20161107-port-provisioning.png)