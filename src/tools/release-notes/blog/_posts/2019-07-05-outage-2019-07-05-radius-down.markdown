---
layout: post
title:  "Outage: 2019-07-05 RADIUS outage in US East region, other regions unaffected"
date:   2019-07-05 00:00:00 +0000
categories: Notice
---
#### Root Cause:
1. Load balancer server failure.

#### Mitigation:
1. Switched over to standby load balancer server.

#### Statistics:
Outage Duration: 5 minutes

Previous Uptime (2019): 100.000%

New Uptime (2019): 99.999%

Days Since Last Outage: 349

Outage Start: 13:17 PDT

Outage Stop: 13:22 PDT

