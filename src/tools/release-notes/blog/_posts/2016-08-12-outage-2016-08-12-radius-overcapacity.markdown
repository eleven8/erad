---
layout: post
title:  "Outage: 2016-08-12 Radius overcapacity"
date:   2016-08-12 00:00:00 +0000
categories: Notice
---
#### Root Cause:
1. Overcapacity caused our RADIUS servers to run out of operational memory. We received 100x more accounting requests per second than usual.

#### Mitigation:
1. Deployed additional RADIUS servers.
1. We are considering enabling an option allowing for accounting packet throttling which should protect against traffic spikes.


#### Statistics:
Outage Duration: 12 minutes

Previous Uptime (2016): 99.993%

New Uptime (2016): 99.991%

Days Since Last Outage: 14

Outage Start: 07:14 PDT

Outage Stop: 07:26 PDT

