---
layout: post
title:  "Notice: Backfilled Version Numbers"
date:   2016-01-26 13:00:00 +0000
categories: Notice
---
The version numbers up to 3.1.0.1 were backfilled to give an overview of the history of the project up to that time. It is not an exhaustive history but rather one to capture important dates and events. Many minor releases and all maintenance releases are left out before version 3.1.0.1. Every production change is recorded starting at version 3.1.0.1. 
