---
layout: post
title:  "Outage: 2016-07-27 Radius ports other than 1812 down"
date:   2016-07-27 00:00:00 +0000
categories: Notice
---
#### Root Cause:
1. A software update caused all RADIUS ports other than port 1812 to go down. Port 1812 continued handling traffic as usual.

#### Mitigation:
1. Manually reverted to the previous version. This resolved the outage.
1. Currently, our pre-deployment tests only check port 1812. We will add additional tests to check other ports. Checking any port other than 1812 would have prevented this outage.

#### Statistics:
Outage Duration: 6 minutes

Previous Uptime (2016): 99.998%

New Uptime (2016): 99.997%

Days Since Last Outage: 41

Outage Start: 11:07 PDT

Outage Stop: 11:13 PDT
