---
layout: post
title:  "3.11.4.0"
date:   2016-04-05 16:57:00 +0000
categories: Patch
---
* Bug fix: If a new device was added with the same username as an existing device, the new device would overwrite the existing device without warning. Now a device cannot be saved with a username that already exists.