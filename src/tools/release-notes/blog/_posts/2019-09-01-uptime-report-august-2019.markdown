---
layout: post
title:  "Uptime Report: August 2019"
date:   2019-09-01 00:00:00 +0000
categories: Notice
---
**Monthly**

| Year | Month | Outages | Uptime | Downtime |
| :--- | :--- | ---: | ---: | :--- |
| 2019 | August | 0 | 100.00% | 0 hrs, 0 mins |
| 2019 | July | 1 | 99.989% | 0 hrs, 5 mins |
| 2019 | June | 0 | 100.00% | 0 hrs, 0 mins |
| 2019 | May | 0 | 100.00% | 0 hrs, 0 mins |
| 2019 | April | 0 | 100.00% | 0 hrs, 0 mins |
| 2019 | March | 0 | 100.00% | 0 hrs, 0 mins |
| 2019 | February | 0 | 100.00% | 0 hrs, 0 mins |
| 2019 | January | 0 | 100.00% | 0 hrs, 0 mins |

<br>

**Yearly**

| Year | Outages | Uptime | Downtime |
| :--- | ---: | ---: | :--- |
| 2019 | 1 | 99.999% | 0 hrs, 5 mins |
| 2018 | 3 | 99.995% | 0 hrs, 26 mins |
| 2017 | 3 | 99.998% | 0 hrs, 12 mins |
| 2016 | 8 | 99.989% | 1 hrs, 0 mins |
| 2015 | 2 | 99.98% | 1 hrs, 29 mins |
