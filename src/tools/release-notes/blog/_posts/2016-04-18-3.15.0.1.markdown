---
layout: post
title:  "3.15.0.1"
date:   2016-04-18 21:20:00 +0000
categories: Maintenance
---
* Added eapol_test to RADIUS servers for diagnostic purposes.