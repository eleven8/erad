---
layout: post
title:  "Outage: 2018-01-31 All systems down"
date:   2018-01-31 00:00:00 +0000
categories: Notice
---
#### Root Cause:
1. A misconfiguration of the SSL certificate for the API load balancer caused all requests to be rejected.

#### Mitigation:
1. Fixed the configuration.

#### Statistics:
Outage Duration: 12 minutes

Previous Uptime (2018): 100%

New Uptime (2018): 99.998%

Days Since Last Outage: 138

Outage Start: 16:00 PST

Outage Stop: 16:12 PST

