---
layout: post
title:  "3.8.0.4"
date:   2016-02-26 18:14:00 +0000
categories: Maintenance
---
* Replaced Comodo SSL certificate for www.enterpriseauth.com (API) with a Thawte extended verification (EV) SSL certificate.
