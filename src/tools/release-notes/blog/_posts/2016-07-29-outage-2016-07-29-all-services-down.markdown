---
layout: post
title:  "Outage: 2016-07-29 All services down"
date:   2016-07-29 00:00:01 +0000
categories: Notice
---
#### Root Cause:
1. Responding to the previous outage resulted in an unexpected state, which caused our automated deployment system to identify our production servers as development servers and shut them down.

#### Mitigation:
1. Redeployed production stack.


#### Statistics:
Outage Duration: 14 minutes

Previous Uptime (2016): 99.996%

New Uptime (2016): 99.993%

Days Since Last Outage: 0

Outage Start: 18:31 PDT

Outage Stop: 18:45 PDT

