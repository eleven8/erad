#!/usr/bin/env bash

yum -y update
yum -y install \
	python3

cd /usr/local/src
pip3 install .

