import pytest
import ports_provision
from os import path
from os import remove
from ports_provision import SiteList
from tempfile import mkstemp
from io import StringIO

cwd = path.dirname(__file__)


def tests_custom_csv_parse():
	test_file_name = path.join(cwd, 'test_csv_file.csv')
	results = ports_provision.custom_csv_parse(test_file_name)
	expected = [
		SiteList(ID='AA-020-25', Name='Hampton Inn Wetumpka (MGMWA)'),
		SiteList(ID='AA-216-55', Name='Hampton Inn & Suites Westford-Chelmsford (BOSWF)'),
		SiteList(ID='AA-244-43', Name='Home2 Suites by Hilton - Blue Ash Cincinnati, OH (CVGAS)')
	]

	assert expected == results


def port_provision_generator():
	results = [
		{
			'site': SiteList(ID='AA-020-25', Name='Hampton Inn Wetumpka (MGMWA)'),
			'us-west-2': {
				"IpAddress": "52.26.34.201",
				"Port": "20140",
				"Region": "us-west-2",
				"Secret": "4905DVCA3B"
			},
			'us-east-2': {
				"IpAddress": "34.192.196.106",
				"Port": "20072",
				"Region": "us-east-2",
				"Secret": "4905DVCA3B"
			}

		},
		{
			'site': SiteList(ID='AA-216-55', Name='Hampton Inn & Suites Westford-Chelmsford (BOSWF)'),
			'us-west-2': {
				"IpAddress": "52.26.34.202",
				"Port": "20142",
				"Region": "us-west-2",
				"Secret": "4905DVCA3B"
			},
			'us-east-2': {
				"IpAddress": "34.192.196.108",
				"Port": "20074",
				"Region": "us-east-2",
				"Secret": "4905DVCA3B"
			}

		},
		{
			'site': SiteList(ID='AA-244-43', Name='Home2 Suites by Hilton - Blue Ash Cincinnati, OH (CVGAS)'),
			'us-west-2': {
				"IpAddress": "52.26.34.203",
				"Port": "20144",
				"Region": "us-west-2",
				"Secret": "4905DVCA3B"
			},
			'us-east-2': {
				"IpAddress": "34.192.196.109",
				"Port": "20076",
				"Region": "us-east-2",
				"Secret": "4905DVCA3B"
			}

		}
	]
	for result in results:
		yield result


def test_write_output():
	(fd, filename) = mkstemp()
	results_generator = port_provision_generator()
	ports_provision.write_output(results_generator, filename)
	iobuffer = StringIO()

	with open(filename, 'r') as f:
		iobuffer.write(f.read())

	expected = '''ID,Region,IP,Auth Port,Acct Port,Secret,Region,IP,Auth Port,Acct Port,Secret,Name
AA-020-25,us-west-2,52.26.34.201,20140,20141,4905DVCA3B,us-east-2,34.192.196.106,20072,20073,4905DVCA3B,Hampton Inn Wetumpka (MGMWA)
AA-216-55,us-west-2,52.26.34.202,20142,20143,4905DVCA3B,us-east-2,34.192.196.108,20074,20075,4905DVCA3B,Hampton Inn & Suites Westford-Chelmsford (BOSWF)
AA-244-43,us-west-2,52.26.34.203,20144,20145,4905DVCA3B,us-east-2,34.192.196.109,20076,20077,4905DVCA3B,"Home2 Suites by Hilton - Blue Ash Cincinnati, OH (CVGAS)"
'''
	assert expected == iobuffer.getvalue()
	remove(filename)

