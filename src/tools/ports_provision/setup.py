# coding: utf-8
import setuptools

setuptools.setup(
	name="ports_provision",
	version="0.1",
	author="",
	author_email="",
	description="Utility to provision ERAD site from csv file",
	url="",
	install_requires=['requests'],
	py_modules=['ports_provision'],
	classifiers=(
		"Programming Language :: Python :: 3",
		"Operating System :: Tested only on Linux",
	),
)
