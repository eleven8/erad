
### How to install,

#### Using AWS CloudFormation

1. To pack source code, run these commands

```bash
cd <path to erad>/erad/src/tools
tar -czf deploy.tgz -C ports_provision .
```

2. Upload `deploy.tgz` to s3 bucket
3. Use `standard-stack` template to deploy file

#### Manually

1. Pack code and copy it to the target machine, (see example in previous paragraph). Or copy `<path to erad>/erad/src/tools/ports_provision` directly to the target machine/directory
2.  Assuming python3 and pip are installed, if not install it. Use `virtualev` if needed.
3. Go to `ports_provision` directory and setup tool using this command
```bash
pip install .
```


### How to run

Use this command

```bash
python3 -m ports_provision  \
	--src-file <path_to_input_file> \
	--result-file <path_to_file_with_results> \
	--api-endpoint '<url_to_erad_api>' \
	--session-id '<session_id_value>'
```

Example with real values

```bash
python3 -m ports_provision  \
	--src-file ./tests/test_csv_file.csv \
	--result-file ./results.csv \
	--api-endpoint 'http://localhost:5000' \
	--session-id 'dhhq7yn2mv6c1rrg48up67ur2y1zbs4p'
```

Pressing Ctrl+C during execution will instruct program to abort execution but only after currently provisioning site will be finished.
