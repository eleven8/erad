#!/bin/env python3

import argparse
import csv
import logging
from collections import namedtuple
from os import path
import requests
import signal

SiteList = namedtuple('SiteList', ['ID', 'Name'])

LOGGER_FORMAT = '%(asctime)-15s:%(name)s : line# %(lineno)s : %(message)s'

logging.basicConfig(
	level=logging.INFO,
	format=LOGGER_FORMAT
)
logger = logging.getLogger(__name__)

process_was_aborted = False


def abort_handler(signum, frame):
	global process_was_aborted
	logger.info('Abort was required')
	process_was_aborted = True


def custom_csv_parse(file_name):
	sites_list = []
	with open(file_name, 'r') as custom_csv_file:
		for (count, line) in enumerate(custom_csv_file, start=1):
			site_id, site_name = map(lambda x: x.strip(), line.split(',', 1))
			if len(site_id) == 0 or len(site_name) == 0:
				raise Exception('Site id or site name can not be empty. Line with error: {0}'.format(count))
			sites_list.append(SiteList(ID=site_id, Name=site_name))
		custom_csv_file.close()

		if len(sites_list) != count:
			raise Exception('Parsed sites amount [{0}] does not match with lines amount [{1}] in input file'.format(
				len(sites_list),
				count
			))
	logger.info('Parsed {0} sites'.format(len(sites_list)))
	return sites_list


def get_new_group_payload(session_id, site_name, site_id):
	url_path = '/erad/admin/group/save'
	payload = {
		'Session': {
			'ID': session_id
		},
		'Group': {
			'Name': site_name,
			'ID': site_id,
			'RemoteServerUrl': '',
			'SharedSecret': ''
		}
	}
	return url_path, payload


def get_new_endpoint_payload(session_id, site_id, region):
	url_path = '/erad/admin/endpoint/save'
	payload = {
		'Session': {
			'ID': session_id
		},
		'Group_ID': site_id,
		'Region': region
	}
	return url_path, payload


def port_provision_generator(api_endpoint, session_id, sites_list):
	global process_was_aborted
	regions = {
		'us-west-2',
		'us-east-2'
	}

	total_sites = len(sites_list)
	try:
		for count, site in enumerate(sites_list, start=1):
			logger.debug(process_was_aborted)
			if process_was_aborted is True:
				return
			cur_site = {}
			(url_path, payload) = get_new_group_payload(session_id, site.Name, site.ID)
			url = api_endpoint + url_path
			post_result = requests.post(url, json=payload)

			if post_result.status_code != 200 or post_result.json()['Error'] is not None:
				logger.error('Stopped procesisng on Site id: {0}'. format(site.ID))
				logger.error(post_result.text)
				raise Exception('API Error happens')

			cur_site['site'] = site
			cur_site['Group'] = post_result.json()['Group']

			for region in regions:
				(url_path, payload) = get_new_endpoint_payload(session_id, site.ID, region)
				url = api_endpoint + url_path
				post_result = requests.post(url, json=payload)

				if post_result.status_code != 200 or post_result.json()['Error'] is not None:
					logger.error('Stopped procesisng on Site id: {0} and region {1}'.format(site.ID, region))
					logger.error(post_result.text)
					raise Exception('API Error happens')

				json_response = post_result.json()
				cur_site[json_response['Endpoint']['Region']] = json_response['Endpoint']

			logger.info('{0}/{1} sites were provisioned, {2}'.format(count, total_sites, site.ID))
			yield cur_site

	except Exception as err:
		logger.exception(err)


def build_csv_row(site):
	return [
		site['site'].ID,
		site['us-west-2']['Region'],
		site['us-west-2']['IpAddress'],
		site['us-west-2']['Port'],
		str(int(site['us-west-2']['Port']) + 1),
		site['us-west-2']['Secret'],
		site['us-east-2']['Region'],
		site['us-east-2']['IpAddress'],
		site['us-east-2']['Port'],
		str(int(site['us-east-2']['Port']) + 1),
		site['us-east-2']['Secret'],
		site['site'].Name,
	]


def write_output(results_generator, output_file_name):
	fieldnames = [
		'ID',
		'Region',
		'IP',
		'Auth Port',
		'Acct Port',
		'Secret',
		'Region',
		'IP',
		'Auth Port',
		'Acct Port',
		'Secret',
		'Name'
	]

	with open(output_file_name, 'w') as output_csvfile:
		writer = csv.writer(
			output_csvfile,
			delimiter=',',
			quoting=csv.QUOTE_MINIMAL,
			lineterminator='\n'
		)
		writer.writerow(fieldnames)

		for site in results_generator:
			row = build_csv_row(site)
			writer.writerow(row)


def get_file_names(infile, outfile):
	return path.realpath(infile), path.realpath(outfile)


def main():
	parser = argparse.ArgumentParser(description='Provision ERAD sites from csv file')
	required = parser.add_argument_group('required named arguments')
	required.add_argument(
		'--src-file',
		metavar='<filename:str>',
		type=str,
		help='Source file',
		required=True)
	required.add_argument(
		'--result-file',
		metavar='<filename:str>',
		type=str,
		help='File for results',
		required=True)

	required.add_argument(
		'--api-endpoint',
		metavar='<api_endpoint:str>',
		type=str,
		help='API endpoint (url)',
		required=True)

	required.add_argument(
		'--session-id',
		metavar='<session_id:str>',
		type=str,
		help='ERAD session id',
		required=True)
	args = parser.parse_args()

	logger.info('Ctrl+C , will abort program execution after current processed site will be provisioned with ports')
	input_file_name, output_file_name = get_file_names(args.src_file, args.result_file)

	api_endpoint = args.api_endpoint.rstrip('/')

	logger.info(input_file_name)
	logger.info(output_file_name)
	logger.info(args.api_endpoint)
	logger.info(args.session_id)

	sites_list = custom_csv_parse(input_file_name)
	results_generator = port_provision_generator(
		api_endpoint=api_endpoint,
		session_id=args.session_id,
		sites_list=sites_list,
	)
	write_output(results_generator=results_generator, output_file_name=output_file_name)


if __name__ == '__main__':
	signal.signal(signal.SIGINT, abort_handler)
	main()
