#!/bin/sh  

echo "Installing Apache"
yum update -y
yum install -y httpd24 php56 mysql55-server php56-mysqlnd
echo "Copying Files..."
cp -r /usr/local/src/* /var/www/html


echo "Modify Server URLS"
pub_ip=$(dig +short myip.opendns.com @resolver1.opendns.com)
sed -i "s/52.35.4.34/$pub_ip/g" /var/www/html/js/dashboard.js


echo "Starting the Server"
service httpd start
