  var app = angular.module('myapp', ['chart.js', 'angularMoment', 'angularSpinner']);
  app.controller('mainCtrl', function($scope, $http, $window, $q, dataService, newService, usSpinnerService, $location) {

  $scope.groups = ["13A", "Eleven-Denver", "Global", "temp"];

  $scope.selectedGroup = $scope.groups[0];

  console.log($scope.selectedGroup);

  $scope.serverurl = $location.absUrl();

  $scope.groupChanged = function(input){
    usSpinnerService.spin('spinner-1');
    $scope.selectedGroup = input;
    newService.load($scope.selectedGroup ).then(function (logs) {
      console.log(logs);
      usSpinnerService.stop('spinner-1');
      // Get 7 days range back from current day
      $scope.dayslist = [];
      $scope.weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

      $scope.access_accept_no = [];
      $scope.access_reject_no = [];

      for(var i = 0; i<7; i++){
        var d = new Date();
        d.setDate(d.getDate()-i);
        var getDayNumber = d.getDay();
        var n = $scope.weekdays[getDayNumber];
        $scope.dayslist.push(n);

        var start_date = moment.utc(d).startOf('day').valueOf();
        var end_date = moment.utc(d).endOf('day').valueOf();

        var newa = [];
        for(var j = 0; j<logs.length; j++){
          var inst = logs[j].Instance.toString().slice(0, -1);
          if(inst >= start_date && inst <= end_date)
            newa.push(logs[j]);
        }

        // console.log(newa);
        var access_accept = 0;
        var access_reject = 0;
        for(var k = 0; k<newa.length; k++){
          if(newa[k].Access == "Access-Accept")
            access_accept++;
          else
            access_reject++;
        }

        $scope.access_accept_no[i] = access_accept;
        $scope.access_reject_no[i] = access_reject;
        
      }
      $scope.dayslist = $scope.dayslist.reverse();
      $scope.access_accept_no = $scope.access_accept_no.reverse();
      $scope.access_reject_no = $scope.access_reject_no.reverse();

      $scope.labels = $scope.dayslist;
      $scope.series = ['Access-Accept', 'Access-Reject'];
      $scope.data = [
        $scope.access_accept_no,
        $scope.access_reject_no
      ];
      $scope.onClick = function (points, evt) {
        console.log(points, evt);
      };
    });
  }

  // Chart 2 Dummy

  $scope.labels2 = ["Access-Accept", "Access-Reject"];
  $scope.data2 = [450, 99];
   
});


app.service('dataService', function ($http, $q){

  var start_date = moment.utc().subtract(7, 'days').startOf('day').valueOf();
  var end_date = moment.utc().endOf('day').valueOf();
  var interval_array = [start_date*10, end_date*10];

  var req = {
        "Session": 
        {
            "ID":"jsteeleSessionId"
            
        },
        "Radius_Log": 
        {
            "Group_ID": "Eleven-Denver",
            "Include_Stray": true,
            "Instance_Interval": interval_array
            
        }
        
    };

    var config = {
                headers : {
                    'Content-Type': 'application/json'
                }
            };

  var defferer = $q.defer();

  $http.post('https://api.enterpriseauth.com/erad/admin/supplicant/log/list', req, config).success(function (data){
    // console.log(data.Radius_Log);
    defferer.resolve(data.Radius_Log)
  })

  return defferer.promise
});

app.service('newService', function ($http, $q){

  this.load = function (group_id) {
    var start_date = moment.utc().subtract(7, 'days').startOf('day').valueOf();
    var end_date = moment.utc().endOf('day').valueOf();
    var interval_array = [start_date*10, end_date*10];

    var req = {
          "Session": 
          {
              "ID":"jsteeleSessionId"
              
          },
          "Radius_Log": 
          {
              "Group_ID": group_id,
              "Include_Stray": true,
              "Instance_Interval": interval_array
              
          }
          
      };

      var config = {
                  headers : {
                      'Content-Type': 'application/json'
                  }
              };

    var defferer = $q.defer();

    $http.post('https://api.enterpriseauth.com/erad/admin/supplicant/log/list', req, config).success(function (data){
      // console.log(data.Radius_Log);
      defferer.resolve(data.Radius_Log)
    })

    return defferer.promise;
  }
});