###Testing copy Erad_Authenticator2

Approximate table size `5.27` MB in bytes = `1024 * 1024 * 5.27 = 5525995.52 bytes`

Approximate items count int table `62000`

`5525995.52 / 62000 = 89.12895999999999
or ~ 90` bytes per record


`1 RCU` means `1 item/s` can be read, if item size `<= 4 KB`

`1 WCU` means `1 item/s` can be written, if item size `<= 1 KB`

If item in `read` operation has size `> 4 KB`, then to read this item AWS require more `RCU`. Required amount calculated by this expression

`Required RCU = ceil(item size / 4 KB)`

If item in `write` operation has size `> 1 KB`, then to write this item AWS require more `WCU`. Required amount calculated by this expression

`Required WCU = ceil(item size / 1 KB)`


So if provisioned `WCU == 1000` then in theory AWS allow to write `1000 items/s`

And full table copying will take `62 seconds`.

DynamoDB sometimes allow burst of writes, there is output from utility

```
...
Jobs 2495/2495 done
62375 records was copied in 62.116793632507324 seconds
[ec2-user@ip-172-31-29-29 ~]$
```

Full copy operation of table took `62` seconds. Although there was only `500 WCU` provisioned, that means DynamoDB for this time allow write with `WCU ~ 1000`.

This test results valid only for records with size `< 1 KB`. If other tables too has record size `< 1 KB`. There should be no problems to copy them.

#


### Installing

1. Copy module directory or clone full Erad repository to computer.
2. Install Python 3, (On Amazon Linux: `yum -y install python36`.
3. Create and activate virtual environment, if it is need to isolate installation from system libs.
    ```
    python3 -m venv <venv_name>
    source <venv_name>/bin/activate
    ```
4. Install module

`pip install path/to/test_table_copy/directory_or_zip_file`

Note: If `pip` executed outside virtual environment, it possibly will lunch for Python 2. Use `python3 -m pip` to run pip with Python 3.
#

### Running

If this `AWS_PRODUCTION_ENV` variable not set, or ` != True`, `test_table_copy` will try to connect to localhosted DynamoDB instance (endpoint = `http://localhost:8091`).

**! Note the value of AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY should be redacted !**

Before lunching utility, export or just set this environment variables.
```
export AWS_PRODUCTION_ENV=True
export AWS_DEFAULT_REGION=us-west-2
export AWS_ACCESS_KEY_ID=localhost-aws-access-key
export AWS_SECRET_ACCESS_KEY=localhost-aws-secret-key
```

To see available commands

`python3 -m test_table_copy -h`

Example how to copy tables

`python3 -m test_table_copy --copy-table --src-table src --dst-table dst --limit-wcu 1000`

If tables are located in different regions use `--src-region` and `--dst-region` options, without specifying it, tool will execute all operation in region defined in `AWS_DEFAULT_REGION` environment variable.

Example:

```bash
python -m test_table_copy --copy-table \
	--src-table test-table-src \
	--src-region us-west-2 \
	--dst-table test-table-dst \
	--dst-region us-east-2 \
	--limit-wcu 10
```

Use `--limit-wcu` option to set maximum write rate for copy operation.

Although `--limit-wcu` will limit request rate, there is a possibility of exceeding the limit. For example, other software concurrently send request. In this case AWS will return the list of unprocessed data. `test_table_copy` will try to write unprocessed data again. By default there will be `15` attempts. If after all attempts there will be unprocessed data, `test_table_copy` will throw exception and stop operation.
For more information, see `batch_write` function code.

#

### Running locally and testing

Use docker-compose file to lunch local instance of DynamoDB
`docker-compose up -d`
Local DynamoDB instance will expose this `http://localhost:8091` endpoint.

Do not set `AWS_PRODUCTION_ENV` variable, utility will use default value.

