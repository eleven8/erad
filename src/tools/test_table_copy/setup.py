# coding: utf-8
import setuptools

setuptools.setup(
	name="test_table_copy",
	version="0.1",
	author="",
	author_email="",
	description="Utility to copy dynamodb tables",
	url="",
	install_requires=['boto3'],
	py_modules=['test_table_copy'],
	classifiers=(
		"Programming Language :: Python :: 3",
		"Operating System :: Tested only on Linux",
	),
)
