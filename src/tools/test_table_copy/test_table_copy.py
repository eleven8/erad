# coding: utf-8

import argparse
import concurrent.futures
from datetime import datetime
from datetime import timedelta
from os import environ
from os import urandom
from pprint import pprint
from threading import BoundedSemaphore
from threading import Lock

import boto3
import time
from botocore.retryhandler import delay_exponential

BATCH_SIZE = 25  # maximum batch size, from boto3 documentation
ONE_SECOND = 1.0
ONE_MINUTE = 60
RATE_DECREASE = 0.25
RATE_INCREASE = 0.05

# how often it can reduce rate
# if not limit this, then several batch writers can quickly drop rate to very small value
DECREASE_TIME_LIMIT = ONE_MINUTE / 6
INCREASE_TIME_LIMIT = ONE_MINUTE * 5  # after this time it is allowed to try to increase rate

base_config = {
	'endpoint_url': None if environ.get('AWS_PRODUCTION_ENV') == 'True' else 'http://localhost:8091',
	'region_name': environ.get('AWS_DEFAULT_REGION', 'us-west-2'),
	'aws_access_key_id': environ.get('AWS_ACCESS_KEY_ID', 'localhost-aws-access-key'),
	'aws_secret_access_key': environ.get('AWS_SECRET_ACCESS_KEY', 'localhost-aws-secret-key'),
}
src_region = None
dst_region = None

stats = {
	'total_count': 0,
	'jobs_done': 0
}


def get_dyndb_connections(region: str=None):
	connection_config = base_config.copy()
	if region is not None:
		connection_config['region_name'] = region
	dynamodb = boto3.resource('dynamodb', **connection_config)
	dyndb_client = boto3.client('dynamodb', **connection_config)
	return dynamodb, dyndb_client


class RateLimiterPool:
	def __init__(self, executor, rate: float=1.0, max_workers=None):
		"""
		:param executor:
		:param rate: WCU units
		This parameter will limit threading calls per second to honor provisioned WCU capacity.
		:param max_workers:
		"""
		if rate == 0:
			raise Exception('`rate` can not be equal 0')

		self.default_rate = rate
		self.batch_per_second = rate / BATCH_SIZE
		self.min_interval = ONE_SECOND / self.batch_per_second
		self.max_queue_size = 1 if self.batch_per_second < 1 else int(self.batch_per_second)

		self.pool = executor(max_workers=max_workers)
		self.pool_queue = BoundedSemaphore(self.max_queue_size)
		self.lock = Lock()
		cur_time = time.time()
		self.last_time_submitted = cur_time
		self.__apply_new_rate(self.default_rate, cur_time)

	def __apply_new_rate(self, rate, cur_time):
		self.rate = rate
		self.min_interval = ONE_SECOND / (self.rate / BATCH_SIZE)
		self.last_time_rate_change = cur_time
		print('New rate: {0}'.format(self.rate))

	def decrease_rate(self):
		self.lock.acquire()
		cur_time = time.time()

		if self.last_time_rate_change is None or cur_time - self.last_time_rate_change > DECREASE_TIME_LIMIT:
			new_rate = int(self.rate - self.rate * RATE_DECREASE)
			if new_rate == 0:
				new_rate = 1
			self.__apply_new_rate(new_rate, cur_time)
		self.lock.release()

	def increase_rate(self):
		self.lock.acquire()

		if self.rate < self.default_rate:
			cur_time = time.time()
			if cur_time - self.last_time_rate_change > INCREASE_TIME_LIMIT:
				new_rate = self.rate + self.rate * RATE_INCREASE
				if new_rate == 0:
					new_rate = 1
				if new_rate > self.default_rate:
					new_rate = self.default_rate
				self.__apply_new_rate(new_rate, cur_time)

		self.lock.release()

	def submit(self, fn, *args, **kwargs):
		self.increase_rate()

		self.lock.acquire()
		cur_time = time.time()
		elapsed = cur_time - self.last_time_submitted
		left_to_wait = self.min_interval - elapsed

		if left_to_wait > 0:
			time.sleep(left_to_wait)
		self.lock.release()

		self.pool_queue.acquire()
		self.last_time_submitted = time.time()

		future = self.pool.submit(fn, *args, **kwargs)
		future.add_done_callback(self.pool_queue_callback)
		return future

	def pool_queue_callback(self, future):
		result = future.result()
		stats['total_count'] += result['records_written']
		stats['jobs_done'] += 1
		print(stats, 'Current rate {0}'.format(self.rate) )
		self.pool_queue.release()

	def shutdown(self):
		self.pool.shutdown()


def delete_test_table(table_name: str):
	dynamodb, dyndb_client = get_dyndb_connections(dst_region)
	table = dynamodb.Table(table_name)
	response = table.delete()
	pprint(response)


def describe_table(table_name: str):
	dynamodb, dyndb_client = get_dyndb_connections(dst_region)
	the_table = dynamodb.Table(table_name)
	pprint(the_table.stream_specification)
	pprint(the_table.table_arn)
	response = dyndb_client.describe_table(TableName=table_name)
	pprint(response)


def get_random_str(size: int):
	return bytearray(urandom(size)).hex()


def populate_items(table_name: str, amount: int):
	dynamodb, dyndb_client = get_dyndb_connections(dst_region)
	the_table = dynamodb.Table(table_name)

	start = time.time()
	with the_table.batch_writer() as batch:
		for x in range(amount):
			test_item = {
				'ID': get_random_str(8),
				'RadiusAttribute': 'nas-ident',
				'Account_ID': get_random_str(7),
				'Group_ID': get_random_str(8)
			}
			batch.put_item(Item=test_item)
	stop = time.time()
	print('Populated {0} records in  {1} seconds'.format(amount, stop-start))


def list_tables():
	dynamodb, dyndb_client = get_dyndb_connections(dst_region)
	response = dyndb_client.list_tables()
	for table_name in response['TableNames']:
		print(table_name)


def create_table(table_name: str, rcu: int =1, wcu: int =1):
	dynamodb, dyndb_client = get_dyndb_connections(dst_region)
	response = dyndb_client.create_table(
		AttributeDefinitions=[
			{
				"AttributeType": "S",
				"AttributeName": "Account_ID"
			},
			{
				"AttributeType": "S",
				"AttributeName": "ID"
			},
			{
				"AttributeType": "S",
				"AttributeName": "Group_ID"
			}
		],
		TableName=table_name,
		KeySchema=[
			{
				"KeyType": "HASH",
				"AttributeName": "ID"
			},
			{
				"KeyType": "RANGE",
				"AttributeName": "Account_ID"
			}
		],
		GlobalSecondaryIndexes=[
			{
				'IndexName': 'Account_ID-Group_ID-index',
				'KeySchema': [
					{
						"KeyType": "HASH",
						"AttributeName": "Account_ID"
					},
					{
						"KeyType": "RANGE",
						"AttributeName": "Group_ID"
					}
				],
				'Projection': {
					'ProjectionType': 'ALL',
				},
				'ProvisionedThroughput': {
					'ReadCapacityUnits': 1,  # For tests purposes index is not used so no need in bit RCU
					'WriteCapacityUnits': wcu
				}
			},
		],
		ProvisionedThroughput={
			'ReadCapacityUnits': rcu,
			'WriteCapacityUnits': wcu
		},
		StreamSpecification={
			'StreamEnabled': True,
			'StreamViewType': 'NEW_IMAGE'
		},
		SSESpecification={
			'Enabled': True
		}
	)

	dyndb_client.get_waiter('table_exists').wait(TableName=table_name)
	response = dyndb_client.describe_table(TableName=table_name)

	if response['Table']['TableStatus'] != 'ACTIVE':
		raise Exception('Table {0} is not created'.format(table_name))
	print('Table {0} created'.format(table_name))


def batch_generator(src_table: str):
	dynamodb, dyndb_client = get_dyndb_connections(src_region)
	paginator = dyndb_client.get_paginator('scan')

	response_iterator = paginator.paginate(
		TableName=src_table,
		Select='ALL_ATTRIBUTES',
		ConsistentRead=True,
	)

	for page in response_iterator:
		items = page['Items']
		batch = []
		for i, item in enumerate(items, start=1):
			# print(i)
			batch.append({'PutRequest': {'Item': item}})

			if i % BATCH_SIZE == 0:
				yield batch
				batch = []

		if len(batch) > 0:
			yield batch


def make_request_items(dst_table: str, batch: list):
	return {dst_table: batch}


def batch_write(request_items, job_id, batch_length: int, pool: RateLimiterPool):
	# create new instance of boto3 resource for each thread
	thread_base_config = base_config.copy()
	if dst_region is not None:
		thread_base_config['region_name'] = dst_region

	dyndb_client_thread = boto3.client('dynamodb', **thread_base_config)
	print('Job {0} started'.format(job_id))
	items = request_items
	limit_loop = 22
	loop_count = 0
	base = 0.1
	growth_factor = 1.5
	while True:
		loop_count += 1
		if loop_count > limit_loop:
			raise Exception('Loop limit exceeded')
		try:
			response = dyndb_client_thread.batch_write_item(
				RequestItems=items
			)
		except dyndb_client_thread.exceptions.ProvisionedThroughputExceededException as error:
			sleep_seconds = ONE_MINUTE
			print('Current pool rate {0} WCU'.format(pool.rate))
			print(
				'Error received Throughput Exception\n'
				'Changing pool rate, and sleep for {1} seconds'.format(error, sleep_seconds)
			)
			pool.decrease_rate()
			time.sleep(sleep_seconds)
		else:
			not_written = len(response['UnprocessedItems'])
			if not_written > 0:
				items = response['UnprocessedItems']
				time.sleep(delay_exponential(base=base, growth_factor=growth_factor, attempts=loop_count))
			elif not_written == 0:
				break
	return {'records_written': batch_length}


def copy_table(src_table: str, dst_table: str, limit_wcu: int=1):
	"""
	:param src_table:
	:param dst_table:
	:param limit_wcu:
	:param dst_region:
	DynamoDB configured with Read/Write capacity options.
	If function copy_table will write records too fast, DynamoDB will throttle request.
	Parameter limit_wcu dedicated to limit write rate per second.

	:return:
	"""
	start = time.time()

	jobs_submited = 0

	# Thread can be created and performed very quickly - which can lead to exceeding WCU limit and
	# threads will stuck in loop tying to write data, but AWS will reject them.
	pool = RateLimiterPool(concurrent.futures.ThreadPoolExecutor, rate=limit_wcu)

	for i, batch in enumerate(batch_generator(src_table), start=1):
		print('interval: {0}'.format(pool.min_interval))
		pool.submit(batch_write, make_request_items(dst_table, batch), jobs_submited + 1, len(batch), pool)
		jobs_submited += 1

	pool.shutdown()
	stop = time.time()

	print('Jobs {0}/{1} done'.format(stats['jobs_done'], jobs_submited))
	print('{0} records was copied in {1} seconds'.format(stats['total_count'], stop-start))


def get_table_metric(table_name: str):
	cloudwatch_config = base_config.copy()

	if dst_region is not None:
		cloudwatch_config['region_name'] = dst_region

	cw_client = boto3.client('cloudwatch', **cloudwatch_config)
	end = datetime.utcnow()
	start = end - timedelta(minutes=5)

	cw_response = cw_client.get_metric_statistics(
		Namespace='AWS/DynamoDB',
		Dimensions=[{
			'Name': 'TableName',
			'Value': 'vplatunov-test-dst'
		}],
		MetricName='ConsumedWriteCapacityUnits',
		Statistics=['Maximum'],
		StartTime=start,
		EndTime=end,
		Period=60,
	)
	pprint(cw_response)


def select_command(args):
	global src_region
	global dst_region
	src_region = args.src_region
	dst_region = args.dst_region

	if args.create_table is not None:
		cu = {}
		if args.rcu is not None:
			cu['rcu'] = args.rcu
		if args.wcu is not None:
			cu['wcu'] = args.wcu
		create_table(args.create_table, **cu)
		return 0

	elif args.populate_data is not None:
		if args.dst_table is None:
			print('Destination table is not specified')
			return 1
		populate_items(args.dst_table, args.populate_data)
		return 0

	elif args.copy_table is True:
		if args.src_table is None:
			print('Source table is not specified')
			return 1
		if args.dst_table is None:
			print('Destination table is not specified')
			return 1

		kwargs = {}
		if args.limit_wcu is not None and args.limit_wcu > 0:
			kwargs['limit_wcu'] = args.limit_wcu
		copy_table(args.src_table, args.dst_table, **kwargs)
		return 0

	elif args.dsc_table is not None:
		describe_table(args.dsc_table)
		return 0

	elif args.rm is not None:
		delete_test_table(args.rm)
		return 0

	elif args.ls is True:
		list_tables()
		return 0

	elif args.validate is True:
		if args.src_table is None:
			print('Source table is not specified')
			return 1
		if args.dst_table is None:
			print('Destination table is not specified')
			return 1
		validate(args.src_table, args.dst_table)

		return 0

	elif args.get_table_metric is not None:
		get_table_metric(args.get_table_metric)
		return 0
	else:
		print('Use -h option to see available options')
		return 0


def validate(src_table: str, dst_table: str):
	dynamodb_src, dyndb_client_src = get_dyndb_connections(src_region)
	dynamodb_dst, dyndb_client_dst = get_dyndb_connections(dst_region)
	paginator = dyndb_client_src.get_paginator('scan')
	dst_table_hndl = dynamodb_dst.Table(dst_table)

	response_iterator = paginator.paginate(
		TableName=src_table,
		Select='ALL_ATTRIBUTES',
		ConsistentRead=True,
	)

	for page in response_iterator:
		items = page['Items']
		for i, item in enumerate(items, start=1):
			get_item_response = dst_table_hndl.get_item(
				Key={
					'ID': item['ID']['S'],
					'Account_ID': item['Account_ID']['S']
				}
			)
			assert(get_item_response['Item']['Group_ID'] == item['Group_ID']['S'])
			assert(get_item_response['Item']['RadiusAttribute'] == item['RadiusAttribute']['S'])

	print('Content of tables "{0}" and "{1}" is match'.format(src_table, dst_table))


def main():
	parser = argparse.ArgumentParser(description='Testing DynamoDB copy')  # type argparse.ArgumentParser
	parser.add_argument(
		'--src-table',
		metavar='<table_name:str>',
		type=str,
		help='Source table')
	parser.add_argument(
		'--dst-table',
		metavar='<table_name:str>',
		type=str,
		help='Destination table')
	parser.add_argument(
		'--src-region',
		metavar='<src_region:str>',
		type=str,
		help='Source region, used to read data, with `--copy-table` command. In other case use `--dst-region`.')
	parser.add_argument(
		'--dst-region',
		metavar='<dst_region:str>',
		type=str,
		help='Destination region')
	parser.add_argument(
		'--create-table',
		metavar='<table_name:str>',
		type=str,
		help='Create new table with specified name')
	parser.add_argument(
		'--rcu',
		metavar='<rcu:int>',
		type=int,
		help='AWS DynamodDB Read Capacity Units settings, that will be applied to created table')
	parser.add_argument(
		'--wcu',
		metavar='<wcu:int>',
		type=int,
		help='AWS DynamodDB Write Capacity Units setting, that will be applied to created table')

	parser.add_argument(
		'--populate-data',
		metavar='<records_amount:int>',
		type=int,
		help='Amount of records, that will be populated in table specified by --dst-table option')

	parser.add_argument(
		'--copy-table',
		action='store_true',
		help='Indication to copy data from --src-table to --dst-table')

	parser.add_argument(
		'--dsc-table',
		metavar='<table_name:str>',
		type=str,
		help='Describe table')

	parser.add_argument(
		'--rm',
		metavar='<table_name:str>',
		type=str,
		help='Remove table')

	parser.add_argument(
		'--ls',
		action='store_true',
		help='List tables')

	parser.add_argument(
		'--limit-wcu',
		metavar='<limit-wcu:int>',
		type=int,
		help='Limiting WCU available to use in copy table operation.')

	parser.add_argument(
		'--validate',
		action='store_true',
		help='Compare two table, record by record')

	parser.add_argument(
		'--get-table-metric',
		metavar='<table_name:str>',
		type=str,
		help='Get ConsumedWriteCapacityUnits CloudWatch metric for <table_name>')

	args = parser.parse_args()
	select_command(args)


if __name__ == '__main__':
	main()
