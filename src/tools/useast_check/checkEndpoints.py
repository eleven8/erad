#!/usr/bin/env python

import sys, getopt, os
from time import sleep
import inspect
import json
from optparse import OptionParser
from pynamodb.connection import Connection
from pynamodb.models import Model
from pynamodb.exceptions import DoesNotExist
from pynamodb.indexes import *
from pynamodb.attributes import (
        Attribute,
        NumberAttribute,
        UnicodeAttribute,
        UTCDateTimeAttribute,
        UnicodeSetAttribute,
        NumberSetAttribute,
        BinarySetAttribute
        #BinaryAttribute,
    )



HostUrl = None
Region = None

class CheckEndpoints():

    def main( self ):

        class Erad_Endpoint(Model):
            class Meta:
                table_name = "Erad_Endpoint"
                name = "Endpoint"
                read_capacity_units=3
                write_capacity_units=2
                region = Region
                host = HostUrl

            # IpAddress: A unique identifier - IP.
            IpAddress = UnicodeAttribute(hash_key=True)

            # Port: The port for connection.
            Port = UnicodeAttribute(range_key=True)

            # Group_ID: The site this endpoint is assigned to, or none.
            Group_ID = UnicodeAttribute(null=True)

            # ClusterId: The radius cluster this endpoint will be hosted by.
            ClusterId = UnicodeAttribute(null=False)

            # Secret: The radius shared secret. Unique to each endpoint.
            Secret = UnicodeAttribute(null=False)

            # Region: The physical location of the endpoint.
            Region = UnicodeAttribute(null=False)

        print "Connecting to DynamoDB:"
        print "Host: "+HostUrl
        print "Region: "+Region
        print " "

        # check Dynamo for new enpoints list
        new_endpoints_arr = Erad_Endpoint.query(
            "34.192.196.106",
            filter_condition=Erad_Endpoint.Group_ID=="::free")
        new_endpoints = [i.Port for i in new_endpoints_arr]

        # read previous endpoints list
        try:
            with open('useastEndpoints.json') as json_data:
                endpoints = json.load(json_data)
            old_endpoints = set(endpoints)
        except:
            old_endpoints = set(new_endpoints)

        # write new endpoints to a file
        with open('useastEndpoints.json', 'w') as outfile:
            json.dump(new_endpoints, outfile)

        # copy new endpoints file to S3
        os.system("aws s3 cp useastEndpoints.json s3://teamfortytwo.db.useastcheck/useastEndpoints.json")

        # compare old and new endpoints
        different_endpoints = old_endpoints.symmetric_difference(set(new_endpoints))

        # check if there was a new port provisioned - if so exit(1) and Jenkins will fail
        if len(different_endpoints) > 0:
            print "Someone provisioned port(s) in us-east-1: " + ', '.join(different_endpoints)
            exit(1)


if __name__ == "__main__":
    parser = OptionParser(usage="Usage: CheckEndpoints.py [options]")
    parser.add_option("-H", "--HostUrl", dest="HostUrl",
                      help="HostUrl to be used for testing", metavar="HostUrl")
    parser.add_option("-R", "--Region", dest="Region",
                      help="Region to be used for testing", metavar="Region")
    (options, args) = parser.parse_args()
    Region = "us-west-2" if not options.Region else options.Region
    HostUrl = "http://localhost:8000" if not options.HostUrl else options.HostUrl

    sys.argv = args
    CheckEndpoints().main()
