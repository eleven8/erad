var app = angular.module('eradClusterTool', []);
app.controller('baseCtrl', function($scope) {

	// Clusters list
	$scope.clusters = [];
	$scope.step = 1;
	$scope.selectedEndpoint = null;
	$scope.success = false;
	$scope.success_message = null;
	$scope.endpoints = null;
	$scope.toggle = false;

    // Dynamodb Configuration
    AWS.config.update({
		region : 'us-west-2',
		endpoint : 'http://35.164.148.78:8000',
		accessKeyId : 'AKIAJTK5BJ3KH2DRVTUQ',
		secretAccessKey : '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'
	});

	AWS.config.apiVersions = {
	  dynamodb: '2012-08-10',
	};

	//  DynamoDB Connection
	var dynamodb = new AWS.DynamoDB();

	//Call fetchClusters method on pageload
	fetchClustersOnDB();

	// ips and regions
	$scope.ipsandregions = [
		{ipaddress: "52.11.20.214", region: "us-west-2"},
		{ipaddress: "34.192.196.106", region: "us-east-1"},
		{ipaddress: "3.17.122.53", region: "us-east-2"}
	];

	$scope.selectedIpAddress = $scope.ipsandregions[0].ipaddress;
	$scope.selectedRegion = $scope.ipsandregions[0].region;

	$scope.addCluster = function(){
		$('#modalAddCluster').modal({show: true, backdrop: 'static'});
	}

	$scope.selected = function (argument) {
		$scope.toggle = !$scope.toggle;
	}
	$scope.nextAddStep = function(){
		if($scope.step == 1){

			$scope.clusterObj = {};
			$scope.cluster_id = $( "#stepForm1" ).find("input[name='txtClusterId']").val();
			$scope.clusterObj.ClusterId = $( "#stepForm1" ).find("input[name='txtClusterId']").val();
			$scope.clusterObj.AcctNum = $( "#stepForm1" ).find("input[name='txtAcctNum']").val();
			$scope.clusterObj.AuthNum = $( "#stepForm1" ).find("input[name='txtAuthNum']").val();
			$scope.clusterObj.Instances = $( "#stepForm1" ).find("input[name='txtInstances']").val();
			$scope.clusterType = $("#stepForm1").find("input[name='clusterType']:checked").val();
			console.log($scope.clusterObj);
			console.log($scope.clusterType);

			//check cluster id first
			if(!check_cluster($scope.cluster_id)){
				if($scope.clusterObj.AcctNum >= 2 && $scope.clusterObj.AuthNum >= 2){
					//  previous method to save only the cluster
					// saveClusterOnDB($scope.clusterObj);
					$scope.step = 2;
					fetchMaxPort($scope.selectedIpAddress);
				}else{
					alert("AcctNum and AuthNum has a minumum of 2");
					$scope.step = 1;
				}
			}else{
				alert("Cluster Already exists!");
				$scope.step = 1;
			}


		}else{
			var endpointsList = [];
			// Save 128 ports per cluster
			for(var x=0; x < 123; x++){
				var endpointObj = {};
				endpointObj.IpAddress = $scope.selectedIpAddress;
				endpointObj.Port = $scope.maxPort.toString();
				endpointObj.ClusterId = $scope.cluster_id;
				endpointObj.Group_ID = "::dirty";
				endpointObj.Region = $scope.selectedRegion;
				endpointObj.Secret = randomSecret();
				// increment port
				$scope.maxPort = $scope.maxPort + 2;
				// //  previous method to save only the endpoint
				// saveEndpointOnDB(endpointObj);
				endpointsList.push(endpointObj);
			}
			// Add both Cluster and Endpoints list at once
			saveClusterAndEndpoints($scope.clusterObj, endpointsList);
			$scope.success = true;
		}
	}

	// Edit Endpoint
	$scope.editEndpoint = function(endpoint){
		console.log(endpoint);
		// reset other rows
		for (var i=0; i<$scope.endpoints.length; i++) {
	  		$scope.endpoints[i].edit = false;
	  	}
		endpoint.edit = true;
		$scope.selectedEndpoint = angular.copy(endpoint);
	}

	// cancel edit
	$scope.cancelEditEndpoint = function(endpoint){
		endpoint.edit = false;
		$scope.selectedEndpoint = null;
	}

	$scope.randomPassword = function(){
		return randomSecret();
	}

	$scope.editGroup = function(value){
		$scope.selectedEndpoint.Group_ID.S = value;
	}

	$scope.cancelUpdate = function(){
		$scope.selectedEndpoint = null;
	}

	// Update Cluster
	$scope.updateCluster = function(){
		console.log("update Cluster");
		// Update Cluster
		var clusterObj = {};
		console.log($scope.selectedCluster);
		clusterObj.ClusterId = $scope.selectedCluster.ClusterId.S;
		clusterObj.AcctNum = $scope.selectedClusterAcctNum.toString();
		clusterObj.AuthNum = $scope.selectedClusterAuthNum.toString();
		clusterObj.Instances = $scope.selectedCluster.Instances.S;
		if(clusterObj.AcctNum >= 2 && clusterObj.AuthNum >= 2){
			saveClusterOnDB(clusterObj);

			$scope.success_message = "Updated Cluster details."

			// reload endpoints table
			loadEndpointsFromDB($scope.selectedCluster);

		}else{
			$scope.success_message = null;
			alert("AcctNum and AuthNum has a minumum of 2");
		}
	}

	// Update Ednpoint
	$scope.updateEndpoint = function(){
		console.log("update Endpoint");
		// Update Endpoint
		var endpointObj = {};
		endpointObj.IpAddress = $scope.selectedEndpoint.IpAddress.S;
		endpointObj.Port = $scope.selectedEndpoint.Port.S;
		endpointObj.ClusterId = $scope.selectedEndpoint.ClusterId.S;
		endpointObj.Group_ID = $scope.selectedEndpoint.Group_ID.S;
		endpointObj.Region = $scope.selectedEndpoint.Region.S;
		endpointObj.Secret = $scope.selectedEndpoint.Secret.S;
		// save on db
		saveEndpointOnDB(endpointObj);
		$scope.success_message = "Updated Endpoint Port "+ $scope.selectedEndpoint.Port.S +" with new details."
		// reload endpoints table
		loadEndpointsFromDB($scope.selectedCluster);
	}

	// Edit Cluster
	$scope.editCluster = function(item){

		console.log(item);
		$scope.selectedCluster = item;

		// Save and bind models
		$scope.selectedClusterAcctNum = parseInt($scope.selectedCluster.AcctNum.N);
		$scope.selectedClusterAuthNum = parseInt($scope.selectedCluster.AuthNum.N);

		// call Erad_Endpoint
		loadEndpointsFromDB(item);
		$('#modalEditCluster').modal('show');
	}

	//  View Clusters Data with Endpoints
	$scope.viewCluster = function(item){
		console.log(item);
		$scope.selectedCluster = item;

		// call endpoints load function
		loadEndpointsFromDB(item);

		$('#modalViewCluster').modal('show');
	}

	// method to load endpoints from DB
	function loadEndpointsFromDB(item){
		// call Erad_Endpoint
		var params = {
		  TableName: 'Erad_Endpoint_Global', /* required */
		  ScanFilter: {
		    ClusterId: {
		      ComparisonOperator: 'EQ',
		      AttributeValueList: [
		        { /* AttributeValue */
		          S: item.ClusterId.S
		        },
		      ]
		    },
		  }
		};
		dynamodb.scan(params, function(err, data) {
		  if (err){
		  	console.log(err, err.stack); // an error occurred
		  }
		  else{
		  	console.log(data);           // successful response
		  	$scope.endpoints = data.Items;
		  	for (var i=0; i<$scope.endpoints.length; i++) {
		  		$scope.endpoints[i].edit = false;
		  	}
		  	$scope.$apply();
		  }
		});
	}

	//  method to save cluster details on db
	var saveClusterOnDB = function(item){
		var params = {
		  Item: { /* required */
		    ClusterId: { /* AttributeValue */
		      S: item.ClusterId
		    },
		    AcctNum: { /* AttributeValue */
		      N: item.AcctNum
		    },
		    AuthNum: { /* AttributeValue */
		      N: item.AuthNum
		    },
		    Instances: { /* AttributeValue */
		      S: item.Instances
		    }
		    /* anotherKey: ... */
		  },
		  TableName: 'Erad_Cluster_Global' /* required */
		};
		dynamodb.putItem(params, function(err, data) {
		  if (err) console.log(err, err.stack); // an error occurred
		  else     console.log(data);           // successful response
		});

		fetchClustersOnDB();
	}

	//  method to save endpoint on db
	var saveEndpointOnDB = function(item){
		var params = {
		  Item: { /* required */
		    IpAddress: { /* AttributeValue */
		      S: item.IpAddress
		    },
		    Port: { /* AttributeValue */
		      S: item.Port
		    },
		    ClusterId: { /* AttributeValue */
		      S: item.ClusterId
		    },
		    Group_ID: { /* AttributeValue */
		      S: item.Group_ID
		    },
		    Region: { /* AttributeValue */
		      S: item.Region
		    },
		    Secret: { /* AttributeValue */
		      S: item.Secret
		    }
		    /* anotherKey: ... */
		  },
		  TableName: 'Erad_Endpoint_Global' /* required */
		};
		dynamodb.putItem(params, function(err, data) {
		  if (err) console.log(err, err.stack); // an error occurred
		  else{
		  	     // console.log(data);
		  }       // successful response
		});

	}

	// method to add cluster and endpoint
	function saveClusterAndEndpoints(cluster, endpoints) {
		console.log(cluster);
		console.log(endpoints);
		var params = {
		  Item: { /* required */
		    ClusterId: { /* AttributeValue */
		      S: cluster.ClusterId
		    },
		    AcctNum: { /* AttributeValue */
		      N: cluster.AcctNum
		    },
		    AuthNum: { /* AttributeValue */
		      N: cluster.AuthNum
		    },
		    Instances: { /* AttributeValue */
		      S: cluster.Instances
		    }
		    /* anotherKey: ... */
		  },
		  TableName: 'Erad_Cluster_Global' /* required */
		};
		dynamodb.putItem(params, function(err, data) {
		  if(err){
		  	console.log(err, err.stack);
		  }else{
		  	endpoints.forEach(function(endpoint){
		  		var paramsEndpoint = {
				  Item: { /* required */
				    IpAddress: { /* AttributeValue */
				      S: endpoint.IpAddress
				    },
				    Port: { /* AttributeValue */
				      S: endpoint.Port
				    },
				    ClusterId: { /* AttributeValue */
				      S: endpoint.ClusterId
				    },
				    Group_ID: { /* AttributeValue */
				      S: endpoint.Group_ID
				    },
				    Region: { /* AttributeValue */
				      S: endpoint.Region
				    },
				    Secret: { /* AttributeValue */
				      S: endpoint.Secret
				    }
				    /* anotherKey: ... */
				  },
				  TableName: 'Erad_Endpoint_Global' /* required */
				};
				dynamodb.putItem(paramsEndpoint, function(err, data) {
				  if (err) console.log(err, err.stack); // an error occurred
				  else{
				  	     // console.log(data);
				  }       // successful response
				});
			});
			fetchClustersOnDB();
		  }
		});
	}

	//  method to test cluster-id
	function check_cluster(cluster_id) {
		var ret = false;
		$scope.clusters.forEach(function(n){
			if (n.ClusterId.S == cluster_id)
				ret = true;
		});
		return ret;
	}

	//  method to load all the clusters from db
	function fetchClustersOnDB(){
		var params = {
		  TableName: 'Erad_Cluster_Global' /* required */
		};

		//  Get List of Clusters
		dynamodb.scan(params, function(err, data) {
		  if (err) {
		  	console.log(err, err.stack); // an error occurred
		  }
		  else{
		  	console.log(data);           // successful response
		  	$scope.clusters = data.Items;
		  	$scope.$apply();
		  }
		});
	}

	function updateMaxPort(arr, defaultPort){
		if (arr.length == 0){
			$scope.maxPort = defaultPort
		} else {
			$scope.maxPort = Math.max.apply(null, arr) + 2;
		}
	}

	// method to detect the highest port
	function fetchMaxPort(ipaddress) {
		console.log(ipaddress);
		var params = {
		  TableName: 'Erad_Endpoint_Global',
		  ScanFilter: {
		    IpAddress: {
		      ComparisonOperator: 'EQ',
		      AttributeValueList: [
		        { /* AttributeValue */
		          S: ipaddress
		        },
		      ]
		    },
		  }
		};
		var array = [];
		var array_high_capacity = [];
		var max =
		//  Get List of Clusters
		dynamodb.scan(params, function(err, data) {
		  if (err) {
		  	console.log(err, err.stack); // an error occurred
		  }
		  else{
		  	console.log(data);           // successful response
		  	data.Items.forEach(function(n){
		  		var portNumber = parseInt(n.Port.S);
				if ((portNumber >= 20000) && (portNumber < 40000)){
					array.push(portNumber);
				} else if (portNumber >= 40000){
					array_high_capacity.push(portNumber);
				}
			});
		  	// debug data
		  	console.log(array);
		  	console.log(array_high_capacity);

			if ($scope.clusterType === 'special'){
				updateMaxPort(array_high_capacity, 40000);
			}
			else if ($scope.clusterType === 'normal'){
				updateMaxPort(array, 20000);
			}

			$scope.$apply();
		  }
		});
	}

	function randomSecret() {
      var text = "";
      var length = 10;
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      for(var i = 0; i < length; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }

	// Event Listner of Add Cluster Modal
	$('#modalAddCluster').on('hidden.bs.modal', function () {
		$scope.step = 1;
		$scope.cluster_id = null;
		fetchClustersOnDB();
		$scope.success = false;
		$scope.endpoints = null;
		$scope.txtClusterId = null;
		$scope.txtAcctNum = null;
		$scope.clusterType = "normal";
		$scope.txtAuthNum = null;
		$scope.toggle = false;
    })

	// Event Listner of Edit Cluster Modal
    $('#modalEditCluster').on('hidden.bs.modal', function () {
		$scope.selectedEndpoint = null;
		$scope.success_message = null;
		$scope.endpoints = null;
    })

    //function to search array
    function search(nameKey, myArray){
	    for (var i=0; i < myArray.length; i++) {
	        if (myArray[i].ipaddress === nameKey) {
	            return myArray[i];
	        }
	    }
	}

    $scope.$watch('selectedIpAddress', function(NewValue, OldValue) {
        $scope.selectedRegion = search(NewValue, $scope.ipsandregions).region;
    }, true);

});
