#!/bin/bash

if [ -z "$T42_PASSPHRASE" ]; then
    T42_PASSPHRASE=$1
fi

# installing packages
echo "Installing Apache"
yum update -y
yum install -y httpd

# copy cluster tool files first
echo "Copying Files..."
#cp -r /usr/local/src/* /var/www/html
cp httpd.conf /etc/httpd/conf/httpd.conf
cp tool.conf /etc/httpd/conf.d/tool.conf

# start cluster on httpd
echo "Starting the Server"
service httpd start


if [ ! -z "$T42_PASSPHRASE" ]; then

  gpg --batch --yes --passphrase $T42_PASSPHRASE --output aws.cred --decrypt aws.cred.secure
  ls
  source ./aws.cred

  # replace ip with the url to production dynamod DB
  sed -i "s#http://35.164.148.78:8000#https://dynamodb.us-west-2.amazonaws.com#g" /usr/local/src/js/controller.js
  sed -i "s#AKIAJTK5BJ3KH2DRVTUQ#$AWS_ACCESS_KEY_ID#g" /usr/local/src/js/controller.js
  sed -i "s#wvNBN3Fh0ijUomsJXBeozZ3M13BG/vVSTKKI9q6O#$AWS_SECRET_ACCESS_KEY#g" /usr/local/src/js/controller.js

else
    # DEVELOPMENT

    # replace ip
    pub_ip=$(dig +short myip.opendns.com @resolver1.opendns.com)
    sed -i "s/35.164.148.78/$pub_ip/g" /usr/local/src/js/controller.js

    # download erad_api
    export AWS_ACCESS_KEY_ID=AKIAJTK5BJ3KH2DRVTUQ
    export AWS_SECRET_ACCESS_KEY=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
    export AWS_DEFAULT_REGION=us-west-2
    aws s3 cp s3://teamfortytwo.deploy/erad_webservices.tar.gz ./api.tar.gz
    tar -xzvf api.tar.gz
    chmod +x erad/api/deploy.sh
    # start API
    ./erad/api/deploy.sh > output_api.log 2>&1

fi
