#!/bin/bash



for branch_name in $(cat bitbucket-example-payload.json | jq -r '[.push.changes[].new | select(. != null )] | unique_by(.name)| .[].name');
do
	echo "http://trigger.example.com/${branch_name}"
done
