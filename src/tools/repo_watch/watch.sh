#!/bin/bash

# Note that in some ceases git need to know the username and email.
# Please configure it for repository
# Example of cases: if some commit was force pushed to remote server, `git pull` command will require username and email

git config user.name "jenkins"
git config user.email "jenkins@localhost"

git fetch

# getting information about branches that on remote and on local
git show-ref | awk -Frefs/remotes/origin/ '{ if (($2) && ($2 !~ /HEAD/)) print $2;}' | sort > remote_branches
git show-ref | awk -Frefs/heads/ '{ if (($2) && ($2 !~ /HEAD/)) print $2;}' | sort > local_branches

# calculate new branches on remote
comm -23 ./remote_branches ./local_branches > new_branches

# obtaining branch names that has new commit
git branch -vv --format='%(refname:short) %(upstream:trackshort)' | grep '<' | awk '{print $1}' > updated_branches

# submitting Jenkins jobs
for branchname in $(cat new_branches updated_branches);
do
	# echo $branchname
	wget "https://ci.gd1.io/buildByToken/buildWithParameters?job=erad-ci-director&Branch=${branchname}&token=5MMkr9dZg8yNZde0NqXt8zqOIjJ9suNbUA5t2"
done

# creating new branches to track information

for branchname in $(cat new_branches);
do
	git checkout $branchname
done

for branchname in $(cat updated_branches);
do
	git checkout $branchname

	# using reset operation for cases when commit on remote was overwritten
	# remote server is source of truth
	git reset --hard origin/${branchname}
done


# cleaning local repository from removed branches
git checkout master
git fetch -p
for branchname in $(git branch -vv --format='%(refname:short) %(upstream:track)'| grep '\[gone\]' | awk '{print $1}');
do
	git branch -D $branchname;
done

git clean -df
