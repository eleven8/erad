import argparse
import logging
import os

from erad.api.erad_model import Erad_Endpoint

logger = logging.getLogger(__name__)
logger.setLevel(
	os.environ.get('LOGLEVEL', 'DEBUG').upper()
)




def move_endpoints(source_cluster_id, target_cluster_id, execute=False):
	logger.debug(f'{source_cluster_id},{target_cluster_id},{execute}')
	for row in Erad_Endpoint.scan(filter_condition=(Erad_Endpoint.ClusterId==source_cluster_id)):
		old_cluster_id = row.ClusterId
		if execute is True:
			row.ClusterId = target_cluster_id
			row.save()

		print(f'{row.Region},{old_cluster_id},{row.IpAddress},{row.Port},{row.Group_ID},{row.ClusterId}')


def main():
	parser = argparse.ArgumentParser(
		description='''Move endpoints from one cluster to another
This script only move endpoints to target cluster but does not regenerate radius config timestamp. This is need to continue serve ports on old instances. New configuration will be generated after cluster redeployment.'''
	)
	#TODO research what will happen when new cluster will be crated
	parser.add_argument(
		'source_cluster',
		type=str,
		help='Source cluster id, endpoints with this cluster id will be moved to target cluster id')
	parser.add_argument(
		'target_cluster',
		type=str,
		help='Target cluster id')

	parser.add_argument(
		'--execute',
		action='store_true',
		help='If False changes will be saved')

	args = parser.parse_args()

	move_endpoints(source_cluster_id=args.source_cluster, target_cluster_id=args.target_cluster, execute=args.execute)
	logger.info('Done.')


if __name__ == '__main__':
	main()
