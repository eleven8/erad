import argparse
import json
import logging
import os

from erad.api.erad_model import Erad_Cluster, Erad_Endpoint, EndpointClusterIdIndex

logger = logging.getLogger(__name__)
logger.setLevel(
	os.environ.get('LOGLEVEL', 'DEBUG').upper()
)

target_capacity = 124


def remove_free_endpoints(cluster_id, execute):
	all_endpoints = EndpointClusterIdIndex.query(cluster_id)

	# if len(all_endpoints) <= target_capacity:
	# 	logger.info(f"Nothing to do cluster has less endpoints that target capacity {target_capacity}")
	# 	return

	dirty_endpoints = 0
	free_endpoints = 0
	active_endpoints = 0

	for endpoint in all_endpoints:
		if endpoint.Group_ID == "::free":
			free_endpoints += 1
		elif endpoint.Group_ID == "::dirty":
			dirty_endpoints += 1
		else:
			active_endpoints += 1

	total_endpoints = dirty_endpoints + free_endpoints + active_endpoints

	if total_endpoints <= target_capacity:
		logger.info(f"Nothing to do cluster has less or equal endpoints than target capacity {target_capacity}")
		return


	target_free_endpoints = target_capacity - active_endpoints - dirty_endpoints
	how_much_to_delete = free_endpoints - target_free_endpoints

	logger.info(f"Total endpoints {total_endpoints}")
	logger.info(f"Dirty endpoints {dirty_endpoints}")
	logger.info(f"Free endpoints {free_endpoints}")
	logger.info(f"Active endpoints {active_endpoints}")
	logger.info(f"Target free endpoints {target_free_endpoints}")
	logger.info(f"How much to delete endpoints {how_much_to_delete}")

	counter = how_much_to_delete

	# previous iterator is empty querying again
	all_endpoints = EndpointClusterIdIndex.query(cluster_id)

	for endpoint in all_endpoints:
		if counter <= 0:
			break
		if endpoint.Group_ID == "::free":
			print(f'{endpoint.Region},{endpoint.ClusterId},{endpoint.IpAddress},{endpoint.Port},{endpoint.Group_ID}')
			if execute is True:
				endpoint.delete()
			counter -= 1


def main():
	parser = argparse.ArgumentParser(
		description='''Remove free endpoints from specified cluster to match target capacity'''
	)

	parser.add_argument(
		'cluster_id',
		type=str,
		help='')

	parser.add_argument(
		'--execute',
		action="store_true",
		help='Without this argument script run in dry run mode')

	args = parser.parse_args()
	remove_free_endpoints(cluster_id=args.cluster_id, execute=args.execute)


if __name__ == '__main__':
	main()
