import argparse

from pynamodb.exceptions import DoesNotExist

from erad.api.erad_model import Erad_Cluster


def remove_clusters(cluster_id, execute=False):
	try:
		cluster = Erad_Cluster.get(cluster_id)
		if execute is True:
			cluster.delete()
		print(f'{cluster.ClusterId},{execute}')
	except DoesNotExist as err:
		logger.info('Cluster Does not exists')
		pass


def main():
	parser = argparse.ArgumentParser(
		description='''Remove specified clusters from Erad_Cluster table. To not redeploy these clusters on next redeployemnt'''
	)

	parser.add_argument(
		'cluster_id',
		type=str,
		help='')

	parser.add_argument(
		'--execute',
		action="store_true",
		help='Without this argument script run in dry run mode')

	args = parser.parse_args()
	remove_clusters(cluster_id=args.cluster_id, execute=args.execute)


if __name__ == '__main__':
	main()
