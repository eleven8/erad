import requests
import argparse

url = 'http://localhost:5000'


def get_configs(cluster_id):
	data = requests.post(
		f'{url}/erad/system/server/config/load',
		json={
			"SystemKey": 'r_7X385PDgqPSB2cMvpmPZ6GHqXh5Cd8ZF',
			"ClusterId": cluster_id,
			"ServerType": "radius"
		})
	print(data.json())


def main():
	parser = argparse.ArgumentParser(
		description='''Fetch confgis'''
	)
	# TODO research what will happen when new cluster will be crated
	parser.add_argument(
		'cluster_id',
		type=str,
		help='')

	args = parser.parse_args()
	get_configs(args.cluster_id)


if __name__ == '__main__':
	main()
