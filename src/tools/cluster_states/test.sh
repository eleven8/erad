#!/usr/bin/env bash
set -e
cd $(dirname $0)

{
    ./report.sh -h

    ./report.sh -l
    ./report.sh -l -d ,
    ./report.sh -l -c us-east-dev-02

    ./report.sh -l -e
    ./report.sh -l -e -d ,
    ./report.sh -l -e -c us-west-dev-01
    ./report.sh -l -e -x ::dirty -x ::free -x ::premium
    ./report.sh -l -e -c us-west-dev-01 -x ::dirty -x ::free -x ::premium

} >/dev/null
echo DONE
