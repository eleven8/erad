import argparse
import logging
import os
import csv


from erad.api.erad_model import Erad_Endpoint

logger = logging.getLogger(__name__)
logger.setLevel(
	os.environ.get('LOGLEVEL', 'DEBUG').upper()
)

dir_path = os.path.dirname(os.path.realpath(__file__))


def read_data(filename):
	input_file = os.path.join(dir_path,filename)

	with open(input_file) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter=',')

		for row in csv_reader:
			region = row[0]
			cluster_id = row[1]
			ip_address = row[2]
			port = row[3]
			group_id = row[4]
			secret = 'fakesecret'

			new_endpoint = Erad_Endpoint(
				IpAddress=ip_address,
				Port=port,
				Group_ID=group_id,
				ClusterId=cluster_id,
				Secret=secret,
				Region=region
			)
			new_endpoint.save()


def main():
	parser = argparse.ArgumentParser(
		description='''Load test data into dynamodb'''
	)
	# TODO research what will happen when new cluster will be crated
	parser.add_argument(
		'filename',
		type=str,
		help='file with test data')

	args = parser.parse_args()

	read_data(args.filename)


if __name__ == '__main__':
	main()
