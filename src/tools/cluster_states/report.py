import argparse
import json
import logging
import os

from erad.api.erad_model import Erad_Cluster, Erad_Endpoint

logger = logging.getLogger(__name__)
logger.setLevel(
	os.environ.get('LOGLEVEL', 'DEBUG').upper()
)


def print_clusters():
	for row in Erad_Cluster.scan():
		json_compatible = row.Instances.replace('\'', '"')
		logger.debug(json_compatible)
		instances = json.loads(json_compatible)

		empty = True

		for instance_type, value in instances.items():
			for instance_id, instances_ip in value.items():
				empty = False
				print(f'{row.ClusterId},{instance_type},{instance_id},{instances_ip}')

		if empty:
			print(f'{row.ClusterId},,')


def print_endpoints():
	for row in Erad_Endpoint.scan():
		print(f'{row.Region},{row.ClusterId},{row.IpAddress},{row.Port},{row.Group_ID}')


def main():
	parser = argparse.ArgumentParser(description='Cluster states')
	parser.add_argument(
		'command',
		type=str,
		choices=['clusters', 'endpoints'],
		help='Choose what to show')

	args = parser.parse_args()

	if args.command == 'clusters':
		print_clusters()
	elif args.command == 'endpoints':
		print_endpoints()


if __name__ == '__main__':
	main()
