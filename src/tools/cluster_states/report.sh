#!/usr/bin/env bash
set -e

cluster_table=Erad_Cluster_Global
endpoint_table=Erad_Endpoint_Global
delim=
exclude=
endpoint=
output='--output text'

dynamo() {
    aws dynamodb $endpoint $output "$@"
}

get_endpoints() {
    query='Items[][Region.S,ClusterId.S,IpAddress.S,Port.S,Group_ID.S]'
    scan="dynamo scan  --table-name $endpoint_table --query $query"

    [ -z "$exclude" ] || {
        filter='not (Group_ID in ('
        values='{'
        i=0
        for ex in $exclude; do
            let i=i+1
            filter=$filter":ex$i,"
            values=$values'":ex'$i'":{"S":"'$ex'"},'
        done
        filter=$(echo $filter | sed 's/.$//')'))'
        values=$(echo $values | sed 's/.$//')'}'
    }
    [ -z "$cluster" ] || {
        [ -z "$filter" ] || filter=$filter' and '
        [ -z "$values" ] && values='{' || values=$(echo $values | sed 's/}$/,/')
        filter=$filter'ClusterId = :cluster'
        values=$values'":cluster":{"S":"'$cluster'"}}'
    }

    printf "Region\tClusterId\tIpAddress\tPort\tGroup_ID\n"
    if [ -z "$filter" ]; then
        $scan
    else
        $scan \
            --filter-expression "$filter" \
            --expression-attribute-values "$values"
    fi
}

get_clusters() {
    if [ -z "$cluster" ]; then
        clusters=$(dynamo scan \
            --table-name $cluster_table \
            --query 'Items[].[ClusterId.S,Instances.S]' \
        )
    else
        clusters=$(dynamo scan \
            --table-name $cluster_table \
            --query 'Items[].[ClusterId.S,Instances.S]' \
            --filter-expression 'ClusterId = :cluster' \
            --expression-attribute-values  '{":cluster":{"S":"'$cluster'"}}' \
        )
    fi

    printf "Region\tClusterId\tIpAddress\n"
    IFS=$'\n'
    for c in $clusters; do
        unset IFS
        name=$(echo $c | awk '{ print $1 }')
        ips=$(echo $c | tr '{},' '\n' \
            | grep "instance-id" \
            | sed "s/.*: '//" | sed "s/.$//" | uniq
        )
        region=$(dynamo query \
            --table-name $endpoint_table \
            --index-name ClusterId-index \
            --key-condition-expression 'ClusterId = :cluster' \
            --expression-attribute-values  '{":cluster":{"S":"'$name'"}}' \
            --query 'Items[].Region.S' \
            --limit 1
        )
        for ip in $ips; do
            printf "$region\t$name\t$ip\n"
        done
    done
}

while getopts "c:d:x:ehlp" opt; do
    case $opt in
    h)
        echo "Usage: $0 [-e] [-l] [-d delimiter] [-c cluster] [-x group]"
        echo "    -e - report endpoints instead of clusters"
        echo "    -l - use local endpoint for dynamodb"
        echo "    -p - print headers"
        echo "    -d , - use comma as a delimiter for the report"
        echo "    -c cluster_id - show report only for cluster with given ID"
        echo "    -x group1 -x group2 - exclude group1 and group2 from endpoints report"
        exit
        ;;
    c)
        cluster=$OPTARG
        ;;
    d)
        delim=$OPTARG
        ;;
    x)
        exclude="$exclude $OPTARG"
        ;;
    e)
        report_type=endpoints
        ;;
    l)
        endpoint='--endpoint http://localhost:8000'
        ;;
    p)
        print_headers='true'
        ;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
    \?)
        exit 1
        ;;
    esac
done
exclude=$(echo $exclude | sed 's/^,//')

[ "$report_type" == 'endpoints' ] || report_type=clusters

{
    echo Report type: $report_type
    [ -z "$cluster" ] || echo Cluster: $cluster
    [ -z "$exclude" ] || echo Exclude: $exclude
    [ -z "$delim" ] || echo Delimiter: $delim
} 1>&2

report=$(get_$report_type)
[ -z "$delim" ] \
    && echo "$report" \
    || echo "$report" | tr '\t' $delim

