import argparse
import logging
import os

from erad.api.erad_model import Erad_Endpoint

logger = logging.getLogger(__name__)
logger.setLevel(
	os.environ.get('LOGLEVEL', 'DEBUG').upper()
)


def move_cluster(cluster_search, cluster_apply, execute=False):
	logger.debug(f'{cluster_search},{cluster_apply},{execute}')
	for row in Erad_Endpoint.scan(filter_condition=(Erad_Endpoint.ClusterId==cluster_search)):
		old_ip = row.IpAddress
		old_cluster_id = row.ClusterId
		if row.IpAddress == '3.17.122.53':  # cluster id was messed up, using ip to fix cluster id
			if execute is True:
				row.ClusterId = cluster_apply
				row.save()

			print(f'{cluster_apply},{old_cluster_id},{old_ip},{row.Region},{row.IpAddress},{row.Port},{row.Group_ID},{row.ClusterId}')

def main():
	parser = argparse.ArgumentParser(
		description=''' One time use only. Do not use it without understanding what it do. Was written to fix issue that was made on production. '''
	)
	#TODO research what will happen when new cluster will be crated
	parser.add_argument(
		'cluster_search',
		type=str,
		help='cluster_search')

	parser.add_argument(
		'cluster_apply',
		type=str,
		help='cluster_apply')

	parser.add_argument(
		'--execute',
		action='store_true',
		help='If False changes will be saved')

	args = parser.parse_args()

	move_cluster(cluster_search=args.cluster_search, cluster_apply=args.cluster_apply, execute=args.execute)
	logger.info('Done.')


if __name__ == '__main__':
	main()
