import logging
import helper

from config import Config
from dispatcher import Dispatcher, Job
from table import Table


logger = logging.getLogger(__name__)


# This method is called by dispatcher
def delete_table(queue, args):

    table = Table(
        table_name=args['table'],
        region=args['region']
    )

    table.delete()

    logger.debug(
        "Deleting table %s on %s region..." % (
            args['table'],
            args['region']
        )
    )

    queue.put("%s deleted succssfully!" % args['table'])


# Removes old replicator tables
def cleanup():

    logger.info("Cleaning DynamoDB tables...")

    dsp = Dispatcher()

    targets = [
        {
            'region': 'default',
            'prefix': Config().prefixes['cleaner']['default']
        },
        {
            'region': 'replica',
            'prefix': Config().prefixes['cleaner']['replica']
        }
    ]

    for target in targets:
        tables = helper.list_tables(
            Config().region[target['region']],
            target['prefix']
        )

        logger.debug(tables)

        for table in tables:
            job = Job(
                name=table,
                target=delete_table,
                args={
                    'region': target['region'],
                    'table': table
                }
            )

            dsp.add_job(job)

    dsp.run()
