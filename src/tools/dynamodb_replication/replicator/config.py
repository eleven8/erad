import argparse
import helper
import log
import os


# Class to store configurations
class Config(object):

    # Initialize script arguments attribute
    args = None

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(cls.__class__, cls).__new__(cls)
        return cls.instance

    def __init__(self):

        # Regions name
        self.region = {
            'default': os.getenv(
                'AWS_DEFAULT_REGION',   # ENV var
                'us-east-1'             # Default value
            ),
            'source': os.getenv(
                'REPLICATOR_SOURCE_REGION',    # ENV var
                'us-east-1'         # Default value
            ),
            'replica': os.getenv(
                'REPLICATOR_REPLICA_REGION',   # ENV var
                'us-west-2'         # Default value
            )
        }

        # Prefixes of tables for replicator and cleaner methods.
        self.prefixes = {
            'replicator': helper.prefix_to_list(
                os.getenv(
                    'REPLICATOR_TABLES_PREFIX',
                    'Erad_'
                )
            ),
            'cleaner': {
                'default': helper.prefix_to_list(
                    os.getenv(
                        'REPLICATOR_CLEANER_SOURCE_TABLES_PREFIX',
                        'DynamoDB'
                    )
                ),
                'replica': helper.prefix_to_list(
                    os.getenv(
                        'REPLICATOR_CLEANER_REPLICA_TABLES_PREFIX',
                        'Erad_'
                    )
                )
            }
        }

        # Path to scripts
        self.scripts = {
            'bootstrap': os.getenv(
                'REPLICATOR_BOOTSTRAP_SCRIPT_PATH',
                '/usr/local/src/dynamodb-import-export-tool.jar'
            ),
            'replication': os.getenv(
                'REPLICATOR_REPLICATION_SCRIPT_PATH',
                '/usr/local/src/dynamodb-cross-region-replication.jar'
            )
        }

        # Name of stack that is hosting EC2 instance
        self.stack = os.getenv('HOST_STACK_NAME')

    def setup(self):
        log.setup_logging()

        parser = argparse.ArgumentParser()

        parser.add_argument(
            "--cleanup",
            help="Cleanup old replication data.",
            action='store_true'
        )

        parser.add_argument(
            "--skip-bootstrap",
            help="Skip copy source table data on bootstrapping.",
            action='store_true'
        )

        self.args = parser.parse_args()
