import cleaner
import group
import helper
import logging
import signal
import subprocess

from config import Config
from dispatcher import Dispatcher, Job


logger = logging.getLogger(__name__)


# Warming up...
def setup():
    Config().setup()

    # Check if a cleanup was required.
    if Config().args.cleanup:

        # Cleanup this mess!
        cleaner.cleanup()


# Where the magic begins!
def replicate():

    logger.info('Bootstrapping replicators...')

    # Get Erad source tables.
    tables = helper.list_tables(
        Config().region['source'],
        Config().prefixes['replicator']
    )

    # Start and run dispatcher
    dispatcher = Dispatcher()

    # Add jobs to dispatcher
    for table in tables:
        # Create a job to each table
        job = Job(
            name=table,
            target=group.ReplicationGroup,
            args=table
        )

        # Add job
        dispatcher.add_job(job)

    dispatcher.run()

    logger.info('Replication finished successfully.')

    # Set a Tag to stack to inform sync status.
    helper.set_stack_tag("Sync status", "RUNNING")


def teardown():
    logger.info('Killing all subprocess...')

    # Kill all running process
    try:
        subprocess.check_call(['pkill', 'java'])

    except subprocess.CalledProcessError:
        logger.error('Failed to kill all java instances.')

    logger.info('Hasta la vista, baby!')


def main():
    try:
        setup()
        replicate()

        # Block main thread to keep child threads running
        signal.pause()

    except (KeyboardInterrupt, SystemExit):
        teardown()
