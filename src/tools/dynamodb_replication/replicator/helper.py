import boto3
import botocore.exceptions
import logging

from config import Config

logger = logging.getLogger(__name__)


def list_tables(region, prefix):
    try:
        response = boto3.client(
            service_name='dynamodb',
            region_name=region
        ).list_tables()

        tables = [
            table
            for table in response['TableNames']
            for p in prefix
            if table.startswith(p)
        ]

        return tables

    except Exception as e:
        logger.exception(e)


def prefix_to_list(prefix):
    # Split by comma and return the list.
    return prefix.split(',')


def swap_arn_region(arn, region):
    token = arn.split(':')
    token[3] = region
    return ':'.join(token)


def set_stack_tag(key, value):
    try:
        client = boto3.client('cloudformation')
        waiter = client.get_waiter('stack_update_complete')

        client.update_stack(
            StackName=Config().stack,
            UsePreviousTemplate=True,
            Parameters=[
                {'ParameterKey': 'AwsAccessKeyId', 'UsePreviousValue': True},
                {'ParameterKey': 'AwsSecretAccessKey', 'UsePreviousValue': True},
                {'ParameterKey': 'TablesPrefix', 'UsePreviousValue': True},
                {'ParameterKey': 'Cleanup', 'UsePreviousValue': True},
                {'ParameterKey': 'SkipBootstrap', 'UsePreviousValue': True},
                {'ParameterKey': 'SourceRegion', 'UsePreviousValue': True},
                {'ParameterKey': 'ReplicaRegion', 'UsePreviousValue': True},
                {'ParameterKey': 'KeyName', 'UsePreviousValue': True},
                {'ParameterKey': 'SecurityGroupId', 'UsePreviousValue': True}
            ],
            Tags=[
                {
                    'Key': key,
                    'Value': value
                }
            ]
        )

        waiter.wait(StackName=Config().stack)

    except botocore.exceptions.ParamValidationError:
        logger.warn("Looks like didn't hosted by a stack.")

    except Exception as e:
        logger.error("Unexpected exception: %s" % e.message)
