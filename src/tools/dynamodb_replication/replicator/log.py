import logging
import logging.config
import logging.handlers
import os
import yaml


# Class to filter logs from libs
class Whitelist(logging.Filter):
    def __init__(self, *whitelist):
        super(Whitelist, self).__init__()
        self.whitelist = [logging.Filter(name) for name in whitelist]

    def filter(self, record):
        return any(f.filter(record) for f in self.whitelist)

    def add(self, name):
        self.whitelist.append(logging.Filter(name))


def setup_logging(
    default_path='logging.yaml',
    default_level=logging.INFO,
    env_key='LOG_CFG'
):
    path = default_path
    value = os.getenv(env_key, None)

    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

    module_name = __name__.split('.')

    for handler in logging.root.handlers:
        handler.addFilter(
            Whitelist(module_name[0])
        )
