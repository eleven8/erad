import logging
import Queue
import threading
import time


logger = logging.getLogger(__name__)


# This dispatcher creates a thread to each job added
class Dispatcher(object):

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(cls.__class__, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        self.max_simultaneously_requests = 2
        self.context_queue = Queue.Queue()
        self.jobs_queue = Queue.Queue()
        self.jobs_remaining = 0
        self.jobs_total = 0

    def dispatch(self, job):

        thread = threading.Thread(
            name=job.name,
            target=job.target,
            args=(self.context_queue, job.args)
        )

        thread.start()

    def add_job(self, job):
        self.jobs_queue.put(job)
        self.jobs_total += 1
        self.jobs_remaining += 1

    def pool(self):
        # Run while still have jobs to be executed.
        while not self.jobs_queue.empty():

            # Make sure it will dispatch up to max_simultaneously_requests
            counter = self.max_simultaneously_requests \
                if self.max_simultaneously_requests < self.jobs_queue.qsize() \
                else self.jobs_queue.qsize()

            # Dispatch slice of jobs on queue
            for i in range(counter):
                self.dispatch(self.jobs_queue.get())

            # Wait until current threads finish
            while counter:

                # Check if have some finished thread
                if self.context_queue.empty():
                    # Sleep to minimize CPU usage
                    time.sleep(5)

                else:
                    # Get response from queue
                    response = self.context_queue.get()

                    # Finished threads - i.e. [2/10]
                    finished = str(self.jobs_total - self.jobs_remaining + 1)
                    self.jobs_remaining -= 1

                    # Inform status
                    logger.info(
                        "[%s/%s] %s" % (
                            finished,
                            str(self.jobs_total),
                            response
                        )
                    )

                    time.sleep(1)

                    counter -= 1

    def run(self):
        # Starting polling to check completed jobs
        self.pool()


class Job(object):

    def __init__(
            self,
            name=None,
            target=None,
            args=None
    ):
        self.name = name
        self.target = target
        self.args = args
