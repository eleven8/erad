import boto3
import logging
import time

from botocore.exceptions import ClientError
from config import Config


logger = logging.getLogger(__name__)


class Table(object):

    def __init__(
            self,
            table_name,
            region='source'
    ):
        try:
            self.table = boto3.resource(
                'dynamodb',
                region_name=Config().region[region]
            ).Table(table_name)

            self.table.load()

            # Check only if it is a source table.
            if region == 'source':
                # Check if Streams are enabled on source table.
                if not self.stream_enabled:
                    # If not, enable streams...
                    self.enable_streams()

        except ClientError as e:
            if e.response['Error']['Code'] == 'ResourceNotFoundException':
                logger.error(
                    'Table %s does not exists on %s region.' % (
                        table_name, region
                    )
                )

        except LookupError as e:
            logger.debug(e.message)

        except Exception as e:
            logger.exception('Unexpected exception: %s' % e)

    @property
    def region(self):
        # Split table arn to get region name
        chunks = str(self.table_arn).split(':')
        region_name = chunks[3]

        # Verify if region is source or replica
        region = [
            key
            for key in Config().region.keys()
            if Config().region[key] == region_name and key in ['source', 'region']
        ]

        return region[0]

    @property
    def table_name(self):

        self.table.load()

        return self.table.table_name

    @property
    def table_arn(self):
        self.table.load()

        return self.table.table_arn

    @property
    def attribute_definitions(self):
        self.table.load()

        return self.table.attribute_definitions

    @property
    def key_schema(self):
        self.table.load()

        return self.table.key_schema

    @property
    def provisioned_throughput(self):
        self.table.load()

        return ProvisionedThroughput(
            self.table.provisioned_throughput
        ).__dict__

    @property
    def global_secondary_indexes(self):
        self.table.load()

        return self.table.global_secondary_indexes

    # Stream must be set as "NEW_AND_OLD_IMAGES" to work properly
    @property
    def stream_enabled(self):
        self.table.load()

        if self.table.stream_specification is not None:
            if self.table.stream_specification['StreamViewType'] == "NEW_AND_OLD_IMAGES":
                return True

        return False

    @property
    def is_active(self):
        self.table.load()

        if self.table.table_status == 'ACTIVE':
            return True
        else:
            return False

    def delete(self):
        self.table.load()

        while not self.is_active:
            time.sleep(1)

        self.table.delete()

        self.table.wait_until_not_exists()

    def enable_streams(self):
        self.table = self.table.update(
            StreamSpecification={
                'StreamEnabled': True,
                'StreamViewType': 'NEW_AND_OLD_IMAGES'
            }
        )

        # Wait until table become active.
        while not self.is_active:
            time.sleep(1)

    def add_gsi(self, attr, gsi):

        # Check if index already exists
        def index_exists(index_name):
            try:
                for index in self.global_secondary_indexes:
                    if index['IndexName'] == index_name:
                        return True
                return False
            # Raised if there are no indexes on replica
            except TypeError:
                logger.debug("Index not exists.")

        # Check if index become active
        def index_active(index_name):
            for index in self.global_secondary_indexes:
                if index['IndexName'] == index_name:
                    if index['IndexStatus'] == 'ACTIVE':
                        return True
            return False

        # boto3 API allows create just one index simultaneously
        for idx in gsi:

            logger.debug(idx)

            # Check if index already exists
            if not index_exists(idx['IndexName']):
                data = [
                    {
                        'Create': {
                            'IndexName': idx['IndexName'],
                            'KeySchema': idx['KeySchema'],
                            'Projection': idx['Projection'],
                            'ProvisionedThroughput': {
                                'ReadCapacityUnits': idx['ProvisionedThroughput']['ReadCapacityUnits'],
                                'WriteCapacityUnits': idx['ProvisionedThroughput']['ReadCapacityUnits']
                            }
                        }
                    }
                ]

                logger.debug(data)

                self.table = self.table.update(
                    AttributeDefinitions=attr,
                    GlobalSecondaryIndexUpdates=data
                )

                # Time to change table status to "Updating"
                time.sleep(2)

                # Set timeout to 30min
                timeout = 360

                # Wait until index become active
                while not index_active(idx['IndexName']):
                    if timeout > 0:
                        time.sleep(5)
                        timeout -= 1

                        # Refresh table
                        self.table.load()

                    else:
                        # Timeout!
                        logger.error(
                            "Timeout creating index %s of table %s" % (
                                idx['IndexName'],
                                self.table_name
                            )
                        )
                        break

                logger.debug("%s index created on %s" % (idx['IndexName'], self.table_name))


class ProvisionedThroughput:
    def __init__(self, provisioned_throughput):
        self.ReadCapacityUnits = provisioned_throughput['ReadCapacityUnits']
        self.WriteCapacityUnits = provisioned_throughput['WriteCapacityUnits']


class GlobalSecondaryIndexes:
    def __init__(self, index):
        self.IndexName = index['IndexName']
        self.ProvisionedThroughput = ProvisionedThroughput(
            index['ProvisionedThroughput']
        ).__dict__

        self.KeySchema = list()
        for key_schema in index['KeySchema']:
            key = dict()
            key['keyType'] = key_schema['KeyType']
            key['attributeName'] = key_schema['AttributeName']

            self.KeySchema.append(key)

        self.Projection = index['Projection']


class AttributeDefinitions:
    def __init__(self, attribute):
        self.AttributeName = attribute['AttributeName']
        self.AttributeType = attribute['AttributeType']


# Clone source table schema to informed region
def clone_schema(source_table, region):

    client = boto3.client(
        'dynamodb',
        region_name=Config().region[region]
    )

    # Get only attributes that are in key schema
    attr = [
        attr
        for attr in source_table.attribute_definitions
        for key in source_table.key_schema
        if attr['AttributeName'] == key['AttributeName']
    ]

    logger.debug(attr)

    # Try to create table on replica region
    try:
        response = client.create_table(
            TableName=source_table.table_name,
            AttributeDefinitions=attr,
            KeySchema=source_table.key_schema,
            ProvisionedThroughput=source_table.provisioned_throughput
        )

        logger.debug(
            'Creating %s table on %s region...' % (
                source_table.table_name,
                region
            )
        )

        client.get_waiter('table_exists').wait(TableName=source_table.table_name)

    except ClientError as e:
        if e.response['Error']['Code'] == 'ResourceInUseException':
            logger.debug(
                'Table %s already exists in %s region.' % (
                    source_table.table_name, region
                )
            )

    # Get the new table
    new_table = Table(
        source_table.table_name,
        region=region
    )

    # If source table have indexes, add them to new table.
    if source_table.global_secondary_indexes:
        new_table.add_gsi(
            source_table.attribute_definitions,
            source_table.global_secondary_indexes
        )

    return new_table

