import logging
import subprocess
import table
import weakref

from config import Config


logger = logging.getLogger(__name__)


class ReplicationGroup(object):

    __instances = set()

    # Make a unique instance to each table.
    def __new__(cls, context_queue, table_name):

        if hasattr(cls, 'instance'):
            for obj in cls.__get_instances():
                if obj.table_name == table_name:

                    cls.instance = obj

                    return cls.instance

        cls.instance = super(cls.__class__, cls).__new__(cls, context_queue, table_name)
        cls.__instances.add(weakref.ref(cls.instance))

        return cls.instance

    def __init__(
            self,
            context_queue,
            table_name
    ):
        self.table_name = table_name
        self.start(context_queue)

    @classmethod
    def __get_instances(cls):
        dead = set()
        for ref in cls.__instances:
            obj = ref()
            if obj is not None:
                yield obj
            else:
                dead.add(ref)
        cls.__instances -= dead

    # Creates a copy of source table into destination region - if it doesn't exists yet.
    def table_copy(self):
        source_table = table.Table(
            self.table_name,
            'source'
        )

        table.clone_schema(
            source_table,
            'replica'
        )

        logger.debug('%s table copied to destination region.' % self.table_name)

    # Copy source table existing data to destionation table
    def bootstrap(self):

        cmd = 'java -jar %s ' \
            '--sourceTable "%s" ' \
            '--sourceEndpoint "https://dynamodb.%s.amazonaws.com" ' \
            '--destinationTable "%s" ' \
            '--destinationEndpoint "https://dynamodb.%s.amazonaws.com" ' \
            '--readThroughputRatio 1 ' \
            '--writeThroughputRatio 1' % (
                Config().scripts['bootstrap'],
                self.table_name,
                Config().region['source'],
                self.table_name,
                Config().region['replica']
            )

        logger.debug(cmd)

        try:
            pipe = subprocess.Popen(
                cmd.split(' '),
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )

            pipe.communicate()

        except (
                subprocess.CalledProcessError,
                OSError,
                ValueError
        ) as e:
            logger.critical(e)

        logger.debug('%s table bootstrapped.' % self.table_name)

    # Enable real-time updates.
    def sync(self, queue):

        # Set checkpoint table name like DynamoDBCrossRegionReplication-us-east-1-Erad_ApiKey-us-west-2-Erad_ApiKey
        taskname = "-%s-%s-%s-%s" % (
            Config().region['source'],
            self.table_name,
            Config().region['replica'],
            self.table_name
        )

        cmd = 'java -jar %s ' \
            '--sourceTable "%s" ' \
            '--sourceEndpoint "https://dynamodb.%s.amazonaws.com" ' \
            '--destinationTable "%s" ' \
            '--destinationEndpoint "https://dynamodb.%s.amazonaws.com" ' \
            '--taskName %s'% (
                Config().scripts['replication'],
                self.table_name,
                Config().region['source'],
                self.table_name,
                Config().region['replica'],
                taskname
            )

        logger.debug(cmd)

        try:
            pipe = subprocess.Popen(
                cmd.split(' '),
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )

            queue.put('%s table sync starting...' % self.table_name)

            pipe.communicate()

        except (
                subprocess.CalledProcessError,
                OSError,
                ValueError
        ) as e:
            queue.put('%s sync table failed: %s' % (self.table_name, e))

    def start(self, context_queue):
        try:
            self.table_copy()

            # Bootstrap table if skip was not required.
            if not Config().args.skip_bootstrap:
                self.bootstrap()

            self.sync(context_queue)

        except (KeyboardInterrupt, SystemExit):
            logger.debug('Process of table %s terminated by user.' % self.table_name)
