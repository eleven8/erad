#!/bin/bash

echo "Packing replication script..."

tar --exclude="pack.sh" --exclude=".*" --exclude="*.pyc" --exclude="*.log" -czvf dynamodb_replication.tar.gz * 2> /dev/null

echo "Uploading to S3..."

aws s3 cp dynamodb_replication.tar.gz s3://teamfortytwo.deploy/dynamodb_replication/dynamodb_replication.tar.gz

echo "Cleaning..."

rm -rf dynamodb_replication.tar.gz

echo "Pack is done!"
