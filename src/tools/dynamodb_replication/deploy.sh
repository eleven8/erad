#!/bin/bash

# Update and install dependencies.
yum remove -y java
yum update -y
yum install -y java-1.8.0-openjdk awscli

# Install Python dependencies
pip install --upgrade pip
pip install -r /usr/local/src/requirements.txt

# Download and unpack dynamodb tools
aws s3 cp s3://teamfortytwo.deploy/public/dynamodb_replication_tools.tar.gz /usr/local/src/dynamodb_replication_tools.tar.gz
tar -C /usr/local/src -zxvf /usr/local/src/dynamodb_replication_tools.tar.gz

export BOOTSTRAP_SCRIPT_PATH=/usr/local/src/dynamodb-import-export-tool.jar
export REPLICATION_SCRIPT_PATH=/usr/local/src/dynamodb-cross-region-replication.jar
