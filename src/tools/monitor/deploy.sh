#!/bin/bash
################################################################################
##
## Copyright 2002-2015 Eleven Wireless Inc.  All rights reserved.
##
## This file is the sole property of Eleven Wireless Inc. and can not be used
## or distributed without the expressed written permission of
## Eleven Wireless Inc.
##
################################################################################
set -e
set -x

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

username='ec2-user'

S3BUCKET=${DeployBucket:-teamfortytwo.deploy}
TARGET=/opt/monitor
PJS_URL=https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
INSTANCE=`curl http://169.254.169.254/latest/meta-data/instance-id`

# system packages
yum -y update
amazon-linux-extras install epel -y
yum install --enablerepo=epel -y \
	libnl \
	libtalloc \
	nodejs \
	npm \
	python3

pip3 install -U -r requirements.txt


# install freeradius
aws s3 cp s3://$S3BUCKET/freeradius_server.tar.gz .
tar fxz freeradius_server.tar.gz -C / \
    usr/local/bin/radclient \
    usr/local/lib/libfreeradius-radius.so \
    usr/local/share/freeradius/*

# install eapol_test
aws s3 cp s3://$S3BUCKET/eapol_test.tar.gz .
tar fxz eapol_test.tar.gz -C / \
    usr/local/bin/eapol_test
chmod +x /usr/local/bin/eapol_test

# copy files
mkdir -p $TARGET
cp radius-monitor.py $TARGET
cp peap-mschapv2.conf $TARGET
cp readme.txt $TARGET
cp config.json $TARGET
cp dictionary.txt $TARGET

# add alarm for cpu-surplus-credits-charged metric
export AWS_ACCESS_KEY_ID=AKIAIDR4RVSXLXAIAUYQ
export AWS_SECRET_ACCESS_KEY=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
aws cloudwatch put-metric-alarm \
    --alarm-name cpu-surplus-credits-charged-$INSTANCE-monitor \
    --alarm-description "CPUSurplusCreditsCharged " \
    --metric-name CPUSurplusCreditsCharged --namespace AWS/EC2 \
    --statistic Average --period 300 --threshold 0 \
    --comparison-operator GreaterThanThreshold \
    --dimensions Name=InstanceId,Value=$INSTANCE \
    --evaluation-periods 1 \
    --alarm-actions arn:aws:sns:us-west-2:439838887486:trouble \
    --unit Count --region us-west-2

source ./aws.cfg
export AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION
export PYTHONPATH=$PYTHONPATH:$TARGET
export PYTHONUNBUFFERED=no_buffer
echo $LD_LIBRARY_PATH
cd $TARGET
python3 radius-monitor.py \
    2> radius-monitor-error.log \
    1> radius-monitor.log \
    & disown

