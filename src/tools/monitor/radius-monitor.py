#!/usr/bin/python
import socket, sys, json
import pyrad.packet
import pyrad.client
import pyrad.dictionary
import time
import boto3
import subprocess
from io import StringIO

# aws cloudwatch
cloudwatch = boto3.client('cloudwatch') # USES ENV VARS

def load_config():
    with open('config.json', 'r') as fj:
        conf = json.load(fj)

    for server in conf['servers']:
        for k in list(conf.keys()):
            if k in ['servers', 'packets']:
                continue
            if not server.get(k):
                server[k] = conf[k]

        # Check if timeout is lower than intervals
        for k, packet in list(conf['packets'].items()):
            if server['timeout'] > packet['interval']:
                print("Timeout must be lower than interval. Check '$config_file'.")
                sys.exit(1)
            if packet.get('send') is None:
                conf['packets'][k]['send'] = globals()['%s_send_packet' % k]

    return conf


def create_connections(conf):
    # Initialize Radius Server connections
    DICTIONARY = open('dictionary.txt').read()

    for s in conf['servers']:
        s['client'] = pyrad.client.Client(
            server=s['host'],
            secret=s['secret'].encode(),
            authport=s['authport'],
            dict=pyrad.dictionary.Dictionary(StringIO(DICTIONARY))
        )
        s['client'].timeout = int(s['timeout'])
        s['client'].retries = int(s['retries'])


# method to store cloudwatch metrics
def store_cloudwatch(namespace, metric_name, value, unit='None'):
    response = cloudwatch.put_metric_data(
        Namespace=namespace,
        MetricData=[
            {
                'MetricName': metric_name,
                'Value': value,
                'Unit': unit
            }
        ]
    )


# method to send radius packets
def standard_send_packet(server, packet, calling_station_id):
    req = server['client'].CreateAuthPacket(code=pyrad.packet.AccessRequest)
    req["User-Name"] = packet['user']
    req["User-Password"] = req.PwCrypt(packet['pass'])
    req["Called-Station-Id"] = server['called_station_id']
    req["Calling-Station-Id"] = calling_station_id

    try:
        reply = server['client'].SendPacket(req)
    except pyrad.client.Timeout:
        print("RADIUS server does not reply")
        return False
    except socket.error as error:
        print("Network error: " + str(error))
        return False
    except Exception as e:
        print("GENERAL error:", e)
        return False

    server['client']._CloseSocket()
    return reply.code == pyrad.packet.AccessAccept


# same for proxy
proxy_send_packet = standard_send_packet


def peap_send_packet(server, packet, calling_station_id):
    p = subprocess.Popen(
        [
            "/usr/local/bin/eapol_test",
            "-a", server['host'],
            "-p", str(server['authport']),
            "-c", "/opt/monitor/peap-mschapv2.conf",
            "-s", str(server['secret']),
            "-N", "30:s:" + server['called_station_id'],
            "-t", str(server['timeout']),
            "-C", "rad_eap_test",
            "-M", str(calling_station_id)
        ],
        stdout=subprocess.PIPE,
        text=True
    )
    output, err = p.communicate()
    return "SUCCESS" in output


def main():
    conf = load_config()
    create_connections(conf)

    while True:
        for k, packet in list(conf['packets'].items()):
            if packet.get('step') and packet['interval'] > int(time.time()) - packet['step']:
                continue
            packet['step'] = int(time.time())

            for server in conf['servers']:
                start_time = float(time.time())
                res = packet['send'](
                    server,
                    packet,
                    "00:00:00:00:00:" + str(packet['step'])[-2:])

                if res:
                    response_time = float(time.time() - start_time)
                    print("%s :: %s :: Success with response time %s" % (server['namespace'], k, response_time))
                    store_cloudwatch(server['namespace'], '%s-accept-success' % k, 1)
                    store_cloudwatch(server['namespace'], '%s-response-time' % k, response_time, unit='Seconds')
                else:
                    print("%s :: %s :: Fail" % (server['namespace'], k))
                    store_cloudwatch(server['namespace'], '%s-accept-success' % k, 0)

        # Sleep a little to minimize CPU utilization
        time.sleep(0.25)


if __name__ == '__main__':
    main()

