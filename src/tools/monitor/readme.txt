The monitoring server is manually deployed. It is not yet automated.

Region Note: SMS alerts through SNS are only available for topics created in the us-east-1 region.
Therefore, all our metrics are in the us-east-1 region.

To deploy:
   - Use a standard-stack deployment.

By default, this should automatically spin up the monitor server and begin sending two RADIUS packets:
   - standard every 5 seconds.
   - proxy every 30 seconds.
Two packets are sent, one as standard RADIUS (babble), the other as a proxy to ElevenOS (SR-980-83/erad-monitor).
Both tests should succeed. The results are logged to CloudWatch (1 for success, 0 for failure):
   Region: us-east-1
   Namespace: RADIUS-Verify
   Metric #1: standard-accept-success
   Metric #2: proxy-accept-success

All the parameters, like user, server, etc, can be modified in a config file (/opt/monitor/config.json) -
as soon the parameters are changed on config file, the monitor should be restarted.

Alarms can be created on each to alert if less than a certain percentage of successes occurred.
For example, we expect to receive 12 standard successes a minute. If the sum over one minute is less than 8,
there is probably a problem worth alerting on.


