import argparse
import logging
import platform
import random
import time
from collections import namedtuple
from datetime import datetime
from os import environ
from subprocess import PIPE
from subprocess import run

import boto3
from botocore.exceptions import ClientError

LOGGER_FORMAT = '%(asctime)-15s:%(name)s : line# %(lineno)s : %(message)s'
numeric_level = getattr(logging, environ.get('LOGLEVEL', 'INFO'), None)

logging.basicConfig(
	level=numeric_level,
	format=LOGGER_FORMAT
)

logger = logging.getLogger(__name__)

Endpoint = namedtuple('Endpoint', ['cluster_id', 'ip', 'port', 'secret'])


class EndpointTester:
	def __init__(self, input_args):
		self.radius_payload = 'User-Name=babble,User-Password=3000,Called-Station-Id=ba-bb-1e-ba-bb-1e'
		if environ.get('AWS_PRODUCTION_ENV') == 'True':
			self.base_config = {
				'region_name': environ.get('AWS_DEFAULT_REGION', 'us-west-2'),
			}
		else:
			self.base_config = {
				'endpoint_url': 'http://localhost:8000',
				'region_name': environ.get('AWS_DEFAULT_REGION', 'us-west-2'),
				'aws_access_key_id': environ.get('AWS_ACCESS_KEY_ID', 'localhost-aws-access-key'),
				'aws_secret_access_key': environ.get('AWS_SECRET_ACCESS_KEY', 'localhost-aws-secret-key'),
			}

		self.endpoint_table_name = 'Erad_Endpoint_Global'
		self.sns_topic_name = 'monitor_endpoints'

		self.log_group_name = 'monitor_endpoints'
		self.log_stream_name = platform.node()
		self.sequence_token_fail = None
		self.sequence_token_success = None

		self.pause_between_tests = input_args.pause_between_tests
		self.radius_response_timeout = input_args.test_response_timeout
		self.radius_repeat = input_args.test_retries
		self.alive_ping_timeout = 60
		self.radius_path_to_dictionary = input_args.path_to_dictionary
		self.minimal_conf_update_tiemout = input_args.conf_update_timeout
		self.current_date  = datetime.utcnow().strftime("%Y%m%d")

		self.dyndb_connection = None
		self.dynamodb_client = None
		self.cloudwatch_client = None
		self.cloudwatch_log_client = None
		self.sns_client = None
		self.sns_topic_arn = None

		self.init_aws_connections()
		self.init_cloudwatch_logs()
		self.init_sns()

	def init_aws_connections(self):
		connection_config = self.base_config.copy()
		self.dyndb_connection = boto3.resource('dynamodb', **connection_config)
		self.dynamodb_client = boto3.client('dynamodb', **connection_config)

		region_only_settings = {'region_name': connection_config['region_name']}

		self.cloudwatch_client = boto3.client('cloudwatch',  **region_only_settings)
		self.cloudwatch_log_client = boto3.client('logs', **region_only_settings)
		self.sns_client = boto3.client('sns', **region_only_settings)

	def init_cloudwatch_logs(self):
		response = self.cloudwatch_log_client.describe_log_groups(
			logGroupNamePrefix=self.log_group_name
		)
		if len(response['logGroups']) == 0:
			self.cloudwatch_log_client.create_log_group(
				logGroupName=self.log_group_name
			)

		for stream_type in {'success', 'fail'}:
			response = self.cloudwatch_log_client.describe_log_streams(
				logGroupName=self.log_group_name,
				logStreamNamePrefix=f'{self.log_stream_name}_{self.current_date}_{stream_type}'

			)
			if len(response['logStreams']) == 0:
				self.cloudwatch_log_client.create_log_stream(
					logGroupName=self.log_group_name,
					logStreamName=f'{self.log_stream_name}_{self.current_date}_{stream_type}'
				)
			else:
				setattr(self, f'sequence_token_{stream_type}', response['logStreams'][0].get('uploadSequenceToken'))


	def init_sns(self):
		response = self.sns_client.create_topic(
			Name=self.sns_topic_name,
			Attributes={
				'DisplayName': self.sns_topic_name,
			}
		)
		self.sns_topic_arn = response['TopicArn']

	def send_alive_ping(self):
		self.cloudwatch_client.put_metric_data(
			Namespace='Endpoints monitoring',
			MetricData=[
				{
					'MetricName': 'monitor_endpoints alive ping',
					'Timestamp': datetime.utcnow(),
					'Value': 1,
					'Unit': 'Count',
				},
			]
		)

	def send_alarm_event(self, endpoint):
		_ = self.sns_client.publish(
			TopicArn=self.sns_topic_arn,
			Message=f'Check of endpoint Cluster: {endpoint.cluster_id}, {endpoint.ip}:{endpoint.port} failed',
			Subject='Endpoint status',
		)

	def log_failed_endpoint(self, endpoint):
		self._log_endpoint_by_type(endpoint, 'fail')

	def log_succcess_endpoint(self, endpoint):
		self._log_endpoint_by_type(endpoint, 'success')

	def _log_endpoint_by_type(self, endpoint, type):
		parameters = {
				'logGroupName': self.log_group_name,
				'logStreamName': f'{self.log_stream_name}_{self.current_date}_{type}',
				'logEvents': [
					{
						'timestamp': int(time.time()*1000),
						'message': f'Check of endpoint Cluster: {endpoint.cluster_id}, {endpoint.ip}:{endpoint.port} {type}'
					},
				],
			}

		seqToken = getattr(self, f"sequence_token_{type}")
		if seqToken is not None:
			parameters['sequenceToken'] = seqToken

		response = self._send_log(parameters)
		setattr(self, f"sequence_token_{type}", response['nextSequenceToken'])

	def _send_log(self, parameters):
		try:
			response = self.cloudwatch_log_client.put_log_events(**parameters)
		except ClientError as err:
			logger.warning('possible communication error with AWS, AWS SDK resending event')
			logger.exception(err)
			if err.response['Error']['Code'] == 'DataAlreadyAcceptedException':
				return
			elif err.response['Error']['Code'] == 'InvalidSequenceTokenException':
				parameters['sequenceToken'] = err.response['expectedSequenceToken']
				response = self._send_log(parameters)
			else:
				raise err
		return response

	def standard_radius_test(self, endpoint):
		try:
			cmd_args = [
				'radclient',
				'-t', self.radius_response_timeout,
				'-x',
				'-r', self.radius_repeat,
				'-D', self.radius_path_to_dictionary,
				'-s',
				f'{endpoint.ip}:{endpoint.port}',
				'auth',
				f'{endpoint.secret}',
			]
			cmd_args = [x for x in map(lambda x: str(x), cmd_args)]

			proc = run(
				cmd_args,
				stdout=PIPE,
				stderr=PIPE,
				input=self.radius_payload,
				encoding='ascii'
			)
			if 'Received Access' in proc.stdout:
				radius_response_received = True
				self.log_succcess_endpoint(endpoint)
			else:
				radius_response_received = False
		except Exception as err:
			logger.exception(err)
			radius_response_received = False

		if radius_response_received is False:
			self.send_alarm_event(endpoint)
			self.log_failed_endpoint(endpoint)

	def load_endpoints(self):
		paginator = self.dynamodb_client.get_paginator('scan')
		response_iterator = paginator.paginate(
			TableName=self.endpoint_table_name,
			FilterExpression='Group_ID <> :x',
			ExpressionAttributeValues={
				':x': {'S': '::dirty'},
			}
		)
		endpoint_list = []

		for response in response_iterator:
			items = response['Items']

			for item in items:
				endpoint_list.append(
					Endpoint(
						cluster_id=item['ClusterId']['S'],
						ip=item['IpAddress']['S'],
						port=item['Port']['S'],
						secret=item['Secret']['S'])
				)
		return endpoint_list

	def runner(self):
		last_conf_update = 0
		last_ping_alive = 0
		endpoints = []
		while True:
			cur_timestamp = time.time()

			if datetime.utcnow().strftime("%Y%m%d") != self.current_date:
				self.init_cloudwatch_logs()

			if cur_timestamp - last_conf_update >= self.minimal_conf_update_tiemout:
				endpoints = self.load_endpoints()
				random.shuffle(endpoints)
				last_conf_update = cur_timestamp

			for i in range(len(endpoints)):
				cur_timestamp = time.time()
				if cur_timestamp - last_ping_alive >= self.alive_ping_timeout:
					self.send_alive_ping()
					last_ping_alive = cur_timestamp

				selected_endpoint = endpoints[i]
				self.standard_radius_test(selected_endpoint)
				time.sleep(self.pause_between_tests)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description='Tool to monitor endpoints',
		formatter_class=argparse.ArgumentDefaultsHelpFormatter
	)

	parser.add_argument(
		'--conf-update-timeout',
		metavar='<conf-update-timeout:int>',
		type=int,
		help='Minimal timeout between endpoints list will be updated (seconds)',
		default=300
		)
	parser.add_argument(
		'--pause-between-tests',
		metavar='<puase-between-tests:int>',
		type=int,
		help='Pause between tests (seconds)',
		default=10
		)
	parser.add_argument(
		'--test-response-timeout',
		metavar='<test-response-timeout:int>',
		type=int,
		help='How much seconds wait before assuming that test radius request is lost.'
		' Or value that will be passed to `radclient` command as value for `-t` parameter (int, seconds)',
		default=10
		)
	parser.add_argument(
		'--test-retries',
		metavar='<test-retries:int>',
		type=int,
		help='If test radius request is lost how many retries to reach radius server, utility should make.'
		' Or value that will be passed to `radclient` command as value for `-r` parameter (int)',
		default=6
		)
	parser.add_argument(
		'--path-to-dictionary',
		metavar='<path-to-dictionary:str>',
		type=str,
		help='Path to directory with radius dictionary file',
		default='/usr/share/freeradius/'
		)

	input_args = parser.parse_args()

	endpoint_tester = EndpointTester(input_args)
	endpoint_tester.runner()
