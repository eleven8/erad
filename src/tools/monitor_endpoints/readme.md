## How this tool works
Monitoring tool load all Endpoints from table `Erad_Endpoint_Global`.
Shuffle list of endpoints and start send test radius packets to each endpoint. Radius packet contain credentials from standard ERAD monitoring account. The most of endpoints should reject provided credentials.
Tool will check response from endpoint that will be passed to stdout. If output contains `Received Access` string (that substring should match with `Received Access-Accept` or `Received Access-Reject`), which means radius response packets received and endpoint is alive. Otherwise endpoint is dead.

Also if `Erad_Endpoint_Global` contains wrong shared radius secret value endpoint will be detected as dead.

If endpoint is dead tool will send message to AWS SNS `monitor_endpoints` topic (it will be created when tool starts), and log information about dead endpoint in AWS CloudWatch Logs (group `monitor_endpoints`).

After all endpoints will be checked. Tool will check how many seconds were passed since list of endpoints was loaded. If this value exceeded `<conf-update-timeout:int>` seconds, then list will be updated.

Also tool send  each 60 seconds "alive ping" metric to CloudWatch to indicate that it is working. If this metric will not be available during 2 minutes, alarm `monitor_endpoints_alive` will go into state `ALARM`.




## How to deploy

### Deploy from local computer

1. Use the next script to deploy EC2 instance with this tool `erad/deploy/monitor_endpoints.sh`

Example

```bash
cd ~/erad/src/tools/monitor_endpoints
bash deploy_from_local.sh <s3-bucket-name-for-deploy> <AWS-security-group> <ssh-key-name>
```

2. This monitoring tool required access to `Erad_Endpoint_Global` DynamoDB table, and for the next AWS services:

- CloudWatch Logs
- CloudWatch Alarms
- CloudWatch Metrics
- SNS
- S3 (only to deploy purposes)

All required access rights will be given through IAM Role that will be attached to EC2 instance during deployment.

If tool will not be able to connect or use some of requested resource, for example missing DynamoDB table, service will be stoped. Logs will be writtern in systemlog.

3. After successful deployment monitoring tool will be launched as service. To change default parameters edit `systemd` unit file `/etc/systemd/system/monitor-endpoints.service`

After changing service parameter - reload service configuration

`systemctl daemon-reload`

Restart service

`systemctl restart monitor-endpoints.service`

To check that service working use
`systemctl status monitor-endpoints.service`


Use `python3 -m monitor_endpoints -h` to see all available options

Currently this options are available
```
  --conf-update-timeout <conf-update-timeout:int>
                        Minimal timeout between endpoints list will be updated
                        (seconds) (default: 300)
  --pause-between-tests <puase-between-tests:int>
                        Pause between tests (seconds) (default: 10)
  --test-response-timeout <test-response-timeout:int>
                        How much seconds wait before assuming that test radius
                        request is lost. Or value that will be passed to
                        `radclient` command as value for `-t` parameter (int,
                        seconds) (default: 10)
  --test-retries <test-retries:int>
                        If test radius request is lost how many retries to
                        reach radius server, utility should make. Or value
                        that will be passed to `radclient` command as value
                        for `-r` parameter (int) (default: 6)
  --path-to-dictionary <path-to-dictionary:str>
                        Path to directory with radius dictionary file
                        (default: /usr/share/freeradius/)
```

### Deploy using Jenkins

1. Use job `tool-pack-anything-to-standard-stack` to deploy it.

