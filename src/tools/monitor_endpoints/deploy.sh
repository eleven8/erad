#!/bin/sh

set -e
set -x

pushd /usr/local/src

yum -y update
yum -y install \
	python3 \
	freeradius-utils   # it contains radclient, that is used for endpoints testings, also it will install freeradius server

# removing default freeradius configuration to prevent accidental start of freeradius daemon
rm -rf /etc/raddb/sites-enabled/

pip3 install .
popd

mkdir -p /etc/systemd/system/monitor-endpoints.service.d

cat << EOF > /etc/systemd/system/monitor-endpoints.service
[Unit]
Description=Monitor endpoints
After=network.target
StartLimitIntervalSec=1
StartLimitBurst=2

[Service]
Type=simple
Restart=always
RestartSec=1
User=ec2-user
ExecStart=/usr/bin/env python3 -m monitor_endpoints

[Install]
WantedBy=multi-user.target

EOF

cat << EOF > /etc/systemd/system/monitor-endpoints.service.d/override.conf
[Service]
Environment="AWS_PRODUCTION_ENV=True"

EOF

systemctl enable monitor-endpoints
systemctl start monitor-endpoints








