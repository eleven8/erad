import setuptools

setuptools.setup(
	name="monitor_endpoints",
	version="0.1",
	author="",
	author_email="",
	description="Utility to monitor endpoints",
	url="",
	install_requires=[
		'boto3'
	],
	py_modules=['monitor_endpoints'],
	classifiers=(
		"Programming Language :: Python :: 3",
		"Operating System :: Tested only on Linux",
	),
)
