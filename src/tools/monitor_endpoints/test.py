# This test requires to have available real credentials because EndpointTester class initialize CloudWatch service
# and there is no equivalent local service.
# Tests only check that data about endpoints loaded correctly.

from os import environ

import boto3
import pytest
from monitor_endpoints import Endpoint
from monitor_endpoints import EndpointTester

base_config = {
	'endpoint_url': None if environ.get('AWS_PRODUCTION_ENV') == 'True' else 'http://localhost:8000',
	'region_name': environ.get('AWS_DEFAULT_REGION', 'us-west-2'),
	'aws_access_key_id': environ.get('AWS_ACCESS_KEY_ID', 'localhost-aws-access-key'),
	'aws_secret_access_key': environ.get('AWS_SECRET_ACCESS_KEY', 'localhost-aws-secret-key'),
}

ENDPOINT_TABLE = 'Erad_Endpoint_Global'


class InputArgs:
	def __init__(self):
		self.pause_between_tests = 5
		self.conf_update_timeout = 5
		self.test_response_timeout = 10
		self.test_retries = 1
		self.path_to_dictionary = ''


def get_dyndb_connections(region: str = None):
	connection_config = base_config.copy()
	if region is not None:
		connection_config['region_name'] = region
	dyndb_connection = boto3.resource('dynamodb', **connection_config)
	dynamodb_client = boto3.client('dynamodb', **connection_config)
	return dyndb_connection, dynamodb_client


@pytest.fixture(scope='module')
def create_test_endpoints():
	dynamodb, dynamodb_client = get_dyndb_connections()
	try:
		dynamodb_client.delete_table(
			TableName=ENDPOINT_TABLE
		)
		waiter = dynamodb_client.get_waiter('table_not_exists')
		waiter.wait(TableName=ENDPOINT_TABLE)
	except Exception as err:
		print(err)

	dynamodb_client.create_table(
		AttributeDefinitions=[
			{
				'AttributeName': 'IpAddress',
				'AttributeType': 'S'
			},
			{
				'AttributeName': 'Port',
				'AttributeType': 'S'
			},
		],
		TableName=ENDPOINT_TABLE,
		KeySchema=[
			{
				'AttributeName': 'IpAddress',
				'KeyType': 'HASH'
			},
			{
				'AttributeName': 'Port',
				'KeyType': 'RANGE'
			},
		],
		ProvisionedThroughput={
			'ReadCapacityUnits': 1,
			'WriteCapacityUnits': 1
		}
	)
	waiter = dynamodb_client.get_waiter('table_exists')
	waiter.wait(TableName=ENDPOINT_TABLE)

	table = dynamodb.Table(ENDPOINT_TABLE)

	test_items = [
		{
			'IpAddress': '127.0.0.1',
			'Port': '1812',
			'Group_ID': 'aa::bb',
			'ClusterId': 'c1',
			'Secret': 'whatever',
			'Region': 'us-west-2'
		},
		{
			'IpAddress': '127.0.0.1',
			'Port': '1813',
			'Group_ID': 'aa::bb',
			'ClusterId': 'c1',
			'Secret': 'whatever',
			'Region': 'us-west-2'
		},
		{
			'IpAddress': '127.0.0.1',
			'Port': '1814',
			'Group_ID': '::dirty',
			'ClusterId': 'c1',
			'Secret': 'whatever',
			'Region': 'us-west-2'
		},
	]

	for item in test_items:
		table.put_item(Item=item)


@pytest.mark.usefixtures('create_test_endpoints')
def test_a():
	endpoint_tester = EndpointTester(InputArgs())
	result = endpoint_tester.load_endpoints()

	expected = [
		Endpoint(cluster_id='c1', ip='127.0.0.1', port='1812', secret='whatever'),
		Endpoint(cluster_id='c1', ip='127.0.0.1', port='1813', secret='whatever')
	]

	for item in expected:
		assert item in result
