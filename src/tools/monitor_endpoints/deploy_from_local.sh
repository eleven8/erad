#!/bin/sh
set -e
set -x
deploy_bucket=$1
sec_group_name=$2
ssh_key_name=$3

AMLinux2_version="amzn2-ami-hvm-2.0.20200917.0-x86_64-gp2"

AMIID=$(aws ec2 describe-images --output text --owners amazon --filters "Name=name,Values=${AMLinux2_version}" --query 'Images[*].ImageId | [0]')

sec_group_id=$(aws ec2 describe-security-groups --query 'SecurityGroups[?GroupName==`'"$sec_group_name"'`].GroupId|[0]' --output text)

tmp_dir=$(mktemp -d)

tar \
    --exclude=".pytest_cache*" \
    --exclude="__pycache__" \
    --exclude=".idea*" \
    --exclude="monitor_endpoints.tgz" \
    -czvf ${tmp_dir}/monitor_endpoints.tgz .

mv ${tmp_dir}/monitor_endpoints.tgz .

aws s3 cp monitor_endpoints.tgz "s3://${deploy_bucket}/"
rm monitor_endpoints.tgz


aws cloudformation deploy \
  --capabilities CAPABILITY_NAMED_IAM \
  --region us-west-2 \
  --stack-name monitor-endpoints \
  --template-file  $(pwd)/standard-deploy.yaml \
  --parameter-overrides  \
    AMIID=${AMIID} \
    DeployBucket="${deploy_bucket}"\
    DeployFile="monitor_endpoints.tgz"\
    SshKeyName="${ssh_key_name}" \
    "SecurityGroupIds=${sec_group_id}" \
