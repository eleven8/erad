# coding: utf-8

import boto3
import logging
from os import environ

logger = logging.getLogger()
logger.setLevel(logging.INFO)

TARGET_EVENT_SOURCE = 'aws:dynamodb'


class StreamWriter:
	def __init__(self):
		# In production lambda function should use IAM role to access to DynamoDB
		if environ.get('AWS_PRODUCTION_ENV') == 'True':
			base_config = {}
		else:
			base_config = {
				'endpoint_url': environ.get('AWS_DYNAMODB_ENDPOINT', 'http://localhost:8091'),
				'region_name': environ.get('AWS_DEFAULT_REGION', 'us-west-2'),
				'aws_access_key_id': environ.get('AWS_ACCESS_KEY_ID', 'localhost-aws-access-key'),
				'aws_secret_access_key': environ.get('AWS_SECRET_ACCESS_KEY', 'localhost-aws-secret-key'),
			}

		self.dyndb_client = boto3.client('dynamodb', **base_config)
		self.target_table_name = environ.get('TARGET_TABLE_NAME', None)
		if self.target_table_name is None or len(self.target_table_name) == 0:
			raise Exception('Config Error: No target table defined')

		self.warning_event_types = {
			'REMOVE',
			'MODIFY'
		}

	def handle(self, event, context):
		for record in event['Records']:

			if record['eventSource'] != TARGET_EVENT_SOURCE:
				return 0

			if record['eventName'] in self.warning_event_types:
				logger.warning('The stream contain {0} event.'.format(record['eventName']))

			new_image = record['dynamodb'].get('NewImage', None)

			if new_image is None:
				logger.warning('The event does not have NewImage')
				continue
			put_item_response = self.dyndb_client.put_item(
				TableName=self.target_table_name,
				Item=new_image
			)


__stream_writer = StreamWriter()


def lambda_handler(event, context):
	__stream_writer.handle(event, context)
