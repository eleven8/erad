### Purpose

This lambda function take event from stream and write into destination table, it is good enough only for "Historical" tables.
Historical tables - The records are never updated or deleted (only written to once).

```
Erad_Radius_Log
Erad_AccountingLogs
Erad_Audit
Erad_SupplicantCertificate
```

### Notes
Lambda read records from stream with batch
If any error will happens, it will retry process the whole batch
It is possible to pause lambda execution by setting function concurrency to 0


### Local testing
Use `create_target_table.py` to create test table locally.
Use `debug_local.py` as main script, it will import `lambda_handler()` function from `stream_writer` module and execute it with samples of events `dynamodb.events`.

Also function can be tested with  [AWS SAM](https://docs.aws.amazon.com/lambda/latest/dg/test-sam-cli.html)

Use this command to test function with AWS SAM, this commands require installed Docker.
```bash
sam local invoke \
    --docker-network dynglobalmig_default \
    -e dynamodb.events \
    -n local-test-envs.json \
    --skip-pull-image
    StreamWriter
```
`--docker-network` - will attach AWS SAM to specified docker network, this is need if localhosted DynamoDB lunched inside container. Also in `local-test-envs.json` defined variable that point to localhosted DynamoDB endpoint.


### Deploy
**Note, the commands assume, its will be executed from directory where jsons file located. Change path to json files if it required.**

**Also do not forget to change names of created resource**

If appropriate role already exists skip creating policy and role, and get ARN of existing role
```bash
aws iam get-role\
    --role-name <value>
```

####1. Create policy
AWS Provide Standard policy to get access to DynamoDB streams and CloudWatch logs:

`AWSLambdaDynamoDBExecutionRole`

ARN: `arn:aws:iam::aws:policy/service-role/AWSLambdaDynamoDBExecutionRole`

StreamWriter require additional policy to write data in DynamoDB table. Use this command to create it, if this policy is not created earlier:

```bash
aws iam create-policy \
    --policy-name <value> \
    --policy-document file://$(pwd)/allow-lambda-write.policy.json
```

####2. Create role for lambda function, and attach required policy
Use this command to create new role

```bash
aws iam create-role \
    --role-name <value> \
    --assume-role-policy-document file://$(pwd)/lambda-stream-writer.role.json
```

And these commands to:

1. Attach AWS managed policy `AWSLambdaDynamoDBExecutionRole`, ARN provided above
```bash
aws iam attach-role-policy \
    --role-name <value>\
    --policy-arn "arn:aws:iam::aws:policy/service-role/AWSLambdaDynamoDBExecutionRole"
```
2. New created policy, take ARN from `create-policy` command output
```bash
aws iam attach-role-policy \
    --role-name <value>\
    --policy-arn <value>
```

####3. Query Dynamodb to get ARN of source table stream

```bash
aws dynamodbstreams list-streams \
    --table-name <value>
```

####4. Create SAM package
This command will pack folder, that referenced in variable `CodeUri` in template.yaml file, and upload result to s3 bucket.
In output template file `CodeUri` variable will be replaced with `s3 urlpath` of uploaded package. Use this template to deploy lambda function.
```bash
sam package \
   --template-file $(pwd)/template.yaml \
   --output-template-file $(pwd)/serverless-output.yaml \
   --s3-bucket <value>
```

####5. Deploy lambda function
```bash
sam deploy \
   --template-file $(pwd)/serverless-output.yaml \
   --stack-name <value> \
   --parameter-overrides \
        AssignedRoleArn="<value>" \
        SourceTableStreamArn="<value>" \
        TargetTableName=<value> \
        AwsProductionEnv="True"

```
