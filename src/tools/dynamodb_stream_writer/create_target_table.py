# coding: utf-8

import boto3
import argparse
from os import environ


base_config = {
	'endpoint_url': None if environ.get('AWS_PRODUCTION_ENV') == 'True' else environ.get('AWS_DYNAMODB_ENDPOINT', 'http://localhost:8091'),
	'region_name': environ.get('AWS_DEFAULT_REGION', 'us-west-2'),
	'aws_access_key_id': environ.get('AWS_ACCESS_KEY_ID', 'localhost-aws-access-key'),
	'aws_secret_access_key': environ.get('AWS_SECRET_ACCESS_KEY', 'localhost-aws-secret-key'),
}

dyndb_client = boto3.client('dynamodb', **base_config)


def create_target_table(table_name):
	response = dyndb_client.create_table(
		AttributeDefinitions=[
			{
				"AttributeType": "N",
				"AttributeName": "Id"
			},
		],
		TableName=table_name,
		KeySchema=[
			{
				"KeyType": "HASH",
				"AttributeName": "Id"
			}
		],
		ProvisionedThroughput={
			'ReadCapacityUnits': 1,
			'WriteCapacityUnits': 1
		}
	)

	dyndb_client.get_waiter('table_exists').wait(TableName=table_name)
	response = dyndb_client.describe_table(TableName=table_name)

	if response['Table']['TableStatus'] != 'ACTIVE':
		raise Exception('Table {0} is not created'.format(table_name))
	print('Table {0} created'.format(table_name))


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Create test target table for lambda function')
	parser.add_argument(
		'table_name',
		metavar='<table_name:str>',
		type=str,
		help='The name of target table')

	args = parser.parse_args()

	create_target_table(args.table_name)


