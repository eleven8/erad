# coding: utf-8

from src.stream_writer import lambda_handler
from os import path
import json

dir_name = path.dirname(path.realpath(__file__))
sample_events = path.join(dir_name, 'dynamodb.events')

with open(sample_events, 'rb') as f:
	json_data = json.loads(f.read())
	lambda_handler(json_data, "")
