#!/bin/bash
set -e
set -x

# The purpose of this script is to simplify syncing freeradius configuration that is used for ERAD with
# original/default configuration.
#
# Can create configuration for freeradius servers and freeradius load balancer
#
# 1. from `src` directory run next command
# `bash create_erad_freeradius_raddb.sh [server|lb] apply_patch `
# This command copy default freeradius configuration and apply (apply patch) current ERAD configuration onto default.
# Also it will print path to temporary directory with result configuration. Use this configuration for testing/editing.
#
# 2. When configuration will be ready create new patch using this command
#
# - To create new configuration for freeradius servers
# `git diff --cached > <path to erad>/src/erad_freeradius_cfg/erad-freeradius-config.patch`
#
# - To create new configuration for freeradius load balancer
# `git diff --cached > <path to erad>/src/erad_freeradius_lb_cfg/erad-freeradius-config.patch`
#
# Commit new patch version.
#
# 3. Create new version of freeradius configuration in  `src/erad/frmod/raddb` (or in `src//freeradius-lb/raddb` in loadbalancer case) using next command
# `bash create_erad_freeradius_raddb.sh [server|lb]`
# And commit new version of `raddb` directory.
#
# You also can use next command to get default freeradius configuration
# `bash create_erad_freeradius_raddb.sh [server|lb] base`


target=${1}
operation=${2:-default}

if [[ -z "$target" ]]; then
	echo "Usage create_erad_freeradius_raddb [server|lb] <operation>"
fi

if [[ "$target" == 'server' ]]; then
	ERAD_FREERADIUS_CFG_DIR="$(pwd)/erad_freeradius_cfg"
	TARGET_RADDB_DIR="$(pwd)/erad/frmod/raddb"
elif  [[ "$target" == 'lb' ]]; then
	ERAD_FREERADIUS_CFG_DIR="$(pwd)/erad_freeradius_lb_cfg"
	TARGET_RADDB_DIR="$(pwd)/freeradius-lb/raddb"
fi


stat "${ERAD_FREERADIUS_CFG_DIR}"

docker build -t latest_freeradius -f df.radius-docker.Dockerfile .
cnt_id=$(docker run -d --rm --entrypoint=/usr/bin/tail latest_freeradius -f /var/log/yum.log)
tmp_dir=$(mktemp -d)
RADDB_DIR="${tmp_dir}/raddb"


docker cp "${cnt_id}:/usr/local/etc/raddb" "${tmp_dir}/"
docker stop "${cnt_id}"

if [[ "$operation" == "base" ]]; then
  echo "$RADDB_DIR"
  exit 0
fi

# applying erad config

# removing certificates that was generated during installation, except *.cnf
# proably these files also can be deleted
find "${RADDB_DIR}/certs" ! -name '*.cnf' -type f -exec rm -fv {} +

pushd "${RADDB_DIR}"
git init .
git add .
git commit -am 'initial'
cp "${ERAD_FREERADIUS_CFG_DIR}/erad-freeradius-config.patch" "${RADDB_DIR}/"
git apply --verbose erad-freeradius-config.patch
rm erad-freeradius-config.patch
git add .
popd

if [[ "$operation" == "apply_patch" ]]; then
  echo "$RADDB_DIR"
  exit 0
fi

# cp /* with empty directory will throw error
if [[ ! -z "$(ls -A "${ERAD_FREERADIUS_CFG_DIR}"/radius-certs/)" ]]; then
   cp -rv "${ERAD_FREERADIUS_CFG_DIR}"/radius-certs/* "${RADDB_DIR}/certs/"
fi

# removing temporary git repo
rm -rf "${RADDB_DIR}/.git"

rm -rf "${TARGET_RADDB_DIR}"
cp -r "${RADDB_DIR}" "${TARGET_RADDB_DIR}"

rm -rf "$tmp_dir"
