
### CODE RED
1. Open `https://status.enterpriseauth.com/`
2. Starting from top to bottom, address the first red section. Use the following table to determine the action to take.

| Section Name | Action to Take | Description of Test | Notes |
| --- | --- | --- | --- |
| HTTP | [Redeploy the API](#how-to-redeploy-the-api) for the affected region. | This tests the individual API servers for basic response--no database access. | A single server should not cause a code red alert because the other servers would keep the service going. Multiple servers being down is concerning because it indicates a defect that might not be cured by a redeploy. |
| DB | Check for Amazon outage. | This tests the individual API servers for a response that requires communication with DynamoDB. | Since a basic API response is occurring, it is likely a service outside our stack that is in trouble. This has happened before, for example, when DynamoDB was offline for an hour. |
| API | Check for Amazon outage.  | Tests http and https. | Since the individual underlying API servers are working, there is likely something wrong with Elastic Load Balancer or DNS. |
| RADIUS | [Redeploy the cluster(s)](#how-to-redeploy-individual-radius-clusters) with failures. | Tests response of the individual RADIUS servers. | Now that we know the API is up, any RADIUS errors are isolated to the RADIUS servers. A single server should not cause a code red alert because the other servers would keep the service up. Multiple servers being down is concerning because it indicates a defect that might not be cured by a redeploy. |
| LB| [Run redeploy job](#how-to-run-the-radius-region-redeploy-job) for the affected region. Possibly manual failover. | Test response from the primary and secondary load balancers in all regions. | The basic step that will usually solve things is to run the redeploy job for the affected region(s). This will take 15 minutes, if it succeeds. A more advanced method follows, which could reduce downtime. The static IP addresses are 34.192.196.106 (us-east-1), 3.17.122.53 (us-east-2), 52.11.20.214 (us-west-2). If one of these IPs is assigned to a server marked as red but the other in the region is green, manually reassign the Elastic IP to the green server (and start the redeploy job to straighten things out). |
| All Green | Wait or deploy new monitoring server. |  | CloudWatch must receive regular updates from our monitoring server or it triggers the Code Red alarm. If the monitoring server dies, the alarm from CloudWatch will be a false positive. Since the service is operational, simply allow time for John, Vladimir, or Piotr to come online and deploy a new monitoring server. |

### Systems
**Jenkins**
URL: https://ci.gd1.io
Individuals with access: John, Vladimir, Piotr, Matt, Lee, Sorawis

**AWS**
The ERAD services are deployed in the Team42 production AWS account.

### How to Redeploy the API
First, prepare the system for a deployment to production by running the `__erad-production-GO-LIVE-switch__` Jenkins job. The status indicator for this job must be green for a job to actually deploy to production.

Now run the `erad-web-stage` Jenkins job with the following parameters.
* **ApiVersionNumber**: Always (always) enter `Deployed`. The default of `Latest` would deploy the branch that was most recently tested on Jenkins.
* **TARGET_REGION**: Choose the value that matches the region that the status page indicated was down.

If the job fails, the `erad-web-stage-tear-down` Jenkins job must be run before trying to perform the deployment job again.

### How to Redeploy Individual RADIUS Clusters
First, prepare the system for a deployment to production by running the `__erad-production-GO-LIVE-switch__` Jenkins job. The status indicator for this job must be green for a job to actually deploy to production.

Now run the `erad-frmod-stage` Jenkins job with the following parameters.
* **RadiusVersionNumber**: Always (always) enter `Deployed`. The default of `Latest` would deploy the branch that was most recently tested on Jenkins.
* **CLUSTER_ID**: Enter the cluster(s) that the status page shows as down. Separate cluster IDs with commas. A cluster ID looks like `uswest-01-0001`.

If the job fails, the `erad-frmod-stage-tear-down` Jenkins job must be run before trying to perform the deployment job again.

### How to Run the RADIUS Region Redeploy Job
Simply run one of the following Jenkins jobs, based on the region that is failing:
* \_\_erad-REDEPLOY-production-radius-useast-01-0001\_\_
* \_\_erad-REDEPLOY-production-radius-useast-02-0001\_\_
* \_\_erad-REDEPLOY-production-radius-uswest-01-0001\_\_

These jobs will automatically trigger the `GO-LIVE` switch.
The job redeploys the first cluster (0001) in the region and causes the load balancers to be re-created. This is a convenient shortcut to quickly run the deployment when it is a load balancer causing issues.

### How to setup local dev environment for manual testing and debug ERAD API + UI

1. Run redis, locally or in docker container

```
docker run --rm --name local-redis -p 6379:6379 -d redis
```

2. Run dynamodb, locally or in docker container. There is docker-compose file, that can be used for this
`erad/testing/docker-compose.yaml`.
```
docker-compose up -d dynamodb-localhost
```
Note the dynamodb-localhost, uses RAM to store data. All data will be lost if service restart. If data need to be saved between service restart. Read `erad/src/dynamodb-localhost/dynamodb-local.Dockerfile` to understand how it works. And edit `erad/testing/docker-compose.yaml`, to change service command line arguments.

Do not forget to expose `8000` port, uncomment, if necessary, this part in docker-compose file.

3. Read `erad_local_setup.txt` file, particular `Configure Python` and `Configure BOTO` parts. Or configure python virtual env and install requirements (`erad/src/erad-webservice-requirements.txt`). And read [this](http://boto.cloudhackers.com/en/latest/boto_config_tut.html) to configure boto.

For Apple computers on ARM use these env variables to install cryptography package

export LDFLAGS="-L/opt/homebrew/opt/openssl@3/lib"
export CPPFLAGS="-I/opt/homebrew/opt/openssl@3/include"


4. Read `erad_frontend_setup.txt` to understand how to run UI part locally. Instruction in that file will install `grunt`, but currently JavaScript code is being being migrating to Webpack. Because of that `grunt` can no longer build erad_static project and used only for testing. In the future `grunt` will be removed completely

Two notes:
1. Current js code and libraries are intended to use with not latest NodeJS version. Amazon Linux 2 uses NodeJS 6.14.3 (2019-01-08). More contemporary versions of NodeJS have some incompatibles with current code. This will be fixed latter.
2. This will only run local http server, that can provide static files: html, js, css etc. If all required nodejs/grunt/bower modules/libraries installed, go to `erad/src/erad_static/` and run `npm run dev-server`. This command will print http url of started server.


5. Read source code of function `erad_cfg()` from `erad/src/erad/util.py`, to understand how to configure ERAD API parameters and configure it.
The default parameters should be ok. ERAD API will be working on `http://0.0.0.0:5000` and Dynamodb endpoints configured in boto config file. See 3rd point of this instruction.

6. Use `erad/src/create_database.py` script to create test/dev database.

7. Use `erad/src/spawn_api_server.py` script to run ERAD API locally. It should start http server on `http://localhost:5000` address.
Usually in dev mode it is useful to see tracebacks. Set environment variable `export ERAD_DEBUG=True` to see tracebacks in API http response, if unexpected exception will happen.

8. Run
```
docker-compose up -d erad-start-ui
```
The definition of this service also in `erad/teststing/docker-compose.yaml` file. This service will run http server on `http://localhost:8888` address. This UI can be used to bootstrap database with necessary data, to make ERAD UI work.
In dropdown menu choose `Localhost` input group/groups name/names and password, and submit form. The page should redirect to page with working ERAD UI.

Note, `erad-start-ui` configured to expect ERAD API on this address `http://localhost:5000`. If ERAD API run on other address change this configuration file `erad/src/tools/dashboard/UI/config.json`, rebuild `erad-start-ui` Docker image and restart `erad-start-ui` service.



### Run tests with local dev environment

#### ERAD API tests

1. Run dynamodb

Use `docker-compose up -d dynamodb-localhost` command and `erad/testing/docker-compose.yaml` file.

2. Run redis

`docker run --rm --name local-redis -p 6379:6379 -d redis`

3. Run ERAD API

Use `erad/src/spawn_api_server.py` script to run ERAD API locally.

4. Start test
Use this command

```
python remove_database.py && \
python create_database.py && \
python -m nose -s ./erad/api/test/
```
This command mostly repeats bash script `erad/src/api_unit_testing.sh`

Tests command can be modified to generate swagger api yaml file.

```
python remove_database.py && \
python create_database.py && \
python -m nose -s \
	./erad/api/test/ \
	./erad/api/tests_teardown.py
```

The swagger api yaml file will be printed to stdout


#### ERAD UI tests

1. Read `erad_frontend_setup.txt` to understand how to run UI part locally.
2. `erad_static_tests` expect that host part of url will be equal `https://testing.example.org`. To do that edit file `erad/src/erad_static/app/scripts/service.js` search string `{%ERAD_SERVER_NAME%}` in function `getApiDomain`. Change value of `v` variables

**!Do not forget remove this line before commit and push to the repository!**
```
v = 'https://testing.example.org';
```
3. Navigate to `erad/src/erad_static/`
4. Run `grunt test`
5. Also tests can be run in browser, as it mentioned in `erad_frontend_setup.txt` file.
Note, Google Chrome (by default) not support cookies on `file://` - some tests will fail. Use other browser to test it.

6. Added another way to run test using headless Chromimum. From `erad/src/erad_static/` run `npm run test`. This command will produce `xunit.xml` file.


#### frmod tests

1. Run dynamodb

	Use `docker-compose up -d dynamodb-localhost` command and `erad/testing/docker-compose.yaml` file.

2. Run redis

	`docker run --rm --name local-redis -p 6379:6379 -d redis`

3. Run ERAD API

	Use `erad/src/spawn_api_server.py` script to run ERAD API locally.

4. Start test
	Use this command

	```
	python remove_database.py && \
	python create_database.py && \
	python -m unittest \
		erad.frmod.test.test_module \
		erad.frmod.test.test_loadeap \
		erad.frmod.test.test_loadvs
	```


### Run tests with Docker

#### ERAD API

1. Go to `erad/testing`
2. Run `./run-test-api-only.sh`

Note if tests runs on "clean new instance" read comments in `./run-test-api-only.sh` file


#### ERAD UI tests

1. Go to `erad/testing`
2. Run `./run-tests-erad-static.sh`


#### frmod tests

Frmod tests includes testing radius connection and authorization. To be able do this tests, `run-tests.sh` script will setup full stack of `ERAD API`, `ERAD UI` and 4 radius server instance.

0. Tests uses pyopenssl and nosetest library - so make virtual environment, or install these libs globally.
1. Go to `erad/testing`.
2. Clean tests directory from previous tests - run
	`./clean-after-tests.sh`.
3. Run
	`./prepare-frot-tests.sh`.
	This will create config files for radius servers.
4. Run tests
	`./run-tests.sh`

You can also run tests by one, use something like this

```
python -m unittest frmod_tests.test_radius_server.TestEapol.test_eap_tls
```

But to do that, all docker containers should be running. See `./run-tests.sh` file for how to run containers. Or comment the last two lines

```
docker-compose down
docker image prune -f
```

in file `./run-tests.sh`, so containers will not be deleted after all tests.


#### Simple EAP stress testing

At this moment (2020-01-12) there is not exists general available tool that can be used to do stress testing with Radius EAP requests. `eapol_test` tool allow test one authentication/radius requests. But does not allow configure how many authentication do or with which frequency send auth requests. To do that it is possible to use `parallel` tool. Here simple example command line that will do next:

`parallel  --joblog ./joblog --eta --halt soon,fail=5% --timeout 5 -j12 :::: ./parallel-data`

`./parallel-data` - file with commands that will be run with parallel. Note that radius server may handle short peak of many requests, so fill this file with many records to load radius server for significant time.
`-j12` - number of workers that will be run in parallel. `parallel` will read first 12 lines from `./parrallel-data` file and execute them simultaneously.
`--halt soon,fail=5%` - sometimes radius request may fail, this parameter control how many failures are acceptable, and stop test when limit will be reached. Failure - means non zero return from command.
`--joblog ./joblog` - will write statistic to csv file `./joblog`. Later after tests this file can be imported in Excel to calculate statistic.

For other parameters see man page for `parallel`

To generate `./parallel-data` use script `get_eap_parallel_data.py`

Use `python ./gen_eap_parallel_data.py -h` to see available parameters

`parallel` tool is installed in `eapol-test` docker container (testing/test-eapol/Dockerfile)

So to run tests do this

```
pushd testing
docker-compose build eapol-test

# Change parameters to desired values
python -m gen_eap_parallel_data ./test-eapol/eapol_test_configs/parallel-data 1000 172.25.0.8
docker-compose up -d eapol-test
docker-compose exec eapol-test /bin/sh
parallel  --joblog ./joblog --eta --halt soon,fail=5% --timeout 5 -j12 :::: ./parallel-data

# read resulst
less ./joblog
```

If one computer can't produce enough radius request to fully load radius server approach with `parallel` can be updated. It support distributed execution - parallel uses ssh connection and run workers on many servers.


#### Freeradius Loadbalancer

aws-freeradius-lb.docker-compose.yaml - to use on production
build-freeradius-lb.docker-compose.yaml - to build docker images
freeradius-lb.docker-compose.yaml - for local testing

Right now there is no CI/CD that build docker images for Freeradius Loadbalancer.

If new version needed do next

1. Change configuration. More explanation how to do it will be describe in another section.
2. Build docker images:

- Set up AWS credentials for production AWS account.
This is required for two reasons. Docker images will have to has AWS account id in it's name. And we need access to push docker image to ECR.

- Build docker images
Use this command
`bash build-radius-lb-image.sh aws <aws ECR region>

This command will print output like this

``
Successfully built 57589961ff4e
Successfully tagged <here will be real aws account id>.dkr.ecr.us-west-2.amazonaws.com/radius-lb:33e76a2e5db626710b63a73325e6e05f39e837c5
...
Successfully built fd230979cc09
Successfully tagged <here will be real aws account id>.dkr.ecr.us-west-2.amazonaws.com/radius-lb-conf-gen:33e76a2e5db626710b63a73325e6e05f39e837c5
```

- Assign stable tag for new images.
First be sure that images are stable - test it first
Use docker and assign stable tag, example

```
docker tag <here will be real aws account id>.dkr.ecr.us-west-2.amazonaws.com/radius-lb:78f861c5566513de9e43aadda637d01b3d4af865 <here will be real aws account id>.dkr.ecr.us-west-2.amazonaws.com/radius-lb:stable
```

- Authorize docker in ECR

Note that example uses aws-cli version 2+.

```
export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query "Account" --output text)
aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin "${AWS_ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com"
```

- Push image that tagged stable to ECR

```
docker push <here will be real aws account id>.dkr.ecr.us-west-2.amazonaws.com/radius-lb:stable
```

After image successfully pushed load balancers can be redeployed. `aws-freeradius-lb.docker-compose.yaml` configured to always use image with `stable` tag so no need to change parameters in load balancer deploy scripts.

### Certificate requirements for Radius servers

Windows 10 does not respect the Subject Alternative Name in the certificate during EAP connection and checks only CommonName CN filed of the certificate presented by radius server. Other OSes seems working fine with SAN.
The theory that Windows 10 does not like EV cert was wrong.
