'use strict';

var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    jscs = require('gulp-jscs'),
    stylish = require('gulp-jscs-stylish'),
    plumber = require('gulp-plumber');

gulp.task('lint', function () {
    return gulp.src('*.js')
        .pipe(plumber())
        .pipe(jshint())
        .pipe(jscs({
            verbose: true
        }))
        .pipe(stylish.combineWithHintResults())   // combine with jshint results
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('watch', function () {
    gulp.watch('*.js', ['lint']);
});

gulp.task('default', ['lint', 'watch']);
