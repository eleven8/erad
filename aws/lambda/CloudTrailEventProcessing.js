'use strict';
// http://docs.aws.amazon.com/lambda/latest/dg/wt-cloudtrail-create-function-create-function.html

var aws  = require('aws-sdk');
var zlib = require('zlib');
var async = require('async');
var _ = require('lodash');

//var EVENT_SOURCE_TO_TRACK = /sns.amazonaws.com/;
//var EVENT_NAME_TO_TRACK   = /CreateTopic/;
var DEFAULT_SNS_REGION  = 'us-west-2';
var SNS_TOPIC_ARN       = 'arn:aws:sns:us-west-2:439838887486:CloudTrail';

var THRESHOLD = 1000*60*15; // 15 minutes

var s3 = new aws.S3();
var sns = new aws.SNS({
	apiVersion: '2010-03-31',
	region: DEFAULT_SNS_REGION
});

var srcBucket, srcKey;

function sorted(a, b) {
	// current sort is reverse alphabetical 
	if (a > b) {
		return -1;
	}
	if (a < b) {
		return 1;
	}
	return 0;
}

function fetchLogFromS3(next){
	console.log('Fetching compressed log from S3...');
	s3.getObject({
	   Bucket: srcBucket,
	   Key: srcKey
	},
	next);
}

function uncompressLog(response, next){
	console.log('Uncompressing log...');
	zlib.gunzip(response.Body, next);
}

function filterUsers(jsonBuffer, next) {
	console.log('Filtering log...');
	var json = jsonBuffer.toString();

	var records;
	try {
		records = JSON.parse(json);
	} catch (err) {
		next('Unable to parse CloudTrail JSON: ' + err);
		return;
	}

	var matchingRecords = records
		.Records
		.filter(function(record) {
			return record.userIdentity.userName !== 'jsteele' &&
			record.userIdentity.userName !== 'root' &&
			record.userIdentity.userName !== 'Production' &&
			record.userIdentity.userName !== 'Automation' &&
			record.userIdentity.userName !== 'teamfortytwo' &&
			record.userIdentity.userName !== 'Jenkins' &&
			record.userIdentity.userName !== '' &&
			record.userIdentity.userName !== null;
		});

	var keyedObjectStore = {};

	matchingRecords.forEach(function (userEvent) {
		var userKey =  userEvent.userIdentity.userName;				
		if ( !keyedObjectStore[userKey] ) {
			keyedObjectStore[userKey] = [];			
		}
		keyedObjectStore[userKey].push({
			name: userKey,
			time: Date.parse(userEvent.eventTime),
			eventName: userEvent.eventName,
			event: userEvent.eventSource + '; ' +
				userEvent.eventName + '; ' +
				userEvent.awsRegion
		});
	});

	next(null, keyedObjectStore);
}

function sendEmails (eventsToEmail, next) {

	var numOfEmails = Object.keys(eventsToEmail).length;

	if (numOfEmails) {
		console.log('Sending notifications for ' + numOfEmails + ' user(s)');
		async.each(
			Object.keys(eventsToEmail),
			function doSNSPublish (record, publishComplete) {
				var messages = _.pluck(eventsToEmail[record], 'event').sort(sorted);
				sns.publish({
					Subject: 'AWS Activity for ' + record,
					Message:
						'Actions By: ' + record + '\r\n\r\n' +
						messages.join('\r\n'),
						TopicArn: SNS_TOPIC_ARN
				}, publishComplete );
			},
			next
		);

	} else {
		console.log('No emails to send');
	}
	next();
}

// Candidate for moving to external 'utils.js'
// object. Currently just used for debuggin/reporting
function countObjectKeys(keyedObject) {
	// ie: {
	//	  'user': []
	// 	}
	var counter = 0;
	_.forEach(_.keys(keyedObject), function (key) {
		counter += keyedObject[key].length;
	});
	return Number(counter);
}

function checkRecentEventLog(newEventsArray, next) {

	var toSendQ = {};
	var toSaveQ;

	function createObectKeyIfMissing(queueType, user) {			
		if ( !queueType[user] ) {
			queueType[user] = [];			
		}
	}

	var s3params = {
	   Bucket: srcBucket,
	   Key: 'lambdaCache2.log'
	};

	console.log('Fetching lambdaCache log file');

	s3.getObject({ 
		Bucket: srcBucket, 
		Key: s3params.Key, 
		ResponseContentType: 'application/json'	
	}, function getLogfileCallback(err, data) {

		if (err) {
			next(err, null);
		}

		// counters for logging
		var logfileEventCount = 0;
		var newEventsLogCount = 0;
		var toSendQCounter = 0;
		var toSaveQCounter = 0;

		var fileData = JSON.parse(data.Body.toString());

		// add old events to queue as we will remove what we don't want later
		toSaveQ = fileData;
		
		// array of users with events in the logfile/cache 
		var usersWithOldEvents = _.keys(fileData);
		var usersWithNewEvents = _.keys(newEventsArray);

		var logfileLength = usersWithOldEvents.length;

		// candidate for re-write as it no-longer needs to return all of the 
		// users, just the one we require/pass in to the function
		// a simple function to get the lastest event for the passed in user
		function lookupTable(username) {

			var table = _.groupBy(_.map(usersWithOldEvents, function (item) { 
				return _.last(_.sortBy(toSaveQ[item], 'time'));
			}), 'name');

			if (table.hasOwnProperty(username)) {
				return table[username][0];
			} else {
				return false;			
			}
		}

		function pushToSaveOrSendQ(err, result, event) {
			// result is true only if we want to send it as an event
			if (result) {
				console.log('Sending event to sendQ for ' + event.name + ' :: ' + event.debug.msg);

				createObectKeyIfMissing(toSendQ, event.name);
				toSendQ[event.name].push(event);

				// if user has stored events, collect them
				if (toSaveQ.hasOwnProperty(event.name)) {
					
					var localCounter = 0;
					
					_.forEach(toSaveQ[event.name], function (item) {
						console.log(item);
						toSendQ[event.name].push(item);
						localCounter++;
					});

					console.log(localCounter + ' old events by ' + event.name + ' sent to sendQ');
					delete toSaveQ[event.name];
				}

				toSendQCounter++;

			} else {
				createObectKeyIfMissing(toSaveQ, event.name);
				toSaveQ[event.name].push(event);
				toSaveQCounter++;
			}
		}

		function evaluateEvent(event, callback) {

			console.log(event.name + ' :: ' + new Date(event.time).toUTCString());

			if ( lookupTable(event.name) ) {
				var searchResult = lookupTable(event.name);

				var lastEventInLogTime = searchResult.time;
				var lastEventInLogType = searchResult.eventName;
				var latestEventFromLambdaType = event.eventName;

				var diff = (event.time - lastEventInLogTime);
				var diffFromNow = (Date.now() - event.time);
				var DAY = 1000*60*60*24; // one day

				if (latestEventFromLambdaType === lastEventInLogType) {

					var YESTERDAYS = diffFromNow > DAY;

					if (YESTERDAYS) {
						event.debug = {
							msg: 'Last logs match but aged event, sending all since yesterday',
							data: (Date.now - event.time)/1000 + ' seconds old'
						};
						return callback(null, true, event);						

					} else {
						event.debug = {
							msg: 'Last log matches current event type, saving',
							data: latestEventFromLambdaType + '===' + lastEventInLogType
						};
						return callback(null, false, event);
					}

				} else if (diff < THRESHOLD) {
					// new event was less than 15 minutes ago
					// store in file 
					event.debug = {
						msg: 'Last event was less than 15 minutes ago, saving',
						data: 'Last event: ' + lastEventInLogTime + '\n' +
							'Seconds since last event: ' + diff / 1000
					};
					return callback(null, false, event);

				} else {
					// new event is of different type and/or 
					// more than 15 minutes since last event
					event.debug = {
						msg: 'Time since last event is greater than 15 mins or of new event type',
						data: ''
					};
					return callback(null, true, event);
				}

			} else {
				event.debug = {
					msg: 'Nothing in log',
					data: ''
				};

				console.log('No events logged found for ' + event.name + ', saving');
				return callback(null, false, event);
			}

		}

		function consoleLogLoop(key) {
			var thisUserEventCount = fileData[key].length;
			logfileEventCount += thisUserEventCount;
			
			console.log(thisUserEventCount + ' events found in logfile' + ' for ' + key);
		}
		
		function newEventsLoop(userFromEvent) {

			var len = newEventsArray[userFromEvent].length;
			var pluralised = len > 1 ? 'events' : 'event';
			var thisUserEvents = _.sortBy(newEventsArray[userFromEvent], 'time');

			console.log('Got ' + len + ' new ' + pluralised + ' for ' + userFromEvent);

			if (len > 1) {
				_.forEach(thisUserEvents, function (event) {
					evaluateEvent(event, pushToSaveOrSendQ);
					newEventsLogCount++;
				});

			} else {			
				evaluateEvent(thisUserEvents[0], pushToSaveOrSendQ);
				newEventsLogCount++;
			}
		}

		// we got data from file, now we can start!
		// *NOTE* Log file can be deleted but needs an empty 
		// object to get started, ie {}
		if (logfileLength !== 0) {

			// loggin/debug loop
			_.forEach(_.keys(fileData), consoleLogLoop);

			// for each user in newEvents array...
			_.forEach(usersWithNewEvents, newEventsLoop);

		} else {
			console.log('Nothing in log, saving all new events');
			toSaveQ = newEventsArray;
		}

		// purely optional debug info
		console.log(
			'\nStatus\n' +
			  '======\n' +
				'\tOld: \t\t' + logfileEventCount + '\n' +
				'\tNew: \t\t' + newEventsLogCount + '\n' +

			'\nNew Events\n' +
			  '==========\n' +
				'\tSaved: \t\t' + toSaveQCounter + '\n' +
				'\tSent : \t\t' + toSendQCounter + '\n'	+
				'\tExpected: \t' + newEventsLogCount + '\n' +

			'\nTotals\n' + 
			  '======\n' +
				'\tSent: \t\t' + countObjectKeys(toSendQ) + '\n' +
				'\tSaved: \t\t' + countObjectKeys(toSaveQ) + '\n' +
				'\tExpected: \t' + ( Number(logfileEventCount) + Number(countObjectKeys(newEventsArray))) + '\n'
		);

		s3.upload({
			Bucket: srcBucket,
			Key: s3params.Key,
			ContentType: 'application/json',
			Body: JSON.stringify(toSaveQ)
		}, function (err) { 
			if (err) {
				next(err, null);
			}
			console.log('... saved log ok');
			next(null, toSendQ);
		});
		
	});
}

exports.handler = function (event, context) {
	srcBucket = event.Records[0].s3.bucket.name;
	srcKey = event.Records[0].s3.object.key;

	async.waterfall([
		fetchLogFromS3,
		uncompressLog,
		filterUsers,
		checkRecentEventLog,
		sendEmails
	], function finalCallback(err) {
		if (err) {
			console.error('Failed to publish notifications: ', err);
		} else {
			console.log('Successfully completed CloudTrail processing run');
		}
		context.done(err);
	});
};