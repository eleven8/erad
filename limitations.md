# Limitation/Requirements


## Eleven

Support only password authentication



## Marriott

Uses real username in anonymous identity field in EAP requests

Expect full EAP session - ERAD radius server should proxy `outer` and `inner` eap tunnels.



## Misc

EAP-TLS request not expected to be proxied.

EAP-TLS request does not contain real user identity, only certificate id. Real user identity can be retrieved from ERAD API response. This is **TRUE** only for EAP-TLS requests, other requests can be matched with wildcarded username stored in ERAD API db. So for other requests types use username from radius request.

